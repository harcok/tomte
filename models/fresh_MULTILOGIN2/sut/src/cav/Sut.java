/*
 * sut is generated from model  : /home/harcok/workspace3.8.1/tomte/input/Fresh_CAV_LOGIN/model.uppaal.xml
 */

package cav;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

//0.3 import org.apache.log4j.Logger;

public class Sut implements sut.interfaces.SutInterface {
    //0.3 private static final Logger logger = Logger.getLogger(Sut.class);

    private Random random = new Random(1234567890); 

    // Initialize statemachine constants,variables and locations
    
    HashMap < Integer,Integer > id2pwd = new HashMap < Integer,Integer > ();
    HashMap < Integer,Boolean > id2loggedin = new HashMap < Integer,Boolean > ();

	private int MAX_REGISTERED_USERS=2;
	private int MAX_LOGGEDIN_USERS=1000000;
	private int loggedin_users=0;



    private int getRandomInt() {
        //return random.nextInt(10000000)+10000000; range between 10 and 20 miljoen
        // note: signed maxint ~ 2 miljard, so draw value between 1 and 2  miljard
    	return random.nextInt(1000000000)+1000000000;
    }

    //handling each Input

    /* register an uid
     * 
     * notes:
     *   - you can only register once for a specific uid
     *   - at max only MAX_REGISTERED_USERS may be registered 
     */
    public OutputAction IRegister(int uid) {
        String methodName = "ONOK";
        List < Parameter > params = new ArrayList < Parameter > ();
        
        if ( ! id2pwd.containsKey(uid)  && id2pwd.keySet().size() < MAX_REGISTERED_USERS ) {
        	methodName = "OOK";
        	int pwd=getRandomInt();
        	id2pwd.put(uid, pwd);
        	id2loggedin.put(uid, false);
            params.add(new Parameter(pwd, 0));        	
        } 
        return new OutputAction(methodName, params);
    }    
    
    /* login an user with uid
     * 
     * notes:
     *   - An user can only login, if the uid  
     *       + is registered 
     *       + and is not logged in
     *   - at max only MAX_LOGGEDIN_USERS users may be logged in 
     */   
    public OutputAction ILogin(int uid,int pwd) {
        String methodName = "ONOK";
        List < Parameter > params = new ArrayList < Parameter > ();

        /*
        System.out.println(uid);
        System.out.println(pwd);
        System.out.println(id2loggedin.keySet().size());
        System.out.println(id2pwd.containsKey(uid)));
        System.out.println(id2pwd.get(uid));
        */
        if ( id2pwd.containsKey(uid) 
        		&& ! id2loggedin.get(uid) 
        		&& pwd == id2pwd.get(uid)
        		&& loggedin_users < MAX_LOGGEDIN_USERS 
            ) {
        	methodName = "OOK";
            loggedin_users++;
        	id2loggedin.put(uid, true );
            params.add(new Parameter(getRandomInt(), 0));   // none important fresh output value     	
        } 
        return new OutputAction(methodName, params);
    }
    
    /* ILogout
     * 
     * A user can only logout when logged in.
     */
    public OutputAction ILogout(int uid) {
        String methodName = "ONOK";
        List < Parameter > params = new ArrayList < Parameter > ();

        if ( id2loggedin.containsKey(uid) && id2loggedin.get(uid) ) {
        	methodName = "OOK";
        	id2loggedin.put(uid, false );
            loggedin_users--;
            params.add(new Parameter(getRandomInt(), 0));   // none important fresh output value      	
        } 
        return new OutputAction(methodName, params);
    }
    
 
    /* IChangePassword
     * 
     * a  user can only change password when logged in
     */
    public OutputAction IChangePassword(int uid) {
        String methodName = "ONOK";
        List < Parameter > params = new ArrayList < Parameter > ();

        if (  id2loggedin.containsKey(uid) &&  id2loggedin.get(uid) ) {
        	methodName = "OOK";
        	int pwd=getRandomInt();
        	id2pwd.put(uid, pwd);
            params.add(new Parameter(pwd, 0));        	
        } 
        return new OutputAction(methodName, params);
    }      


    



    // handling all inputs
    public OutputAction handle(InputAction inputAction) {
        String methodName=inputAction.getMethodName();
        if (methodName.equals("ILogout")) {
            List < Parameter > params = inputAction.getParameters();
            int uid=params.get(0).getValue();
            return ILogout(uid);
        }
        if (methodName.equals("IRegister")) {
            List < Parameter > params = inputAction.getParameters();
            int id=params.get(0).getValue();
            return IRegister(id);
        }
        if (methodName.equals("IChangePassword")) {
            List < Parameter > params = inputAction.getParameters();
            int id=params.get(0).getValue();
            return IChangePassword(id);
        }
        if (methodName.equals("ILogin")) {
            List < Parameter > params = inputAction.getParameters();
            int id=params.get(0).getValue();
            int pwd=params.get(1).getValue();
            return ILogin(id,pwd);
        }
        
        throw new RuntimeException("SUT does not support the input:" + inputAction.getMethodName());
    }

  	@Override
	public sut.interfaces.OutputAction sendInput( sut.interfaces.InputAction origInputAction) {
		InputAction inputAction = new InputAction(origInputAction); // make copy to be safe!!
		OutputAction  outputAction=handle(inputAction);
		return outputAction;  //outputAction implements sut.interfaces.OutputAction interface
	}

	@Override
	public void sendReset() {
    	// no one registered
        id2pwd = new HashMap < Integer,Integer > ();
        // no one logged in
        loggedin_users=0; // keeping separate counter is faster than counting true values in id2loggedin map
        id2loggedin = new HashMap < Integer,Boolean > ();
	}

}
