import os, sys
script_dir=os.path.abspath(os.path.dirname(sys.argv[0]))
script_name=os.path.basename(sys.argv[0])

# changedir to python scriptdir and add this to the python path
python_script_dir=os.path.abspath(script_dir+'/../share/pymodules/')
python_script_path=os.path.abspath(script_dir+'/../share/pymodules/' + script_name)
sys.path.insert(0, python_script_dir)

#os.chdir(python_script_dir)

import pprint
#pprint.pprint(sys.path)
##pprint.pprint([script_name] + sys.argv[1:])
#
#print script_dir
#print script_name
#print python_script_dir
#print python_script_path

def run(prefix=None):
    try:
        if prefix == None:
            # use  xxxx_ as prefix  where x!=_ 
            module=script_name[script_name.find('_')+1:]
        else:    
            module=script_name.replace(prefix,'')
        #print module
        mod = __import__(module)
        # use name of this script and not of python module!!
        #mod.main([script_name] + sys.argv[1:])
        mod.main([python_script_path] + sys.argv[1:])
    except KeyboardInterrupt as e:
        # Note: KeyboardInterrupt happens when user kills program with CTRL-C
        #       When this happens we display a nice message instead of a traceback  :
        print >> sys.stderr, "\n\nExiting on user cancel."
        sys.exit(1)
