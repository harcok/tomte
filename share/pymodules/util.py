

#-------------------------------------------------------------------------------
#   file/name utilities 
#-------------------------------------------------------------------------------

def strip_extension(path):
    """
      strips extension from filepath
      note: assumes no . in directory names!
    """
    if '.' in path:
       return path[:path.find('.')]
    else:
       return path 
        

def get_bare_name(path):
    """
       gives bare name of path :
         - strips dirs
         - strips extension
       e.g. /tmp/a/bla.bak.txt -> bla
    """
    import os.path
    return strip_extension(os.path.basename(path))
            

#-------------------------------------------------------------------------------
#   file/dir checkers : they abort program on failure
#-------------------------------------------------------------------------------

def check_file_extension(file,extension):
    """
    check_file_extension - checks file extension where on failed check it exits the program
                           with an error message.
                           The extension is checked case insensitive.

      extension: the extension string without the '.'
                 e.g. txt
    """
    if  not file.lower().endswith( "." + extension.lower()):
       app_error("file '"+ file + "' is not a " + extension.lower() +  " file!!")

def check_isreadablefile(file):
    if  not isreadablefile(file):
        app_error("input file '" + file + "' is not readable")

def check_iswritabledir(dir):
    if  not iswritabledir(dir):
        app_error("directory '" + dir + "' is not writable")

def check_noneexistingdir(path):
    import os.path
    if os.path.isdir(path):
        app_error("output dir '" + path + "' already exists! \nGive as argument a none-existing directory name which will be created for you.")
#-------------------------------------------------------------------------------
#   file/dir  helpers
#-------------------------------------------------------------------------------





def isreadablefile(path):
    import os
    return  os.path.isfile(path) and os.access(path,os.R_OK)

def iswritablefile(path):
    import os
    return  os.path.isfile(path) and os.access(path,os.W_OK)


def iswritabledir(path):
    import os
    return  os.path.isdir(path) and os.access(path,os.W_OK)


def parentdir(dir):
    import os
    return os.path.dirname(dir.rstrip('/\\'))


def create_dir(dir_name):
    """
        create dir if not yes exists
    """
    import os
    if not os.path.isdir(dir_name):
        os.mkdir(dir_name)

def create_path(path_name):
    import os
    # determine subpaths by first finding absolute path
    # strip heading and tailing directory separators
    # split on directory separator
    subpaths=os.path.normpath(path_name).strip(os.sep).split(os.sep)

    if subpaths[0][1] == ":":
       # windows path starting with drive letter
       path= subpaths.pop(0) + os.sep
    else:
       # unix rootpath starting with separator
       path=os.sep

    while subpaths:
       path=path + os.sep + subpaths.pop(0)
       create_dir(path)

#-------------------------------------------------------------------------------
#   config  helpers
#-------------------------------------------------------------------------------


def write_config(output_file,data):
    import yaml  # external libraries   -> installed local copy   (pure python versions) (not yaml doesn't use libyaml but pure python version)

    selected_keys=('inputInterfaces','outputInterfaces','constants')
    #dictionary comprehension only supported for python 2.7 and higher
    #config={k:v for k,v in data.iteritems() if k in selected_keys}
    config={}
    for k,v in data.iteritems():
        if k in selected_keys:
            config[k]=v

    with open(output_file,'w') as f:
      f.write(yaml.safe_dump(config,default_flow_style=False))
      #f.write(yaml.safe_dump(config))

#-------------------------------------------------------------------------------
#   error
#-------------------------------------------------------------------------------

def app_error(msg,return_code=1):
    print "\n\nAPPLICATION ERROR:\n"
    print msg
    print "\n\n"
    exit(return_code)




#-------------------------------------------------------------------------------
#   check cmdline programs required
#-------------------------------------------------------------------------------

def os_is_windows():
    """
      returns true if os is windows
    """
    import platform
    current_os = platform.system()
    #note: current_os.startswith('CYGWIN') is only true if using cygwin's python
    #      and not when using standard window's python from cygwin bash shell
    if  current_os == 'Linux' or current_os == 'Darwin' or  current_os.startswith('CYGWIN'):
        return False
    if current_os == 'Windows':
        return True
    return False


def check_program(program,windows_extension='.exe'):

    # set extra extension for windows programs 
    # default windows extension for windows program is '.exe' but others are also allowed
    # windows extension is ignored for other operations systems
    extension=''
    if os_is_windows():
        extension=windows_extension

    if not which(program+extension):
      print "Error 1: cannot find program '" +  program + "'in PATH"
      exit(1)

def which(program):
    import os

    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

#-------------------------------------------------------------------------------
#   run commandline programs
#-------------------------------------------------------------------------------



def join_command(command):
    """
        Joins an command existing of an array of strings in a safe way
        Safe means : if a string in the array contains spaces it will
                     be enclosed with quotes in final string.
    """

    if not isinstance(command,list):
      return command

    strings=[]
    for str in command:
        if ' ' in str:    # put " qoutes arround string
          str=str.replace('"', '\\"')
          strings.append('"'+str+'"')
        else:
          strings.append(str)
    return ' '.join(strings)

def run_command(command,cwd=None):
    import subprocess
    try:
      process = subprocess.Popen(command,cwd=cwd)
    except Exception as inst:
      app_error("1. problem running command : \n   "  + join_command(command) + "\n problem : " + str(inst))

    process.communicate()  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
        app_error("2. problem running command : \n   "  + join_command(command),process.returncode)

def run_command_with_output(command,input=None,cwd=None):
    import subprocess
    try:
      process = subprocess.Popen(command,cwd=cwd,stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    except Exception as inst:
      app_error("problem running command : \n   "  + join_command(command))

    [stdoutdata, stderrdata]=process.communicate(input)  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
      print stderrdata
      app_error("problem running command : \n   "  + join_command(command),process.returncode)

    return stdoutdata



# def check_command_available(command):
#     args=['which',' "print \'hallo\'" ']
#     process = subprocess.Popen(["which",args])
#     return_code=process.wait()
#     check_return_code(return_code,"problem running print")
#


#-------------------------------------------------------------------------------
#   test
#-------------------------------------------------------------------------------

def test_run_command_with_output():
    command=['/bin/ls','/home/']
    print run_command_with_output(command,input=None,cwd=None)

def test_run_command():
    #program = "ppython"
    program = "python"

    python_lines=[
      "import time",
      "print 'start'",
      "time.sleep(2)",
      'print "end"'
    ]

    args=["-c" , ';'.join(python_lines) ]
    command=[ program ] + args

    #args=['python','-c','import time;print "start";time.sleep(2);print "end";']
    print "run command in child"
    run_command( command )
    print "back at parent"

