import logging
logger = logging.getLogger(__name__)

import os, sys
# add parentdir of python scriptdir  to the python path
script_dir=os.path.abspath(os.path.dirname(sys.argv[0]))
python_script_dir=os.path.abspath(script_dir+'/../')
sys.path.insert(0, python_script_dir)


 

################################################################################
#    test setup
################################################################################

import batchrun
import batchruncmd

from multiprocessing import  current_process

import tempfile
import time

# def ExampleTaskCmd(task,pid):
#     command=[ '/bin/ls','/tmp/' + task['name'] ]
#     task['cmd']=command
#     result=batchrun.run_command(command)
#     return result


def ExampleTask(task,pid):
    import random
    # initialize static function variable
    if not hasattr(ExampleTask,"counter"):
       ExampleTask.counter=0
       #random.seed(pid)

       #TODO: remove pid as argument!
       #      REASON: Not often needed, and when needed you can get it like this:
       pid=current_process().pid
       random.seed(task['seed']*pid)

    # task code
    num =random.randint(1,3)
    time.sleep(num)
    ExampleTask.counter=ExampleTask.counter+1

    # create final result object
    result={"processId":pid, "counter":ExampleTask.counter}
    return result

def getExampleTasksRootDir():
    rootdir=tempfile.gettempdir()  + os.path.sep + "my_batch_test"
    if not os.path.isdir(rootdir):
        os.mkdir(rootdir)
        for dir in "abcdefghijklmnopqrstuvwxyz":
           os.mkdir(rootdir + os.path.sep+dir)
    return rootdir



class ExampleBatch(batchrun.SubdirsTimedSeededBatch):
    def onBeforeTaskExec(self,data):
         logger.info("start task : " + data['task']['name'] + " seed: " + str(data['task']['seed']) )
         return super(ExampleBatch,self).onBeforeTaskExec(data)

    def onAfterTaskExec(self,data):
         super(ExampleBatch,self).onAfterTaskExec(data)
         real_time_int=int(round(data['end_time']-data['start_time']))
         data['time']= real_time_int
         del data['start_time']
         del data['end_time']
         return  data

    def onResult(self,data):
         if data['runTask']:
           logger.info("finished task : " + data['task']['name']  + " (seed:" + str(data['task']['seed']) + ",time:" + str(data['time']) + ")")
         else :
           logger.info("fetched  task : " + data['task']['name']  + " (seed:" + str(data['task']['seed']) + ")")
         return super(ExampleBatch,self).onResult(data)


################################################################################
#     test main
################################################################################



    
def run_test(args):
    
    logger.info("prepare tasks")
    tasksRootDir=getExampleTasksRootDir()
    resultsRootDir=os.path.join(tasksRootDir,'.results')
    seedList=[123,345]
    b=ExampleBatch(ExampleTask,seeds=seedList,max=8)

    b.setTasksRootDir(tasksRootDir)
    b.setResultsRootDir(resultsRootDir)


    b.setSubdirPrefixes([""])
    b.addTasksFromTaskRootDir()

    logger.info("run tasks")
    results=b.run()


    logger.info("finished tasks")
    # if possible print third result of now executed tasks
    if len(results) > 2:
      logger.info("\nthird returned result")
      result=results[2]
      logger.info(pprint.pformat(result))

    # you can also fetch results later from disk
    logger.info("\nall results")
    results=b.getTasksResultsFromDisk()
    for result in results:
      logger.info(pprint.pformat(result))


    logger.info("cleanup")
    shutil.rmtree(tasksRootDir)

def main(args):
    """ does main stuff : args parsing, initialization logging,
                          then starts application with parsed arg data
    """
    import applogging
    # get log level from args
    logLevel=applogging.getLogLevelFromArgs(args,defaultLogLevel="INFO")
    # initialize application specific logging
    applogging.initLogging(logLevel)
    logging.getLogger('batchrun').setLevel(logLevel)
    #    `->  customize logging in this application speficic applogging module
    #    -> pass "OFF" or "NONE" to disable logging

    # start application
    run_test(args)

if __name__ == "__main__":
    main(sys.argv)


