
"""

unittests for python modules in tomte project

run all tests:
   python test.py

run single test :
   # it is needed you cd to this directory, and then :
   python -m unittest test.TestCompare.test_compare

"""

import unittest

import os, sys

# add parentdir of python scriptdir  to the python path
script_dir=os.path.abspath(os.path.dirname(sys.argv[0]))
python_script_dir=os.path.abspath(script_dir+'/../')
sys.path.insert(0, python_script_dir)


class TestLearnresult2uppaal(unittest.TestCase):
     def setUp(self):
         
         self.abstractmodel1=script_dir + "/stateVar/outputabstr/model.dot"
         self.abstractions1=script_dir + "/stateVar/outputabstr/abstractions.json"
         self.concreteModel1= script_dir + "/stateVar/outputabstr/out.xml"
         
         self.abstractmodel=script_dir + "/stateVar/inputabstr/model.dot"
         self.abstractions=script_dir + "/stateVar/inputabstr/abstractions.json"
         self.concreteModel= script_dir + "/stateVar/inputabstr/out.xml"
         
         self.abstractmodel2=script_dir + "/stateVar/other/model.dot"
         self.abstractions2=script_dir + "/stateVar/other/abstractions.json"
         self.concreteModel2= script_dir + "/stateVar/other/out.xml"         
         
         self.giveAbstractInputsInstead= False

     def test_stateVar1(self):
           import learnresult2uppaal
           learnresult2uppaal.learnresult2uppaal_statevar(self.abstractions1,self.abstractmodel1,self.concreteModel1,self.giveAbstractInputsInstead)

     def test_stateVar2(self):
           import learnresult2uppaal
           learnresult2uppaal.learnresult2uppaal_statevar(self.abstractions2,self.abstractmodel2,self.concreteModel2,self.giveAbstractInputsInstead)


     def test_stateVar(self):
           import learnresult2uppaal
           learnresult2uppaal.learnresult2uppaal_statevar(self.abstractions,self.abstractmodel,self.concreteModel,self.giveAbstractInputsInstead)
           #result=compare.compare(self.modelfile1,self.modelfile2,0,0,False)
           #self.assertTrue( result.strip().endswith('TRUE') )
           #self.assertTrue( True )


#import compare
#import uppaal2bcg
#
#class TestCompare(unittest.TestCase):
#     def setUp(self):
#         self.modelfile1=script_dir + "/learnedModel.xml"
#         self.modelfile2=script_dir + "/sut.xml"
#
#     def test_compare(self):
#           global CONFIG
#           CONFIG=uppaal2bcg.settings([script_dir + '/../'] )
#           result=compare.compare(self.modelfile1,self.modelfile2,0,0,False)
#           self.assertTrue( result.strip().endswith('TRUE') )
#
#     def test_remote_compare(self):
#           global CONFIG
#           CONFIG=uppaal2bcg.settings([script_dir + '/../'] )
#           result=compare.compare(self.modelfile1,self.modelfile2,0,0,False)
#           self.assertTrue( result.strip().endswith('TRUE') )

if __name__ == '__main__':
    #unittest.main()
    suite = unittest.TestSuite()

    #suite.addTest(TestCompare("test_compare"))
    suite.addTest(TestLearnresult2uppaal("test_stateVar"))
    suite.addTest(TestLearnresult2uppaal("test_stateVar1"))
    suite.addTest(TestLearnresult2uppaal("test_stateVar2"))
    unittest.TextTestRunner().run(suite)
