import logging
logger = logging.getLogger(__name__)

import os, sys
# add parentdir of python scriptdir  to the python path
script_dir=os.path.abspath(os.path.dirname(sys.argv[0]))
python_script_dir=os.path.abspath(script_dir+'/../')
sys.path.insert(0, python_script_dir)


 



################################################################################
#    test setup
################################################################################

import batchrun_new as batchrun

from multiprocessing import  current_process

import performer

import time,random

# def ExampleTaskCmd(task,pid):
#     command=[ '/bin/ls','/tmp/' + task['name'] ]
#     task['cmd']=command
#     result=batchrun.run_command(command)
#     return result


import os, errno

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


def taskFunc(taskData):
    # initialize static function variable
    if not hasattr(taskFunc,"counter"):
       taskFunc.counter=0

    # task code
    num =random.randint(1,3)
    time.sleep(num)
    taskFunc.counter=taskFunc.counter+1

    print "counter",taskFunc.counter 

    # create final result object
    result={"counter":taskFunc.counter}
    return result

def taskDiskFunc(taskData):
    result=taskFunc(taskData)
    rootResultDir=taskData['resultsRootDir']
    mkdir_p(rootResultDir)
    f=open(os.path.join(rootResultDir,'bla.txt'),'w')
    f.write('hoi\n')


def getExampleTasksRootDir(prefix=""):
    import tempfile
    rootdir=tempfile.gettempdir()  + os.path.sep + "my_batch_test"
    if not os.path.isdir(rootdir):
        os.mkdir(rootdir)
        for subdir in "abcdef":
        #for subdir in "abcdefghijklmnopqrstuvwxyz":
           os.mkdir(rootdir + os.path.sep+prefix+subdir)
    return rootdir



# class ExampleBatch(batchrun.SubdirsTimedSeededBatch):
#     def onBeforeTaskExec(self,data):
#          logger.info("start task : " + data['task']['name'] + " seed: " + str(data['task']['seed']) )
#          return super(ExampleBatch,self).onBeforeTaskExec(data)
# 
#     def onAfterTaskExec(self,data):
#          super(ExampleBatch,self).onAfterTaskExec(data)
#          real_time_int=int(round(data['end_time']-data['start_time']))
#          data['time']= real_time_int
#          del data['start_time']
#          del data['end_time']
#          return  data
# 
#     def onResult(self,data):
#          if data['runTask']:
#            logger.info("finished task : " + data['task']['name']  + " (seed:" + str(data['task']['seed']) + ",time:" + str(data['time']) + ")")
#          else :
#            logger.info("fetched  task : " + data['task']['name']  + " (seed:" + str(data['task']['seed']) + ")")
#          return super(ExampleBatch,self).onResult(data)


################################################################################
#     test main
################################################################################

class TimedPerformer(performer.TimedMixin,performer.Performer):
   pass
   
class CacheTimedPerformer(performer.CacheMixin,performer.TimedMixin,performer.Performer):
   pass   

class SubdirsBatch(batchrun.SubdirsMixin,batchrun.Batch):
    pass

class ParameterizeBatch(batchrun.ParameterizeMixin,batchrun.Batch):
    pass

class SubdirsParameterizeBatch(batchrun.SubdirsMixin,batchrun.ParameterizeMixin,batchrun.Batch):
    pass

import pprint
import shutil


def run_test_singlePerformer(args):
    
    logger.info("make performer run")
    
    taskData={}
    workItem=(taskFunc,taskData)
    

    
    performerKeywordArgs={
      'workerNum' : 1,
      'pid' : 234,
    }
    performer=TimedPerformer(**performerKeywordArgs)
    runData=performer.run(workItem)
    
    performer=TimedPerformer()    # uses default pid:-1 and workerNum:-1
    runData=performer.run(workItem)
        
    pprint.pprint(runData)     
 
def run_test_singleDiskPerformer(args):
    
    logger.info("make performer run")
    
    import tempfile
    resultsRootDir=os.path.join(tempfile.gettempdir(),"results")
    logger.info("resultsRootDir: "+resultsRootDir)
    taskData={
      'resultsRootDir': resultsRootDir
    }
    workItem=(taskDiskFunc,taskData)
    
    
    
    performerKeywordArgs={
      'workerNum' : 1,
      'pid' : 234,
    }
    
    performer=TimedPerformer(**performerKeywordArgs)
    runData=performer.run(workItem)
    
    performer=TimedPerformer()    # uses default pid:-1 and workerNum:-1
    runData=performer.run(workItem)
    
    logger.info("cleanup")    
    shutil.rmtree(resultsRootDir)
     
        
    pprint.pprint(runData)        
   
def run_test_singleCachePerformer(args):
    
    logger.info("make performer run")
    
    taskData={}
    workItem=(taskFunc,taskData)
    
    import tempfile
    resultsRootDir=tempfile.mkdtemp(prefix='batchrun_')
    
    performerKeywordArgs={
      'workerNum' : 1,
      'pid' : 234,
      'resultsRootDir' : resultsRootDir
    }
    performer=CacheTimedPerformer(**performerKeywordArgs)
    runData=performer.run(workItem)
        
    pprint.pprint(runData)        
   
    
def run_test_manual_batch(args):
        
    logger.info("make performer run")
    
    taskData={}
    workItem=(taskFunc,taskData)
    
    b=batchrun.Batch(TimedPerformer)
    
    b.addTasks([workItem,workItem])
    b.addTask(workItem)
    results=b.run()
    
    pprint.pprint(results)  



def run_test_subdirs(args):
    logger.info("prepare tasks")
    subdirPrefix="task"
    tasksRootDir=getExampleTasksRootDir(subdirPrefix)
    logger.info("tasksRootDir:" +tasksRootDir)
        
    b=SubdirsBatch(TimedPerformer,max=2)
    b.setTasksRootDir(tasksRootDir)
    b.setSubdirPrefixes([subdirPrefix])
    b.setTaskFunc(taskFunc)
    b.addTasksFromTaskRootDir(taskFunc=taskFunc)
    
    logger.info("run tasks")    
    results=b.run()
    
    # return result to normal stdout output
    logger.info("print results")  
    pprint.pprint(results)  

    logger.info("cleanup")    
    shutil.rmtree(tasksRootDir)
    
def run_test_subdirs_parameterize(args):
    logger.info("prepare tasks")
    subdirPrefix="task"
    tasksRootDir=getExampleTasksRootDir(subdirPrefix)
    logger.info("tasksRootDir:" +tasksRootDir)
    paramSets=[ {'a':1, 'b':1 }, {'a':2, 'b':3 } ]
        
    b=SubdirsParameterizeBatch(TimedPerformer,max=2)
    b.setParamSets(paramSets)
    b.setTasksRootDir(tasksRootDir)
    b.setSubdirPrefixes([subdirPrefix])
    b.setTaskFunc(taskFunc)
    b.addTasksFromTaskRootDir(taskFunc=taskFunc)
    
    logger.info("run tasks")    
    results=b.run()
    
    # return result to normal stdout output
    logger.info("print results")  
    pprint.pprint(results)  

    logger.info("cleanup")    
    shutil.rmtree(tasksRootDir)  
    

def run_test_old(args):
    
    logger.info("prepare tasks")
    tasksRootDir=getExampleTasksRootDir()
    
    seedList=[123,345]
    
    b=ExampleBatch(ExampleTask,seeds=seedList,max=8)
    b.setTasksRootDir(tasksRootDir)
    b.setSubdirPrefixes([""])
    b.addTasksFromTaskRootDir()
    
    resultsRootDir=os.path.join(tasksRootDir,'.results')
    b.setResultsRootDir(resultsRootDir)

    logger.info("run tasks")
    results=b.run()


    logger.info("finished tasks")
    # if possible print third result of now executed tasks
    if len(results) > 2:
      logger.info("\nthird returned result")
      result=results[2]
      logger.info(pprint.pformat(result))

    # you can also fetch results later from disk
    logger.info("\nall results")
    results=b.getTasksResultsFromDisk()
    for result in results:
      logger.info(pprint.pformat(result))


    logger.info("cleanup")    
    shutil.rmtree(tasksRootDir)

def run_tests(args):
    
    #run_test_singlePerformer(args)
    run_test_singleDiskPerformer(args)
    #TODO: run_test_singleCachePerformer(args)

    #run_test_manual_batch(args)
    #run_test_subdirs(args)
    #run_test_subdirs_parameterize(args)
    #run_test_cache_subdir(args)
    
def main(args):
    """ does main stuff : args parsing, initialization logging,
                          then starts application with parsed arg data
    """
    import applogging
    # get log level from args
    logLevel=applogging.getLogLevelFromArgs(args,defaultLogLevel="NOTSET")
    # initialize application specific logging
    applogging.initLogging(logLevel)
    logging.getLogger('batchrun').setLevel(logLevel)
    logging.getLogger('batchrun_new').setLevel(logLevel)
    logging.getLogger('performer').setLevel(logLevel)
    #    `->  customize logging in this application speficic applogging module
    #    -> pass "OFF" or "NONE" to disable logging

    logger.info("logLevel: " + logLevel)

    # start application
    run_tests(args)

if __name__ == "__main__":
    main(sys.argv)


