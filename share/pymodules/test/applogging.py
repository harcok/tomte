import logging

logLevels=[ 'DEBUG', 'INFO' , 'WARNING', 'ERROR', 'CRITICAL','NOTSET']
logLevelAliases=['FATAL','OFF','NONE']
logLevelStrings=logLevels+logLevelAliases

def getLogLevelFromString(logLevel):
    logLevel=logLevel.upper()
    for level in logLevelStrings:
        if level.startswith(logLevel):
            logLevel=level
            # use fatal as alias for critical 
            if logLevel=='FATAL':
                logLevel='CRITICAL'
            if logLevel in ["OFF" , "NONE"]:    
                logLevel='NOTSET'
            return logLevel
    
    raise ValueError('{0} is not valid log level'.format(logLevel))
    
def getLogLevelFromArgs(args,defaultLogLevel):
    """
        fetch log level from command line option :
          e.g. option : --log=debug
    """

    # if loglevel not set in args then  use defaultLogLevel
    foundLogLevel=getLogLevelFromString(defaultLogLevel)

    # search logOptions 
    logOptions=[ s for s in args if s.startswith('--log=') ]
    if logOptions:
        lastLogOption=logOptions.pop()
        foundLogLevel=  lastLogOption[6:].upper()

    # find right loglevel string for foundLogLevel string
    # note: only prefix needs to match (eg. i,in,inf,info all will be seen  as INFO)
    return getLogLevelFromString(foundLogLevel)   

def initLogging(logLevel="ERROR"):
    # initialize logging for application
 
    # special loglevel disables logging
    if logLevel == "NOTSET":
       # to disable logging just skip initialization,
       # however all logger.X calls still need a register handler
       # otherwise logging framework complains, 
       # => see: https://docs.python.org/2/howto/logging.html#library-config
       # so therefore we install a nullHandler in the rootLogger:
       rootlogger = logging.getLogger('')
       rootlogger.addHandler(logging.NullHandler())
       return
        

    # set default log level for both
    # - root of logger hierarchy
    # - console appender
    logLevelNumber=getattr(logging, logLevel)

    # create file handler which logs even debug messages
    fh = logging.FileHandler('/tmp/spam.log',mode='w')  # default mode='a'
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logLevelNumber)

    # create formatter and add it to the handlers
    #formatter = logging.Formatter('%(asctime)s - %(levelname)-8.8s -  %(filename)20s:%(lineno)-4d  %(name)40.40s:%(funcName)-20.20s  - %(message)s')                                                                                      `-> logger name which is either module or class name!
    #formatstr='%(asctime)s - %(levelname)s -  %(filename)s:%(lineno)d  %(name)s:%(funcName)s  - %(message)s'
    #
    # KISS keep format string simple: level of severity, in which function and what message
    #formatstr='%(levelname)-8.8s %(name)s %(funcName)s: %(message)s'
    #                       `-> note: critical has 8 characters
    #formatstr='%(levelname)-8.8s  %(name)s %(funcName)s: %(message)s'
    formatstr='%(levelname)-8.8s  %(name)s:%(lineno)d %(funcName)s: %(message)s'
    #formatstr='%(levelname)-8.8s %(filename):%(lineno)-4d%(name)s \n %(levelname)-8.8s %(funcName)s: %(message)s'
    formatter = logging.Formatter(formatstr)

    fh.setFormatter(formatter)
    ch.setFormatter(formatter)


    # set root level in rootlogger
    rootlogger = logging.getLogger('')
    rootlogger.setLevel(logLevelNumber)
    # note: do NOT use :
    # rootlogger=logging.RootLogger(logging.DEBUG)
    # -> use 'root' name as root, but the real root has '' as name!

    # add the handlers to the logger
    rootlogger.addHandler(fh)
    rootlogger.addHandler(ch)

    # extend/limit some module 
    #  -> module must be imported before doing next call
    #     because logger must be already setup in module
    #     (verified this!)
    #logging.getLogger('pack.examplemodule').setLevel(logging.DEBUG)
    ## more verbose:
    #logger_pack_logexamplemodule = logging.getLogger('pack.examplemodule')
    #logger_pack_logexamplemodule.setLevel(logging.DEBUG)
