"""Utilities to pretty print xml code.


pformat(xml_string, level=0, step="  ") - Pretty format xml
pprint(xml_string, level=0, step="  ")  - Pretty print xml
strip(xml) - Removes whitespace between end tag and next begin tag
"""

# Author: Harco Kuppens

__all__ = ["pformat","pprint","strip"]

import re, xml.etree.ElementTree  # modules from standard lib (python 2.6 and later)


def indent(elem, level=0, step="  "):
    """Pretty print everything below xml element

       Formatting is different per node type :
       -

         TODO

    """
    # from http://docs.python.org/library/xml.etree.elementtree.html#xml.etree.ElementTree.Element
    #
    # An XML element is everything from (including) the element's start tag to
    # (including) the element's end tag.
    #
    # xml.etree.ElementTree.Element represent a xml element which contains :
    # - tag    : the element type
    # - attrib : dictionary containing the element's attributes
    # - text   : any text found between the element tags
    # - tail   : any text found after the element's end tag and before the next tag
    # - a number of child elements which can be fetched by iterating over the element
    #
    # initial whitespace indention : level*step
    whitespace_sep = "\n" + level*step
    if len(elem):
        # -> element has child tags : only indent content if tag doesn't contain text
        #
        # Indent content of tag not containing text (only childs tags)
        # Indent is done by starting content on next line with one step
        # increased whitespace, which is done by storing this whitespace as
        # the elements text (which was empty!)
        #  -> if tag contains none-whitespace text : we don't indent
        if not elem.text or not elem.text.strip():
            elem.text = whitespace_sep + step
        # next start tag must have same indent as current element
        if not elem.tail or not elem.tail.strip():
            elem.tail = whitespace_sep
        # recursively add indent to child tags
        for elem in elem:
            indent(elem, level+1, step)
        # note: elem is now last child elem
        #  -> next element after last child element must
        #     must have whitespace after it so that the parents closing
        #     tag is alligned with its start tag.
        #  -> if tail contains none-whitespace text : we don't indent
        if not elem.tail or not elem.tail.strip():
            elem.tail = whitespace_sep
    else:
        #
        # note : skipped for root tag which has level=0!
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = whitespace_sep





def pformat(xml_string, level=0, step="  "):
    """Pretty format xml

       Xml code is prettry formatted with the xml.etree.ElementTree module.
       Everything above the root node is not formatted but just literally
       copied in the output because the xml.etree.ElementTree module only
       can access from the root node.
    """
    elem=xml.etree.ElementTree.fromstring(xml_string)
    header=xml_string[:xml_string.find('<' + elem.tag)]
    indent(elem, level, step)
    # print header and then indented tree starting on a new line
    return header.strip() + '\n' + xml.etree.ElementTree.tostring(elem)
    # only for python 2.7 and later:
    # return xml.etree.ElementTree.tostring(elem, encoding="us-ascii", method="xml")

def pprint(xml_string, level=0, step="  "):
    """Pretty print xml

       Xml code is prettry printed with the xml.etree.ElementTree module.
       Everything above the root node is not formatted but just literally
       copied in the output because the xml.etree.ElementTree module only
       can access from the root node.
    """
    print pformat(xml_string, level, step)

def strip(xml):
    """Removes whitespace between end tag and next begin tag

    As long as the XML is well-formed, whitespace between tags is
    allowed. So you can add whitespace between end and begin tags
    make it easier to understand and edit. You can indent
    nested tags and the structre will still remain well-formed.

    However some xml tools still have problems with this whitespace,
    which is incorrect behaviour. To solve problems for these tools
    this function removes all the whitespace.
    """
    return re.sub('>\s*<', '><', xml)
