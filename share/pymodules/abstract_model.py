"""parse abstract model

parse_labels_of_lts(transitions)- Parse labels of labeled transition system

"""

#-------------------------------------------------------------------------------
#  parse abstract model
#-------------------------------------------------------------------------------



def parse_abstract_input(inputstr):
        inputparts=inputstr.split('_')
        inputlabel=inputparts[0]
        inputargs=inputparts[1:]
        return (inputlabel,inputargs)

def parse_abstract_output(outputstr):
        outputupdates={}
        outputfreshvals=[]
        outputlabel="parseOutputError"
        pieces=outputstr.split('_')
        prefixLength=0

        # note: in current abstractions we do assignments before unsets
        for piece in pieces:
            if '=' in piece:
                # assignment x_i
                (statevar,update)=piece.split('=')
                # sanity check on abstraction
                if outputupdates.has_key(statevar):
                    print "error: statevar can only be changed once in parallel update in abstraction: ", outputstr
                    sys.exit(1)
                outputupdates[statevar]=update
                prefixLength=prefixLength+1
            elif piece.startswith("x"):
                # unset x_i
                statevar=piece
                update="-1" # meaning unset statevar
                # sanity check on abstraction
                if outputupdates.has_key(statevar):
                    print "error: statevar can only be changed once in parallel update in abstraction: ", outputstr
                    sys.exit(1)
                outputupdates[statevar]=update
                prefixLength=prefixLength+1
            elif piece.startswith("f"):
                # draw fresh value in system
                outputfreshvals.append(piece)
                prefixLength=prefixLength+1
            else:
                # output method
                outputlabel=piece
                prefixLength=prefixLength+1
                break
        # rest of pieces : output args
        outputargs=pieces[prefixLength:]
        # note: we assume valid outputargs
        return (outputupdates,outputlabel,outputargs,outputfreshvals)




def parse_labels_of_lts(transitions):
    """Parse labels of labeled transition system
    """
    trans_out=[]
    for t in transitions:

        label=t['label']
        [inputstr,outputstr]=label.split('/')
        (inputlabel,inputargs)=parse_abstract_input(inputstr)
        (outputupdates,outputlabel,outputargs,outputfreshvals)= parse_abstract_output(outputstr)
        parsed_t={
          'source' : t['source'],
          'target' : t['target'],
          'input': {
              'name': inputlabel,
              'args': inputargs
          },
          'output': {
              'name': outputlabel,
              'args': outputargs,
              'updates': outputupdates
          },
          'abstractinput':  inputstr,
          'abstractoutput': outputstr,
          'freshValues' : outputfreshvals,
          'altValues': None,
        }
        trans_out.append(parsed_t)

    return trans_out

def setAlternativeValuesInAlternativeInputParam(trans_out):
    # get per (state,input) combo the set of transitions
    stateXmethodY2translist={}
    for t in  trans_out:
         stateXmethodY=(t['source'],t['input']['name'])

         # if key not yet in map then create it
         if stateXmethodY not in  stateXmethodY2translist:
            stateXmethodY2translist[stateXmethodY]=[]

         # add transition to translist for current key
         translist=stateXmethodY2translist[stateXmethodY]
         translist.append(t)

    # for set of transitions with  same source state and input action
    # determine per input parameter the set of used abstraction values
    # and store that in the transition with the abstract value -1 for that param
    for translist in stateXmethodY2translist.values():
         altValues={}
         for paramIndex in range(len(translist[0]['input']['args'])):
             altValues[paramIndex]=set()
             #alternative_transition = None      # for current paramIndex!!
             for transition in translist:
                 abstractValue=transition['input']['args'][paramIndex]
                 if int(abstractValue) == -1:
                    # only store altValues in transition with abstract value -1
                    transition['altValues']=altValues
                 else:
                    # if abstract value != -1 then add it to altValues list
                    altValues[paramIndex].add( int(abstractValue)  )

    return trans_out
