"""Utilities for lts.

get_lts_from_dotfile(dot_file) - Get label transition system from graphviz dot file
print_lts(start_state,transitions) - Print labeled transition system

"""

#-------------------------------------------------------------------------------
#  read lts from dot file
#-------------------------------------------------------------------------------

import re

def get_lts_from_dotfile(dot_file):
    """ Get labeled transition system from graphviz dot file

        The dot file:
           - describes a digraph with labels
           - encodes the start state with the color='red' attribute
             note: this corresponds with the highlighted state in learnlib API


        Returns: [start_state,transions]
        Where :
           - start_state: start state label
           - transitions: list of transitions
             where each transition :
               {
                  'source' :  .. ,
                  'target' :  .. ],
                  'label':  ..
               }

        Note: when parsing dot file it throws away transitions with the keywords :

                 inconsistency or undefined

    """

    start_state='unknown'
    f=file(dot_file)
    lines=f.readlines()

    # find start state
    for line in lines:
        line=line.replace('label=""','')
        if line.find('[color="red"]') != -1:
            start_state=line[:line.find(' ')]
            break


    # get transitions
    transitions=[]
    for line in lines:
        if line.find('->') != -1:
            transitions.append(line)

    # throw away transitions with the keywords : quiescence or inconsistency or undefined
    #transitions = [ t for t in transitions if ( 'quiescence' not in t ) and ( 'inconsistency' not in t  )  and ( 'undefined' not in t  )]
    transitions = [ t for t in transitions if  ( 'inconsistency' not in t  )  and ( 'undefined' not in t  )]

    trans_out=[]
    regexpr_transition=re.compile(r'\s*(\w*)\s*-\>\s*(\w*)\s*\[label=\<(.*)\>\]')
    regexpr_tag=re.compile(r'<[^>]+>')
    for transition in transitions:
        match=regexpr_transition.match(transition)
        if match:
            match=match.groups()
            label=regexpr_tag.sub('',match[2])
            trans_out.append({
              'source' : match[0],
              'target' : match[1],
              'label': label
            })

    return [start_state,trans_out]

#     trans_out=[]
#     start_state='unknown'
#     G=pygraphviz.AGraph(dot_file)
#     transitions=G.edges()
#     for t in transitions:
#         label=t.attr['label']
#         label=re.sub('<[^<]+?>', '', label)
#         trans_out.append({
#           'source' : str(t[0]),
#           'target' : str(t[1]),
#           'label': label
#         })
#     for n in G.nodes():
#         if n.attr['color'] == 'red':
#             start_state=n.attr['label']
#     return [start_state,trans_out]

def print_lts(start_state,transitions):
    """Print labeled transition system

       The transitions are printed as used in a statemachine::

          source -> target : label

    """
    indent='  '
    print "start state: " + start_state
    for t in transitions:
        print t['source'] + ' -> ' + t['target']  + ' : ' + t['label']

