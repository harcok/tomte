"""Utilities for extended state machines.


print_transitions(transitions) - Print transitions
print_transitions_long(transitions) - Print transitions in long format
get_locations(transitions) - Get set of all locations in statemachine
split_input_and_output(transitions) - Split transitions with both an input
                                      and output event into two transitions
get_uppaal_model() - Get uppaal model


"""

# Author: Harco Kuppens

__all__ = ["get_lts_from_dotfile",
           "print_lts",
           "parse_labels_of_lts",
           "read_abstractions_from_jsonfile",
           "apply_mapper_data_on_transitions",
           "print_transitions",
           "print_transitions_long",
           "get_locations",
           "split_input_and_output",
           "get_uppaal_model",
          ]

import sys, re, copy # module from standard lib (python 2.6 and later)
import pprint

import pyuppaal
#import pygraphviz



#-------------------------------------------------------------------------------
# post processing
#-------------------------------------------------------------------------------



def print_transitions_long(transitions):
    """Print transitions in long format

       For each transition the guard,input,output,action are printed
       on a separate line::

          <source> -> <target>
             guard: <guard>
             input: <input>
             output: <output>
             action: <action>

    """
    indent='  '
    for t in transitions:
        print t['source'] + ' -> ' + t['target']
        print indent*2, 'guard:',  t['guard']
        #print indent*2, 'guard:', '[ ', t['guard'] , ' ]'
        print indent*2, 'input:', t['input']
        print indent*2, 'output:', t['output']
        print indent*2, 'action:', t['action']

def print_transitions(transitions):
    """Print transitions

       The transitions are printed as used in a statemachine::

          source -> target : [guard] input/output ; action

    """
    indent='  '
    for t in transitions:
        guard=''
        if  t['guard']:
            guard= " [ " + t['guard']  + ' ] '
        action=''
        if  t['action']:
            action = " ; " + t['action']
        print t['source'] + ' -> ' + t['target']  + ' : ' + guard +  t['input'] + ' / ' +  t['output'] +  action
 
def split_input_and_output(transitions):
    """Split transitions with both an input and output event into two transitions

       Makes two sequential transitions with a dummy state in between:
        - dummy state <midstate> is labeled :
             m_<counter>
        - put guard,input and action on first transition and only output o
          second transition :
            1. first transition :
                           <guard> <input> / <action>
                 <source> ----------------------------> <midstate>
            2. second transition :
                                 _ / <output>
                 <midstate> --------------------------> <target>
    """
    trans_out=[]
    id=0
    for t in transitions:
        midstate= 'm' + str(id)
        trans_out.append({
            'source': t['source'],
            'target': midstate,
            'guard' : t['guard'],
            'input' : t['input'],
            'output' : '',
            'action' : t['action']
        })
        trans_out.append({
            'source': midstate,
            'target': t['target'],
            'guard' : '',
            'input' : '',
            'output' : t['output'],
            'action' : ''
        })
        id=id+1
    return trans_out



def get_locations(transitions):
    """ Get set of all locations in statemachine """

    s=set()
    for t in transitions:
        s.add(t['source'])
        s.add(t['target'])

    return s

def get_uppaal_model(begin_state,transitions,interfaces,statevars,fullHypOutput,cons=[]):
    """Get uppaal model

    fullHypOutput :  if false:  hyp output:  only concrete output
                     if true :  hyp output: special output which contains info
                                  - abstract input
                                  - abstract output
                                  - concrete output
                                HACKED into single concrete output as:
                                O<absinp>__<absoutp>__concreteoutputname(p0,..)
                                                      -------------------------
                                                       `-> normal concrete output

                                note: '-' is serialized as 'XMINX' because
                                      in parsing uppaal interface declarations
                                      we don't allow '-' in method names!

    note: models generate with fullHypOutput==true are only used for learning
          and therefore don't need to declare the output interfaces in the model!

    """

    # TODO:
    # - make decl_str from cons,variables,interfaces :
    #     constants = 1,2;
    #     int v1, v2;
    #     void IIN2(int n2) {}
    # - add begin_state to model : <init ref="id11"/>


    declarations=[]
    cons=set(cons)
    #if cons: declarations.append('constants = ' + ','.join([str(x) for x in cons]) + ';')
    for c in cons:
        declarations.append('const int CONSTANT' + str(c) + ' = ' + str(c) + ';')
       


     
    #if variables: declarations.append('int ' + ','.join(variables) + ';'

    # from interfaces make in uppaal :
    # - method declaration per input/output event
    # - two integer declarations for each parameter of an input
    #    `-> these integers are needed in applying mapper rules
    decs=[]



    for i in interfaces:
        param_types=[]
        pos=0

        for t in  i['param_types']:
            # create string for params in method declaration
            #   note: also fixes param type : Integer->int
            if t=='Integer':
                param_types.append('int p'+str(pos))
            else:
                param_types.append(t+' p'+str(pos))

#             # for each INPUT Ixxx(p0,p1,...) add two integers for each of its parameters
#             #  eg. IBLA(p0,p1)
#             #     -> int first_IBLA_p0,last_IBLA_p0,first_IBLA_p1,last_IBLA_p1
#             if i['method_name'][0] == 'I' :
#                 declarations.append('int first_' +  i['method_name'] + '_p' + str(pos) + ' = -1 ;')
#                 declarations.append('int last_' +  i['method_name'] + '_p' + str(pos) + ' = -1 ;')
            pos=pos+1

        # always add input declarations
        # only add output declarations when giving fullHypOutput==false
        # Note: output alphabet not needed in uppaal model for learning!!
        if  (i['method_name'][0] == "I") or ( not fullHypOutput )   :
            decs.append( 'void ' + i['method_name'] + '(' + ','.join(param_types) + '){}' )

    for statevar in statevars:
        declarations.append( "int " +statevar +" = -1 ;")

    declarations+=decs
    decl_str='\n'.join(declarations)

    system_str='Process = Template();system Process;'
    model= pyuppaal.NTA(declaration=decl_str,system=system_str)

    locs=[]
    initloc=None
    for loc in get_locations(transitions):
        upploc=pyuppaal.Location(id=loc,name=loc)
        locs.append( upploc )
        if loc==begin_state:
            initloc=upploc
 
 
    trans=[]
    for t in transitions:
        source=pyuppaal.Location(id=t['source'],name=t['source'])
        target=pyuppaal.Location(id=t['target'],name=t['target'])
        tran=pyuppaal.Transition(source, target, guard=t['guard'], synchronisation= t['input'] + t['output'] , assignment=t['action'])
 
        trans.append( tran )
 
 
    template=pyuppaal.Template(name='Template', locations=locs, initlocation=initloc, transitions=trans)
    model.add_template(template)
    return model




