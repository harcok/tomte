################################################################################
#   logging 
################################################################################

import logging
logger = logging.getLogger(__name__)  # need such a line for every python file

# library logging
#----------------
#     this module is seen as root of library you use -> because you are as developer
#     production user of this lib
#     A) by default limit loging of lib to only ERROR msgs
logger.setLevel(logging.ERROR)
#     B) add nullhandler so that if app using library doesn't uses logging
#        still a NEEDED(by logging framework) handler is registered which
#        effectively does nothing
logger.addHandler(logging.NullHandler())


################################################################################
#    module Batch
################################################################################

from multiprocessing import Process, Queue, current_process
import pprint
import time
import os
import json
import copy

import sys

# add taskResultDir(dirname) and resultFile(basename)  to extra fields of result object
#   -> makes fetching extra results from taskresultdir easier
#      note: these fields are only added if result is cached to disk using resultMixin

# add separate seed mixin!
# worker must pipe stderr/stdout to stderr/stdout files in task result dir





# task_queue : tasks to be  runned
# work_queue : tasks activated to be executed, removed from this queue when it
#              gets executed! On execution move to in_work_queue
#
# inwork_queue :tasks executing -> kill pid if task not needed anymore
#              eg. if you see that a task finishes because it reaches maxtime
#                  than all tasks of same level params or higher level params can
#                  be aborted (killled). Because not possible to learn with that
#                  or any higher level.
#                  Often low level tasks are finished earlier, so it is
#                  sufficient to just kill all tasks, that is just end batch run.
#                   -> do in onResult which detects the condition in the result
#
# done_queue  : pass result from worker process to batch process for further proc.
# ticket queue :   wait tickets for  futre results -> if queue empty -> nothin to wait for

#TODO: remove pid as argument for task func -> see below in code

#todo: 

#
# todo:   batch.run can be run multiple times
#
#       eg. in second run of batch 
#            - set different global param causing taskfunc to change and rerun all tasks!
#            - or just give another taskfunc as argument to run!
#
#  1) about results :
#     batch run stores results in self.taskresults and combines of all runs in self.results
#         => implemented this, but still need to test this!
#  
#  2) make setter for taskfunc
#                  `->  not do allow as param in init and run
#                      KISS -> only one method to set it!!
#                   -> in run check if taskfunc is set!
#        
#   3) implement ticketQueue  :
#
#       current : in the current implementation we only have addtasks
#                and it adds tasks to both self.tasks and task_queue and updates self.numTasks
#                                           `-> rename to self.runTasks
#                which is used to see if we are done in run method
#            =>  To add a task during a run we have to  batchobject.addTasks([task])
#                which adds to both  task_queue and updates self.numTasks (otherwise batch would 
#                stop to early with waiting on results!)
#
#            problem:
#               update numtasks in batch process not possible from task process,
#               because each process has its own version! -> not shared data!
#            solution : instead of numtasks use a ticket queue
#                when creating new task :
#                    add task to taskqueue
#                    add ticket to ticketqueue  -> each ticket is a promise for a future result in result queue
#                   -> receiver of task results must first get ticket for that it can fetch a result from a result queue
#                      note: the receiver does get_nowait on ticket_queue -> if it doesn't get item
#                            then it knows all result are processed and can stop
#           thus add both task(job for worker) and ticket(promissed result for result processor) !
#
#  4)  means :  we don't have a currentRunTasks list anymore
#               only a taskQueue, but in _worker we can put a get task from taskQueue to 
#               the doneTasksQueue  -> filled more and more by each run
#
#             if run is done taskqueue is empty, so if  you want to repeat
#             a run with the same tasks but different taskfunc -> you have to add the tasks again! 
#             => we can use addTasks method which adds task to taskqueue and also creates tickets
#                for them in ticketqueue
#               note: during run of batch on can add still tasks from a task in two ways : 
#                     - you can use addTask 
#                     - or you must add task to taskqueue and ticket queue yourself!  => prefer addTask
#               note: addTask(task) == addTasks([task])
#                     
#




# note: for windows a Process cannot launch a bound/unbound method
#       but only functions. Reason is that linux uses forking, and gets
#       automatically all state copied.
#       However windows does not use forking, and just creates a new process and
#       therefore needs to pickle/unpickle all state to the new process.
#       Pickling and unpickling is only done for all function arguments, and
#       therefore you cannot use a bound/unbound method as target.
#       Solution: use as target a helper function which gets object of
#                 method as argument!
#
def launchWorker(obj,workerNum):
#    try:
        obj._worker(workerNum)
#     except KeyboardInterrupt as e:
#         import sys
#         # Note: KeyboardInterrupt happens when user kills program with CTRL-C
#         #       When this happens we display a nice message instead of a traceback  :
#         print >> sys.stderr, "\n\nExiting on user cancel."
#         sys.exit(1)

def PositiveNonZeroInt(x):
    try:
        if type(x)!=int:
           return False
        if x>0:
          return True
        else:
          return False
    except ValueError:
        return False

# run batch of tasks
class Batch(object):


    def __init__(self,taskFunc,cmd=None,tasks=[],max=4):
        self.taskFunc=taskFunc
        self.cmd=cmd
        self.maxNumProcesses=max
        self.results=[]
        self.numTasks=0
        self.processes=[]
        
        if not PositiveNonZeroInt(self.maxNumProcesses) :
           # wrong parameters must raise a module exeception -> wrong usage module
           # also log the message as an error message.
           import sys
           msg="param max for Batch must be a positive integer"
           logger.error(msg)
           raise Exception(msg)

        self.funcMode=True
        if self.cmd:
           self.funcMode=False




        # Create queues
        self.task_queue = Queue()
        self.done_queue = Queue()

        self.tasks=[]
        self.workers=[]

        self.addTasks(tasks)

    def cleanup(self):
        self.task_queue.close()
        self.done_queue.close()

    def _worker(self,workerNum):
        """
        Function run by worker process which launches processes task
        by calling taskfunc on it!
        note: when launching the worker the whole batch object state
              is transferred to new process! In this transfer
              the queue objects are special and allow communication/synchronisation
              between the processes. (see multiprocessing module)
              => means : batch object must be pickable/unpickable
        """
        try:
            pid=current_process().pid
            logger.debug("start worker; pid: {pid}".format(pid=pid))
            for task in iter(self.task_queue.get, 'STOP'):
                data={
                   'pid':pid,
                   'workerNum' : workerNum,
                   'task': task,
                   'result':None,
                   'runTask':True
                }
                
                # In case we can somehow prefetch result we can skip the work.
                # Eg. get result from some cache
                # notes:
                #   * If  task['runTask'] is false then the tasked is skipped
                #     from running. The data from the onBeforeTaskProcessed is
                #     used as result instead!
                #   * If  task['runTask'] is false and task['result'] is "SKIP_TASK"
                #     then the task is skipped and even the the result is ignored
                #     in the run method!
                #   * processing "runTask" is done in onBeforeTaskProcessed and
                #     not ininitial getTasks method so that you still are able
                #     to dynamically add tasks during the batch run! 
                #     e.g. in a running task itself you can put new task in the task_queue
                data = self.onBeforeTaskProcessed(data)
                if  data['runTask']:
                    # execute task
                    data=self.onBeforeTaskExec(data)
                    result = self.taskFunc(task,pid)
                    data['result']=result
                    data=self.onAfterTaskExec(data)
                else:
                    # not execute task
                    logger.debug("task result prefetched: {0}".format(task))
                data=self.onAfterTaskProcessed(data)
                self.done_queue.put(data)
        except KeyboardInterrupt as e:
            pass
        logger.debug("stop worker; pid: {0}".format(pid))

    # executed in batch Process
    def onStartBatch(self,results):
        pass

    # executed in batch Process
    def onTaskActivation(self,data):
        return data

    # executed in worker Process
    # note: onAfterTaskProcessed is  called for all tasks, also skipped/cached tasks
    def onBeforeTaskProcessed(self,data):
        return data

    # executed in worker Process
    # only executed for tasks getting executed
    def onBeforeTaskExec(self,data):
        logger.debug(pprint.pformat(data).replace("\n",""))
        return data

    # executed in worker Process
    # only executed for tasks getting executed
    def onAfterTaskExec(self,data):
        return data

    # executed in worker Process
    # note: onAfterTaskProcessed is  called for all tasks, also skipped/cached tasks
    def onAfterTaskProcessed(self,data):
        return data

    # executed in batch Process
    # note: before onResult so that you can do last changes here before outputting
    #       it as a result !
    def onTaskDeActivation(self,data):
        return data

    # executed in batch Process
    # note: onResult is not called for skipped tasks, but is for cached tasks
    def onResult(self,data):
        #logger.debug(pprint.pformat(data).replace("\n",""))
        return data

    # executed in batch Process
    def onEndBatch(self,results):
        pass

    def onAddTask(self,task):
        pass



    def addTasks(self,newTasks):
        self.tasks.extend(newTasks)    # todo: remove numTasks and use ticket queue instead -> see comments in top of file
        self.numTasks=self.numTasks+len(newTasks)  
        for task in newTasks:
            self.onAddTask(task)
             
    def addDataForAllTasks(self,data):
        for task in self.tasks:
            task.update(data)
     
    
    # returns results from all runs
    def getResults(self):
        return self.results

    def terminateWorkers(self):
        for p in self.processes:
            p.terminate()


    def enqueueTasksToWorkQueue(self):
        # Submit  tasks to work queue
        for task in self.tasks:
            self.task_queue.put(task)

    def run(self):
        if not callable(self.taskFunc):
           import sys
           msg="taskfunc must be callable"
           logger.error(msg)
           raise Exception(msg)

        self.doneTasks=0
        self.runResults=[]

        self.enqueueTasksToWorkQueue()

        # Create worker processes
        for i in range(self.maxNumProcesses):
            # launch worker
            # note: for windows we need a function helper because
            #       only function can be used to launch a process
            #       For more info: see comment at launchWorker
            p=Process(target=launchWorker, args=(self,i))
            p.start()
            self.processes.append(p)
            # note: next line is not allowed on windows because whole
            #       batch object must be pickable/unpickable. And process
            #       handles aren't!  -> instead you could store the
            #                           the workers in a module variable instead!
            #self.workers.append(p)


        self.onStartBatch(self.runResults) # results current run (empty list initially)!


        # call taskmanager func with as arg ("init")
        #  -> adds initial tasks!
        # simplest implementation is add all tasks at init, and do nothing
        # with called with any resulting data param (see below)



        # Get and apply results
        # note:  printing of results here and not in task makes printing of
        #        result of a task atomic. If you would do it in the task
        #        itself the different results could be printed mangled because
        #        tasks run in parallel!!
        #        However in this central task the results are serialize in
        #        the done_queue Queue and thus printer on FIFO order!

        while self.numTasks != self.doneTasks:
             data=self.done_queue.get()
             if data["result"] == "SKIP_TASK":
                 logger.debug("skipped task : {0}".format(task))
                 self.doneTasks=self.doneTasks+1
                 continue
             data=self.onResult(data)
             self.runResults.append(data)
             self.doneTasks=self.doneTasks+1

             # call taskmanager func with as arg (data)
             #-------------------------------------
             # check if new task(s) must started
             # and if so start each task with :
             #   first call OnStartTaskDispatch -> executed before worker process  gets it => note :
             #   then add to taskqueue

        self.onEndBatch(self.runResults)
        
        # collect results from all runs
        self.results.extend(self.runResults)

        # Tell child processes to stop
        logger.debug("send STOP to tasks")
        for i in range(self.maxNumProcesses):
            self.task_queue.put('STOP')

        logger.debug("wait until all tasks fetched STOP")
        while not self.task_queue.empty() :
            pass
        logger.debug("finished batch")
        
        # returns results from current run
        return self.runResults


# stores meta result data of task in a result.json file 
# if task asked to run again and the ResultMixin sees a result.json 
# file for this task already exists, then it skips this task from rerunning
class ResultMixin(object):

    def __init__(self,*args,**kwargs):
        self.resultsRootDir = kwargs.pop('resultsRootDir',None)
        if self.resultsRootDir:
            self.setResultsRootDir(resultsRootDir)
        self.resultFile = kwargs.pop('resultFile', "result.json")

        super(ResultMixin, self).__init__(*args,**kwargs)

    def setResultsRootDir(self,resultsRootDir):
        self.resultsRootDir=resultsRootDir
        if not os.path.isdir(self.resultsRootDir):
            os.mkdir(self.resultsRootDir)


    # can be overriden to define a new result file
#TODO: allow set resultFile parameterized
# resultFile =  "${taskname} / bla _ ${seed} / result.json"
#  -> subdirs are automatically created
#   -> parameterized with taskdict and resultsRootDir is just prepended

    def getTaskResultDir(self,task,resultsRootDir):
        taskResultDir=os.path.join(resultsRootDir, task['name'])
        if not os.path.isdir(taskResultDir):
           os.mkdir(taskResultDir)
        return taskResultDir

    def _getTaskResultFile(self,task,resultsRootDir,resultFile):
        taskResultDir=self.getTaskResultDir(task, resultsRootDir )
        return os.path.join(taskResultDir, resultFile)

    def _getTaskResultFromDisk(self,task):
        resultFile=self._getTaskResultFile(task, self.resultsRootDir, self.resultFile)
        if os.path.isfile(resultFile):
            # load cached data from json file
            try:
               result=json.load( file( resultFile ) )
            except Exception as e:
                logger.warning("continue with no result; result file '" + resultFile + "' found but is not of correct json format:" + str(e))
                result=None
        else:
            result=None
        return result


    def getTasksResultsFromDisk(self):
        results=[]
        for task in self.tasks:
            result=self._getTaskResultFromDisk(task)
            if result: results.append(result)
        return results


    def onAddTask(self,task):
        taskResultDir=self.getTaskResultDir(task, self.resultsRootDir )
        # Store in task so task can use it to store stuff in this directory!
        # Should only be used in task, and not for result processing later,
        # because later the result directory could be moved to another
        # location. Therefore we remove the 'resultDir' field from task,
        # before saving result on disk in onAfterTaskProcessed below!
        task['resultDir']=taskResultDir

    def onBeforeTaskProcessed(self,data):
        resultFile=self._getTaskResultFile(data['task'], self.resultsRootDir, self.resultFile)
        olddata=self._getTaskResultFromDisk(data['task'])
        if olddata:
            data['keepOldResult']=True
            # before overwriting with old data get :
            pid=data['pid']
            workerNum=data['workerNum']
            #update currrent data with olddata, and disable task from running again
            data.update(olddata)
            data['runTask']=False  # task needs not to run, we fetched result from cache!
            #fetch done by :
            data['pid']=pid
            data['workerNum']=workerNum
            logger.debug("fetch result from file: " + resultFile)
        return super(ResultMixin,self).onAfterTaskProcessed(data)

    # executed in worker Process
    def onAfterTaskProcessed(self,data):

        # write full_result as json to file
        resultFile=self._getTaskResultFile(data['task'], self.resultsRootDir, self.resultFile)
        if data['task'].has_key('resultDir'):
            del data['task']['resultDir']

        # only write resultFile for new result, that is preserve old result
        # note: end user can twist the keepOldResult value to change the
        #       behaviour when to keep old results and when to overwrite
        #       with a new result
        if data.has_key('keepOldResult'):
            keepOldResult=data['keepOldResult']
            del data['keepOldResult']
        else:
            keepOldResult=False
        if resultFile and not keepOldResult:
            f=open(resultFile, 'w')
            json.dump(data, f, sort_keys=True, indent=4)
            f.close()
        return super(ResultMixin,self).onAfterTaskProcessed(data)


    def run(self):
        if not self.resultsRootDir:
           logger.error("no resultsRootDir set")
           time.sleep(0.01) #needed otherwise IOError and calling cleanup doesn't help
           import sys
           sys.exit()
        return super(ResultMixin,self).run()


class TimeTasks(object):
    """
     mixin class which times tasks
    """
    # executed in worker Process
    def onBeforeTaskExec(self,data):
        data['start_time'] = time.time()
        return super(TimeTasks,self).onBeforeTaskExec(data)

    # executed in worker Process
    def onAfterTaskExec(self,data):
        data['end_time'] = time.time()
        real_time_int=int(round(data['end_time']-data['start_time']))
        data['time']= real_time_int
        return super(TimeTasks,self).onAfterTaskExec(data)

# subdirs determine tasks in batch to run
class SubdirsBatch(Batch):

    def __init__(self,*args,**kwargs):
        self.tasksRootDir = kwargs.pop('tasksRootDir', None)
        if self.tasksRootDir:
           self.setTasksRootDir(self.tasksRootDir)
        self.subdirPrefixes=["task"]
        super(SubdirsBatch, self).__init__(*args,**kwargs)

    def setTasksRootDir(self,tasksRootDir):
        self.tasksRootDir=tasksRootDir


    def setSubdirPrefixes(self,subdirPrefixes):
        if type(subdirPrefixes) is str:
           self.subdirPrefixes=[subdirPrefixes] # single subdir prefix
        elif type(subdirPrefixes) is unicode:
           self.subdirPrefixes=[subdirPrefixes] # single subdir prefix
        else:   
           self.subdirPrefixes=subdirPrefixes # list of subdir prefixes

    def matchesPrefixes(self,dir):
        """
            matchesPrefixes - checks whether dir matches one of the
                              specified prefixes
        """
        for prefix in self.subdirPrefixes:
            if dir.startswith(prefix) : return True


    def addTasksFromTaskRootDir(self):
        """
          You must addTasks explicitly so that we can get onAddTask events
          executed before batch.run.
          Note: we do not call addTasksFromTaskRootDir automatically
                in  setTasksRootDir because there could be still parameters
                needed to be set on batch object which are used in onAddTask event
                Thus user must first set all params of batch, and then call
                add tasks so that we can  execute onAddTask event with all
                batch parameters set!
        """
        if not self.tasksRootDir:
           logger.error("no tasksRootDir set")
           import sys
           sys.exit()
        foundTasks=self.getTasksFromTasksRootDir(self.tasksRootDir)
        self.addTasks(foundTasks)
        return len(foundTasks)



    def getTasksFromTasksRootDir(self,tasksRootDir):
        tasks= []
        walker=os.walk(tasksRootDir)
        (dirpath, dirnames, filenames) = walker.next()
        for dir in dirnames:
            # skip hidden directories
            if dir.startswith('.') :
               continue
            # skip directories which are not matched by the prefixes
            if not self.matchesPrefixes(dir):
               continue
            # strip possible _ from start of dir -> use as exclude marker, but this marker should not be in name!
            taskname=dir.strip('_')
            task={'name':taskname,
                  'taskDir':os.path.join(dirpath,dir)
            }
            tasks.append(task)
        return tasks

    # special case is '_' prefix : means mark subdir to be excluded from learning,
    # however when displaying summary of results include also these marked subdirs
    # where the taskname is the name of the subdir without the "_" prefix
    def disableSubdirExcludeMarker(self):
        if "append" in dir(self.subdirPrefixes):
            self.subdirPrefixes.append("_")

    def run(self):
        if not self.tasksRootDir:
           logger.error("no tasksRootDir set")
           import sys
           sys.exit()
        return super(SubdirsBatch,self).run()

# subdirs determine tasks in batch to run
# if having n seeds then for each subdir n tasks are created, one for each seed  
# using TimeTasks mixin : all task runs are timed
# using ResultMixin mixin : all task results are gathered 
class SubdirsTimedSeededBatch(TimeTasks,ResultMixin,SubdirsBatch):

    def __init__(self,*args,**kwargs):
        self.seeds = kwargs.pop('seeds', [123])
        super(SubdirsTimedSeededBatch, self).__init__(*args,**kwargs)

    def getTasksFromTasksRootDir(self,tasksRootDir=None):
        if not tasksRootDir: tasksRootDir=self.tasksRootDir
        dirTasks=super(SubdirsTimedSeededBatch,self).getTasksFromTasksRootDir(tasksRootDir)
        tasks=[]
        dirTasks.sort()
        for task in dirTasks:
            for seed in self.seeds:
                newTask=copy.deepcopy(task)
                newTask['seed']=seed
                tasks.append(newTask)
        return tasks

    # tasksResults is OrderedDict sorted by taskName
    #   => results are  grouped per taskname (note: multiple seeds per taskname)
    def getTasksResultsFromTasksRootDir(self,tasksRootDir=None):
        # put results per task in ordered dictionary
        from collections import OrderedDict
        tasksResults=OrderedDict()        
        
        if not tasksRootDir: tasksRootDir=self.tasksRootDir

        # get tasks from subdirs
        dirTasks=super(SubdirsTimedSeededBatch,self).getTasksFromTasksRootDir(tasksRootDir)
        dirTasks.sort()

        # get tasks results
        results=self.getTasksResultsFromDisk()

        # sort results by taskname,seed
        sortedResults=sorted(results, key=lambda result: (result["task"]["name"],result["task"]["seed"]) )


        for task in dirTasks:
            taskName=task['name']
            tasksResults[taskName]=[]

        for result in sortedResults:
           taskName=result["task"]["name"]
           # next line can be uncommment if result is eg. stdout of command
           # and can be ignored because real result is stored somewhere else on disk
           # TODO: add ignore result file contents option -> only use as boolean to see if task is done or not!
           #result['result'] =""
           #better:
           #del  result['result']
           # TODO: validate stderr in result if cmd went wrong or not?
           tasksResults[ taskName ] += [ result ]

        return tasksResults


    def getTaskResultDir(self,task,resultsRootDir):
        dir=os.path.join(resultsRootDir, task['name'] + '_' + str(task['seed']))
        if not os.path.isdir(dir):
           os.mkdir(dir)
        return dir

# same as SubdirsTimedSeededBatch but is verbose in output!
class VerboseSubdirsTimedSeededBatch(SubdirsTimedSeededBatch):
    first=True
    def onBeforeTaskExec(self,data):
         logger.info("start task : " + data['task']['name'] + " (workerNum:" + str(data['workerNum']) + " pid:" + str(data['pid']) + " seed:" + str(data['task']['seed']) + ")" )        
         return super(VerboseSubdirsTimedSeededBatch,self).onBeforeTaskExec(data)

    def onAfterTaskProcessed(self,data):
         data=super(VerboseSubdirsTimedSeededBatch,self).onAfterTaskProcessed(data)
         # note: onAfterTaskProcessed is also run for tasks already run and fetched from cache, whereas
         #       onAfterTaskExec is only run for task which are really now executed
         # note: run following here and not in onResult to make sure a finished message
         #       is printed before new start message is printed.
         #       Because onResult is run by batch process  then a new task can
         #       already be started by fetching job from task_queue before
         #       batch process gets it turn! However when you do it in
         #       onAfterTaskProcessed you know it is executed in worker Process,
         #       before passing the result to OnResult in batch Process.
         if data['runTask']:
           logger.info("finished task : " + data['task']['name']  + " (workerNum:" + str(data['workerNum']) + " pid:" + str(data['pid']) + " seed:" + str(data['task']['seed']) + ",  execution time:" + str(data['time']) + ")")
         else :
           logger.info("fetched  task : " + data['task']['name']  + " (workerNum:" + str(data['workerNum']) + " pid:" + str(data['pid']) + " seed:" + str(data['task']['seed']) + ",fetched ex. time:" + str(data['time']) + ")")
         return  data




################################################################################
#   TODO: doc
################################################################################

    # mapping
    # - task <-> filesystem                 -> subdir mixin
    # - result object <-> filesystem
    #     `-> result mixin  :  on onBeforeTaskProcessed checks if  resuls.json exist -> if yes stops execution!
    #                          on onAfterTaskProcessed  stores result on disk
    #               `-> needs method writeResult()
    #                             -> here you have to use results_rootdir and seed etc ..
    #                  needs method readResult()
    # note: both result and done file is unnecessary, if we got result file
    #       then also task is done, because our Batch module only writes result
    #       file when task is done!
    # note:  getTasksFromTasksRootDir skips hidden folders such as .results/ !


# real time of process :
#  http://stackoverflow.com/questions/1557571/how-to-get-time-of-a-python-program-execution
#   -> just use time.time() before and after


# subprocess timing ala unix time  :
#   user(time cpu in usermode)
#   kernel(time cpu in kernel mode: syscalls)
#   real (real time passed on clock!)
#
#  note: kernel/user time can be much bigger than real time on a multicore machine
#        because the timings of multiple cpu's are combined
#        e.g. 4 cpu's doing each 1 second of processing in parallel then :
#               - user+kernel time = 4 second
#               - real time        = 1 second    -> shorter!!
#
#  see: http://stackoverflow.com/questions/1608724/run-time-of-a-subprocess-popen-instance
#
# -> tricky to implement unix time command so for now stick with real time which
#    is ok if we start not more processes than cpu's!!



# about result of task
#   a) gives back result(or part of it)
#   b) result is as side effect stored on disk
#
# ad a)  for dir disk solution
#  1) always use done files! why not? because delivers us
#      smart continuation after an interruption of our batch
#      -> user can specify name of done file!
#     note: for redoing batch either use different output dir or use delete the previous one!
#  2) always store result on disk
#     because we support smart continuation after an interruption of our batch
#     and for that we must have permanent storage of results
#       -> so we remove results list of generic batch solution!
#     note: when batch process is killed then all results in memory are lost!
#      `-> user can specify name of result file!
#          better: on completion task give result to on_finished method
#                  which by default writes task returned value to an result.json file
#                  but user can overwrite this method to :
#                     use another filename, or storage format or
#                     even gather results from disk so that they can also
#                     be included in the result file!



# on_start  -> print some info to console
# on_finish -> print some info to console
#           -> store result to batch output dir
#         => in fact onResult extended!

# terminology :
#  * job queue :  a job is an executable function
#      http://en.wikipedia.org/wiki/Job_queue
#  * task queue : a task is a unit of work    -> executed by some standard function
#      https://developers.google.com/appengine/docs/java/taskqueue/overview#Task_Concepts
#
# main difference is that jobs must be launched each in a new process, where as
# for the taskqueue one can run a set of worker processes which execute the tasks!




# main : launch 4 tasks each in separate process
# each process: puts result in resultqueue and ends
# main: upon receiving result launch new process with task until tasks are finished
#       note: only main launches new processes!
# -> advantage over using a task_queue : each job really gets its own process!

