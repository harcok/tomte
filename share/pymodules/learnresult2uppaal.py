#!/usr/bin/env python
"""
convert output data of tomte's in java implemented learner to a concreet uppaal model

learned data :
   json file :  specifies learned abstractions and input,output alphabet sut
    dot file :  specifies learned abstract model 
"""


# Author: Harco Kuppens


import sys, re, pprint  # modules from standard lib (python 2.6 and later)
import argparse

import pp_xml  # local module (which only uses standard lib modules)
import efsm    # local module ; next to standard lib modules it uses: pyuppaal module -> locally installed
import util

import abstract_model,abstractions,lts,mapper

def learnresult2uppaal(json_filename_in,dot_filename_in,model_file_out,fullHypOutput):
    """
     convert output data of tomte's in java implemented learner to a concreet uppaal model

     learned data :
        json file :  specifies learned abstractions and input,output alphabet sut
        dot file :   specifies learned abstract model 
    """

    # get start state and labeled transitions
    [start_state,labeled_transitions]=lts.get_lts_from_dotfile(dot_filename_in)

    #TODO: get below info also from yaml file!!
    [input_abstractions,cons,interfaces]=abstractions.read_abstractions_from_jsonfile(json_filename_in)
    # min max value in json are used for learning

    # from interfaces determine number_params per action
    #       only used to verify if actual action in transition has the correct
    #       number of params  in  mapper.apply_mapper_data_on_transitions
    number_params={}
    for i in interfaces:
        number_params[ i['method_name'] ] = len(i['param_types'])

    # get abstract transitions
    abstract_transitions=abstract_model.parse_labels_of_lts(labeled_transitions)
    # for inputactionparam with abstraction -1 we need to know which abstractions are use for 
    # that param in other transitions with same method from same state to determine
    # what exactly -1 means! Note: in reduce hyp not all global defined alternative abstractions could not all be used in that state! 
    abstract_transitions=abstract_model.setAlternativeValuesInAlternativeInputParam(abstract_transitions)

    (concrete_io_transitions,statevars)=mapper.apply_mapper_data_on_transitions(abstract_transitions,input_abstractions,cons,number_params,fullHypOutput)
    concrete_i_o_transitions=efsm.split_input_and_output(concrete_io_transitions)

    model=efsm.get_uppaal_model(start_state,concrete_i_o_transitions,interfaces,statevars,fullHypOutput,cons)

    f=open(model_file_out,'w')
    f.write( pp_xml.pformat(model.to_xml(),step="    ") )
    f.close()

def args_parsing(args):
    import os.path
    argparser = argparse.ArgumentParser(prog=os.path.basename(args[0]))

    argparser.add_argument('jsonfile', help='json file containing learned abstractions')
    argparser.add_argument('dotfile', help='graphviz dot file containing learned abstract model')

    argparser.add_argument('modelfile', help='output efsm in uppaal model file')
    argparser.add_argument('mode', help='mode of learning, eg stateVar mode')

    argparser.add_argument('output', help='type of output: concreteOutput or fullHypOutput ')



    parse_result=argparser.parse_args(args[1:])


    # handle input/output file argument
    json_filename_in=os.path.abspath(parse_result.jsonfile).replace('\\','/')
    dot_filename_in=os.path.abspath(parse_result.dotfile).replace('\\','/')
    model_file_out=os.path.abspath(parse_result.modelfile).replace('\\','/')
    # Note: on windows also interprets / as dir sep, and returns path with windows \
    mode=parse_result.mode
    output=parse_result.output

    util.check_isreadablefile(json_filename_in)
    util.check_isreadablefile(dot_filename_in)

    return (json_filename_in,dot_filename_in,model_file_out,mode,output)

def main(args):
    """
     convert output data of tomte's in java implemented learner to a concreet uppaal model

     learned data :
        json file :  specifies learned abstractions and input,output alphabet sut
        dot file :   specifies learned abstract model 

        output : concreteOutput or fullHypOutput
                     if concreteOutput:  hyp output:  only concrete output
                     if fullHypOutput :  hyp output: special output which contains info
                                  - abstract input
                                  - abstract output
                                  - concrete output
                                HACKED into single concrete output as:
                                O<absinp>__<absoutp>__concreteoutputname(p0,..)
                                                      -------------------------
                                                       `-> normal concrete output

                                note: '-' is serialized as 'XMINX' because
                                      in parsing uppaal interface declarations
                                      we don't allow '-' in method names!
    note: models generate with fullHypOutput==true are only used for learning
          and therefore don't need to declare the output interfaces in the model!


    """
    (json_filename_in,dot_filename_in,model_file_out,mode,output)=args_parsing(args)
    if mode=="stateVar":
        fullHypOutput=False
        if output=="fullHypOutput":
           fullHypOutput=True
        learnresult2uppaal(json_filename_in,dot_filename_in,model_file_out,fullHypOutput)
    else:
        print "error: not supported mode: ", mode
        sys.exit(1)




if __name__ == "__main__":
    main(sys.argv)
