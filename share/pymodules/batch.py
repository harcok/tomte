

from __future__ import division

import batchrun
import batchruncmd

import logging
logger = logging.getLogger(__name__)


from multiprocessing import Process, Queue, current_process
import pprint
import time
import os
import json
import copy
import datetime
import sys

# labels=[
#   'name',
#   '#succ',
#   '#learnRes',
#   '#learnInp',
#   '#testRes',
#   '#testInp',
#   '#anaRes',
#   '#anaInp', 
#   '#states',
# 
#   '#timeMs',
#   '#ce',
#   '#sufNum',
#   '#sufMaxPar',
#   '#sufMaxSym'
# ]  

labels=[
  'taskName',
  'succeeded',
  'numLearnResets',
  'numLearnInputs',
  'numTestResets',
  'numTestInputs',
  'numCeAnalysisResets',
  'numCeAnalysisInputs', 
  'finalNumStates',

  'realTime',
  'numCeEquivOracle',
  
  'sufNum',
  'sufMaxPar',
  'sufMaxSym',
]

label2index={
  'taskName': 1,
  'succeeded':26,
  'numLearnResets': 4,
  'numLearnInputs': 14,
  'numTestResets':12,
  'numTestInputs':16,
  'numCeAnalysisResets':10,
  'numCeAnalysisInputs':24, 
  'finalNumStates':20,

  'realTime':6,
  'numCeEquivOracle':2,
  
  'sufNum':18,
  'sufMaxPar':8,
  'sufMaxSym':22,
}  
chars=" ,"



field_spec={
       "taskName": "taskName",        

       "finalNumStates":"#states",

       "numMemQueriesLearning":"#absRes",
       "numMemInputsLearning":"#absInp",

       "numLearnResets":"#learnRes",
       "numLearnInputs":"#learnInp",
       "numTestResets":"#testRes",
       "numTestInputs":"#testInp",
       "numCeAnalysisResets":"#anaRes",
       "numCeAnalysisInputs":"#anaInp",

       "numCounterexamplesForLearner":"#absCE",
       "refinementCounter":"#guards",
       "numStoreUpdatesOnCe":"#GLT",
       "numStoreUpdatesOnInconsistency":"#OLT",

       "numSeeds":"#seeds",  
       "learningSuccessfull":"#sucF",
       "succeeded":"#succ",

       "numCeEquivOracle":"#ce",
       "numOfReusedCounterExamples":"#ceReuse",           
       "sizeCeEquivOracle":"#ceInp",           
       "sizeCeAfterLoopReduction":"#ceInpRedu",


       "numNodesInTree":"#nodes",
       "numEndNodesInTree":"#leafs",

       "timeLearning":"timeLearn",
       "timeTesting": "timeTest",
       "timeAnalysis":"timeAna",

       "timeRunning":"timeTotal",                 
       "realTime":"realTime",
           
       'sufNum':'sufNum',
       'sufMaxPar':'sufMaxPar',
       'sufMaxSym':'sufMaxSym',           
           
       # may contain unexisting fields in data    
       "xrealTime":"yrealTime",    
} 

    #result['numSeeds']=num
    #result["maxFinalNumStates"]=maxFinalNumStates
    #result["learningSuccessfull"]=int(round(result["learningSuccessfull"]*num))
    #result["succeeded"]=  str(result["learningSuccessfull"]) + '/' +  str(result["numSeeds"])
    
    
# used for average and standard deviation
row_spec__thesis_fides=[
       #numRuns        
       "succeeded",
       #           "numSeeds",  
       #           "learningSuccessfull",           
        
       "finalNumStates",
       "|" ,
       "numMemQueriesLearning",
       "numMemInputsLearning",
       "|" ,
       "numLearnResets",
       "numLearnInputs",
       "numTestResets",
       "numTestInputs",
       "numCeAnalysisResets",
       "numCeAnalysisInputs",
       "|" ,
       "numCounterexamplesForLearner",
       "refinementCounter",
       "numStoreUpdatesOnCe",
       "numStoreUpdatesOnInconsistency",
]

multiheader_spec__thesis_fides =[
   ['      ',"succ","$\mid$Q$\mid$","abs","abs","learn","learn","test","test","ana","ana","abs","abs","GLT","OLT"],
   ['      ',""    ,""             ,"res","inp","res"  ,"inp"  ,"res" ,"inp" ,"res","inp","CE" ,"ref",""   , ""  ],
]

multiheader_spec__tomte_vs_learnlib =[
   ['      ',"succ","learn","learn","test","test","ana","ana","ce" ,"ce" ,"ce"  ,"abs","abs","abs","abs","GLT","OLT"],
   ['      ',""    ,"res"  ,"inp"  ,"res" ,"inp" ,"res","inp","num","inp","inpR","res","inp","CE" ,"ref",""   , ""  ],
]
# used for average and standard deviation
row_spec__tomte_vs_learnlib=[

       "succeeded",
       "|" ,
       "numLearnResets",
       "numLearnInputs",
       "numTestResets",
       "numTestInputs",
       "numCeAnalysisResets",
       "numCeAnalysisInputs",
       "|" ,#ce  #ceInp  #ceInpRedu 
       "numCeEquivOracle", #:"#ce",
       "sizeCeEquivOracle", #:"#ceInp",           
       "sizeCeAfterLoopReduction", #:"#ceInpRedu",    
       "|" ,
       "numMemQueriesLearning",
       "numMemInputsLearning",
       "|" ,           
       "numCounterexamplesForLearner",
       "refinementCounter",
       "numStoreUpdatesOnCe",
       "numStoreUpdatesOnInconsistency",
] 

multiheader_spec__learnlib =[
   ['      ',"succ","learn","learn","test","test","ana","ana","ce" ,  "suf" ,"suf"  ,"suf"    ],
   ['      ',""    ,"res"  ,"inp"  ,"res" ,"inp" ,"res","inp","num",  "num","maxPar","maxSym" ],
]
    
# used for average and standard deviation
row_spec__learnlib=[

       "succeeded",
       "|" ,
       "numLearnResets",
       "numLearnInputs",
       "numTestResets",
       "numTestInputs",
       "numCeAnalysisResets",
       "numCeAnalysisInputs",
       "|" ,#ce  #ceInp  #ceInpRedu 
       "numCeEquivOracle", #:"#ce",
       "|",
       'sufNum',
       'sufMaxPar',
       'sufMaxSym',           
]     


# used for average and standard deviation
row_spec__text_table=[

       #numRuns        
       "succeeded",
       #           "numSeeds",  
       #           "learningSuccessfull",           
                   
       "finalNumStates",
       "|" ,
       "numMemQueriesLearning",
       "numMemInputsLearning",
       "|" ,
       "numLearnResets",
       "numLearnInputs",
       "numTestResets",
       "numTestInputs",
       "numCeAnalysisResets",
       "numCeAnalysisInputs",
       "|" ,
       "numCounterexamplesForLearner",
       "refinementCounter",
       "numStoreUpdatesOnCe",
       "numStoreUpdatesOnInconsistency",

       "|" ,

       "numCeEquivOracle",
       "numOfReusedCounterExamples",           
       "sizeCeEquivOracle",           
       "sizeCeAfterLoopReduction",
      

       "|" ,

       "numNodesInTree",
       "numEndNodesInTree",
       "|" ,
       "timeLearning",
       "timeTesting",
       "timeAnalysis",
       #"timeRunning",                 
       "|",
       "realTime",



]  




multiheader_spec__small =[
   ['      ',"learn","learn","test","test","ana","ana" ],
   ['      ',"res"  ,"inp"  ,"res" ,"inp" ,"res","inp" ],
]
    
# used for average and standard deviation
row_spec__small=[
       "numLearnResets",
       "numLearnInputs",
       "numTestResets",
       "numTestInputs",
       "numCeAnalysisResets",
       "numCeAnalysisInputs",           
]     





# used for average and standard deviation
row_spec__text_table=[

       #numRuns        
       "succeeded",
       #           "numSeeds",  
       #           "learningSuccessfull",           
                   
       "finalNumStates",
       "|" ,
       "numMemQueriesLearning",
       "numMemInputsLearning",
       "|" ,
       "numLearnResets",
       "numLearnInputs",
       "numTestResets",
       "numTestInputs",
       "numCeAnalysisResets",
       "numCeAnalysisInputs",
       "|" ,
       "numCounterexamplesForLearner",
       "refinementCounter",
       "numStoreUpdatesOnCe",
       "numStoreUpdatesOnInconsistency",

       "|" ,

       "numCeEquivOracle",
       "numOfReusedCounterExamples",           
       "sizeCeEquivOracle",           
       "sizeCeAfterLoopReduction",
      

       "|" ,

       "numNodesInTree",
       "numEndNodesInTree",
       "|" ,
       "timeLearning",
       "timeTesting",
       "timeAnalysis",
       #"timeRunning",                 
       "|",
       "realTime",



]  


# writeLatexTableWithSDEV(stream,tableData,field_spec,row_spec,multiheader_spec):
#      writeTableWithSDEV(stream,tableData,field_spec,row_spec,multiheader_spec)
def writeSummary(stream,field_spec,tasksData,writeSummaryFunc,row_spec,multiheader_spec,description,landscapeMode,scaleFactor):        

        #print "tasksData: ", pprint.pformat(tasksData), "\n"
        
        

        """
            getTaskSummaryData
                get data of a task averaged over a set of experiments
                including calculated standard deviations
                
                note: data from runs(runs during learning) not used, only final data of learn process 
        """        
        
        tasksSummaryData=getTasksSummaryData(tasksData,getTaskSummaryData)
        
        
        #print "tasksSummaryData: ", pprint.pformat(tasksSummaryData)

        """
         getSummaryTableData(tasksSummaryData):
        
             convert dict 
                 taskname -> taskdata
             to 
               list of taskdata  where taskdata['taskName'] is set to taskname
            
        """
        tableData=getSummaryTableData(tasksSummaryData)
        
        #print "tableData: ", pprint.pformat(tableData)
        writeSummaryFunc(stream,tableData,field_spec,row_spec,multiheader_spec,description,landscapeMode,scaleFactor)



      

 

def readJsonFile(jsonfile):
    if os.path.isfile(jsonfile):
        try:
           result=json.load( file( jsonfile ) )
        except Exception as e:
           logger.warning("corrupted json file '" + jsonfile + "' :" + str(e))
           result=None
    else:
        logger.warning("cannot find json file '" + jsonfile + "'")
        result=None
    return result


    
def formatData(data):
       for key in data:
          if "time" in key.lower():
             data[key]=datetime.timedelta(seconds=int( round( data[key] //1000  ) ) )

          # note: next is only needed when a query number is a float after averaging
          #       then we want to round it back to an integer!
          if "num" in key.lower():
             data[key]=int(round(data[key]))
          #       then we want to round it back to an integer!
          if "counter" in key.lower():
             data[key]=int(round(data[key]))

          if "size" in key.lower():
             data[key]=int(round(data[key]))



def format_listofmapdata(list_of_mapdata):
    result=[]
    for mapdata in list_of_mapdata:
         result.append(format_mapdata(mapdata))
    return result

def format_mapdata(mapdata):
    result={}    
    for field,value in  mapdata.items():
        result[field]=format_fieldvalue(field,value)
    return result

def format_fieldvalue(field,value):
    if "time" in field.lower():
       return str(value)
    else:
       return str(value)





  
    
def writeLatexTableWithSDEV(stream,tableData,field_spec,row_spec,multiheader_spec,description,landscapeMode,scaleFactor):
  
    
    #print "tableData: ", pprint.pformat(tableData)
    defined_fields=tableData[0].viewkeys()
    #print "defined_fields: ", pprint.pformat(defined_fields) 

    align_spec=["|","l","|"]
    avg_row_spec=[]
    sdev_row_spec=[]
    for item in row_spec:
        if item in  defined_fields:
             avg_row_spec.append('&')
             sdev_row_spec.append('&')
                         
             avg_row_spec.append(item)
             sdev_row_spec.append( "SDEV_" + item )
             
             align_spec.append("r")
        else:     
             #sdev_row_spec.append(  item )             
             align_spec.append(item)
    align_spec.append("|")         
             
    avg_row_spec.append(r'\\')
    sdev_row_spec.append(r'\\')             
             
    #print "sdev_row_spec: ", pprint.pformat(sdev_row_spec) 


    
    
    
    # missing SDEV fields in field_spec get "" as label
    for item in field_spec.keys():
        field_spec["SDEV_" + item]=""

 
    # header spec : only first row of multirow spec
    #multiheader_spec=[[ '      ' ] + avg_row_spec ]


#     multiheader_spec =[
#        ['      ',"succ","$\mid$Q$\mid$","abs","abs","learn","learn","test","test","ana","ana","abs","abs","GLT","OLT"],
#        ['      ',""    ,""             ,"res","inp","res"  ,"inp"  ,"res" ,"inp" ,"res","inp","CE" ,"ref",""   , ""  ],
#     ]

#     multiheader_spec =[
#        ['      ',"succ","learn","learn","test","test","ana","ana","abs","ce" ,"ce" ,"ce"  ,"abs","abs","abs","GLT","OLT"],
#        ['      ',""    ,"res"  ,"inp"  ,"res" ,"inp" ,"res","inp","res","num","inp","inpR","inp","CE" ,"ref",""   , ""  ],
#     ]
    #ce  #ceInp  #ceInpRedu 
    def insertBetweenElements(list,elem):
         r=[]
         for item in list:
             #r.append("{" + item +":>10}")
             #r.append("{0:>15}".format(item))
             
             # TODO: size should be determined from matching data row!!
             r.append("{0:>10}".format(item))
             r.append(elem)
         return r[:-1]
    
    new_multiheader_spec=[]  
    for row in multiheader_spec:
          newrow=insertBetweenElements(row,"&")
          newrow.append(r'\\')
          new_multiheader_spec.append(newrow)
    multiheader_spec=new_multiheader_spec          

    #multiheader_spec=[[ '      ' ] + head_row_spec ]
    
    #multirow_spec=[ avg_row_spec, +  sdev_row_spec ]
    
    # width field = 15
    multirow_spec=[   [ '   avg         ' ] + avg_row_spec,
                      [ '   stddev      ' ] + sdev_row_spec ]
    
    
    
   
    if multiheader_spec == None:
        multiheader_spec=multirow_spec
     

    
    # filter fields from multirow_spec
    usable_fields=field_spec.keys()    
    row_fields=determineFieldsInMultiRowSpec(usable_fields,multirow_spec)    
    fields=['taskName'] # not used in spec, but added manually in format_multirow
    for rowfields in row_fields:
        fields.extend(rowfields)
        
  
  
  
  
    # filter data for fields specified
    tableData=filterData(fields,tableData)  
    #print "tableData: ", pprint.pformat(tableData);  
     
    # format data to formatted strings ready to be printed 
    tableData=format_listofmapdata(tableData)
    #print "tableData: ", pprint.pformat(tableData);  
       
    # get maximum width for a field by looking at width header label and its data     
    field2width=getField2width(field_spec,tableData)
    #print "field2width: ", pprint.pformat(field2width)
    
    #print "multirow_spec: ", pprint.pformat(multirow_spec)
    
    
#     for key,value in field2width.items():
#         #print key, value
#         field2width[key]=15
    #sys.exit(0)     
    
    determineMaximumBetweenRelatedFields(field2width,multirow_spec)
    #print "field2width: ", pprint.pformat(field2width)
    
    
    multirow_formatstr= getMultiRow_formatstr(multirow_spec,field2width)
    multiheader_formatstr= getMultiRow_formatstr(multiheader_spec,field2width)

        
    #print "multirow_formatstr: ", pprint.pformat(multirow_formatstr) 
    #print "multiheader_formatstr: ", pprint.pformat(multiheader_formatstr)   
     
    # TODO: 
    #   should do class inheritance to overwrite header and multirow formatting
    #   and overwrite writeTablewithSDEV    
    format_latex_table(stream,field_spec,multiheader_formatstr,tableData,multirow_formatstr,
                 format_latex_header,format_latex_multirow,align_spec,description,landscapeMode,scaleFactor) 
        


def writeTableWithSDEV(stream,tableData,field_spec,avg_row_spec,multiheader_spec,description,landscapeMode,scaleFactor):

    
    '''
    multiheader_spec : not used -> just for compatibility with writeLatexTableWithSDEV

    note: 
      - not all fields in table in header
      - tableDataSet can contain more fields than displayed!
         `-> need set of fields which specify which fields to include  
                `-> fields_spec!
      - label of field in header used to be determine maxwidth column
         => thus needed to easily find out label of some column for some field
            thus needed to find label from field directly  
               => also specify in fields_spec
    
     field_spec specifies both:
       - for which fields columns shown
       - label for that field use in header (only needed if field used in multiheader_spec)
       note: field_spec may contain fields  not  in data
             data is only filtered for fields in data which are in field_spec
             however a field in row_spec must be defined in data
             and fields used in  header_spec must be a subset of fields used row_spec  
    '''     
    
    #print "tableData: ", pprint.pformat(tableData)
    defined_fields=tableData[0].viewkeys()
    #print "defined_fields: ", pprint.pformat(defined_fields) 

    sdev_row_spec=[]
    for item in avg_row_spec:
        if item in  defined_fields:
             sdev_row_spec.append( "SDEV_" + item )
        else:     
             sdev_row_spec.append(  item )

    # missing SDEV fields in field_spec get "" as label
    for item in field_spec.keys():
        field_spec["SDEV_" + item]=""

    


 
    # header spec : only first row of multirow spec
    multiheader_spec=[[ '      ' ] + avg_row_spec ]
    #multirow_spec=[ avg_row_spec, +  sdev_row_spec ]
    multirow_spec=[ [ 'avg   ' ] + avg_row_spec,[  'stddev' ] +  sdev_row_spec ]
    if multiheader_spec == None:
        multiheader_spec=multirow_spec
    

    # filter fields from multirow_spec
    usable_fields=field_spec.viewkeys()    
    row_fields=determineFieldsInMultiRowSpec(usable_fields,multirow_spec)    
    fields=['taskName'] # not used in spec, but added manually in format_multirow
    for rowfields in row_fields:
        fields.extend(rowfields)
        
 
    # filter data for fields specified
    tableData=filterData(fields,tableData)  
    #print "tableData: ", pprint.pformat(tableData);  
     
    # format data to formatted strings ready to be printed 
    tableData=format_listofmapdata(tableData)
    #print "tableData: ", pprint.pformat(tableData);  
       
    # get maximum width for a field by looking at width header label and its data     
    field2width=getField2width(field_spec,tableData)
    #print "field2width: ", pprint.pformat(field2width)
       
    
    #print "multirow_spec: ", pprint.pformat(multirow_spec)
    
    determineMaximumBetweenRelatedFields(field2width,multirow_spec)
    #print "field2width: ", pprint.pformat(field2width)
    
    
    multirow_formatstr= getMultiRow_formatstr(multirow_spec,field2width)
    multiheader_formatstr= getMultiRow_formatstr(multiheader_spec,field2width)

        
    #print "multirow_formatstr: ", pprint.pformat(multirow_formatstr)   
    #print "multiheader_formatstr: ", pprint.pformat(multiheader_formatstr)   
        
    format_table(stream,field_spec,multiheader_formatstr,tableData,multirow_formatstr) 
    
def getMultiRow_formatstr(multirow_spec,field2width):
    multirow_formatstr=[]
    for row_spec in  multirow_spec:
        formatstr=getFormatStr(row_spec,field2width)
        multirow_formatstr.append(formatstr)
    return multirow_formatstr

def getFormatStr(row_spec,field2width):
    formats=[]
    defined_fields=field2width.viewkeys()
    for item in row_spec:
        if item in defined_fields:
            formatitem="{"+item+":>" + str(field2width[item]) + "}"
        else:
            formatitem=item
        formats.append(formatitem)
    return "  " + "  ".join(formats)
    
def get_max_line_length(field_spec,multiheader_formatstr,tableData,multirow_formatstr):    

    
    import StringIO
    output = StringIO.StringIO()
    format_header(output,field_spec,multiheader_formatstr)
    format_multirow(output,tableData[-1],multirow_formatstr)
    stringlist=output.getvalue().splitlines()
    output.close()
    maxlength = max(len(s) for s in stringlist) 
    
    return maxlength
 
def format_latex_table(stream,field_spec,multiheader_formatstr,tableData,multirow_formatstr,
                       format_header,format_multirow,align_spec,description,landscapeMode,scaleFactor):    

    # filter should work better
    #print tableData[0].viewkeys() 
    numfields=len(tableData[0].viewkeys())
    # taskName is printed in first line, 
    # average fields on second line
    # stdev fields on second line
    # for both second and third line a column is added for "avg" and "stddev" keywords       
    numfields=(numfields-1)//2 + 1
                        

    
    
    #numfields=21
    #maxlength = get_max_line_length(field_spec,multiheader_formatstr,tableData,multirow_formatstr) 
    
    #line='-'*maxlength   + "\n"
    #doubleline='='*maxlength  + "\n"      #'~'*124 +  '\n' + '~'*124
    #hashline='='*maxlength  + "\n"
    line="\\hline\n"
        
    latexRowFormat= "".join(align_spec)
    #" | " + " | ".join( ["l","r"] + ["c"]*(numCols-2) ) + " | "
    if  not description: description="DESCRIPTION"
    
    latexTable=""
    if landscapeMode:
        startlatexTable=r"\begin{sidewaystable}[ph!]"
        endLatexTable=r"\end{sidewaystable}"
    else:
        startlatexTable=r"\begin{table}[t]"
        endLatexTable=r"\end{table}"
            
    latexCaption=r"\caption{" + description + "}"
    latexLabel=r"%\label{tab:REFLABEL}"       
    #scaleFactor="0.8" 
    
    scaleBox=""
    if not scaleFactor:
        scaleBox=r"\scalebox{1}{"
    else:
        scaleBox=r"\scalebox{"+scaleFactor+"}{" 
    #latexTableHeader="\n".join( [ r"\begin{table}[t]",r"\begin{center}",r"{",r"\footnotesize",r"\begin{tabular}{"+latexRowFormat+"}"] )
    latexTableHeader="\n".join( [startlatexTable ,r"\begin{center}",scaleBox,r"\footnotesize",r"\begin{tabular}{"+latexRowFormat+"}"] )
    latexTableFooter="\n".join( [ r"\end{tabular}", r"}",latexCaption,latexLabel, r"\end{center}", endLatexTable ] )
    
        
    #stream.write(""+' '.join(align_spec)
    stream.write(latexTableHeader)
    stream.write( "\n" )  
    stream.write( line )  
    format_header(stream,field_spec,multiheader_formatstr)
    stream.write( line ) 
    for tableDataSet in tableData[:-1]:
        format_multirow(stream,tableDataSet,multirow_formatstr,numfields)
        stream.write( line )
    # close last entry with double line    
    format_multirow(stream,tableData[-1],multirow_formatstr,numfields)
    stream.write( line ) 
    stream.write(latexTableFooter)     
        
        
def format_latex_header(stream,field_spec,multiheader_formatstr):  
    for formatstr in   multiheader_formatstr:
        stream.write( formatstr.format(**field_spec) + '\n')
        
        
  
    
def format_latex_multirow(stream,tableDataSet,multirow_formatstr,numfields):  
    taskName=tableDataSet["taskName"]
    #stream.write("{0}\n".format(taskName) )
    
    stream.write( r"\multicolumn{" + str(numfields) + r"}{|l|}{" + taskName + r"} \\")
    stream.write("\n")
    stream.write("\\hline\n")
    for formatstr in   multirow_formatstr:
        stream.write( formatstr.format(**tableDataSet) + '\n')
            
        
def format_table(stream,field_spec,multiheader_formatstr,tableData,multirow_formatstr):    


    maxlength = get_max_line_length(field_spec,multiheader_formatstr,tableData,multirow_formatstr) 
    
    line='-'*maxlength   + "\n"
    doubleline='='*maxlength  + "\n"      #'~'*124 +  '\n' + '~'*124
    hashline='='*maxlength  + "\n"
        
    stream.write( doubleline )  
    format_header(stream,field_spec,multiheader_formatstr)
    stream.write( doubleline ) 
    for tableDataSet in tableData[:-1]:
        format_multirow(stream,tableDataSet,multirow_formatstr)
        stream.write( line )
    # close last entry with double line    
    format_multirow(stream,tableData[-1],multirow_formatstr)
    stream.write( doubleline )        
            
         
def format_header(stream,field_spec,multiheader_formatstr):  
    for formatstr in   multiheader_formatstr:
        stream.write( formatstr.format(**field_spec) + '\n')
        
        
  
    
def format_multirow(stream,tableDataSet,multirow_formatstr):  
    taskName=tableDataSet["taskName"]
    stream.write("{0}\n".format(taskName) )
    for formatstr in   multirow_formatstr:
        stream.write( formatstr.format(**tableDataSet) + '\n')
    


def getFieldSpecFromMultirowSpec(defined_fields,multirow_spec,field_spec,copyfieldname):
    '''
      input data defines which fields are usable :   defined fields !
      
      multiRow spec specifies used fields :  used fields 

      autogenerate field_spec    => no field_spec given
          thus from multiRow spec we can derive a field_spec
          by taken the labels the same as the fieldnames  
      autoextend field_spec      => field_spec given with optional copyfieldname boolean
          thus from multiRow spec we can extend a field_spec
          by taken the labels for the to be added missing fields:
           - the same as the fieldnames          => copyfieldname True
           - or as ""                            => copyfieldname False 
           
      thus a given field_spec is leading but if not given
      it is derived/extended from multiRow spec      
      
      
      note: multiHeader spec may only use a subset of fields 
            used in multiRow spec      => verify this condition!        

      note: when no multiHeader spec is given, then by default
            the multiRow spec is used!!
            
                        

    '''
    # filter fields from multirow_spec
    multirow_fields=[]
    for multirow in multirow_spec:
        fields=[]
        for item in multirow:
            if item in defined_fields:
                fields.append(item)
        multirow_fields.append(fields)





def filterData(fields,tableData):
    result=[]
    for data in tableData:
        newData=dict((k, v) for k,v in data.iteritems() if k in fields)
        result.append(newData)
    return result
    
    
    
def getField2width(headerData,tableData):
    field2width={}
    fields=headerData.viewkeys()
    for field in fields:
        field2width[field]=len(headerData[field])
        
    for data in tableData:
        for field,value in data.items():
             max=field2width[field]            
             if len(value) > max:
                 field2width[field]=len(value)
        
    return field2width

def determineFieldsInMultiRowSpec(usable_fields,multirow_spec):
    multirow_fields=[]
    for row_spec in multirow_spec:
        fields=determineFieldsInRowSpec(usable_fields,row_spec)
        multirow_fields.append(fields)
    return multirow_fields  

def determineFieldsInRowSpec(usable_fields,row_spec):
    fields=[]
    for item in row_spec:
        if item in usable_fields:
            fields.append(item) 
    return fields     
      
def determineMaximumBetweenRelatedFields(field2width,multirow_spec):
    
   
    
    # fields usable in row spec
    usable_fields=field2width.keys()
    
    
    # filter fields from multirow_spec
    multirow_fields=determineFieldsInMultiRowSpec(usable_fields,multirow_spec)


    # number of fields in row
    number_of_fields_in_row=len(multirow_fields[0])


    # walk over fields at same index in related rows and find max width
    for fieldindex in range(0,number_of_fields_in_row):
        maxwidth=0
        related_fields=[]
        for fields in multirow_fields:
            field=fields[fieldindex]
            related_fields.append(field)
            fieldwidth=field2width[field]
            if fieldwidth > maxwidth:  maxwidth=fieldwidth
        for field in related_fields:
            field2width[field]=maxwidth

# fetch data for table
def getTaskDataRALIB(result,taskResultDir):
    """
      get all data of a single execution of a task from result directory of task 
    """

    
    
    taskname=result['task']['name']
    #pprint.pprint(result)
    #pprint.pprint(data)
    
    data=result['result']
   
    

    stdoutfile=taskResultDir+'/stdout.txt'
    try: 
        temp = open(stdoutfile)
        temp.seek(-1000, 2)
        output = temp.read()
        temp.close()
    except IOError:
        return result
        pass

    try: 
        (numStates,learnResets, learnInputs, testResets, testInputs, success)=getNumbersRalib(output)
        
        data["finalNumStates"]=numStates;
        data["numLearnResets"]=learnResets;
        data["numLearnInputs"]=learnInputs;
        data["numTestResets"]=testResets;
        data["numTestInputs"]=testInputs;
        data["learningSuccessfull"]=success;
    except Exception:
        pass
   
    # HACK:
    import os.path
    if  "fifo" in taskname:
        data["learningSuccessfull"]=os.path.isfile(taskResultDir + "/model.xml")     
    
    if data["learningSuccessfull"] == True:
        data["learningSuccessfull"] = 1
    else:
        data["learningSuccessfull"] = 0
    

    data["realTime"]=result['time']*1000 # use time from batch
    #print data["realTime"]

           
    runs=[]  # not used for summary       
    return (data,runs)

def getTaskData(result,taskResultDir):
    """
      get all data of a single execution of a task from result directory of task 
    """
    statisticsFile = taskResultDir + "/statistics.json"

    #logger.info("statisticsFile: "+ statisticsFile)
    statistics=readJsonFile(statisticsFile)
    
    if statistics == None:
        return None
    
    runs=statistics["runs"]
    
    #stat=statistics["summary"]
    del statistics["runs"]
    stat=statistics
    
    data={}
    #data["totaltime"]=int(round(result['end_time']-result['start_time']))
    data.update(result)

    del data["task"]
    data.update(result["task"])
    del data["cmd"]
    data.update(stat)


    # learningSuccessfull
    #   "true"  :   equal found by cadp compare
    #   "false"  :    unequal found by cadp compare
    #   "unknown"  no cadp compare done
    #
    # => always exclude last tests because they have no effect for last hyp
   
    # make from learningSuccessfull an int value 0 or 1 so that we can count number of success
    # note: not using cadp is given value 0 because we cannot say anything about success


    #HACK: if no  equivalenceCheck is used, then learningSuccessfull is not set
    #      then  assume learning is successfull to still get values in tables!!
    if not data.has_key('learningSuccessfull'):
        data["learningSuccessfull"] = True 



    if data["learningSuccessfull"] == True:
        data["learningSuccessfull"] = 1
    else:
        data["learningSuccessfull"] = 0


#     if data["learningSuccessfull"] == "no":
#         data["runningTime"]=data["runningTimeWithLastTestRun"]
#         data["timeTesting"]=data["timeTestingWithLastRun"]
#         data["equivQueries"]=data["equivQueriesWithLastRun"] 
#                
    return (data,runs)


def getTaskSummaryData(taskData):
    """
        get data of a task averaged over a set of experiments
        including calculated standard deviations
        
        taskData: list of multiple results for  a specific task (e.g. different runs for different seeds)
        
        note: data from runs(runs during learning) not used, only final data of learn process 
    """
    #fields=["finalNumStates","totalRefinementCounter" ,"totalRunningTime","learningSuccesful","totalTimeLearning","totalMemQueriesLearning","totalTimeTesting","totalEquivQueries","totalMemQueriesTesting"]

    
    fields=["numRuns",
           "finalNumStates",
           "numMemQueriesLearning",
           "numMemInputsLearning",
           "numLearnResets",
           "numLearnInputs",
           "numTestResets",
           "numTestInputs",
           "numCeAnalysisResets",
           "numCeAnalysisInputs",
           "numStoreUpdatesOnInconsistency",
           "numCounterexamplesForLearner",
           "refinementCounter",
           "numStoreUpdatesOnCe",
           "learningSuccessfull",

           "numCeEquivOracle",
           "numOfReusedCounterExamples",           
           "sizeCeEquivOracle",           
           "sizeCeAfterLoopReduction",
          
         
           
           
           "numNodesInTree",
           "numEndNodesInTree",
           "realTime",
           "timeLearning",
           "timeTesting",
           "timeAnalysis",
           "timeRunning"]    

    
    
    # initialize some data structures for calculations  
    num=len(taskData)
    if not num: return None     
    average={}
    meansqrs={}
    stddev={}
    sum={}
    for field in fields:
        sum[field]=0
        meansqrs[field]=0




    # 1. calculate average
    # sum data from different seed runs
    numSuc=0 # num succeeded
    for data in taskData:
        finaldata,runs = data
        if finaldata["learningSuccessfull"]: # do not add data of failed experiments
            numSuc=numSuc+1
            for field in fields:
               sum[field]+=finaldata[field]
    #  then divide by number runs => average  
    for field in fields:
        average[field]=0
        if numSuc: 
            average[field]=sum[field]/numSuc

    # 2. calculate stddev
    # sum meansqr data  from different seed runs   
    for data in taskData:
        finaldata,runs = data
        if finaldata["learningSuccessfull"]: # do not add data of failed experiments
           for field in fields:
               meansqrs[field]+=(finaldata[field]-average[field])**2
               
    #  then divide by number runs => meansqr
    for field in fields:
        stddev[ "SDEV_" + field ]=0
        if numSuc: stddev[ "SDEV_" + field ]=(meansqrs[field]/numSuc)**0.5

    # 3. find maximum value 
    #   - for numStates found
    maxFinalNumStates=0
    for data in taskData:
         finaldata,runs = data
         if finaldata["finalNumStates"] > maxFinalNumStates:
             maxFinalNumStates = finaldata["finalNumStates"]


    #pprint.pprint(average)
    result=average
    result.update(stddev)

    formatData(result)
    #pprint.pprint(result)

#    keys=[
#           "numCeEquivOracle",
#           "numOfReusedCounterExamples",           
#           "sizeCeEquivOracle",           
#           "sizeCeAfterLoopReduction" ]
#    for key in keys:
#        result[key]="{0:.1}".format(result[key])
#        skey="SDEV_"+key
#        result[skey]="{0:.1}".format(result[skey])


    result['numSeeds']=num
    result["maxFinalNumStates"]=maxFinalNumStates
    result["learningSuccessfull"]=numSuc #int(round(result["learningSuccessfull"]*num))
    result["succeeded"]=  str(result["learningSuccessfull"]) + '/' +  str(result["numSeeds"])
    
    result['SDEV_numSeeds']=''
    result['SDEV_maxFinalNumStates']=''
    result['SDEV_learningSuccessfull']=''
    result['SDEV_succeeded']=''
    
    return result







def getTasksData(tasksResults,getTaskResultDir,resultsRootDir,getTaskDataFunc):
    """
     tasksData is OrderedDict sorted by taskName
      -> data grouped per taskname : per taskname(dir) multiple datasets: one dataset per seed!
    """
    from collections import OrderedDict
    tasksData=OrderedDict()

    # tasksResults is OrderedDict sorted by taskName
    for taskName,results in tasksResults.items():
        taskData=[]
        for result in results:
            taskResultDir=getTaskResultDir(result["task"],resultsRootDir)
            #print taskResultDir   
            #print result["task"]
            data=getTaskDataFunc(result,taskResultDir)
            if not data:
               print "nodata"
               continue
            else:
               taskData.append(data)
        if taskData:
           tasksData[taskName]=taskData
    return tasksData # Odict: key => value( list of datasets )


def getTasksSummaryData(tasksData,getTaskSummaryDataFunc):
    """
     tasksSummaryData is OrderedDict sorted by taskName
      -> data  per taskname : per taskname(dir)  -> one summarydata!
    """

    from collections import OrderedDict
    tasksSummaryData=OrderedDict()

    # tasksData is OrderedDict sorted by taskName
    for taskName,taskData in tasksData.items():
          data=getTaskSummaryDataFunc(taskData)
          if not data:
             continue
          else:
             taskName=taskName.replace("model_","").replace("_","") 
             tasksSummaryData[taskName]=data
    return tasksSummaryData # Odict: key => value summary data 



def getSummaryTableData(tasksSummaryData):
    """
     convert dict 
         taskname -> taskdata
     to 
       list of taskdata  where taskdata['taskName'] is set to taskname
        
    """
    # get table Data
    tableData=[]
    for taskName,taskData in tasksSummaryData.items():        
        tableDataSet=taskData
        tableDataSet["taskName"] = taskName
        tableData.append(tableDataSet)         

    return tableData











def JtorxTaskCmd_logall(task,pid):
    jtorxCommand='jtorxcmd'
    jtorxioCommand='jtorxio'

    logfile=task['resultDir'] + '/result.log'
    configfile= task['taskDir'] + '/config.jtorx'
    seed=str(task['seed'])
    command=[ jtorxCommand, '-s',seed, '-l',  logfile , configfile]

    task['cmd']=command
    task['cmdStr']=' '.join(command)
    result=batchruncmd.run_command(command,cwd=task['taskDir'])

    command=[ jtorxioCommand, '-trace',logfile]

    #traceresult=batchruncmd.run_command(command)
    #pprint.pprint(traceresult)

    trace=batchruncmd.run_command(command)['stdout']
    result['trace']=trace
    result['numIOSymbols']=len(trace.split('\n')) -1 # don't count last newline
    print  task['taskDir'], result['numIOSymbols']
    #print  result['trace']

    return result


#  use logfile for trace, but do not log stdio (send to /dev/null)
def JtorxTaskCmd(task,pid):
    jtorxCommand='jtorxcmd'
    jtorxioCommand='jtorxio'

    logfile=task['resultDir'] + '/result.log'
    configfile= task['taskDir'] + '/config.jtorx'
    seed=str(task['seed'])
    command=[ jtorxCommand, '-s',seed, '-l',  logfile , configfile]

    task['cmd']=command
    task['cmdStr']=' '.join(command)
    result=batchruncmd.run_command_devnull(command,cwd=task['taskDir'])

    command=[ jtorxioCommand, '-trace',logfile]

    #traceresult=batchruncmd.run_command(command)
    #pprint.pprint(traceresult)

    trace=batchruncmd.run_command(command)['stdout']
    result['trace']=trace
    result['numIOSymbols']=len(trace.split('\n')) -1 # don't count last newline
    print  task['taskDir'], result['numIOSymbols']
    #print  result['trace']

    return result



import pprint
def TomteTaskCmd(task,pid):
    logger.debug(" TomteTaskCmd" + pprint.pformat(task))
    
    command=[ 'tomte_learn','-e']
    if task.has_key('maxMemory'):
        command.extend( ['--max-memory',str(task['maxMemory'])] )

    command.extend( ['--output-dir', task['resultDir'], '--seed',str(task['seed']),  task['taskDir'] + '/config.yaml' ] )
 
    configOptions=task.get('configOptions',[])
    for configOption in configOptions:
        command.append("--config-option") 
        command.append(configOption)    
    
    task['cmd']=command
    task['cmdStr']=' '.join(command)
    logger.info("pid:" + str(pid) + " cmd:" + ' '.join(command))
    
    #exit(1)
    result=batchruncmd.run_command(command)

#     time.sleep(3)
#     result="yes"

#     stdoutfile=task['resultDir'] + "/stdout.txt"
#     stderrfile=task['resultDir'] + "/stderr.txt"
#     print stdoutfile
#     result=batchruncmd.run_command_filed(command,stdoutfile,stderrfile)

    return result


emptyTaskResult = {
           "numRuns": 0,
           "finalNumStates": 0,
           
           "numMemQueriesLearning":0,
           "numMemInputsLearning": 0,
           
           "numLearnResets":0,
           "numLearnInputs":0,
           "numTestResets":0,
           "numTestInputs":0,
           "numCeAnalysisResets":0,
           "numCeAnalysisInputs":0,
           
           "numStoreUpdatesOnInconsistency":0,  
           "numCounterexamplesForLearner":0,
           "refinementCounter":0,               
           "numStoreUpdatesOnCe":0, 
           
           "learningSuccessfull": False,

           "numCeEquivOracle":0,
           "numOfReusedCounterExamples":0,           
           "sizeCeEquivOracle":0,       
           "sizeCeAfterLoopReduction":0,
          
           "numNodesInTree":0,
           "numEndNodesInTree":0,
           "realTime":0,
           "timeLearning":0,
           "timeTesting":0,
           "timeAnalysis":0,
           "timeRunning":0                  
}

import re

def getStatsRa(statRegex, output):
    statLine = re.search(statRegex, output).group(0)
    statsNum = re.search('[0-9]+', statLine).group(0)
    return statsNum

def getSuccesRa( output):
    return "EQ-TEST did not find counterexample!" in output
    #statLine = re.search(r"EQ-TEST did not find counterexample!", output)
    #return statLine is not None

def getNumbersRalib(output):
    learnResets = getStatsRa(".*Resets\sLearning:.*",output)
    learnInputs = getStatsRa(".*Inputs\sLearning:.*",output)
    testResets = getStatsRa(".*Resets\sTesting:.*",output)
    testInputs = getStatsRa(".*Inputs\sTesting:.*",output)
    success = getSuccesRa(output)
    #print getStatsRa(".*Locations:.*",output)
    numStates=getStatsRa("Input.*Locations:.*",output)
    results= (int(numStates), int(learnResets), int(learnInputs),
            int(testResets), int(testInputs) , success)
    return  results

def RalibTaskCmd(task,pid):
    # write results in statistics.json

    
    configfile= task['taskDir'] + '/configio.txt'
    modelfile= task['taskDir'] + '/model.register.xml'
    seed=str(task['seed'])
    seedarg='random.seed=' + seed
    modelarg='target=' + modelfile
    
    # problem with command below: model file stored in cwd and overwritten each time 
    #command=[ 'java', '-ea', '-jar', 'ralib-0.1-SNAPSHOT-jar-with-dependencies.jar', 
    #          'iosimulator', '-f', configfile, seedarg +";" + modelarg ]
    
    # in configfile specify model
    # note: ralib command is shell script wrapper so that ralib is run in
    #       resultDir so that it stores the resulting model there, because ralib
    #       stores model in current working directory 
    command=[ 'ralib', configfile, task['resultDir'] , seedarg  ]
 
    # http://stackoverflow.com/questions/1191374/using-module-subprocess-with-timeout
    # #  => only in python 3 subprocess has timeout!!
    # # http://www.ostricher.com/2015/01/python-subprocess-with-timeout/
    # #  => easiest : use gnu timeout command before command to implement timeout
    #
    #timeout = 60 # seconds
    #command=["timeout", str(timeout)]+command
    # --> instead implemented timeout in ralib wrapper script which fetches
    #     timeout from config file + 20 seconds (to let ralib itself handle timeout) 
   
    task['cmd']=command
    task['cmdStr']=' '.join(command)
    logger.info("pid:" + str(pid) + " cmd:" + ' '.join(command))
    
    #result=batchruncmd.run_command(command)
    
    #batchruncmd.run_command(command, out=open("tempOut","w"))
    
    stdoutfile=task['resultDir']+'/stdout.txt'
    stderrfile=task['resultDir']+'/stderr.txt'
    
    result = copy.deepcopy(emptyTaskResult) 

    tuple_exception_returncode=batchruncmd.run_command_filed(command, stdoutfile, stderrfile)
    result["statusCommand"]=tuple_exception_returncode

#    #stdoutfile=task['resultDir']+'/stdout.txt'
#    try: 
#        temp = open(stdoutfile)
#        temp.seek(-1000, 2)
#        output = temp.read()
#        temp.close()
#    except IOError:
#        return result
#        pass
#
#    try: 
#        (numStates,learnResets, learnInputs, testResets, testInputs, success)=getNumbersRalib(output)
#        
#        result["finalNumStates"]=numStates;
#        result["numLearnResets"]=learnResets;
#        result["numLearnInputs"]=learnInputs;
#        result["numTestResets"]=testResets;
#        result["numTestInputs"]=testInputs;
#        result["learningSuccessfull"]=success;
#    except Exception:
#        pass
    
    
    #pprint.pprint(result)

    return result

 
    

# stops whole batch on first task who does not learns successful (within maxtime)
class DirectStopBatch(batchrun.VerboseSubdirsTimedSeededBatch):
    def onResult(self,data):
        taskResultDir=self.getTaskResultDir(data["task"],self.resultsRootDir)
        statisticsFile = taskResultDir + "/statistics.json"
        try:
          statistics=json.load(file(statisticsFile))
        except:
          return data
        stat=statistics["summary"]

        if not stat["learningSuccesful"]:
            print "task found which did run till timout without finding right model"
            print "stop whole batch"
            self.terminateWorkers()
            import sys
            logger.info("exit") # to store time of exit
            sys.exit(0)

        return data

def getLearnlibData(filename,label2index):
    """
       
    """
    import csv 
    # tableData=
    #f=open('report-10-1000-100-0.2.csv')
    f=open(filename)
    spamreader = csv.reader(f, delimiter=',', quotechar='"')
    count=0
    results=[]
    rowsize=0
    sdevprefix="SDEV_"
    labels=label2index.keys()
    
    # the following reads data from cvs file from Falk (learnlib)
    # - only odd rows contain data
    # - first row contains header, but these are only used to determine size,
    #   where the rowsize is only used to verify if you read in a valid data line
    # - instead of header labels, the given label2index specifies a label for 
    #   each value in the row 
    for row in spamreader:
        if count == 0:
            # don't remove last label name if empty (or only whitespace)
            # because all rows have this problem!
            rowsize=len(row)
            #print    len(row), rowsize
            #print "continue count == 0"
            count=count+1
            continue

        if len(row) != rowsize:
           #print "continue len(row) != rowsize"
           print "detected a smaller data line in cvs -> skip line:" + str(count+1)
           count=count+1
           continue

        if count%2 == 1:
            result={}
            #print row
            for label in labels:
                sdevlabel=sdevprefix+label
                if label == "taskName":
                    s=row[label2index[label]]
                    # remove unwanted labels
                    s = s.replace('.register.xml','').replace('/model','')
                    # strip any prefixed paths and surrounding whitespace
                    result[label]=s[s.rfind('/')+1:].strip()
                    #result[label+"_stdev"]=""
                    result[sdevlabel]=""
                else:
                    
                    result[label]=row[label2index[label]]
                    result[sdevlabel]=row[label2index[label]+1]
                    #print  result[sdevlabel]
                    # convert string to float 
                    result[label]=float(result[label])
                    result[sdevlabel]=float(result[sdevlabel])
                    
                    
                    format2digit="{0:>10.2f}"
                    format1digit="{0:>10.1f}"
                    
                    # does rounding good! 1.49 -> 1  and 1.50 to 2
                    formatint="{0:>10.0f}"
                    
                    formatused=formatint
                    result[label]=formatused.format(result[label])
                    result[sdevlabel]=formatused.format(result[sdevlabel])
                    
                    # round float strings
                    #result[label]=int(round(float(result[label])))
                    #result[label+"_stdev"]=int(round(float(result[label+"_stdev"])))

            results.append(result)
            #print ', '.join(row)
        count=count+1
    return results


def main(args):
    smallTable=False
    if len(args) < 2:
        print "forgot command as argument"
        print "usage: tomte_batch command tasksRootDir"
        sys.exit(1)
    command=args[1]
    if  command not in ['textsummary','latexsummary','learn','latexsummaryLL','learn_ralib','textsummary_ralib']:
        print "not valid command: " +  command
        print "command must be one of: learn,textsummary,latexsummary,latexsummaryLL,learn_ralib,textsummary_ralib"
        print "usage: tomte_batch command tasksRootDir"

        sys.exit(1)

    if len(args) < 3:
        print "forgot tasksrootdir as argument"
        sys.exit(1)
    tasksRootDir=os.path.abspath(args[2])
    if not os.path.isdir(tasksRootDir):
        print "not valid directory: " +  tasksRootDir
        sys.exit(1)

    if len(args) > 3:
        smallTable=True


    tasksRootDirBaseName=os.path.basename(tasksRootDir)
    tasksRootDirBaseName=tasksRootDirBaseName.replace("model_","").replace("_","") 


    logLevel="INFO"
    if command == "summary" or  command == "shortsummary":
       formatstr='%(levelname)s: %(message)s'
    elif command == 'latex':
       formatstr='%(message)s'
    else:
       formatstr='%(asctime)s %(levelname)s: %(message)s'

    logging.basicConfig(level=getattr(logging, logLevel),format=formatstr)






    resultsRootDir=os.path.join(tasksRootDir,'results')
    logger.info("tasksRootDir: {0} resultsRootDir: {1}".format(tasksRootDir, resultsRootDir))



    from multiprocessing import cpu_count
    default_config={
       'subdirPrefix':'task',
       'maximumNumberProcesses': cpu_count(),
       'seeds': [1293774756020]
    }
    conffile=os.path.join(tasksRootDir,"batch.json")
    conf=readJsonFile(conffile)
    if not conf:
        print "ERROR: cannot read batch config file: " +  conffile
        sys.exit(1)
        print "continue with default values:"
        conf=default_config
        print "    subdirPrefix              : '{subdirPrefix}'".format(subdirPrefix=conf['subdirPrefix'])
        print "    maximumNumberProcesses    : cpu_count() == {0}".format(conf['maximumNumberProcesses'])
        print "    seeds                     : {seeds}".format( seeds=pprint.pformat(conf['seeds']) )

    seedList=conf.get("seeds",[1293774756020])
    subdirPrefix=conf.get("subdirPrefix","task")
    taskData=conf.get("taskData",{})


    maximumNumberProcesses=conf.get('maximumNumberProcesses',cpu_count())

    logger.info("maximumNumberProcesses: {0}".format(maximumNumberProcesses))

    if command == "jtorxconform":
       b=batchrun.VerboseSubdirsTimedSeededBatch(JtorxTaskCmd,seeds=seedList,max=maximumNumberProcesses)
       command="learn"
    elif command == "learn_ralib":
       b=batchrun.VerboseSubdirsTimedSeededBatch(RalibTaskCmd,seeds=seedList,max=maximumNumberProcesses)         
       command="learn"
    else:
       b=batchrun.VerboseSubdirsTimedSeededBatch(TomteTaskCmd,seeds=seedList,max=maximumNumberProcesses)
    #b=DirectStopBatch(TomteTaskCmd,seeds=seedList,max=4)
    b.setTasksRootDir(tasksRootDir)
    b.setResultsRootDir(resultsRootDir)

    b.setSubdirPrefixes(subdirPrefix) # single subdir prefix or list of subdir prefixes

    # for displaying summary also include results from task which are marked to be
    # excluded for execution using a "_" prefix
    if command.endswith("summary"):
        # special case is '_' prefix : means mark subdir to be excluded from learning,
        # however when displaying summary of results include also these marked subdirs
        # where the taskname is the name of the subdir without the "_" prefix
        b.disableSubdirExcludeMarker()




    # TODO: pass here range of values for each param in batch.json
    num=b.addTasksFromTaskRootDir()
    b.addDataForAllTasks(taskData)


#     def addTasks(self,newTasks):
#         self.tasks.extend(newTasks)    # todo: remove numTasks and use ticket queue instead -> see comments in top of file
#         self.numTasks=self.numTasks+len(newTasks)  
#         # Submit new tasks to work queue
#         for task in newTasks:
#             self.onAddTask(task)
#             self.task_queue.put(task)
     
#     # returns results from all runs
#     def getResults(self):
#         return self.res
#     if num:
#         logger.info("{0} tasks found".format(num))
#     else:
#         logger.info( "exit, because no tasks found")
#         sys.exit()

    #print b.tasks
    #exit(1)
    if command == "learn":
        logger.info("run tasks")
        try:
            results=b.run()
        except Exception as e:
           print("cannot handle exception: " + str(e) )
           print("thus exit(1) app")
           sys.exit(1)


    if command == "latexsummary":
        logger.info("collect task data results")
        # tasksResults is OrderedDict sorted by taskName
        #   => results are  grouped per taskname (note: multiple seeds per taskname)
        tasksResults=b.getTasksResultsFromTasksRootDir()
        tasksData=getTasksData(tasksResults,b.getTaskResultDir,resultsRootDir,getTaskData)
        # tasksData is OrderedDict sorted by taskName
        # -> data grouped per taskname : per taskname(dir) multiple datasets: one dataset per seed!

        logger.info("write latex summary")
        

        
        writeSummaryFunc=writeLatexTableWithSDEV
        #called as:    writeSummaryFunc(stream,tableData,field_spec,row_spec,multiheader_spec)
        row_spec=row_spec__tomte_vs_learnlib
        multiheader_spec=multiheader_spec__tomte_vs_learnlib 
         
        if smallTable: 
            row_spec=row_spec__small
            multiheader_spec=multiheader_spec__small 
        
        description=tasksRootDirBaseName
        landscapeMode=True
        scaleFactor="1" 
         
        streamWriterFunc= lambda outputstream: writeSummary(outputstream,field_spec,tasksData,writeSummaryFunc,row_spec,multiheader_spec,description,landscapeMode,scaleFactor)
        pagerOnStreamWriterFunc(streamWriterFunc)
        
    if command == "latexsummaryLL":
        logger.info("collect task data results")
        # tasksResults is OrderedDict sorted by taskName
        #   => results are  grouped per taskname (note: multiple seeds per taskname)
        #tasksResults=b.getTasksResultsFromTasksRootDir()
        #tasksData=getTasksData(tasksResults,b.getTaskResultDir,resultsRootDir,getTaskData)
        # tasksData is OrderedDict sorted by taskName
        # -> data grouped per taskname : per taskname(dir) multiple datasets: one dataset per seed!

        tableData=getLearnlibData(tasksRootDir+"/result.csv",label2index)

        logger.info("write latex summary")
        
        tasks=[]
        for data in tableData:
            tasks.append(data['taskName'])
        
        
        keys=tasks
        fifokeys=[key for key in keys if key.startswith("fifo")]
        setkeys=[key for key in keys if key.startswith("set")]
        lifokeys=[key for key in keys if key.startswith("lifo")]
        fmkeys=[key for key in keys if key[0].isupper()]
        
        fifodata=[ data for data in tableData if data['taskName'] in fifokeys ]
        setdata=[ data for data in tableData if data['taskName'] in setkeys ]
        lifodata=[ data for data in tableData if data['taskName'] in lifokeys ]
        fmdata=[ data for data in tableData if data['taskName'] in fmkeys ]
        
        #print fifodata
        #print fifosetkeys
        #print fifokeys
        #print lifokeys
        #exit(0)
        
        
        writeSummaryFunc=writeLatexTableWithSDEV
        #called as:    writeSummaryFunc(stream,tableData,field_spec,row_spec,multiheader_spec)
        row_spec=row_spec__learnlib
        multiheader_spec=multiheader_spec__learnlib 
         
        if smallTable: 
            row_spec=row_spec__small
            multiheader_spec=multiheader_spec__small 
         
         
        description=tasksRootDirBaseName
        landscapeMode=True
        scaleFactor="1" 
         
        
        #streamWriterFunc= lambda outputstream: writeSummaryFunc(outputstream,tableData,field_spec,row_spec,multiheader_spec,description,landscapeMode,scaleFactor)
        
        #pagerOnStreamWriterFunc(streamWriterFunc)

        # create string buffer stream
        import StringIO
        outputstream = StringIO.StringIO()
        
        dataWriterFunc= lambda tableData: writeSummaryFunc(outputstream,tableData,field_spec,row_spec,multiheader_spec,description,landscapeMode,scaleFactor)

        # write result to string buffer   
        dataWriterFunc(fifodata) 
        dataWriterFunc(setdata) 
        dataWriterFunc(lifodata) 
        dataWriterFunc(fmdata) 
        

        
        
        # get result from  string buffer
        string = outputstream.getvalue()
        # Close string buffer object and discard memory buffer --
        outputstream.close()
        #outputstream.flush()
        
        # view  string in pager
        pager(string) 


    if command == "textsummary":
        logger.info("collect task data results")
       # tasksResults is OrderedDict sorted by taskName
        #   => results are  grouped per taskname (note: multiple seeds per taskname)
        tasksResults=b.getTasksResultsFromTasksRootDir()                
        tasksData=getTasksData(tasksResults,b.getTaskResultDir,resultsRootDir,getTaskData)
        # tasksData is OrderedDict sorted by taskName
        # -> data grouped per taskname : per taskname(dir) multiple datasets: one dataset per seed!
        
        #pprint.pprint(tasksData)
        #exit(0)
        
        #logger.info("wite text summary")
        #writeSummary(sys.stdout,field_spec,tasksData,writeTextSummary)

        logger.info("wite text summary")

        
        writeSummaryFunc=writeTableWithSDEV
        #called as:    writeSummaryFunc(stream,tableData,field_spec,row_spec,multiheader_spec)
        row_spec=row_spec__text_table
        multiheader_spec=None 
        description=None
        landscapeMode=None
        scaleFactor=None         

        #streamWriterFunc= lambda outputstream: writeSummary(outputstream,field_spec,tasksData,writeTextSummary)
         # writeTableWithSDEV(stream,tableData,field_spec,row_spec,multiheader_spec)
        streamWriterFunc= lambda outputstream: writeSummary(outputstream,field_spec,tasksData,writeSummaryFunc,row_spec,multiheader_spec,description,landscapeMode,scaleFactor)
        pagerOnStreamWriterFunc(streamWriterFunc)
        
        
    if command == "textsummary_ralib":
        logger.info("collect task data results")
       # tasksResults is OrderedDict sorted by taskName
        #   => results are  grouped per taskname (note: multiple seeds per taskname)
        tasksResults=b.getTasksResultsFromTasksRootDir()                
        tasksData=getTasksData(tasksResults,b.getTaskResultDir,resultsRootDir,getTaskDataRALIB)
        # tasksData is OrderedDict sorted by taskName
        # -> data grouped per taskname : per taskname(dir) multiple datasets: one dataset per seed!
        
        #pprint.pprint(tasksData)
        #exit(0)
        
        #logger.info("wite text summary")
        #writeSummary(sys.stdout,field_spec,tasksData,writeTextSummary)

        logger.info("wite text summary")

        
        writeSummaryFunc=writeTableWithSDEV
        #called as:    writeSummaryFunc(stream,tableData,field_spec,row_spec,multiheader_spec)
        row_spec=row_spec__text_table
        multiheader_spec=None 
        description=None
        landscapeMode=None
        scaleFactor=None         

        #streamWriterFunc= lambda outputstream: writeSummary(outputstream,field_spec,tasksData,writeTextSummary)
         # writeTableWithSDEV(stream,tableData,field_spec,row_spec,multiheader_spec)
        streamWriterFunc= lambda outputstream: writeSummary(outputstream,field_spec,tasksData,writeSummaryFunc,row_spec,multiheader_spec,description,landscapeMode,scaleFactor)
        pagerOnStreamWriterFunc(streamWriterFunc)        

def pagerOnStreamWriterFunc(streamWriterFunc):
        # create string buffer stream
        import StringIO
        outputstream = StringIO.StringIO()
        # write result to string buffer   
        streamWriterFunc(outputstream) 
        # get result from  string buffer
        string = outputstream.getvalue()
        # Close string buffer object and discard memory buffer --
        outputstream.close()
        #outputstream.flush()
        
        # view  string in pager
        pager(string)        

        
        
def pager(string):
        # view string result in pager
        import pydoc
        os.environ['LESS']='-SC'
        pydoc.pager(string)





def writeResults(stream,tasksData):
    line='-'*124 + '\n'
    doubleline='='*124 + '\n'       #'~'*124 +  '\n' + '~'*124
    hashline='='*124 + '\n'

    formatstr="{seed:14} {learningSuccesful:1} {totalRunningTime:} {numRuns:4} {finalNumStates:5} {totalRefinementCounter:3}" +\
      "   |  {totalTimeLearning} {totalMemQueriesLearning:10} "  +\
      "|   {totalTimeTesting} {totalEquivQueries:10} {totalMemQueriesTesting:10}  |  {terminationReason} "
    formatrunstr="                         {runNumber:4} {numStates:5} {refinementCounter:3}" +\
      "   |  {runMemTime} {numMemQueriesLearning:10} "  +\
      "|   {runTestTime} {numEquivQueries:10} {numMemQueriesTesting:10}  |   "


    # get table Data
    tableData=[]
    for taskName,taskData in tasksData.items():        
        tableDataSet=taskData
        tableDataSet["taskName"] = taskName
        tableData.append(tableDataSet) 
     
     
    # format table Data 
    tableData=format_listofmapdata(tableData)
         
    #for taskName,taskData in tasksData.items():
    for taskData in tableData:
          taskName=taskData["taskName"]
          # results is OrderDict sorted by seed+-
          stream.write("\n" + taskName + '\n')
          stream.write("="*len(taskName)  + '\n')


          stream.write(hashline)
          stream.write("         seed Suc  time runs states  NRef | memtime   memquery  | equivtime equivquery   memquery  |  terminationReason\n")
          stream.write(hashline)

          # data for a task exists of multiple data from experiments with different seeds
          for data in taskData:
               (finaldata,runs)=data
               formatData(finaldata)
               for run in runs:
                   formatData(run)
                   stream.write(formatrunstr.format(**run) + '\n')
               stream.write(line)
               stream.write(formatstr.format(**finaldata) + '\n')
               stream.write(doubleline)

          stream.write('\n')


def writeLatexTable(stream,latexCaption,latexLabel,tabledata):
    colWidth=15
    hline=r"\hline"
    stddev='std.dev.'
    fields=[
      'maxFinalNumStates',
      'totalMemQueriesLearning',
      'SDEV_totalMemQueriesLearning',
      'totalTimeLearning',
      'SDEV_totalTimeLearning',
      'totalEquivQueries',
      'SDEV_totalEquivQueries',
      'totalTimeTesting',
      'SDEV_totalTimeTesting',
      'succeeded',
    ]
    emptyColumn={
      'header': '',
      'maxFinalNumStates': '',
      'totalMemQueriesLearning': '',
      'SDEV_totalMemQueriesLearning':  '' ,
      'totalTimeLearning': '',
      'SDEV_totalTimeLearning': '',
      'totalEquivQueries': '',
      'SDEV_totalEquivQueries': '',
      'totalTimeTesting': '',
      'SDEV_totalTimeTesting': '',
      'succeeded': '',
    }
    namesColumn={
      'header': '',
      'maxFinalNumStates': 'states',
      'totalMemQueriesLearning': 'MQ',
      'SDEV_totalMemQueriesLearning':  stddev ,
      'totalTimeLearning': 'time MQ',
      'SDEV_totalTimeLearning': stddev,
      'totalEquivQueries': 'EQ',
      'SDEV_totalEquivQueries': stddev,
      'totalTimeTesting': 'time EQ',
      'SDEV_totalTimeTesting': stddev,
      'succeeded': 'Succeeded',
    }


    rows=[]
    for rowlabel,rowdata in tabledata:
        col1=copy.copy(emptyColumn)
        col2=copy.copy(namesColumn)
        col1[fields[0]]= rowlabel  # first field acts as rowlabel in col1
        row=[col1,col2]
        for data in rowdata:
            if not data:
               row.append(copy.copy(emptyColumn))
            else:
               row.append(data)
        rows.append(row)

    numCols=len(rows[0])
    formatstr=" & ".join( [ '{' + str(i) + ':^' + str(colWidth) + '}' for i in range( numCols ) ] ) + r"\\"

    latexRowFormat= " | " + " | ".join( ["l","r"] + ["c"]*(numCols-2) ) + " | "

    latexTableHeader="\n".join( [ r"\begin{table}[t]",r"\begin{center}",r"{\footnotesize",r"\begin{tabular}{"+latexRowFormat+"}"] )
    latexTableFooter="\n".join( [ r"\end{tabular}", r"}",latexCaption,latexLabel, r"\end{center}", r"\end{table}" ] )

    #print table
    print latexTableHeader
    for row in rows:

       print hline

       headerline=[]
       for col in row:
           headerline.append( r"\textbf{" + str(col['header']) +'}' )
       print formatstr.format(*headerline)

       print hlinenewrow

       for field in fields:
          dataline=[]
          for col in row:
              dataline.append(str(col[field]))
          print formatstr.format(*dataline)

    print hline
    print latexTableFooter



if __name__ == "__main__":
    # load modules needed only for test code
    import random,sys,tempfile,shutil
    # start test main()
    try:
        main(sys.argv)
    except KeyboardInterrupt as e:
        # Note: KeyboardInterrupt happens when user kills program with CTRL-C
        #       When this happens we display a nice message instead of a traceback  :
        print >> sys.stderr, "\n\nExiting on user cancel."
        sys.exit(1)




