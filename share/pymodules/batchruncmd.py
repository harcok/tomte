
################################################################################
#    util
################################################################################


def run_command_devnull(command,input=None,cwd=None):
    import subprocess
    exc=None
    returncode=None # meaning exception  happened when running command
    stdoutdata=None
    stderrdata=None
    try:
        process = subprocess.Popen(command,cwd=cwd,stdin=subprocess.PIPE,stdout=open('/dev/null'),stderr=open('/dev/null'))
        [stdoutdata, stderrdata]=process.communicate(input)
        returncode=process.returncode
    except Exception as inst:
        exc=str(inst)
    return {
      'exception': exc,
      'returncode':returncode,
      'stdout':stdoutdata,
      'stderr':stderrdata }

def run_command(command,input=None,cwd=None):
    import subprocess
    exc=None
    returncode=None # meaning exception  happened when running command
    stdoutdata=None
    stderrdata=None
    try:
        process = subprocess.Popen(command,cwd=cwd,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        [stdoutdata, stderrdata]=process.communicate(input)
        returncode=process.returncode
    except Exception as inst:
        exc=str(inst)
    return {
      'exception': exc,
      'returncode':returncode,
      'stdout':stdoutdata,
      'stderr':stderrdata }

def run_command_filed(command,stdoutfile,stderrfile,input=None,cwd=None):
    import subprocess

    stdout_fh=open(stdoutfile,"wb")
    if stdoutfile == stderrfile:
      stderr_fh=subprocess.STDOUT
    else:
      stderr_fh=open(stderrfile,"wb")

    stderr_fh.write('error:\n')
    stderr_fh.flush()
    stdout_fh.write('out:\n')
    stdout_fh.flush()
    exc=None
    returncode=None # meaning exception  happened when running command
    try:
        process = subprocess.Popen(command,cwd=cwd,stdin=subprocess.PIPE,stdout=stdout_fh,stderr=stderr_fh)
        #process.communicate(input)  # no pipes set, but pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()
        process.wait()
        returncode=process.returncode
    except Exception as inst:
        exc=str(inst)

    stdout_fh.close()
    stderr_fh.close()
    return {
      'exception': exc,
      'returncode':returncode }

