#!/usr/bin/env python
"""
 run tomte learner 

 usage:  tomte_learn  config.yaml

   where config.yaml  specifies :
      - learning parameters
      - sutinfo file which describes alphabet used by sut 
      - how to connect to sut (default is using socket localhost:7892

"""
# Author: Harco Kuppens


import sys, re, pprint,os  # modules from standard lib (python 2.6 and later)

import tempfile
import shutil
import argparse_custom as argparse 
import yaml

import util
import learnresult2uppaal

MAX_MEMORY_IN_GB=2

def check_dependencies():
    util.check_program('java','.exe')




def args_parsing(args):

    argparser = argparse.ArgumentParser(prog=os.path.basename(args[0]))

    #argparser.add_argument("--max-traces", action='store'  ,help='Maximum number of traces to run during equivalence testing.')
    #argparser.add_argument("--min-trace-length", action='store', help="Minimum length of traces during equivalence query.")
    #argparser.add_argument("--max-trace-length", action='store',help="Maximum length of traces during equivalence query.")

    argparser.add_argument("--seed", action='store', help="Seed to use for random number generator.")	
    argparser.add_argument("--max-memory", action='store',type=int,default=2, help="Maximum memory to use by virtual machine in GB (default:2GB)")	
    argparser.add_argument("--output-dir", action='store',help="Directory to store output.")
    argparser.add_argument('--server',action='store',help='use socket interface to connect to the SUT listening on specified server')
    argparser.add_argument('--port',action='store',help='use socket interface to connect to the SUT listening on specified port')
    argparser.add_argument("-e","--eclipse", action='store_true',help="Run using build code from eclipse project.")
    argparser.add_argument('--config-option',action='append', help="Overrule a configuration option in config.yaml file.")



    argparser.add_argument('configfile', help='learner configuration file')


    parse_result=argparser.parse_args(args[1:])


    seed=parse_result.seed
    max_memory=parse_result.max_memory

    output_dir=parse_result.output_dir
    port=parse_result.port
    server=parse_result.server
    eclipse=parse_result.eclipse
    config_options=parse_result.config_option
    if config_options==None:
        config_options=[]

    # handle input/output file argument
    configfile=os.path.abspath(parse_result.configfile).replace('\\','/')
    util.check_isreadablefile(configfile)

    return  (configfile,output_dir,seed,server,port,eclipse,config_options,max_memory)

def get_tomte_external_libraries(tomte_root_dir):
    # * in classpath means all jar files in that directory
    lib_path = os.path.join(tomte_root_dir,"lib/*")
    learnlib_path = os.path.join(tomte_root_dir,"lib/learnlib/*")
    statemachinelib_path = os.path.join(tomte_root_dir,"lib/statemachinelib/*")
    return [ lib_path, learnlib_path, statemachinelib_path ]

def get_tomte_full_classpath(tomte_root_dir,eclipse):
    if eclipse:
        tomte_classpath="build/"
        tomte_classpath=os.path.abspath(os.path.join(tomte_root_dir,tomte_classpath))
    else:
        tomte_classpath="lib/tomte.jar"
        tomte_classpath=os.path.abspath(os.path.join(tomte_root_dir,tomte_classpath))
        if  not util.isreadablefile(tomte_classpath):
            util.app_error("Input file '" + tomte_classpath+ "' is not readable.\nNote: use the '-e' option if you are using build code from the eclipse project instead.")
    tomte_external_libraries=get_tomte_external_libraries(tomte_root_dir)
    classpath = [tomte_classpath] + tomte_external_libraries
    classpath = os.path.pathsep.join(classpath)
    return classpath

def run_java_program(classpath,mainclass,java_program_args):
    # set options for virtual machine 
    vm_options=[]
    maxmem_option='-Xmx' + str(MAX_MEMORY_IN_GB) + 'g'
    vm_options.append(maxmem_option)
    bitsize_option=''
    if MAX_MEMORY_IN_GB > 3:
         bitsize_option="-d64"
         vm_options.append(bitsize_option)

    # create command from  vm_options, classpath with mainclass and  program_args
    command=[ 'java'] + vm_options  + [ '-cp', classpath, mainclass] + java_program_args
    print("running command : \n   "  + util.join_command(command) )
    run_command(command)

def run_command(command):
    """
      run command
      if command exits with error
         than this function will also exit the current program with the error
      else
         just returns from this function
    """
    import subprocess
    try:
      process = subprocess.Popen(command)
    except Exception as inst:
      print("problem running command : \n   "  + util.join_command(command) + "\n problem : " + str(inst))

    process.communicate()  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
       print "problem:  command exited with errorcode " + str(process.returncode)
       sys.exit(process.returncode)


def learn(configfile,output_dir,seed,server,port,tomte_root_dir,tomte_classpath,config_options):
    """
	 run tomte learner 

	 usage:  tomte_learn  config.yaml

	   where config.yaml  specifies :
	      - learning parameters
	      - sutinfo file which describes alphabet used by sut 
              - how to connect to sut (default is using socket localhost:7892

    """

    # read config and sutinfo files to retreive sut's name
    configdir=os.path.dirname(configfile)
   

    configdata=yaml.load(file(configfile))



    # 1. run tomte java application
    #-------------------------------

    mainclass= 'abslearning.learner.Main'
    classpath=get_tomte_full_classpath(tomte_root_dir,tomte_classpath)

    # tomte's java command args
    java_program_args=[]
    #
    # required options:
    java_program_args+=['--tomte-root-path',tomte_root_dir]
    # optionally options:
    if output_dir: java_program_args+=['--output-dir',str(outputDir)]
    if seed: java_program_args+=['--seed',str(seed)]
    if port: java_program_args+=['--port',str(port)]
    if server: java_program_args+=['--server',str(server)]
    if server or port :
        config_options.append("sutInterface_type=socket")
    if config_options:
        for config_option in config_options:
            java_program_args+=['--config-option',config_option]
    # normal arguments
    java_program_args.append(configfile)

    run_java_program(classpath,mainclass,java_program_args)



    print 'learning done'

    print "\n\n"





def main(args):
    """
	 run tomte learner 

	 usage:  tomte_learn  config.yaml

	   where config.yaml  specifies :
	      - learning parameters
	      - sutinfo file which describes alphabet used by sut 
              - how to connect to sut (default is using socket localhost:7892


    """
    global MAX_MEMORY_IN_GB
    check_dependencies()

    (configfile,output_dir,seed,server,port,eclipse,config_options,max_memory)=args_parsing(args)
    # set global for java command
    MAX_MEMORY_IN_GB=max_memory
    current_script_dir=os.path.dirname(sys.argv[0])
    tomte_root_dir= os.path.normpath(os.path.join(current_script_dir,'..') )
    learn(configfile,output_dir,seed,server,port,tomte_root_dir,eclipse,config_options)



if __name__ == "__main__":
    print main(sys.argv)

