
#-------------------------------------------------------------------------------
#   config  helpers
#-------------------------------------------------------------------------------


def write_config(output_file,data):
    import yaml  # external libraries   -> installed local copy   (pure python versions) (not yaml doesn't use libyaml but pure python version)

    selected_keys=('inputInterfaces','outputInterfaces','constants','configParams')
    #dictionary comprehension only supported for python 2.7 and higher
    #config={k:v for k,v in data.iteritems() if k in selected_keys}
    config={}
    for k,v in data.iteritems():
        if k in selected_keys:
            config[k]=v

    with open(output_file,'w') as f:
      f.write(yaml.safe_dump(config,default_flow_style=False))
      #f.write(yaml.safe_dump(config))

