
import error  # absolute path -> also works because first looks in current package
#from . import error # relative path

#-------------------------------------------------------------------------------
#   run commandline programs
#-------------------------------------------------------------------------------



def join_command(command):
    """
        Joins an command existing of an array of strings in a safe way
        Safe means : if a string in the array contains spaces it will
                     be enclosed with quotes in final string.
        note: mainly used for printing command with showing clearly the
              different params             
    """

    if not isinstance(command,list):
      return command

    strings=[]
    for str in command:
        if ' ' in str:    # put " qoutes arround string
          str=str.replace('"', '\\"')
          strings.append('"'+str+'"')
        else:
          strings.append(str)
    return ' '.join(strings)

def run_command(command,cwd=None):
    import subprocess
    try:
      process = subprocess.Popen(command,cwd=cwd)
    except Exception as inst:
      app_error("1. problem running command : \n   "  + join_command(command) + "\n problem : " + str(inst))

    process.communicate()  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
        app_error("2. problem running command : \n   "  + join_command(command),process.returncode)

def run_command_with_output(command,input=None,cwd=None):
    import subprocess
    try:
      process = subprocess.Popen(command,cwd=cwd,stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    except Exception as inst:
      app_error("problem running command : \n   "  + join_command(command))

    [stdoutdata, stderrdata]=process.communicate(input)  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
      print stderrdata
      app_error("problem running command : \n   "  + join_command(command),process.returncode)

    return stdoutdata



# def check_command_available(command):
#     args=['which',' "print \'hallo\'" ']
#     process = subprocess.Popen(["which",args])
#     return_code=process.wait()
#     check_return_code(return_code,"problem running print")
#


#-------------------------------------------------------------------------------
#   test
#-------------------------------------------------------------------------------

def test_run_command_with_output():
    command=['/bin/ls','/home/']
    print run_command_with_output(command,input=None,cwd=None)

def test_run_command():
    #program = "ppython"
    program = "python"

    python_lines=[
      "import time",
      "print 'start'",
      "time.sleep(2)",
      'print "end"'
    ]

    args=["-c" , ';'.join(python_lines) ]
    command=[ program ] + args

    #args=['python','-c','import time;print "start";time.sleep(2);print "end";']
    print "run command in child"
    run_command( command )
    print "back at parent"

