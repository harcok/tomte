
class CheckException(Exception): pass




#-------------------------------------------------------------------------------
#   check user exist
#-------------------------------------------------------------------------------

# TODO: only works on linux because module pwd only exist on linux
def check_user_exist(username):
    """
      check user exist on local machin
    """
    import pwd
    #if userid in [x[0] for x in pwd.getpwall()]:
    if username in [x.pw_name for x in pwd.getpwall()]:
        return
    else:
        raise CheckException("user '"+ username + "' does not exist ")

#-------------------------------------------------------------------------------
#   check cmdline programs required
#-------------------------------------------------------------------------------


def check_program_exist(program):
    if not which(program):
      raise CheckException("cannot find program '" +  program + "'in PATH")

def which(program):
    import os

    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

#-------------------------------------------------------------------------------
#   file/dir checkers : they abort program on failure
#-------------------------------------------------------------------------------

def check_file_extension(filepath,extension):
    """
    check_file_extension - checks file extension where on failed check it exits the program
                           with an error message.
                           The extension is checked case insensitive.

      extension: the extension string without the '.'
                 e.g. txt
    """
    if  not filepath.lower().endswith( "." + extension.lower()):
       raise CheckException("file '"+ filepath + "' is not a " + extension.lower() +  " file!!")

def check_isreadablefile(filepath):
    if  not isreadablefile(filepath):
        raise CheckException("file '" + filepath + "' is not readable")

def check_iswritabledir(directory):
    if  not iswritabledir(directory):
        raise CheckException("directory '" + directory + "' is not writable")

#-------------------------------------------------------------------------------
#   file/dir  helpers
#-------------------------------------------------------------------------------





def isreadablefile(path):
    import os
    return  os.path.isfile(path) and os.access(path,os.R_OK)

def iswritablefile(path):
    import os
    return  os.path.isfile(path) and os.access(path,os.W_OK)


def iswritabledir(path):
    import os
    return  os.path.isdir(path) and os.access(path,os.W_OK)



