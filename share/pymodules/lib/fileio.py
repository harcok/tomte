
def parentdir(dir):
    import os
    return os.path.dirname(dir.rstrip('/\\'))


def create_dir(dir_name):
    """
        create dir if not yes exists
    """
    import os
    if not os.path.isdir(dir_name):
        os.mkdir(dir_name)

def create_path(path_name):
    import os
    # determine subpaths by first finding absolute path
    # strip heading and tailing directory separators
    # split on directory separator
    subpaths=os.path.normpath(path_name).strip(os.sep).split(os.sep)

    if subpaths[0][1] == ":":
       # windows path starting with drive letter
       path= subpaths.pop(0) + os.sep
    else:
       # unix rootpath starting with separator
       path=os.sep

    while subpaths:
       path=path + os.sep + subpaths.pop(0)
       create_dir(path)

