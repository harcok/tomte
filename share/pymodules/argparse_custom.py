import argparse,sys

class ArgumentParser( argparse.ArgumentParser):

    def error(self, message):
        """error(message: string)

        Prints a usage message incorporating the message to stderr and exits.
        """
        sys.stderr.write('%s: error: %s\n' % (self.prog, message))
        self.print_usage(sys.stderr)
        sys.stderr.write("Try '%s --help' for more information.\n" % self.prog)
        sys.exit(2)

