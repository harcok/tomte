
################################################################################
#   logging 
################################################################################

import logging
logger = logging.getLogger(__name__)  # need such a line for every python file

# library logging
#----------------
#     this module is seen as root of library you use -> because you are as developer
#     production user of this lib
#     A) by default limit loging of lib to only ERROR msgs
logger.setLevel(logging.ERROR)
#     B) add nullhandler so that if app using library doesn't uses logging
#        still a NEEDED(by logging framework) handler is registered which
#        effectively does nothing
logger.addHandler(logging.NullHandler())


################################################################################
#    module Performer
################################################################################

from multiprocessing import Process, Queue, current_process

# src: http://stackoverflow.com/questions/7109093/checking-for-empty-queue-in-pythons-multiprocessing
# you have to use the multiprocessing.Queue class, but use the Queue.Empty exception 
from Queue import Empty

import pprint
import time
import os
import json
import copy

import sys





#===============================================================================      
#   executing workItem by Performer
#===============================================================================      


class Performer(object):
    """ performs a workitem consisting of a taskFunc and taskData 
       
       responsibility: run task in worker process
       
       at different phases calls a handler with as argument processData
       which can be used to customize the process of performing.
    """
    

    def __init__(self,*args,**kwargs):
        self.workerNum=kwargs.get("workerNum",-1)
        self.pid=kwargs.get("pid",-1)

    # methods
    #-------------------
        
      
    def process(self,data):
        data = self.onBeforeTaskProcessed(data)
        data=self.execute(data)
        data=self.onAfterTaskProcessed(data)
        return  data
                
    def execute(self,data):
        data=self.onBeforeTaskExec(data)
        taskFunc=data['taskFunc']
        taskResult = taskFunc(data['taskData'])
        data['taskResult']=taskResult
        # taskFunc not needed anymore; replace with its name so looks nicer in results
        data['taskFunc']=taskFunc.__name__
        data=self.onAfterTaskExec(data)  
        return  data
        
    def run(self,workItem):
        """
        Function run by worker process which launches processes task
        by calling taskfunc on it!
        note: when launching the worker the whole batch object state
              is transferred to new process! In this transfer
              the queue objects are special and allow communication/synchronisation
              between the processes. (see multiprocessing module)
              => means : batch object must be pickable/unpickable
        """
        
        logger.debug("start run; performing workItem in worker: {0} pid: {1}".format(self.workerNum,self.pid))
        
        taskFunc,taskData=workItem  
        #self.taskFunc=taskFunc 
        
        # performer data
        #  - data about performer's run
        #       * workerId  and pid
        #       * result run
        #  - contains also workItem data: 
        #      taskFunc (serializable, but not so nice; but we don't have to unserialize, so no problem)
        #      taskData    
        data={
           'pid':self.pid,
           'workerNum' : self.workerNum,
           'taskFunc': taskFunc,
           'taskData': taskData,
           'taskResult':None,
        }
        try:
            data=self.process(data)
        except KeyboardInterrupt as e:
            logger.debug("KeyboardInterrupt worker; pid: {0}".format(pid))
            
        logger.debug("end run; performing workItem in worker: {0}  pid: {1}".format(self.workerNum,self.pid))
        return data
        

    # extension points
    #-------------------
        
      
    # onAfterTaskProcessed is  called for all tasks (also cached ones)
    def onBeforeTaskProcessed(self,data):
        return data

    # only executed for tasks getting executed  (not for cached ones)
    def onBeforeTaskExec(self,data):
        logger.debug(pprint.pformat(data).replace("\n",""))
        return data

    # only executed for tasks getting executed  (not for cached ones)
    def onAfterTaskExec(self,data):
        return data

    # onAfterTaskProcessed is  called for all tasks (also cached ones)
    def onAfterTaskProcessed(self,data):
        return data    
        
#===============================================================================          
#   Performer  mixins
#===============================================================================      
        
        
#-------------------------------------------------------------------------------
#  timed mixin  for Performer
#-------------------------------------------------------------------------------
        
class TimedMixin(object):
    """
     mixin class for Performer which measures workitem execution time
     
     adds to data:
      - start_time  :  real time at start of execution
      - end_time    :   "    "   "   end  "    "
      - time= end_time - start_time :  real time passed 

      IMPORTANT:  real time passed is measured, but not the  effective cpu-time
      
      background info:
      * effective cpu-time can in linux be measured with 'time' cmdline program
        which give you three times :
        
          $ time program
          user(time cpu in usermode)
          kernel(time cpu in kernel mode: syscalls)
          real (real time passed on clock!)
  
        where cpu-time=kernel+real
        notes:
          - if a job is not continuous scheduled by OS then real time passed is bigger 
            than the time spend on the job! (process time= user + kernel time!)
          - however on multicore machines kernel/user time can be much bigger than 
            the real time passed because the timings of multiple cpu's are summed up!!
            e.g. 4 cpu's doing each 1 second of processing in parallel then :
                      - user+kernel time = 4 second
                      - real time        = 1 second    -> shorter!!
  
       * to implement similar functionality as the linux 'time' cmd in python
         is tricky, therefore we stick with real time as done above. This is 
         reasonable ok if we do not start more tasks in parallel than we have cpu's!        
         see for details:
          http://stackoverflow.com/questions/1608724/run-time-of-a-subprocess-popen-instance
    """
    # executed in worker Process
    def onBeforeTaskExec(self,data):
        data['start_time'] = time.time()
        return super(TimedMixin,self).onBeforeTaskExec(data)

    # executed in worker Process
    def onAfterTaskExec(self,data):
        data['end_time'] = time.time()
        real_time_int=int(round(data['end_time']-data['start_time']))
        data['time']= real_time_int
        return super(TimedMixin,self).onAfterTaskExec(data)        
      

#-------------------------------------------------------------------------------
#  cache mixin  for Performer
#-------------------------------------------------------------------------------

class CacheMixin(object):
    """Cache workitem result data in a cache file on disk
    
    Stores workitem result data in a cache file on disk. 
    If task asked to run again and the CacheMixin sees a  cache file  
    for this task already exists, then it skips this task from rerunning.
    Note: the result data can in some case only be meta data about the workitem,
          because the real result of a workitem can be stored somewhere else 
          depending on what kind of work is done.  
    """
    def __init__(self,*args,**kwargs):
        resultsRootDir = kwargs.get('resultsRootDir',None)       
        if resultsRootDir:
            self.setResultsRootDir(resultsRootDir)
                           
        self.resultFile = kwargs.get('resultFile', "result.json")

        super(CacheMixin, self).__init__(*args,**kwargs)

    def setResultsRootDir(self,resultsRootDir):
        self.resultsRootDir=resultsRootDir
        if not os.path.isdir(self.resultsRootDir):
            os.mkdir(self.resultsRootDir)
            
    def execute(self,data):        
        """ check if it is cached already, 
            if so skip running and use cached data
            else execute it and cache data
            note:  data is cached before it  goes through  onAfterTaskProcessed, 
                   but also fetch cached data goes through  onAfterTaskProcessed!!
        """  
        cacheddata=self._fetch(data)
        if cacheddata:
           logger.debug("task result fetched from cache: {0}".format(data))
           return cachedata 
        else:
           data=super(CacheMixin,self).execute(data) 
           self._cache(data)        
           return data 
        
    def getTaskResultDir(self,task,resultsRootDir):
        taskResultDir=os.path.join(resultsRootDir, task['name'])
        if not os.path.isdir(taskResultDir):
           os.mkdir(taskResultDir)
        return taskResultDir

    def _getTaskResultFile(self,task,resultsRootDir,resultFile):
        taskResultDir=self.getTaskResultDir(task, resultsRootDir )
        return os.path.join(taskResultDir, resultFile)

    def _getTaskResultFromDisk(self,task):
        resultFile=self._getTaskResultFile(task, self.resultsRootDir, self.resultFile)
        if os.path.isfile(resultFile):
            # load cached data from json file
            try:
               result=json.load( file( resultFile ) )
            except Exception as e:
                logger.warning("continue with no result; result file '" + resultFile + "' found but is not of correct json format:" + str(e))
                result=None
        else:
            result=None
        return result
         
    def _fetch(self,data):
        resultFile=self._getTaskResultFile(data['task'], self.resultsRootDir, self.resultFile)
        cacheddata=self._getTaskResultFromDisk(data['task'])
        return cacheddata
        
    def _cache(self,data):

        # write full_result as json to file
        resultFile=self._getTaskResultFile(data['task'], self.resultsRootDir, self.resultFile)
        if data['task'].has_key('resultDir'):
            del data['task']['resultDir']

        # only write resultFile for new result, that is preserve old result
        # note: end user can twist the keepOldResult value to change the
        #       behaviour when to keep old results and when to overwrite
        #       with a new result
        if data.has_key('keepOldResult'):
            keepOldResult=data['keepOldResult']
            del data['keepOldResult']
        else:
            keepOldResult=False
        if resultFile and not keepOldResult:
            f=open(resultFile, 'w')
            json.dump(data, f, sort_keys=True, indent=4)
            f.close()
        return super(CacheMixin,self).onAfterTaskProcessed(data)


    def run(self,workItem):
        """ check params needed for this mixin are set"""
        if not self.resultsRootDir:
           logger.error("no resultsRootDir set")
           time.sleep(0.01) #needed otherwise IOError and calling cleanup doesn't help
           import sys
           sys.exit()
           
        result=super(CacheMixin,self).run(workItem)



