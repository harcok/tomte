"""parse abstractions

read_abstractions_from_jsonfile(json_filename) - Read data from learner

"""

import json





def read_abstractions_from_jsonfile(json_filename_in):
    """ Read data from learner

        Reads in json file which contains data from learned model
        learned with learner.
    """
    f=open(json_filename_in,'r')
    mapper=json.load( f )
    f.close()
    #print json.dumps(jsonstr, sort_keys=True, indent=4)

    # sut interfaces
    interfaces=mapper['concrete_alphabet']

    #constants
    cons=mapper['constants']

    # input abstractions
#   inputs = [
#         {
#             "name": "IRegister",
#             "params": [
#                 [],
#                 [
#                     {
#                         "statevarindex": 0,
#                         "statevartype": "p"
#                     }
#                 ]
#             ]
#         }
#   ]

    # input abstactions
    input_abstractions={}
    ins=mapper['inputs']
    for i in ins:
        input_abstractions[ i['name'] ] = i['params']



    return  [input_abstractions,cons,interfaces]
