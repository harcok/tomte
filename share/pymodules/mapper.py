"""Utilities for extended state machines.

apply_mapper_data_on_transitions(..) - Apply data from mapper on learner results



"""

# Author: Harco Kuppens

__all__ = [
           "apply_mapper_data_on_transitions",
          ]

import sys, re, copy # module from standard lib (python 2.6 and later)
import pprint

import pyuppaal
#import pygraphviz



#-------------------------------------------------------------------------------
#  guard
#-------------------------------------------------------------------------------


# abstraction in  firstlast and statemode
#     {
#         "methodname": "IRegister",
#         "paramindex": 1,
#         "stateid": 0
#     }
#

# input abstraction in statevar mode
#     {
#         "statevarindex": 0,
#         "statevartype": "p"
#     }
# note: output abstractions are generated on the fly from memV and are stored in dot model
def get_guard_from_input(rule,operator,cons,pos,inputlabel,source_state):
    if rule['statevartype'] == "c":
        return "p" + str(pos) + ' ' + operator + ' ' +  str( cons[rule['statevarindex']] )
    elif rule['statevartype'] == "x":
        return "p" + str(pos) + ' ' + operator + ' x' +  str( rule['statevarindex'])
    elif rule['statevartype'] == "i":
        return "p" + str(pos) + ' ' + operator + ' x' +  str( rule['statevarindex']) + ' + 1 '       
    elif rule['statevartype'] == "p":
        return "p" + str(pos) + ' ' + operator + ' p' +  str( rule['statevarindex'] )




def get_guard(altValues,inputlabel,inputargs,cons,input_abstractions,source_state):
        # construct guard for transition
        # -> constructed from :  inputlabel,inputargs,cons  and input_abstractions(rules)
        # 1. guards are retreived by applying input_rules on input
        parameter_index=0
        guards=[]
        for abstraction_index in inputargs:
            if int(abstraction_index) != -1:
                rule=input_abstractions[ inputlabel ][parameter_index][int(abstraction_index)]
                guards.append( get_guard_from_input(rule,'==',cons,parameter_index,inputlabel,source_state) )

                # below addition makes sure if during testing you choose x_i=x_j that only
                # the transition with abstraction x_i with i<j is taken and not with abstraction x_j  
                #  => single choice, thus still deterministic!! 
                #      note:during learning we do not have this problem because then we choose always x different valued!!! 
                if not altValues == None:
                  for anti_abstraction_index in altValues[parameter_index]:
                      if  int(anti_abstraction_index) < int(abstraction_index):
                          rule=input_abstractions[ inputlabel ][parameter_index][int(anti_abstraction_index)]
                          guards.append( get_guard_from_input(rule,'!=',cons,parameter_index, inputlabel,source_state ) )
            else:
                # all alt. rules for inputarg gives != guard
                #for rule in input_abstractions[ inputlabel ][parameter_index]:
                #print altValues,parameter_index

                for anti_abstraction_index in altValues[parameter_index]:
                    rule=input_abstractions[ inputlabel ][parameter_index][int(anti_abstraction_index)]
                    guards.append( get_guard_from_input(rule,'!=',cons,parameter_index, inputlabel,source_state ) )
            parameter_index=parameter_index+1
        # 2. store collect guard conditions joined with boolean AND in transition
        if guards:
            return ' && '.join(guards)
        else:
            return '' # no guard

#-------------------------------------------------------------------------------
#  input
#-------------------------------------------------------------------------------

def get_input_event(inputlabel,num_input_params):
        # construct input event and its arguments for transition
        # -> constructed from : inputlabel and num_input_params
        inputparams=[]
        for num in xrange(num_input_params):
            inputparams.append('p'+str(num))
        return inputlabel + '(' + ','.join(inputparams)  + ')'

#-------------------------------------------------------------------------------
#  helper for:  actions and output
#-------------------------------------------------------------------------------

def is_local_var_needed(statevar, outputargs, outputupdates):
    """
     helper function used in :
        - get_output_event_using_outputlabel_only
        - get_update_actions_from_outputlabel_only
    """
    # 1. we skip self assignments (eg. x0=x0)
    #    `-> no local variable needed then
    if outputupdates.has_key(statevar) and outputupdates[statevar] == statevar:
        return False

    # 2. check statevar is used in output action
    if (statevar in outputupdates) and (statevar in outputargs) :
        # print "LOCAL NEEDED statevar", statevar, "in outputargs",  outputargs
        return True

    # 3. check statevar is used as value in update action (because in update action we need to use the original value)
    #  -> local variable needed to garantee the parallel update of the prefix of the output abstraction!
    if (statevar in outputupdates) and (statevar in  outputupdates.values()):
        # print "LOCAL NEEDED statevar", statevar, "in outputupdates", outputupdates
        return True

    return False

#-------------------------------------------------------------------------------
#  actions
#-------------------------------------------------------------------------------


def get_update_actions_from_outputlabel_only(freshValues,statevars,outputargs,outputupdates,cons):
    actions=[]
    # 0. initialize each fresh value
    for freshValue in freshValues:
        action= freshValue + "=random();"
        actions.append(action)
        statevars.add(freshValue)
    # 1. if local variable is needed for statevar
    #       - add local statevar
    #       - add action to assign statevar to local statevar before doing updates
    #         e,g, local_x2=x2
    for (statevar,update) in outputupdates.items():
        #print "update: ",statevar, " = " , update
        if is_local_var_needed(statevar,outputargs,outputupdates):
            #print "---> local var needed "
            local_statevar=  "local_" + statevar
            statevars.add(local_statevar)
            keep_old_in_local_statevar_action= local_statevar + "=" + statevar + ";"
            #print "keep_old_in_local_statevar_action : ", keep_old_in_local_statevar_action
            actions.append(keep_old_in_local_statevar_action)
    # 2. get all actions from outputupdates
    for (statevar,update) in outputupdates.items():
        statevars.add(statevar)

        # skip assignment when assigning  statevar to itself e.g. x0=x0
        if statevar == update:
            continue

        # add action to assign new value to statevar
        if update.startswith("p"):
            action= statevar + "=" + update + ";"
        elif update.startswith("x"):
            if is_local_var_needed(update,outputargs,outputupdates):
                local_statevar=  "local_" + update
                action= statevar + "=" + local_statevar + ";"
                statevars.add(local_statevar)
            else:
                action= statevar + "=" +  update + ";"
        elif update.startswith("f"):
            action= statevar + "=" + update + ";"
        elif str(update) == "-1":  # meaning unset statevar
            #action=""
            action= statevar + "=-1000000;"
            #action= statevar + "=-1;"
        elif update.startswith("c"):
            consIndex=int(update[1:])
            value=cons[consIndex]
            action= statevar + "=" +  str(value)  + ";"
        else:
            print "error: update doesn't start with x,f,c nor p!!  update: ", update
            sys.exit(1)
        actions.append(action)
    # 3. for each pX in an output we :
    #     - add statevar 'local_pX'
    #     - add for corresponding input transition the action 'local_pX=pX'
    #     - and rename pX in output to 'local_pX'
    #            `-> we do this in method 'get_output_event_using_outputlabel_only'
    for indx,outputarg in enumerate(outputargs):
        if outputarg.startswith("p"):
            statevar="local_" + outputarg
            statevars.add(statevar)
            action= statevar + "=" + outputarg + ";"
            actions.append(action)
    # 4. combine all actions in single result action
    if actions:
        return ' '.join(actions)
    else:
        return '' # no action

#-------------------------------------------------------------------------------
#  output
#-------------------------------------------------------------------------------


def get_output_event_using_outputlabel_only(statevars,outputlabel,outputargs,outputupdates,cons):
    params=[]
    #print outputargs


    for arg in outputargs:
        param=arg
        if arg.startswith("x") and  is_local_var_needed(param,outputargs,outputupdates):
            param="local_" + param
            statevars.add(param)
        if arg.startswith("p"):
            param="local_" + param
            statevars.add(param)
        if arg.startswith("c"):
            consIndex=int(arg[1:])
            param=str(cons[consIndex])
        params.append(param)
    #print outputlabel + '(' + ','.join(params) + ')'
    return outputlabel + '(' + ','.join(params) + ')'

#-------------------------------------------------------------------------------
#  main function applying mapping on each transition
#-------------------------------------------------------------------------------


def apply_mapper_data_on_transitions(transitions,input_abstractions,cons,number_params,fullHypOutput):
    """ Apply data from mapper on results from learner

   """

    # example input abstractions
    # from abstractions.txt
    #
    #    ILogin([x0], [x1])
    #    ILogout()
    #    IRegister([], [])
    #
    # from abstractions.json ; key "inputs" contains input abstractions:
    #
    #    "inputs": [
    #        {
    #            "name": "ILogin",
    #            "params": [
    #                [
    #                    {
    #                        "statevarindex": 0,
    #                        "statevartype": "x"
    #                    }
    #                ],
    #                [
    #                    {
    #                        "statevarindex": 1,
    #                        "statevartype": "x"
    #                    }
    #                ]
    #            ]
    #        },
    #        {
    #            "name": "ILogout",
    #            "params": []
    #        },
    #        {
    #            "name": "IRegister",
    #            "params": [
    #                [],
    #                []
    #            ]
    #        }
    #    ]


    # OLD DOC: maybe interesting for future statebase input abstractions:
    # an input abstaction i for some method param is of the form : Sx_In_Pj which specifies a storage point (=action parameter index)
    # loop over all input abstractions and fetch :
    #   - statevars: statevar for each unique storage point Sx_In_Pj
    #   - storage_index: map each state's input method 'Sx_In' to a set of storage points 'Sx_In_Pj'  which store date for that state's input




    # collect needed statevars from input abstractions:
    statevars=set()
    for  action_abstractions in  input_abstractions.values():
      # abstractions for some action X
      for  param_abstractions in  action_abstractions:
        # abstractions for some action X and parameter i of that action : per action parameter we can have multiple abstractions!
        for abstraction in  param_abstractions:
            if abstraction:
                # i_index :  increment of x_index
                if abstraction['statevartype'].startswith("i"):
                     statevar =  "x" + str(abstraction['statevarindex'])
                     statevars.add(statevar) 
                # x_index itself!!                 
                if abstraction['statevartype'].startswith("x"):
                     statevar =  "x" + str(abstraction['statevarindex'])
                     statevars.add(statevar)

    # loop over transitions
    # and for each transition create an object for which you set the concrete:
    #   - input
    #   - output
    #   - guard
    #   - action
    # where you did apply the abstraction rules on there abstract counter parts
    trans_out=[]
    indent='  '
    for t in transitions:
        #print t
        transition={}
        source=transition['source']=t['source']   # eg. s0  from dot file
        target=transition['target']=t['target']


        inputlabel=t['input']['name']
        num_input_params=number_params[inputlabel]
        inputargs=t['input']['args']
        if  not num_input_params == len(inputargs):
            print "error: wrong number of inputs "
            sys.exit(1)
        outputlabel=t['output']['name']
        outputargs=t['output']['args']
        outputupdates=t['output']['updates']

        freshValues=t['freshValues']
        altValues=t['altValues']



        transition['output']=get_output_event_using_outputlabel_only(statevars,outputlabel,outputargs,outputupdates,cons)
        if fullHypOutput:
            specialprefix="O"+t['abstractinput'] + "____" + t['abstractoutput'] +   "____"
            specialprefix=specialprefix.replace('-','XMINX')
            specialprefix=specialprefix.replace('=','XEQX')

            # not XMINX has three X..X to distinguish from possible matching method names when reverting back to -!
            transition['output']=specialprefix + transition['output']


        transition['input']=get_input_event(inputlabel,num_input_params)
        transition['guard']=get_guard(altValues,inputlabel,inputargs,cons,input_abstractions,transition['source'])
        transition['action']=get_update_actions_from_outputlabel_only(freshValues,statevars,outputargs,outputupdates,cons)

     
        trans_out.append(transition)

    return (trans_out,statevars)