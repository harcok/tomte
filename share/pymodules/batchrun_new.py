'''create and run batch of workitems, where workitem=(taskfunc,taskdata)

TERMINOLOGY

background: no consistent terminology does exist:
  * job queue :  a job is something to be runned
      http://en.wikipedia.org/wiki/Job_queue
  * task queue : a task is a unit of work    -> executed by some standard function
      https://developers.google.com/appengine/docs/java/taskqueue/overview#Task_Concepts
        If an app needs to execute some background work, it can use the Task Queue API 
        to organize that work into small, discrete units, called tasks. 
  * http://en.wikipedia.org/wiki/Task_%28computing%29
       a task is a unit of execution within a job
     http://en.wikipedia.org/wiki/Job_stream   
       A job consists of the execution of one or more programs.   (probably tasks?)
       
  => so job is big piece of work which can be divided in smaller pieces called tasks

we want to separate functional and data part :
    instead of task we talk about  taskFunc(also called "task") and taskData(also called "case").

in workflow management TERMINOLOGY"
   task : atomic unit of work, activity,  function
   case : actual params of task
   workitem: (task,case)
   performer : actor which performs work items
  
=> many confusing terms       
so we use simplified TERMINOLOGY:

  taskFunction ( called task in workflow) :   function to execute
  taskData (case) :  data for taskFunction to execute
  task (workItem): tuple of taskFunction and taskData 
  Performer: executes workitem
  
  
  Batch : sets up workitems to execute and delegates them to worker processes  
          by adding them to work queue
  Worker process : lets a Performer run workitems from work queue
    
    

workitem
   `-> tuple: (taskfunc,taskdata)
    -> easy pickable
 

main Process
------------
Batch        
  -> runs in mainProces      
  create workitems 
      `-> workitem: (taskfunc,taskdata)
      
  delegate running set of workitems using a pool of processes

  mixins: 
     subdir     tasks from dirs    
     seed       repeat for seeds
                      
workerProcess
-------------

workerFunc 
  -> runs in workerProces
  gets task from workqueue in current worker process
  call Performer to execute task
  repeat until workqueue empty  
   
Performer 
  -> runs in workerProces
  execute workItem = (taskFunc,taskData)     in run method
  
  mixins:
      time
      LOCK plugin!! -> LOCKED ones are currently running, so don't mess with its
                       result directly
                    -> also usefull for external monitoring : LOCKED ones are currently running!
                       
      cache 
        -> if workitem is already run, then we can get result from cache  
        -> uses taskData[cacheFile] field to configurate location of cacheFile
        
           cache prevents redoing work:
                 to get results after an earlier run again,
                 we don't need to really run the workitems again,
                 so we run the batch, but instead of running the results
                 are fetched from cache!
                 
           fetch results from cache only      
                 sometimes some runs fail, but we then only want to
                 fetch results from none failed (read cached runs)
                 and look at them.                 
                 Or you can interrupt the batch halfway, and still be
                 able to look at results so far without running anything.
                 
                 

 
  
  
================================================================================
                                                     responsibility
  batch   -> create workitems                            batch                                                   
  subdirs -> create workitems from subdirs               batch (subdirsBatch) 
    
  cache -> cache result on disk                         performer  
            `-> memory caching makes no sense!
           -> cached in jsonfile => NEEDS its path!!  
               note: we can store them all in same dir 
                     with different file, 
                     or each in different dir etc..
              => implement method getCacheFilePath
                 which contructs/retreives it from data
           -> can be used with or without subdirs mixin 
                         
  time  -> time each task                               performer
                                                            `-> time mixing

================================================================================

about output:
  real output: result of all running tasks  -> practically written to file, but could also be outputted to stdout
  diagnostic output :  msgs how running goes=> enduser can see it goes well or not!
                      note: could also be done only by user initiative by running cmd  batch_status 
  
================================================================================
 
================================================================================


  
  resultsRootDir -> set in batch
  workItemResultDir/File -> set in task  in onAddTask
   `-> use dir and fixed file name-> l
   
'''


################################################################################
#   logging 
################################################################################

import logging
logger = logging.getLogger(__name__)  # need such a line for every python file

# library logging
#----------------
#     this module is seen as root of library you use -> because you are as developer
#     production user of this lib
#     A) by default limit loging of lib to only ERROR msgs
logger.setLevel(logging.ERROR)
#     B) add nullhandler so that if app using library doesn't uses logging
#        still a NEEDED(by logging framework) handler is registered which
#        effectively does nothing
logger.addHandler(logging.NullHandler())




################################################################################
#    module Batch
################################################################################

from multiprocessing import Process, Queue, current_process

# src: http://stackoverflow.com/questions/7109093/checking-for-empty-queue-in-pythons-multiprocessing
# you have to use the multiprocessing.Queue class, but use the Queue.Empty exception 
from Queue import Empty



import pprint
import time
import os
import json
import copy

import sys


#===============================================================================      
#   executing batch  of workitems
#===============================================================================      



def workerProcessFunc(workerNum,performerClass,performerArgs,task_queue,done_queue):
    """
     IMPORTANT REMARK:
       For windows all arguments of workerProcessFunc must be pickable
       otherwise we cannot launch this using multiprocessing.Process
  
       src: https://docs.python.org/2/library/multiprocessing.html#windows
          Ensure that all arguments to Process.__init__() are picklable.
  
        Note: for linux the arguments don't need to be pickable. Because linux
        uses forking. However windows doesn't support forking and therefore
        instead must instantiated a new process itself and communicate all 
        arguments to the process using a IPC communication mechanism  which
        is only possible if all arguments are serializable(pickable)!
  
        Note: for the same reason we also use  a function 'workerProcessFunc' 
              target argument for multiprocessing.Process instead of a 
              method of some instantiated object (the object can be unpickable).
    """
    pid=current_process().pid    
    logger.info("start worker process {0} with pid {1}".format(workerNum,pid))
    performerArgs["pid"]=pid
    performerArgs["workerNum"]=workerNum
    #performer=performerClass(workerNum,pid)
    performer=performerClass(**performerArgs)    
    for workItem in iter(task_queue.get, 'STOP'):        
        result_data = performer.run(workItem)
        done_queue.put(result_data)
    # put acknowledment of stop of worker process in done queue 
    #time.sleep(500) #-> to test timeout if ack not send fast enough!!   
    done_queue.put( "acknowledge stop worker process {0} with pid {1}".format(workerNum,pid) )    
    #logger.debug("stop worker process {0} with pid {1}".format(workerNum,pid))  

#===============================================================================      
      
def PositiveNonZeroInt(x):
    """ checks if argument is a integer > 0
    """
    try:
        if type(x)!=int:
           return False
        if x>0:
          return True
        else:
          return False
    except ValueError:
        return False
      
      
# run batch of tasks
class Batch(object):
    """
         responsibility: 
           * create workitems 
           * delegate the execution of the workitems using a pool of worker processes         
    """

    def __init__(self,performerClass=None,performerArgs={},tasks=[],max=4):
        self.performerClass=performerClass
        self.performerArgs=performerArgs
        self.maxNumProcesses=max
        self.results=[]
        self.workerprocesses=[]
        
        if not PositiveNonZeroInt(self.maxNumProcesses) :
           # wrong parameters must raise a module exeception -> wrong usage module
           # also log the message as an error message.
           import sys
           msg="param max for Batch must be a positive integer"
           logger.error(msg)
           raise Exception(msg)

        # Create queues
        self.task_queue = Queue()
        self.ticket_queue = Queue()
        self.done_queue = Queue()
        
        self.addTasks(tasks)


    # extension points
    #-------------------

    def onAddTask(self,task):
        """handler called when a task is added
        
        note: task is used as alias for workItem
        """       
        return workItem
     
    def onStartBatch(self,tasks):
        """ executed on start of batch
        
        tasks: list of initial task-results before starting batch
        """
        return tasks
    
    def onResult(self,task):
        """ Handler called in main process of batch when running a 
            workitem gives a new result 
        """ 
        #logger.debug(pprint.pformat(data).replace("\n",""))
        return result

    # executed in batch Process
    def onEndBatch(self,tasks):
        """ executed on end of batch
        
        tasks: list of performerData for each workItem
        """    
        return tasks


    # methods
    #--------
    
    def getTaskId(self,task):
        taskFunc,taskData=task
        if taskData.has_key('taskId'):
            return taskData['taskId']
        else:
            return taskFunc.__name__
            
    def preProcessTask(self,task):
        # set taskId        
        taskFunc,taskData=task
        taskData['taskId']=self.getTaskId(task)
        return task
        
    def enqueueTask(self,task):
        # extension point            
        task=self.onAddTask(task)               
        
        # enqueue task
        self.task_queue.put(task)
        self.ticket_queue.put(1)   
              
    def addTask(self,task):
        # note: addTask splitted in separate preprocess and enqueueTask
        #       part so that it can easier by overriden by paramterize mixin
        # taskId added by single task preprocess
        task=self.preProcessTask(task)     
        self.enqueueTask(task) 
                    
    def addTasks(self,newTasks):
        for task in newTasks:
            self.addTask(task)
             
     
    def getResults(self):
        """returns results from all workitems"""
        return self.results

    # TODO: look into good cleanup maybe automatically
    #   maybe interesting is http://stackoverflow.com/questions/865115/how-do-i-correctly-clean-up-a-python-object
    def cleanup(self): 
        """ cleanup batch
        
        """
        self.ticket_queue.close()
        self.task_queue.close()
        self.done_queue.close()
        
    def terminateWorkers(self):
        for p in self.workerprocesses:
            p.terminate()



    def run(self):
        """ run jobs in task_queue until it is empty
        """ 

        self.runResults=[]

        # Create worker processes
        for i in range(self.maxNumProcesses):
            # launch worker
            # note: for windows we need a function helper because
            #       only function can be used to launch a process
            #       For more info: see comment at workerProcessFunc
            p=Process(target=workerProcessFunc, args=(i,self.performerClass,self.performerArgs,self.task_queue,self.done_queue))
            p.start()
            self.workerprocesses.append(p)


        self.runResults=self.onStartBatch(self.runResults) # results current run (empty list initially)!


        # fetch first a ticket (= promise for future result)
        # if there are no tickets left, then there are no results coming anymore => we are done
        # if fetched a ticket, then wait for result
        # note: workers themselves can add new tasks (adding a ticket to ticket_queue and a task to task_queue)
        while True:
             # first get ticket
             try:
                self.ticket_queue.get_nowait()
             except Empty:
                # no more tickets we are done  
                break   
             # wait for result for ticket   
             data=self.done_queue.get()     # blocks if result not yet there
             data=self.onResult(data)
             self.runResults.append(data)

              
        self.runResults=self.onEndBatch(self.runResults)
        
        # collect results from all runs
        self.results.extend(self.runResults)
  
    
        # Tell child processes to stop
        logger.debug("send STOP to tasks")
        for i in range(self.maxNumProcesses):
            self.ticket_queue.put(1)
            self.task_queue.put('STOP')       
 
#         logger.debug("wait until all tasks fetched STOP")
#         while not self.task_queue.empty() :
#             pass 
            
        logger.info("wait until all tasks fetched STOP and report stopped")         
        
#         while not self.ticket_queue.empty():
#              self.ticket_queue.get_nowait()
#              stop_ack=self.done_queue.get()     # blocks if result not yet there
#              logger.info(stop_ack)   
                      
        while True:
             # first get ticket
             try:
                self.ticket_queue.get_nowait()
             except Empty:
                # no more tickets we are done  
                break   
             #stop_ack=self.done_queue.get()     # blocks if result not yet there
             try:
                stop_ack=self.done_queue.get(True,10)  #  blocks 10 seconds
                logger.info(stop_ack)
             except Empty:
                logger.info("timeout for stop all tasks passed, force terminate all workers")
                self.terminateWorkers()
                break   
             



        logger.info("finished batch")
        
        # returns results from current run
        return self.runResults


#-------------------------------------------------------------------------------
#  subdirs mixin for Batch
#-------------------------------------------------------------------------------
class ParameterizeMixin(object):
    """parameterize mixin determines tasks in batch to run
    

       note: if also using SubdirsMixin then it may be mixed in after 
             this mixin, because SubdirsMixin must use  addTask overriden
             by the  ParameterizeMixin  
             so SubdirsMixin must first determinze tasks from subdir,
             and then when adding ParameterizeMixin parameterizes them       
    """
    
 
    def setParamSets(self,paramSets):
        self.paramSets=paramSets
    
    def addTask(self,task):
        task=self.preProcessTask(task)      # taskId added by parent by single task preprocess
        for paramSet in self.paramSets:
            # copy task 
            import copy
            copytask=copy.deepcopy(task)
            # add params and updated id  using paramSet
            taskFunc,taskData=copytask
            taskData.update(paramSet)
            str_paramSet="_".join([ str(item[0]) + "=" + str(item[1]) for item in paramSet.items() ])
            taskData['taskId']= taskData['taskId'] + "_" + str_paramSet
            # enqueueTask 
            self.enqueueTask(copytask)              
    
    
        
 
 
class SubdirsMixin(object):
    """subdirs mixin helps adding tasks from subdirs"""
    
    tasksRootDir=None
    taskFunc=None
    subdirPrefixes=['task']

    def setTaskFunc(self,taskFunc):
        self.taskFunc=taskFunc
        
    def setTasksRootDir(self,tasksRootDir):
        self.tasksRootDir=tasksRootDir
    
    def setSubdirPrefixes(self,subdirPrefixes):
        """ only subdirs which match one of the prefixes are used in batch"""
        if type(subdirPrefixes) is not list:
           logger.error("setSubdirPrefixes needs a list of strings argument")
           import sys
           sys.exit()
        self.subdirPrefixes=subdirPrefixes # list of subdir prefixes

    def matchesPrefixes(self,dirname):
        """ checks whether dirname matches one of the specified prefixes
        """
        for prefix in self.subdirPrefixes:
            if dirname.startswith(prefix) : 
                return True

    def addTasksFromTaskRootDir(self,*args,**kwargs):
        """
          You must addTasks explicitly so that we can get onAddTask events
          executed before batch.run.
          Note: we do not call addTasksFromTaskRootDir automatically
                in  setTasksRootDir because there could be still parameters
                needed to be set on batch object which are used in onAddTask event
                Thus user must first set all params of batch, and then call
                add tasks so that we can  execute onAddTask event with all
                batch parameters set!
        """
        self.tasksRootDir = kwargs.get('tasksRootDir', self.tasksRootDir)
        self.taskFunc = kwargs.get('taskFunc', self.taskFunc) 
        subdirPrefixes = kwargs.get('subdirPrefixes', self.subdirPrefixes) 
        self.setSubdirPrefixes(subdirPrefixes)
              
        if not self.tasksRootDir:
           logger.error("no tasksRootDir set")
           import sys
           sys.exit()
        if not callable(self.taskFunc):
           logger.error("taskFunc not callable")
           import sys
           sys.exit() 
                        
        logger.info(self.taskFunc)  
                 
        foundTasks=self.getTasksFromTasksRootDir(self.tasksRootDir)
        for taskData in foundTasks:          
           workItem=(self.taskFunc,taskData)
           self.addTask(workItem)
        return len(foundTasks)

    def getTasksFromTasksRootDir(self,tasksRootDir):
        """ get tasks from root dir"""
        tasks= []
        walker=os.walk(tasksRootDir)
        (dirpath, dirnames, filenames) = walker.next()
        for dirname in dirnames:
            # skip directories which are not matched by the prefixes
            if not self.matchesPrefixes(dirname):
               continue
               
            # we are creating new task to be added  
            # so we choose a good taskId for it:   
            taskId=dirname
            taskData={
                  'taskId': taskId,
                  'taskDir':os.path.join(dirpath,dirname)
            }
            tasks.append(taskData)
        return tasks


    def run(self):
        return super(SubdirsMixin,self).run()





    
