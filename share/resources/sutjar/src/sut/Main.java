package sut;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;


import util.InputAction;
import util.OutputAction;

public class Main implements Runnable {
	private Sut sut;
	private BufferedReader sockinLL;
	private PrintWriter sockoutLL;
	private Socket sockLL;

	public Main(Sut sut, Socket sockLL, BufferedReader sockinLL, PrintWriter sockoutLL) {
		this.sut = sut;
		this.sockLL = sockLL;
		this.sockinLL = sockinLL;
		this.sockoutLL = sockoutLL;
	}
	
	public static void globalOut(String s) {
		//System.err.println("myout : " + s);
	}

	public static void main(String[] args) {
		System.out.println("Starting Sut...");
		ServerSocket servsockLL;
		try {
			// port used for communication with LearnLib
			int portNo = 7892;

			// instantiate a socket for accepting a connection
			servsockLL = new ServerSocket(portNo);
			servsockLL.setSoTimeout(35000); // accept waits 35 seconds for connection
			while (true) {
				// wait to accept a connection request
				// then a data socket is created
				Socket sockLL;
				try {
				    sockLL = servsockLL.accept();
				} catch  (java.net.SocketTimeoutException e) {
				    servsockLL.close();	
		            break;  		
				}
				// get an input stream for reading from the data socket
				InputStream inStreamLL = sockLL.getInputStream();
				// create a BufferedReader object for text line input
				BufferedReader sockinLL = new BufferedReader(
						new InputStreamReader(inStreamLL));

				// get an output stream for writing to the data socket
				OutputStream outStreamLL = sockLL.getOutputStream();
				// create a PrinterWriter object for character-mode output
				PrintWriter sockoutLL = new PrintWriter(new OutputStreamWriter(
						outStreamLL));

				System.out.println("New client...");
				Main client = new Main(new Sut(), sockLL, sockinLL, sockoutLL);
				new Thread(client).start();
			}
			
		} catch (IOException e) {
			
			System.out.println("IOException in main ...");
			e.printStackTrace();
		}
		
		System.out.println("Sut Client Starter Finished... the started clients keep on running!");		
	}

	public void run() {
		System.out.println("Starting client...");
		try {
			String input;
			while ((input = sockinLL.readLine()) != null) {

				//System.out.println("Handling: " + concreteInput);

				if (input.equals("reset")) {
					sut.reset();
					//System.err.println("--> RESET <---");
					continue;
				}
				
				InputAction inputAction = new InputAction(input);
				OutputAction result = sut.processSymbol(inputAction);

				//System.err.println(input + " -> " + result.getValuesAsString() );
				//System.err.println("input: " + input);
				//Main.globalOut(result.getValuesAsString());
				
				//System.out.println("Returning: " + result.toString());

				// send concrete output to LearnLib
				sockoutLL.println(result.getValuesAsString());
				sockoutLL.flush();
			}
		} catch (SocketException e) {
			System.out.println("Server closed connection");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("reset...");
		} 
		
		try {
			sockinLL.close();
			sockoutLL.close();
			sockLL.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		System.out.println("Closing client...");
	}
}
