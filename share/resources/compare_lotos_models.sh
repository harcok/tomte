#!/bin/bash

#-------------------------------------------------------------------------------
# initialize
#-------------------------------------------------------------------------------


if [[ -z "$CADP" ]]; 
then 
  echo 'CADP environment variable not set!';
  echo 'Set with following command (in bash shell) :'
  echo ''
  echo '  export CADP=/root/directory/of/CDAP/'
  echo ''
  exit 1 ; 
fi

CADP_ARCH=`"$CADP"/com/arch`
PATH="$PATH:$CADP/bin.$CADP_ARCH:$CADP/com" ; export PATH


############################################################################33
# helpers
############################################################################33


# initialize call_dir script_dir and script_name
cleandir() {
    if [ ! -d "$1" ]
    then
      echo "cleandir: invalid directory $1"
    exit 1
    fi
    xx_call_dir=$PWD
    cd $1
    xx_dir=$PWD
    cd $xx_call_dir
    echo "${xx_dir}"
    unset xx_dir xx_call_dir
}


cleanpath() {
    if [  -d "$1" ]
    then
      cleandir "$1"
    else
      xx_dir=$(dirname $1)
      xx_name=$(basename $1)
      xx_call_dir=$PWD
      cd $xx_dir
      xx_dir=$PWD
      cd $xx_call_dir
      echo "${xx_dir}/${xx_name}"
      unset xx_dir xx_name xx_call_dir
    fi
}


print_var() {
    if [  "$#" == 0  ]
    then
     echo "error: print_var misses none-empty argument"
     exit 1
    fi
    symbol_name=$1
    symbol_val=$(eval printf  \"\$$(printf $symbol_name)\")
    echo "$symbol_name=$symbol_val"
}


call_dir=$PWD
script_dir=$(cleandir $(dirname $0))
script_name=$(basename $0)



# load error handling library
set -e  # script ends with signal EXIT on error  (trap handler called!)
set -u  # script ends with signal EXIT when unset variable is used


error_handler () {
    errornu=$?
    if [ $errornu -eq 0 ] 
    then 
     return
    fi
    echo ""
    echo ""
    echo "   ===> ERROR($errornu) HAPPENED <===="
    echo ""
    echo ""
    # note: "${x+set}" expands to 'set' if $x is set and to the empty string if $x is unset
    #        see : man dash
    if [ -n "${BASH_COMMAND+set}" ]
    then
     echo "last executing command : $BASH_COMMAND"
    fi
}    

trap error_handler EXIT



check_eqm() {
    eqm=$1
    if [     "$eqm" != "strong" \
         -a  "$eqm" != "weaktrace"  \
         -a  "$eqm" != "trace"  \
         -a  "$eqm" != "branching"  \
         -a  "$eqm" != "observational"  \
         -a  "$eqm" != "safety"  \
         -a  "$eqm" != "taustar"  \
       ]
    then
      echo "'$eqm' is not a right eq_method"
      usage
    fi
    #unset eqm
}

check_read_file() {
    if [ ! -r "$1" ]
    then
      echo "error: cannot read input file : $1";
      exit 1;
    fi
}

header() {
    echo
    echo "------------------------------------------------------------------------------"
    echo " $script_name : $1"
    echo "------------------------------------------------------------------------------"
}

############################################################################33

usage() {
  echo "usage: $script_name [eq_method] "
  echo "   eq_method is one of the values: strong,trace,weaktrace,branching,observational,safety,taustar"
  echo "   eq_method is by default : 'strong'"
  echo "   for more info see : http://www.inrialpes.fr/vasy/cadp/man/bisimulator.html"
  echo ""
  exit 0
}

if [[  $0 == 'bash'  ]]
then
    shift 
fi    

if [[  $# > 1  ]]
then 
  usage
fi

lotos_path=$script_dir

if [  "$#" == 0  ]
then
  eqm="strong"
else
    if [[  "$1" = '-h' || "$1" = '--help' ]]
    then 
      usage
    fi
    eqm="$1"
fi
check_eqm "$eqm"



#check svl command in path 
if [[ -z `which svl` ]]; then echo 'CADP svl command not found in path';exit 1 ; fi
if [[ -z `which bcg_open` ]]; then echo 'CADP bcg_open command not found in path';exit 1 ; fi

# check required resources
teacher_model="$lotos_path/teacher/model.lotos"
check_read_file "$teacher_model"
learned_model="$lotos_path/learned/model.lotos"
check_read_file "$learned_model"

#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------

# generate lts from lotos
#------------------------

header "generating fsm in  $lotos_path/teacher/"
cd $lotos_path/teacher/
time svl

header "generating fsm in  $lotos_path/learned/"
cd $lotos_path/learned/
time svl

# compare lotos
#----------------

# check required resources
file1=$lotos_path/teacher/model.bcg
file2=$lotos_path/learned/model.bcg
check_read_file "$file1"
check_read_file "$file2"

header "comparing fsm : using  method '$eqm'"
time bcg_open $file1 bisimulator -diag -bfs -$eqm  $file2

# cleanup file generated
rm bisimulator


cd $call_dir
