
# which experiment directories to include in summaries:
mydirs='fifo* fm* lifo* extra__*'    



# create summaries
# -----------------
mkdir summaries
for d in $mydirs;
do 
    printf "\n\n\n$d\n";  
    tomte_batch textsummary $d  > summaries/$d.txt; 
    cp summaries/$d.txt $d/results.txt
    
    tomte_batch latexsummary $d  > summaries/$d.tex; 
    # tomte_batch latexsummary $d 1 > summaries/$d.tex; 
    #                             `-> hack to print only subset (small summaries)
    cp summaries/$d.tex $d/results.tex
done

