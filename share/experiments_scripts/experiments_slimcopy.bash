# do slimcopy which 
# + only copies from the results/ folder
#   - results.json
#   - statistics.json 
# + copies all from all other folders  (model input folders are fully copied)  
slimcopy() {
    rsync -av --exclude '/slimcopy' --include='statistics.json' --include='result.json' --exclude='results/*/*' . slimcopy  
}
slimcopy

if [ ! -d slimcopy ]; 
then 
    echo "Error: something wrong in rsync operation in slimcopy"; 
    exit 1
fi;

# remove stdin/stdout from result.json 
# REASON: can be very large 
for f in `find slimcopy -type f -name result.json`; 
do 
   # perl -i -ne 'if (!/\s*"stdout/ && !/\s*"stderr/ ) { print $_} '  $f;
   #perl -i -ne 'if (/\s*"stdout/  ) {print '        "stderr":""' } else { print $_} '  $f;


   perl -i -ne 'if (!/\s*"stderr/  ) { print $_}' $f;
   #perl -i -ne 'if (/\s*"stderr/  ) {print "        \"stderr\":\"\"\n" } else { print $_}' $f;
   perl -i -ne 'if (/\s*"stdout/  ) {print "        \"stdout\":\"stdout and stderr removed in slimcopy\"\n" } else { print $_}' $f;
   #echo $f
done


