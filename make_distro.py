#!/bin/env python

import os,sys
import tempfile
import shutil
import argparse
import re
import fileinput


def get_immediate_subdirectories(a_dir):
     return [name for name in os.listdir(a_dir)
         if os.path.isdir(os.path.join(a_dir, name))]

def get_none_distro_subdirectories(a_dir,distrotag):
     result = []
     for subdir in get_immediate_subdirectories(a_dir) :
         if not os.path.isfile( os.path.join(a_dir,subdir,distrotag) ):
             result.append(subdir)
     return result        

def remove_none_distro_subdirectories(a_dir,distrotag):
     for subdir in  get_none_distro_subdirectories(a_dir,distrotag):
        shutil.rmtree(os.path.join(a_dir,subdir))


cadpverification=re.compile(r'method: \s*cadp')
def unset_cadp_verification_in_config(configfile):
  #with fileinput.input(inplace=True, files=(configfile)) as f:
  #   for line in f:
      for line in  fileinput.input(inplace=True, files=(configfile)):
        if cadpverification.search(line) is not None:
            print line.replace("cadp","null"), 
        else:
            print line, # this goes to the current file

def unset_cadp_verification_in_models_config(modelsdir,configfile):
     for subdir in get_immediate_subdirectories(modelsdir) :
         print os.path.join(modelsdir,subdir,configfile)
         unset_cadp_verification_in_config(os.path.join(modelsdir,subdir,configfile))


def get_svn_dirs(root_dir,arg=None):
  svn_dirs=[]
  def printall(arg,dirpath,namelist):
    for name in namelist:
        if name==".svn":
            path=os.path.join(dirpath,name)
            if os.path.isdir(path):  # follows symbolic links in check
                svn_dirs.append(path)
  os.path.walk(root_dir,printall, None)
  return svn_dirs

def rm_svn_subdirs(dir,ask=False):
    """
      remove .svn subdirs in dir
    """

    svn_dirs=get_svn_dirs(dir)
    if ask:
        if svn_dirs:
            print("")
            print("Found .svn dirs:")
            print("----------------")
            for dir in svn_dirs: print dir
            print("")
            answer=raw_input("Do you want to remove these dirs? (y/n)")
            if answer.lower().strip()=="y":
               for dir in svn_dirs: shutil.rmtree(dir)
               print "\nDirectories removed\n"
            else:
               print "\nNothing done\n"
        else:
            print("no .svn dirs found")
    else:
        if svn_dirs:
            for dir in svn_dirs: shutil.rmtree(dir)

def filter_tree(tree_root_dir):
    foundfilenames=[]
    swapfile=re.compile(r'\.sw.$')
    for dirpath, dirnames, filenames in os.walk(tree_root_dir):
        if '.svn' in dirnames:
            dirnames.remove('.svn')
        for name in filenames:
            path=os.path.join(dirpath,name)
            if name.endswith('~'):
                foundfilenames.append(path)
                continue
            #if not igrep(name,string):   # note: string='' always found with grep in path
            #                        #    -> use that substr to find all!
            #    continue
            if swapfile.search(name) is not None:
                foundfilenames.append(path)
                continue

            if  path.lower().endswith('pyc'):
                foundfilenames.append(path)
                continue

    return foundfilenames

def iswritabledir(path):
    import os
    return  os.path.isdir(path) and os.access(path,os.W_OK)



def args_parsing(args):
    """
      make distro

      args:
        - name       : name of tool
        - version    : version label
        - outputDir : where to place the constructed zipfile (default: <scriptdir>/dist/  )
                       note: ask for overwrite in case file already exists
                       note: don't use tmp as default output dir because should only
                             be used for temporary stuff. The generated file
                             isn't a temporary file which will be removed when
                             the process exits. KISS: only write in  tmp if it gets cleaned up afterwards!!
    """

    scriptdir=os.path.abspath(os.path.dirname(args[0]))
    distdir=os.path.join(scriptdir,'dist')

    argparser = argparse.ArgumentParser(prog=os.path.basename(args[0]))


    argparser.add_argument('toolName', help='name of the tool')
    argparser.add_argument('versionNumber', help='version number')
    argparser.add_argument('outputDir', nargs='?', default=distdir, help='where to place the constructed zipfile (default: <scriptdir>/dist/)')
    parse_result=argparser.parse_args(args[1:])


    name=parse_result.toolName
    version=parse_result.versionNumber


    # make outputdir if not exist
    output_dir=parse_result.outputDir
    output_dir=os.path.abspath(output_dir)   # on windows also interprets / as dir sep, and returns path with windows \
    output_dir.replace('\\','/')
    # - check if we can create the output directory on the fly
    #if os.path.isdir(output_dir):
    #    app_error("output directory '" + output_dir + "' already exists")

    # make sure output_dir is available for writing distfile
    parent_dir=get_parent_dir(output_dir)
    if not iswritabledir(output_dir):  # writeable outputdir exist
        if os.path.isdir(output_dir):
            app_error("output directory '" + output_dir + "' not writable")
        if not iswritabledir(parent_dir):
            app_error("output directory '" + output_dir + "' cannot be created because parent directory is not writable")
        os.mkdir(output_dir)
    return  (name,version,output_dir)

def check_overwrite_file(path):

    if os.path.isfile(path):
        print "\nFile '" + path + "' already exists!"
        answer=raw_input("Do you want to overwrite this file? (y/N)")
        if answer.lower()!='y':
            app_error("stop because not able to write output file")
        os.remove(path)

def print_tree(tree_root_dir,cygwin=False):
    sep=os.path.sep
    for dirpath, dirnames, filenames in os.walk(tree_root_dir):
        # note: by default os.walk doesn't recurse into symbolic links
        #       pointing to directories. Can be turned on with option though.
        #       see : http://docs.python.org/library/os.html#os.walk
        for name in dirnames:         # name give true for os.path.isdir  (note isdir follows symbolic links in check)
            path=os.path.join(dirpath,name)
            if cygwin: path=path.replace('\\',sep)
            if os.path.islink(path):    # note: broken link is seen as file by os.walk
                                        #       so this place never reached by broken link!
                print path + sep + ' -> '
            else:
               print path  + sep

        for name in filenames:       # name gives false for os.path.isdir
            path=os.path.join(dirpath,name)
            if cygwin: path=path.replace('\\',sep)
            if os.path.islink(path):  # only reached when symbolic link is dead or
                                        # points to a special file/socket/pipe
                                        # because otherwise isfile/isdir whould
                                        # already have returned true
                print path + ' -> '
            elif os.path.isfile(path):  # follows symbolic links in check
                                        # returns false for  a special file/socket/pipe/symlink

               print path
            else:
                print 'special: ' + path
                pass # (symlink to) special files, sockets, pipes, or broken sysmlink -> for them use  : http://docs.python.org/library/stat.html

def get_parent_dir(dir):
    return os.path.abspath(os.path.join(dir, os.pardir))
    # in linux os.pardir is '..'

def zipdir(path, zip):
    """
      zips whole directory
      where paths are stored relative from the parent of dir
      e.g zip  /a/b/c dir
          then file /a/b/c/d.txt
          gets path c/d.txt in zip file
    """
    parent_path=get_parent_dir(path)
    for root, dirs, files in os.walk(path):
        relative_root=os.path.relpath(root,parent_path)
        for file in files:
            filepath=os.path.join(root, file)
            stored_filepath_in_zip=os.path.join(relative_root, file)
            zip.write(filepath,stored_filepath_in_zip)

def mkzip_of_dir(newzipfile,dir):
    import zipfile
    zipf = zipfile.ZipFile(newzipfile, 'w')
    zipdir(dir, zipf)
    zipf.close()

def lszip(file):
    import zipfile
    zf = zipfile.ZipFile(file, 'r')
    for f in zf.namelist(): print f

def cleanup(dir):
    rm_svn_subdirs(dir)

    foundfilenames=filter_tree(dir)
    if foundfilenames:
        for name in foundfilenames:
            os.remove(name)

def write_release_file(distdir,version):
    releasefile=distdir +  os.path.sep +  'release-version.txt'
    f=open(releasefile,'w')
    f.write(version+'\n')
    f.close()


# def run_java_program(classpath,mainclass,java_program_args):
#     command=[ 'java', '-cp', classpath, mainclass] + java_program_args
#     print("running command : \n   "  + join_command(command) )
#     run_command(command)

# def compile_java_code(classpath,mainclass,java_program_args):
#     command=[ 'javac', '-cp', classpath, mainclass] + java_program_args
#     print("running command : \n   "  + join_command(command) )
#     run_command(command)

def isreadablefile(path):
    import os
    return  os.path.isfile(path) and os.access(path,os.R_OK)

def app_error(msg,return_code=1):
    print "\n\nAPPLICATION ERROR:\n"
    print msg
    print "\n\n"
    exit(return_code)

def check_isreadablefile(file):
    if  not isreadablefile(file):
        app_error("input file '" + file + "' is not readable")


def get_libraries_in_dir(lib_path):
    file_names = os.listdir(lib_path)
    jar_names = [entry for entry in file_names if entry[-4:].lower() == ".jar"]
    libraries = [ os.path.abspath(os.path.join(lib_path,entry)) for entry in jar_names]
    return libraries
    
def get_tomte_external_libraries(tomte_root_dir):
    lib_path = os.path.join(tomte_root_dir,"lib")
    learnlib_path = os.path.join(lib_path,"learnlib")
    return get_libraries_in_dir(lib_path) + get_libraries_in_dir(learnlib_path)
	
def OLD_get_tomte_external_libraries_classpath(tomte_root_dir):
    tomte_root_dir=os.path.abspath(tomte_root_dir)
    tomte_external_libraries=get_tomte_external_libraries(tomte_root_dir)
    classpath = os.path.pathsep.join(tomte_external_libraries)
    return classpath

def get_tomte_external_libraries_classpath(tomte_root_dir):
    tomte_root_dir=os.path.abspath(tomte_root_dir)
    lib_path = os.path.join(tomte_root_dir,"lib")
    learnlib_path = os.path.join(lib_path,"learnlib")
    # note: <dir>/* in classpath means all jar files in that directory
    classpath = [os.path.join(lib_path,'*'),os.path.join(learnlib_path,'*')]
    classpath = os.path.pathsep.join(classpath)
    return classpath

def join_command(command):
    """
        Joins an command existing of an array of strings in a safe way
        Safe means : if a string in the array contains spaces it will
                     be enclosed with quotes in final string.
    """

    if not isinstance(command,list):
      return command

    strings=[]
    for str in command:
        if ' ' in str:    # put " qoutes arround string
          str=str.replace('"', '\\"')
          strings.append('"'+str+'"')
        else:
          strings.append(str)
    return ' '.join(strings)


def compile_java_source(java_source_path,classpath,main_source_file,java_class_dir):
    """
      compile java source
      into java_class_dir
    """
    command=['javac','-sourcepath',java_source_path, '-cp', classpath,'-d',java_class_dir,main_source_file]
    print("running command : \n   "  + join_command(command) )
    run_command(command)

def create_jar_file(jar_file,java_class_dir):
    """
      create jar file from class files   (not executable)
    """
    command=['jar','-cvf',jar_file,'-C',java_class_dir,'.']
    print("running command : \n   "  + join_command(command) )
    run_command(command)



def run_command(command):
    """
      run command
      if command exits with error
         than this function will also exit the current program with the error
      else
         just returns from this function
    """
    import subprocess
    try:
      process = subprocess.Popen(command)
    except Exception as inst:
      print("problem running command : \n   "  + join_command(command) + "\n problem : " + str(inst))

    process.communicate()  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
       print "problem:  command exited with errorcode " + str(process.returncode)
       sys.exit(process.returncode)

def isreadablefile(path):
    import os
    return  os.path.isfile(path) and os.access(path,os.R_OK)


def main(args):
    """
      make distro

      args:
        - name       : name of tool
        - version    : version label
        - output dir : where to place the constructed zipfile (default: <cwd>/dist/  )
                       note: ask for overwrite in case file already exists
                       note: don't use tmp as default output dir because should only
                             be used for temporary stuff. The generated file
                             isn't a temporary file which will be removed when
                             the process exits. KISS: only write in  tmp if it gets cleaned up afterwards!!
    """
    (name,version,distdir_for_zipdistfile)=args_parsing(args)
    dist= name + "-" + version


    # goal : make distfile
    distfile=os.path.join(distdir_for_zipdistfile, dist + ".zip" )

    #if os.path.isfile(distfile):
    #    app_error("output dist file '" + distfile + "' already exists")
    check_overwrite_file(distfile)



    scriptdir=os.path.abspath(os.path.dirname(args[0]))
    tmpdir=tempfile.mkdtemp()

    # 0. make temporary distdir as subdir of tmpdir
    distdir=tmpdir + os.path.sep + dist
    os.mkdir(distdir)

    # 1. create release file
    write_release_file(distdir,version)

    # 2. copy standard subdirs
    subdirs=["bin","share","lib","config"]
    for subdir in subdirs:
       shutil.copytree(os.path.join(scriptdir,subdir),os.path.join(distdir,subdir))

    # 3. copy "models" subdir for distro
    shutil.copytree(os.path.join(scriptdir,"models"),os.path.join(distdir,"models"))
    # strip none distro models 
    remove_none_distro_subdirectories(os.path.join(distdir,"models"),"TAG_distr")
    # unset cadp verification option in model config
    unset_cadp_verification_in_models_config(os.path.join(distdir,"models"),"config.yaml")


    # cleanup: remove unwanted files (eg .pyc) and dirs (eg. .svn/)
    cleanup(distdir)



    # make <dist>/lib/tomte.jar file from tomte src
    # ----------------------------------------------

    # 0) define some dirs
    source_dir=os.path.join(scriptdir,'src')

    # 1) compile java source into classes
    # - make temporary output dir for compiled classes
    java_class_dir=os.path.join(tmpdir,'bin')
    os.mkdir(java_class_dir)
    # - set source dir and main source file
    main_source_file=os.path.join(source_dir,'abslearning/learner/Main.java')
    java_source_path=source_dir    #java_source_path='src/:sutsrc/'
    # - get classpath of external libraries
    #   note: use external libraries from distdir because these are distributed
    classpath=get_tomte_external_libraries_classpath(distdir)
    # - compile java source into classes
    compile_java_source(java_source_path,classpath,main_source_file,java_class_dir)

    # 2) create jar from classes
    jar_file=os.path.join(distdir,'lib',"tomte.jar")
    create_jar_file(jar_file,java_class_dir)

    # cleanup
    # when jar file is create then we do not need java_class_dir anymore
    shutil.rmtree(java_class_dir)






    # zip distdir to zip file (in dir  distdir_for_zipdistfile)
    mkzip_of_dir(distfile,distdir)


    #print_tree(tmpdir)
    #answer=raw_input("pause")

    # cleanup temporary distdir
    shutil.rmtree(distdir)

    # cleanup tempdir
    shutil.rmtree(tmpdir)



    print "\n\ncreated: "  + distfile
    print "\n\n"

    # lszip(distfile)


if __name__ == "__main__":
     main(sys.argv)
