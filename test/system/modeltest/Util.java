package modeltest;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerRepository;
import org.junit.Assume;

import abslearning.app.Config;
import abslearning.app.Statistics;
import abslearning.verifier.HypDesc;
import abslearning.verifier.HypVerifier;
import abslearning.verifier.VerifierStore;

public class Util {
    private static final Logger logger = Logger.getLogger(Util.class);
    private static final Statistics statistics = Statistics.getInstance();

    public static List<String> verificationMethods;

    public static void overrideConfigOptionsForTesting(Config config) {

        // set output dir for testing
        String tmpDir = "test-output/learn_output/"; //temporary dir we use for unit testing
        config.learnResults_outputDir = tmpDir + "${timestamp}_${sutname}_" + config.testing_seed;

        // maximal testing time
        config.learning_maxTime = 340; // 4*60 seconds = 4 minutes

        // enforce printing stack traces for exceptions
        config.devel_printStackTraceOfExceptions = true;

        // enforce modest logging
        config.logging_learnlibLogLevel = "info";
        config.logging_rootLoggerLevel = "info";
        config.logging_consoleThreshold = "info";
        config.logging_logFileThreshold = "info";
        // alternative:
        //   config.logging_rootLoggerLevel = "OFF"; -> disables logging entirely

        // enforce modest childLoggerLevels
        LinkedHashMap<String, String> childLoggerLevels = new LinkedHashMap<String, String>();
        //childLoggerLevels.put("abslearning", "info");
        config.logging_childLoggerLevels = childLoggerLevels;
    }

    public static Config getConfigForTesting(String referenceDir, long seed) throws FileNotFoundException {

        // read in params needed for learning
        String configFilepath = referenceDir + "config.yaml";
        String[] args = { configFilepath };
        Config config = Config.getInstance();
        config.initFromTomteCmdlineArguments(args); // simply loads params from config file

        // override specific config options which are different for unit testing
        config.testing_seed = seed;
        overrideConfigOptionsForTesting(config);

        // validates and initialize config for Tomte
        config.validate();
        config.initialize();

        return config;
    }

    public static Config learn(String referenceDir, long seed) throws Throwable {
        // init basic logging; later replaced with advanced logging settings using a properties file
        Config.initBasicLogging();

        // get testing config
        Config config = getConfigForTesting(referenceDir, seed);


        // don't do verification during learning test, do in next test
        verificationMethods = config.verification_methods;
        config.verification_methods = Collections.emptyList();

        // do learning
        abslearning.learner.Main.learn(config);
        return config;
    }

    public static void verifyLearnedModel(String referenceDir, Config config) {

        // re-enable verification after learning is done
        config.verification_methods = verificationMethods;
        HypVerifier hypVerifier = VerifierStore.getVerifier(config);

        if (hypVerifier == null) {
            logger.info("\n\n    no verification configured; compareConcreteModels test will be SKIPPED");

            Assume.assumeTrue(false); // makes you skip the test
            //src: http://junit.sourceforge.net/javadoc/org/junit/Assume.html
            //      assumeTrue(boolean b)
            //        If called with an expression evaluating to false, the test will halt and be ignored.
            return; // should be unreachable code
        }

        logger.info("CADP: check hypothesis equal SUT");

        // get learned Model
        HypDesc hypDesc = HypDesc.getDescForLearnedModel(config);

        boolean equal = hypVerifier.verifyHyp(hypDesc);
        String verifierName = hypVerifier.getClass().getSimpleName();

        if (equal) {
            logger.info(verifierName + " verification passed");
        } else {
            logger.fatal(verifierName + " verification failed");
            fail(verifierName + " verification failed");
        }
    }
}
