package abslearning.stores;

import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import abslearning.stores.Event;
import abslearning.stores.ProbabilityManager;

public class TestProbabilityManager {
	
	private static Random rand; 
	
	@BeforeClass
	public static void init() {
		rand = new Random(1);
	}
	
	@Test
	public void test_draw() {
		ProbabilityManager mgr = new ProbabilityManager();
		mgr.addEvent(Event.DRAW_STATEVAR, 50);
		mgr.addEvent(Event.DRAW_CONSTANT, 20);
		mgr.addEvent(Event.DRAW_PREVPARAM, 30);
		
		for (int i=0; i < 100; i++) {
			Event drawn = mgr.drawEvent(rand);
			Assert.assertTrue("Corrent event not drawn " , Arrays.asList(Event.DRAW_CONSTANT, Event.DRAW_PREVPARAM, Event.DRAW_STATEVAR).contains(drawn));
		}
	}
	
	
	@Test
	public void test_management() {
		ProbabilityManager actual = new ProbabilityManager();
		actual.addEvent(Event.DRAW_STATEVAR, 20);
		actual.addEvent(Event.DRAW_CONSTANT, 20);
		actual.addEvent(Event.DRAW_PREVPARAM, 60);
		actual.removeEvent(Event.DRAW_PREVPARAM);
		ProbabilityManager expected = new ProbabilityManager();
		expected.addEvent(Event.DRAW_STATEVAR, (20*100)/40);
		expected.addEvent(Event.DRAW_CONSTANT, (20*100)/40);
		Assert.assertTrue(actual.matches(expected));
		actual.removeEvent(Event.DRAW_CONSTANT);
		expected = new ProbabilityManager();
		expected.addEvent(Event.DRAW_STATEVAR, 100);
		Assert.assertTrue(actual.matches(expected));
	}
	
}
