package abslearning.relation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;

import abslearning.ceanalysis.graph.ParameterNode;
import abslearning.ceanalysis.graph.RelationEdge;
import abslearning.ceanalysis.graph.RelationGraph;
import abslearning.ceanalysis.graph.TraceParameterNode;

import org.junit.Assert;

public class TestRelationGraph {

	@Test
	public void test_shortestPath() {
		Relation.activateRelations(Arrays.asList(Relation.SUCC.toString()));
		RelationGraph relationGraph = new RelationGraph();
		TraceParameterNode p1 = new TraceParameterNode (10, 0,0);
		relationGraph.addNode(p1);
		relationGraph.addNode(new TraceParameterNode (11, 0,0));
		relationGraph.addNode(new TraceParameterNode (12, 0,1));
		relationGraph.addNode(new TraceParameterNode (10, 0,2));
		relationGraph.addNode(new TraceParameterNode (11, 0,3));
		TraceParameterNode p3 = new TraceParameterNode (12, 0,4);
		relationGraph.addNode(p3);
		TraceParameterNode p2 = new TraceParameterNode (13, 0,5);
		relationGraph.addNode(p2);
		
		LinkedList<RelationEdge<TraceParameterNode>> shortestPath = relationGraph.getShortestPath(p1, p2);
		List<Integer> values = getValues(shortestPath);
		check(Arrays.asList(11,12,13), values);
		 shortestPath = relationGraph.getShortestPath(p3, p2);
		check(Arrays.asList(13), getValues(shortestPath));
		shortestPath = relationGraph.getShortestPath(p2, p3);
		check(new LinkedList<Integer>(), getValues(shortestPath));
	}
	
	@Test
	public void test_maintainConnectivity() {
		Relation.activateRelations(Arrays.asList(Relation.SUCC.toString()));
		RelationGraph relationGraph = new RelationGraph();
		relationGraph.maintainConnectivity(true);
		relationGraph.addNode(new TraceParameterNode (11, 0,0));
		relationGraph.addNode(new TraceParameterNode (12, 0,1));
		relationGraph.addNode(new TraceParameterNode (15, 0,2));
		check(Arrays.asList(11,12), getValues(relationGraph));
	}
	
	private List<Integer> getValues(RelationGraph graph) {
		final List<Integer> values = new ArrayList<Integer>();
		graph.getNodes().forEach(new Consumer<TraceParameterNode>(){
			public void accept(TraceParameterNode t) {
				values.add(t.getContent());
			}
		});
		return values;
	}

	private List<Integer> getValues(LinkedList<RelationEdge<TraceParameterNode>>shortestPath) {
		LinkedList<Integer> values = new LinkedList<Integer>();
		if (shortestPath != null)
		for (RelationEdge<TraceParameterNode> relationEdge : shortestPath) {
			values.add(relationEdge.endNode.getContent());
		}
		return values;
	}
	
	private void check(List<Integer> expected, List<Integer> actual) {
		Assert.assertArrayEquals(expected.toArray(new Integer [expected.size()]),actual.toArray(new Integer [actual.size()]));
	}
}
