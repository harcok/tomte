package abslearning.determinizer;

import java.util.Collections;

import org.junit.BeforeClass;
import org.junit.Test;

import abslearning.determinizer.mapper.ValueMapperProvider;
import abslearning.stores.ConfigurableValueStoreProvider;
import abslearning.trace.Trace;
import abslearning.trace.TraceBuilder;
import abslearning.trace.action.Action;

public class TestDeterminizer {
	
	@BeforeClass
	public static void init() {
		
	}
	
	@Test
	public void test1() {
		Determinizer determinizer =new Determinizer(Collections.emptyList(), ValueMapperProvider.DUALFORREST,  ConfigurableValueStoreProvider.FRESH, DiscriminatorProvider.SYSTEM );
		Trace testTrace = TraceBuilder.buildTrace("IRegister_-1(293) / OOK_?(-10) -> IRegister_0(293) / ONOK_?() -> IRegister_0(293) / ONOK_?() -> ILogout_-1(127) / ONOK_?() -> ILogout_-1(293) / ONOK_?() -> ILogin_-1_-1(127,293) / ONOK_?() -> ILogin_-1_-1(293,-10) / OOK_?(-110)");
		determinizeTrace(testTrace, determinizer);
		// pass
	}
	
	
	private Trace determinizeTrace(Trace trace, Determinizer determinizer) {
		Trace detTrace = new Trace();
		for(int index = 0; index <= trace.size(); index = index+2) {
			Action input = trace.get(index);
			detTrace.add(determinizer.decanonize(input));
			Action output = trace.get(index + 1);
			detTrace.add(determinizer.canonize(output));
		}
		return detTrace;
	}
}
