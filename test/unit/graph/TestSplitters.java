package graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import abslearning.ceanalysis.graph.RelationEdge;
import abslearning.ceanalysis.graph.RelationGraph;
import abslearning.ceanalysis.graph.TraceParameterNode;
import abslearning.ceanalysis.graph.TraceRelationEdge;
import abslearning.relation.Relation;
import abslearning.stores.StepWiseValueStore;
import org.junit.Assert;

public class TestSplitters {
	private RelationGraph graph;
	
	@BeforeClass
	public static void init() {
		StepWiseValueStore.setDefaultStep(1);
		Relation.activateRelations(Arrays.asList("EQUAL","SUCC"));
	}
	
	@Before
	public void testInit() {
		this.graph = mockGraph();
	}
	
	@Test
	public void testSplit() {
		int numSplits = 0;
		for (Split<TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph> split : graph.splits(3)) {
			System.out.println(split.first.getNodes() + " " + split.second.getNodes() + " \n\n" );
			numSplits ++;
		}
		checkSplitCount(4, numSplits);
	}
	
	@Test 
	public void testScalable() {
		RelationGraph graph = buildScalableGraph(21, 10, 17, Relation.SUCC);
		int numSplits = 0;
		for (Split<TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph> split : graph.connectedSplits(3)) {
			Assert.assertTrue(split.first.isConnected() && split.second.isConnected());
			numSplits ++;
		}
		System.out.println("Covered "+ numSplits + " splits");
	}
	
	@Test
	public void testConnectedSplit() {
		int numSplits = 0;
		for (Split<TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph> split : graph.connectedSplits(3)) {
			Assert.assertTrue(split.first.isConnected() && split.second.isConnected());
			numSplits ++;
		}
		System.out.println(numSplits);
		checkSplitCount(3, numSplits);
	}
	
	private void checkSplitCount(int expected, int actual) {
		Assert.assertTrue("Expected "+ expected + " splits, got " + actual, expected == actual);
	}
	
	private RelationGraph mockGraph() {
		RelationGraph relationGraph = new RelationGraph();
		relationGraph.addNode(new TraceParameterNode (10, 0,0));
		relationGraph.addNode(new TraceParameterNode (11, 0,1));
		relationGraph.addNode(new TraceParameterNode (12, 0,2));
		relationGraph.addNode(new TraceParameterNode (10, 0,3));
		relationGraph.addNode(new TraceParameterNode (11, 0,4));
		relationGraph.addNode(new TraceParameterNode (12, 0,5));
		return relationGraph;
	}
	
	/**
	 * Builds a graph with {@code numNodes} nodes where starting from a root with value  
	 * {@code startingValue} each subsequent node added contains either a value equal to or an application
	 * of the relation of the previous node. This is done such that the number of applications from root is evenly distributed
	 * across all nodes.
	 * </br>
	 * 
	 */
	private RelationGraph buildScalableGraph(int numNodes, int startingValue, int numApplications, Relation relation) {
		RelationGraph relationGraph = new RelationGraph();
		int nodesPerApplication = numNodes / (numApplications + 1);
		if (numApplications % (numApplications + 1) != 0) {
			nodesPerApplication ++; // there is no Math.divCeil function
		}
		int nodeValue = startingValue;
		int nodeIndex = 0;
		for (int applicationIndex = 0; applicationIndex <= numApplications; applicationIndex ++) {
			if (nodesPerApplication > numNodes - nodeIndex) {
				nodesPerApplication = numNodes - nodeIndex;
			}
			for(int i = 0; i < nodesPerApplication; i ++)
				relationGraph.addNode(new TraceParameterNode(nodeValue, 0, nodeIndex ++));
			nodeValue = relation.map(nodeValue);
		}
		
		return relationGraph;
	}
	
	
	
	private List<Integer> getNodeValueList(RelationGraph graph) {
		final List<Integer> valueList = new ArrayList<Integer>();
		graph.getNodes().forEach(new Consumer<TraceParameterNode>() {
			public void accept(TraceParameterNode t) {
				valueList.add(t.getContent());
			}
		});
		return valueList;
	}
	
}
