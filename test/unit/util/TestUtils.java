package util;

import java.util.List;

import org.junit.Assert;

import abslearning.trace.Trace;

public class TestUtils {
	public static void checkTrace(Trace actualTrace, Trace expectedTrace) {
		List<Integer> concreteInputValuesInActual = actualTrace.getAllInputIntegerValues();
		List<Integer> concreteInputValuesInExpected = expectedTrace.getAllInputIntegerValues();
		Assert.assertTrue(concreteInputValuesInActual.equals(concreteInputValuesInExpected));
		List<Integer> concreteOutputValuesInActual = actualTrace.getAllOutputIntegerValues();
		List<Integer> concreteOutputValuesInExpected = expectedTrace.getAllOutputIntegerValues();
		String msg = "expected:" + expectedTrace + "\n" +"got:" + actualTrace;
		Assert.assertTrue(msg, concreteOutputValuesInActual.equals(concreteOutputValuesInExpected));
	} 
}
