Tomte is currently undergoing restructuring in which mayor changes will take
place in base code and structural organization. 

We do not give support to the legacy code in the repository. However soon we will 
release a new improved version for which we will give support.

Tomte's homepage is http://tomte.cs.ru.nl/  where you can find documentation,
downloads, and more related information.

The master branch contains the current stable version of Tomte. ( above release 0.41)

There are branches for the latest releases of tomte:
* release-0.41  
* release-0.4  
* release0.3 

