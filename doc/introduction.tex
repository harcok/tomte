\chapter{Introduction}


\section{Tomte Tool}

Tomte is a tool that fully automatically constructs abstractions for
automata learning. Usually, a component implementing the learning
algorithm (the learner) is directly connected to the SUT (the teacher).
By observing how the SUT responds to queries sent by the learner, a
model of the behavior of the SUT can be constructed. This is not enough
for learning models of realistic software components which, due to the
presence of program variables and data parameters in messages, typically
have much larger state spaces.

This inspired us to build the Tomte tool, which is placed in between the
SUT and the learner, and transforms the large set of actions of the SUT
into a small set of abstract actions that can be handled by the learner,
see figure~\ref{fig:system}. This means the inference is performed on a small alphabet
and once the learning is done the abstract model is combined with the
abstraction to construct a concrete version of the SUT. This gave us the
idea for the name of the tool. Tomte is the creature that shrank Nils
Holgersson into a gnome and (after numerous adventures) changed him back
to his normal size again.



\begin{figure}[H]
	\begin{centering}
		\includegraphics[scale=0.3]{Tomte_structure.png}
		\par\end{centering}
	\caption{Architecture of the learning frameworkt}
	\label{fig:system}
\end{figure}




We use \href{http://www.learnlib.de/}{LearnLib} as our basic learning
tool and therefore the abstraction of the SUT may not exhibit any
nondeterminism: if it does then LearnLib crashes and we have to refine
the abstraction. This is exactly what is implemented in the Tomte tool.
As learner also other inference algorithms can be used as long as they
detect nondeterministic behavior.

The current version of Tomte, Tomte-0.41, can learn register automata
with fresh output values. Using Tomte we have succeeded to learn - fully
automatically - models of several realistic software components,
including the biometric passport and the SIP protocol.

Tomte comes together with the SUT tool which supports simulating an SUT
from a state machine model, which is convenient for trying out the tool
on known models before applying it to learn real systems. The Tomte
installation contains a set of models we used in our experiments can be
found in the .zip file in the
\href{http://tomte.cs.ru.nl/Downloads/Description}{Downloads} page. If
you want to simulate your own model, please study the
\href{http://tomte.cs.ru.nl/Sut-0-4/MakingSut}{Making SUT} page. Note
that we can only learn a restricted class of extended finite state
machines (EFSM), i.e. Register Automata (also known as Scalarset Mealy
machines). For details on the restrictions, please study the
\href{http://tomte.cs.ru.nl/Sut-0-4/MakingSut}{Making SUT} page and our
paper
\href{http://www.mbsd.cs.ru.nl/publications/papers/fvaan/CEGAR12/}{"Automata
	Learning Through Counterexample-Guided Abstraction Refinement"}
{[}CEGAR12{]} and the phd thesis of
\href{http://repository.ubn.ru.nl/handle/2066/130428}{Fides Aarts
	"Tomte: Bridging the Gap between Active Learning and Real-world
	Systems"}



\section{Usage}

\hypertarget{usage}{%
	\section[Usage]{\texorpdfstring{Usage\protect\hypertarget{Usage}{}{}}{Usage}}\label{usage}}

\hypertarget{prerequisite}{%
	\subsection[Prerequisite :]{\texorpdfstring{Prerequisite
			:\protect\hypertarget{Prerequisite:}{}{}}{Prerequisite :}}\label{prerequisite}}

Before proceeding with this tutorial, make sure you have installed both
Tomte and the Sut tool.
Both tools are compatible with Windows, Linux and Mac.
Check the corresponding install pages for
\href{http://tomte.cs.ru.nl/Tomte-0-4/Install}{Tomte} and the
\href{http://tomte.cs.ru.nl/Sut-0-4/Install}{Sut tool} respectively.

\hypertarget{three-steps}{%
	\subsection[Three steps :]{\texorpdfstring{Three steps
			:\protect\hypertarget{Threesteps:}{}{}}{Three steps :}}\label{three-steps}}

\begin{itemize}
	\tightlist
	\item Step 1: run the SUT
	\item Step 2: learn the SUT with Tomte
	\item Step 3: check equivalence learned model with teacher model
\end{itemize}


\subsection{ Step 1: run the SUT}



You can use any SUT which can communicate over tcp/ip network sockets.
To demonstrate Tomte we use the
\href{http://tomte.cs.ru.nl/Sut-0-4/Description}{Sut tool}. With the Sut
tool you can easily generate an implementation of an SUT from an EFSM
model specified in Uppaal.

The Tomte tool comes with some predefined models in the
\texttt{tomte-0.41/models/} subdirectory.
If you want to learn your own model, please study the
\href{http://tomte.cs.ru.nl/Sut-0-4/MakingSut}{Making SUT} section.

For example let us use the \emph{BiometricPassport} uppaal model as
teacher model in our learning experiment and run it on the fly as an SUT
using the command:

\begin{centerverbatim}
> sut_run tomte-0.41/models/BiometricPassport/model.xml --port 9999

SUT simulation socketserver
-> listening at port : 9999
-> verbose mode : OFF
-> the server has a timeout of 30 seconds
note: to prevent unnecessary servers to keep on running
\end{centerverbatim}

Now the SUT is running and waiting for anyone to communicate with it!
For more details about the Sut tool see the
\href{http://tomte.cs.ru.nl/Sut-0-4/Usage}{Sut tool usage} page.

\subsection{Step 2: learn the SUT with Tomte}

Run the 'tomte\_learn' command with as argument a configuration file
which specifies learning parameters used.
For a quick run of tomte, you can opt to learn one of models in the
\emph{models} directory.
Assuming we want Tomte to learn the \emph{BiometricPassport} system, as
run in the previous section,
we use the command:

\begin{centerverbatim}
> tomte_learn tomte-0.41/models/BiometricPassport/config.yaml --port 9999
...
\end{centerverbatim}

Similarly, we can test Tomte on any of the models provided in the
\emph{models} directory. Just run the Sut tool
on the model's \emph{model.xml} specification, then launch Tomte using
the model's \emph{config.yaml} file.

When running without arguments 'tomte\_learn' will show you its usage
message:

\begin{centerverbatim}
> tomte_learn
tomte_learn: error: too few arguments
usage: tomte_learn [-h] [--seed SEED] [--max-memory MAX_MEMORY]
                   [--output-dir OUTPUT_DIR] [--port PORT] [-e]
                   [--config-option CONFIG_OPTION]
                   configfile
Try 'tomte_learn --help' for more information.
\end{centerverbatim}

When running with the \emph{-h} commandline option it will show you a
more detailed help message:

\begin{centerverbatim}
> tomte_learn -h
usage: tomte_learn [-h] [--seed SEED] [--max-memory MAX_MEMORY]
                   [--output-dir OUTPUT_DIR] [--port PORT] [-e]
                   [--config-option CONFIG_OPTION]
                   configfile

positional arguments:
  configfile            learner configuration file

optional arguments:
  -h, --help            show this help message and exit
  --seed SEED           Seed to use for random number generator.
  --max-memory MAX_MEMORY
                        Maximum memory to use by virtual machine in GB
                        (default:2GB)
  --output-dir OUTPUT_DIR
                        Directory to store output.
  --port PORT           tcp port to listen on for incoming connections
  --config-option CONFIG_OPTION
                        Overrule a configuration option in config.yaml file.
\end{centerverbatim}

Before we can run Tomte, we must first make a configuration file, which
specifies learning parameters as well as the system to be learned. There are many
configurable parameters, we will only touch on the most important. Below is a simple
configuration file:

\begin{centerverbatim}
learning:
sutinfoFile: "sutinfo.yaml"
seed: 1299777356020

testing:
minValue: 256
maxValue: 511
minTraceLength: 100
maxTraceLength: 100
maxNumTraces: 1000
resetProbability: 0.05
\end{centerverbatim}

The learning section specifies the parameters that are needed for
generating an hypothesis during learning. Explanation of the learning parameters :

\begin{itemize}
	\tightlist
	\item
	sutInfoFile: for learning we need to know the alphabet of the SUT,
	which is specified in this file. For convenience the name of the SUT
	is also specified here.
	\item
	seed: we use pseudo randomness during learning using this seed.
	Consequently, experiments run on the same seed have the same outcome.
\end{itemize}

The testing section specifies the parameters needed for testing an
hypothesis against the SUT. In Tomte we use random walk testing
approach, 
and the parameters here specify the kind and number of test traces we
use to test the hypothesis.

\begin{itemize}
	\tightlist
	\item
	minValue and maxValue: specify the range from which input values are
	chosen from during testing.
	\item
	minTraceLength and maxTraceLength: specify the size for the test
	traces during testing. Currently only maxTraceLength is used during
	testing.
	\item
	resetProbability: a reset probability of 0.05 means that after each
	randomly drawn test input you get 5 percent change to end the test
	trace earlier. Using this parameter cause a variation in test traces
	size.
\end{itemize}

The \emph{sutinfoFile} can also be generated from the teacher model
using the \emph{sut\_uppaal2sutinfo} command. For the
\emph{BiometricPassport} model as teacher in our learning experiment the
configuration file ( \emph{sutinfo.yaml}) can be generated as:

\begin{centerverbatim}
> cd tomte-0.41/models/BiometricPassport/
> sut_uppaal2sutinfo model.xml sutinfo.yaml
\end{centerverbatim}

The \emph{sutinfo.yaml} file contents is:

\begin{centerverbatim}
constants:
- 257
- 258
- 259
inputInterfaces:
IAA: []
ICA: []
ICompleteBAC: []
IFailBAC: []
IFailEAC: []
IGetChallenge: []
IReadFile:
- Integer
IReset: []
ITA: []
name: BiometricPassport
outputInterfaces:
ONOK: []
OOK: []
\end{centerverbatim}

The \emph{sutinfo.yaml} file describes some information about the SUT
you are allowed to know :

\begin{itemize}
	\tightlist
	\item
	input interfaces of SUT
	\item
	output interfaces of SUT
	\item
	name of SUT
\end{itemize}

For more details please study the
\href{http://tomte.cs.ru.nl/Sut-0-4/MakingSut}{Making SUT} section.

With our \emph{config.yaml} and \emph{sutinfo.yaml} files ready we can
start learning the sut.\\
An example run:\\

\begin{centerverbatim}
> tomte_learn config.yaml --port 9999
...
...
\end{centerverbatim}

That's it. You now learned the model of the SUT!\\
The results of learning are put in the default \emph{output/} directory
:

\begin{itemize}
	\tightlist
	\item
	the learned model
	\item
	statistics of the learning process
\end{itemize}

Just try it yourself!! 

Notes:

\begin{itemize}
	\tightlist
	\item
	Results are saved by default in the folder 'output'
	\item
	When you have the python model pygraphviz install, then you can
	generate a pdf file of the learned concrete uppaal model with the
	following command :
\end{itemize}

\begin{centerverbatim}
> sut_uppaal2layoutformat learnedConcreteModel.xml learnedConcreteModel.pdf
\end{centerverbatim}

\begin{itemize}
	\tightlist
	\item
	With the -\/-port commandline option you can specify to use a different
	tcp/ip port. By default port 7892 is used.
	\item
	All parameters for learning are configured in the config file, however
	some configuration parameters can also be given as a commandline
	parameter which then overrules the setting in the configuration file.
	An example is the \emph{-\/-port} option we used.
	\item
	If no SUT is listening on the other side ot the network socket then
	Tomte will quit with an error.
\end{itemize}

\hypertarget{step-3-check-equivalence-learned-model-with-teacher-model}{%
	\subsection[Step 3: check equivalence learned model with teacher
	model]{\texorpdfstring{\protect\hypertarget{step3}{}{}Step 3: check
			equivalence learned model with teacher
			model\protect\hypertarget{ux5ctextlessux7bux7daux20name=ux27step3ux27ux20id=ux27step3ux27ux5ctextgreaterux7bux7dux5ctextlessux7bux7dux2faux5ctextgreaterux7bux7dStep3:checkequivalencelearnedmodelwithteachermodel}{}{}}{Step 3: check equivalence learned model with teacher model}}\label{step-3-check-equivalence-learned-model-with-teacher-model}}

For equivalence checking the
\href{http://tomte.cs.ru.nl/Sut-0-4/Description}{Sut tool} and
\href{http://www.inrialpes.fr/vasy/cadp/}{CADP tool set} is needed.\\
The only requirement for the Sut tool to work with the CADP toolset is
that its cmdline tools are available through the system PATH.\\
Setup:

\begin{itemize}
	\tightlist
	\item
	install the \href{http://tomte.cs.ru.nl/Sut-0-4/Description}{Sut tool}
	\item
	install the \href{http://www.inrialpes.fr/vasy/cadp/}{CADP tool set}
	and put into your \textasciitilde{}/.bashrc file the following lines:
\end{itemize}

\begin{centerverbatim}
export CADP=/usr/local/cadp/
export CADP_ARCH=`"$CADP"/com/arch`
export PATH="$PATH:$CADP/bin.$CADP_ARCH:$CADP/com"
export CADP_LANGUAGE=english
\end{centerverbatim}

To compare two models we then can just use the \emph{sut\_compare}
command which internally uses the
\href{http://www.inrialpes.fr/vasy/cadp/}{CADP tool set}.

Thus we can do equivalence checking as follows:

\begin{centerverbatim}
> sut_compare model1.xml model2.xml 0 1
...
TRUE or FALSE with a trace
\end{centerverbatim}

The equivalence prints TRUE in case of equivalence, otherwise it prints
FALSE followed by the trace showing the difference between the models.

The parameters 0 and 1 specify the range used for bisimulation checking.
This range is needed when the learned Extended Finite State Machine
(EFSM)\\
is transformed into a LOTOS specification by means of the
\href{http://www.inrialpes.fr/vasy/cadp/}{CADP}\href{http://www.inrialpes.fr/vasy/cadp/}{tool
	set}. We have to limit the range of data parameters in order to check
the equivalence in a limited amount of time.

Notes:

\begin{itemize}
	\tightlist
	\item
	Also different equivalences can be checked with the CADP toolset. The
	CADP equivalence type 'strong' is used by default .
\end{itemize}