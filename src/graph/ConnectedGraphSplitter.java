package graph;

import java.util.Iterator;

/**
 * Also generates splits for a dag graph from the node at the given index, but this
 * time only splits in which both subgraph components are connected are returned.
 */
public class ConnectedGraphSplitter<N, E extends Edge<N>, G extends AbstractGraph<N,E,G>>  implements Iterator<Split<N,E,G>>{

	private Split<N,E,G> nextSplit = null; 
	private BasicGraphSplitter<N,E,G> basicSplitter = null;
	
	public ConnectedGraphSplitter(G graphToSplit, int fromNodeIndex) {
		basicSplitter = new BasicGraphSplitter<N,E,G> (graphToSplit, fromNodeIndex);
	}
	
	public boolean hasNext() {
		if (nextSplit != null) {
			return true;
		} 
		if (!basicSplitter.hasNext()) {
			return false;
		} else {
			Split<N,E,G> nextSplit = basicSplitter.next();
			Split<N,E,G> goodSplit = null;
			while (true) {
				if (nextSplit.first.isConnected() && nextSplit.second.isConnected()) {
					goodSplit = nextSplit;
					break;
				}
				if (basicSplitter.hasNext())
					nextSplit = basicSplitter.next();
				else 
					break;
			} 
			
			this.nextSplit = goodSplit;
			return goodSplit != null;
		}
	}
	
	public Split<N,E,G> next() {
		if (this.nextSplit != null) {
			Split<N, E, G> split = this.nextSplit;
			this.nextSplit = null;
			return split;
		} else {
			if (hasNext()) {
				return this.nextSplit;
			} else {
				return null;
			}
		}
	}
}
