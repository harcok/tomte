package graph;

import java.util.LinkedHashMap;
import java.util.Set;

public class Graph <N,E extends Edge<N>> extends AbstractGraph<N, E, Graph<N,E>>{

	public Graph(LinkedHashMap<N, Set<E>> adjacencyList,EdgeFactory<N, E> edgeFactory) {
		super(adjacencyList,edgeFactory);
	}

	protected Graph<N,E> buildNew(LinkedHashMap<N, Set<E>> adjacencyList2, EdgeFactory<N, E> edgeFactory) {
		return new Graph<N,E>(adjacencyList2, edgeFactory);
	}
}
