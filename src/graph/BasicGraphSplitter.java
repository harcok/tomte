package graph;

import java.util.Iterator;

/**
 * Splits a dag graph in two with one of the subgraphs rooted in the node
 * at the given index. It follows that all nodes before this node are included
 * in the other subgraph.
 * 
 * The class generates all possible splits.  
 */
public class BasicGraphSplitter <N,E extends Edge<N>,G extends AbstractGraph<N,E,G>> implements Iterator<Split<N,E,G>>{
	private final G graph;
	private final long max;
	private final int refNodeIndex;
	private long bitWord;
	
	
	public BasicGraphSplitter (G graphToSplit, int refNodeIndex) {
		this.graph = graphToSplit;
		this.refNodeIndex = refNodeIndex;
		int numOfSplitParams = graphToSplit.size() - refNodeIndex - 1;
		this.max = 1 << numOfSplitParams; // max encodes the split for nodes after the refNode
		this.bitWord = 0;
	}
	

	public boolean hasNext() {
		return bitWord < max;
	}
	
	public Split<N,E,G> next() {
		if(hasNext()) {
			long splitWord = bitWord; // split configuration after ref node
			splitWord = (splitWord << 1) + 1; // ref node is always in one subgraph 
			if (refNodeIndex > 0)
				splitWord = splitWord << refNodeIndex; // while all the preceding nodes are in the other
			Split<N,E,G> split = graph.split(splitWord);
			bitWord ++; // increment bitword
			return split;
		}
		return null;
	}
	
}
