package graph.content;

public interface ContentNode <C> {
	public void setContent(C content);
	public C getContent();
 }
