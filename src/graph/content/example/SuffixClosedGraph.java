package graph.content.example;

import java.util.LinkedHashMap;
import java.util.Set;

import graph.EdgeFactory;
import graph.content.ContentDagGraph;

public class SuffixClosedGraph extends ContentDagGraph<String, StringNode, ConcatEdge, SuffixClosedGraph> {

	public SuffixClosedGraph() {
		super(new ConcatEdgeFactory());
	}
	
	public SuffixClosedGraph(LinkedHashMap<StringNode, Set<ConcatEdge>> adjacencyList2,
			EdgeFactory<StringNode, ConcatEdge> edgeFactory2) {
		super(adjacencyList2, edgeFactory2);
	}

	protected SuffixClosedGraph buildNew(LinkedHashMap<StringNode, Set<ConcatEdge>> adjacencyList2,
			EdgeFactory<StringNode, ConcatEdge> edgeFactory2) {
		return new SuffixClosedGraph(adjacencyList2, edgeFactory2);
	}

}
