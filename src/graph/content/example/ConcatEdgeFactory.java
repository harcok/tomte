package graph.content.example;

import graph.EdgeFactory;

public class ConcatEdgeFactory implements EdgeFactory<StringNode, ConcatEdge> {

	@Override
	public ConcatEdge buildEdge(StringNode fromNode, StringNode toNode) {
		if (toNode.getContent().startsWith(fromNode.getContent())) {
			System.out.println("Connecting " + fromNode + " to " + toNode);
			String suffix = toNode.getContent().substring(fromNode.getContent().length());
			return new ConcatEdge(fromNode, toNode, suffix);
		}
		return null;
	}

}
