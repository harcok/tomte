package graph.content.example;

import graph.content.ProcessingEdge;

public class ConcatEdge extends ProcessingEdge<String, StringNode> {

	private String concatString;

	public ConcatEdge(StringNode startNode, StringNode endNode, String concatString) {
		super(startNode, endNode);
		this.concatString = concatString;
	}

	@Override
	public String processContent(String content) {
		return content + concatString;
	}

	public String toString() {
		return startNode + " - +" + concatString + " -> " + endNode; 
	}
}
