package graph.content;

import java.util.LinkedHashMap;
import java.util.Set;
import java.util.function.Consumer;

import graph.AbstractGraph;
import graph.DagBreadthFirstEdgeIterator;
import graph.EdgeFactory;


/**
 * A direct acyclic graph where each node encapsulates content
 * and each edge implements a processing over this content. The
 * content of the start node of an edge is processed, 
 * the resulting content is then pushed to the end node.
 * 
 * @param <C> content type
 * @param <N> the node type
 * @param <E> the edge type
 * @param <G> the actual graph type
 */
public abstract class ContentDagGraph<C, N extends ContentNode<C>, E extends ProcessingEdge<C,N>, G extends ContentDagGraph<C,N,E,G>> 
extends AbstractGraph<N,E,G>{

	/**
	 * Builds a shallow copy of a graph.
	 */
	public ContentDagGraph(G graph) {
		super(graph);
	}
	
	public ContentDagGraph(EdgeFactory<N,E> edgeFactory) {
		super(edgeFactory);
	}
	
	public ContentDagGraph(LinkedHashMap<N, Set<E>> adjacencyList2, EdgeFactory<N, E> edgeFactory2) {
		super(adjacencyList2, edgeFactory2);
	}
	
	
	/**
	 * Adds a node to the graph, plus edges connecting all other nodes to this node.
	 * Edges are not added if the {@code edgeFactory} cannot make an edge connecting 
	 * the two nodes.
	 */
	public boolean addNode(final N node) {
		boolean success = super.addNode(node);
		if (success) {
			_getNodes().forEach(new Consumer<N>(){
				public void accept(N crtNode) {
					if (!node.equals(crtNode)) {
						addEdge(crtNode,node);
					}
				}});
		}
		
		return success;
	}
	
	/**
	 * Sets the content of the root node to {@code newContent} and starts a processing
	 * chain of all edges. 
	 */
	public void updateContent( C newContent) {
		updateContent(super.getRoot(), newContent);
	}
	
	/**
	 * Sets the content of {@code node} to {@code newContent} and starts a processing
	 * chain of all edges from this node. 
	 */
	protected void updateContent(N node, C newContent) {
		node.setContent(newContent);
		DagBreadthFirstEdgeIterator<N, E, G> iterator = new DagBreadthFirstEdgeIterator<N,E, G>(this, node);
		while (iterator.hasNext()) {
			E edge = iterator.next();
			edge.update();
		}
	}

}
