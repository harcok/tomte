package graph.content;

public interface ContentProcessor<C> {
	public C processContent(C content);
}
