package graph;

public class Split<N,E extends Edge<N>, G extends AbstractGraph<N,E,G>> {
	public Split(G first, G second) {
		super();
		this.first = first;
		this.second = second;
	}
	public final G first;
	public final G second;
	
	public String toString() {
		return " first:" + first.getNodes() + "\n " + " second:" + second.getNodes(); 
	}
}
