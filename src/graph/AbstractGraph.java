package graph;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Predicate;

import abslearning.ceanalysis.graph.TraceParameterNode;
import abslearning.exceptions.BugException;

/**
 * A configurable abstract graph providing basic implementation for connectivity and splitting.
 * Subclasses extending this class should implement the {@code buildNew} builder methods.
 * 
 * @param <N> the node type
 * @param <E> the edge type
 * @param <G> the actual graph type. This is needed so that all methods returning graphs
 * return them in their actual type. ( and not in the abstract type of this class)
 */
public abstract class AbstractGraph <N, E extends Edge<N>, G extends AbstractGraph<N,E,G>>{
	
	protected LinkedHashMap<N, Set<E>> adjacencyList;
	protected EdgeFactory<N,E> edgeFactory;  
	
	
	// builds a shallow copy
	protected AbstractGraph(G graph) {
		this.adjacencyList = graph.adjacencyList;
		this.edgeFactory = graph.edgeFactory;
	}
	
	public AbstractGraph(EdgeFactory<N,E> edgeFactory) {
		super();
		this.adjacencyList = new LinkedHashMap<N, Set<E>>();
		this.edgeFactory = edgeFactory;
	}

	public AbstractGraph(LinkedHashMap<N, Set<E>> adjacencyList, EdgeFactory<N,E> edgeFactory) {
		super();
		this.adjacencyList = adjacencyList;
		this.edgeFactory = edgeFactory;
	}
	
	/**
	 * Returns a deep copy of the graph.
	 */
	public G clone() {
		LinkedHashMap<N, Set<E>> adjacencyList = new LinkedHashMap<N, Set<E>>();
		Iterator<Entry<N, Set<E>>> iterator = this.adjacencyList.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<N, Set<E>> next = iterator.next();
			adjacencyList.put(next.getKey(), new LinkedHashSet<E>(next.getValue()));
		}
		return buildNew (adjacencyList, this.edgeFactory);
	}
	
	/**
	 * Adds node to the graph with no edges connected.
	 * @return true if node added, false if node already exists
	 */
	public boolean addNode(N node) {
		boolean success = false;
		if (!containsNode(node)) {
			adjacencyList.put(node, new LinkedHashSet<E>());
			success = true;
		}
		return success;
	}
	
	
	/**
	 * Removes a node and all its associated edges.
	 * @return true if node removed, false if node wasn't found in graph.
	 */
	public boolean removeNode(final N node) {
		boolean success = false;
		if (containsNode(node)) {
			adjacencyList.remove(node);
			for (Set<E> connectedEdges : this.adjacencyList.values()) {
				connectedEdges.removeIf(new Predicate<E>() {
					public boolean test(E t) {
						return t.endNode.equals(node);
					}});
			}
			success = true;
		}
		return success;
	}
	
	/**
	 * Adds an edge using the edge factory. No edge is added if one of the nodes is not 
	 * in the graph or if the {@code edgeFactory} cannot build this edge.
	 * 
	 * @return true if an edge between nodes was successfully added  <br/> false otherwise
	 */
	public boolean addEdge(N fromNode, N toNode) {
		boolean success = false;
		if (containsNode(fromNode) && containsNode(toNode)) {
			E newEdge = edgeFactory.buildEdge(fromNode, toNode);
			if (newEdge != null) {
				Set<E> fromNodeEdges = adjacencyList.get(fromNode);
				fromNodeEdges.add(newEdge);
				success = true;
			}
		}
		return success;
	}
	
	protected boolean canConnectNode(N externalNode) {
		for(N node : _getNodes()) {
			if (edgeFactory.buildEdge(node, externalNode) != null)
				return true;
		}
		return false;
	}
	
	/**
	 * Checks if node is contained in graph.
	 * @return true if node is in graph <br/> false node not in graph  
	 */
	public boolean containsNode(N node) {
		return _getNodes().contains(node);
	}

	/**
	 * Returns a fresh linked set of the graph's nodes.
	 */
	public LinkedHashSet<N> getNodes() {
		return new LinkedHashSet<N>(adjacencyList.keySet());
	}
	
	/**
	 * Returns the internal ordered set of the graph's nodes.
	 */
	protected Set<N> _getNodes() {
		return  adjacencyList.keySet();
	}
	
	/**
	 * Returns all the nodes connected by an edge to this node
	 * where this node is the startNode
	 */
	public LinkedHashSet<N> getConnectedNodes(N node) {
		LinkedHashSet<N> allNodes = new LinkedHashSet<N>();
		if (containsNode(node)) {
			Set<E> fromNodeEdges = adjacencyList.get(node);
			for (E edge : fromNodeEdges) {
				allNodes.add(edge.endNode);
			}
		}
		return allNodes;
	}
	
	public LinkedHashSet<N> getConnectedNodesTo(N node) {
		final LinkedHashSet<N> allNodes = new LinkedHashSet<N>();
		if (containsNode(node)) {
			for(Set<E> edgeSet : adjacencyList.values()) {
				edgeSet.forEach(new Consumer<E>(){
					public void accept(E t) {
						if(t.endNode.equals(node) && !allNodes.contains(t.startNode)) {
							allNodes.add(t.startNode);
						}
						
					}});
			}
		}
		return allNodes;
	}
	
	public E getConnectingEdge(N fromNode, N toNode) {
		E connectingEdge = null;
		if (containsNode(fromNode) && containsNode(toNode)) {
			Set<E> edges = adjacencyList.get(fromNode);
			for (E edge : edges) {
				if (edge.startNode.equals(fromNode) && edge.endNode.equals(toNode)) {
					connectingEdge = edge;
					break;
				}
			}
		}
		return connectingEdge;
	}
	
	/**
	 * Returns all edges connected to this node.
	 */
	public Set<E> getConnectedEdges(N node) {
		return new LinkedHashSet<E>(_getConnectedEdges(node));
	}
	
	/**
	 * Returns the internal set of the edges connected to the node.
	 */
	protected Set<E> _getConnectedEdges(N node) {
		return adjacencyList.get(node);
	}
	
	/**
	 * Returns the root or the graph or null if the graph is empty. 
	 * The root is the first node added to the graph.
	 */
	public N getRoot() {
		return size() == 0 ? null : _getNodes().iterator().next();
	}
	
	/**
	 * Gets all other nodes in graph then the ones given
	 * @param nodes
	 * @return complementNodes
	 */
	protected Set<N> getComplementNodes(Set<N> nodes){
		Set<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
        return  complementNodes; 
	}
	
	/**
	 * Splits graph based on the binary representation of a long value. (e.g. for 8, the bin repr is 1000)
	 * The first node is associated with the lsb in the value, the second node, with the second lsb...
	 * </p>
	 * Nodes associated with 1 form the {@code first} subgraph of the split, those with 0 the {@code second}.  
	 */
	public Split<N,E,G> split(long encodedSplit) {
		Set<N> nodes = getNodesFromCode(encodedSplit);
		Set<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
		return new Split<N,E,G> (subgraph(nodes),subgraph(complementNodes));
	}
	
	private Set<N> getNodesFromCode(long encodedSplit) {
		int index=0;
		final List<N> nodes = new ArrayList<N>(_getNodes());
		final LinkedHashSet<N> splitNodes = new LinkedHashSet<N>();
		while(encodedSplit != 0) {
			long bit = encodedSplit%2;
			if (bit == 1) {
				splitNodes.add(nodes.get(index));
			}
			index ++;
			encodedSplit/=2;
		}
		return splitNodes;
	}

	public Split<N,E,G> split(BitSet encodedSplit) {
		Set<N> nodes = getNodesFromCode(encodedSplit);
		Set<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
		return new Split<N,E,G> (subgraph(nodes),subgraph(complementNodes));
	}
	
	public Split<N,E,G> split(List<Integer> nodeIndexes) {
		LinkedHashSet<N> nodes = getNodesFromCode(nodeIndexes);
		LinkedHashSet<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
		return new Split<N,E,G>(subgraph(nodes),subgraph(complementNodes));
	}
	
	private LinkedHashSet<N> getNodesFromCode(final List<Integer> nodeIndexes) {
		final List<N> nodes = new ArrayList<N>(_getNodes());
		final LinkedHashSet<N> splitNodes = new LinkedHashSet<N>();
		nodeIndexes.forEach(new Consumer<Integer>(){
			public void accept(Integer t) {
				splitNodes.add(nodes.get(t));
			}
		});
		return splitNodes;
	}
	
	private LinkedHashSet<N> getNodesFromCode(BitSet encodedSplit) {
		int min = encodedSplit.length() >= size()? size() : encodedSplit.length();
		List<N> nodes = new ArrayList<N>(_getNodes());
		LinkedHashSet<N> selectedNodes = new LinkedHashSet<N>();
		for(int nodeIndex = 0; nodeIndex < min; nodeIndex ++) {
			if (encodedSplit.get(nodeIndex)) {
				selectedNodes.add(nodes.get(nodeIndex));
			}
		}
		return selectedNodes;
	}

	/**
	 * Builds a subgraph containing the nodes given by the split.
	 */
	public G subgraph( Set<N> split) {
		Set<N> nodesToRemove=getComplementNodes(split);
		G clone = this.clone();
		for (N node : nodesToRemove) {
			clone.removeNode(node);
		}
		return clone;
	}
	
	/**
	 * Checks if from the root all other nodes can be reached.
	 */
	public boolean isConnected() {
		BreadthFirstNodeIterator<N,E,G> nodeIterator = new BreadthFirstNodeIterator<N,E,G>(this, getRoot());
		Set<N> nodesReachedFromRoot = new HashSet<N>();
		while(nodeIterator.hasNext()) {
			nodesReachedFromRoot.add(nodeIterator.next());
		}
		Set<N> nodesInGraph = _getNodes();
				
		return nodesReachedFromRoot.size() == nodesInGraph.size();
	}
	
	public LinkedList<E>  getShortestPath(N fromNode, N toNode) {
		LinkedList<LinkedList<E>> paths = new LinkedList<>();
		LinkedList<E> shortestPath = null;
		
		BreadthFirstEdgeIterator<N,E,G> edgeVisitor =  new BreadthFirstEdgeIterator<>(this, fromNode);
		while(edgeVisitor.hasNext()) {
			E edge = edgeVisitor.next();
			LinkedList<LinkedList<E>> pathsToRemove = new LinkedList<>();
			LinkedList<LinkedList<E>> pathsToAdd = new LinkedList<> ();
			if (paths.isEmpty()) {
				LinkedList<E> singleton = new LinkedList<E>();
				singleton.add(edge);
				pathsToAdd.add(singleton);
			} else {
				for (LinkedList<E> path : paths) {
					if (path.getLast().endNode == edge.startNode) {
						pathsToRemove.add(path);
						LinkedList<E> pathToAdd = new LinkedList<>(path);
						pathToAdd.add(edge);
						pathsToAdd.add(pathToAdd);
					}
				}
			}
			if (edge.endNode.equals(toNode)) {
				shortestPath = pathsToAdd.getFirst();
				break;
			} 
			paths.removeAll(pathsToRemove);
			paths.addAll(pathsToAdd);
		}
		
		return shortestPath;
	}
	
	public LinkedList<E>  getLongestPath(N fromNode, N toNode) {
        LinkedList<LinkedList<E>> paths = new LinkedList<>();
        LinkedList<E> longestPath = null;
        
        BreadthFirstEdgeIterator<N,E,G> edgeVisitor =  new BreadthFirstEdgeIterator<>(this, fromNode);
        while(edgeVisitor.hasNext()) {
            E edge = edgeVisitor.next();
            LinkedList<LinkedList<E>> pathsToRemove = new LinkedList<>();
            LinkedList<LinkedList<E>> pathsToAdd = new LinkedList<> ();
            if (paths.isEmpty()) {
                LinkedList<E> singleton = new LinkedList<E>();
                singleton.add(edge);
                pathsToAdd.add(singleton);
            } else {
                for (LinkedList<E> path : paths) {
                    if (path.getLast().endNode == edge.startNode) {
                        pathsToRemove.add(path);
                        LinkedList<E> pathToAdd = new LinkedList<>(path);
                        pathToAdd.add(edge);
                        pathsToAdd.add(pathToAdd);
                    }
                }
            }
            paths.removeAll(pathsToRemove);
            paths.addAll(pathsToAdd);
        }
        longestPath = paths.getLast();
        
        return longestPath;
    }
	
//	private LinkedHashSet<E> getShortestPath(N crtNode, N toNode, LinkedList<E> pathSoFar) {
//		if (get)
//	}
	
	/**
	 * Returns the maximal connected subgraph from the given node.
	 */
	public G connectedComponent(N fromNode) {
		BreadthFirstNodeIterator<N,E,G> nodeIter = new BreadthFirstNodeIterator<N,E,G>(this, fromNode);
		LinkedHashSet<N> subgraphNodes = new LinkedHashSet<N>();
		while (nodeIter.hasNext()) {
			N node = nodeIter.next();
			subgraphNodes.add(node);
		}
		return subgraph(subgraphNodes);
	}
	
	/**
	 * Returns all the connected components in the graph. If the graph is connected,
	 * there is only one such component. 
	 */
	public List<G> connectedComponents() {
		LinkedHashSet<N> nodesLeft = getNodes();
		List<G> connectedComponents = new LinkedList<G>();
		while(!nodesLeft.isEmpty()) {
			N firstNode = nodesLeft.iterator().next();
			G connectedComponent = connectedComponent(firstNode);
			connectedComponents.add(connectedComponent);
			nodesLeft.removeAll(connectedComponent._getNodes());
		}
		return connectedComponents;
	}
	

	/**
	 * Gets the size of the graph.
	 */
	public int size() {
		return this.adjacencyList.size();
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(N currentNode : _getNodes()) {
			builder.append(currentNode).append(" { \n");
			for (E edge : this._getConnectedEdges(currentNode)) {
				builder.append("  ").append(edge.toString()).append("\n");
			}
			builder.append("}\n");
		}
		return builder.toString();
	}
	
	public N getNode(int index) {
		int i=0;
		for(N node : _getNodes()) {
			if (index == i) {
				return node;
			}
			i ++;
		}
		return null;
	}
	
	public int indexOf(N node) {
		int index = 0;
		for (N graphNode : this._getNodes()) {
			if (node.equals(graphNode)) {
				return index;
			}
			index ++;
		}
		return index;
	}
	
	public Iterable<Split<N,E,G>> splits(final int refNodeIndex) {
		if (refNodeIndex < 0 || refNodeIndex >= size()) {
			throw new BugException("Ref node index outsize of graph").addDecoration("refNode", refNodeIndex).addDecoration("graph size", size());
		}
		return new Iterable<Split<N,E,G>> () {
			@SuppressWarnings("unchecked")
			public Iterator<Split<N, E, G>> iterator() {
				return new BasicGraphSplitter<N,E,G>((G)AbstractGraph.this, refNodeIndex);
			}
		};
	}
		
		
	
	public Iterable<Split<N,E,G>> connectedSplits(final int refNodeIndex) {
		if (refNodeIndex < 0 || refNodeIndex >= size()) {
			throw new BugException("Ref node index outsize of graph").addDecoration("refNode", refNodeIndex).addDecoration("graph size", size());
		}
		return new Iterable<Split<N,E,G>> () {
			@SuppressWarnings("unchecked")
			public Iterator<Split<N, E, G>> iterator() {
				return new ConnectedGraphSplitter<N,E,G>((G)AbstractGraph.this, refNodeIndex);
			}
		};
	}
	
	protected G buildNew(EdgeFactory<N, E> edgeFactory2) {
		return buildNew(new LinkedHashMap<N,Set<E>>(), edgeFactory2);
	}
	
	protected abstract G buildNew(LinkedHashMap<N, Set<E>> adjacencyList2, EdgeFactory<N, E> edgeFactory2);
}
