package sut.implementation;

import java.io.File;

import org.apache.log4j.Logger;

import util.Filesystem;
import util.RunCmd;
import abslearning.app.Config;

public class Util {
	private static final Logger logger = Logger.getLogger(Util.class);

	public static String tomteLearnresult2uppaalCmd;
	public static String sutUppaal2JarCmd;
	public static String sutUppaal2SutInfoCmd;

	public static void init(Config config) {

	   tomteLearnresult2uppaalCmd=config.dependency_tomteLearnresult2uppaalCmd;
	   sutUppaal2JarCmd=config.dependency_sutUppaal2JarCmd;
       sutUppaal2SutInfoCmd=config.dependency_sutUppaal2SutInfoCmd;


	}


	/* createUppaalModelFileFromAbstractionsAndAbstractModel
	 *
	 * input:
	 *   - abstractionsJsonFilename
	 *   - abstractModelDotFilename
	 * output:
	 *   -concreteModelFilename: generated efsm model in an uppaal file <modelFile>
	 *
	 */
	public static void createUppaalModelFileFromAbstractionsAndAbstractModel(String abstractionsJsonFilename,String abstractModelDotFilename,String concreteModelFilename, boolean fullHypOutput) {
		// get dir where to generate model
		//String modelDir=(new File(concreteModelFilename)).getParent() + "/";

		//jsonFile and dotFile ->  uppaal modelfile
		String output="concreteOutput";
		if ( fullHypOutput ) {
			output="fullHypOutput";
		}
		//only to get the script working
	    String mode="stateVar";
		String cmd_str=tomteLearnresult2uppaalCmd + " " + abstractionsJsonFilename + " " +  abstractModelDotFilename + " " + concreteModelFilename + " " + mode + " " + output;
        RunCmd.runCmd(cmd_str,System.out,System.err,true,true);
	}


// wrappers
// -----------------------------------




	public static void createJarFileFromAbstractionsAndAbstractModel(
			String abstractionsJsonFilename,String abstractModelDotFilename,
			String jarFile, String classPackage,String className,
			boolean fullHypOutput) {

		// get dir where to generate model
		String outputDir=(new File(jarFile)).getParent() + "/";
		String name=Filesystem.getFileNameWithoutExtension(jarFile);

		if (! new File(outputDir).isDirectory() ) {
			//throw new ExceptionAdapter(new FileNotFoundException("Cannot find output directory for generating Sut"));
			//System.err.println("Error:\n    Cannot find output directory for generating Sut in createSutFromHypothesis in " +  Log.class.getName()  + "class, directory: " + classPathDir );
			System.err.println("\n\nError: stop program \n    Cannot find output directory '" + outputDir +  "' for generating Sut! "   );
			System.exit(1);
		}

		String concreteModelFilename=outputDir + "/" + name + ".xml";

		//String concreteModelFilename=sourcePathDir + "/" + classPackage.replace('.', '/') + className + ".xml";
		logger.info("create uppaal model for concrete hyp");
		createUppaalModelFileFromAbstractionsAndAbstractModel(  abstractionsJsonFilename, abstractModelDotFilename, concreteModelFilename, fullHypOutput);
		logger.info("create sut from uppaal model of concrete hyp");
		createJarFileFromUppaalModelFile(concreteModelFilename,jarFile, classPackage,className);
	}



// methods
// -----------------------------------

	/* createSutInfoFromUppaalModel - from uppaal model generate information about SUT in a SutInfo yaml file
	 *
	 * input:
	 *   - uppaal model
	 *   - interfacesYamlFile
	 * outputs:
	 *   - nothing, but creates a SutInfo yaml file which specifies the model's interfaces in <interfacesYamlFile> specified as input
	 *
	 */
	public static void createSutInfoFileFromUppaalModelFile(String modelFile,String interfacesYamlFile) {

		String cmd_str= sutUppaal2SutInfoCmd + " " + modelFile + " " + interfacesYamlFile;
		RunCmd.runCmd(cmd_str,System.out,System.err,true,true);

	}


	/* createSutFromUppaalModel - from uppaal model generate a Sut implementation in a java jar file
	 *
	 * input:
	 *   - uppaal model
	 *   - jarfile
	 * returns:
	 *   - nothing, but creates object implementing SutInterface in <jarfile>  input
	 */
	public static void createJarFileFromUppaalModelFile(String modelFile,String jarfile, String classPackage, String className) {


		String cmd_str=sutUppaal2JarCmd + " "  + modelFile + " " + jarfile + " " + classPackage + " " + className ;
		RunCmd.runCmd(cmd_str,System.out,System.err,true,true);
		logger.fatal("create model created");
	}








}
