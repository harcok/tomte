/*
 * sut is generated from model  : /home/dummy6/workspace/AbsLearnRGB_tree/models/FWGC2/model.xml
 */

package sut.implementation;

import java.util.ArrayList;
import java.util.List;




public class Sut implements sut.interfaces.SutInterface{
    // Initialize statemachine variables and locations
    int v_in = 0;
    int cabbage = 4;
    int wolf = 2;
    int goat = 3;
    int farmer = 1;
    

    public enum Location {
        id8,
        id9,
        id6,
        id7,
        id4,
        id5,
        id2,
        id3,
        id0,
        id1,
        id10,
        id11,
        id12,
        id13,
        id14,
        id15,
        id16,
        id17,
        ERR
    }

    private Location currentLoc = Location.id17;


    public void reset() {
        v_in = 0;
        cabbage = 4;
        wolf = 2;
        goat = 3;
        farmer = 1;
        currentLoc = Location.id17;
    }

    public Location getCurrentLocation() {
        return currentLoc;
    }


    //Inputs
    public void IIN(  Integer tomte_p0  ) {
        switch (currentLoc) {
        
            case id8:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id9;
                        break;
                    
            case id6:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id5;
                        break;
                    
            case id3:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id2;
                        break;
                    
            case id1:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id0;
                        break;
                    
            case id10:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id4;
                        break;
                    
            case id12:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id11;
                        break;
                    
            case id14:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id13;
                        break;
                    
            case id15:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id7;
                        break;
                    
            case id17:
                
                        v_in=tomte_p0;
                        currentLoc = Location.id16;
                        break;
                    
        default:
        }
    }
    


    //Outputs
    public OutputAction delta() {
        int paramIndex = 0;
        String methodName = "Oquiescence";
        List < Parameter > result = new ArrayList < Parameter > ();
        switch (currentLoc) {
        
            case id9:
                if ( v_in==goat ) {
                            methodName = "OOK";
                            currentLoc = Location.id14;
                            break;
                        }
                    if ( v_in==wolf ) {
                            methodName = "OOK";
                            currentLoc = Location.id3;
                            break;
                        }
                    if ( v_in==farmer ) {
                            methodName = "OEATEN";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==cabbage ) {
                            methodName = "ONOK";
                            currentLoc = Location.id8;
                            break;
                        }
                    
            case id7:
                if ( v_in==wolf || v_in==cabbage ) {
                            methodName = "ONOK";
                            currentLoc = Location.id15;
                            break;
                        }
                    if ( v_in==goat ) {
                            methodName = "OOK";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==farmer ) {
                            methodName = "OOK";
                            currentLoc = Location.id6;
                            break;
                        }
                    
            case id4:
                if ( v_in==goat ) {
                            methodName = "OOK";
                            currentLoc = Location.id12;
                            break;
                        }
                    if ( v_in==cabbage ) {
                            methodName = "OOK";
                            currentLoc = Location.id3;
                            break;
                        }
                    if ( v_in==farmer ) {
                            methodName = "OEATEN";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==wolf ) {
                            methodName = "ONOK";
                            currentLoc = Location.id10;
                            break;
                        }
                    
            case id5:
                if ( v_in==goat ) {
                            methodName = "ONOK";
                            currentLoc = Location.id6;
                            break;
                        }
                    if ( v_in==cabbage ) {
                            methodName = "OOK";
                            currentLoc = Location.id14;
                            break;
                        }
                    if ( v_in==wolf ) {
                            methodName = "OOK";
                            currentLoc = Location.id12;
                            break;
                        }
                    if ( v_in==farmer ) {
                            methodName = "OOK";
                            currentLoc = Location.id15;
                            break;
                        }
                    
            case id2:
                if ( v_in==cabbage ) {
                            methodName = "OOK";
                            currentLoc = Location.id10;
                            break;
                        }
                    if ( v_in==wolf ) {
                            methodName = "OOK";
                            currentLoc = Location.id8;
                            break;
                        }
                    if ( v_in==goat ) {
                            methodName = "ONOK";
                            currentLoc = Location.id3;
                            break;
                        }
                    if ( v_in==farmer ) {
                            methodName = "OOK";
                            currentLoc = Location.id1;
                            break;
                        }
                    
            case id0:
                if ( v_in==goat ) {
                            methodName = "ODONE";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==farmer ) {
                            methodName = "OOK";
                            currentLoc = Location.id3;
                            break;
                        }
                    if ( v_in==wolf || v_in==cabbage ) {
                            methodName = "ONOK";
                            currentLoc = Location.id1;
                            break;
                        }
                    
            case id11:
                if ( v_in==farmer ) {
                            methodName = "OEATEN";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==cabbage ) {
                            methodName = "ONOK";
                            currentLoc = Location.id12;
                            break;
                        }
                    if ( v_in==wolf ) {
                            methodName = "OOK";
                            currentLoc = Location.id6;
                            break;
                        }
                    if ( v_in==goat ) {
                            methodName = "OOK";
                            currentLoc = Location.id10;
                            break;
                        }
                    
            case id13:
                if ( v_in==farmer ) {
                            methodName = "OEATEN";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==wolf ) {
                            methodName = "ONOK";
                            currentLoc = Location.id14;
                            break;
                        }
                    if ( v_in==cabbage ) {
                            methodName = "OOK";
                            currentLoc = Location.id6;
                            break;
                        }
                    if ( v_in==goat ) {
                            methodName = "OOK";
                            currentLoc = Location.id8;
                            break;
                        }
                    
            case id16:
                if ( v_in!=goat ) {
                            methodName = "OEATEN";
                            currentLoc = Location.id17;
                            break;
                        }
                    if ( v_in==goat ) {
                            methodName = "OOK";
                            currentLoc = Location.id15;
                            break;
                        }
                    
        default:
        }
        return new OutputAction(methodName, result);
    }


    public OutputAction processSymbol(InputAction methodWrapper) {
        if (methodWrapper.getMethodName().equals("IIN")) {
            List < Parameter > params = methodWrapper.getParameters();
            IIN((params.get(0).getValue()));
            return delta();
        }
        
        // else : 
        System.out.println("\n\nERROR:\n  unsupported input: " + methodWrapper.getMethodName());
        System.exit(-1);
        return null;
    }

	public sut.interfaces.OutputAction sendInput( sut.interfaces.InputAction action) {
		InputAction ia = new InputAction(action);
		OutputAction  oa=processSymbol(ia);
		return oa;  //oa implements sut.interfaces.OutputAction interface
	}	

	public void sendReset() {
		reset();
	}



}