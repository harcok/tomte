package sut.info;

import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

/**
 * basic interface to SUT/Hypothesis which uses Tomte's InputAction/OutputAction classes
 * 
 * note: classes implementing this interface internal use the SutInterface which uses
 *       simpler input and output action classes.
 */
public interface TomteSutInterface {
	
	//TODO it is confusing that sometimes this is used to describe concrete inputs, other times abstract inputs
	// process input to an output 
	public OutputAction sendInput(InputAction concreteInput);

	// reset SUT
	public void sendReset();

}
