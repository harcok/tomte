package sut.info;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.antlr.symtab.PrimitiveType;
import org.antlr.symtab.Type;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import com.google.common.collect.ImmutableSet;

import abslearning.app.SutInfoYaml;
import abslearning.hyp.JarHypBuilder;
import abslearning.hyp.RegisterAutomatonHypBuilder;
import abslearning.stores.ConfigurableValueStoreProvider;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.AlphabetImpl;
import de.ls5.jlearn.shared.SymbolImpl;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import sut.implementation.Util;
import sut.interfacing.DeterminizerWrapper;
import sut.interfacing.GeneratedSutDirectWrapper;
import sut.interfacing.SutSocketWrapper;
import util.ExceptionAdapter;
import util.Filesystem;

public class SutInfo {
	public static String name;
    private static List < Integer > constants = new ArrayList < Integer > ();
    private static List < ActionSignature > inputSignatures;
    private static List < ActionSignature > outputSignatures;
	private static String sutWrapperClassName;
	public static String sutInfoYamlFile;
	private static int  socketPortNumber;
	private static String socketAddress;
	private static String communication_method;
	private static String methodCallSutClassName;
	private static String methodCallSutPackageName;

	private static ImmutableSet<DataInputActionType> inputAlphabet;
	private static ImmutableSet<DataOutputActionType> outputAlphabet;


	protected static void addFlattenedSymbols(Alphabet result, String symbol, int min,int max,int numParams,int level) {
		for(int x = min; x <= max; x = x+1){
			String newSymbol=symbol + "_" + x;
			if ( level == numParams ) {
				result.addSymbol(new SymbolImpl(newSymbol));
			} else {
				addFlattenedSymbols(result,newSymbol,min,max,numParams,level+1);
			}
		}
	}


    public static List < Integer > getConstants() {
        return constants;
    }

    public static void setConstants(List < Integer > constants) {
        	SutInfo.constants=constants;
    }


    public static void useSocketCommunication( String server, int port) {
        	communication_method="socket";
        	socketPortNumber=port;
        	socketAddress = server;
    }



    public static void useMethodCallCommunication( String packageName, String className  ) {
        	communication_method="methodCall";
        	methodCallSutClassName=className;
        	methodCallSutPackageName=packageName;
    }


    public static void initialize( String sutInfoYamlFile, boolean useSutSimulation,
    		                       String modelFile, String outputDir, String useExistingJarfile) {


      SutInfo.sutInfoYamlFile=sutInfoYamlFile;

       	if ( useSutSimulation ) {
       		if ( useExistingJarfile == null ) {

                // simulation mode using generated code: so sut.jar and sutInfo.yaml file will generated on the fly at :
			    sutInfoYamlFile =outputDir  +  "/generated/sut/sutInfo.yaml";
			    String jarfile=outputDir  + "/generated/sut/sut.jar";

			    // generate output dir
			    Filesystem.mkdirhere(outputDir  + "/generated/sut/");

			    // generate java class file and sutInfo.yaml file
			    SutInfo.sutWrapperClassName ="GeneratedSutDirectWrapper";
			    GeneratedSutDirectWrapper.generateSutFromModel(modelFile,sutInfoYamlFile,jarfile, methodCallSutPackageName, methodCallSutClassName );

			    // make copy of original input in output dir
			    Filesystem.copyfile(modelFile,  outputDir  + "/input/" + "model.xml");

       		} else {
			    SutInfo.sutWrapperClassName ="GeneratedSutDirectWrapper";
			    GeneratedSutDirectWrapper.SutFromJarFile(sutInfoYamlFile,useExistingJarfile, methodCallSutPackageName, methodCallSutClassName );
       		}

	    	// read in information given about sut from SutInfo.yaml file
		    SutInfo.loadFromYaml(sutInfoYamlFile);
       	} else {
	       	if (  communication_method.equals("socket") ) {
			    // use socket wrapper :
			    // -> uses static files :
			    //    - input/sutInfo.yaml
			    //    - sut/implementation/Sut.java
			    //sutInfoYamlFile = "input/sutInfo.yaml";
	       		SutInfo.sutWrapperClassName ="SutSocketWrapper";
	    		Filesystem.copyfile(sutInfoYamlFile, outputDir + "/sutinfo.yaml");
	    	} else {
	    		System.err.println("\nAbort:\n  using simulation   : \"" + useSutSimulation + "\"" );
	    		System.err.println("\nAbort:\n  Invalid communication method with SUT  : \"" + SutInfo.communication_method + "\"" );
	    		System.exit(1);
	    	}
       	}

    }
    public static TomteSutInterface newSutWrapper() {
     	return newSutWrapper(false);
    }

    public static TomteSutInterface newSutWrapper(boolean flatten) {
    	TomteSutInterface sutWrapper=null;

    	if ( SutInfo.sutWrapperClassName.equals("GeneratedSutDirectWrapper")  ) {
    		sutWrapper= new GeneratedSutDirectWrapper(flatten) ;
//    	} else if ( SutInfo.sutWrapperClassName.equals("SutDirectWrapper") ) {
//    		sutWrapper= new SutDirectWrapper(flatten);
    	} else if ( SutInfo.sutWrapperClassName.equals("SutSocketWrapper") ) {
    		sutWrapper=  new SutSocketWrapper(socketAddress,socketPortNumber) ;
    	} else {
    		System.err.println("\nAbort:\n  Invalid sut wrapper class : \"" + SutInfo.sutWrapperClassName + "\"" );
    		System.exit(1);
    	}

    	return sutWrapper;
    }

    public static List < ActionSignature > getInputSignatures() {
        return new ArrayList < ActionSignature > (inputSignatures);
    }

    public static void setInputSignatures(Map<String, List<String>> signatures) {
        // old implementation
        SutInfo.inputSignatures = new ArrayList < ActionSignature > ();
        	for (Entry<String, List<String>> entry : signatures.entrySet()) {
        		SutInfo.inputSignatures.add(new ActionSignature(entry.getKey(), entry.getValue()));
        	}

        	// new implementation
        	ImmutableSet.Builder<DataInputActionType> alphabetBuilder=ImmutableSet.builder();
        for (Entry<String, List<String>> entry : signatures.entrySet()) {
            String methodName=entry.getKey();
            List<String> parameterTypeStrings=entry.getValue();
            List<Type> parameterTypes= new ArrayList<Type>();
            for ( String type: parameterTypeStrings) {
                parameterTypes.add(new PrimitiveType(type));
            }
            DataInputActionType inputType= new DataInputActionType(methodName, parameterTypes);
            alphabetBuilder.add(inputType);
        }
        inputAlphabet = alphabetBuilder.build();
    }

    public static ActionSignature getInputSignature(String methodName) {
        for (ActionSignature sig: inputSignatures) {
            if (sig.getMethodName().equals(methodName)) {
                return sig;
            }
        }
        return null;
    }

    public static void addInputSignature(String methodName, List < String > parameters) {
    	SutInfo.inputSignatures.add(new ActionSignature(methodName, parameters));
    }




    public static List < ActionSignature > getOutputSignatures() {
        return new ArrayList < ActionSignature > (outputSignatures);
    }

    public static void setOutputSignatures(Map<String, List<String>> signatures) {
        // old implementation
        SutInfo.outputSignatures = new ArrayList < ActionSignature > ();
        for (Entry<String, List<String>> entry : signatures.entrySet()) {
        	SutInfo.outputSignatures.add(new ActionSignature(entry.getKey(), entry.getValue()));
        }

        // new implementation
        ImmutableSet.Builder<DataOutputActionType> alphabetBuilder=ImmutableSet.builder();
        for (Entry<String, List<String>> entry : signatures.entrySet()) {
            String methodName=entry.getKey();
            List<String> parameterTypeStrings=entry.getValue();
            List<Type> parameterTypes= new ArrayList<Type>();
            for ( String type: parameterTypeStrings) {
                parameterTypes.add(new PrimitiveType(type));
            }
            DataOutputActionType outputType= new DataOutputActionType(methodName, parameterTypes);
            alphabetBuilder.add(outputType);
        }
        outputAlphabet = alphabetBuilder.build();
    }



    public static ActionSignature getOutputSignature(String methodName) {
        for (ActionSignature sig: outputSignatures) {
            if (sig.getMethodName().equals(methodName)) {
                return sig;
            }
        }
        return null;
    }

    public static void addOutputSignature(String methodName, List < String > parameters) {
        SutInfo.outputSignatures.add(new ActionSignature(methodName, parameters));
    }

	public static void loadFromYaml(String sutInfoYamlFileName) {
		InputStream sutYamlStream=null;
		try {
			sutYamlStream = new FileInputStream(sutInfoYamlFileName);
		} catch (FileNotFoundException fnfe) {
			System.err.println("FileNotFoundException in loadFromYaml in sut.info.SutInfo class, file: " + sutInfoYamlFileName );
			throw new ExceptionAdapter(fnfe);
		}

	    Yaml yamlSut = new Yaml(new Constructor(SutInfoYaml.class));
	    SutInfoYaml sutinterfaces = (SutInfoYaml) yamlSut.load(sutYamlStream);

	    SutInfo.name=sutinterfaces.name;
	    SutInfo.setConstants(new ArrayList<Integer>(sutinterfaces.constants));

	    SutInfo.setInputSignatures(sutinterfaces.inputInterfaces);
	    SutInfo.setOutputSignatures(sutinterfaces.outputInterfaces);
	}

	public static String getName() {
		return name;
	}

    static public ImmutableSet<DataInputActionType>  getInputAlphabet() {
          return inputAlphabet;
    }

    static public ImmutableSet<DataOutputActionType>  getOutputAlphabet() {
        return outputAlphabet;
    }


}
