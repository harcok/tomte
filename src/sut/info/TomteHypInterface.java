package sut.info;

import java.util.SortedMap;

import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;


/**
 * extended interface to Hypothesis which uses Tomte's InputAction/OutputAction classes
 * 
 * note: classes implementing this interface internal use the SutInterface which uses
 *       simpler input and output action classes.
 */
public interface TomteHypInterface  extends TomteSutInterface{
	
	// process input to an output 
	public OutputAction sendInput(InputAction concreteInput);

	// reset SUT
	public void sendReset();
	
	public SortedMap<Integer, Integer> getStatevars();

}
