package sut.interfacing;

import java.util.ArrayList;
import java.util.List;

import abslearning.determinizer.Determinizer;
import sut.interfaces.SutInterface;


/***
 * Applies the determinizer over the sut. The determinizer canonizes fresh output values 
 * but leaves fresh input values unchanged.
 */
public class DeterminizerWrapper implements sut.info.TomteSutInterface {
	protected sut.info.TomteSutInterface sut=null;
	// this trace is only used locally to perform the canonizing
	private Determinizer determinizer;
	
	public DeterminizerWrapper(SutInterface concSut, List<Integer> constants) {
		this(new TomteSutWrapper ( concSut), new Determinizer(constants)); 
	}
	
	public DeterminizerWrapper(sut.info.TomteSutInterface sut, List<Integer> constants) {
		this(sut, new Determinizer(constants)); 
	}
	
	public DeterminizerWrapper(sut.info.TomteSutInterface sut, Determinizer determinizer) {
		this.sut = sut;
		this.determinizer = determinizer;
	}
	
//	public DeterminizerWrapper(sut.info.TomteSutInterface sut, List<Integer> constants, ConfigurableValueStoreProvider valueStoreProvider) {
//		this.determizer = new Determinizer(constants, ValueMapperProvider.DUALFORREST, ConfigurableValueStoreProvider. 
//	}
	
	

	public abslearning.trace.action.OutputAction sendInput(abslearning.trace.action.InputAction input) {

		input = determinizer.decanonize(input);
		abslearning.trace.action.OutputAction output = sut.sendInput(input);		
        
        List<Integer> concretizedParams = new ArrayList<Integer>();
	    for(Integer hypOutput : output.getConcreteParameters()) {
	    	concretizedParams.add(hypOutput);
	    }
	    
	    output.setConcreteValues(concretizedParams);		
	    output = determinizer.canonize(output);
	    
		return output;		
	}

	public void sendReset()  {
            sut.sendReset();
            determinizer.reset();
	}

}


