package sut.interfacing;

import sut.interfaces.InputAction;
import sut.interfaces.OutputAction;
import sut.interfaces.SutInterface;

public class TomteSutWrapper implements sut.info.TomteSutInterface{
	
	private sut.interfaces.SutInterface sut;
	
	public TomteSutWrapper(SutInterface sut) {
		this.sut = sut;
	}

	@Override
	public abslearning.trace.action.OutputAction sendInput(
			abslearning.trace.action.InputAction concreteInput) {
		InputAction sutInput = (sut.interfaces.InputAction)concreteInput;
		
		// Send input to SUT and get output
		OutputAction sutOutput = sut.sendInput(sutInput);
		
		// convert back output to right type for tomte
		abslearning.trace.action.OutputAction concreteOutput = new abslearning.trace.action.OutputAction(sutOutput);
		return concreteOutput;
	}


	public void sendReset() {
		sut.sendReset();
	}
	
}
