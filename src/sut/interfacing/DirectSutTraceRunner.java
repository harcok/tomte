package sut.interfacing;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.tree.trace.SutTraceInterface;
import sut.info.TomteSutInterface;

public class DirectSutTraceRunner implements SutTraceInterface{
	private TomteSutInterface sut;

	public DirectSutTraceRunner(TomteSutInterface sutWrapper) {
		this.sut = sutWrapper;
	}

	@Override
	public OutputAction sendInput(InputAction concreteInput, Trace trace) {
		return this.sut.sendInput(concreteInput);
	}

	@Override
	public void sendReset() {
		this.sut.sendReset();
	} 
}
