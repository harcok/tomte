package sut.interfacing;

import sut.implementation.Util;

public class GeneratedSutDirectWrapper extends SutDirectWrapper {

    static private String jarfile;
    static private String classPackage;
    static private String className;  
    static private boolean doneGeneration=false;
    
	    
     
	/* Constructor which assumes java class already generated before by calling the static method
     * 'GeneratedSutDirectWrapper.generateSutFromModel' first before calling this constructor.
     * 
     *  This constructor only does initialization in which it dynamically loads the sut java class.
     */  	
	public GeneratedSutDirectWrapper(boolean enableFlatten) {
		
		if (! doneGeneration ) {
    		System.err.println("\nAbort:\n  forgotten to do generation of java class" );
    		System.exit(1);			
		}
		this.flatten = enableFlatten;
		init( );
	} 
	
	public GeneratedSutDirectWrapper() {
		this(false);
	}	
	
	

	
	 
	   /* generateSutFromModel 
	        method to generate
	           - java source of Sut class 
	           - yaml file describing  alphabet used by Sut
	        params     
			   - modelFile : uppaal modelfile   (set in config.yaml)
			  
			   - sutInfoYamlFile  : sut yaml file to be generated which is needed by learner to easily read SutInfo of model 
			   
			   - sourcePathDir : root directory where generated java files are stored  (<logdir>/models/)
  		       - classPackage : package of generated java class (sut.implementation)
			   - className : name of generated java class (Sut)		
			   - classPath: is the java CLASSPATH which specifies all paths having  java compiled bytecode classes which may be needed
	                        for compiling dependencies  (build/) 
	   */	
		static public  void generateSutFromModel(String modelFile,String sutInfoYamlFile, String jarfile, String classPackage,String className ) {

			GeneratedSutDirectWrapper.classPackage=classPackage;
			GeneratedSutDirectWrapper.className=className;
			GeneratedSutDirectWrapper.jarfile=jarfile;
			


			 // generate java implementation of SUT:   SutImplementation.java 
			Util.createJarFileFromUppaalModelFile(modelFile,jarfile,classPackage,className);
			
            // generate information file for SUT: sutinfo.yaml			
			Util.createSutInfoFileFromUppaalModelFile(modelFile,sutInfoYamlFile);
             
             			
			doneGeneration=true;
		}	 
		
		

	    // sut java implementation is only generated once from uppaal model in generateSutFromModel(..) method,
		// however multiple instance of this sut class can be instantiated by instatiating a GeneratedSutDirectWrapper class
		 private void  init() {
			// initialize a new sut instance to communicate with
			this.sut= new TomteSutWrapper( (sut.interfaces.SutInterface) util.JavaUtil.loadJavaClass(jarfile,classPackage + "." + className));		
			
		}

		public static void SutFromJarFile(String sutInfoYamlFile,
				String jarfile, String classPackage, String className) {
			
			GeneratedSutDirectWrapper.classPackage=classPackage;
			GeneratedSutDirectWrapper.className=className;
			GeneratedSutDirectWrapper.jarfile=jarfile;
			
			doneGeneration=true;  
			
		}



}


