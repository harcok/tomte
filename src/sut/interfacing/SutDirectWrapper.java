package sut.interfacing;



import abslearning.app.Statistics;
import abslearning.trace.action.OutputAction;

public class SutDirectWrapper implements sut.info.TomteSutInterface  {
	private static final Statistics statistics = Statistics.getInstance();
	private boolean skipReset=false;
	protected sut.info.TomteSutInterface  sut=null;
	protected boolean flatten=false;




	public SutDirectWrapper(boolean enableFlatten) {
		this.flatten = enableFlatten;
		// initialize a new sut instance to communicate with
		this.sut=new TomteSutWrapper(new sut.implementation.Sut());
	}
	
	public SutDirectWrapper() {
		this(false);
	}	



	public OutputAction sendInput(abslearning.trace.action.InputAction input) {
		statistics.incSutInput();
		skipReset=false; // only when no inputs are send we can skip another reset
		if (flatten) {
			// deflatten input action 
			input.deflatten();
		}
       abslearning.trace.action.OutputAction   output= sut.sendInput(input);
       
		if (flatten) {
			// flatten output action
			output.flatten();
		}
		
		return output;		
	}

	public void sendReset()  {
		// if no inputs are send after a reset we skip the reset requested
		// if inputs are send we cannot skip another reset
		if ( skipReset ) return; 
		statistics.incSutReset();
        sut.sendReset();
        skipReset=true; // only when no inputs are send we can skip another reset
	}
	
	public void close() {
	}

	

	
	
}

