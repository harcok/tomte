package sut.interfacing;

import java.util.LinkedHashMap;
import java.util.SortedMap;
import java.util.TreeMap;

import util.Tuple3;
import abslearning.exceptions.BugException;
import abslearning.mapper.AbstractionMapper;
import abslearning.trace.Trace;
import abslearning.trace.action.OutputAction;



/**
 * Interfaces a concrete hypothesis that returns concrete outputs concatenated with the respective abstract
 * input and output strings. These abstractions are used to maintain a set of state variables.
 * 
 * Abstraction and concretization is done based on the abstract inputs and outputs which can contain
 * strings with operations on the set of state variables. 
 */
public class HypWithFullInfoWrapper implements sut.info.TomteHypInterface {
	protected sut.info.TomteSutInterface hypWithFullInfo=null;
	private SortedMap<Integer,Integer> statevarsBeforeTransition = new TreeMap<Integer,Integer>();;
	private SortedMap<Integer,Integer> statevarsAfterTransition = new TreeMap<Integer,Integer>();

	/**
	 * @param hyp - The provided hyp on sending an input should return both input and output abstractions along with the concrete input.
	 */
	public HypWithFullInfoWrapper(sut.info.TomteSutInterface hyp) {
		this.hypWithFullInfo = hyp;
	}
	
	/*
	 * get statevars in current state of hyp
	 * 
	 */
	public SortedMap<Integer, Integer> getStatevars() {
		return statevarsAfterTransition;
	}	

	public void sendReset()  {
		hypWithFullInfo.sendReset();
		statevarsBeforeTransition = new TreeMap<Integer,Integer>();;
		statevarsAfterTransition = new TreeMap<Integer,Integer>();		
    }	
	
	/** sendInput
	 *   sends concrete input to Hyp,
	 *   Hyp sets abstract input in input
	 *   Hyp returns output action which contains both concrete and abstract output
	 *   Hyp updates the statevars to state after transition which you can be 
	 *   retreived by later call getStateVars on hyp.
	 * 
	 * @see sut.info.TomteHypInterface#sendInput(abslearning.trace.action.InputAction)
	 */
	public abslearning.trace.action.OutputAction sendInput(abslearning.trace.action.InputAction concreteInput) {
		// Send input to SUT and get output
	       // abslearning.trace.OutputAction mixedOutput= sendInputBasic(input);
	    abslearning.trace.action.OutputAction mixedOutput= hypWithFullInfo.sendInput(concreteInput);
	        
		
		//next transition:  statevars before transition get values from statevars after previous transition
		statevarsBeforeTransition = new TreeMap<Integer,Integer>(statevarsAfterTransition); 
		

		// important: concreteHypWithFullInfo is a special hyp interface
		//            which gives for each concrete input as result:
		//                    - abstract input 
		//                    - abstract output
		//                    - concrete output
		Tuple3<String,String,OutputAction> absinput_absoutput_concoutput=getAbstractInputAbstractOutputAndConcreteOutputAndFromHyp(mixedOutput);
		String abstractInputString=absinput_absoutput_concoutput.tuple0;
		String abstractOutputString=absinput_absoutput_concoutput.tuple1;
		OutputAction concreteOutput =absinput_absoutput_concoutput.tuple2;
		
		concreteInput.setAbstractionString(abstractInputString);
		
		OutputAction output  = new OutputAction(concreteOutput);
		output.setAbstractionString(abstractOutputString);
		
		// from abstract output determine statevar updates
		LinkedHashMap<Integer, Integer> statevarUpdates=AbstractionMapper.getStateVarUpdatesFromOutputAbstractionString(statevarsBeforeTransition, concreteInput, output,  abstractOutputString);
		
		// update statevars in trace and apply updates to calculate statevars after transition
		statevarsAfterTransition = Trace.getUpdatedStateVars(statevarsBeforeTransition, statevarUpdates);
		
		return output;		
	}



    



	// mixedOutput : Special output action which contains
	//   - mingled abstract input 
	//   - mingled abstract output
	//   - concrete output 
    //
	static public Tuple3<String,String,OutputAction>  getAbstractInputAbstractOutputAndConcreteOutputAndFromHyp( OutputAction mixedOutput ) { 	
						
		String mixedmethodName = mixedOutput.getMethodName();
		
		String[] stuff=mixedmethodName.split("____");
		if (stuff.length != 3) {
			throw new BugException("The hypothesis has returned an unexpected format. " + mixedmethodName + 
					"\n  Expecting: <absInput'>____<absOutput'>____<concOutput>");
		}
		
		String abstractInputString=stuff[0];
		// we have to fix the abstract input string first 
		// note: is mangled to get it nicely through our SUT implementation	
		//        -> input must start with I
		//        -> for each input a method with that name in java is created, and 
		//           java doesn't allow  characters like '-' and '=' in methodnames!
		abstractInputString=abstractInputString.replaceAll("XMINX", "-");
		abstractInputString=abstractInputString.replaceFirst("^OI", "I");								

		String abstractOutputString=stuff[1];
		// we also have to fix the mangled abstract output string
		abstractOutputString=abstractOutputString.replaceAll("XMINX", "-");
		abstractOutputString=abstractOutputString.replaceAll("XEQX", "=");								
		
		
		String outputMethodName=stuff[2];
		OutputAction concreteOutput=new OutputAction(mixedOutput.getConcreteParameters(),outputMethodName);
		
		return new Tuple3<String,String,OutputAction>(abstractInputString,abstractOutputString,concreteOutput);	
	}		

}





