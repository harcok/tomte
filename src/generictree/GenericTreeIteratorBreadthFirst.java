package generictree;

import java.util.LinkedList;


public class GenericTreeIteratorBreadthFirst<T> extends GenericTreeIteratorAbstract<T> implements GenericTreeIterator<GenericTreeNode<T>> {


    protected LinkedList<GenericTreeNode<T>> currentLevelNodeCache = new LinkedList<GenericTreeNode<T>>();  
    protected LinkedList<GenericTreeNode<T>> nextLevelNodeCache = new LinkedList<GenericTreeNode<T>>();  
    // nodeCache uses queue (LIFO) interface of linked list, choosen for fast add(at end) and remove(at begin) LIFO style )
    
  
    /** Construct an GenericTreeIteratorBreadthFirst object.
     * @param startNode The node of the tree where to start the iteration.
     */
    public GenericTreeIteratorBreadthFirst(GenericTreeNode<T> startNode) {        
    	super(startNode);
    	setStartNode(startNode);
    }
    
    public GenericTreeIteratorBreadthFirst<T> newInstance() {
    	return new GenericTreeIteratorBreadthFirst<T>(this.startNode);
    }     
    
	/** Reset the iterator.
	 */
	public void reset() {
		this.nextNode=this.startNode;
	}

    /* gets new value for this.nextNode
     * Note: if no next node node is found it return null 
     */
    GenericTreeNode<T> getNextNode() {   	
		this.nextLevelNodeCache.addAll(this.nextNode.getChildren());

    	// find new this.nextNode node        	
    	if ( this.currentLevelNodeCache.isEmpty()   ) {
    		if ( this.nextLevelNodeCache.isEmpty()   ) {
    			// no more nodes left in tree
    			return null;
    		} 	
    		this.currentLevelNodeCache=this.nextLevelNodeCache;
    		this.nextLevelNodeCache= new LinkedList<GenericTreeNode<T>>();
    	}
    	return this.currentLevelNodeCache.remove(); // remove from begin queue and return node
    	
    }
	
	
	/** Fetches the next node from the tree,
	 *  Note: this function is only called when next node exists!
	 */
	public GenericTreeNode<T> fetchNext() {
    	
    	// this.nextNode becomes node to return
    	GenericTreeNode<T> node=this.nextNode;  
    	
        // prefetch next node for next call to this method
    	// getNextNode() will return null if whole tree is reached
    	// note: if this.nextNode is set to null then hasNext() will on next call return false on iteration will stop!
    	this.nextNode=getNextNode();   
    	
    	return node;  
	}
	

}    
