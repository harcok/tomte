package generictree;

public interface TreeNodeVisitor<T> {
	public void  visit(GenericTreeNode<T> node );
}
