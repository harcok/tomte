package generictree;

import java.util.Iterator;

public interface GenericTreeIterator<E> extends Iterator<E> {

	public E getStartNode();
	public void setStartNode(E startNode);
	//public void reset();	
	public GenericTreeIterator<E> newInstance();
}
