package abslearning.learner;

import abslearning.exceptions.ConfigurationException;
import de.ls5.jlearn.algorithms.angluin.Angluin;
import de.ls5.jlearn.algorithms.packs.ObservationPack;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.Word;

/**
 * Learner adapter for the old learn lib version.
 */
public class OldLearnLibAdapter implements LearnerInterface {

	private  de.ls5.jlearn.interfaces.Learner learner;
	public OldLearnLibAdapter(Alphabet inputAlphabet, Run run, String learningAlgorithm){
		MembershipOracle membershipOracle = new MembershipOracle(run);
		switch(learningAlgorithm.toLowerCase()){
		  case "observationpack": learner = new ObservationPack(); break;
		  case "angluin": learner = new Angluin(); break;
		  default: throw new ConfigurationException("unsupported learning algorithm:" + learningAlgorithm);

		}
		learner.setOracle(membershipOracle);
		learner.setAlphabet(inputAlphabet);
	}

	@Override
	public void startLearning() throws Exception{
		learner.learn();
	}

	@Override
	public Automaton getHypothesis() {
		return learner.getResult();
	}


	@Override
	public boolean addCounterExample(Word ceInputs, Word sutOutputs) throws Exception{
		/*
		 * note:
		 *   The learnlib 'addCounterExample' method does only
		 *   check if a trace is a counterexample for the hypothesis,
		 *   but does not check if it is a valid trace of the sut!
		 *
		 *   If learnlib is given an counterexample trace from the sut
		 *   which is invalid, then learnlib will sometimes not
		 *   terminate the 'addCounterExample' method.
		 */
		boolean isCounterExample = learner.addCounterExample(ceInputs, sutOutputs);
		return isCounterExample;
	}

}
