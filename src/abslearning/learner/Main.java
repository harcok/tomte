package abslearning.learner;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.app.Config;
import abslearning.app.HypStore;
import abslearning.app.Phase;
import abslearning.app.Statistics;
import abslearning.ceanalysis.Analyzer;
import abslearning.ceanalysis.CeStore;
import abslearning.ceanalysis.reduction.Counterexample;
import abslearning.determinizer.Canonicalizer;
import abslearning.exceptions.BugException;
import abslearning.exceptions.ConfigurationException;
import abslearning.exceptions.LearnerCounterExampleException;
import abslearning.exceptions.MaxNumRunsException;
import abslearning.exceptions.Messages;
import abslearning.exceptions.OutOfTimeException;
import abslearning.exceptions.RestartLearningException;
import abslearning.exceptions.UpdateConstantActionException;
import abslearning.hyp.HypBuilderStore;
import abslearning.hyp.HypothesisBuilder;
import abslearning.hyp.HypothesisReducer;
import abslearning.logger.LearnLogger;
import abslearning.logger.StructLogger;
import abslearning.lookahead.LookaheadConcretizerProvider;
import abslearning.lookahead.trace.LookaheadTrace;
import abslearning.lookahead.trace.LookaheadTraceStore;
import abslearning.mapper.AbstractionMapper;
import abslearning.relation.Relation;
import abslearning.stores.ConfigurableValueStoreProvider;
import abslearning.stores.PredicateStore;
import abslearning.stores.RandomRangedValueStore;
import abslearning.stores.RandomValueStore;
import abslearning.stores.StepWiseValueStore;
import abslearning.tester.HypTester;
import abslearning.tester.TesterStore;
import abslearning.trace.Trace;
import abslearning.trace.TraceBuilder;
import abslearning.trace.action.OutputAction;
import abslearning.tree.ConcreteTree;
import abslearning.tree.TreeDumper;
import abslearning.tree.TreeLoader;
import abslearning.verifier.HypDesc;
import abslearning.verifier.HypVerifier;
import abslearning.verifier.VerifierStore;
import de.ls5.jlearn.abstractclasses.LearningException;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.EquivalenceOracleOutput;
import de.ls5.jlearn.interfaces.Word;
import net.automatalib.words.impl.SimpleAlphabet;
import sut.info.ActionSignature;
import sut.info.SutInfo;
import sut.info.TomteHypInterface;
import util.Test;
import util.Tuple2;
import util.exceptions.CheckException;


public class Main {



	private static final Logger logger = Logger.getLogger(Main.class);


	private static final Statistics statistics = Statistics.getInstance();


	public static boolean preferFirst;

	// TODO: put in global GlobalConstants class instead, or some other solution





	public static void logReleaseVersion(String tomteRootPath, String outputDir) throws FileNotFoundException {
		// print version info
		// -------------------------------------------------


	    /*/
		// fetch TOMTE release version
		Scanner scanner = new Scanner(new File(tomteRootPath + File.separator + "release-version.txt"));
		scanner.useLocale(Locale.US); // .useDelimiter("\\s*");;
		double version = 0;
		if (scanner.hasNextDouble()) {
			version = scanner.nextDouble();
		} else {
			logger.warn("couldn't determine release-version");
		}
		scanner.close();
		*/

	    Path path = Paths.get(tomteRootPath, "release-version.txt");
		List<String> lines;
		String version;
        try {
            lines = Files.readAllLines(path);
            version = lines.get(0);
        } catch (IOException e) {
            version="unknown";
        }


		// output TOMTE release version
		PrintWriter versionStream = util.Filesystem.getUtf8FilePrintWriter(outputDir + "/version.txt");
		String release = "TOMTE release-version: " + version;
		versionStream.println(release);
		logger.info(release);
		versionStream.close();
	}


	public static void setupSutInterface(Config config) {
	       if ( config.sutInterface_type.equals("socket") ) {
	        	SutInfo.useSocketCommunication(config.sutInterface_socket_server, config.sutInterface_socket_portNumber);
	        }
	        if ( config.sutInterface_type.equals("jar") ) {
	    		   SutInfo.useMethodCallCommunication(config.sutInterface_jar_package,config.sutInterface_jar_className);
	        }
	        if ( config.sutInterface_type.equals("model")   ) {
                SutInfo.useMethodCallCommunication(config.sutInterface_model_package,config.sutInterface_model_className);
			}

	        boolean sutSimulation_method = config.sutInterface_type.equals("model")  || config.sutInterface_type.equals("jar") ;
			SutInfo.initialize(config.sutInterface_sutinfoFile,
			        sutSimulation_method,
					config.sutInterface_model_file,
					config.learnResults_outputDir,
					config.sutInterface_jar_file);
	}

	public static void initLearner(Config config) throws FileNotFoundException {

		 System.out.println("INITIALIZING TOMTE");

		 // initialize sut utility library
		 //  - wraps tomte python scripts to generate concrete model(=uppaal file) from learned hypothesis
		 //  - wraps sut tool to run concrete model (specified in uppaal file)
		 sut.implementation.Util.init(config);

		 logReleaseVersion(config.params_tomteRootPath, config.learnResults_outputDir );





		// initialize hyp information
		// -------------------------------------------------
		LearnLogger.setHypDir(config.learnResults_outputDir + config.logging_hypDir);


		// initialize sut information
		// -------------------------------------------------
		// note: by SutInfo.initiliaze all needed input info about sut is stored
		// in output folder


		setupSutInterface(config);



		// check if constants are in the range defined by the config file
		// check only possible after SutInfo has been initialized, see code
		// above
		for (int constant : SutInfo.getConstants()) {
			logger.fatal("Constant: " + constant);
			if (constant > config.testing_maxValue
					|| constant < config.testing_minValue) {
				throw new ConfigurationException(Messages.WRONG_CONSTANT_RANGE);
			}
		}


        // initialize fresh value range (excluding constants from SUT as fresh value!)
        // -------------------------------------------------
        // params min_value and max_value
        // specify information about which fresh values are chosen when to learning and testing sut
		RandomRangedValueStore.setFreshValueRange( config.testing_minValue, config.testing_maxValue, config.testing_valuesExtendable );
		RandomRangedValueStore.setConstants(SutInfo.getConstants());

		// initialize constants through class hierarchy
		// --------------------------------------------
		//initializing learning mode
		Relation.activateRelations(config.learning_relations);

	}


	// run from command line in windows :
	// java -cp
	// bin;lib\json_simple-1.1.jar;lib\learnlib-distribution-20110714.jar;lib\snakeyaml-1.9.jar
	// abslearning.learner.Main
	public static void main(String[] args) {

        // init basic logging; later replaced with advanced logging settings using a properties file
        Config.initBasicLogging();


		boolean fatalErrorDetected = false;


		// if --test-class option is given then run that test class instead
		if ( Test.handleArgs(args) ) { Test.runTest();return;}

		boolean printStackTrace=true;
		try {
		         Config  config = getConfig(args); // init and validates config

				 printStackTrace=config.devel_printStackTraceOfExceptions;

				 learn(config); // learn for given config

		} catch ( CheckException ce ) {
			logger.fatal("\n\nDEPENDENCY ERROR:" + ce.getMessage());
			if ( printStackTrace)  ce.printStackTrace();
			fatalErrorDetected = true;
		} catch (ConfigurationException ce) {
			logger.fatal("\n\nCONFIGURATION ERROR:\n     " + ce.getMessage());
			if (printStackTrace) ce.printStackTrace();
			fatalErrorDetected = true;
		} catch (BugException be) {
			logger.fatal(Messages.LEARNING_PROBLEM);
			logger.fatal(be.getMessage());
			if (printStackTrace)  be.printStackTrace();
			fatalErrorDetected = true;
		} catch (UpdateConstantActionException ucae) {
			logger.fatal(ucae.getMessage());
			logger.fatal(Messages.UPDATING_CONSTANT);
			if (printStackTrace) ucae.printStackTrace();
			fatalErrorDetected = true;
		} catch (FileNotFoundException fnfe) {
			logger.fatal(fnfe.getMessage());
			if (printStackTrace) fnfe.printStackTrace();
			fatalErrorDetected = true;

		} catch ( java.lang.AssertionError  e) {
			logger.fatal(Messages.BUG + " : CATCHED a DEBUGGING ASSERTION\n\n");

			// no specific handler match: rethrow e and let java print exception
			//if (printStackTrace) logger.fatal("ASSERT MSG:\n      " + e.getMessage());
		    if (printStackTrace) logger.fatal("ASSERT STACKTRACE:\n" + util.JavaUtil.getStackTraceString(e)); // note: also contains assertin message

		    fatalErrorDetected = true;


		} catch (Exception e) {
			logger.fatal(Messages.BUG);
			// no specific handler match: rethrow e and let java print exception
			if (printStackTrace) logger.fatal(e.getMessage());
			//if (printStackTrace) e.printStackTrace();
			if (printStackTrace) logger.fatal("STACKTRACE:\n" + util.JavaUtil.getStackTraceString(e));


			fatalErrorDetected = true;

		}


	    // finish up
			util.Time.millisleep(3000); // give the late started parallel threads some time to print error code before finishing main
			System.out.flush();
			System.err.flush();
		if ( fatalErrorDetected ) {
			logger.info("\n\n\n        ENDED WITH FATAL ERROR (exitcode=1)");

			System.out.flush();
			System.err.flush();
		    System.exit(1);
		} else {
		util.Time.millisleep(3000); // give the late started parallel threads some time to print error code before finishing main
		logger.info("THE END");
		System.out.flush();
		System.err.flush();
	}

	}
    /**
     * read config from args and config yaml file - args have higher
     * priority then yaml file - using --configfile x.yaml an
     * alternative yaml config file can be specified serialize final
     * config to config yaml file
     * @throws FileNotFoundException
     */
	private static Config getConfig(String[] args) throws FileNotFoundException {

         Config  config = Config.getInstance();   // singleton
         config.initFromTomteCmdlineArguments(args);  // loads config file from configFile param and allows extra overruling config params
         // validates and initialize config for Tomte
         config.validate();
         config.initialize();
         return config;
    }


    public static void learn(Config config) throws Exception {

        // initialize tomte
        initLearner(config);

		// TODO: replace PrintStreams below by above file strings
		PrintStream statisticsFileStream = new PrintStream(new FileOutputStream(
				config.learnResults_outputDir + config.learnResults_statisticsWithReport, false));

		statistics.initStatistics(statisticsFileStream); // sets file stream and creates first run



		logger.info("STARTING TOMTE");
		logger.fatal("STARTING TOMTE");
        logger.info("maximum learning time (seconds): " + config.learning_maxTime);

		statistics.setMaxTime(config.learning_maxTime);


		StepWiseValueStore.setDefaultStep(config.learning_freshValueStep);



		boolean reduceCounterExample = false;
  		if (config.learning_reduceCounterExample) {
  			reduceCounterExample=true;
  			Counterexample.setReductionStrategies(config.learning_reductionStrategies);
  		}


  		PredicateStore predicateStore=  PredicateStore.getInstance();
  		for (ActionSignature sig : SutInfo.getInputSignatures()) {
  			predicateStore.createInputPredicate(sig);
		}

		// generate input alphabet before creating memberOracle, because memberOracle needs abstract inputs
		// for lookahead  initialization which does lookahead using abtract input traces
  		predicateStore.generateInputAlphabet();

  	    // Alphabet for old learnlib
  		Alphabet inputAlphabet=predicateStore.getInputAlphabet();

		// initialize constants
		List<Integer> constants=SutInfo.getConstants();
		Trace.initConstants(constants);
		LookaheadTrace.initConstants(constants);


		AbstractionMapper abstractionMapper= new AbstractionMapper(predicateStore,constants);
		LookaheadTraceStore lookaheadStore = new LookaheadTraceStore(LookaheadConcretizerProvider.PERMUTATIVE);
		if (config.learning_sourcedLookaheads) {
			lookaheadStore.useSourceLts();
		}
		Run run=Run.getInstance();
		// setup concrete tree
		ConcreteTree concreteTree = setupConcreteTreeAndCache(config, constants);

		run.initRun(constants, concreteTree, abstractionMapper, lookaheadStore);



		String learningAlgorithm = config.learning_algorithm;
		LearnerFactory learnerFactory = new LearnerFactory();
		CeStore ceStore = new CeStore();

		Analyzer analyzer = new Analyzer(run, ceStore, predicateStore, lookaheadStore, config.learning_reconstructAbstractions,constants);


		// HypBuilderStore ... => TODO: just make single hyp implementation => get rid of store
		HypothesisBuilder hypBuilder = HypBuilderStore.getHypBuilder(config, predicateStore, abstractionMapper, constants);

		HypTester hypTester = TesterStore.getTester(config, abstractionMapper, inputAlphabet, run.getTraceInstance("direct"));
		HypVerifier hypVerifier = VerifierStore.getVerifier(config);
		int hypCounter = 0;
		boolean done = false;
		boolean firstTime=true;
		Automaton abstractHyp = null;
		while (!done) { //step: rerun with new learner:  relearn from scratch with updated lookahead/abstraction
			// restart learner because new lookahead and or abstraction

			if (firstTime) {
				firstTime=false;
			} else {
				// get new alphabet
				predicateStore.generateInputAlphabet();
				inputAlphabet=predicateStore.getInputAlphabet();

				// set new alphabet in tree
				//run.initObservationTree(inputAlphabet,predicateStore);

			}

			LearnerInterface learner = learnerFactory.buildLearner(inputAlphabet, run, learningAlgorithm);

			try {

				while (!done) {	// step: hyp step in learner
					            //    learn next hypothesis with learner and then use testing to find ce for it

					// TODO
					//  * improve statistics
					//      run counter
					//      hyp counter  -> only one in statistics!!
					//

					//  * in statistics keep version number of
					//      + number of input abstractions  in predicate store
					//        -> after adding input abstraction increment
					//      + number lookahead traces in lookahead store
					//        -> after adding lookahead trace increment

					//  * improve statistics
					//      - if stopping a phase throw an exception when current phase is a different phase!!  ->  extra safety!!
					//      - make cadp checking its own phase in statistics -> time spend not added by any other phases
					//  * reapply ce : first check ce still ce before passing to learnlib !!

					if(config.learning_maxNumRuns != -1 && statistics.getCurrentRunNumber() > config.learning_maxNumRuns) {
						throw new MaxNumRunsException(config.learning_maxNumRuns);
					}

					// ----------------
					// learn hypothesis
					//-----------------

					statistics.startLearning();

					logger.info("Start Learning");


					StringWriter strWriter = new StringWriter();
					LearnLogger.printCurrentAbstraction(predicateStore, strWriter);
					logger.fatal("abstraction:\n" + strWriter.toString());

					logger.info("Before Learning");
					logger.fatal("hypCounter(counts number of hyps found)  " + hypCounter );
					logger.fatal("start learning: " + (hypCounter+1));


					// learn
					learner.startLearning();

					logger.info("Done Learning");
					statistics.stopLearning();

					//----------------------
					// log new learned info
					//-----------------------
					statistics.startProcessLearningResult();


					// increment hypCounter
					hypCounter = hypCounter + 1;
					statistics.incHypLIndex();
					logger.info("Hypothesis " + hypCounter);

					// log abstractions
					LearnLogger.logAbstractions(hypCounter,predicateStore);

					abstractHyp = learner.getHypothesis();
					LearnLogger.logAbstractHypothesisDot(abstractHyp, hypCounter);

					// store statistics of abstract hyp
					statistics.storeNumStatesHypL(abstractHyp.getAllStates().size());
					Automaton reducedHyp = HypothesisReducer.reduceHypothesisToEliminateNonDeterminism(abstractHyp);
					LearnLogger.logAbstractReducedHypothesisDot(reducedHyp, hypCounter);

					TomteHypInterface concreteHyp = hypBuilder.buildHypothesis(abstractHyp, hypCounter);

					// update abstractHyp,abstractReducedHyp,concreteHyp,concreteHypWithFullInfo in mapper
					// note: loads abstract hyps from Hypothesis class, and loads concrete hyps from jarfiles
					logger.info("update hyp in mapper (run)");
					run.setHypothesis(abstractHyp, reducedHyp, concreteHyp);




			        // log concrete tree if configure to do so => debug option!!
					if ( config.logging_special_concreteTree) {
						// replace name.dot with namehypNum.dot so we can have trees for all hyps
						 String treedotfile=config.logging_logDir + config.logging_special_concreteTreeFile.replaceAll(".dot", hypCounter + ".dot");
						 logger.info("output SUT concrete tree info in file: " + treedotfile);
						 StructLogger.printSutConcreteTreeInfo( run.getConcreteTree(),treedotfile );
					}


					logger.info("get number (end)nodes in tree");
					statistics.storeNumEndNodesInTree(concreteTree.getTree().getNumberOfEndNodes());
					statistics.storeNumNodesInTree(concreteTree.getTree().getNumberOfNodes());



					statistics.stopProcessLearningResult();

					if( config.verification_when.equals("afterHyp") && hypVerifier != null) {
						statistics.startVerification();
						HypDesc learnModelDesc = HypDesc.getDescForHyp(hypCounter);
						boolean equal = hypVerifier.verifyHyp(learnModelDesc);
						String verifierName=hypVerifier.getClass().getSimpleName();
						statistics.learningSuccessful = equal;

					    statistics.stopVerification();
						if( equal ) {
						    statistics.terminationReason= " hyp. found CORRECT according to verifier: " + verifierName;
							logger.fatal( verifierName + " verification passed");
							done = true;
							continue;
						} else {
							logger.fatal( verifierName + " verification failed");
						}

					}

					Canonicalizer canonicalizer = new Canonicalizer();

					// ----------------------------------------------------------
					// search for counterexample
					//------------------------------------------------------------

					statistics.startTesting();

				    Tuple2<Trace,OutputAction> ceTuple = null;
                    if (hypTester!= null ) {
			 		  ceTuple= hypTester.testHyp(hypCounter);
                    }

                    statistics.stopTesting();

                    if (ceTuple!=null) {

	                    Trace sutTrace= ceTuple.tuple0;

	                    // store in statistics ce (concrete inputs)
	                    statistics.storeCeEquivOracle(sutTrace);


	                    OutputAction outputSut = sutTrace.getLastOutputAction();
	                    OutputAction outputHyp = ceTuple.tuple1;

	                    try {
	                    	statistics.startAnalysisCounterexample();

	             	    	logger.fatal("Found Counterexample:");
	            			logger.fatal(" |-> trace  to distinguishing output: "+ sutTrace);
	            			logger.fatal(" `-> result of distinguishing output:  SUT " + outputSut + "  NOT equals HYP " + outputHyp + " ??");


            				// reduce counter example
            				//------------------------------

            			    Trace ceConcreteTrace=sutTrace;

            				/*TODO: make simpler reduce counter example function which works on concrete inputs only
            				 *      adds hyp abstract inputs itself
            				 * => TODO: make work on inputs instead of trace , and integrate into function/class
            				 *   and returns smaller concrete counter example inputs without abstractions
            				 */

            				Trace concreteCeTraceWithAlsoHypAbstractions=run.runOnConcreteHypothesisAndAddAbstractionsAndStatevars(ceConcreteTrace.getInputActions());
            				logger.fatal("concreteCeTraceWithAlsoHypAbstractions:\n    " + concreteCeTraceWithAlsoHypAbstractions);

            				if( reduceCounterExample  && !ceStore.hasCe(ceConcreteTrace)) {
            					Counterexample counterexample = new Counterexample(concreteCeTraceWithAlsoHypAbstractions);
            					counterexample.reduceCounterexample();
            					ceConcreteTrace=counterexample.getTrace();
            				}

            				// store in statistics ce inputs after loop reduction
            				statistics.storeCeAfterLoopReduction(ceConcreteTrace);

	            			// canolize the concrete counter example
	            			//---------------------------------------
	            			ceConcreteTrace = canonicalizer.canonicalize(ceConcreteTrace);
	            			logger.info(" Canonicalcounterexample: " + ceConcreteTrace);


	            			// analyze the concrete counter example
	            			//---------------------------------------
	            			analyzer.analyseCounterExample(ceConcreteTrace.getInputActions());

	                    	statistics.stopAnalysisCounterexample();
	                    } catch (LearnerCounterExampleException e) {



	                    	statistics.incNumCounterexamplesForLearner();
	                    	statistics.stopAnalysisCounterexample();
	                    	logger.fatal("caught LearnerCounterExampleException in Main");
							// ----------------------
							// LEARNER counterexample found
							// ----------------------
	                    	Counterexample learnerCounterExample=e.getCounterexample();
							// ----------------------
							// counterexample found
							// ----------------------

		                    // log and validate abstract counterexample
		                    learnerCounterExample.abstractValidationAndLogging(abstractHyp);


							// get counterexample inputs and sut outputs (differs from hypothesis)
		                    EquivalenceOracleOutput equivOracleOutput = learnerCounterExample.getEquivalenceOracleOutput();
							Word ceInputs = equivOracleOutput.getCounterExample();
							Word sutOutputs = equivOracleOutput.getOracleOutput();

							// get hyp outputs from hypothesis
							Word hypOutputs=abstractHyp.getTraceOutput(ceInputs);
							logger.info("Counterexample:" +
							            "\n    ceInputs: " + ceInputs  +
							            "\n    sutOutputs: " + sutOutputs +
							            "\n    hypOutputs: " + hypOutputs);

							// found ce. with testing must be a valid ce
							assert !hypOutputs.equals(sutOutputs)  : "something wrong with testing : found counter example is NOT a counter example!";

							// log counter example
							//logger.info("Found Counterexample: " + equivOracleOutput.getCounterExample().toString());
							logger.info("Send Counterexample to LearnLib");

							// -----------------------------------------------------------------
							// give counter example to learner
							statistics.startAddCounterexample();


		                    /* addCounterExample
		                     *  1. validate counterexample is indeed not a trace of hypothesis.
		                     *     If not immmediately returns false.
		                     *  2. find suffixes using the createSplitters method of a SplitterCreator class (see below)
		                     *     If none find, return false.
		                     *  3. apply suffixes in observation table, and then returns true
		                     *
		                     * We use the createSplitters method of the RivestStyleSplitterCreator:
		                     *  Description:
		                     *         from counterexample returns suffix(es) to be added to observation table
		                     *  Does:
		                     *  1. search for shortest input sequence which is still a counter example
		                     *  2. finds largest prefix (of orignal counterexample inputs) which can
		                     *     be replace by access sequence which is still a counter example.
		                     *  3. suffix = remove largest prefix from counterexample
		                     *  4. note that in step 2. we could have found a total different counterexample
		                     *     Therefore we do extra step to try to shorten the suffix.
		                     *  5. return suffix   (as only element in list)
		                     */

							logger.debug("just before learner.addCounterExample(ceInputs, ceOutputs)");
							logger.info("addCounterExample inputs: " + ceInputs.toString() + " outputs: " + sutOutputs.toString() );

							learner.addCounterExample(ceInputs, sutOutputs);


							statistics.stopAddCounterexample();

							// start new run (repeat this loop)
							statistics.startNewRun(false);
							continue;
						}
                    }

					// ----------------------------------------------------------
					// when no counterexample found  : finished learning
					//------------------------------------------------------------

					// no counter example -> learning is done
					// break out of both while loops when no counter example
					// found : learning is done!



				    statistics.terminationReason="Couldn't find counterexample after maxNumTraces applied";

					// stop learning!
					done = true;
	    		        statistics.addDescriptionTerminationRun(statistics.terminationReason);
					logger.fatal(statistics.terminationReason);

				} // end learner run

		// only first catch does restart learning with new learner  (other catches end learning!)
			} catch (RestartLearningException ex) {
				Phase currentPhase=statistics.getCurrentPhase();
				logger.info("Catched RestartLearningException in Main during phase: "+ currentPhase);

				statistics.stopAnyPhase();

				String type=ex.type;
				if ( type.equals("newLookaheadTraceFoundByCe") ) {
						logger.info("Inconsistent concrete tree. Restart learning with new lookahead traces!");
				} else	if ( type.equals("newInputAbstractionFoundByCe") ) {
						logger.info("Done testing; abstraction refinement ");
						if ( abstractHyp != null ) statistics.storeNumStatesHypL(abstractHyp.getAllStates().size());
				} else	if ( type.equals("relabeledInputAbstractions") ) {
                     // afterLearning : relabeling hypM
					logger.info("Done Learning; hypm relabeling done");
				} else	if ( type.equals("newLookaheadTraceFoundByInconsistencyInTree") ) {
					logger.info("Inconsistent concrete tree. Restart learning with new lookahead traces!");
				} else {
					statisticsFileStream.close();
					throw new BugException("unknown RestartLearningException type");
				}
				logger.info("Restart Learning");
                // `-> start new run in which we learn from scratch with new abstractions (thus also start building new abstract tree!)
				statistics.startNewRun(true);
				// reset tracestore  : note: abstract trace contains same data as old observation table
				//                           both become invalidate by adding a new abstraction rule
				//                     note: for firstlast mode all output abstractions were fixed at beginning
				//                           however for state mode output abstractions are found on the fly
				//                           and cause an invalidation by a RestartLearningException
				//                           ( thrown in PredicateStore when adding abstraction!)
				logger.fatal("RESET TRACE STORE");

				// restart learning with new learner


		// all catch below end learning
			} catch (MaxNumRunsException ex) {
				// we did not finish learning within specified number of runs, so the process is terminated
				statistics.terminationReason=ex.getMessage();
				logger.info(statistics.terminationReason);
				done = true;
				statistics.learningSuccessful = false;
    		        statistics.addDescriptionTerminationRun("max run times exceeded");
    		    //statistics.finished();
		    } catch (OutOfTimeException ex) {
				// we did not finish learning within specified time, so we process is stopped at that time artificially
				statistics.terminationReason=ex.getMessage();
				logger.info(statistics.terminationReason);
				// note: if we verify if we are successfull
				done=true;
    		        statistics.addDescriptionTerminationRun("max learning time passed");
    		    //statistics.finished();
			} catch (LearningException ex) {
				// learnlib gives learning problem : meaning we have a bug somewhere in our learning algorithm!
				logger.error("LearningException in Main");
				ex.printStackTrace();
				statisticsFileStream.close();
				throw new BugException("learnlib throws learning exception: we have a bug in our learning algorithm" );
			}
		}


		if (hypVerifier != null && config.verification_when.equals("atEnd")) {
			statistics.startVerification();
			HypDesc hypDesc = HypDesc.getDescForHyp(hypCounter);
			boolean equal = hypVerifier.verifyHyp(hypDesc);
			String verifierName=hypVerifier.getClass().getSimpleName();
			if( equal ) {
				logger.fatal( verifierName + " verification passed");
			} else {
				logger.fatal( verifierName + " verification failed");
			}
			statistics.learningSuccessful = equal;
		    statistics.terminationReason="Correct hyp. found according to verifier: " + verifierName;
		    statistics.stopVerification();
		}

		copyFilesToOutputDir(config, hypCounter);

		// output all kinds of statistics
		// ------------------------------------------

		statistics.finished();  // finish statistics in statistics.txt
	    statisticsFileStream.close();

		logStatsAndFinalResult(config, statistics);

		logSpecial(config, run);

	}

    private static ConcreteTree setupConcreteTreeAndCache(Config config, List<Integer> constants) {
  		ConcreteTree concreteTree;
  		if (config.cache_loadFrom != null && new File(config.cache_loadFrom).exists()) {
  			TreeLoader loader = new TreeLoader(statistics, constants);
  			concreteTree = loader.loadTreeAndUpdateStats(config.cache_loadFrom);
  		} else {
  			concreteTree = new ConcreteTree(constants);
  		}
  		if (config.cache_dumpTo != null) {
  			TreeDumper dumper = new TreeDumper(concreteTree, statistics, constants);
  			dumper.setupTreeDumperHook(config.cache_dumpTo);
  		}
  		return concreteTree;
  	}


	public static void copyFilesToOutputDir (Config config, int hypCounter) {
		//TODO * Generate xml file for abstract hyp
		if (!Relation.SUCC.isActive() && !Relation.NEXT.isActive()) {
			LearnLogger.logAbstractHypothesisXml(hypCounter);
			util.Filesystem.copyfile(HypStore.getConcreteHypXmlFile(hypCounter), config.learnResults_outputDir  + config.learnResults_learnedConcreteModelFile);
		}
		util.Filesystem.copyfile(HypStore.getAbstractReducedHypothesisDotFile(hypCounter), config.learnResults_outputDir + config.learnResults_abstractModelDotFile);
		util.Filesystem.copyfile(HypStore.getAbstractionsJsonFile(hypCounter), config.learnResults_outputDir + config.learnResults_learnedModelDataFile);
		util.Filesystem.copyfile(HypStore.getAbstractionsTxtFile(hypCounter), config.learnResults_outputDir + config.learnResults_abstractionFile);
	}


	public static void logStatsAndFinalResult(Config config, Statistics statistics) throws FileNotFoundException {

	    // log statistics in json file
		PrintStream statisticsJsonFileStream = new PrintStream(new FileOutputStream(
				config.learnResults_outputDir + config.learnResults_statisticsJsonFile, false));
		statistics.printJsonStatistics(statisticsJsonFileStream, true);
		statisticsJsonFileStream.close();

	    PrintStream statisticsTxtFileStream = new PrintStream(new FileOutputStream(
	                config.learnResults_outputDir + config.learnResults_statisticsFile, false));
	    statistics.printStatistics(statisticsTxtFileStream, true);
	    statisticsTxtFileStream.close();

        PrintStream ceAnalysisTxtFileStream = new PrintStream(new FileOutputStream(
                config.learnResults_outputDir + config.learnResults_ReportFile, false));
        statistics.printStatisticsCeAnalysis(ceAnalysisTxtFileStream);
        ceAnalysisTxtFileStream.close();


		// print statistics to console
		//statistics.printStatistics(System.out, true);

		logger.info("Seed: " + Long.toString(config.testing_seed));

		// we assume we can check if  hyp. is correct with cadp
		// * if correct with cadp :
		//   => SUCCES : correct hyp. found
		// * else we are are still searching and when learning gets terminated then :
		//   => FAILURE : no correct hyp. found
		//      two cases :
		//        - maxtime reached   -> outoftimeexception
		//        - maxtraces reached -> no counter example found after testing

		if ( statistics.learningSuccessful == null  ) {
			    logger.info("TOMTE FINISHED learning: learned model cannot be verified to be correct");
		} else if (statistics.learningSuccessful) {
			logger.info("TERMINATION REASON: " + statistics.terminationReason);
			logger.info("TOMTE SUCCESSFUL in learning: VERIFIED CORRECTNESS of learned model");
		} else {
		        logger.info("TOMTE FAILED in learning according to verifier " );
		}
	}

	public static void logSpecial(Config config, Run run) {
		 /*
		 print python version as check the right python version is used
		 note: you can force which python version is used
		       by amending the PATH environment variable
		 */
		 //RunCmd.runCmd("python --version",stdout,false);

		 if ( config.logging_special_concreteTree) {
			 String treedotfile=config.logging_logDir + config.logging_special_concreteTreeFile;
			 logger.info("output SUT concrete tree info in file: " + treedotfile);
			 StructLogger.printSutConcreteTreeInfo( run.getConcreteTree(),treedotfile );
		 }

        if ( config.logging_special_concreteTreeStatistics ) {
   		  // print state statistics
       	  // String treedotfile="concreteTree.dot";
   		  // Log.printSutConcreteTreeInfo( traceWarehouse.getConcreteTree(),treedotfile );

       	  String treeStatisticsfile=config.logging_logDir + config.logging_special_concreteTreeStatisticsFile;
		      logger.info("output SUT concrete tree statistics in file: " + treeStatisticsfile);
		      StructLogger.outputTreeStatisticsInJson(treeStatisticsfile,run.getConcreteTree());


    		  if ( config.logging_special_plotConcreteTreeStatistics ) {

    			 logger.info("plot SUT concrete tree statistics from file: " + treeStatisticsfile);
   			 String[] cmd = {config.dependency_pythonCmd , config.params_tomteRootPath + "/share/pymodules/plot.py",  treeStatisticsfile }; // note: python can interpret unix paths!
   			 util.RunCmd.runCmd(cmd, System.out, System.err , false,false); // use this blocked calln instead because
   		  }
        }
	}


}
