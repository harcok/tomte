package abslearning.learner;

import java.util.Arrays;
import java.util.List;

import abslearning.exceptions.ConfigurationException;
import de.ls5.jlearn.interfaces.Alphabet;

public class LearnerFactory {
    private static final List<String> oldLearnLibAlgorithms = Arrays.asList("angluin", "observationpack");
    private static final List<String> newLearnLibAlgorithms = Arrays.asList("ttt");

    public LearnerInterface buildLearner(Alphabet inputAlphabet, Run run, String learningAlgorithm) {
        learningAlgorithm = learningAlgorithm.toLowerCase();
        if (newLearnLibAlgorithms.contains(learningAlgorithm)) {
            return new NewLearnLibAdapter(inputAlphabet, run, learningAlgorithm);
        } else {
            if (oldLearnLibAlgorithms.contains(learningAlgorithm)) {
                return new OldLearnLibAdapter(inputAlphabet, run, learningAlgorithm);
            } else {
                throw new ConfigurationException("unsupported learning algorithm:" + learningAlgorithm);
            }
        }
    }
}
