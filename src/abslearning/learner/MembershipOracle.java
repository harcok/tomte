package abslearning.learner;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.trace.action.OutputAction;
import de.ls5.jlearn.abstractclasses.LearningException;
import de.ls5.jlearn.interfaces.Oracle;
import de.ls5.jlearn.interfaces.Word;
import de.ls5.jlearn.shared.SymbolImpl;
import de.ls5.jlearn.shared.WordImpl;

public class MembershipOracle implements Oracle {

	private static final long serialVersionUID = 5045750098594315331L;
	private static final Logger logger = Logger.getLogger(MembershipOracle.class);
	private static final Logger logger_memTraces = Logger.getLogger("memTraces");

	private static final Statistics statistics= Statistics.getInstance();
	private Run run;

    public MembershipOracle(Run run) {
		this.run=run;
	}


	/*  processQuery
	 *
	 */
	public Word processQuery(Word query) throws LearningException {

		abslearning.trace.Trace currentTrace = run.runFreshTraceOnSut(query);
		ArrayList<OutputAction> outputs = (ArrayList<OutputAction>) currentTrace.getOutputActions();

		Word result = new WordImpl();
		for (OutputAction output : outputs) {
				result.addSymbol(new SymbolImpl(output.getAbstractionString()));
		}

        if ( logger_memTraces.isDebugEnabled() ) {
            logger_memTraces.debug("Run.trace: " + statistics.getCurrentRunNumber() + "." + statistics.getNumMemQueriesLearning() + ": "  + query + " -> " + result);
            logger_memTraces.debug("Run.trace: " + statistics.getCurrentRunNumber() + "." + statistics.getNumMemQueriesLearning() + ": "  + currentTrace);
        }

		return result;
	}

}
