package abslearning.learner;

import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.determinizer.Canonicalizer;
import abslearning.exceptions.BugException;
import abslearning.exceptions.InconsistencyException;
import abslearning.exceptions.Messages;
import abslearning.lookahead.LookaheadStore;
import abslearning.lookahead.trace.LookaheadTraceStore;
import abslearning.mapper.AbstractionMapper;
import abslearning.mapper.StateVar1Mapper;
import abslearning.mapper.StateVarMapper;
import abslearning.mapper.StateVarNMapper;
import abslearning.trace.Trace;
import abslearning.trace.Transition;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.tree.ConcreteTree;
import abslearning.tree.InconsistencyHandling;
import abslearning.tree.trace.SutTraceInterface;

//import abslearning.tree.trace.SutTreeInterface;
//import abslearning.tree.trace.SutTreeTrace;
//import abslearning.tree.trace.TreeAsCacheSutTrace;

import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.interfaces.Word;
import sut.info.TomteHypInterface;
import util.Tuple2;

/**
 * Class which provides access to the abstract, reduced and concrete implementations of each hypothesis
 * as well as interfaces to the sut.
 * 
 * It also implements various methods of running full sequences of inputs on the sut which are used during
 * testing and ce analysis.
 */
public class Run {
	private static final Logger logger = Logger.getLogger(Run.class);
	private static final Statistics statistics = Statistics.getInstance();
	
	// members
	//--------
	private Automaton abstractHyp = null;
	private Automaton abstractReducedHyp = null; // only set: not yet used, except for concrete implementation of hyp!!

	
	public SutTraceInterface directSutTrace;
	public SutTraceInterface treeSutTrace;
	public SutTraceInterface ceTreeSutTrace;
		
	private  TomteHypInterface concreteHyp = null;
	
	public AbstractionMapper abstractionMapper;
	public StateVarMapper stateVarMapper;	
	
	
	private List<Integer> constants;
	private ConcreteTree concreteTree;
	private LookaheadStore lookaheadStore;
	
	public	Automaton getAbstractHyp() {
		return abstractHyp;
	}
	
    // sul for newlearnlib
	private SulInterfaceForSut sul;
//=======
	public Automaton getAbstractReducedHyp() {
		return abstractReducedHyp;
	}
	
	public TomteHypInterface getConcreteHyp() {
		return concreteHyp;
	}
	
    // singleton
    // ---------
    private static final Run INSTANCE = new Run();

    public static Run getInstance() {
        return INSTANCE;
    }

    // constructor cannot be called because private => we can only get single
    private Run() {
    }

    // method to initialize things in singleton
    public void initRun( List<Integer> constants, ConcreteTree concreteTree, AbstractionMapper abstractionMapper, LookaheadStore lookaheadStore) {
	 
    	this.constants=constants;
    	this.abstractionMapper= abstractionMapper;
    	
		this.stateVarMapper = new StateVarNMapper();
    	
    	this.concreteTree = concreteTree;
    	this.lookaheadStore = lookaheadStore; 
    	
    	this.directSutTrace = getTraceInstance("direct");		
    	this.treeSutTrace = getTraceInstance("tree");
    	
    	this.ceTreeSutTrace = new SutBuilder().addDeterminizer().addTrace("lookahead").addLookaheadStore(lookaheadStore).
    			addTree(this.concreteTree).setInconsistencyHandler(InconsistencyHandling.VERIFY_RESTART_LEARNING).build();
    	
    	
    	// setup sul for newlearnlib
    	sul=new SulInterfaceForSut(this);
    }
    
//	public SutConcreteTree getConcreteTree(){
//	     return this.concreteTree;
//    }
	public ConcreteTree getConcreteTree(){
		return this.concreteTree;
	}
	
    public SutTraceInterface getTraceInstance(String sutTraceType) {
    	SutTraceInterface trace;    	
    	SutBuilder sutBuilder = new SutBuilder().
    			addDeterminizer();
    	
    	if ( sutTraceType.equals("tree")  ) {
    		// use tree as cache and also execute lookahead to fetch memorable values
            trace = sutBuilder.addTrace("lookahead").addTree(this.concreteTree).addLookaheadStore(lookaheadStore).
            		setInconsistencyHandler(InconsistencyHandling.VERIFY_RESTART_LEARNING).build();
    	} else if ( sutTraceType.equals("direct")  )  {
    		// Send input to SUT directly and get output directly back (direct : no caching nor tree in between)
    		trace=  sutBuilder.addTrace("direct").build();
		} else if (sutTraceType.equals("cached")) {
			trace = sutBuilder.addTrace("cached").addTree(this.concreteTree).build(); 
		} else {
			throw new BugException("unknown type of Sut Trace instance: " + sutTraceType);
		}
    	
		return trace;			
    }    
    
    
    
	public void setHypothesis(Automaton abstractHyp, Automaton reducedHyp, TomteHypInterface concreteHyp) {
		this.abstractHyp = abstractHyp;
		this.abstractReducedHyp = reducedHyp;
		this.concreteHyp = concreteHyp;
	}		    

	public void increaseVersionOfObservationTree() {
		  this.concreteTree.increaseVersion();
	}
	
 	public void storeAbstractCEinStatistics( Trace freshTraceOnSut ){
 		
		// TODO: instead pass new AbstractCounterExample class to statistics
		Word abstractCeInputs = freshTraceOnSut.getAbstractInputWord();
		Word abstractCeOutputsSut = freshTraceOnSut.getAbstractOutputWord();
		Word abstractCeOutputsHyp =   runOnAbstractHypothesis(abstractCeInputs);
		// add data to statistics of ce analysis
    	statistics.storeAbstractCE(abstractCeInputs, abstractCeOutputsSut, abstractCeOutputsHyp);		
 		
 	}
 	
//-------------------------------------------------------------------------------------------------------
//	 run on abstract Hypothesis
//-------------------------------------------------------------------------------------------------------
 	
	public Word runOnAbstractHypothesis(Word abstractInputs) {		
		Word abstractOutputs =   this.abstractHyp.getTraceOutput(abstractInputs);
		return abstractOutputs;
	}		
 	

//-------------------------------------------------------------------------------------------------------
//  run on concrete Hypothesis   
//-------------------------------------------------------------------------------------------------------

	
	
	/** runs concrete copies of given Inputs on concrete hypothesis and returns
	 * a newly created Trace with concrete inputs and outputs */
	public Trace runOnConcreteHypothesis(List<InputAction> concreteInputs) {
		
		Trace resultTrace= new Trace();
		
		this.concreteHyp.sendReset();
		for(InputAction origConcreteInput : concreteInputs){

			InputAction  concreteInput=new InputAction(origConcreteInput.getConcreteParameters(),origConcreteInput.getMethodName()); // concrete copy 
			
			// get concrete output from concrete hypothesis
			OutputAction concreteOutput = this.concreteHyp.sendInput(concreteInput);
			
			resultTrace.add(concreteInput);
			resultTrace.add(concreteOutput);
			
		}

		return resultTrace;
	}		
	
	
	/** runs concrete copies of given inputs on concrete full hypothesis and returns
	 * a newly created Trace with both concrete and abstract inputs and outputs and also statevars 
	 */
	public Trace runOnConcreteHypothesisAndAddAbstractionsAndStatevars(List<InputAction> concreteInputs) {
		
		Trace resultTrace= new Trace();
		
  	
  	// get special hypothesis which gives Abstract Inputs as Output
     this.concreteHyp.sendReset();       				
		
		for(InputAction origConcreteInput : concreteInputs){
			InputAction  input=new InputAction(origConcreteInput.getConcreteParameters(),origConcreteInput.getMethodName()); // concrete copy 
			OutputAction output=this.concreteHyp.sendInput(input);
			resultTrace.add(input); 
			resultTrace.add(output);	
			resultTrace.updateStatevars(this.concreteHyp.getStatevars());
		}
		return resultTrace;
	}			

	
 
//-------------------------------------------------------------------------------------------------------
//	run on SUT   
//-------------------------------------------------------------------------------------------------------
	 	
	
    
    
	public abslearning.trace.Trace runFreshTraceOnSut(Word query){ 	

	       sul.pre();
	       for (Symbol currentSymbol : query.getSymbolList()) {
	    	   sul.step(currentSymbol);
	       }
	       sul.post();
	       
	       return sul.getTrace();
	}
	
	
 	

	/**  run any concrete trace directly on SUT
	 *   
     *   returns
     *     a newly created Trace with concrete inputs and outputs 
     *     
     *   note: doesn't modify  given inputs on input  
	 **/
	public Trace runOnConcreteSut(List<InputAction> concreteInputs) {
		

		Trace result = new Trace();			
		directSutTrace.sendReset();
		for(InputAction origConcreteInput : concreteInputs){

			InputAction  concreteInput=new InputAction(origConcreteInput.getConcreteParameters(),origConcreteInput.getMethodName()); // concrete copy 
			

			result.add(concreteInput);
				
			OutputAction concreteOutput = directSutTrace.sendInput(concreteInput,result);

			result.add(concreteOutput);
       }
       return result;		
	}	 	
 	
    /** run any concrete trace on SUT  (via observation tree with lookahead)
     *  for given:
     *     concrete inputs
  	 *  adds :
  	 *     concrete outputs
  	 *     abstract inputs and outputs
  	 *  note:
  	 *    uses lookahead to determine statevars which it needs to determine the abstract inputs and outputs   
  	 * 
  	 */
  	public abslearning.trace.Trace runOnConcreteSutAddStateVarsAndAbstractions(List<InputAction> concreteInputs){ //throws LearningException {
  		abslearning.trace.Trace resultTrace = new abslearning.trace.Trace(); 
  		treeSutTrace.sendReset();
  		
  		statistics.incMemQueries();
  		for(InputAction origConcreteInput : concreteInputs){
  		    statistics.incMemInputs();
  		    
  		    // first make a copy of input because we are going to change it,
  		    // and we don't want to change original input  
  			// Thus concrete copy orig input:
  			InputAction  input=new InputAction(origConcreteInput.getConcreteParameters(),origConcreteInput.getMethodName());  
  						

      
      		resultTrace.add(input);	
      		
  			// abstract the concrete input 
  		    //   using the known abstractions set the abstract input for the concrete input 
  		    this.abstractionMapper.abstractInput(input, resultTrace);
  			    

  			// send concrete input to treeSutTrace
      		OutputAction output = treeSutTrace.sendInput(input,resultTrace);
      		
      		resultTrace.add(output);	
      		
      		// update statevars in trace
      		this.stateVarMapper.determineStateVarUpdatesFromMemVs(input, resultTrace);
  		    		
      		
      		//get abstract output for concrete output
  	    	String abstractOutput=this.abstractionMapper.abstractOutput(input, output, resultTrace);
  	    	output.setAbstractionString(abstractOutput);	    

  		}
  	    return resultTrace;
  	}
  	

  	public abslearning.trace.Trace decorateStateVarTraceWithAbstractions(abslearning.trace.Trace traceWithStateVars) {
  		// make a concrete copy
  		Trace traceWithAbstractions = new Trace(traceWithStateVars, true);
  		for (Transition trans : traceWithAbstractions.eachTransition()) {
  			// abstract the concrete input 
  		    //   using the known abstractions set the abstract input for the concrete input 
  		    this.abstractionMapper.abstractInput(trans.input, traceWithAbstractions.getStatevarsBeforeTransition());
  		    
  		    traceWithAbstractions.addMemV(traceWithStateVars.getMemvAfterTransition(trans.index));

  		    // update statevars in trace
      		this.stateVarMapper.determineStateVarUpdatesFromMemVs(trans.input, traceWithAbstractions);
      		
      		//get abstract output for concrete output
  	    	String abstractOutput=this.abstractionMapper.abstractOutput(trans.input, trans.output, traceWithAbstractions);
  	    	trans.output.setAbstractionString(abstractOutput);	
  		}
  		return traceWithAbstractions;
  	}
  	
  	
  	public abslearning.trace.Trace runOnConcreteSutAndAddStateVars(List<InputAction> concreteInputs){ 
  		abslearning.trace.Trace resultTrace = new abslearning.trace.Trace(); 
  		ceTreeSutTrace.sendReset();
  		
  		statistics.incMemQueries();
  		for(InputAction origConcreteInput : concreteInputs){
  			statistics.incMemInputs();
  		    
  		    // first make a copy of input because we are going to change it,
  		    // and we don't want to change original input  
  			// Thus concrete copy orig input:
  			InputAction  input=new InputAction(origConcreteInput.getConcreteParameters(),origConcreteInput.getMethodName());  
  						
      		resultTrace.add(input);
      		
	  		// send concrete input to treeSutTrace
	      	OutputAction output = ceTreeSutTrace.sendInput(input,resultTrace);
      
      		// update statevars in trace
      		this.stateVarMapper.determineStateVarUpdatesFromMemVs(input, resultTrace);
      		
      		resultTrace.add(output);	

  		}
  	    return resultTrace;
  	}
  	
	
//-------------------------------------------------------------------------------------------------------
//	    check inputs gives counter example   
//-------------------------------------------------------------------------------------------------------
			
	
	/*
	 * check if given abstract inputs gives an abstract counter example 
	 * 
	 * That is we check if abstract outputs from abstract hypothesis and
	 * abstract outputs from sut are the same.
	 * 
	 * Note: 
	 *  - abstract inputs are concretized fresh
	 *  - the concretized inputs are run on the sut on stored in observation
	 *  - the concretized outputs are abstracted using information from observation tree, 
	 *    where the observation tree also contains information retreived by lookahead.
	 *  
	 * returns
	 *    null:	 if not counterexample
	 *    abstract inputs of ce  (TODO: could be shorter than original abstract inputs!)
	 * TODO: is it possible/what happens if the only difference is in the update: 
	 * aka: x=f_OK_f and OK_f     
	 */
	public Word checkIsAbstractCounterExample(Word abstractInputsWord) {
		Trace freshtraceOfCeOnSut= runFreshTraceOnSut(abstractInputsWord);
		 
		 Word freshOutputsSut=freshtraceOfCeOnSut.getAbstractOutputWord();
      Word freshOutputsHyp = abstractHyp.getTraceOutput(abstractInputsWord);
      
      boolean isAbstractCe = !freshOutputsSut.equals(freshOutputsHyp);
		    
		String msg="\n    freshTraceOnSut:  " +
				   "\n           "+ freshtraceOfCeOnSut +
				   "\n    abstractInputs:  " + abstractInputsWord +
		           "\n       OutputsHyp: " + freshOutputsHyp +	    		
		           "\n       OutputsSut: " + freshOutputsSut ;
		logger.fatal("\n" + msg +"\n\n");
	     
		if (isAbstractCe) {
				// if after fresh run still a counter example: resolve ce by learnlib
				String msg2="\n\n   => give back to learner the abstract CE :" + msg ;
			    statistics.addDescriptionRun(msg2); 	    
				logger.fatal(msg2);
				return abstractInputsWord;
		 }			
		return null;
	}		
	

		/**
		 * Checks if given concrete inputs gives a counterexample.
		 * 
		 *    runs inputs directly on SUT and on concrete hyp to check if concrete outputs still equal
		 *    returns the so far executed trace if outputs differ, and returns null if outputs never differ.
		 *    
		 * Procedure aborts if an input cannot be decanonized and null is returned. An invalid sequence of
		 * inputs does not make a counterexample. 
		 * 
		 * @param  concreteInputs 
		 * @return 
		 *      null if outputs never differ  (not ce)
		 *      the so far executed sut trace and the last hyp output which differs from last output in sut trace
		 *      note: a shorter ce trace could be returned! 
		 *  
		 */
	 public Tuple2<Trace,OutputAction> checkIsCounterExample(List<InputAction> concreteInputs) {

		abslearning.trace.Trace sutTrace = new abslearning.trace.Trace(); 
		

		directSutTrace.sendReset();		
		this.concreteHyp.sendReset();

		try {
			for (InputAction concreteInput : concreteInputs) {
				sutTrace.add(concreteInput);	
			
				OutputAction outputSut = directSutTrace.sendInput(concreteInput,sutTrace);
				OutputAction outputHyp =  this.concreteHyp.sendInput(concreteInput); 
				
				if(! outputSut.concreteEquals(outputHyp) ){ 
					
				    //  currentTrace is ce	
					sutTrace.add(outputSut);
					logger.fatal("\nnew ce: " + sutTrace);
					return new Tuple2<Trace,OutputAction>(sutTrace,outputHyp); //sutTrace
					
				}
				                                                                
				
				sutTrace.add(outputSut); // or add outputHyp  which is equal outputSut
			}
			
		 }catch(abslearning.exceptions.ValueCannotBeDecanonizedException e) {
			// not ce
		} catch (BugException bug) {
			bug.addDecoration("sutTrace:", sutTrace);
			throw bug;
		}
		logger.fatal("not a ce: " + sutTrace);
	    //all outputs the same =>   not ce
	    return null; // means not ce
	}
	 
	 public Tuple2<Trace,OutputAction> checkIsCounterExampleCached(List<InputAction> concreteInputs) {

	        abslearning.trace.Trace sutTrace = new abslearning.trace.Trace(); 
	        SutTraceInterface sut = new SutBuilder().addDeterminizer().addTrace("cached")
	                .addTree(this.concreteTree).build();

	        sut.sendReset();     
	        this.concreteHyp.sendReset();

	        try {
	            for (InputAction concreteInput : concreteInputs) {
	                sutTrace.add(concreteInput);    
	            
	                OutputAction outputSut = sut.sendInput(concreteInput,sutTrace);
	                OutputAction outputHyp =  this.concreteHyp.sendInput(concreteInput); 
	                
	                if(! outputSut.concreteEquals(outputHyp) ){ 
	                    
	                    //  currentTrace is ce  
	                    sutTrace.add(outputSut);
	                    logger.fatal("\nnew ce: " + sutTrace);
	                    return new Tuple2<Trace,OutputAction>(sutTrace,outputHyp); //sutTrace
	                    
	                }
	                                                                                
	                
	                sutTrace.add(outputSut); // or add outputHyp  which is equal outputSut
	            }
	            
	         }catch(abslearning.exceptions.ValueCannotBeDecanonizedException e) {
	            // not ce
	        } catch (BugException bug) {
	            bug.addDecoration("sutTrace:", sutTrace);
	            throw bug;
	        }
	        logger.fatal("not a ce: " + sutTrace);
	        //all outputs the same =>   not ce
	        return null; // means not ce
	    }
	 
	 /**
	  * Checks if the sequence of inputs is a ce, caches, but also canonicalizes the trace after each input sent. 
	  * The returned trace in case of ce may have different input than the one given as param. The reasoning is that
	  * by toggling values to fresh, some canonized concrete inputs might be left dangling because the output they 
	  * referred to is no longer there.
	  * Ex: in(0) / out(-10) in(-11) / out(-11) in(-12)
	  * toggling -11(in) to fresh while preserving all other relations might result in the trace:
	  *   in(0) / out(-10) in(10) / noout() in(-12)
	  *   Which is invalid
	  */
	 public Tuple2<Trace,OutputAction> checkIsCounterExampleCachedAndFixInputs(List<InputAction> concreteInputs) {

         abslearning.trace.Trace sutTrace = new abslearning.trace.Trace(); 
         SutTraceInterface sut = new SutBuilder().addDeterminizer().addTrace("cached")
                 .addTree(this.concreteTree).build();
         Canonicalizer canonizer = new Canonicalizer();

         sut.sendReset();     
         this.concreteHyp.sendReset();

         try {
             for (InputAction concreteInput : concreteInputs) {
                 sutTrace.add(concreteInput);
                 sutTrace = canonizer.canonicalize(sutTrace);
                 concreteInput = (InputAction) sutTrace.get(sutTrace.size()-1);
             
                 OutputAction outputSut = sut.sendInput(concreteInput,sutTrace);
                 OutputAction outputHyp =  this.concreteHyp.sendInput(concreteInput); 
                 
                 if(! outputSut.concreteEquals(outputHyp) ){ 
                     
                     //  currentTrace is ce  
                     sutTrace.add(outputSut);
                     logger.fatal("\nnew ce: " + sutTrace);
                     return new Tuple2<Trace,OutputAction>(sutTrace,outputHyp); //sutTrace
                     
                 }
                                                                                 
                 
                 sutTrace.add(outputSut); // or add outputHyp  which is equal outputSut
             }
             
          }catch(abslearning.exceptions.ValueCannotBeDecanonizedException e) {
             // not ce
         } catch (BugException bug) {
             bug.addDecoration("sutTrace:", sutTrace);
             throw bug;
         }
         logger.fatal("not a ce: " + sutTrace);
         //all outputs the same =>   not ce
         return null; // means not ce
     }

	/* determineRealAbstractHypParamsForConcreteInputs
	 
	   for each concrete input parameter it determines the abstraction according to the hyp.
	   Note: just running on runOnConcreteHypothesisAndAddAbstractions is not sufficient,
	         because sometimes in some transitions an input param abstraction (eg.x0) is defined
	         for the current input value however it is not significant for the hyp behaviour
	         and therefore is taken similar with the -1 abstraction which is returned instead.
	         However this function does is determining the real input abstraction (x0) instead
	         of the reduced -1 abstraction. 
	         Note: we are interested for the abstraction of a parameter, and not for the
	              transition as a whole!
	
	
	 Explanation:
	 
	 To concretize hyp we need to remove redundant abstract input transitions otherwise the machine would 
	 become undeterministic.
	 For login model
	   in case we learned  for param p0  of ILogin abstraction x0 
	   we get from runOnConcreteHypothesisAndAddAbstractions : (thus with redundant transitions removed)
	   
	         IRegister_-1_-1(0,1) / x0=p0_OOK() -> ILogin_-1_-1(0,1) / x0_ONOK()
	   
	   Note that the  ILogin(0,) relation with IRegister(0,) is covered by x0 abstraction, however as long
	   x1 abstraction is not found  ILogin_x0 is redundant because you will get always NOK after login. 
	   (that is you must choose both username and password correct).
	   
	   However when analyzing the counterexample we want to know which params are already covered by 
	   a found abstraction so that we can skip it. Thus we want the presentation :
	          
	      IRegister_-1_-1(0,1) / x0=p0_OOK() -> ILogin_x0_-1(0,1) / x0_ONOK()
	
	  says that for this concrete trace  ILogin(0, is already covered by an abstraction and doesn't need
	  resolving anymore!!!
	
	
	
	*/ 
	public Trace determineRealAbstractHypParamsForConcreteInputs(List<InputAction> concreteInputs) {
		
		/* runs concrete inputs  on hyp 
		 *  using  the statevars it recalculates the input abstractions
		 *  where for each param  the REAL abstract param is calculated (and not the reduced version!)
		 *  Then return these abstract input params!
		*/		
		
		
		Trace hypTraceWithAbstractions=this.runOnConcreteHypothesisAndAddAbstractionsAndStatevars(concreteInputs);
		List<InputAction> inputs = hypTraceWithAbstractions.getInputActions();
		
		Trace resultTrace= new Trace();
		for(InputAction input : inputs){
	
	
			resultTrace.add(input);	// increases transition index
			
			int transitionIndex=resultTrace.getTransitionIndex();
			
			// abstract the concrete input 
		    //   using the known abstractions set the abstract input for the concrete input 
		    this.abstractionMapper.abstractInput(input, resultTrace);
			    		
			
		    OutputAction output= hypTraceWithAbstractions.getOutputAction(transitionIndex);
	
	
		    resultTrace.add(output);
	
			// from abstract output determine statevar updates
			LinkedHashMap<Integer, Integer> statevarUpdates=abstractionMapper.getStateVarUpdatesFromOutputAbstractionString(resultTrace, input, output, output.getAbstractionString());
			
			
			// update statevars in trace and apply updates to calculate statevars after transition
			stateVarMapper.updateStateVars(resultTrace,statevarUpdates);    		
			
	
		}
	
	    return  resultTrace;	
	}

}
