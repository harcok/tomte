package abslearning.learner;

import java.util.List;
import java.util.function.BiConsumer;

import abslearning.determinizer.Determinizer;
import abslearning.exceptions.BugException;
import abslearning.lookahead.LookaheadStore;
import abslearning.lookahead.LookaheadTraceRunner;
import abslearning.tree.ConcreteTree;
import abslearning.tree.Inconsistency;
import abslearning.tree.TreeCacheIterator;
import abslearning.tree.trace.CachedTraceRunner;
import abslearning.tree.trace.SutTraceInterface;
import sut.info.SutInfo;
import sut.info.TomteSutInterface;
import sut.interfacing.DeterminizerWrapper;
import sut.interfacing.DirectSutTraceRunner;

public class SutBuilder {
	private TomteSutInterface sut;
	private Determinizer determinizer;
	protected List<Integer> constants ;
	
	
	public SutBuilder() {
		sut = SutInfo.newSutWrapper();
		constants = SutInfo.getConstants();
	}
	
	
	public SutBuilder addDeterminizer(Determinizer determinizer) {
		this.determinizer  = determinizer; 
		sut = new DeterminizerWrapper(sut, determinizer);
		return this;
	}
	
	public SutBuilder addDeterminizer() {
		return addDeterminizer(new Determinizer(constants));
	}
	
	public SutTraceBuilder addTrace(String type) {
		return new SutTraceBuilder(type,sut, determinizer, constants);
	}
	
	public TomteSutInterface build() {
		return sut;
	}
	
	static class SutTraceBuilder {
		private TomteSutInterface sut;
		private ConcreteTree concreteTree = null;
		private String type;
		private Determinizer determinizer;
		private List<Integer> constants;
		private LookaheadStore lookaheadStore;
		private BiConsumer<Inconsistency, Boolean> inconsistencyHandler;
		SutTraceBuilder(String type, TomteSutInterface sut, Determinizer determinizer, List<Integer> constants) {
			this.sut = sut;
			this.type = type;
			this.determinizer = determinizer;
			this.constants = constants;
		}
		
		public SutTraceInterface build() {
			switch(type.toLowerCase()) {
			case "direct": 
				
				return new DirectSutTraceRunner(sut);
			case "cached": 
				assertObjectNotNull("tree",concreteTree);
				return new CachedTraceRunner(sut, new TreeCacheIterator(constants, concreteTree));
			case "lookahead": 
				assertObjectNotNull("tree",concreteTree);
				assertObjectNotNull("lookaheadStore", lookaheadStore);
				Determinizer oracleDeterminizer = new Determinizer(determinizer);
				TomteSutInterface sutOracle = new SutBuilder().addDeterminizer(oracleDeterminizer).build();
				return new LookaheadTraceRunner(sutOracle, lookaheadStore, concreteTree, inconsistencyHandler, constants );
			default: throw new BugException("No trace runner for type: " + type);
			}
		} 
		
		public SutTraceBuilder addTree(ConcreteTree tree) {
			this.concreteTree = tree;
			return this;
		}
		
		public SutTraceBuilder setInconsistencyHandler(BiConsumer<Inconsistency, Boolean> handler) {
			this.inconsistencyHandler = handler;
			return this;
		}
		
		
		public SutTraceBuilder addLookaheadStore(LookaheadStore lookaheadStore) {
			this.lookaheadStore = lookaheadStore;
			return this;
		}
	}
	
	static private void assertObjectNotNull(String fieldName, Object field) {
		if (field == null) {
			throw new BugException(fieldName + "not set");
		}
	}
}
