package abslearning.learner;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.Word;

/**
 * For every Mealy Machine learner implementation
 * TODO * maybe a state machine type pattern could be used here?  
 */
public interface LearnerInterface {
	/**
	 * Starts learning of the system or resumes learning after a counterexample was added. 
	 * Once learning is done, produces a hypothesis retrievable via {@code getHypothesis}.
	 */
	public void startLearning() throws Exception;
	
	/**
	 * Retrieves the learned hypothesis
	 */
	public Automaton getHypothesis();
	
	/**
	 * Processes a counterexample. 
	 * @return true if processing was successful, false otherwise
	 */
	public boolean addCounterExample(Word ceInputs, Word sutOutputs) throws Exception;
}
