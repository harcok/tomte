package abslearning.learner;

import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.Word;

public class LearnerLoggerWrapper implements LearnerAdapter{
	private LearnerAdapter learner;

	public LearnerLoggerWrapper(LearnerAdapter learnerAdapter) {
		this.learner = learnerAdapter;
	}

	@Override
	public void startLearning() throws Exception {
		learner.startLearning();
	}

	@Override
	public Automaton getHypothesis() {
		Automaton learnerHyp = learner.getHypothesis();
		return learnerHyp;
	}

	@Override
	public boolean addCounterExample(Word ceInputs, Word sutOutputs) throws Exception {
		return learner.addCounterExample(ceInputs, sutOutputs);
	}
}
