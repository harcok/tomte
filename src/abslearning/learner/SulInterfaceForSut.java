package abslearning.learner;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.mapper.AbstractionMapper;
import abslearning.mapper.StateVarMapper;
import abslearning.stores.CanonicalValueStore;
import abslearning.stores.FreshInputValueStore;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.tree.ConcreteTree;
import abslearning.tree.trace.SutTraceInterface;

//import abslearning.stores.ValueStoreMQ;
//import abslearning.trace.InputAction;
//import abslearning.trace.OutputAction;
//import abslearning.tree.trace.SutTreeInterface;

import de.learnlib.api.SUL;
import de.learnlib.api.SULException;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.SymbolImpl;
import util.learnlib.AutomatonUtil;

public class SulInterfaceForSut implements SUL<Symbol, Symbol> {
	private static final Logger logger = Logger.getLogger(SulInterfaceForSut.class);
	private static final Statistics statistics = Statistics.getInstance();
	
	private abslearning.trace.Trace resultTrace;
	private abslearning.trace.Trace currentTrace;
	private CanonicalValueStore valueStore;
	
	public SutTraceInterface treeSutTrace;
	
	public AbstractionMapper abstractionMapper;
	public StateVarMapper stateVarMapper;	
	
	public SulInterfaceForSut(Run run) {
		treeSutTrace=run.getTraceInstance("tree");
		
		this.abstractionMapper=run.abstractionMapper;
		this.stateVarMapper=run.stateVarMapper;
	}

	@Override
	public void pre() {
		resultTrace = new abslearning.trace.Trace();
 	    currentTrace = new abslearning.trace.Trace(); 
 		
 		treeSutTrace.sendReset(); 
 	    valueStore = new FreshInputValueStore();
 		
 		statistics.incMemQueries();
 		logger.trace("Member query number: " + statistics.getNumMemQueriesLearning());
	}
	
	@Override
	public Symbol step(Symbol currentSymbol) throws SULException {
 			statistics.incMemInputs();
 			
 	 		InputAction input;
 	 		OutputAction output;
 			
 			
 			//logger.debug("Learning symbol: " + currentSymbol + " of Membership query: " + query.getSymbolList() + " in MembershipOracle");
 			input = new InputAction(currentSymbol.toString());
 			logger.debug("Learning input: " + input.getAbstractionString());
 			
 			
 			currentTrace.add(input);// NEEDED to get transition index increased in Trace : when adding input then new transition index is increase

 			// concretize input 
 			int returnValue=this.abstractionMapper.concretizeInput(input, currentTrace.getStatevarsBeforeTransition(),valueStore);
 			if ( returnValue !=0 ) {
 				
                 // not allowed  input
 				// ignore by :
 				//    - skip sending it to SUT
 				//    - just return Oundefined to LL by adding it only to resultTrace
 				
 				
 				// skip sending to SUT: means skip input in currentTrace which is send to SUT
 				currentTrace.removeLastAction();
 				
 				resultTrace.add(input);
 				output = new OutputAction(AutomatonUtil.UNDEF.toString());  // abstract output "Oundefined"
 				resultTrace.add(output);

 				return AutomatonUtil.UNDEF;
 				//return new SymbolImpl(output.getAbstractionString()); // is basicly Symbol("Oundefined")
 			}
 			
 			// add input action to result trace			
 			resultTrace.add(input);
 			
 			// send concrete input to treeSutTrace 
 			output = treeSutTrace.sendInput(input,currentTrace);
 			
 			//* here I had to introduce outputs before I would determine updates.
 			// add output to mapper cache to and to currentTrace!!	
 			currentTrace.add(output);
 			resultTrace.add(output);
 			
 			// update statevars in trace
 			this.stateVarMapper.determineStateVarUpdatesFromMemVs(input, currentTrace);
 			
 			//get abstract output for concrete output
 			String abstractOutput=this.abstractionMapper.abstractOutput(input, output, currentTrace);
 	    	output.setAbstractionString(abstractOutput);	    
 	    	
 			logger.debug("Get output from sut, and add output to abstract tree: " + input + " -> " + output);

 			logger.debug("Received: " + output.toString());
 			
 			// return symbol
 			return new SymbolImpl(output.getAbstractionString());
	}

	@Override
	public void post() {
 		if ( logger.isDebugEnabled() ) logger.debug("MQ Learning newHyp. " + (statistics.getHypLIndex()+1) + ": "+ currentTrace.toStringConcrete() );
 		//assert query.size() == (resultTrace.size()/2) : "query: " + query.toString() + " has not the same length as outputs: " + currentTrace.toString();
	}


	public Trace getTrace() {
		return resultTrace;
	}

}
