package abslearning.learner;

import abslearning.exceptions.ConfigurationException;
import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.SUL;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.SULOracle;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.interfaces.Word;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.impl.SimpleAlphabet;

public class NewLearnLibAdapter implements LearnerInterface {

	private MealyMachine<?, Symbol, ?, Symbol>  learnedHyp;
	private DefaultQuery<Symbol, net.automatalib.words.Word<Symbol>> ce;
	private  LearningAlgorithm.MealyLearner<Symbol, Symbol> learner;
	private Alphabet inputAlphabet;

	public NewLearnLibAdapter(Alphabet inputAlphabet, Run run, String learningAlgorithm) {
		SUL<Symbol, Symbol> sul=new SulInterfaceForSut(run);
		   // SUL<Symbol, Symbol> sul=run.getSul();
		SULOracle<Symbol, Symbol> sulOracle = new SULOracle<>(sul);


		// Alphabet for new learnlib
		net.automatalib.words.Alphabet<Symbol> newlearner_inputAlphabet=
  				new SimpleAlphabet<Symbol>(inputAlphabet.getSymbolList());
		switch(learningAlgorithm.toLowerCase()) {
		case "ttt":
			learner = new TTTLearnerMealy<>(newlearner_inputAlphabet, sulOracle, AcexAnalyzers.LINEAR_FWD);
			break;
		default:
		    throw new ConfigurationException("unsupported learning algorithm:" + learningAlgorithm);
		}
		this.inputAlphabet = inputAlphabet;
		this.ce = null;
		this.learnedHyp = null;
	}

	@Override
	public void startLearning() throws Exception {
		if (ce == null) {
			learner.startLearning();
		} else {
			learner.refineHypothesis(ce);
		}
	}

	@Override
	public Automaton getHypothesis() {
		learnedHyp = learner.getHypothesisModel();
		Automaton abstractHyp = util.learnlib.AutomatonUtil.convertHyp(learnedHyp,inputAlphabet);
		return abstractHyp;
	}

	@Override
	public boolean addCounterExample(Word ceInputs, Word sutOutputs) throws Exception {
		// from API: DefaultQuery
		//     A query is a container for tests a learning algorithms performs,
		///   containing the actual test and the corresponding result.
		//
		//    de.learnlib.oracles.DefaultQuery<I, D>
		//
		//		Type Parameters:
		//		<I> input symbol type     -> Symbol
		//		<D> output domain type    -> Word<Symbol>

		//  DefaultQuery(Word<I> input, @Nullable D output)
		//   `-> DefaultQuery(Word<Symbol> input, @Nullable Word<Symbol> output)

		// from API: findCounterExample
		//    A counterexample is query which, when performed on the SUL,
		//    yields a different output than what was predicted by the hypothesis. I
		//
		//    The output field in the DefaultQuery contains the SUL output for the respective query.
		net.automatalib.words.Word<Symbol> inputWord=
				 net.automatalib.words.Word.fromList(ceInputs.getSymbolList());
		net.automatalib.words.Word<Symbol> outputWord=
				 net.automatalib.words.Word.fromList(sutOutputs.getSymbolList());

		net.automatalib.words.Word<Symbol> prefix= net.automatalib.words.Word.epsilon();
		ce = new DefaultQuery<>(prefix,inputWord,outputWord);
		boolean isCounterexample = isCounterExample(learnedHyp, ce);
		return isCounterexample;
	}

    /**  check sut query is a counter example on given hypothesis
     *
     * @param hypothesis
     * @param query : sut query : contains inputs and the corresponding outputs for the sut
     * @return
     */

	private <I,O> boolean isCounterExample(MealyMachine<?,I,?,O> hypothesis,DefaultQuery<I, net.automatalib.words.Word<O>> query ) {
		net.automatalib.words.Word<O> o1 = hypothesis.computeOutput(query.getInput());
		net.automatalib.words.Word<O> o2 = query.getOutput();

		assert o1 != null;
		assert o2 != null;

		// If equal => no counterexample :(
		if(!o1.equals(o2)) return true;
		return false;
	}
}
