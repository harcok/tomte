package abslearning.ceanalysis;

import java.util.LinkedList;
import java.util.List;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;

/**
 * Stores all counterexamples in their final form, that is after they have been reduced
 * and all coincidental relations have been removed.
 */
public class CeStore {
	public final List<CeEntry> concreteCeCache;
	
	public CeStore() {
		super();
		this.concreteCeCache = new LinkedList<> ();
	}
	
	public Trace getLastCe() {
		return concreteCeCache.size() > 0 ? 
				concreteCeCache.get(concreteCeCache.size() - 1).concreteCeTrace : null;
	}
	
	public List<Trace> getCes() {
		List<Trace> ces = new LinkedList<Trace>();
		this.concreteCeCache.stream().map(entry -> entry.concreteCeTrace).
		forEach(ceTrace -> ces.add(ceTrace));
		return ces;
	} 
	
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		this.concreteCeCache.forEach(entry ->
				builder.append(entry.toString()).append("\n"));
		return builder.toString();
	}

	public void storeNewCe(Trace traceWithStateVars) {
		CeEntry ceEntry = new CeEntry(traceWithStateVars);
		if (!concreteCeCache.contains(ceEntry)) {
			concreteCeCache.add(ceEntry);
		}  
	}
	
	public boolean hasCe(Trace ceTrace) {
		CeEntry ceEntry = new CeEntry(ceTrace);
		return concreteCeCache.contains(ceEntry);
	}
	static class CeEntry {
		public final Trace concreteCeTrace;

		public CeEntry(Trace concreteCeTrace) {
			super();
			this.concreteCeTrace = concreteCeTrace;
		}
		
		public String toString() {
			return concreteCeTrace.toString();
		}
		
		public boolean equals(Object ce) {
			return ce instanceof CeEntry && 
					(((CeEntry ) ce).concreteCeTrace.equalsConcrete(this.concreteCeTrace));
		}
		
		public int hashCode() {
			int product = 1;
			
			for (InputAction action : this.concreteCeTrace.getConcreteInputActions()) {
				product += 30*action.getMethodName().hashCode();
				for (Integer val : action.getConcreteParameters()) {
					product *= (val + 100);
				}
			}
			return product;
		}
		
	}
}
