package abslearning.ceanalysis;

import java.util.List;

import abslearning.ceanalysis.graph.RelationEdge;
import abslearning.ceanalysis.graph.ParameterNode;

public class RelationTrace {
	public final List<RelationEdge<ParameterNode<Integer>>> relationPath;
	public RelationTrace(List<RelationEdge<ParameterNode<Integer>>> relationPath) {
		this.relationPath = relationPath;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (RelationEdge<ParameterNode<Integer>> edge : relationPath) {
			builder.append(edge.startNode).append(" " + edge.relation.toCompact() + " ");
		}
		builder.append(relationPath.get(relationPath.size() - 1).endNode);
		return builder.toString();
	}
}
