package abslearning.ceanalysis;

import java.util.List;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.ceanalysis.reduction.Counterexample;
import abslearning.exceptions.BugException;
import abslearning.exceptions.LearnerCounterExampleException;
import abslearning.exceptions.Messages;
import abslearning.exceptions.RestartLearningException;
import abslearning.learner.Run;
import abslearning.lookahead.LookaheadStore;
import abslearning.stores.PredicateStore;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.interfaces.Word;
import util.Tuple2;


public class Analyzer {
	private static final Logger logger = Logger.getLogger(Analyzer.class);
	private static final Statistics statistics = Statistics.getInstance();

	private Run run;
	private CeStore ceStore;
	
	private AbstractionResolver abstractionResolver;
	private LookaheadResolver lookaheadResolver;
	private RelationResolver relationResolver;
	
	private boolean reconstructAbstractions;
	private LookaheadStore lookaheadStore;
	
	
	public Analyzer(Run run, CeStore ceStore, PredicateStore predicateStore, LookaheadStore lookaheadStore, boolean reconstructAbstractions,List<Integer> constants) {
		this.run=run;
		this.lookaheadStore = lookaheadStore;
		this.ceStore = ceStore;
		this.relationResolver = new RelationResolver(run, constants);
		this.lookaheadResolver = new LookaheadResolver(run, lookaheadStore, constants);
		this.abstractionResolver = new AbstractionResolver(constants, predicateStore);
		this.reconstructAbstractions = reconstructAbstractions;
	}


	//-------------------------------------------------------------------------------------------------------
	//    analyse found counter example   
	//-------------------------------------------------------------------------------------------------------


		

	public void analyseCounterExample(List<InputAction> concreteInputs) {
	
		// abstract the concrete counter example using hyp and sut
		//--------------------------------------------------------
		Trace traceOfCeOnSut=this.run.runOnConcreteSutAddStateVarsAndAbstractions(concreteInputs);
		Trace traceOfCeOnHyp=this.run.runOnConcreteHypothesisAndAddAbstractionsAndStatevars(traceOfCeOnSut.getConcreteInputActions());
		
		logCounterexampleHeader(traceOfCeOnSut, traceOfCeOnHyp);
	    
		// search for abstract ce
		//-----------------------
	    checkIfCeForLearner(traceOfCeOnSut);
	
		// search for new lookahead traces or abstractions
		//---------------------------------
		logger.info("Abstraction Refinement");						
		logger.fatal("Ce trace: " +  traceOfCeOnSut);
		
		Tuple2<Boolean, Trace> result = doAbstractionRefinement(traceOfCeOnSut,traceOfCeOnHyp);
		boolean successful = result.tuple0;
		
		if(successful) {
			throw new RestartLearningException(Messages.RESTART_LEARNING, null,"newInputAbstractionFoundByCe");
								
		} else {				
			// search for abstract ce a second time, 
			// the ce might have changed => we take the last version from the store
			traceOfCeOnSut = result.tuple1;
			checkIfCeForLearner(traceOfCeOnSut);
			throw new BugException(" Couldn't find abstraction!")
			.addDecoration("sutTrace", traceOfCeOnSut.toStringWithStatevars())
			.addDecoration("Ce Store", ceStore.toString());
		}	   
		
		
	
	}

	public void checkIfCeForLearner(Trace traceOfCeOnSut) {
		Word AbstractCe = this.run.checkIsAbstractCounterExample(traceOfCeOnSut
				.getAbstractInputWord());

		if (AbstractCe != null) {

			logger.info("abstract ce:" + AbstractCe);

			// TODO: change learnerCounterExample data to AbstractCounterExample
			// object with: (abstractInputs, abstractOutputsSut,
			// abstractOutputsHyp)
			Trace freshTraceOnSut = this.run.runFreshTraceOnSut(AbstractCe);
			statistics
					.addDescriptionRun("\n\n \n\n Termination Reason: concrete counterexample found! => Counterexample for Learner");

			run.storeAbstractCEinStatistics(freshTraceOnSut);
			ceStore.storeNewCe(traceOfCeOnSut);
			logger.fatal("Ce store:\n" + ceStore.toString());

			throw new LearnerCounterExampleException(new Counterexample(
					freshTraceOnSut));
		}

	}

	
	/*
	 * Abstraction refinement is a three step process: first we remove any redundant relations in the 
	 * counterexample, then we update the lookahead store until all missing memorable values are discovered,
	 * finally, we add any missing abstractions.
	 * <br/>
	 * 
	 * Example Login System: <br/>
	 * Concrete ce:
	 * 		sut: reg(0,0)\ok login(0,0)\ok  (!= ok hyp)
	 * After relationResolver (ce without redundant relations):
	 * 		ce: reg(0,1)\ok login(0,1)\ok 
	 * After lookaheadResolver (ce with all state vars):
	 * 		new lts: login(L0,l1) and login(l0,L1) (provided a lookahead trace store is used)
	 * 		ce: ref(0,1)\ok [0,1] login(0,1)\ok   
	 * After abstractionResolver:
	 * 		new abstractions: login([x0],[x1])
	 */
	private Tuple2<Boolean, Trace> doAbstractionRefinement( Trace traceOfCeOnSut,Trace traceOfCeOnHyp) {
		
		// first gather any relations between parameters present in the counterexample trace that are not
		// captured by existing abstractions
		Trace ceWithoutRedundantRelations = traceOfCeOnSut;
		if (!ceStore.hasCe(traceOfCeOnSut))
			ceWithoutRedundantRelations = relationResolver.removeRedundantRelations(traceOfCeOnSut);
		logger.fatal("Ce without redundant relations:\n   " + ceWithoutRedundantRelations);
		
		
		List<InputAction> concreteCeInputs = ceWithoutRedundantRelations.getInputActions();
		// build a trace of the ce with abstractions and memorable values
		Trace traceOfCeOnSutWithStateVars = run.runOnConcreteSutAndAddStateVars(concreteCeInputs);
		Trace traceOfCeOnHypWithStateVars = run.runOnConcreteHypothesisAndAddAbstractionsAndStatevars(concreteCeInputs);
		
		logCounterexampleWithStateVars(traceOfCeOnSutWithStateVars, traceOfCeOnHypWithStateVars);
		statistics.storeSimplifiedCEwithFoundRelation(traceOfCeOnSutWithStateVars, traceOfCeOnHypWithStateVars);
		
		// find lookahead traces that capture any memorable values not currently captured, which are 
		// needed to describe the found relations
		Trace ceWithAllStateVars = lookaheadResolver.resolveLookaheadTraces(ceWithoutRedundantRelations);

		
		// in case the store was updated, we have to increase the version of the observation tree  
		if (lookaheadResolver.hasUpdatedStore()) {
			logger.fatal(this.lookaheadStore.logStateUpdate());
			logger.fatal("new ce trace on sut:\n   " + ceWithAllStateVars.toStringWithStatevars());
			statistics.storeLookaheadUpdateOnCE(this.lookaheadStore.logStateUpdate());
			
			// if the reconstruct abstraction option was used, we build all abstractions
			if (reconstructAbstractions) {
				reconstructAbstractions(ceWithAllStateVars);
			}
		}
		
		// store the ce in a store. When we rebuild the abstractions, we run the ce's stored in this store
		ceStore.storeNewCe(ceWithAllStateVars);
		Trace ceWithStateVarsAndAbstractions = run.runOnConcreteSutAddStateVarsAndAbstractions(ceWithAllStateVars.getConcreteInputActions());///decorateStateVarTraceWithAbstractions(ceWithAllStateVars);
		
		// analyze the ce trace and add all abstractions needed to distinguish this concrete 
		List<Symbol> newSymbols = abstractionResolver.findNewAbstractionsInTrace(ceWithStateVarsAndAbstractions);
		newSymbols.forEach(sym -> statistics.storeAbstraction(sym.toString()));
		
		
		// it can happen that you are successful by just finding new symbols (to be continued)
		boolean analyzeSuccess = lookaheadResolver.hasUpdatedStore() || !newSymbols.isEmpty();
		return new Tuple2<>( analyzeSuccess, ceWithStateVarsAndAbstractions) ;
	}
	
	/*
	 * Abstractions can go out of sync when finding new memorable values, as these might
	 * break the abstractions' indexes. For example, x1 might point to different memorable
	 * values at different points in the learning process. 
	 *  
	 * As such, we should reconstruct all abstractions to keep their index in sync.
	 * 
	 */
	private void reconstructAbstractions(Trace currentCeWithStateVars) {
		logger.fatal("reconstructing abstractions");
		abstractionResolver.clearAbstractions();
		Trace traceOnSutWithAllStateVars = currentCeWithStateVars;
		for (Trace ceTrace : ceStore.getCes()) {
			traceOnSutWithAllStateVars = run.runOnConcreteSutAddStateVarsAndAbstractions(ceTrace.getConcreteInputActions());
			abstractionResolver.findNewAbstractionsInTrace(traceOnSutWithAllStateVars);
		}
	} 
	
	/**
	 * A {@code ce} is made distinguishable after abstractions if when computing its current abstractions and 
	 * running them fresh, we obtain the same output method. This is not a thorough check, since we ignore 
	 * the parameter values. 
	 * 
	 *  Tomte can not yet distinguish betwen differring output parameter values which then lead to different behavior,
	 *  so this case is covered.
	 */
	private boolean isCeDistinguishableAfterRefinement( List<InputAction> currentCeInputs) {
		Trace newTraceOnSut = this.run.runOnConcreteSutAddStateVarsAndAbstractions(currentCeInputs);
		Trace freshTraceOnSut = this.run.runFreshTraceOnSut(newTraceOnSut.getAbstractInputWord());
		OutputAction originalOutput = newTraceOnSut.getLastOutputAction();
		OutputAction currentOutput = freshTraceOnSut.getLastOutputAction();
		String originalOutputSymbol = originalOutput.getMethodName();
		String currentOutputSymbol = currentOutput.getMethodName();
		return originalOutputSymbol.equals(currentOutputSymbol);
	}
	
	private void logCounterexampleHeader(Trace traceOfCeOnSut, Trace traceOfCeOnHyp) {
		// print ce
		// ----------
		String ceDescrMsg="\n    concrete CE inputs: " +
		                  "\n           "  + traceOfCeOnSut.getConcreteInputWord()  + // .getInputActions() +		
		                  "\n    executed:" +
	    		          "\n       on Sut (with abstractions determined using statevars of sut):" +
	    		          "\n           "  + traceOfCeOnSut +
		                  "\n       on Hyp (with abstractions determined using statevars of hyp):" +
	    		          "\n           "  + traceOfCeOnHyp ;	    
	   
		logger.fatal(ceDescrMsg);
		
		String msg2="\n\n Termination Reason: concrete counterexample found!\n"  +  ceDescrMsg ;
	    statistics.addDescriptionRun(msg2); 				   		       
		logger.fatal(msg2);
	    
	    statistics.storeCE(traceOfCeOnSut, traceOfCeOnHyp);
	}
	
	private void logCounterexampleWithStateVars(Trace traceOfCeOnSut, Trace traceOfCeOnHyp) {
		// trace decoration with abstractions might fail in case the trace is lookahead incomplete
//		try {
//			Trace traceOfCeOnSutWithStateVars = this.run.decorateStateVarTraceWithAbstractions(traceOfCeOnSut);
//			traceOfCeOnSut = traceOfCeOnSutWithStateVars;
//		} catch(BugException b) {
//			logger.fatal("Couldn't decorate sut trace with abstractions because of\n" + b.getMessage());
//		}
		String msg="\ntrace on sut \n" + traceOfCeOnSut.toStringWithStatevars() + "\n\n" +
			        "\ntrace on hyp \n" + traceOfCeOnHyp.toStringWithStatevars() + "\n\n";
		// add data to statistics of ce analysis
			
		statistics.addDescriptionRun(msg); 	    
		logger.fatal(msg);
	}
}
