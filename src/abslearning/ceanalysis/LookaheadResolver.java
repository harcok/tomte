package abslearning.ceanalysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import abslearning.ceanalysis.graph.RelationGraph;
import abslearning.ceanalysis.graph.TraceParameterNode;
import abslearning.learner.Run;
import abslearning.lookahead.LookaheadStore;
import abslearning.relation.Relation;
import abslearning.trace.Trace;
import abslearning.trace.Transition;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.trace.action.Parameter;
import util.Tuple2;

public class LookaheadResolver {
	private static Logger logger = Logger.getLogger(LookaheadResolver.class);

	private LookaheadStore lookaheadStore;
	private boolean updatedStore;
	private List<Integer> constants;
	private Run run;

	public LookaheadResolver(Run run, LookaheadStore lookaheadStore, List<Integer> constants) {
		this.run = run;
		this.lookaheadStore = lookaheadStore;
		this.constants = constants;
	}

	/**
	 * Decorates a ce trace with state vars and for any lost mem values, updates the lookahead store
	 * so that the values are recovered on re-running the ce-inputs. 
	 * @return a trace with no missing state variables
	 */
	public Trace resolveLookaheadTraces(Trace ceWithoutRedundantRelations) {
		this.updatedStore = false;
		Trace traceWithAllStateVars = run
				.runOnConcreteSutAndAddStateVars(ceWithoutRedundantRelations.getConcreteInputActions());
		Set<Integer> history = new HashSet<Integer>();
		RelationGraph graph = new RelationGraph();
		
		for(int transitionIndex=0; transitionIndex<=traceWithAllStateVars.getTransitionIndex(); transitionIndex++) {
			InputAction input = traceWithAllStateVars.getInputAction(transitionIndex);
			// maintains a set of all values known before an input parameter, that is, values from which the paramter should originate. 
			HashSet<Integer> originatingValues = new HashSet<Integer>(traceWithAllStateVars.getStateVarsBeforeTransition(transitionIndex).values());
			for (int paramIndex = 0; paramIndex < input.getNumberParams(); paramIndex ++) {
				Parameter param = input.getParam(paramIndex);
				Integer paramValue = param.getIntegerValue();
				if (this.constants.contains(paramValue)) {
					continue; // we ignore constants
				}
				TraceParameterNode paramNode = new TraceParameterNode(param);
				graph.addNode(paramNode);
				
				// if a param value is in stateVarsBefore then it is already found memorable 
				// and we don't need a lookahead trace for it, only an abstraction
				if ( Relation.hasRelatedValue(history, paramValue) &&
						!Relation.hasRelatedValue(originatingValues, paramValue))  
						 {
					TraceParameterNode sourceParamNode = graph.getSource(paramNode);
					Parameter sourceParam = sourceParamNode.getCorrespondingParam(traceWithAllStateVars);
					
					// it can be that sourceParam is in the same input, in which case we don't need to add a lookahead trace,
					// only an abstraction
					if (sourceParam.getAction().getTraceIndex() < input.getTraceIndex()) {
					    List<Parameter> paramPathFromSourceToRef = getParamPathFromSourceToRef(sourceParamNode, paramNode, graph, traceWithAllStateVars);
					    Tuple2<Integer, Integer> losingAndReemergingTrans = findLosingAndReemergingTransitions(paramPathFromSourceToRef, traceWithAllStateVars);
					    int losingTransition = losingAndReemergingTrans.tuple0;
					    int reemergingTransition = losingAndReemergingTrans.tuple1 == null ? -1 : losingAndReemergingTrans.tuple1;
						Trace lookaheadTrace = buildTraceFromLosingParamTransition(traceWithAllStateVars, losingTransition);
						Parameter refParam = getRefParamInLt(traceWithAllStateVars, lookaheadTrace, param);
						
						boolean isUpdated = lookaheadStore.updateFromCounterExample(traceWithAllStateVars, lookaheadTrace, refParam, reemergingTransition);
						
						if (isUpdated) {
							logger.fatal(lookaheadStore.logStateUpdate());
							this.updatedStore = true;
							run.increaseVersionOfObservationTree();
							traceWithAllStateVars = run.runOnConcreteSutAndAddStateVars(ceWithoutRedundantRelations.getConcreteInputActions());
							input = traceWithAllStateVars.getInputAction(transitionIndex);
						}
					}
				}
				history.add(paramValue);
				originatingValues.add(paramValue);
			}
			
			OutputAction outputAction = traceWithAllStateVars.getOutputAction(transitionIndex);
			for (Parameter param : outputAction.getParameters()) {
				graph.addNode(new TraceParameterNode(param));
				history.add(param.getIntegerValue());
			}
		}
		return traceWithAllStateVars;
	}
	
	/**
	 * Gets the list of related parameters from source to ref param.
	 */
	private List<Parameter> getParamPathFromSourceToRef( TraceParameterNode sourceParamNode, TraceParameterNode refParamNode, RelationGraph graph, Trace trace ) {
	    // get graph nodes
	    LinkedHashSet<TraceParameterNode> relatedNodesBetweenSourceAndRef = graph.getNodes();
	    // remove those outside of source/ref indexes
	    relatedNodesBetweenSourceAndRef.removeIf(n -> graph.indexOf(n) < graph.indexOf(sourceParamNode) 
	            && graph.indexOf(n) > graph.indexOf(refParamNode));
	    // remove all nodes that don't originate from the source
	    relatedNodesBetweenSourceAndRef.removeIf(n -> !graph.getSource(n).equals(sourceParamNode));
 
	    // add the corresponding parameters from the trace
	    List<Parameter> paramPathFromSourceToRef = new ArrayList<>( relatedNodesBetweenSourceAndRef.size());
        relatedNodesBetweenSourceAndRef.forEach(n -> paramPathFromSourceToRef.add(n.getCorrespondingParam(trace)));
        return paramPathFromSourceToRef;
	}
	
	/**
	 * Returns the parameter in the ltTrace equivalent to the one in the counterexample. 
	 */
	private Parameter getRefParamInLt(Trace counterexample, Trace lookaheadTrace, Parameter refParamInCe) {
		int actionIndex = refParamInCe.getAction().getTraceIndex();
		int actionIndexInLt = actionIndex - (counterexample.size() - lookaheadTrace.size());
		return lookaheadTrace.get(actionIndexInLt).getParam(refParamInCe.getParameterIndex());
	}

	/**
	 * Returns true if the store was updated following a {@code resolveLookaheadTraces} call, 
	 * or false if it wasn't.
	 */
	public boolean hasUpdatedStore() {
		return this.updatedStore;
	}
	
	
	
	/**
	 * Returns sub-trace of {@code counterexample} starting from the transition
	 * which loses {@code sourceParam}.
	 * @param paramPathFromSourceToRef 
	 * @param graph 
	 * @param refParamNode 
	 */
	private Trace buildTraceFromLosingParamTransition(Trace counterexample, int losingTransitionIndex) {
		int endLookaheadTransitionIndex = counterexample.size() / 2;
		int startLookaheadTransitionIndex = 1 + losingTransitionIndex;
		Trace concreteLookaheadTrace = counterexample.getSubTrace(startLookaheadTransitionIndex * 2,
				endLookaheadTransitionIndex * 2);
		return concreteLookaheadTrace;
	}
	
	/**
     * Retrieves the transition after which a value is forgotten and the transition which "re-introduces the 
     * value".  The idea is that we build a lookahead traces for each pair of losing -> re-emerging transitions.
     */
	private Tuple2<Integer, Integer> findLosingAndReemergingTransitions(List<Parameter> paramPathFromSourceToRef, Trace counterexample) {
	    Iterator<Parameter> iter = paramPathFromSourceToRef.iterator();
	    Parameter prevParam = iter.next();
	    int prevTransitionIndex = prevParam.getAction().getTransitionIndex();
	    Integer losingTransition = null;
	    Integer reemergingTransition = null;
	    while(iter.hasNext()) {
	        Parameter nextParam = iter.next();
	        int nextTransitionIndex = nextParam.getAction().getTransitionIndex();
	        if (nextTransitionIndex > prevTransitionIndex)
    	        for (Transition trans : counterexample.eachTransition(prevTransitionIndex)) {
    	            if (!Relation.hasRelatedValue(trans.stateVarsAfter.values(), nextParam.getIntegerValue())) {
    	                // .. in(0)/ok()[] <- lost .... in(0)/ok() <- reemerging
    	                losingTransition = trans.index;
    	                reemergingTransition = nextTransitionIndex;
    	                return new Tuple2<>(losingTransition, reemergingTransition);
    	            }
    	            if (trans.index == nextTransitionIndex -1) 
                        break;
    	        }
	        
	        // this doesn't work if multiple paths arrive at the same ref param: 0 1 2 3 1 2 3 4. prev = 3 next = 1 
	        prevParam = nextParam;
	        prevTransitionIndex = nextTransitionIndex;
	    }
	    
	    return null;
	}
}
