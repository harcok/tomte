package abslearning.ceanalysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;
import abslearning.stores.PredicateStore;
import abslearning.trace.AbstractionVariable;
import abslearning.trace.Trace;
import abslearning.trace.Transition;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.Parameter;
import de.ls5.jlearn.interfaces.Symbol;

public class AbstractionResolver {
	
	private List<Integer> constants;
	private PredicateStore predicateStore;
	
	public AbstractionResolver(List<Integer> constants, PredicateStore predicateStore) {
		this.constants = constants;
		this.predicateStore = predicateStore;
	}
	
	public List<Symbol> findNewAbstractionsInTrace(Trace traceWithStateVars) {
		List<Symbol> foundAbstractions = new ArrayList<Symbol>();
		Set<Integer> concreteValues = new HashSet<Integer>();
		concreteValues.addAll(constants);
		for (Transition transition : traceWithStateVars.eachTransition()) {
			for (Parameter param : transition.input.getParameters()) {
				Integer paramValue = param.getIntegerValue();
					
				// if the parameter isn't fresh but has a -1 abstraction we know we have a problem
				if (Relation.hasRelatedValue(concreteValues, paramValue) && (param.getAbstractValue() == null || param.getAbstractValue() == -1)) {
					List<Integer> preceedingParamValues = getInputParamValuesBeforeParam(param, transition.input);
					AbstractionVariable newVar = resolveAbstractionVariable(param, preceedingParamValues, transition.stateVarsBefore);
					if (newVar == null ) {
//						throw new BugException(" Relation could not be resolved in trace ").
//						addDecoration("traceWithStateVars", traceWithStateVars ).
//						addDecoration("parameter", param.toStringWithAction());
						continue;
					} else {
						Symbol newSymbol = predicateStore.addInputPredicate(param.getAction(), param.getParameterIndex(), newVar);
						boolean isAlreadyInPredicateStore = (newSymbol == null);
						if (!isAlreadyInPredicateStore)
							foundAbstractions.add(newSymbol);
					}
				}
				concreteValues.add(paramValue);
			}
			
			for (Parameter param : transition.output.getParameters()) {
				concreteValues.add(param.getIntegerValue());
			}
		}
		return foundAbstractions;
	}
	
	private List<Integer> getInputParamValuesBeforeParam(Parameter param, InputAction input) {
		List<Integer> paramValues = new ArrayList<Integer>();
		int toIndex = param.getParameterIndex();
		for (int paramIndex = 0; paramIndex < toIndex; paramIndex ++) {
			paramValues.add(input.getParam(paramIndex).getIntegerValue());
		}
		return paramValues;
	}
	
    //private static java.util.Random rand = new java.util.Random(); // to verify assumption
	public AbstractionVariable resolveAbstractionVariable(Parameter refParam, List<Integer> previousParams, 
			SortedMap<Integer,Integer> stateVarsBeforeTransition) {
		
		AbstractionVariable newAbstractionVariable;
		Integer refValue = refParam.getIntegerValue();
		if ( constants.contains(refValue) ) { 
	    	// value of black edge is constant 
	        // only add new abstraction  
	        // => constant always available, no lookahead needed to find it memorable => no new lookahead trace needed
	        // => abstraction MUST be undefined otherwise should be found in abstraction of concrete ce! 
			newAbstractionVariable = new AbstractionVariable('c', constants.indexOf(refValue)); 
			
	     } else {
			 List<Integer> relatedValues = Relation.getRelatedValues(stateVarsBeforeTransition.values(), refValue);
//			 if (relatedValues.size() > 1) {
//				 throw new ConfusionException("Value " + paramValue + " is related to multiple state vars!\n state var values: " + stateVarValues);
//			 } 

			 //	TODO !! Analyze how picking a relation to construct an abstraction influences learning, when several relations are candidates. 
			 if (relatedValues.size() >= 1 ) {  
				 //Integer relatedValue = relatedValues.get(rand.nextInt(relatedValues.size()));
				 Integer relatedValue = relatedValues.get(0);
				 Integer stateVarIndex=util.JavaUtil.findFirstKeyOfSortedMapHavingValue(stateVarsBeforeTransition, relatedValue);
				 Relation relationWithStateVar = Relation.getRelation(relatedValue, refValue);
				 switch (relationWithStateVar) {
				 case EQUAL:
					 newAbstractionVariable = new AbstractionVariable('x', stateVarIndex); break;
				 case SUCC:
				 case NEXT:
				     newAbstractionVariable = new AbstractionVariable('i', stateVarIndex); break;
					 default: 
						 throw new BugException("State Var abstraction not defined for relation: " + relationWithStateVar);
				 }
	
	    	 } else if (previousParams.contains(refValue)) { 	 
		    	 // relation with param in same input  
		         // only add new abstraction
		         // => for relation between params in same input we do not need a statevar => no new lookahead trace needed
		         // => abstraction MUST be undefined otherwise should be found in abstraction of concrete ce! 
	    		 newAbstractionVariable = new AbstractionVariable('p', previousParams.indexOf(refValue));
	    	 } else { 
	    		 // no relation was found
	    		 // this situation can happen if resolving this relation or a previous one adds a lookahead trace to
	    		 // uncover a memorable value at an index for which there was already an abstraction defined 
	    		 newAbstractionVariable = null;
	    	 }
	     }
		
		return newAbstractionVariable;
	} 
	
	public void clearAbstractions() {
		this.predicateStore.clearAllInputActionPredicates();
	}	
}