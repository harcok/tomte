package abslearning.ceanalysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import abslearning.ceanalysis.graph.RelationEdge;
import abslearning.ceanalysis.graph.RelationGraph;
import abslearning.ceanalysis.graph.TraceParameterNode;
import abslearning.determinizer.Canonicalizer;
import abslearning.learner.Run;
import abslearning.relation.Relation;
import abslearning.stores.CanonicalValueStore;
import abslearning.stores.FreshInputValueStore;
import abslearning.trace.Trace;
import abslearning.trace.TraceParamIterator;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.IntegerParamValue;
import abslearning.trace.action.OutputAction;
import abslearning.trace.action.Parameter;
import graph.Split;
import util.Tuple2;

public class RelationResolver {

	private Run run;
	private List<Integer> constants;

	public RelationResolver(Run run, List<Integer> intConstants) {
		this.run = run;
		this.constants = intConstants;
	}
	
	/**
	 * Disambiguates a concrete CE by breaking any relation between two parameters
	 * that is not relevant for the CE. It leaves alone relations covered by existing
	 * abstractions. (TODO this might hide some coincidental relations if the abstractions
	 * themselves are coincidental) 
	 * 
	 * @param concreteCeTrace - a concrete counterexample trace
	 * @return a disambiguated counterexample
	 */
	public Trace removeRedundantRelations(Trace concreteCeTrace) {
		CanonicalValueStore store = new FreshInputValueStore(concreteCeTrace.getAllIntegerValues()); // used to generate fresh values
		boolean updatedCe = false;
		Integer freshValue = store.popFreshValue();
		Set<Integer> history = new HashSet<Integer>();
		List<InputAction> concreteInputs = concreteCeTrace.getConcreteInputActions();
		Trace ceWithStateVars = run.determineRealAbstractHypParamsForConcreteInputs(concreteInputs);  
		history.addAll(constants);
		
		TraceParamIterator paramIterator = new TraceParamIterator(ceWithStateVars);
		while(paramIterator.hasNext()) {
			Parameter param = paramIterator.next();
			Integer currentValue = param.getIntegerValue();
			if (  param.getAbstractValue() != null && param.getAbstractValue() == -1 &&
					isValueRelatedToHistory(currentValue, history) ) {
				// source values form the history values, they are always included in the complement
				List<Integer> sourceValues = Relation.getRelatedValues(history, currentValue);
				
				// build a graph from source values,  ref param, and all connected params to the end
				RelationGraph relationGraph = buildConnectedGraph(sourceValues, param,  paramIterator.remainingParams());
				
				// the index of the ref param in the graph 
				int indexOfRefParam = relationGraph.indexOf(new TraceParameterNode(param)); 
				
				// iterate over all the graph's connected splits where the first subgraph contains ref param 
				// and try to find a toggle combination which preserves the ce;    
				Trace newCeTrace = toggleFromRefNodeAndPreserveCe(ceWithStateVars, relationGraph, indexOfRefParam, freshValue);
				if (newCeTrace != null) {
					//ceWithStateVars = run.runOnConcreteHypothesisAndAddAbstractionsAndStatevars(newCeTrace.getConcreteInputActions());
					ceWithStateVars = run.determineRealAbstractHypParamsForConcreteInputs(newCeTrace.getConcreteInputActions());
					paramIterator = new TraceParamIterator(ceWithStateVars, param);
					currentValue = freshValue;
					freshValue = store.popFreshValue();
					updatedCe = true;
					
					// we do this to skip the current parameter 
					if (paramIterator.hasNext()) {
						paramIterator.next(); 
					}
				}
			}
			history.add(currentValue);
		}
		if (updatedCe) {
//			Canonicalizer canonizer = new Canonicalizer(constants);
//			ceWithStateVars = canonizer.canonicalize(ceWithStateVars);
		}
		return ceWithStateVars;
	}

	private Trace toggleFromRefNodeAndPreserveCe(Trace ceTraceWithStateVars, RelationGraph relationGraph, int refNodeIndex, Integer freshValue) {
		Trace newCe = null;
		for (Split<TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph>  split : relationGraph.connectedSplits(refNodeIndex)) {
			newCe = toggleNodesAndPreserveCe(ceTraceWithStateVars, split.first, freshValue);
			if (newCe != null)
				break;
		}
		return newCe;
	}
	
	
	private Trace toggleNodesAndPreserveCe(Trace ceTraceWithStateVars, RelationGraph connectedSubgraph, Integer freshValue) {
		LinkedHashSet<TraceParameterNode> toggledGraphParams = connectedSubgraph.getNodes();
		Integer origValue = connectedSubgraph.getRoot().getContent();
		connectedSubgraph.updateContent(freshValue);
		Trace newCeTrace = buildNewTraceWithParamsToggled(ceTraceWithStateVars, toggledGraphParams);
		Canonicalizer canonicalizer = new Canonicalizer();
		newCeTrace = canonicalizer.canonicalize(newCeTrace); // we need to canonicalize, otherwise we might get invalid canonized valued
		Tuple2<Trace, OutputAction> ceTuple = run.checkIsCounterExampleCachedAndFixInputs(newCeTrace.getConcreteInputActions());
		if (ceTuple != null) {
			return ceTuple.tuple0;
		}
		connectedSubgraph.updateContent(origValue);
		return null;
	}

	private Trace buildNewTraceWithParamsToggled(Trace ceTraceWithStateVars,
			LinkedHashSet<TraceParameterNode> toggledGraphParams) {
		Trace newCeTrace = new Trace(ceTraceWithStateVars);
		for (TraceParameterNode param : toggledGraphParams) {
			newCeTrace.get(param.actionIndex).getParam(param.paramIndex).setConcreteValue(new IntegerParamValue(param.getContent()));
		}
		return newCeTrace;
	}

	private RelationGraph buildConnectedGraph(List<Integer> sourceValues, Parameter refParam, List<Parameter> remainingParams) {
		List<Parameter> graphParams = new ArrayList<Parameter>(remainingParams.size() + 1);
		graphParams.add(refParam);
		graphParams.addAll(remainingParams);
		return new RelationGraph(graphParams, sourceValues, true);
	}

	private boolean isValueRelatedToHistory(Integer origValue, Set<Integer> history) {
		for (Integer historyValue : history) {
			if (Relation.areRelated(historyValue, origValue)) {
				return true;
			}
		}
		return false;
	}
}