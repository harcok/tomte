package abslearning.ceanalysis.graph;

import abslearning.trace.Trace;
import abslearning.trace.action.Action;
import abslearning.trace.action.Parameter;

public class TraceParameterNode extends ParameterNode<Integer> {

	public final String actionName;
	
	public TraceParameterNode(Parameter parameter) {
		super(parameter.getIntegerValue(), parameter.getAction().getTraceIndex(), parameter.getParameterIndex());
		actionName = parameter.getAction().getMethodName();
	}
	
	public TraceParameterNode(Integer value, int traceIndex, int paramIndex) {
		super(value, traceIndex, paramIndex);
		this.actionName = "undef";
	}
	
	public TraceParameterNode(Integer constant) {
		super(constant);
		this.actionName = null;
	}
	
	public Parameter getCorrespondingParam(Trace trace) {
		Parameter param = null;
		if (actionIndex < trace.size()) {
			Action action = trace.get(actionIndex);
			if ( paramIndex < action.getNumberParams() ) {
				param = action.getParam(paramIndex);
			}
		}
		return param;
	}
	
	public String toString() {
		if (actionName != null) {
			return actionName + actionIndex + "_p" + paramIndex + " [ " + getContent() + "]";
		} else {
			return super.toString();
		}
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((actionName == null) ? 0 : actionName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraceParameterNode other = (TraceParameterNode) obj;
		if (actionName == null) {
			if (other.actionName != null)
				return false;
		} else if (!actionName.equals(other.actionName))
			return false;
		return true;
	}
}
