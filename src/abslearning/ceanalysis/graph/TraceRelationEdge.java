package abslearning.ceanalysis.graph;

import abslearning.relation.Relation;

public class TraceRelationEdge extends RelationEdge<TraceParameterNode>{
	public TraceRelationEdge(TraceParameterNode startNode, TraceParameterNode endNode, Relation relation) {
		super(startNode, endNode, relation);
	}
}
