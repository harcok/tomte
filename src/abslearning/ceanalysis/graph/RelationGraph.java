package abslearning.ceanalysis.graph;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import abslearning.trace.action.Parameter;
import graph.EdgeFactory;
import graph.content.ContentDagGraph;

/*
 * Relation graph over Integer parameter nodes. One of the main purposes of this class
 * is to shield from the generics.  
 */
public class RelationGraph extends ContentDagGraph<Integer, TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph>{

	protected boolean maintainConnectivity = false; 
	// the graph can be configured to enforce connectivity, in which case if no edge can be added,
	
	public RelationGraph() {
		super(new RelationParameterEdgeFactory());
	}
	
	public RelationGraph(LinkedHashMap<TraceParameterNode, Set<RelationEdge<TraceParameterNode>>> adjacencyList,
			EdgeFactory<TraceParameterNode, RelationEdge<TraceParameterNode>> edgeFactory) {
		super(adjacencyList, edgeFactory);
	}
	
	protected RelationGraph buildNew(
			LinkedHashMap<TraceParameterNode, Set<RelationEdge<TraceParameterNode>>> adjacencyList,
			EdgeFactory<TraceParameterNode, RelationEdge<TraceParameterNode>> edgeFactory) {
		return new RelationGraph(adjacencyList, edgeFactory);
	}

	/**
	 * Updates the content of a node and all its connected parameters. It potentially leaves the 
	 * graph with invalid relations, since it doesn't update the relation edges.  
	 */
	public void updateNodeNaively(TraceParameterNode node, Integer newValue){
		super.updateContent(node, newValue);
	}
	
	/**
	 * Configures a connectivity option so that new nodes are only added if 
	 * a connecting edge can be built. This is conditioned by the edge factory. 
	 */
	public void maintainConnectivity(boolean cond) {
		this.maintainConnectivity = cond;
	}
	
	public boolean addNode(TraceParameterNode node) {
		boolean success;
		if (!maintainConnectivity || size() == 0 || canConnectNode(node)) 
			success = super.addNode(node);
		else 
			success = false;
		return success;
	}
	
	public TraceParameterNode getSource(TraceParameterNode node) {
		LinkedHashSet<TraceParameterNode> prevConnectedNodes = this.getConnectedNodesTo(node);
		while(!prevConnectedNodes.isEmpty()) {
			node = prevConnectedNodes.iterator().next();
			prevConnectedNodes = this.getConnectedNodesTo(node);
		}
		return node;
	}
	
	public TraceParameterNode getFirstNodeWithValue(Integer value) {
		for (TraceParameterNode node : _getNodes()) {
			if (node.getContent().equals(value)) {
				return node;
			}
		}
		return null;
	}
	
	/**
	 * Builds a relation graph by first adding the values in history in the order they appear in the list, 
	 * then adding the parameters, also in order. 
	 * If {@code isConnected} is set to true, only the parameters preserving connectivity are added.  
	 */
	public RelationGraph(List<Parameter> traceParams, List<Integer> history, boolean isConnected) {
		this();
		this.maintainConnectivity(isConnected);
		for (Integer constant : history) {
			this.addNode(new TraceParameterNode(constant));
		}
		addNodes(traceParams);
	}
	
	public void addNodes(List<Parameter> traceParams) {
		for (Parameter param : traceParams) {
			this.addNode(new TraceParameterNode(param));
		}
	}
}
