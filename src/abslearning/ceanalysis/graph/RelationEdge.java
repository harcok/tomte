package abslearning.ceanalysis.graph;

import graph.content.ProcessingEdge;
import graph.content.ValueNode;
import abslearning.relation.Relation;

public class RelationEdge<N extends ValueNode<Integer>> extends ProcessingEdge<Integer, N>{
	
	public final Relation relation;

	public RelationEdge(N startNode, N endNode, Relation relation) {
		super(startNode, endNode);
		this.relation = relation;
	}

	public Integer processContent(Integer content) {
		return relation.map(content);
	}
}
