package abslearning.ceanalysis.graph;

import graph.content.ValueNode;

public class ParameterNode<C> extends ValueNode<C> {

	public final int actionIndex;
	public final int paramIndex;

	public ParameterNode(C value, int actionIndex, int paramIndex) {
		super(value);
		this.actionIndex = actionIndex;
		this.paramIndex = paramIndex;
	}
	
	public ParameterNode(C value) {
		super(value);
		this.actionIndex = -1;
		this.paramIndex = -1;
	}
	
	public boolean isConstant() {
		return actionIndex == -1;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (actionIndex == -1) {
			builder.append("const(= ").append(super.getContent()).append(")");
		} else {
			builder.append("param(").append(actionIndex).append(":").append(paramIndex).append(")");
		}
		return builder.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + actionIndex;
		result = prime * result + paramIndex;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterNode other = (ParameterNode) obj;
		if (actionIndex != other.actionIndex)
			return false;
		if (paramIndex != other.paramIndex)
			return false;
		return true;
	}
}
