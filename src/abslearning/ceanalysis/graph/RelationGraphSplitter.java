package abslearning.ceanalysis.graph;

import graph.ConnectedGraphSplitter;

public class RelationGraphSplitter extends ConnectedGraphSplitter<TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph>{

	public RelationGraphSplitter(RelationGraph graphToSplit, int fromNodeIndex) {
		super(graphToSplit, fromNodeIndex);
	}

}
