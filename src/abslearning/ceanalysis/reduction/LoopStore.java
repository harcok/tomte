package abslearning.ceanalysis.reduction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Word;

public class LoopStore {
	private static final Logger logger = Logger.getLogger(LoopStore.class);
	
	//loops: SortedMap with 
	//  key:   length of  loop 
	//  value: the longest loop of a state with key's length (can be multiple)
	private SortedMap<Integer, ArrayList<Loop>> length2stateLongestLoops;
	
	public LoopStore(Automaton hyp, Word abstractInputWord) {
		
		logger.debug("Initialize loops");
    	length2stateLongestLoops = new TreeMap<Integer, ArrayList<Loop>>();
    	
    	
        Map<Integer,Loop> stateID2longestLoop = new HashMap<Integer,Loop>();
        //get for every input Symbol in Word the corresponding state in the hypothesis
        for (int inputIndex=0; inputIndex<abstractInputWord.size();inputIndex++){
            State state = hyp.getTraceState(abstractInputWord, inputIndex);
            int StateID=state.getId();
            //logger.debug("State id: " + StateID + ", abstractInputWordTrace.getSymbolByIndex(i): " + abstractInputWordTrace.getSymbolByIndex(inputIndex));
            
            //update the loop length for every state
            Loop loop;
            if(!stateID2longestLoop.containsKey(StateID)){
            	//there is no loop for this state
            	// create a new loop of length 0, needed to add longer loops later  
            	loop = new Loop(state.getId(),inputIndex); 
            	stateID2longestLoop.put(StateID, loop); 
            } else {
                // there exists already a loop for this state extend the length of this loop
                loop = stateID2longestLoop.get(StateID);
                //extend length of loop
                loop.setLength(inputIndex-loop.getStartIndex());
                // for each state's loop 
                //  - increment counter 
                loop.incrementSubloopCounter();
                //  - store endpoint as value for that counter in map counter2endpoint map (subloops)
                //   -> subloops gets sorted by key(counter) => the higher the counter the longer the loop!
                loop.setSubloops(inputIndex);
            }
              
        }
        
        //enter information from longest loop per state 
        //into a SortedMap with
        //   key: length of longest loop per state; 
        //   value: the longest loop
        for (int stateID : stateID2longestLoop.keySet()){
        	logger.debug("Loop: " + stateID2longestLoop.get(stateID));
            Loop longestLoop = stateID2longestLoop.get(stateID);
            
            if(longestLoop.getLength()==0) {
                // no loop was found this state
            	continue;
            }	
            ArrayList<Loop> loopList = new ArrayList<Loop>();
            if(length2stateLongestLoops.containsKey(longestLoop.getLength())){
                // there exists already a loop with this length 
            	// possible because the longest loop of two different states can have the same length
            	loopList = length2stateLongestLoops.get(longestLoop.getLength());       
            } else {
            	//there didn't exists already a loop with this length
            	// create array for this length
                loopList = new ArrayList<Loop>();
                length2stateLongestLoops.put(longestLoop.getLength(), loopList);
            }
            // add loop to list
 	        loopList.add(longestLoop);
        }
        
        //length2stateLoops
	}



	/**
     * Prints all loops in the LoopStore
     */
	public void printAllLoops() {
        for (int key : length2stateLongestLoops.keySet()){
        	logger.debug("Length: " + key + ", value: " + length2stateLongestLoops.get(key).toString());
        }
        logger.debug("---------------------------------------------------"); 
    }


	
    public void addLoop(Loop loop) {
		int length=loop.getLength();
		ArrayList<Loop> loopArray = getLoopsOfLength(length);
		loopArray.add(loop);
	}
	
	public void addLoops(List<Loop> subloops) {
		for ( Loop loop: subloops) {
			addLoop(loop);
		}
	}


	private ArrayList<Loop> getLoopsOfLength(int length) {
        ArrayList<Loop> loopsOfSameLength = new ArrayList<Loop>();
        if(length2stateLongestLoops.containsKey(length)){
        	// fetch list if already exist
            loopsOfSameLength = length2stateLongestLoops.get(length); 
        } else {
        	// else create it and put it to length2loops mapping
            loopsOfSameLength = new ArrayList<Loop>();
            length2stateLongestLoops.put(length, loopsOfSameLength);
        }
        return loopsOfSameLength;
	}

    /**
     * Removes a loop from the LoopStore
     * 
     * @param loop: the loop to be removed 
     */
    public void removeLoop(Loop loop) {
        int loopLength = loop.getLength();
        ArrayList<Loop> l = length2stateLongestLoops.get(loopLength);
        
        //remove the given loop from the list of loops with loopLength
        l.remove(loop);
        
        //if all loops in the array have been removed, then also remove the 
        //entry for this loopLength from the SortedMap
        if(l.isEmpty()){
            length2stateLongestLoops.remove(loopLength);
        }
    }
    
    public boolean isEmpty(){
    	return length2stateLongestLoops.isEmpty();
    }

	public Loop getLongestLoop() {
    	// length2stateLongestLoops is sorted on key => lastKey is longest length
        int longestLoopLength = length2stateLongestLoops.lastKey();
        // multiple loops a longest length possible: get first element of List
        Loop longestLoop = length2stateLongestLoops.get(longestLoopLength).get(0);
        return longestLoop;
	}
}
