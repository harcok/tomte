package abslearning.ceanalysis.reduction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * class that defines a loop for a trace identified by the states (their id's) 
 * visited in the trace
 * 
 * @author Fides
 *
 */
//public class Loop implements Comparable<Loop>{
public class Loop {
	
    private int stateID; // state where loop starts and ends 
    private int length ;			//nr of actions in the loop (independent of subloops)
    private int startIndex;         //  index of first input in inputs of trace 
    private int subloopCounter;		//nr of times state 'id' is visited in the loop
    private Map<Integer,Integer> subloops;
	
	public Loop(int stateID, int startIndex) {
		this.stateID = stateID;
		this.length = 0;
		this.startIndex = startIndex;
		this.subloopCounter = 0;
		//this.subloops = new HashMap<Integer,Integer>();
		this.subloops = new TreeMap<Integer,Integer>();  // sorts by key(counter)
	}
	
	public Loop(int id, int length, int startIndex, Map<Integer,Integer> subloops) {
		this.stateID = id;
		this.length = length;
		this.startIndex = startIndex;
		this.subloopCounter = 0;
		//this.subloops = new HashMap<Integer,Integer>();
		this.subloops = new TreeMap<Integer,Integer>();
		for(int key : subloops.keySet()) {
			int value = subloops.get(key);
			if(value>startIndex && value<=(startIndex+length)){
				this.subloopCounter++;	
				this.subloops.put(subloopCounter, value);		//re-initialize subloopCounter (important for second loop)
			}
		}
	}

/*	
    // TODO:  implement equals which is consisten with compareTo !!
    // => current compareTo for loop not used!! 
 
	public int compareTo(Loop other) {	//has to be consistent with equals() method
		final int SMALLER=-1;
		final int EQUAL=0;
		final int BIGGER=1;

		if(this.length == other.length) return EQUAL;
		else if (this.length < other.length) return SMALLER;
		else return BIGGER;
	}
*/	
	
	public String toString() {
		String str = "";
		str = "id: " + this.stateID + "/ length: " + this.length + "/ startIndex: " + this.startIndex + "/ subloopCounter: " + this.subloopCounter;
		for(int key : subloops.keySet()){
			str += "/ subloop nr: " + key + "/ starting at: " + subloops.get(key);
		}
		return str;
	}
	
	public void setId(Integer id){
		this.stateID=id;
	}
	
	public int getId(){
		return stateID;
	}
	
	public void setLength(int length){
		this.length=length;
	}
	
	public int getLength(){
		return length;
	}
	
	public void setStartIndex(int startIndex){
		this.startIndex=startIndex;
	}
	
	public int getStartIndex(){
		return startIndex;
	}
	
	public void incrementSubloopCounter(){
		this.subloopCounter++;
	}
	public void setSubloops(int index){
		subloops.put(subloopCounter, index);
	}	
	
	public int getSubloopCounter(){
		return subloopCounter;
	}
	/*
	public void setSubloopCounter(int subloopCounter){
		this.subloopCounter = subloopCounter;
	}
	*/
	

	
	public int getSubloopIndex(int subloopCounter){
		return subloops.get(subloopCounter);
	}
	
	public Map<Integer,Integer> getSubloops(){
		return subloops;
	}

	public boolean isSplittable() {
		return ( subloopCounter > 1 );
	}

    /**
     * Split a loop into two loops, where each loop contains the same number 
     * of subloops (nr of times state 'id' is visited in the loop)  
     * 
     * @param loop that has to be split 
     * @param loops: SortedMap with key: length of loop and value: ArrayList of loops that
     *               have this length; the old loop is removed from this structure (loops) and
     *               the two new loops are added to it
     */	
	public List<Loop> split() {
		if (!isSplittable()) return null;
		
		Loop loop=this;
		
        //nr of subloops in the loop
        int subloopCounter = loop.getSubloopCounter();
 
        //create two new subloops, which/where
        //have the same id
        //have new lengths calculated using the index of the middle subloop (loop.getSubloopIndex(subloopCounter/2))
        //the first loop has the same starting index, the second loop has the starting index of the middle subloop
        //have updated subloops
        Loop subloop1 = new Loop(loop.getId(),loop.getSubloopIndex(subloopCounter/2)-loop.getStartIndex(),loop.getStartIndex(),loop.getSubloops());
        Loop subloop2 = new Loop(loop.getId(),loop.getSubloopIndex(subloopCounter)-loop.getSubloopIndex(subloopCounter/2),loop.getSubloopIndex(subloopCounter/2),loop.getSubloops());
        
        ArrayList<Loop> subloops=new ArrayList<Loop>();
        subloops.add(subloop1);
        subloops.add(subloop2);
        return subloops;
	}
}
