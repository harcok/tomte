package abslearning.ceanalysis.reduction;

import util.Tuple2;
import abslearning.trace.Trace;
import abslearning.trace.action.OutputAction;

public class SingleTransitionReduction extends ReductionStrategy {

	public Trace reduceCe(Trace ce) {
		Trace reducedTrace = applySingleTraceReduction(ce);
		
		return reducedTrace;
	}
	
	public Trace applySingleTraceReduction(Trace trace) {
    	Trace resultTrace = new Trace(trace, true);

    	for (int transitionIndex = 0; transitionIndex <= resultTrace.getTransitionIndex(); transitionIndex ++) {
    		Trace reducedTrace = getReducedTrace(resultTrace, transitionIndex, transitionIndex+1);
        	Tuple2<Trace,OutputAction> result=this.run.checkIsCounterExampleCachedAndFixInputs(reducedTrace.getConcreteInputActions());
        	if (result != null) {
        		resultTrace =  result.tuple0;
        		transitionIndex --;
        	}
        }
    	
    	if (resultTrace.size() == trace.size()) {
    		return null;
    	} else {
    		return resultTrace;
    	}
    }

}
