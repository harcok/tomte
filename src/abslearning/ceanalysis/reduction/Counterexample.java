package abslearning.ceanalysis.reduction;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.exceptions.BugException;
import abslearning.trace.Trace;
import de.ls5.jlearn.equivalenceoracles.EquivalenceOracleOutputImpl;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.EquivalenceOracleOutput;
import de.ls5.jlearn.interfaces.Word;

/**
 * class that contains a counterexample trace and methods to reduce the counterexample 
 * @author Fides & Harco & Paul
 *
 */
public class Counterexample {
	private static final Logger logger = Logger.getLogger(Counterexample.class);
	private static final List<ReductionStrategy> reductionStrategies = new ArrayList<ReductionStrategy>();
	private static Trace lastReducedCe = null;
	
	public static void setReductionStrategies (List<String> reductionStrategies) {
		for (String reductionStrategy : reductionStrategies) {
			String simpleClassName = reductionStrategy.substring(0, 1).toUpperCase() + reductionStrategy.substring(1);
			String fullClassName = "abslearning.ceanalysis.reduction." + simpleClassName + "Reduction";
			try {
				ReductionStrategy strategy = (ReductionStrategy) Class.forName(fullClassName).newInstance();
				Counterexample.reductionStrategies.add(strategy);
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				logger.fatal("Reduction strategy " + fullClassName + " does not exist or has a constructor with parameters");
				e.printStackTrace();
			}
		}
	} 
	
	private static void setLastReducedCe(Trace reducedCe) {
		lastReducedCe = reducedCe;
	}
	
	private static boolean hasCeBeenReduced (Trace reducedCe) {
		return lastReducedCe != null && Trace.equalsConcreteInputs(reducedCe.getConcreteInputActions(), lastReducedCe.getConcreteInputActions());
	}
	
	
    
	private Trace counterexample;



	// Important counterexample must have abstract inputs of hypothesis set!!
	public Counterexample(Trace counterexample){
		this.counterexample = counterexample; // trace has abstract inputs of abstract hyp which we use to remove loops in hyp
	}
	
	
	 /**
     * Tries to reduce the length of a counterexample by removing loops in the trace by looking at the abstract hypothesis
     * @param counterexample that has to be reduced
     */
    public void  reduceCounterexample() {	
    	
        Trace reducedTrace = counterexample;
        
    	if (! hasCeBeenReduced(reducedTrace) ) {
	    
	        reducedTrace = applyReductionStrategies(reducedTrace);
	        
	        //for reduced trace the input and output abstractions might not be correct any more
	        //we might have removed inputs that are referred to by other inputs (using an abstraction)
	        //therefore we need to set the abstract inputs again in the correct way
	        // and remove all abstract outputs 
	
	        reducedTrace = new Trace(reducedTrace,true );
	        
	        reducedTrace.updateTraceIndexes();
	        
	        // update counter example in object
	        counterexample = reducedTrace;
	        
	        setLastReducedCe(counterexample);
    	} else {
    		logger.fatal("ce already reduced");
    	}
    }

	private Trace applyReductionStrategies(Trace reducedTrace) {
		for (ReductionStrategy reductionStrategy : reductionStrategies) {

			logger.fatal("Started " + reductionStrategy);
			Trace result = reductionStrategy.reduceCe(reducedTrace);
			logger.fatal("Finished " + reductionStrategy);
			
			if (result == null || result.size() == reducedTrace.size()) {
				logger.fatal("Ce unchanged");
			} else if (result.size() < reducedTrace.size()) {
				reducedTrace = result;
				logger.fatal("New ce: " + reducedTrace);
			} else { //result.size() < reducedTrace.size())  
				throw new BugException("Ce should not grow bigger").
				addDecoration("ce before reduction", reducedTrace).addDecoration("ce after reduction", result);
			}
		}
		return reducedTrace;
	}
 
	public Trace getTrace(){ 
		return counterexample;
	}
	
	public EquivalenceOracleOutput getEquivalenceOracleOutput() {
		Word abstractInputWord = counterexample.getAbstractInputWord();
		Word abstractOutputWord = counterexample.getAbstractOutputWord();
		return new EquivalenceOracleOutputImpl(abstractInputWord, abstractOutputWord);
	}
	
	 public void abstractValidationAndLogging(Automaton hypL) {
		// get counterexample inputs and sut outputs (differs from hypothesis)
		Word ceInputs = counterexample.getAbstractInputWord();
		Word sutOutputs = counterexample.getAbstractOutputWord();
		
		// get hypothesis outputs
		Word hypOutputs=hypL.getTraceOutput(ceInputs);							
		logger.debug( "\n\n     Abstract Counterexample:" + 
		               "\n        ceInputs: " + ceInputs  + 
		               "\n        sutOutputs: " + sutOutputs + 
		               "\n        hypOutputs: " + hypOutputs + "\n\n");	
		
		// found ce. with testing must be a valid ce						
		if ( hypOutputs.equals(sutOutputs)  ) {
			throw new BugException("something wrong with testing : found counter example is NOT a counter example!").
			addDecoration("ce: ", counterexample);
		}	     						
		
	}


	
}
