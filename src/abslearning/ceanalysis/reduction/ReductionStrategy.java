package abslearning.ceanalysis.reduction;

import java.lang.invoke.MethodHandles;

import org.apache.log4j.Logger;

import abslearning.determinizer.Canonicalizer;
import abslearning.learner.Run;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

public abstract class ReductionStrategy {
	public final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass());
	protected final Run run;
	private String strategyName;
	
	/**
	 * Applies the class specific reduction strategy on the counterexample trace.
	 * @param ce - Counter example trace.
	 * @return either a reduced or a concrete copy of the counterexample (not {@code null})
	 */
	public abstract Trace reduceCe(Trace ce);
	
	public ReductionStrategy() {
		this.run = Run.getInstance();
		this.strategyName = buildName();
	} 
	
	// Turns "LoopReduction" into "loop reduction"
	private String buildName() {
		String [] stringSegments = this.getClass().getSimpleName().split("(?=[A-Z])");
		StringBuilder nameBuilder = new StringBuilder();
		for (String stringSegment : stringSegments) {
			nameBuilder.append(stringSegment);
			nameBuilder.append(' ');
		}
		String name = nameBuilder.substring(0, nameBuilder.length() - 1).toLowerCase();
		return name;
	}
	
	
	public String toString () {
		return this.strategyName;
	}
	
	
	 /**
     * Creates a subTrace from an initial trace by removing transitions between the start
     * and end index. 
     * 
     * @param trace: the input trace that has to be reduced 
     * @param start: first transition that has to be removed
     * @param end: last transition that has to removed
     * @return input trace without elements between start and end
     */
    protected Trace getReducedTrace(Trace trace, int startTransIndex, int endTransIndex) {
    	Trace reducedTrace = new Trace();
    	int numTransitions = trace.size()/2;
    	
    	// just create a new trace where we copy only the the transitions of the reduced trace
        for(int transitionIndex=0; transitionIndex<numTransitions; transitionIndex++){
            if(transitionIndex<startTransIndex || transitionIndex>=endTransIndex){
            	reducedTrace.add(new InputAction(trace.getInputAction(transitionIndex)));
            	reducedTrace.add(new OutputAction(trace.getOutputAction(transitionIndex)));
            }
        }
        
        // canonicalize needed so that fresh outputs are rightly canonicalized! 
        // => we always run with output determinizer!!!!
        //    so if we remove a piece which gave fresh output -3, and after that trace gives -4 freshoutput 
        //    then reduced traces gives fresh outputs -2,-4  
        //    -> problem: second fresh output should be -3 
        //                because determinizer only has original fresh values for 
        //                two canolized output values -2, and -3 but none for -4!!
        //                
        // => canonicalization only needed for fresh output determinization!! 
        //    If we have no fresh outputs in sut, then canonicalization not needed!!
        //    However still nice to get a canonical ce trace!!
        //
        //The examples are for the basic  1 step canonicalization. Since we are handling successors, we have
        //also adapted canonicalization for a step greater than 1. 
        
        return new Canonicalizer().canonicalize(reducedTrace);
    }    
    

    
    
    
}
