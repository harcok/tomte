package abslearning.ceanalysis.reduction;

import java.util.List;

import util.Tuple2;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import de.ls5.jlearn.interfaces.Automaton;

public class LoopReduction extends ReductionStrategy{

	public Trace reduceCe(Trace ce) {
    	
        //get current hypothesis from LearnLib
        Automaton hyp = run.getAbstractHyp();
		
		logger.fatal("Starting loop reduction");
        
        //as long as there are unchecked loops in the counterexample
		Trace reducedTrace = applyLoopReduction(ce, hyp);
        
        logger.debug("Finished loop reduction");
		return reducedTrace;
	}
	
	public Trace applyLoopReduction(Trace trace, Automaton hyp) {
    	// make full copy (concrete and abstract)
    	Trace resultTrace = new Trace(trace);
    	
	    /* apply hyp abstactions on concrete counter example so that you can easily remove loops in hyp*/
    	//Trace hypTraceWithAbstractions = run.runOnConcreteHypothesisAndAddAbstractionsAndStatevars(resultTrace.getConcreteInputActions());
    	
        LoopStore loopStore= new LoopStore(hyp, resultTrace.getAbstractInputWord());
        
        
        //as long as there are unchecked loops in the trace
        while(!loopStore.isEmpty()){    
        	
            //get longest loop 
        	Loop longestLoop=loopStore.getLongestLoop();
        	
        	
            //remove loop from current trace
            Trace reducedCeTrace = getReducedTrace(resultTrace,longestLoop.getStartIndex(), longestLoop.getStartIndex()+longestLoop.getLength());
            
            // verify reduce trace is still ce
            List<InputAction> reducedInputTrace = reducedCeTrace.getInputActions();
            Tuple2<Trace,OutputAction> result= this.run.checkIsCounterExampleCachedAndFixInputs(reducedInputTrace);
            if(result==null) {
                // we cannot remove the loop from the ce trace
            	
            	// the loop is not a possible remove candidate anymore so remove it 
            	loopStore.removeLoop(longestLoop); 
            	
            	// however if there are subloops : split the loop into two subloops and add them 
            	if ( longestLoop.isSplittable() ) {
            		List<Loop> subloops=longestLoop.split();
            		loopStore.addLoops(subloops);
                }
                             
            } else {	
            	//use reduced input trace as new trace
            	resultTrace = result.tuple0;
            	logger.debug("use reduced input trace as new trace");

            	// reinit loops for shorter trace
                loopStore= new LoopStore(hyp,resultTrace.getAbstractInputWord());
            }
        }
        return resultTrace;
    }
}
