package abslearning.ceanalysis;

import abslearning.trace.AbstractionVariable;
import abslearning.trace.action.Parameter;

/**
 * TODO Perhaps make a class for a list of these pairs with parameters from the same action.
 */
public class AbstractionPair {
	public final AbstractionVariable absVariable;
	public final Parameter parameter;
	
	public AbstractionPair(Parameter parameter, AbstractionVariable absVariable ) {
		super();
		this.parameter = parameter;
		this.absVariable = absVariable;
	}
	
	public String toString() {
		return new StringBuilder().append(this.parameter.toStringWithAction()).append(" -> ").append(absVariable.toString()).toString();
	}
}
