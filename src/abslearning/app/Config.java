package abslearning.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.LoggerRepository;

import abslearning.exceptions.ConfigurationException;
import abslearning.exceptions.Messages;
import de.ls5.jlearn.logging.LearnLog;
import de.ls5.jlearn.logging.LogLevel;
import de.ls5.jlearn.logging.PrintStreamLoggingAppender;

import sut.info.SutInfo;
import util.ExceptionAdapter;
import util.JavaUtil;



/*

IMPORTANT
    in config.yaml
      sutInterface_sutinfoFile : path relative from config.yaml file
      sutInterface_model_file : path relative from config.yaml file   -> only used when doing simulation
      learnResults_outputDir   : path relative from current working directory
      logging_logDir           : path relative from learnResults_outputDir   -> in future should be maybe relative to cwd (again!)
*/

public class Config extends util.Config {

    private static Logger logger = Logger.getLogger(Config.class);; // logger is initialized when logging is initialized in initialize();
    public static String currentOutputDir; // only use as quick hack to get outputDir to write some stuff when debugging


    // get info about sut : name, input and output alphabet
    public String sutInterface_sutinfoFile="sutinfo.yaml";
    // Note: doesn't need to be set when simulating sut using model , because
    //       then generated from model

    // set type of communication with sut, and then set per type some parameters
    public String sutInterface_type = "model"; // "socket";
    // socket communication
    public int sutInterface_socket_portNumber = 7892;
    public String sutInterface_socket_server = "localhost";
    // directCall communication to existing jar file
    // note: next to  a jar file also a sutinfo file must be supplied from which we fetch the sut info
    public String sutInterface_jar_file = null; // the jar file must always be in same directory as the config file
    public String sutInterface_jar_package = "generated.sut";
    public String sutInterface_jar_className = "SutImpl";
    // communication with simulation of sut from a model
    public String sutInterface_model_file = "model.xml";  // the model file must always be in same directory as the config file
    public String sutInterface_model_sutName = "${modelDirName}"; // sut name for model , which can be defined using the following
                                                                   // parameters :
                                                                   //  ${modelFileName} : name of model file
                                                                   //  ${modelDirName} : name of directory containing model
    // static because when generating code from model package and classname is always the same
    static public String sutInterface_model_package = "generated.sut";
    static public String sutInterface_model_className = "SutImpl";


    public List<String> learning_relations = Arrays.asList("EQUAL"); // relations used for learning ["EQUAL", "SUCC"]


    public boolean learning_reuseCounterExample = true;
    public boolean learning_reduceCounterExample = true;

    // the learning algorithm used, "observationPack", "angluin","ttt"
    public String learning_algorithm = "observationPack";

    // fresh input values are obtained by multiplying this step.
    // For example, for step 100: 10 110 210...
    // this ensures that a new value generated is not related to a previous one
    // (when only successor relations are considered)
    // in the sequence: 10 11 12 100; 12 is successor of 11, 11 of 10, 100 (a new
    // value) is independent of either 10,11 or 12
    public int learning_freshValueStep = 100;  // set to 100 for models with successors otherwise keep to 1 !


    public List<String> learning_reductionStrategies = Arrays.asList("loop", "singleTransition");

    public int learning_maxTime = 0; // maxtime in seconds. Where 0 means no maxtime.
    public int learning_maxNumRuns = -1; // a limit to the number of runs. -1 means no limit.


    // IMPLEMENTATION detail:
    // iF true the HypothesisBuilder builds the concrete hyp by generating code and then compiling it.
    // Alternatively, it builds the hyp in memory from the hyp's abstract automaton.
    // NOTE:
    //  * currently we only use it as false (builds the hyp in memory from the hyp's abstract automaton.)
    //  * for the sut model we do still generate code!!
    public boolean learning_generateHypCode = false;




    
    //cache
    public String cache_loadFrom;
    public String cache_dumpTo;

    // reconstructs the abstractions every time a new lookahead is added, needed if memVOrder is altered
    public boolean learning_reconstructAbstractions = false;

    // testing
    public long testing_seed = 123;
    // the interval within which fresh input values are choosen
    public int testing_minValue = 0;
    public int testing_maxValue = 100;

    public boolean testing_valuesExtendable = true; // is the range extendable in case we use up the fresh value range
    // possible test methods : ["randomWalk","randomWalkFromState", "traces", "yannakakis", "cadp"]
    public List<String> testing_methods = Arrays.asList();
    // randomWalkFromState specific
    public double testing_randomWalkFromState_drawFresh = 0.2;
    public int testing_randomWalkFromState_maxNumTraces = 100;
    public int testing_randomWalkFromState_randomLength = 10;
    // randomWalk specific
    public double testing_randomWalk_drawFresh = 0.2;
    public int testing_randomWalk_maxNumTraces = 100;
    public int testing_randomWalk_minTraceLength = 0;
    public int testing_randomWalk_avgNumStepsAfterMinLength = 10;
    public int testing_randomWalk_maxTraceLength = 100;
    // yannakakis specific
    public double testing_yannakakis_drawFresh = 0.2;
    public int testing_yannakakis_maxNumTraces = 100;
    public String testing_yannakakis_cmd = null;
    // traces specific
    public List<String> testing_traces_testTraces = Collections.emptyList();

    // verification
    public String verification_when = "atEnd"; // "atEnd", "afterHyp"
    //  which verification methods used
    public List<String> verification_methods = Arrays.asList(); // can be any sublist of ["cadp", "traces", "dataStructure"]
    // traces specific
    public List<String> verification_traces_verificationTraces = null;
    // cadp specific
    public String verification_cadp_server = null; // eg. "http://remote.compare.server:8000"; note: null means do local compare
    public int verification_cadp_compareMinValue = 0;
    public int verification_cadp_compareMaxValue = 2;
    // dataStructure specific
    public String verification_dataStructure_type = null;  // supported types: fifo,lifo, map or fifo-set
    public int verification_dataStructure_size = 0;




    public String learnResults_outputDir = "output/${sutname}";
    // note:  params ${timestamp} and ${sutname} are filled in dynamically
    // alternative: "output/${timestamp}_${sutname}";

    // abstract model
    public String learnResults_abstractModelDotFile = "learnedAbstractModel.dot";
    public String learnResults_abstractModelPdfFile = "learnedAbstractModel.pdf";
    public boolean learnResults_writeAbstractModelPdfFile = false;

    // concrete model
    public String learnResults_learnedConcreteModelFile = "learnedConcreteModel.xml";

    // abstractions
    public String learnResults_abstractionFile = "learnedAbstraction.txt";
    public String learnResults_learnedModelDataFile = "learnedModelData.json";

    // statistics
    public String learnResults_statisticsFile = "statistics.txt";
    public String learnResults_statisticsWithReport = "statistics_with_report.txt";
    public String learnResults_ReportFile = "report.txt";
    public String learnResults_statisticsJsonFile = "statistics.json";

    /*
     * log4j : The levels of logging are TRACE, DEBUG, INFO, WARN, ERROR and FATAL.
     *
     * ALL - has the lowest possible rank and is intended to turn on all logging.
     *
     * TRACE - finer-grained informational events than the DEBUG DEBUG -
     * fine-grained informational events that are most useful to debug an
     * application.
     *
     * INFO - informational messages that highlight the progress of the
     * application at coarse-grained level.
     *
     * WARN - potentially harmful situations.
     *
     * ERROR - error events that might still allow the application to continue
     * running.
     *
     * FATAL - very severe error events that will presumably lead the
     * application to abort. OFF - has the highest possible rank and is intended to
     * turn off logging.
     *
     * learnlib logging: The levels of logging are FINE, DEBUG, INFO, WARN, and
     * ERROR Default the loglevel : OFF which means leanlib logging not turned on.
     */

    // general log dir
    public String logging_logDir = "log/";
    public String logging_hypDir = "generated/hypothesis/";

    // configure which logging messages are stored where
    public String logging_logFile = "log.txt";
    public String logging_logFileThreshold = "off"; // if "off" nothing is logged to file, enable it to log everything
                                                    // by setting it to "debug";
    public String logging_consoleThreshold = "info";
    public String logging_consoleStream = "stdout";  // alternative: stderr, but because stdout not used for library we use that

    // configure which logging messages are generated
    public String logging_rootLoggerLevel = "info";
    public LinkedHashMap<String, String> logging_childLoggerLevels;

    // extra log4j properties
    public LinkedHashMap<String, String> logging_log4jExtraProperties;

    // display level or the learnlib tool used ; to disable set to"off"
    public String logging_learnlibLogLevel = "off";  //  "info";
    //public String logging_learnlibLogLevel = "info";  //  "info";

    // special logs
    // - not generated by log4j
    public boolean logging_special_hypotheses = true; // DON'T set to false => NEEDED for concrete hyp generation!
    public boolean logging_special_hypothesesWritePdf = false;

    // public boolean logging_special_abstractions = true;
    // public String logging_special_abstractionsFile = "abstractions.txt";

    // - generated by log4j
    // log learning or testing traces ; both display both concrete and abstract traces
    public boolean logging_special_memTraces = false;  // learning traces
    public boolean logging_special_equivTraces = false; // test traces
    public String logging_special_equivTracesFile = "equivTraces.txt";
    public String logging_special_memTracesFile = "memTraces.txt";



    public boolean logging_special_concreteTree = false;
    public String logging_special_concreteTreeFile = "concreteTree.dot";

    public boolean logging_special_concreteTreeStatistics = false;
    public String logging_special_concreteTreeStatisticsFile = "concreteTreeStatistics.json";
    public boolean logging_special_plotConcreteTreeStatistics = false;

    // original params given for learning
    public LinkedList<String> params_originalArgs;
    public String params_configFile;
    public String params_tomteRootPath;
    public int params_verboseLevel = 0;

    public ArrayList<String> dependency_extraPaths; // get prepended to PATH so that they have higher priority
    public String dependency_pythonCmd = "python";
    public String dependency_javaCmd = "java";
    public String dependency_javacCmd = "javac";
    public String dependency_tomteLearnresult2uppaalCmd = "tomte_learnresult2uppaal";
    public String dependency_sutUppaal2JarCmd = "sut_uppaal2jar";
    public String dependency_sutUppaal2SutInfoCmd = "sut_uppaal2sutinfo";

    // public String dependency_sutUppaal2Java="sut_uppaal2java"; //deprecated in
    // favor of dependency_sutUppaal2JarCmd
    public String dependency_sutRunCmd = "sut_run";
    public String dependency_sutCompareRemoteCmd = "sut_compare_remote";
    public String dependency_sutCompareCmd = "sut_compare";

    public String dependency_cadpCmd = "cadp_lib";

    // only for development :
    // ------------------------
    public boolean devel_printStackTraceOfExceptions = true;
	public boolean learning_sourcedLookaheads = false;
	// debug mode, run a single trace on the system and then exit
    public String devel_trace = null;

    // singleton
    // ---------
    private static final Config INSTANCE = new Config();

    public static Config getInstance() {
        return INSTANCE;
    }

    // constructor cannot be called because private => we can only get single
    private Config() {
    }

    /**
     * initialize config from cmdline arguments
     * <p>
     * NOTE: configFile is an obligated argument from which configuration is read
     * in, but also other arguments may effect the config
     *
     * @param args
     */
    public void initFromTomteCmdlineArguments(String[] args) {
        String configFile = null;

        // manual read in commandline args
        Commandline.handleArgs(args);

        // TOMTE_ROOT_PATH : root path where tomte is installed ( in development this
        // folder is also an eclipse project )
        // - if called by eclipse this is the current working directory -> we can just simply use "" as TOMTE_ROOT_PATH
        // - if called not by eclipse we have to supply it as an option :
        //       --tomte-root-path dir
        // For example is needed to read the release version of tomte!
        if (Commandline.tomteRootPath != null) {
            params_tomteRootPath = Commandline.tomteRootPath;
        } else {
            params_tomteRootPath = (new File("")).getAbsolutePath(); // current working directory
        }

        // get verbose level
        params_verboseLevel = Commandline.verboseLevel;

        // fetch config
        // - read in default config file
        String defaultConfigFile =params_tomteRootPath + File.separator + "config" +  File.separator + "config.yaml";
        System.out.println("Reading in default config file: " + defaultConfigFile);
        handleConfigFile(defaultConfigFile);
        // - read in config file specified on command line
        configFile = Commandline.configFile;
        System.out.println("Reading in config file: " + configFile);
        handleConfigFile(configFile);
        // - read in local config file when give
        String localConfigFile = params_tomteRootPath  + File.separator + "config.yaml";
        if (  util.Filesystem.isreadablefile(localConfigFile) ) {
            System.out.println("Reading in local config file: " + localConfigFile);
            handleConfigFile(localConfigFile);
        }
        // - handle config options on commandline -> commandline options have higher priority then local options!!
        handleConfigOptions(Commandline.configOptions);

        // store original configFile and args
        params_configFile = configFile;
        params_originalArgs = new LinkedList<String>(Arrays.asList(args));

        // cmdline options overrule config options :
        if (Commandline.seed != 0)
            testing_seed = Commandline.seed;
        if (Commandline.maxNumTraces != 0) {
            testing_randomWalk_maxNumTraces = Commandline.maxNumTraces;
            testing_randomWalkFromState_maxNumTraces = Commandline.maxNumTraces;
        }
        if (Commandline.minTraceLength != 0)
            testing_randomWalk_minTraceLength = Commandline.minTraceLength;
        if (Commandline.maxTraceLength != 0)
            testing_randomWalk_maxTraceLength = Commandline.maxTraceLength;
        if (Commandline.outputDir != null)
            learnResults_outputDir = Commandline.outputDir;
        if (Commandline.portNumber != 0)
            sutInterface_socket_portNumber = Commandline.portNumber;
        if (Commandline.serverAddress != null)
            sutInterface_socket_server = Commandline.serverAddress;
    }



    public void validate() {

        System.out.println("environment PATH: " + util.Os.getPath());

        // validate config params
        // -------------------------

        // add extra paths to PATH environment variable
        if (dependency_extraPaths != null) {
            for (String path : dependency_extraPaths) {
                util.Os.prependToPath(path);
            }
        }
        // tomte commands should be normally in tomteRootPath/bin/
        util.Os.prependToPath(params_tomteRootPath + "/bin/");
        // note: but that can be overridden in one of the above extraPaths

        // basic requirements
        dependency_javaCmd = util.Check.programExist(dependency_javaCmd);
        dependency_pythonCmd = util.Check.programExist(dependency_pythonCmd);
        dependency_tomteLearnresult2uppaalCmd = util.Check.programExist(dependency_tomteLearnresult2uppaalCmd);

        if (sutInterface_type.equals("model")) {
            // if using simulation using a model generate an implementation for our SUT model

            dependency_javacCmd = util.Check.programExist("javac");
            JavaUtil.setJavacCmd(dependency_javacCmd);

            dependency_sutUppaal2SutInfoCmd = util.Check.programExist(dependency_sutUppaal2SutInfoCmd);
            dependency_sutUppaal2JarCmd = util.Check.programExist(dependency_sutUppaal2JarCmd);
        }

        if (sutInterface_type.equals("jar")) {
             dependency_sutUppaal2JarCmd = util.Check.programExist(dependency_sutUppaal2JarCmd);
        }


        //  cadp verification only done when learning a sut simulated by a model to afterwards verify learned model simular
        if (verification_methods.contains("cadp") && ! sutInterface_type.equals("model")) {
                System.err.println("remove cadp verification because we can only do cadp verification when having a model ; that is if sutInterface_type==model");
                verification_methods.remove("cadp");
        }

        // disable cadp verification if cadp not locally installed and no remote cadp
        // verification server is configured
        if (verification_methods.contains("cadp") && verification_cadp_server == null) {
            String foundPath = util.Os.which(dependency_cadpCmd);
            if (foundPath == null) {
                System.err.println("remove cadp verification because '" + dependency_cadpCmd + "' cannot be found on path: "+ util.Os.getPath());
                verification_methods.remove("cadp");
            }
        }

        if (verification_methods.contains("cadp")) {
            // both programs come with SUT tool, so really one need to be checked, however
            // to be safe we check both
            dependency_sutCompareCmd = util.Check.programExist(dependency_sutCompareCmd);
            dependency_sutCompareRemoteCmd = util.Check.programExist(dependency_sutCompareRemoteCmd);
        } else {
            dependency_sutCompareRemoteCmd = null; // by making it null prevents it to be printed in effective config
            dependency_sutCompareCmd = null; // by making it null prevents it to be printed in effective config
        }

        // validate config parameters
        // -------------------------------
        if (testing_maxValue < testing_minValue) {
            throw new ConfigurationException(Messages.WRONG_LEARNING_VALUES);
        }

        if (testing_randomWalk_maxTraceLength < testing_randomWalk_minTraceLength) {
            throw new ConfigurationException(Messages.WRONG_TESTING_VALUES);
        }

    }



    public void initialize() throws FileNotFoundException {

        Config config = this;

        // initialize simulation when used
        //--------------------------------
        if (config.sutInterface_type.equals("model") || config.sutInterface_type.equals("jar")) {
            initSimulation(config);
        }

        // determine SUT name and timestamp which are needed for naming output/log dir
        // ----------------------------------------------------------------------------
        String sutName = getSutName(config);
        SutInfo.name = sutName; // will be obsolete later when we better generate sutinfo file
        String timestamp = util.Time.getTimestamp().replace(':', '.');
        // note timestamp has ':' which is not allowed in directory names

        // init outputDir and Logging
        // ---------------------------
        // - initializes logging
        // - for that it most create an output folder which is parameterized with
        // sutname and timestamp
        // - it writes the current config with the original output and log folder in the
        // new created output folder
        // - then it adapts the config with the new parameterized output and log folder
        String outputDir = initOutputDir(config.learnResults_outputDir, sutName, timestamp);
        String logDir = initLoggingUsingProperties(config, outputDir);

        logger.info("SUT name: " + sutName);
        logger.info("output dir: " + outputDir);
        logger.info("log dir: " + logDir);

        // log original and effective config file and switch config to timestamped
        // outputDir and logDir
        // --------------------------------------------------------------------------------------------
        logOriginalAndEffectiveConfigFile(outputDir + "/input/", config);
        config.logging_logDir = logDir;
        config.learnResults_outputDir = outputDir;

        // setup learnlib logging
        // -------------------------------------------------
        // order: FINE,DEBUG,INFO,WARN,ERROR
        LogLevel learnlibLogLevel = null;
        boolean turnOnLearnlibLogging = true;
        config.logging_learnlibLogLevel = config.logging_learnlibLogLevel.toLowerCase();
        if (config.logging_learnlibLogLevel.equals("fine")) {
            learnlibLogLevel = LogLevel.FINE;
        } else if (config.logging_learnlibLogLevel.equals("debug")) {
            learnlibLogLevel = LogLevel.DEBUG;
        } else if (config.logging_learnlibLogLevel.equals("info")) {
            learnlibLogLevel = LogLevel.INFO;
        } else if (config.logging_learnlibLogLevel.equals("warn")) {
            learnlibLogLevel = LogLevel.WARN;
        } else if (config.logging_learnlibLogLevel.equals("error")) {
            learnlibLogLevel = LogLevel.ERROR;
        } else if (config.logging_learnlibLogLevel.equals("off")) {
            turnOnLearnlibLogging = false;
        } else {
            throw new ConfigurationException(Messages.WRONG_LEARNLIB_LOG_LEVEL);
        }
        if (turnOnLearnlibLogging) {

           if (config.logging_consoleStream.equals("stdout")  ) {
                LearnLog.addAppender(new PrintStreamLoggingAppender(learnlibLogLevel, System.out));
           } else {
               LearnLog.addAppender(new PrintStreamLoggingAppender(learnlibLogLevel, System.err));
           }

        }

    }

    /**
     * Returns true if it appears that log4j have been previously configured. This
     * code checks to see if there are any appenders defined for log4j which is the
     * definitive way to tell if log4j is already initialized
     */
    public static boolean isLog4jConfigured() {
        Enumeration<?> appenders = Logger.getRootLogger().getAllAppenders();
        if (appenders.hasMoreElements()) {
            return true;
        } else {
            Enumeration<?> loggers = LogManager.getCurrentLoggers();
            while (loggers.hasMoreElements()) {
                Logger c = (Logger) loggers.nextElement();
                if (c.getAllAppenders().hasMoreElements())
                    return true;
            }
        }
        return false;
    }

    public static void initSimulation(Config config) {
            // SUT is simulated

            if (config.sutInterface_type.equals("jar")) {
                // do simulation with a predefined jar file with sutinfo file

                // config.learning_useExistingJarfile is taken as path relative from config dir
                // TODO: also allow absolute path for config.learning_useExistingJarfile
                String configDir = (new File(config.params_configFile)).getParent();
                config.sutInterface_jar_file = configDir + "/" + config.sutInterface_jar_file;
                util.Check.readableFile(config.sutInterface_jar_file);

            } else if (config.sutInterface_type.equals("model") ) {
                // do simulation with a model file

                // generate sut jar and sutinfo file from model
                config.sutInterface_sutinfoFile = "__SIMULATION__GENERATED__"; // mark in effective config file we ignored sutinfo file

                // when simulating SUT we determine SUT name from modelfile :

                // first get modelfile
                String configDir = (new File(config.params_configFile)).getParent();
                String modelFile = configDir + File.separator + (new File(config.sutInterface_model_file)).toString();
                util.Check.readableFile(modelFile);
                config.sutInterface_model_file= modelFile; // set resolved absolute path to model in config (instead of config file relative file)

                String sutName = config.sutInterface_model_sutName;
                String modelFileName = (new File(modelFile)).getName();
                String modelDirPath = (new File(modelFile)).getParent();
                String modelDirName = (new File(modelDirPath)).getName();
                config.sutInterface_model_sutName = sutName.replace("${modelFileName}", modelFileName).replace("${modelDirName}", modelDirName);
            } else {
                throw new ConfigurationException("invalid simulation method");
            }
    }


    /*
     * getSutName - get name for SUT
     */
    public static String getSutName(Config config) {
        // determine SUT name
        String sutName;
        if (config.sutInterface_type.equals("model")) {
           // SUT is simulated with model
           // note: sutinfo is generated from model
            sutName=config.sutInterface_model_sutName;
        } else {
            // sut name must be set in sutinfo file
            loadSutInfo(config);
            sutName = SutInfo.getName();
        }
        return sutName;
    }

    public static void loadSutInfo(Config config) {
        // For normal SUT we get an sutinfo file which must describe SUT name.
        // - read in information given about sut from SutInfo.yaml file
        // - config.learning_sutinfoFile is taken as path relative from config dir
        // TODO: also allow absolute path for config.learning_sutinfoFile
        // TODO: add check file is readable
        String configDir = (new File(config.params_configFile)).getParent();
        String sutinfoFilePath = configDir + "/" + config.sutInterface_sutinfoFile;

        System.err.println("configDir: " + configDir);
        System.err.println("config.learning_sutinfoFile: " + config.sutInterface_sutinfoFile);
        System.err.println("reading sutinfoFile: " + sutinfoFilePath);

        SutInfo.loadFromYaml(sutinfoFilePath);
        config.sutInterface_sutinfoFile = sutinfoFilePath;
    }

    public static String initOutputDir(String parameterizedOutputDir, String sutName, String timestamp)
            throws FileNotFoundException {
        String outputDir;

        // create output directory
        // ----------------------------------------------
        // output directory can be parameterized with :
        // ${timestamp} : dynamically generated timestamp which allows you to
        // store output in a per execution unique directory
        // so that results of previous executions are never overwritten
        // ${sutname} : to quickly see name of sut in output directory
        outputDir = parameterizedOutputDir.replace("${timestamp}", timestamp).replace("${sutname}", sutName) + "/";

        // reuse existing output dir if possible, or just make new one
        if (util.Filesystem.isexisting(outputDir)) {
            if (util.Filesystem.iswritabledir(outputDir)) {
                System.out.println("WARNING: deleting existing output directory : " + outputDir);
                util.Filesystem.rmdirhier(outputDir);
            } else {
                throw new ConfigurationException("problem creating directory: " + outputDir);
            }
        }
        util.Filesystem.mkdirhere(outputDir);

        // create <outputDir>/input/
        util.Filesystem.mkdirhere(outputDir + "/input/");

        currentOutputDir = outputDir;// only use as quick hack to get outputDir to write some stuff when debugging

        return outputDir;
    }


    public static void initBasicLogging() {
        BasicConfigurator.configure();
        // get the containing repository
        LoggerRepository repository = logger.getLoggerRepository();
        // Set the hierarchy-wide threshold to INFO
        repository.setThreshold(Level.INFO);

        // adapt basic configurator with different PatternLayout
        Logger root=repository.getRootLogger();
        root.removeAllAppenders();
        Layout layout = new PatternLayout("TOMTE [%p]  %C.%M(%C{1}.java:%L): %m%n");
        ConsoleAppender consoleAppender=new ConsoleAppender(layout);
        root.addAppender(consoleAppender);

        logger.info("basic logging configured");
    }


    public static String initLoggingUsingProperties(Config config, String outputDir) throws FileNotFoundException {

        // see
        // http://stackoverflow.com/questions/8489551/logging-error-to-stderr-and-debug-info-to-stdout-with-log4j#answer-10314912
        // for explanation for how to use min and max level in properties file.

        // initialize logging
        // -------------------------------------------------

        // hyp/logging dir is specified relative output dir
        String logDir = outputDir + config.logging_logDir + "/";
        String hypDir = outputDir + config.logging_hypDir + "/";

        Properties properties = new Properties();

        // set params in log4j config which can be used in log4j config file
        // -> make is possible to use the logDir set in the config.yaml file in the
        // log4j config file!
        properties.setProperty("logDir", logDir);
        properties.setProperty("logFile", config.logging_logFile);
        // threshold for console and default log file
        properties.setProperty("logFileThreshold", config.logging_logFileThreshold);

        // all logging either to stdout or to stderr
        String consoleStdOutThreshold,consoleStdErrThreshold;
        if (config.logging_consoleStream.equals("stdout")  ) {
             consoleStdOutThreshold = config.logging_consoleThreshold.toUpperCase();
             consoleStdErrThreshold = "OFF";
        } else {
             consoleStdOutThreshold = "OFF";
             consoleStdErrThreshold = config.logging_consoleThreshold.toUpperCase();
        }
        properties.setProperty("consoleStdOutThreshold", consoleStdOutThreshold);
        properties.setProperty("consoleStdErrThreshold", consoleStdErrThreshold);

        // special logfiles
        properties.setProperty("memTracesFile", config.logging_special_memTracesFile);
        properties.setProperty("equivTracesFile", config.logging_special_equivTracesFile);

        // configure rootLogger with its appenders
        properties.setProperty("rootLogLevel", config.logging_rootLoggerLevel);
        String rootAppenders = "";
        if (!config.logging_consoleThreshold.equals("off")) {
            rootAppenders = rootAppenders + " , stdout, stderr";
        }
        if (!config.logging_logFileThreshold.equals("off")) {
            rootAppenders = rootAppenders + " , log_file";
        }
        properties.setProperty("rootAppenders", rootAppenders);

        try {
            Properties properties_adv = new Properties();
            properties_adv.load(new FileReader(config.params_tomteRootPath + "/config/log4j.conf"));
            properties.putAll(properties_adv);

            if (config.logging_childLoggerLevels != null) {
                for (String key : config.logging_childLoggerLevels.keySet()) {
                    properties.setProperty("log4j.logger." + key, config.logging_childLoggerLevels.get(key));
                }
            }
            // enable loggers based on config
            if (config.logging_special_memTraces) {
                properties.setProperty("log4j.logger.memTraces", "debug, memTraces_file");
            }
            if (config.logging_special_equivTraces) {
                properties.setProperty("log4j.logger.equivTraces", "debug, equivTraces_file");
            }

        } catch (IOException e) {
            throw new ExceptionAdapter(e);
        }

        // extra properties are always added
        if (config.logging_log4jExtraProperties != null) {
            properties.putAll(config.logging_log4jExtraProperties);
        }

        // apply logging properties
        LogManager.resetConfiguration(); // reset Basic logging configuration done at startup using Config.initBasicLogging()
        PropertyConfigurator.configure(properties);

        // store final logging properties in a file
        PrintWriter log4jStream = util.Filesystem.getUtf8FilePrintWriter(outputDir + "/input/log4j.conf");
        properties.list(log4jStream);
        log4jStream.close();

        util.Filesystem.mkdirhere(logDir);
        if (config.logging_special_hypotheses)
            util.Filesystem.mkdirhere(hypDir);

        return logDir;
    }

    public static void logOriginalAndEffectiveConfigFile(String outputDir, Config config) throws FileNotFoundException {

        // log original configuration input into output folder
        // ----------------------------------------------------
        // copy original configuration to input folder
        util.Filesystem.copyfile(config.params_configFile, outputDir + "/config.yaml");

        // write original cmdline params to input folder into file cmdlineparams.txt
        PrintWriter argsStream = util.Filesystem.getUtf8FilePrintWriter(outputDir + "/cmdlineparams.txt");
        argsStream.print(config.params_originalArgs);
        argsStream.close();

        // log effective configuration input into output folder
        // ----------------------------------------------------
        // effective configuration : config.yaml combined with cmdline params
        StringWriter strWriter = new StringWriter();
        config.dumpAsYaml(strWriter, "    ");
        String configString = strWriter.toString();

        // log to general log
        logger.info("CURRENT CONFIG:\n\n" + configString);

        // log to input folder
        PrintWriter configStream = util.Filesystem.getUtf8FilePrintWriter(outputDir + "/config.effective.yaml");
        configStream.print(configString);
        configStream.close();

    }


}
