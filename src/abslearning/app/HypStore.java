package abslearning.app;

import java.io.File;

import abslearning.logger.LearnLogger;

public class HypStore {

    public static String getHypFile(int hypCounter, String extension) {
        String filename = "hyp" + hypCounter + extension;
        return LearnLogger.hypDir + filename;
    }

    public static String getAbstractionsJsonFile(int hypCounter) {
        return getHypFile(hypCounter, ".abstractions.json");
    }

    public static String getAbstractionsTxtFile(int hypCounter) {
        return getHypFile(hypCounter, ".abstractions.txt");
    }

    public static String getAbstractHypothesisDotFile(int hypCounter) {
        return getHypFile(hypCounter, ".orig.abstract.dot");
    }

    public static String getAbstractReducedHypothesisDotFile(int hypCounter) {
        return getHypFile(hypCounter, ".abstract.dot");
    }

    public static String getConcreteHypXmlFile(int hypCounter) {
        String xmlFile = getHypFile(hypCounter, ".concrete.xml");
        return xmlFile;
    }
    
    public static void buildConcreteHypXml(String jsonAbsFile, String dotFile, String xmlFile) {
    	if (!checkFileExists(xmlFile)) {
	    	sut.implementation.Util.createUppaalModelFileFromAbstractionsAndAbstractModel(jsonAbsFile, dotFile, xmlFile,
	                false);
    	}
    }

    private static boolean checkFileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    public static String getHypJarFile(int hypCounter, String suffix) {
        return getHypFile(hypCounter, suffix + ".concrete.jar");
    }

}
