package abslearning.app;


import java.util.LinkedList;

public class Commandline {
	public static int portNumber;
	public static String serverAddress;
	public static int maxNumTraces;
	public static int minTraceLength;
	public static int maxTraceLength;
	public static long seed;
	public static String outputDir;

	public static String configFile;
	public static String tomteRootPath;


	public static int verboseLevel=0;

	public static LinkedList<String> configOptions=new LinkedList<String>();


	public static void handleArgs(String[] args) {


		LinkedList <String> normalArgs= new LinkedList<String>();
		for (int i = 0; i < args.length; i++) {
			String argument=args[i];
			String lowerCaseOption=argument.toLowerCase();	// allow options both lowercase as uppercase
			if ("--help".equals(lowerCaseOption) || "-h".equals(lowerCaseOption)  ) {
				printUsage();
				System.exit(0);
			}
			if ("--verbose".equals(lowerCaseOption) || "-v".equals(lowerCaseOption)  ) {
				verboseLevel = 1;
				continue;
			}
			if ("-vv".equals(lowerCaseOption)  ) {
				verboseLevel = 2;
				continue;
			}
			if ("--tomte-root-path".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --tomte-root-path.");
					printUsage();
					System.exit(-1);
				}
				tomteRootPath = args[++i];
				continue;
			}
			if ("--output-dir".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --output-dir.");
					printUsage();
					System.exit(-1);
				}
				outputDir = args[++i];
				continue;
			}
			if ("--config-option".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --config-option.");
					printUsage();
					System.exit(-1);
				}
				configOptions.add(args[++i]);
				continue;
			}
			if ("--max-traces".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --max-traces.");
					printUsage();
					System.exit(-1);
				}
				try {
					maxNumTraces = new Integer(args[++i]);
				} catch (NumberFormatException ex) {
					System.err.println("Error parsing argument for --max-traces. Must be integer. " + args[i]);
					System.exit(-1);
				}
				continue;
			}
			if ("--min-trace-length".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --min-trace-length.");
					printUsage();
					System.exit(-1);
				}
				try {
					minTraceLength = new Integer(args[++i]);
				} catch (NumberFormatException ex) {
					System.err.println("Error parsing argument for --min-trace-length. Must be integer. " + args[i]);
					printUsage();
					System.exit(-1);
				}
				continue;
			}
			if ("--max-trace-length".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --max-trace-length.");
					printUsage();
					System.exit(-1);
				}
				try {
					maxTraceLength = new Integer(args[++i]);
				} catch (NumberFormatException ex) {
					System.err.println("Error parsing argument for --max-trace-length. Must be integer. " + args[i]);
					printUsage();
					System.exit(-1);
				}
				continue;
			}
			if ("--seed".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --seed.");
					printUsage();
					System.exit(-1);
				}
				try {
					seed = new Long(args[++i]);
				} catch (NumberFormatException ex) {
					System.err.println("Error parsing argument for --seed. Must be integer. " + args[i]);
					printUsage();
					System.exit(-1);
				}
				continue;
			}
			if ("--port".equals(lowerCaseOption)) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --port.");
					printUsage();
					System.exit(-1);
				}
				try {
					portNumber = new Integer(args[++i]);
				} catch (NumberFormatException ex) {
					System.err.println("Error parsing argument for --port. Must be integer. " + args[i]);
					printUsage();
					System.exit(-1);
				}
				continue;
			}
	         if ("--server".equals(lowerCaseOption)) {
	                if (i == args.length - 1) {
	                    System.err.println("Missing argument for --server.");
	                    printUsage();
	                    System.exit(-1);
	                }
                    serverAddress =  args[++i];
	                continue;
	         }
			// assume remaining are normal arguments, that is no option!
			// verify that it is no option
			if ( argument.startsWith("-") ) {
				System.err.println("Unknown option: " + argument);
				printUsage();
				System.exit(-1);
			}
			// none option arguments
			normalArgs.addLast(argument);
		}



	   // enforce parameter config.yaml is given!
	   // note: we also enforce arguments in learn_model.py python script
		if ( normalArgs.size() != 1 ) {
			System.err.println("Only one argument needed; however following arguments given: " + normalArgs);
			printUsage();
			System.exit(-1);
		}
		configFile= normalArgs.get(0);
	}

	public static void printUsage() {
		System.out.println("usage: java -jar tomte.jar [options]  config.yaml");
		System.out.println("");
		System.out.println("   options:");
		System.out.println("    --max-traces       - Maximum number of traces to run during equivalence testing.");
		System.out.println("    --min-trace-length - Minimum length of traces during equivalence query.");
		System.out.println("    --max-trace-length - Maximum length of traces during equivalence query.");
		System.out.println("    --seed             - Seed to use for random number generator.");
		System.out.println("    --output-dir       - Directory to store output.");
	  	System.out.println("    --port n           - Use tcp port n to listen on for incoming connections.");
		System.out.println("    --tomte-root-path  - Tomte Root directory");
		System.out.println("    --config-option    - Overrule a config option. Use as: --config-option optionname=optionvalue");
	  	// When running tomte.jar in production it doesn't know the tomte root directory, but must
		// must be explicitly gives as commandline parameter (when running as eclipse project it is the cwd)
		// Needed for getting release version from the tomte root directory.

	}

}
