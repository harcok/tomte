package abslearning.app;

public enum Phase {
	startOfRun,
	learning,afterLearning,
	testing,afterTesting,
	analyzeCounterexample,afterAnalyzeCounterexample,
	addCounterexample,afterAddCounterexample,
	
}
