package abslearning.app;


/*
 *

statistics:
  per run :
     data
     timings

 timings (stored in run):
    process phase timings  => records times of process
    timers  => records times of some code, useful to test where bottlenecks are
               note: a timer's recorded time can optionally be
                     excluded from phase time!
                     eg. for extra cadp check

thus:
  * statistics has two global variables:
     - currentRun
     - currentPhase   => currently in run, but should be in statistics !!

  *	 remove all specific start and end phase functions
	 instead use
	   switchToPhase( phaseEnum )    => implicit end phase!

  *  in the run per phase records the time in phase
    run.phase2time map!
  note: adds time => so phase x can be active multiple times in a run

  * no data (or code) specific per phase
    instead in the data collector functions
    we can check in which phase we are and
    store the data in the run in a specific variable for that run
    eg. see    incSutInput()

     note: memQueryIndex is set to 0 at start of learning phase
          However this extra field memQueryIndex is unnecesarry
          because  checkTimePassed_MemQueries
          can just use currentRun.numMemQueries instead!!
         => thus no need for code at start of learning phase!

     THUS: phase are just labels,
               * used for phase timings
               * and which  data setters can use to store more specific

  * because of using simple   switchToPhase( phaseEnum )
    and run.phase2time map we can easily display
    per run the time of each phase and the
    for all runs the time of each phase!
    => save lot of cumbersome code!!
    note: makes it also easier to add/remove phases!

 * timer just records the time for some piece of code

    start_timer(label)
     .. code ..
    end_timer(label)

   per run stores in  map  label2time the time recorded by each timer
   note: same timer label may be used multiple times in a run
         (times are just added)

     => timers are useful to find the bottlenecks of your code
        (performance benchmarking!)

 *  IMPORTANT: timers are independent of phases, however they
               can be used to correct phase timings!

    e.g. "cadp_compare" is done to verify that everything is going
         well, but isn't really needed
         => this time should be excluded from the whole learning time


		     start_timer("cadp_compare", exclude=true)
		     .. code ..
		     end_timer("cadp_compare")


       the second param "exclude" of start_timer
       let you specify to exclude this time of the proces's phase time!


 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import ru.yandex.lc.jbd.Dumper;
import util.Time;
import abslearning.exceptions.OutOfTimeException;
import abslearning.trace.Trace;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import de.ls5.jlearn.interfaces.Word;
//import org.json.simple.JSONValue;

//	String runtime_hms= runtime_hours + ":" + runtime_minutes + ":"  + runtime_seconds;
//  statisticsFileStream.println("Total running time: "  + runtime_hms + " (h:m:s)");

public class Statistics {
	private static final Logger logger = Logger.getLogger(Statistics.class);

	private long startTime,endTime;
	public long maxTime=0; // in milliseconds
	public long verificationTime=0; // ignore this time in maxtime
	public long verificationTime_start=0;
	public long verificationTime_stop=0;

	public long processLearningResultTime=0;
	public long processLearningResultTime_start=0;
	public long processLearningResultTime_stop=0;


	public String terminationReason="unknown";
	public Boolean learningSuccessful=null; // null means unknown!

	// we are counting hyps from 1 to n
	private int hypLIndex=0; // 0 means zero hypotheses found, 1 means one hyp. found, etc..
	private int hypMIndex=0; // gets hypL index when hypL is set in Mapper as hypothesis to use for abstractions
	private long numStatesHypL=0;
	private long numStatesHypM=0;

	// object fields
	private LinkedList<SingleRun> runs;
	private SingleRun currentRun;
	private int currentRunNumber=0;

    // params to poll for stop during equivQuery
	// TODO: instead on trace number , use number of actions and reset
	//       then let user specify time neede per action/reset
	private int equivCheckNumber = 10000; //educated guess
    private int equivQueryIndex;

    // params to poll for stop during equivQuery
    private int memQueryCheckNumber = 1000; //educated guess
    private int memQueryIndex;

    PrintStream logStream;


    private Trace lastCe=null;



//---------------------------------------------------------------------------
//  construct/init
//---------------------------------------------------------------------------



	// singleton
	// ---------
	private static final Statistics INSTANCE = new Statistics();

	public static Statistics getInstance() {
		return INSTANCE;
	}

	// constructor cannot be called because private => we can only get single
	private Statistics() {
		runs = new LinkedList<SingleRun>();
		startTime=System.currentTimeMillis();
	}

	public void initStatistics( PrintStream stream ) {
        logStream=stream;
        printStatisticsHeader( logStream);

        startNewRun(true); // create first run
	}


//---------------------------------------------------------------------------
//  run
//---------------------------------------------------------------------------


	class SingleRun {
		// only fields with Expose annotation are serialized in statistics.json file

		@Expose public int runNumber;
		public Phase phase=Phase.startOfRun;

		@Expose public long refinementCounter = 0;
		@Expose public long numStoreUpdatesOnCE = 0;
		@Expose public long numStoreUpdatesOnInconsistency = 0;
		@Expose public long numCounterexamplesForLearner = 0;

		@Expose public int hypLIndex = 0 ;
		@Expose public long numStatesHypL = 0;

		@Expose public int hypMIndex = 0 ;
		@Expose public long numStatesHypM;


		@Expose public boolean learnFromScratch=true;

		@Expose public long numMemQueriesLearning = 0;
		@Expose public long numMemInputsLearning = 0;
		@Expose public long numMemQueriesCeAnalysis = 0;
		@Expose public long numMemInputsCeAnalysis = 0;
		@Expose public long numEquivQueries = 0;
		@Expose public long numEquivInputs = 0;
		//public long timeMemQueries = 0;
		//public long timeEquivQueries = 0;

		@Expose public long startLearningTime = 0;
		@Expose public long stopLearningTime = 0;
		@Expose public long startTestingTime = 0;
		@Expose public long stopTestingTime = 0;
		@Expose public long startAnalysisTime = 0;
		@Expose public long stopAnalysisTime = 0;


		public String  descriptionTerminationRun="";

		// learn inputs are effectively send by lookahead to SUT
		// because when a membership query is executed the future is already
		// executed by lookahead
		@Expose public long numLearnResets = 0;
		@Expose  public long numLearnInputs = 0;

		@Expose public long numTestResets = 0;
		@Expose public long numTestInputs = 0;

		@Expose public long numCeAnalysisResets = 0;
		@Expose public long numCeAnalysisInputs = 0;

		@Expose public int numNodesInTree=0;
		@Expose public int numEndNodesInTree=0;



//		public Trace ceTraceOnSut;
//		public Trace ceTraceOnHyp;
//		public Word abstractCeInputs;
//		public Word abstractCeOutputsSut;
//		public Word abstractCeOutputsHyp;
//		public Trace ceSimplifiedTraceOnSut;
//		public Trace ceSimplifiedTraceOnHyp;
//		public LookaheadTrace lookaheadTrace;
//		public Edge ceSimplifiedEdge;
//		public Symbol abstraction;


		public Trace ceEquivOracle;
		public Trace ceAfterLoopReduction;
		@Expose public Trace ceAfterCeAnalysis;

		@Expose public int sizeCeEquivOracle=0;
		@Expose public int sizeCeAfterLoopReduction=0;
		@Expose public int sizeCeAfterCeAnalysis=0;

		@Expose public int numOfReusedCounterExamples=0;



        // results stored during learning => only for reporting needed
		//TODO: make Result class with always :
		//              short description
		//              boolean containsInfoToContinueLearning
		//        => extra fields for specific  sub Result class
		// => store results in list => keeps order of finding the results
		public String ceAfterLoopReduction_TraceOnSut;
		public String ceAfterLoopReduction_TraceOnHyp;
		public String abstractCeInputs;
		public String abstractCeOutputsSut;
		public String abstractCeOutputsHyp;
		public String ceAfterCeAnalysis_TraceOnSut;
		public String ceAfterCeAnalysis_TraceOnHyp;
		public String lookaheadUpdateOnCE;
		public String lookaheadUpdateOnInconsistency;
		public String abstraction;
		public String ceAfterCeAnalysis_TraceOnSutWithStatevars;
        public String ceAfterCeAnalysis_TraceOnHypWithStatevars;


	}

	public static class Total {
		// only fields with Expose annotation are serialized in statistics.json file
		@Expose LinkedList<SingleRun> runs;
		@Expose public int numRuns;

		@Expose long numMemQueriesLearning = 0;
		@Expose long numMemInputsLearning = 0;
		@Expose long numMemQueriesCeAnalysis = 0;
		@Expose long numMemInputsCeAnalysis = 0;
		@Expose long numEquivQueries = 0;
		@Expose long numEquivInputs = 0;
		@Expose long numEquivQueriesWithLastRun=0;
		@Expose long numEquivInputsWithLastRun=0;


		@Expose long finalNumStates=0;
		@Expose long refinementCounter = 0;
		@Expose long numStoreUpdatesOnCe = 0;
		@Expose long numStoreUpdatesOnInconsistency = 0;
		@Expose long numCounterexamplesForLearner = 0;

		@Expose long numLearnResets = 0;
		@Expose long numLearnInputs = 0;
		@Expose long numTestResets =  0;
		@Expose long numTestInputs = 0;
		@Expose long numCeAnalysisResets = 0;
		@Expose long numCeAnalysisInputs  = 0;
		@Expose long numTestResetsWithLastRun=0;
		@Expose long numTestInputsWithLastRun=0;

		@Expose long startTime= 0;
		@Expose long endTime= 0;
		@Expose long realTime= 0;

		@Expose long timeLearning = 0;
		@Expose long timeTesting = 0;
		@Expose long timeTestingWithLastRun=0;
		@Expose long timeAnalysis = 0;

		@Expose long verificationResultTime=0;
		@Expose public long processLearningResultTime=0;

		@Expose public String terminationReason;
		@Expose public Boolean learningSuccessfull;

		@Expose public int numNodesInTree=0;
		@Expose public int numEndNodesInTree=0;
		@Expose public long timeRunning=0;
		@Expose public long timeRunningWithLastTestRun;


		// note: below not each run set!!
		@Expose public int numCeEquivOracle;
		@Expose public int sizeCeEquivOracle;
		@Expose public int numCeAfterLoopReduction;
		@Expose public int sizeCeAfterLoopReduction;
		@Expose public int numCeAfterCeAnalysis;
		@Expose public int sizeCeAfterCeAnalysis;


		@Expose public int numOfReusedCounterExamples=0;



		String toJson() {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
			String json = gson.toJson(this);
			return json;
		}

	}

	// TODO: put this code in constructor total class
	//         => as argument you must give
	//              startTime, endTime
	//               terminationReason, learningSuccessfull
	//              and runs
	public Total getTotal() {
		Total total=new Total();
		total.startTime=startTime;
		total.endTime=endTime;
		total.realTime= endTime-startTime;
		total.runs=runs;
		total.numRuns=runs.size();
		for ( int index=0 ; index < (runs.size()-1) ; index++) {
			SingleRun run=runs.get(index);

			long runMemTime = run.stopLearningTime - run.startLearningTime;
			long runTestTime = run.stopTestingTime - run.startTestingTime;
			long runAnalysisTime = run.stopAnalysisTime - run.startAnalysisTime;
			total.timeLearning = total.timeLearning + runMemTime;
			total.timeTesting = total.timeTesting + runTestTime;
			total.timeAnalysis = total.timeAnalysis + runAnalysisTime;

			total.numMemQueriesLearning = total.numMemQueriesLearning + run.numMemQueriesLearning;
			total.numMemQueriesCeAnalysis = total.numMemQueriesCeAnalysis + run.numMemQueriesCeAnalysis;
			total.numEquivQueries = total.numEquivQueries + run.numEquivQueries;

			total.numMemInputsLearning = total.numMemInputsLearning + run.numMemInputsLearning;
			total.numMemInputsCeAnalysis = total.numMemInputsCeAnalysis + run.numMemInputsCeAnalysis;
			total.numEquivInputs = total.numEquivInputs + run.numEquivInputs;

			total.numLearnResets = total.numLearnResets +                 run.numLearnResets;
			total.numLearnInputs =           total.numLearnInputs +       run.numLearnInputs;
			total.numTestResets =            total.numTestResets +        run.numTestResets;
			total.numTestInputs =            total.numTestInputs +        run.numTestInputs;
			total.numCeAnalysisResets =      total.numCeAnalysisResets +  run.numCeAnalysisResets;
			total.numCeAnalysisInputs  =     total.numCeAnalysisInputs +  run.numCeAnalysisInputs;

			total.refinementCounter = total.refinementCounter + run.refinementCounter;
			total.numStoreUpdatesOnCe = total.numStoreUpdatesOnCe + run.numStoreUpdatesOnCE;
			total.numStoreUpdatesOnInconsistency = total.numStoreUpdatesOnInconsistency + run.numStoreUpdatesOnInconsistency;
			total.numCounterexamplesForLearner = total.numCounterexamplesForLearner + run.numCounterexamplesForLearner;
			total.finalNumStates=run.numStatesHypL;

			// note: in last run no ce analysis is done! Thus below numbers are final!
			if ( run.sizeCeEquivOracle != 0 ) {
			   total.sizeCeEquivOracle=total.sizeCeEquivOracle+run.sizeCeEquivOracle;
			   total.numCeEquivOracle=total.numCeEquivOracle+1;
			}
			if ( run.sizeCeAfterLoopReduction != 0 ) {
				   total.sizeCeAfterLoopReduction=total.sizeCeAfterLoopReduction+run.sizeCeAfterLoopReduction;
				   total.numCeAfterLoopReduction=total.numCeAfterLoopReduction+1;
			}
			if ( run.sizeCeAfterCeAnalysis != 0 ) {
				   total.sizeCeAfterCeAnalysis=total.sizeCeAfterCeAnalysis+run.sizeCeAfterCeAnalysis;
				   total.numCeAfterCeAnalysis=total.numCeAfterCeAnalysis+1;
			}

			total.numOfReusedCounterExamples=total.numOfReusedCounterExamples+run.numOfReusedCounterExamples;


		}
		SingleRun lastRun=runs.getLast();

		// times
		//-------
		long lastRunMemTime = lastRun.stopLearningTime - lastRun.startLearningTime;
		total.timeLearning = total.timeLearning + lastRunMemTime;
		long lastRunTestTime = lastRun.stopTestingTime - lastRun.startTestingTime;
		total.timeTestingWithLastRun=total.timeTesting + lastRunTestTime;

		// note: in last run no ce analysis is done!
		//long lastRunAnalysisTime = lastRun.stopAnalysisTime - lastRun.startAnalysisTime;
		//total.timeAnalysis = total.timeAnalysis + lastRunAnalysisTime;

		total.timeRunning=total.timeLearning+total.timeTesting+total.timeAnalysis;
		total.timeRunningWithLastTestRun=total.timeLearning+total.timeTestingWithLastRun+total.timeAnalysis;

		// abstract counts
		//-----------------
		total.numMemQueriesLearning = total.numMemQueriesLearning + lastRun.numMemQueriesLearning;
		total.numEquivQueriesWithLastRun=total.numEquivQueries + lastRun.numEquivQueries;
		total.numMemInputsLearning = total.numMemInputsLearning + lastRun.numMemInputsLearning;
		total.numEquivInputsWithLastRun=total.numEquivInputs + lastRun.numEquivInputs;
		// note: in last run no ce analysis is done!
		//total.numMemInputsCeAnalysis = total.numMemInputsCeAnalysis + lastRun.numMemInputsCeAnalysis;
		//total.numMemQueriesCeAnalysis = total.numMemQueriesCeAnalysis + lastRun.numMemQueriesCeAnalysis;

		// sut counts
		//-----------------
		total.numLearnResets = total.numLearnResets +                 lastRun.numLearnResets;
		total.numLearnInputs = total.numLearnInputs +                 lastRun.numLearnInputs;
		total.numTestResetsWithLastRun = total.numTestResets +        lastRun.numTestResets;
		total.numTestInputsWithLastRun = total.numTestInputs +        lastRun.numTestInputs;


		// process counts
		//-----------------
		total.refinementCounter = total.refinementCounter + lastRun.refinementCounter;
		total.numStoreUpdatesOnCe = total.numStoreUpdatesOnCe + lastRun.numStoreUpdatesOnCE;
		total.numStoreUpdatesOnInconsistency = total.numStoreUpdatesOnInconsistency + lastRun.numStoreUpdatesOnInconsistency;
		total.numCounterexamplesForLearner = total.numCounterexamplesForLearner + lastRun.numCounterexamplesForLearner;
		total.finalNumStates=lastRun.numStatesHypL;




		total.numNodesInTree=lastRun.numNodesInTree;
		total.numEndNodesInTree=lastRun.numEndNodesInTree;


		total.processLearningResultTime=processLearningResultTime;
		total.verificationResultTime=verificationTime;

		total.terminationReason=terminationReason;
		total.learningSuccessfull=learningSuccessful;
		return total;
	}



	public void startNewRun(boolean learnFromScratch) {


		endRun();

		currentRunNumber=currentRunNumber+1;

		currentRun = new SingleRun();
		currentRun.runNumber=currentRunNumber;
		currentRun.hypLIndex=hypLIndex;
		currentRun.hypMIndex=hypMIndex;
		currentRun.numStatesHypL=numStatesHypL;
		currentRun.numStatesHypM=numStatesHypM;
		runs.add(currentRun);

		logger.info(  runHeader(currentRunNumber));

		currentRun.learnFromScratch=learnFromScratch;
	}

	public String runHeader(int run) {
		String line = "===================================================================================\n";
		String indent = "                         Run: ";
		return "\n\n" + line + indent + run + "\n" + line;
	}



	public String phaseHeader(Phase phase, int runNumber) {
		String line = "-----------------------------------------------------------------------------------\n";
		String indent = "                         start ";
		return "\n\n" + line + indent + phase + " (in run "+ runNumber + ")" + "\n" + line;
	}


	public int getCurrentRunNumber() {
		//return  runs.indexOf(currentRun)+1;
		return currentRunNumber;
	}

	private void setCurrentPhase(Phase phase) {
		currentRun.phase= phase;
		logger.info( phaseHeader(phase,currentRun.runNumber) );
	}

	public Phase getCurrentPhase() {
		return currentRun.phase;
	}


	/*
	public void setDescriptionRun(String descriptionRun) {
		currentRun.descriptionRun =  "during phase '" + currentRun.phase  + "': " + descriptionRun;
	}
	*/
	public void addDescriptionTerminationRun(String descriptionTerminationRun) {

		currentRun.descriptionTerminationRun =  currentRun.descriptionTerminationRun + descriptionTerminationRun;

//		Gson gson = new Gson();
//		String json = gson.toJson(currentRun.descriptionRun);
//		logger.fatal("jsonString: " + json);

	}


// ------------------------------
// end Run
// ------------------------------

	private void endRun() {
		if (currentRunNumber==0) return; // no run defined yet

		// stop any possible still running phase
		stopAnyPhase();

		StringWriter strWriter=new StringWriter() ;
		printCeAnalysisOfRun(strWriter,currentRun);
		printStatisticsRun( strWriter, currentRun,true);
		String statsRun=strWriter.toString();

		logStream.print(statsRun);
		logger.info( "\n\n\n" + statsRun + "\n\n\n"  );
		logStream.flush();
	}

// ------------------------------
// finished : end of all runs
// ------------------------------


	public void finished() {
		endRun();
		endTime=System.currentTimeMillis();
		printStatisticsSummary( logStream,true);
		printCounterExamples(logStream);
		logStream.flush();
	}



// ------------------------------
// phases of learning within run
// ------------------------------

	public void startLearning() {
		checkForAbort();
		memQueryIndex=0;
		currentRun.startLearningTime = System.currentTimeMillis();
		setCurrentPhase(Phase.learning);
	}


	public void stopLearning() {
		currentRun.stopLearningTime = System.currentTimeMillis();
		setCurrentPhase(Phase.afterLearning);
	}

	public void startTesting() {
		checkForAbort();
		equivQueryIndex=0;
		currentRun.startTestingTime = System.currentTimeMillis();
		setCurrentPhase(Phase.testing);
	}

	public void stopTesting() {
		currentRun.stopTestingTime = System.currentTimeMillis();
		setCurrentPhase(Phase.afterTesting);
	}

	public void startAnalysisCounterexample() {
		checkForAbort();
		currentRun.startAnalysisTime = System.currentTimeMillis();
		setCurrentPhase(Phase.analyzeCounterexample);
	}

	public void stopAnalysisCounterexample() {
		currentRun.stopAnalysisTime = System.currentTimeMillis();
		setCurrentPhase(Phase.afterAnalyzeCounterexample);
	}

	public void startAddCounterexample() {
		checkForAbort();
		setCurrentPhase(Phase.addCounterexample);
	}

	public void stopAddCounterexample() {
		setCurrentPhase(Phase.afterAddCounterexample);
	}

	public void stopAnyPhase() {
		if ( currentRun.phase.equals(Phase.testing) ) stopTesting();
		if ( currentRun.phase.equals(Phase.learning) ) stopLearning();
		if ( currentRun.phase.equals(Phase.analyzeCounterexample) ) stopAnalysisCounterexample();
		if ( currentRun.phase.equals(Phase.addCounterexample) ) stopAddCounterexample();
	}

// ------------------------------
// timed jobs within run
// ------------------------------

	public void startVerification() {
		checkForAbort();
		verificationTime_start =  System.currentTimeMillis();
	}

	public void stopVerification() {
		verificationTime_stop = System.currentTimeMillis();
		verificationTime = verificationTime + ( verificationTime_stop - verificationTime_start );
	}

	public void startProcessLearningResult() {
		checkForAbort();
		processLearningResultTime_start =  System.currentTimeMillis();
	}

	public void stopProcessLearningResult() {
		processLearningResultTime_stop = System.currentTimeMillis();
		processLearningResultTime = processLearningResultTime + ( processLearningResultTime_stop - processLearningResultTime_start );
	}



//---------------------------------------------------------------------------
//  store global info
//---------------------------------------------------------------------------


	public void incNumberOfReusedCounterExamples() {
		currentRun.numOfReusedCounterExamples =	currentRun.numOfReusedCounterExamples +1;

	}


//---------------------------------------------------------------------------
//  store info per run
//---------------------------------------------------------------------------

   //  store info of counter example analysis
   //---------------------------------------------------------------------------

    public static String toStringOrPreserveNull(Object obj) {
        return obj != null ? obj.toString() : null;
    }


	public Trace getLastCe() {
		return this.lastCe;
	}

	public void setLastCe(Trace lastCe) {
		this.lastCe = lastCe;
	}

	public void storeCE(Trace ceTraceOnSut,Trace ceTraceOnHyp) {
		currentRun.ceAfterLoopReduction_TraceOnSut = toStringOrPreserveNull(ceTraceOnSut);
		currentRun.ceAfterLoopReduction_TraceOnHyp = toStringOrPreserveNull(ceTraceOnHyp);
	}

	public void storeAbstractCE(Word abstractInputs, Word abstractOutputsSut , Word abstractOutputsHyp) {
		currentRun.abstractCeInputs = toStringOrPreserveNull(abstractInputs);
		currentRun.abstractCeOutputsSut = toStringOrPreserveNull(abstractOutputsSut);
		currentRun.abstractCeOutputsHyp = toStringOrPreserveNull(abstractOutputsHyp);
	}



	public void storeCeEquivOracle(Trace ce) {
		currentRun.ceEquivOracle=ce;
		currentRun.sizeCeEquivOracle=ce.size();
		setLastCe(ce);
	}

	public void storeCeAfterLoopReduction(Trace ce) {
		currentRun.ceAfterLoopReduction=ce;
		currentRun.sizeCeAfterLoopReduction=ce.size();
		setLastCe(ce);
	}

	public void storeCeAfterCeAnalysis(Trace ce) {
		currentRun.ceAfterCeAnalysis=ce;
		currentRun.sizeCeAfterCeAnalysis=ce.size();
		setLastCe(ce);
	}



	public void storeSimplifiedCEwithFoundRelation(Trace ceSimplifiedTraceOnSut, Trace ceSimplifiedTraceOnHyp) {

		currentRun.ceAfterCeAnalysis_TraceOnSut = toStringOrPreserveNull(ceSimplifiedTraceOnSut);


		currentRun.ceAfterCeAnalysis_TraceOnSutWithStatevars = ceSimplifiedTraceOnSut.toStringWithStatevars();

		currentRun.ceAfterCeAnalysis_TraceOnHyp = toStringOrPreserveNull(ceSimplifiedTraceOnHyp);

		currentRun.ceAfterCeAnalysis_TraceOnHypWithStatevars = ceSimplifiedTraceOnHyp.toStringWithStatevars();

	}


	public void storeLookaheadUpdateOnCE(String lookaheadStoreUpdate) {
		currentRun.lookaheadUpdateOnCE = toStringOrPreserveNull(lookaheadStoreUpdate);
	}
	public void storeLookaheadUpdateOnInconsistency(String lookaheadStoreUpdate) {
		currentRun.lookaheadUpdateOnInconsistency = toStringOrPreserveNull(lookaheadStoreUpdate);
	}


	public void storeAbstraction(String abstraction) {
		 currentRun.abstraction = toStringOrPreserveNull(abstraction);
	}

	public void storeNumEndNodesInTree(int numEndNodesInTree) {
		currentRun.numEndNodesInTree = numEndNodesInTree;
	}
	public void storeNumNodesInTree(int numNodesInTree) {
		currentRun.numNodesInTree = numNodesInTree;
	}

	public void storeNumStatesHypL(long numberStatesInHypothesis) {
		currentRun.numStatesHypL = numberStatesInHypothesis;
		numStatesHypL = numberStatesInHypothesis;
	}

	public int getHypMIndex() {
		return hypMIndex;
	}

	public void setHypMIndex(int hypMIndex) {
		this.hypMIndex = hypMIndex;
		currentRun.hypMIndex = hypMIndex;
	}

	public void storeNumStatesHypM(long numberStatesInHypothesis) {
		currentRun.numStatesHypM = numberStatesInHypothesis;
		numStatesHypM = numberStatesInHypothesis;
	}

//---------------------------------------------------------------------------
//  counters per run
//---------------------------------------------------------------------------

	public void incHypLIndex() {
		hypLIndex=hypLIndex+1;
		currentRun.hypLIndex = hypLIndex;
	}

	//TODO: keep separate index for hypL!!
	public int getHypLIndex() {
		//return  getCurrentRunIndex()+1;
		return hypLIndex;
	}

	public void incRefinementCounter() {
		currentRun.refinementCounter = currentRun.refinementCounter + 1;
	}

	public void incNumLookaheadUpdatesOnCE() {
		currentRun.numStoreUpdatesOnCE = currentRun.numStoreUpdatesOnCE + 1;
	}

	public void incNumLookaheadUpdatesOnInconsistency() {
		currentRun.numStoreUpdatesOnInconsistency = currentRun.numStoreUpdatesOnInconsistency + 1;
	}

	public void incNumCounterexamplesForLearner() {
		currentRun.numCounterexamplesForLearner = currentRun.numCounterexamplesForLearner + 1;
	}


	public void incMemQueries() {
		if(currentRun.startTestingTime>0){
			currentRun.numMemQueriesCeAnalysis = currentRun.numMemQueriesCeAnalysis + 1;
		}
		else {
			currentRun.numMemQueriesLearning = currentRun.numMemQueriesLearning + 1;

            // only poll time during real member inputs , because we only have few member inputs  during testing
			checkTimePassed_MemQueries();
		}
	}

	public void incMemInputs() {
		if(currentRun.startTestingTime>0){
			currentRun.numMemInputsCeAnalysis = currentRun.numMemInputsCeAnalysis + 1;
		}
		else {
			currentRun.numMemInputsLearning = currentRun.numMemInputsLearning + 1;
		}
	}

	public void decMemQueries() {
		if(currentRun.startTestingTime>0){
			currentRun.numMemQueriesCeAnalysis = currentRun.numMemQueriesCeAnalysis - 1;
		}
		else {
			currentRun.numMemQueriesLearning = currentRun.numMemQueriesLearning - 1;
		}
	}

	public long getNumMemQueriesLearning() {
		return currentRun.numMemQueriesLearning;
	}
	public long getNumMemInputsLearning() {
		return currentRun.numMemInputsLearning;
	}

	public long getNumMemQueriesCeAnalysis() {
		return currentRun.numMemQueriesCeAnalysis;
	}
	public long getNumMemInputsCeAnalysis() {
		return currentRun.numMemInputsCeAnalysis;
	}



	public void incEquivQueries() {
		currentRun.numEquivQueries = currentRun.numEquivQueries + 1;
		checkTimePassed_EquivQueries();
	}
	public void incEquivInputs() {
		currentRun.numEquivInputs = currentRun.numEquivInputs + 1;
	}

	public long getNumEquivQueries() {
		return currentRun.numEquivQueries;
	}
	public long getNumEquivInputs() {
		return currentRun.numEquivInputs;
	}


	public void incSutReset() {
		switch ( currentRun.phase ){
			case startOfRun:
				currentRun.numLearnResets++;
				break;
			case learning:
				currentRun.numLearnResets++;
				break;
			case testing:
				currentRun.numTestResets++;
				break;
			case analyzeCounterexample:
				currentRun.numCeAnalysisResets++;
				break;
			case addCounterexample:
				currentRun.numCeAnalysisResets++;
				break;
			default:
				logger.fatal("Verification test");
				//throw new BugException("Statistically Uncovered sut reset in phase: " + currentRun.phase );
		}
	}

	public void incSutInput() {
		//logger.fatal(currentRun.phase);
		//System.exit(1);
		switch ( currentRun.phase ){
		case startOfRun:
			currentRun.numLearnInputs++;
			break;
		case learning:
			currentRun.numLearnInputs++;
			break;
		case testing:
			currentRun.numTestInputs++;
			break;
		case analyzeCounterexample:
			currentRun.numCeAnalysisInputs++;
			break;
		case addCounterexample:
			currentRun.numCeAnalysisInputs++;
			break;
		default:
			// TODO	 verification test (make another phase in learning)
			//throw new BugException("Statistically Uncovered sut input in phase: " + currentRun.phase);
	}	}




//---------------------------------------------------------------------------
//        timed abort helpers
//---------------------------------------------------------------------------

	/* set maxtime in seconds */
	public void setMaxTime(long maxTime) {
		this.maxTime=maxTime*1000;
	}





	private boolean checkTimePassed() {
		//logger.fatal( "check Time Passed" );
		long currentTime=System.currentTimeMillis();

// TODO: fix params first before enabling next line
		logger.fatal( "Check MaxTime(" + this.maxTime/1000 + "s) Passed; Runtime:  " +  (currentTime - startTime -  verificationTime)/1000 + " seconds, number member/test queries  in current run : " + currentRun.numMemQueriesLearning + "/" + currentRun.numEquivQueries );

		if ( maxTime > 0  &&  currentTime >  startTime+maxTime+verificationTime ) {
			return true;
		}
		return false;
	}

	private void checkForAbort() {
		  if ( checkTimePassed() ) {
			  stopLearning();
			  throw new OutOfTimeException("total learning time passed" );
		  }
	}

	private void checkTimePassed_EquivQueries(){
		equivQueryIndex=equivQueryIndex+1;
		if ( equivQueryIndex > equivCheckNumber ) {
			  if ( checkTimePassed() ) {
				  stopTesting();
				  throw new OutOfTimeException("total learning time passed when executing a equivQuery" );
			  }
              equivQueryIndex=0;
		}
	}

	private void checkTimePassed_MemQueries(){
		memQueryIndex=memQueryIndex+1;
		if ( memQueryIndex > memQueryCheckNumber ) {
			  if ( checkTimePassed() ) {
				  stopLearning();
				  throw new OutOfTimeException("total learning time passed when executing a memQuery" );
			  }
              memQueryIndex=0;
		}
	}











//---------------------------------------------------------------------------
// text output
//---------------------------------------------------------------------------

    public void printStatisticsCeAnalysis( PrintStream stream) {
        printCounterExamples(stream);
        printStatisticsRunsCeAnalysis(stream);
        stream.flush();
    }


	public void printStatistics( PrintStream stream, boolean withTiming) {
		logger.info("total time running verification: " + verificationTime/1000);
		printStatisticsHeader(stream);
		printStatisticsRuns(stream,withTiming);
		printStatisticsSummary(stream,withTiming);

		stream.flush();

		/*
		StringWriter strWriter=new StringWriter() ;
		dumpStatistics(strWriter);
		stream.print(strWriter.toString());
        */
	}

	private void  printStatisticsHeader( PrintStream stream ) {
		stream.println("-------------------------------------------------------------------------------------");
	  	stream.println("                           RUNS");
        stream.println("-------------------------------------------------------------------------------------");
	}

	private void printStatisticsRuns( PrintStream stream, boolean withTiming) {
		StringWriter strWriter=new StringWriter() ;

		for ( int i=0 ; i < runs.size() ; i++ ) {
			SingleRun run=runs.get(i);
			printStatisticsRun( strWriter, run,true);
		}
		String statsRun=strWriter.toString();
		stream.print(statsRun);
		stream.flush();
	}

	private void printStatisticsRunsCeAnalysis( PrintStream stream) {
		StringWriter strWriter=new StringWriter() ;

		for ( int i=0 ; i < runs.size() ; i++ ) {
			SingleRun run=runs.get(i);
			printCeAnalysisOfRun(strWriter,run);
		}
		String statsRun=strWriter.toString();
		stream.print(statsRun);
		stream.flush();
	}

	private void printStatisticsRun( Writer writer , SingleRun run, boolean withTiming) {
		long runMemTime = run.stopLearningTime - run.startLearningTime;
		long runTestTime = run.stopTestingTime - run.startTestingTime;
		long runAnalysisTime = run.stopAnalysisTime - run.startAnalysisTime;

		PrintWriter stream = new PrintWriter(writer);
        stream.println("-------------------------------------------------------------------------------------");
        stream.println("run " +  run.runNumber + " STATISTICS:");
        stream.println("-------------------------------------------------------------------------------------");
		stream.println("Run number                                     : " + run.runNumber );
		stream.println("Start learning from scratch                    : " + run.learnFromScratch );
		stream.println("HypL index/#states after run                   : " + run.hypLIndex + "/" + run.numStatesHypL  );
		//stream.println("HypM index/#states after run                   : " + run.hypMIndex + "/" + run.numStatesHypM  );
		//stream.println("HypL/HypM index after run                      : " + run.hypLIndex + "/" + run.hypMIndex  );
		stream.println("Membership queries                             : " + run.numMemQueriesLearning);
		stream.println("Membership inputs                              : " + run.numMemInputsLearning);
		if (withTiming) stream.println("Running time of membership                     : " + Time.formatTime(runMemTime) );
		stream.println("Testing equivalence queries                    : " + run.numEquivQueries);
		stream.println("Testing equivalence inputs                     : " + run.numEquivInputs);
		if (withTiming) stream.println("Running time of testing                        : " + Time.formatTime(runTestTime) );
		stream.println("Ce analysis membership queries                 : " + run.numMemQueriesCeAnalysis);
		stream.println("Ce analysis membership inputs                  : " + run.numMemInputsCeAnalysis);
		if (withTiming) stream.println("Running time of counterexample analysis        : " + Time.formatTime(runAnalysisTime) );
		stream.println("");
		stream.println("Learn resets                                   : " + run.numLearnResets);
		stream.println("Learn inputs                                   : " + run.numLearnInputs);
		stream.println("Testing resets                                 : " + run.numTestResets);
		stream.println("Testing inputs                                 : " + run.numTestInputs);
		stream.println("Ce Analysis resets                             : " + run.numCeAnalysisResets);
		stream.println("Ce Analysis inputs                             : " + run.numCeAnalysisInputs);
		stream.println("");
		stream.println("Number nodes in observation tree               : " + run.numNodesInTree);
		stream.println("Number end nodes in observation tree           : " + run.numEndNodesInTree);

		stream.println("");

		stream.println("num Ce inputs EquivOracle                      : " + run.sizeCeEquivOracle);
		stream.println("num Ce inputs AfterLoopReduction               : " + run.sizeCeAfterLoopReduction);
		stream.println("num Ce inputs AfterCeAnalysis                  : " + run.sizeCeAfterCeAnalysis);
		stream.println("number Of Reused CounterExamples               : " + run.numOfReusedCounterExamples);
		stream.println("");

		stream.println("Abstraction refinement done                    : " + run.refinementCounter);
		stream.println("Store updates on CE                            : " + run.numStoreUpdatesOnCE);
		stream.println("Store updates on inconsistency                 : " + run.numStoreUpdatesOnInconsistency);
		stream.println("Counterexample sent to learner                 : " + run.numCounterexamplesForLearner);
		//stream.println("States in hypothesis                           : " + run.numStates );

		stream.println("Description of run                             : " + run.descriptionTerminationRun );
		stream.println("");
		stream.flush();


		if ( stream.checkError() )  logger.error("problem writing statistics to File");

		stream.close();


	}


    private void printCeAnalysisOfRun( Writer writer , SingleRun run) {



        PrintWriter stream = new PrintWriter(writer);



        stream.print("\n\n");
        stream.println("-------------------------------------------------------");
        stream.print("run " +  run.runNumber + " CE ANALYSIS ");


        ArrayList<String> results= new ArrayList<>();

        boolean found_solution = false;
        if ( run.ceAfterLoopReduction_TraceOnSut != null ) {
            results.add("counter example found");
        }
        if ( run.lookaheadUpdateOnInconsistency !=null ) {
            results.add("by tree inconsistency a new output lookahead trace found");
            found_solution = true;
        }
        if ( run.abstractCeInputs != null ) {
            results.add("abstract counter example found");
            found_solution = true;
        }
        if ( run.lookaheadUpdateOnCE != null ) {
            results.add("from ce a new guarded lookahead trace found");
            found_solution = true;
        }
        if ( run.abstraction != null ) {
            results.add("from ce a new input abstraction found");
            found_solution = true;
        }
        if ( !found_solution ) {
            if ( run.ceAfterLoopReduction_TraceOnSut == null ) {
                stream.println("RESULT: finished  learning because no ce found "  );
            } else {
                stream.println("RESULT: PROBLEM: ce found, but  unknown what happened?? "  );
            }
        } else {
            stream.println("RESULT: " + results );
        }




        stream.println("-------------------------------------------------------");

        stream.print("\n\n");

        found_solution = false;
        if ( run.ceAfterLoopReduction_TraceOnSut != null ) {
            stream.println("counter example found:");
            stream.println("   on sut: " + run.ceAfterLoopReduction_TraceOnSut.toString() );
            stream.println("   on hyp: " + run.ceAfterLoopReduction_TraceOnHyp.toString() );
            stream.println();
        }
        if ( run.lookaheadUpdateOnInconsistency !=null ) {
            stream.println("RESULT: by tree inconsistency a new output lookahead trace found : ");
            stream.println("    " + run.lookaheadUpdateOnInconsistency.toString());
            found_solution = true;
        }
        if ( run.abstractCeInputs != null ) {
            stream.println("RESULT: abstract counter example found:");
            stream.println("    inputs: " + run.abstractCeInputs.toString());
            stream.println("    outputs sut: " + run.abstractCeOutputsSut.toString() );
            stream.println("    outputs hyp: " + run.abstractCeOutputsHyp.toString() );
            found_solution = true;
        }
        if ( run.lookaheadUpdateOnCE != null ) {
            stream.println("RESULT: new lookahead found from CE: " + run.lookaheadUpdateOnCE.toString());
            found_solution = true;
        }
        if ( run.abstraction != null ) {
            stream.println("RESULT: new input abstraction found from CE: " +run.abstraction.toString());
            found_solution = true;
        }
        if ( !found_solution ) {
            if ( run.ceAfterLoopReduction_TraceOnSut == null ) {
                stream.println("RESULT: finished  learning because no ce found "  );
            } else {
                stream.println("PROBLEM: ce found, but  unknown what happened?? "  );
            }
        }


/*
        if ( run.ceAfterLoopReduction_TraceOnSut == null ) {
            stream.println(" no counter example found ");
            if ( run.lookaheadUpdateOnInconsistency !=null ) {
                stream.println("tree inconsistency detected");
                stream.println("lookahead update on inconsistency: " + run.lookaheadUpdateOnInconsistency.toString());
            } else {
                stream.println(" UNKNOWN what happened ?? ");
            }

        } else {

            stream.println("counter example found:");
            stream.println("   on sut: " + run.ceAfterLoopReduction_TraceOnSut.toString() );
            stream.println("   on hyp: " + run.ceAfterLoopReduction_TraceOnHyp.toString() );

            stream.print("\n\n");

            if ( run.lookaheadUpdateOnInconsistency !=null ) {          // ce && inconsistency
                stream.println("tree inconsistency detected");
                stream.println("lookahead update on inconsistency: " + run.lookaheadUpdateOnInconsistency.toString());
            } else {

                if ( run.abstractCeInputs != null ) {
                    stream.println("RESULT: abstract counter example found:");
                    stream.println("   inputs: " + run.abstractCeInputs.toString());
                    stream.println("   outputs sut: " + run.abstractCeOutputsSut.toString() );
                    stream.println("   outputs hyp: " + run.abstractCeOutputsHyp.toString() );
                } else if ( run.ceAfterCeAnalysis_TraceOnSut != null)  {
                    if ( run.lookaheadUpdateOnCE != null ) {
                        stream.println("RESULT: lookahead update on CE: " + run.lookaheadUpdateOnCE.toString());
                    }
                    if ( run.abstraction != null ) {
                        stream.println("RESULT: new input abstraction found: " +run.abstraction.toString());
                    }
                    if ( run.lookaheadUpdateOnCE == null && run.abstraction == null ) {
                        stream.println("PROBLEM: nothing found to solve ce! ") ;
                    }
                    stream.print("\n\n");
                    stream.println("simplified counter example found with new abstraction relation found:");
                    stream.println("   on sut: " + run.ceAfterCeAnalysis_TraceOnSut.toString() );
                    stream.println("   on hyp: " + run.ceAfterCeAnalysis_TraceOnHyp.toString() );
                    stream.print("\n\n");
                    stream.println("full sut trace:");
                    stream.println("   " + run.ceAfterCeAnalysis_TraceOnSutWithStatevars.toString() );
                    stream.print("\n\n");
                    stream.println("full hyp trace:");
                    stream.println("   " + run.ceAfterCeAnalysis_TraceOnHypWithStatevars.toString() );
                    stream.print("\n\n");


                } else {
                    stream.println("  UNKNOWN what happened with CE?? ");
                }
            }
        }
    */

        stream.print("\n\n");
        stream.flush();
    }


    private void printCounterExamples( PrintStream stream) {

        stream.println("-------------------------------------------------------------------------------------");
        stream.println("                          COUNTER EXAMPLES");
        stream.println("-------------------------------------------------------------------------------------");
        stream.print("\n\n");
        stream.println("number of runs: " + runs.size() + "\n");
        for ( int index=0 ; index < (runs.size()-1) ; index++) {
            SingleRun run=runs.get(index);

            if (run.ceAfterLoopReduction != null) {
                int runNumber=index+1;
                stream.println("run " + runNumber  + ": " + run.ceAfterLoopReduction);
            }
        }
        stream.print("\n\n");
        stream.flush();
    }



	private void printStatisticsSummary( PrintStream stream, boolean withTiming) {

		Total total=getTotal();
		stream.println("-------------------------------------------------------------------------------------");
		stream.println("                           SUMMARY");
		stream.println("-------------------------------------------------------------------------------------");
		if (withTiming) stream.println("Total running time                             : " + Time.formatTime(total.timeRunning) );
		if (withTiming) stream.println("               `-> with last run               : " + Time.formatTime(total.timeRunningWithLastTestRun) );

		if (withTiming) stream.println("Total running time of Membership               : " + Time.formatTime(total.timeLearning));
		if (withTiming) stream.println("Total running time of Testing                  : " + Time.formatTime(total.timeTesting));
		if (withTiming) stream.println("                          `-> with last run    : " + Time.formatTime(total.timeTestingWithLastRun));
		if (withTiming) stream.println("Total running time of CounterExample analysis  : " + Time.formatTime(total.timeAnalysis));
		if (withTiming) stream.println("");
		if (withTiming) stream.println("Total processLearningResultTime                      : " + Time.formatTime(total.processLearningResultTime));;
		if (withTiming) stream.println("Total verificationTime                               : " + Time.formatTime(total.verificationResultTime));;
		if (withTiming) stream.println("");


		if (withTiming) stream.println("Total realTime                                 : " + Time.formatTime(total.realTime));
		if (withTiming) stream.println("real start time                                : " + total.startTime + " ms since Jan 1, 1970 GMT  = " + Time.millisecond2humanDateString(total.startTime)  );
		if (withTiming) stream.println("real end time                                  : " + total.endTime + " ms since Jan 1, 1970 GMT  = " + Time.millisecond2humanDateString(total.endTime)  );
		if (withTiming) stream.println("");



		stream.println("Total runs (include last test run)             : " + total.numRuns );

		stream.println("Total membership queries                       : " + total.numMemQueriesLearning);
		stream.println("Total membership inputs                        : " + total.numMemInputsLearning);
		stream.println("Total Testing equivalence queries              : " + total.numEquivQueries);
		stream.println("                  `-> with last run            : " + total.numEquivQueriesWithLastRun);
		stream.println("Total Testing equivalence inputs               : " + total.numEquivInputs);
		stream.println("                  `-> with last run            : " + total.numEquivInputsWithLastRun);
		stream.println("Total Ce Analysis membership queries           : " + total.numMemQueriesCeAnalysis);
		stream.println("Total Ce Analysis membership inputs            : " + total.numMemInputsCeAnalysis);

		stream.println("Total abstraction refinements                  : " + total.refinementCounter );
		stream.println("Total number of guard lookahead traces added   : " + total.numStoreUpdatesOnCe );
		stream.println("Total number of output lookahead traces added  : " + total.numStoreUpdatesOnInconsistency );
		stream.println("Total number of counterexamples sent to learner: " + total.numCounterexamplesForLearner );
		stream.println("Total states in learned abstract Mealy machine : " + total.finalNumStates );

		stream.println("");
		stream.println("Learn resets                                   : " + total.numLearnResets);
		stream.println("Learn inputs                                   : " + total.numLearnInputs);
		stream.println("Testing resets                                 : " + total.numTestResets);
		stream.println("Testing inputs                                 : " + total.numTestInputs);
		stream.println("Ce Analysis resets                             : " + total.numCeAnalysisResets);
		stream.println("Ce Analysis inputs                             : " + total.numCeAnalysisInputs);
		stream.println("");

		stream.println("Number nodes in observation tree               : " + total.numNodesInTree);
		stream.println("Number end nodes in observation tree           : " + total.numEndNodesInTree);
		stream.println("");

		stream.println("num Ce traces EquivOracle                      : " + total.numCeEquivOracle);
		stream.println("num Ce inputs EquivOracle                      : " + total.sizeCeEquivOracle);
		stream.println("num Ce traces AfterLoopReduction               : " + total.numCeAfterLoopReduction);
		stream.println("num Ce inputs AfterLoopReduction               : " + total.sizeCeAfterLoopReduction);
		stream.println("num Ce traces CeAnalysis                       : " + total.numCeAfterCeAnalysis);
		stream.println("num Ce inputs AfterCeAnalysis                  : " + total.sizeCeAfterCeAnalysis);
		stream.println("number Of Reused CounterExamples               : " + total.numOfReusedCounterExamples);
		stream.println("");



		stream.println("Termination Reason                             : " + total.terminationReason);
		stream.println("Learning Successfull                           : " + total.learningSuccessfull);
		stream.println("");

		stream.flush();
		if ( stream.checkError() )  logger.error("problem writing statistics to File");
	}


	private void dumpStatistics(Writer outstream) {
		PrintWriter stream=new PrintWriter(outstream);
		Dumper dmpr = new Dumper();
		dmpr.setMaxDepth(2);  // do not follow this in inner class!
		stream.write(dmpr.dump(runs));
		if ( stream.checkError() )  logger.error("problem writing statistics to File");
	}


	/*
	private  long getTotalRefinementCounter() {

		long totalRefinementCounter = 0;

		for ( int index=0 ; index < runs.size(); index++) {
			SingleRun run=runs.get(index);
			totalRefinementCounter = totalRefinementCounter + run.refinementCounter;
		}
		return totalRefinementCounter;
	}
	*/

	//---------------------------------------------------------------------------
	// json output
	//---------------------------------------------------------------------------



		public void printJsonStatistics( PrintStream outstream, boolean withTiming) {

			 // old: use gson pretty printer instead
			 //JsonWriter stream = new JsonWriter(new OutputStreamWriter (outstream));
			 //JsonWriter has a BUG: does also pretty printing on [ { inside string, which it shouldn't!

			Writer stream = new OutputStreamWriter (outstream);

			 Total total=getTotal();
			 String jsonText = total.toJson();
			try {
				stream.write(jsonText);
				stream.flush();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		public void addDescriptionRun(String string) {
			
		}
}
