package abslearning.logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import abslearning.ceanalysis.graph.RelationEdge;
import abslearning.ceanalysis.graph.RelationGraph;
import abslearning.ceanalysis.graph.TraceParameterNode;
import abslearning.tree.ConcreteTree;
import abslearning.tree.TreeNodeData;
import de.ls5.jlearn.interfaces.Symbol;
import generictree.GenericTree;
import generictree.GenericTreeIteratorBreadthFirst;
import generictree.GenericTreeIteratorUpwards;
import generictree.GenericTreeNode;
import graph.BreadthFirstEdgeIterator;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.util.graphs.dot.GraphDOT;
import util.ExceptionAdapter;

public class StructLogger {
	private static final Logger logger = Logger.getLogger(StructLogger.class); 
	
	
	/**
	 * Produces a dot-file and a PDF (if graphviz is installed)
	 * @param fileName filename without extension - will be used for the .dot and .pdf
	 * @param model
	 * @param alphabet
	 * @param verboseError whether to print an error explaing that you need graphviz
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void printMachineAsDotfile(String fileName, MealyMachine<?,Symbol,?,Symbol> model, net.automatalib.words.Alphabet<Symbol> alphabet, boolean verboseError) throws FileNotFoundException, IOException {
		PrintWriter dotWriter = new PrintWriter(fileName + ".dot");
		GraphDOT.write(model, alphabet, dotWriter);
	/*	// for pdf 
		try {
			DOT.runDOT(new File(fileName + ".dot"), "pdf", new File(fileName + ".pdf"));
		} catch (Exception e) {
			if (verboseError) {
				System.err.println("Warning: Install graphviz to convert dot-files to PDF");
				System.err.println(e.getMessage());
			}
		}
	*/
		dotWriter.close();
	}

	static public void printSutConcreteTreeDotFile( GenericTree<TreeNodeData> concreteTree, String dotfile) {
		
		// print tree in dotfile
		try {
			logger.debug("Start creating dot");
			PrintStream outstream = new PrintStream(new FileOutputStream(dotfile,false));			
			outstream.println("digraph G {");
			outstream.println("label=\"\"");		
			
			
			int numNodes=concreteTree.getNumberOfNodes();
			logger.debug("  number nodes : " +  numNodes);
			logger.debug("  number end nodes : " +  concreteTree.getNumberOfEndNodes());
			
			printSutConcreteTree_DotTransitions( concreteTree, outstream);

	        
	        outstream.println("}");
	        outstream.close();
	        logger.debug("Finished creating dot");
		} catch (FileNotFoundException fnfe) {
			logger.debug("FileNotFoundException in printSutConcreteTreeAsDotFile() ...");
			throw new ExceptionAdapter(fnfe);			
		}      
	}
	
	

	static private void printSutConcreteTree_DotTransitions( GenericTree<TreeNodeData> concreteTree, PrintStream outstream) {
	
		concreteTree=concreteTree.getShallowCopy(); // to unaffect root and iterator in original tree
		
		
		concreteTree.setIterator( new GenericTreeIteratorBreadthFirst<TreeNodeData>( concreteTree.getRoot() ) );
	    for(GenericTreeNode<TreeNodeData> node : concreteTree) {
	    	 TreeNodeData data=node.getData(); 
	    	 int id=data.getId();  
	    	 String nodeDesc = data.toNodeString();
	    	
	    	 // // no html:
	    	 // outstream.println("s" + id + " [label=\"" + nodeDesc  + "\"]");
	    	 // // html in table
	    	 // outstream.println("s" + id + " [label=<<table><tr><td>" + nodeDesc  + "</td></tr></table>>]");
	    	 
	    	 boolean edgeNode = data.getVersion() == -1;
	    	 // html without table
	    	 outstream.println("s" + id + " [label=<" + nodeDesc  + ">"+ (edgeNode?" color=\"blue\"":"")+"]");
	    	 
	    	 for(GenericTreeNode<TreeNodeData> child : node.getChildren()){
	
	    		 outstream.println("s" + id + " -> s" + child.getData().getId() + " [label=<" + child.getData().toEdgeString()   + ">]"  ); 		    		 
	    	 }
	    }			
	}
	
	static private void printRelationGraphDotFile( RelationGraph relationGraph, String dotFile) {
		logger.debug("Start creating dot");
		try {
			PrintStream outstream = new PrintStream(new FileOutputStream(dotFile,false));			
			outstream.println("digraph G {");
			outstream.println("label=\"\"");		
			
			List<RelationGraph> connectedSubgraphs = relationGraph.connectedComponents();
			for (RelationGraph connectedSubgraph : connectedSubgraphs) {
				printConnectedComponent(connectedSubgraph,  outstream);
			}
			
			outstream.println("}");
		    outstream.close();
		} catch (FileNotFoundException fnfe) {
			logger.debug("FileNotFoundException in printRelationGraphDot() ...");
			throw new ExceptionAdapter(fnfe);			
		} 
	}
	

	private static void printConnectedComponent(
			RelationGraph connectedSubgraph, PrintStream outstream) {
		StringBuilder stringBuilder = new StringBuilder();
 		BreadthFirstEdgeIterator<TraceParameterNode, RelationEdge<TraceParameterNode>,RelationGraph> edgeIter = 
				new BreadthFirstEdgeIterator<TraceParameterNode, RelationEdge<TraceParameterNode>, RelationGraph>(connectedSubgraph);
		while(edgeIter.hasNext()) {
			RelationEdge<TraceParameterNode> edge = edgeIter.next();	
			stringBuilder.append("  ").append(edge.startNode).append(" -> ").append(edge.endNode).
			append(" [label=\"").append(edge.relation).append("\"];\n");
		}
		outstream.println(stringBuilder.toString());
	}



	static public void printSutConcreteTreeAsDotFilePrintAllInNode( GenericTree<TreeNodeData> concreteTree, String dotfile) {
		
		concreteTree=concreteTree.getShallowCopy(); // to unaffect root and iterator in original tree
		
		
		// print tree in dotfile
		try {
			logger.debug("Start creating dot");
			PrintStream outstream = new PrintStream(new FileOutputStream(dotfile,false));			
			outstream.println("digraph G {");
			outstream.println("label=\"\"");		
			outstream.println("s0 [color=\"red\"]");
			int i = 1;
			
			int numNodes=concreteTree.getNumberOfNodes();
			logger.debug("  number nodes : " +  numNodes);
			logger.debug("  number end nodes : " +  concreteTree.getNumberOfEndNodes());
			while ( i < numNodes ) {
				outstream.println("s"+i);
				i++;
			}
			concreteTree.setIterator( new GenericTreeIteratorBreadthFirst<TreeNodeData>( concreteTree.getRoot() ) );
	        for(GenericTreeNode<TreeNodeData> node : concreteTree) {
	        	 TreeNodeData data=node.getData(); 
	        	 int id=data.getId();  
	        	 String nodeDesc = data.toTreeString();
	        	
		    	 // // no html:
	        	 // outstream.println("s" + id + " [label=\"" + nodeDesc  + "\"]");
		    	 // // html in table
	        	 // outstream.println("s" + id + " [label=<<table><tr><td>" + nodeDesc  + "</td></tr></table>>]");
		    	 
	        	 // html without table
	        	 outstream.println("s" + id + " [label=<" + nodeDesc  + ">]");
	        	 
		    	 for(GenericTreeNode<TreeNodeData> child : node.getChildren()){

		    		 outstream.println("s" + id + " -> s" + child.getData().getId()); 		    		 
		    	 }
		    }			
			//s0 [label="<input>/<output>"];
			//s0 -> s0
			//s0 -> s1
	        
	        outstream.println("}");
	        outstream.close();
	        logger.debug("Finished creating dot");
		} catch (FileNotFoundException fnfe) {
			logger.debug("FileNotFoundException in printConcreteTree() ...");
			throw new ExceptionAdapter(fnfe);			
		}      
	}	
	
	static private void printTraceToCurrentTreeNode_DotTransitions( GenericTree<TreeNodeData> concreteTree, GenericTreeNode<TreeNodeData> startNode, PrintStream outstream) {
	
		concreteTree=concreteTree.getShallowCopy(); // to unaffect root and iterator in original tree

		List<GenericTreeNode<TreeNodeData>> trace= concreteTree.getNodeTrace(startNode);
		ListIterator<GenericTreeNode<TreeNodeData>> iter = trace.listIterator();
		while (iter.hasNext()) {
		       // System.out.println("index: " + iter.nextIndex() + " value: " + iter.next());
			     GenericTreeNode<TreeNodeData> node = iter.next();
			     
			     // print node
	        	 TreeNodeData data=node.getData(); 
	        	 int id=data.getId();  
	        	 String nodeDesc = data.toNodeString();
	        	 outstream.println("s" + id + " [label=<" + nodeDesc  + ">]");
	        	 
	        	 // print transition to next node
	        	 if ( iter.hasNext() )  {
	        		 GenericTreeNode<TreeNodeData>  child=trace.get( iter.nextIndex() ); 
		    		 outstream.println("s" + id + " -> s" + child.getData().getId() + " [label=<" + child.getData().toEdgeString()   + ">]"  ); 		    		 		        		 
	        	 }
	        	 
		}			
	}
	
	static public void printTraceToCurrentTreeNode( GenericTree<TreeNodeData> concreteTree, String dotfile,GenericTreeNode<TreeNodeData> startNode) {
		
		// print tree in dotfile
		try {
			logger.debug("Start creating dot");
			PrintStream outstream = new PrintStream(new FileOutputStream(dotfile,false));			
			outstream.println("digraph G {");
			outstream.println("label=\"\"");		
		//	outstream.println("s0 [color=\"red\"]");
		
	        
			printTraceToCurrentTreeNode_DotTransitions( concreteTree,  startNode,  outstream);
     
	        
	        outstream.println("}");
	        outstream.close();
	        logger.debug("Finished creating dot");
		} catch (FileNotFoundException fnfe) {
			logger.debug("FileNotFoundException in printConcreteTree() ...");
			throw new ExceptionAdapter(fnfe);			
		}      
		
	}	
	
	static public void printTraceToCurrentTreeNodeAndLookaheadTree( GenericTree<TreeNodeData> concreteTree, String dotfile,GenericTreeNode<TreeNodeData> startNode) {
		
		
		concreteTree=concreteTree.getShallowCopy(); // to unaffect root and iterator in original tree
		
		// print tree in dotfile
		try {
			logger.debug("Start creating dot");
			PrintStream outstream = new PrintStream(new FileOutputStream(dotfile,false));			
			outstream.println("digraph G {");
			outstream.println("label=\"\"");		
		//	outstream.println("s0 [color=\"red\"]");
		
	        
			printTraceToCurrentTreeNode_DotTransitions( concreteTree,  startNode,  outstream);

			concreteTree.setRoot(startNode);
			
			printSutConcreteTree_DotTransitions( concreteTree, outstream);
	        
	        outstream.println("}");
	        outstream.close();
	        logger.debug("Finished creating dot");
		} catch (FileNotFoundException fnfe) {
			logger.debug("FileNotFoundException in printConcreteTree() ...");
			throw new ExceptionAdapter(fnfe);			
		}      
		
	}		
		
	static public void printSutConcreteTreeInfo(ConcreteTree sutConcreteTree,String dotfile) {
		printSutConcreteTreeDotFile( sutConcreteTree.getTree(), dotfile );
	}
	
	static public void printSutConcreteTreeInfo(ConcreteTree sutConcreteTree,String dotfile,GenericTreeNode<TreeNodeData> node) {
		
		GenericTree<TreeNodeData> concreteTree=sutConcreteTree.getTree();
		concreteTree=concreteTree.getShallowCopy(); // to unaffect root and iterator in original tree
		concreteTree.setRoot(node);
		printSutConcreteTreeDotFile( concreteTree , dotfile );
	}	
	
	public static void printSutConcreteTreeInfoUpwards(ConcreteTree concreteTree, String treedotfileUp, GenericTreeNode<TreeNodeData> currentNode) {
		logger.fatal("-----------------------------------");
		logger.fatal("printSutConcreteTreeInfoUpwards");
		concreteTree.getTree().setIterator(new GenericTreeIteratorUpwards<TreeNodeData>(currentNode));
		for(GenericTreeNode<TreeNodeData> node : concreteTree.getTree()) {
			logger.fatal("Node: " + node);
		}
		logger.fatal("-----------------------------------");
	}	

	
	@SuppressWarnings("unchecked")
	static public  void outputTreeStatisticsInJson(String jsonFilePath,ConcreteTree  concreteTree)   { 
		PrintStream outstream=null;
		try {
			outstream = new PrintStream(new FileOutputStream(jsonFilePath,false));
		} catch (FileNotFoundException fnfe) {
			System.err.println("FileNotFoundException in outputTreeStatisticsInJson in " +  StructLogger.class.getName()  + "class, file: " + jsonFilePath );
			throw new ExceptionAdapter(fnfe);
		}
		
		//JSONPrettyPrint root = new JSONPrettyPrint();
		JSONObject root = new JSONObject();	
	
		// add number of nodes at each depth	
	 	
       JSONArray depthIndex = new JSONArray();
	   Map <Integer,Integer> mymap= concreteTree.getTree().depthIndex();
	   //java.util.Collections.sort( .toArray(new Integer[0]).
	   for (Integer depth:  new TreeSet <Integer>(mymap.keySet()) )  {
		   //System.err.println(depth + " : " + mymap.get(depth));
		   depthIndex.add(mymap.get(depth));
	   }			
	
	  // this.getConcreteTree().getTree().getNumberOfEndNodes()

	   root.put("depth_index",depthIndex);
	   
	   outstream.print(root);
	   outstream.close();
	}		
}
