package abslearning.logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import abslearning.app.HypStore;
import abslearning.exceptions.BugException;
import abslearning.predicates.InputActionPredicate;
import abslearning.predicates.InputParameterPredicate;
import abslearning.stores.PredicateStore;
import abslearning.trace.AbstractionVariable;
import abslearning.verifier.HypDesc;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.util.DotUtil;
import sut.info.ActionSignature;
import sut.info.SutInfo;
import util.ExceptionAdapter;

public class LearnLogger {
	private static final Logger logger = Logger.getLogger(LearnLogger.class);
	public static String hypDir;
	private static boolean generatePdf = false;

	public static void setGeneratePdf(boolean generatePdf) {
		LearnLogger.generatePdf = generatePdf;
	}

	public static void setHypDir(String hypDir) {
		LearnLogger.hypDir = hypDir;
	}

	public static void logAbstractions(int hypCounter, PredicateStore predicateStore)  {
		String abstractionsTxtFile=HypStore.getAbstractionsTxtFile(hypCounter);
		String abstractionsJsonFile=HypStore.getAbstractionsJsonFile(hypCounter);

		// log abstractions for hyp only once
		if (!checkFileExists(abstractionsJsonFile))  {
			// log abstractions in .txt file

			PrintStream abstractionFileStream = null;
			try {
				abstractionFileStream = new PrintStream(new FileOutputStream(abstractionsTxtFile,false));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new BugException(e.toString());

			}
			StringWriter strWriter = new StringWriter();
			printCurrentAbstraction(predicateStore, strWriter);
			String abst = strWriter.toString();
			abstractionFileStream.print(abst);
			abstractionFileStream.close();



			outputInJson(abstractionsJsonFile,null,predicateStore);
		}
	}

	public static void logAbstractHypothesisDot(Automaton hypothesis, int hypCounter) {
		logHypothesis(hypothesis, hypCounter, ".orig");
	}

	public static void logAbstractReducedHypothesisDot(Automaton hypothesis, int hypCounter) {
		logHypothesis(hypothesis, hypCounter, "");
	}

	public static void logAbstractHypothesisXml(int hypCounter) {
		HypDesc hypDesc= HypDesc.getDescForHyp(hypCounter);
		HypStore.buildConcreteHypXml(hypDesc.abstractionFile, hypDesc.dotModelFile, hypDesc.concreteXmlModelFile);
	}

	private static void logHypothesis(Automaton hypothesis, int hypCounter, String suffix) {
		String dotFile=HypStore.getHypFile(hypCounter,suffix+".abstract.dot");

		// log hypothesis only once
		if (!checkFileExists(dotFile)) {
			util.learnlib.Dot.writeDotFile(hypothesis, dotFile);
			if (generatePdf) {
			   // convert  abstract hypothesis .dot file to  .pdf file
			   String pdfFile=HypStore.getHypFile(hypCounter,suffix+".abstract.pdf");
			   DotUtil.invokeDot(new File(dotFile), "pdf",new File(pdfFile));
			}
		}
	}

	private static boolean checkFileExists(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//for given hypothesis write all info in json output
	//-----------------------------------------------------------------------------------------------------------------------------

	/*  outputInJson - output learning results in a json file
	 *  - http://stackoverflow.com/questions/2646613/how-to-avoid-eclipse-warnings-when-using-legacy-code-without-generics
	 *     -> says  to use @SuppressWarnings("unchecked")
	 *  - http://www.rojotek.com/blog/2009/05/07/a-review-of-5-java-json-libraries/
	 *     -> advices to use JSON.simple
	 */

	@SuppressWarnings("unchecked")
	static private void outputInJson(String jsonFilePath, Automaton automat,PredicateStore predicateStore )   {
		PrintStream outstream=null;
		try {
			outstream = new PrintStream(new FileOutputStream(jsonFilePath,false));
		} catch (FileNotFoundException fnfe) {
			System.err.println("FileNotFoundException in outputInJson in " +  StructLogger.class.getName()  + "class, file: " + jsonFilePath );
			throw new ExceptionAdapter(fnfe);
		}

		//JSONPrettyPrint root = new JSONPrettyPrint();
		JSONObject root = new JSONObject();
		JSONArray inputs = new JSONArray();

		JSONArray concrete_alphabet = new JSONArray();

	    // output input/output interfaces
	    // input :
		for (ActionSignature curSig : SutInfo.getInputSignatures()) {
			JSONObject methodInterface = new JSONObject();
			methodInterface.put("method_name",curSig.getMethodName());

			JSONArray paramtypes = new JSONArray();
			List<String> parameterTypes = curSig.getParameterTypes();
			for (int i = 0; i < parameterTypes.size(); i++) {
				paramtypes.add(parameterTypes.get(i));
			}
			methodInterface.put("param_types",paramtypes);

			concrete_alphabet.add(methodInterface);
		}
	    // output :
		for (ActionSignature curSig : SutInfo.getOutputSignatures()) {
			JSONObject methodInterface = new JSONObject();
			methodInterface.put("method_name",curSig.getMethodName());

			JSONArray paramtypes = new JSONArray();
			List<String> parameterTypes = curSig.getParameterTypes();
			for (int i = 0; i < parameterTypes.size(); i++) {
				paramtypes.add(parameterTypes.get(i));
			}
			methodInterface.put("param_types",paramtypes);

			concrete_alphabet.add(methodInterface);
		}

	    /*  output learned input/output labels
	        in form :

	         inputs: [
	           {
	               method_name : IStart2,
	               occurence:last,
	               param_index:0
	           },
	           ...
	         ],
	         outputs: [
	         ...
	         ]
		*/
	    // inputs :
		String methodName;
		for (ActionSignature sig : SutInfo.getInputSignatures()) {
			JSONObject input = new JSONObject();
			JSONArray params = new JSONArray();
			methodName = sig.getMethodName();

			input.put("name",methodName);
			InputActionPredicate curActionPredicate = predicateStore.getInputActionPredicate(methodName);
			Iterator<InputParameterPredicate> i = curActionPredicate.getAll().iterator();
			while (i.hasNext()) {
				//params.add(i.next());
				InputParameterPredicate p=i.next();
				Iterator<AbstractionVariable> j = p.getAll().iterator();
				JSONArray indexedparams = new JSONArray();
				while (j.hasNext()) {
					AbstractionVariable item=j.next();
					JSONObject aparam = item.getJSON();
					indexedparams.add(aparam);
				}
				params.add(indexedparams);
			}
			input.put("params",params);
			inputs.add(input);

		}

		root.put("concrete_alphabet", concrete_alphabet);
		root.put("inputs", inputs);
		root.put("constants", SutInfo.getConstants() );

		outstream.print(root);
		outstream.flush();
		outstream.close();
	}



	//-----------------------------------------------------------------------------------------------------------------------------
	//  abstraction
//	    printCurrentAbstraction : print to system.out
//	    outputAbstraction :  print to given file
	//-----------------------------------------------------------------------------------------------------------------------------

		static public void printCurrentAbstraction(PredicateStore predicateStore,Writer outstream) {
			PrintWriter stream=new PrintWriter(outstream);

			stream.println("----------------------------------------------");
			stream.println("Concrete alphabet:");

			for (ActionSignature curSig : SutInfo.getInputSignatures()) {
				stream.print(curSig.getMethodName() + "(");
				List<String> parameterTypes = curSig.getParameterTypes();
				for (int i = 0; i < parameterTypes.size(); i++) {
					stream.print(parameterTypes.get(i));
					if (i < parameterTypes.size() - 1) {
						stream.print(", ");
					}
				}
				stream.println(")");
			}

			for (ActionSignature curSig : SutInfo.getOutputSignatures()) {
				stream.print(curSig.getMethodName() + "(");
				List<String> parameterTypes = curSig.getParameterTypes();
				for (int i = 0; i < parameterTypes.size(); i++) {
					stream.print(parameterTypes.get(i));
					if (i < parameterTypes.size() - 1) {
						stream.print(", ");
					}
				}
				stream.println(")");
			}

			stream.println("----------------------------------------------");





			// here





			stream.println("Abstraction:");
			logger.debug("Abstraction:");

			for (ActionSignature sig : SutInfo.getInputSignatures()) {
				String methodName = sig.getMethodName();
				stream.print(methodName + "(");
				InputActionPredicate curActionPredicate = predicateStore.getInputActionPredicate(methodName);
				Iterator<InputParameterPredicate> i = curActionPredicate.getAll().iterator();
				while (i.hasNext()) {
					stream.print(i.next());
					if (i.hasNext()) {
						stream.print(", ");
					}
				}
				stream.println(")");
			}

			// TODO: remove, however first check if someone depends on it
			for (ActionSignature sig : SutInfo.getOutputSignatures()) {
				String methodName = sig.getMethodName();
				stream.print(methodName + "(");
				stream.println(")");
			}

			stream.println("----------------------------------------------");
			stream.println("Abstract alphabet:");
			for (Symbol symbol : predicateStore.getInputAlphabet().getSymbolList()) {
				stream.println(symbol.toString());
			}
			/*
			for (Symbol symbol : predicateStore.getOutputAlphabet().getSymbolList()) {
				stream.println(symbol.toString());
			}
			*/
			stream.println("----------------------------------------------");
			stream.println("Constants:");
			stream.println(SutInfo.getConstants());
			stream.println("----------------------------------------------");
		}

		static public void outputAbstraction(String filePathString,PredicateStore predicateStore )  {
		   outputAbstraction(filePathString, predicateStore, true);
		}

		static public void outputAbstraction(String filePathString,PredicateStore predicateStore, boolean append )  {
			PrintStream outstream = null;
			try {
				outstream = new PrintStream(new FileOutputStream(filePathString,append));
			} catch (FileNotFoundException fnfe) {
				System.err.println("FileNotFoundException in outputAbstraction in " +  StructLogger.class.getName()  + "class, file: " + filePathString );
				throw new ExceptionAdapter(fnfe);
			}

		//	outstream.println("hypothese: " + statistics.);

			for (ActionSignature sig : SutInfo.getInputSignatures()) {
				String methodName = sig.getMethodName();
				outstream.print(methodName + "(");
				InputActionPredicate curActionPredicate = predicateStore.getInputActionPredicate(methodName);
				Iterator<InputParameterPredicate> i = curActionPredicate.getAll().iterator();
				while (i.hasNext()) {
					outstream.print(i.next());
					if (i.hasNext()) {
						outstream.print(", ");
					}
				}
				outstream.println(")");
			}

			//TODO: remove => python scripts should be adapted
			for (ActionSignature sig : SutInfo.getOutputSignatures()) {
				String methodName = sig.getMethodName();
				outstream.print(methodName + "(");
				outstream.println(")");
			}

			outstream.print("CONSTANTS: ");
			outstream.println(SutInfo.getConstants());
			outstream.println("--------------------------------------------------------------------------------");
			outstream.close();
		}

}
