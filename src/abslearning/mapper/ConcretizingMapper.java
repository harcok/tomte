package abslearning.mapper;

import java.util.SortedMap;

import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

/*
 * RA   -> conc input   -> TestingMapper -> abs input  -> state_machine
 * RA 	<- conc output  <- TestingMapper <- abs output <- state_machine
 */
public interface ConcretizingMapper {
	
	/**
	 * an input is abstracted and applied to the internal abstract state machine  
	 */
	public void abstractInput(InputAction concreteInput, SortedMap<Integer, Integer> statevars);
	
	/**
	 * the generated abstract output string is concretized   
	 */
	public OutputAction concretizeOutput(InputAction concreteInput, SortedMap<Integer, Integer> stateVars, String abstractOutputString);
	public void reset();
}
