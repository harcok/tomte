//<<<<<<< HEAD
//package abslearning.mapper;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.LinkedHashSet;
//import java.util.List;
//import java.util.SortedMap;
//import java.util.TreeMap;
//import java.util.TreeSet;
//
//import org.apache.log4j.Logger;
//
//import sut.info.ActionSignature;
//import sut.info.SutInfo;
//
//import abslearning.exceptions.BugException;
//import abslearning.exceptions.Messages;
//import abslearning.trace.InputAction;
//import abslearning.trace.IntegerParamValue;
//import abslearning.trace.OutputAction;
//import abslearning.trace.Parameter;
//import abslearning.trace.Trace;
//
//public class StateVarNMapper implements StateVarMapper{
//	private static final Logger logger = Logger.getLogger(StateVar1Mapper.class);	
//	
//	private HashMap<String,Integer> methodAndParamIndex2stateVarIndex= new HashMap <String,Integer>();
//	
//	protected List<Integer> constants;
//
//	//------------------------------------------------------------------------------------------------------------------	
//
//	
//	public StateVarNMapper(List<Integer> constants) {
//		this.constants=constants;
//	}		
//	
//	//------------------------------------------------------------------------------------------------------------------	
//
//	/*
//	private HashMap <StateVariable,Integer> sv2stateVarIndex = new HashMap <StateVariable,Integer> ();
//	
//	// map
//	//    from: statevar x_i ( actually its index i) 
//	//    to:   StateVarMethodParamIndex  (object containing just methodname and paramindex) 
//	private HashMap <Integer,StateVariable> stateVarIndex2sv = new HashMap <Integer,StateVariable> ();
//   */
//	/**
//	 * Gets the StateVariable for a specified state variable (counter)
//	 * @param stateVarCounter The state variable for which we want to retrieve the corresponding StateVarMethodParamIndex
//	 * 
//	 * StateVariable is object representing statevar 
//	 * 
//	 * @return The StateVariable that belongs to a specified state variable
//	 
//	private StateVariable getStateVariable(int stateVarCounter){
//		return stateVarIndex2sv.get(stateVarCounter);
//	}
//	*/	
//
//
//
//
//	
////------------------------------------------------------------------------------------------------------------------	
//	
//	
//	
////--------------------------------------------------------     
////  update state vars in trace
////--------------------------------------------------------     
//     
//	   
//	   /*
//	    *  get index of statevar into the value input parameter must be stored
//	    *  
//	    *  returns null if no statevar exist which contains that value
//	    */
//	   private Integer xgetStateVarIndexToStoreInputParamInto(Trace trace,InputAction concreteInput, int paramIndex) {
//
//			Integer value = concreteInput.getConcreteParameters().get(paramIndex);   
//			int transitionIndex  = concreteInput.getTransitionIndex();
//			LinkedHashSet<Integer> memV=trace.getMemvAfterTransition(transitionIndex);
//			Integer stateVarIndex=new ArrayList<Integer>(memV).indexOf(value);
//			if ( stateVarIndex.equals( new Integer(-1) ) ) return null; 
//	   		return stateVarIndex;
//   		
//       }
//	   
//	   /*
//	    * trace: sut run on observation tree with added memv
//	    */
//	   public Integer getStateVarIndexToStoreInputParamInto(Trace trace,InputAction concreteInput, int paramIndex) {
//
//			Integer value = concreteInput.getConcreteParameters().get(paramIndex);   
//			int transitionIndex  = concreteInput.getTransitionIndex();
//			LinkedHashSet<Integer> memV=trace.getMemvAfterTransition(transitionIndex-1);
//			Integer stateVarIndex=new ArrayList<Integer>(memV).indexOf(value);
//			if ( stateVarIndex.equals( new Integer(-1) ) ) return null; 
//	   		return stateVarIndex;
//   		
//       }	
//	   
//	   /*
//	    *  get index of statevar into the value input parameter must be stored
//	    *  
//	    *  returns null if no statevar exist which contains that value
//	   
//	   public Integer getStateVarIndexToStoreInputParamInto(Trace trace,InputAction concreteInput, int paramIndex) {
//
//		  
//			SortedMap<Integer,Integer> statevarsBeforeReferringTrans = trace.getStatevarsAfterTransition(concreteInput.getTransitionIndex()-1);
//				
//			Integer paramValue=concreteInput.getConcreteParameters().get(paramIndex);
//			for ( Integer statevarIndex : statevarsBeforeReferringTrans.keySet() ) {
//				Integer value=statevarsBeforeReferringTrans.get(statevarIndex);
//				if ( value.equals(paramValue)) {
//					return statevarIndex;
//				}
//			}
//			return null;
//			
//        }	
//         */
//	   
//    /**
//     * Updates state variables when a new value occurs in memV or a value in memV has been removed
//	 * Approach where we only have one state variable per parameter
//     * @param concreteInput The input that contains the new value we have to store
//     * @param trace The trace for which we have to update the state variables. The trace MUST
//     * contain the concrete input/output of the last transition.  
//     */
//    public void determineStateVarUpdatesFromMemVs(InputAction concreteInput, Trace trace){
//       
//    	
//    	// we want to find statevar updates  (from memorable values before and after transition)
// 		LinkedHashMap<Integer, Integer> statevarUpdates=new LinkedHashMap<Integer, Integer>();
//    	
//    	// get memorable values before and after the transition 
//    	Integer transIndex=trace.getTransitionIndex();
//    	LinkedHashSet<Integer> memVafterTrans=trace.getMemvAfterTransition(transIndex);
//    	LinkedHashSet<Integer> memVbeforeTrans=trace.getMemvAfterTransition(transIndex-1);
//   
// 
//  /*  	
//    //	LinkedHashMap<Integer, Integer> statevarUpdates=new LinkedHashMap<Integer, Integer>();
//    	
//    	// IMPORTANT
//    	// first do remove of memorable of statevar, and then do update of statevar
//    	// because a statevariable can be assigned a new value. If we would first the update and then 
//    	// the unset then statevariable would get unset instead of updated!
//    	
//    	//update state variables when value in memV has been removed
//    	LinkedHashSet<Integer> removedMemVValues = util.JavaUtil.getElementsOnlyInFirstSet(memVbeforeTrans,memVafterTrans);    	
//    	for ( Integer mem : removedMemVValues )	 {
//    		// find out in which statevar this value is stored
//    		// we ask trace because trace stores statevars
//    		Integer stateVarIndex=trace.getStateVariableContainingValue(mem);
//    		// add update to unset state variable
//    		statevarUpdates.put(stateVarIndex, null);
//    		
//    		// unset state variable in trace
//    		//trace.updateStateVariable(stateVarIndex, null);
//		}    	
//    	*/
//    	
//    	ArrayList<Integer>  statevarsBefore= new ArrayList<Integer> (memVbeforeTrans);
//    	int numberStatevarsBefore=statevarsBefore.size();
//    	
//    	ArrayList<Integer>  statevarsAfter= new ArrayList<Integer> (memVafterTrans);    	
//    	int numberStatevarsAfter=statevarsAfter.size();
////    	logger.fatal("statevarsBefore="+ statevarsBefore);
////    	logger.fatal("statevarsAfter="+ statevarsAfter);
//    	for ( int stateVarIndex=0;  stateVarIndex < numberStatevarsAfter; stateVarIndex++ )	 {
//            
//    		Integer newValue = statevarsAfter.get(stateVarIndex);
//    		
//    		if ( stateVarIndex < numberStatevarsBefore ) { 
//    			// update statevar only for a changed value 
//    			Integer oldValue = statevarsBefore.get(stateVarIndex);
//    			if ( !newValue.equals(oldValue)  ) statevarUpdates.put(stateVarIndex, newValue);
//    		} else {
//    			// always update if extending list of statevars with new statevars
//                statevarUpdates.put(stateVarIndex, newValue);
//    		}
//    	}
//    	logger.fatal(statevarUpdates);
//    	
//    	// if we have less statevars we have to unset statevars
//    	if (  numberStatevarsAfter <  numberStatevarsBefore ) {
//    	    for ( int  stateVarIndex = numberStatevarsAfter ; stateVarIndex < numberStatevarsBefore ;stateVarIndex++ ) {
//        		// add update to unset state variable
//        		statevarUpdates.put(stateVarIndex, null);    	    	
//    	    }
//    	}
//    	
//    	
//    	/*
//    	//update state variables when new value occurs in memV
//    	LinkedHashSet<Integer> newMemVValues = util.JavaUtil.getElementsOnlyInFirstSet(memVafterTrans,memVbeforeTrans);  
//    	ArrayList<Integer> concreteParamValues = concreteInput.getConcreteParameters();
//    	for ( Integer mem : newMemVValues )	 {
//    		//get the index of the parameter in the input
//			// IMPORTANT: if multiple parameters have this value 
//			//            take the first one 
//			//            => free to choose some other strategy, but this is the choice by Frits in its Mapper definition!     		
//    		// note: constants are never put in memV
//    		//       thus only possible source of value in  memV is an input parameter    	   		    		
//    		// thus add extra check new memorable value is really from input	
//    		if(! concreteParamValues.contains(mem)) throw new BugException(Messages.UNKNOWN_MEMV_VALUE);    		
//			
//    		//get the index of the parameter in the input
//			// IMPORTANT: if multiple parameters have this value 
//			//            take the first one 
//			//            => free to choose some other strategy, but this is the choice by Frits in its Mapper definition! 
//    		int paramIndex=concreteParamValues.indexOf(mem);
//  		
//    		Integer stateVarIndex=xgetStateVarIndexToStoreInputParamInto( trace, concreteInput , paramIndex);
//    		
//    		// add update for statevar
//    		statevarUpdates.put(stateVarIndex, mem);
//    		
//    		//update the state variables in the trace
//    		//trace.updateStateVariable(stateVarIndex, concreteInput.getParam(paramIndex));
//    		
//		}    
//    	*/
//    	
//    	/*
//    	SortedMap<Integer, Integer> statevarsAfterTransition=new TreeMap<Integer, Integer>();;
//    	int stateVarIndex=0;
//    	for ( Integer value :  memVafterTrans ) {
//    		statevarsAfterTransition.put(new Integer(stateVarIndex), value);
//    		stateVarIndex++;
//    	}
//
//    	trace.updateStatevars(statevarsAfterTransition)	;    	
//    	*/
//    	
//    	logger.fatal(statevarUpdates);
//    	updateStateVars(trace,statevarUpdates);
//    	
//    	
//    	
// /*   	
//    	// we want to find statevar updates  (from memorable values before and after transition)
// 		LinkedHashMap<Integer, Integer> statevarUpdates=new LinkedHashMap<Integer, Integer>();
//    	
//    	// get memorable values before and after the transition 
//    	Integer transIndex=trace.getTransitionIndex();
//    	LinkedHashSet<Integer> memVafterTrans=trace.getMemvAfterTransition(transIndex);
//    	LinkedHashSet<Integer> memVbeforeTrans=trace.getMemvAfterTransition(transIndex-1);
//   
//      	
//    	// IMPORTANT
//    	// first do remove of memorable of statevar, and then do update of statevar
//    	// because a statevariable can be assigned a new value. If we would first the update and then 
//    	// the unset then statevariable would get unset instead of updated!
//    	
//    	//update state variables when value in memV has been removed
//    	LinkedHashSet<Integer> removedMemVValues = util.JavaUtil.getElementsOnlyInFirstSet(memVbeforeTrans,memVafterTrans);    	
//    	for ( Integer mem : removedMemVValues )	 {
//    		// find out in which statevar this value is stored
//    		// we ask trace because trace stores statevars
//    		Integer stateVarIndex=trace.getStateVariableContainingValue(mem);
//    		// add update to unset state variable
//    		statevarUpdates.put(stateVarIndex, null);
//    		
//    		// unset state variable in trace
//    		//trace.updateStateVariable(stateVarIndex, null);
//		}    	
//    	
//    	//update state variables when new value occurs in memV
//    	LinkedHashSet<Integer> newMemVValues = util.JavaUtil.getElementsOnlyInFirstSet(memVafterTrans,memVbeforeTrans);  
//    	ArrayList<Integer> concreteParamValues = concreteInput.getConcreteParameters();
//    	for ( Integer mem : newMemVValues )	 {
//    		//get the index of the parameter in the input
//			// IMPORTANT: if multiple parameters have this value 
//			//            take the first one 
//			//            => free to choose some other strategy, but this is the choice by Frits in its Mapper definition!     		
//    		// note: constants are never put in memV
//    		//       thus only possible source of value in  memV is an input parameter    	   		    		
//    		// thus add extra check new memorable value is really from input	
//    		if(! concreteParamValues.contains(mem)) throw new BugException(Messages.UNKNOWN_MEMV_VALUE);    		
//			
//    		//get the index of the parameter in the input
//			// IMPORTANT: if multiple parameters have this value 
//			//            take the first one 
//			//            => free to choose some other strategy, but this is the choice by Frits in its Mapper definition! 
//    		int paramIndex=concreteParamValues.indexOf(mem);
//  		
//    		Integer stateVarIndex=getStateVarIndexToStoreInputParamInto( trace, concreteInput , paramIndex);
//    		
//    		// add update for statevar
//    		statevarUpdates.put(stateVarIndex, mem);
//    		
//    		//update the state variables in the trace
//    		//trace.updateStateVariable(stateVarIndex, concreteInput.getParam(paramIndex));
//    		
//		}
//		
//		
//    	updateStateVars(trace,statevarUpdates);
//    	  
//    	 
//    	 */
//
//    	
//    }
//    
//	public void updateStateVars(Trace trace,LinkedHashMap<Integer, Integer> statevarUpdates) {
//		
//		// store stateVar updates in trace 
//		trace.addStatevarUpdates(statevarUpdates);    	
//		
//		
//		// update statevars for new transition
//		SortedMap<Integer, Integer> statevarsBeforeTransition = trace.getStatevarsAfterTransition(); // from previous transition
//		SortedMap<Integer, Integer> statevarsAfterTransition=getUpdatedStateVars(trace,statevarsBeforeTransition,statevarUpdates);
//		trace.updateStatevars(statevarsAfterTransition)	;
//		
//		
//		// EXTRA: store in each transition the statevars
//		//trace.transition2statevarsAfterTransition.add(statevarsAfterTransition);		
//		
//	}
//    
//
//
//	private SortedMap<Integer, Integer> getUpdatedStateVars(Trace trace,SortedMap<Integer, Integer> statevarsBeforeTransition,LinkedHashMap<Integer, Integer> statevarUpdates) {
//		
//		// copy statevarsBeforeTransition in new statevarsAfterTransition
//		SortedMap<Integer, Integer> statevarsAfterTransition= new TreeMap<Integer,Integer>(statevarsBeforeTransition);
//		HashMap <Integer,Parameter> oldStatevar2lastAssignedParam= trace.getStateVarIndex2lastAssignedParam();
//		//List<Parameter> values = new ArrayList(oldStatevar2lastAssignedParam.values());
//		
//		// first do all unsets and the updates to
//		// make sure that if a value get assigned to different statevar
//		// we do not by accident remove it from value2sv
//		//
//		// first do all unsets  
//		for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
//			Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
//			if ( stateVarUpdate == null ) {
//				// update statevars
//				statevarsAfterTransition.remove(stateVarIndex);
//
//				// update in trace  value2statevarIndex mapping
//				// needed 
//				//   in 'updateStateVars' to find out which stateVariable
//				//   must be unset if a memorable value is not memorable anymore
//				Integer stateVarValue=statevarsBeforeTransition.get(stateVarIndex);
//				trace.updateValue2stateVarIndex(stateVarValue,null);
//				
//				// update in trace the param source for statevar 
//				trace.updateStateVariableParamSource(stateVarIndex, null);
//			}
//		}
//		// then do updates
//		for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
//			Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
//			
//			if ( stateVarUpdate != null ) {
//				// update statevars
//				statevarsAfterTransition.put(stateVarIndex, stateVarUpdate);
//				
//				// update in trace  value2statevarIndex mapping
//				// needed 
//				//   in 'updateStateVars' to find out which stateVariable
//				//   must be unset if a memorable value is not memorable anymore
//				trace.updateValue2stateVarIndex(stateVarUpdate,stateVarIndex);
//			
//				// update in trace the param source for statevar 
//				if (! statevarsBeforeTransition.containsValue(stateVarUpdate)) {
//					// fetch the source param which lead to the update
//					Parameter sourceParam = getSourceParamForUpdateFromTrace(trace, stateVarUpdate);
//					if ( sourceParam != null ) {
//						trace.updateStateVariableParamSource(stateVarIndex, sourceParam);
//					} else {
//						logger.fatal(trace);
//						logger.fatal("update stateVars from: " + statevarsBeforeTransition + " to "+ statevarsAfterTransition);
//						throw new BugException("Unknown source param for value used in statevar update " + stateVarUpdate);
//					}
//				} else {
//					// update value in previous statevars
//					// get source param from  statevar2sourceparam index in previous state
//					//statevarsBeforeTransition.  stateVarUpdate
//					boolean found=false;
//					for ( Integer statevarIndex: statevarsBeforeTransition.keySet() ) {
//						if ( statevarsBeforeTransition.get(statevarIndex).equals(stateVarUpdate) ) {
//							Parameter sourceParam=oldStatevar2lastAssignedParam.get(statevarIndex);
//							trace.updateStateVariableParamSource(stateVarIndex, sourceParam);
//							found=true;
//							break;
//						}
//					}
//					if (!found) throw new BugException("Cannot find source param");
//				}
//			}	
//		}	
//		logger.fatal("update stateVars from: " + statevarsBeforeTransition + " to "+ statevarsAfterTransition);
//		return statevarsAfterTransition;
//	}
//	
//	/**
//	 * Retrieves the source parameter which lead to the given update or null if it cannot find it. 
//	 * It looks for it in the current input action and in the current output action.
//	 * 
//	 * @return the Parameter object or null if the source isn't found
//	 */
//	private Parameter getSourceParamForUpdateFromTrace(Trace trace, Integer stateVarUpdate) {
//		int transitionIndex=trace.getTransitionIndex();
//		InputAction inputAction=trace.getInputAction(transitionIndex);
//		OutputAction outputAction=trace.getLastOutputAction();
//		List<Integer> inputValues=inputAction.getConcreteParameters();
//		Parameter sourceParam = null;
//		int sourceParamIndex=inputValues.indexOf(stateVarUpdate);
//		if(sourceParamIndex != -1) {
//			sourceParam = inputAction.getParam(sourceParamIndex);
//		} else {
//			if( outputAction != null) {
//				List<Integer> outputValues = outputAction.getConcreteParameters();
//				sourceParamIndex=outputValues.indexOf(stateVarUpdate);
//				if(sourceParamIndex != -1) {
//					sourceParam = outputAction.getParam(sourceParamIndex);
//				} 		
//			}
//		}
//		return sourceParam;
//	}
//	
//
//}
//=======
package abslearning.mapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;

public class StateVarNMapper implements StateVarMapper{
	private static final Logger logger = Logger.getLogger(StateVar1Mapper.class);	
	
	public StateVarNMapper() {
	}		
	



	
//------------------------------------------------------------------------------------------------------------------	
	
	
	
//--------------------------------------------------------     
//  update state vars in trace
//--------------------------------------------------------     
     

	   
    /**
     * Updates state variables when a new value occurs in memV or a value in memV has been removed
	 * Approach where we only have one state variable per parameter
     * @param concreteInput The input that contains the new value we have to store
     * @param trace The trace for which we have to update the state variables
     */
    public void determineStateVarUpdatesFromMemVs(InputAction concreteInput, Trace trace){
       
    	
    	// we want to find statevar updates  (from memorable values before and after transition)
 		LinkedHashMap<Integer, Integer> statevarUpdates=new LinkedHashMap<Integer, Integer>();
    	
    	// get memorable values before and after the transition 
    	Integer transIndex=trace.getTransitionIndex();
    	LinkedHashSet<Integer> memVafterTrans=trace.getMemvAfterTransition(transIndex);
    	LinkedHashSet<Integer> memVbeforeTrans=trace.getMemvAfterTransition(transIndex-1);
   
 
    	
    	ArrayList<Integer>  statevarsBefore= new ArrayList<Integer> (memVbeforeTrans);
    	int numberStatevarsBefore=statevarsBefore.size();
    	
    	ArrayList<Integer>  statevarsAfter= new ArrayList<Integer> (memVafterTrans);    	
    	int numberStatevarsAfter=statevarsAfter.size();
    	
    	for ( int stateVarIndex=0;  stateVarIndex < numberStatevarsAfter; stateVarIndex++ )	 {
            
    		Integer newValue = statevarsAfter.get(stateVarIndex);
    		
    		if ( stateVarIndex < numberStatevarsBefore ) { 
    			// update statevar only for a changed value 
    			Integer oldValue = statevarsBefore.get(stateVarIndex);
    			if ( !newValue.equals(oldValue)  ) statevarUpdates.put(stateVarIndex, newValue);
    		} else {
    			// always update if extending list of statevars with new statevars
                statevarUpdates.put(stateVarIndex, newValue);
    		}
    	}
    	
    	// if we have less statevars we have to unset statevars
    	if (  numberStatevarsAfter <  numberStatevarsBefore ) {
    	    for ( int  stateVarIndex = numberStatevarsAfter ; stateVarIndex < numberStatevarsBefore ;stateVarIndex++ ) {
        		// add update to unset state variable
        		statevarUpdates.put(stateVarIndex, null);    	    	
    	    }
    	}
    	
    	
    	updateStateVars(trace,statevarUpdates);
    	
    }
	public void updateStateVars(Trace trace,LinkedHashMap<Integer, Integer> statevarUpdates) {
		
		// store stateVar updates in trace 
		trace.addStatevarUpdates(statevarUpdates);    	
		
		// calculate statevars after transition and update this in trace  
		SortedMap<Integer, Integer> statevarsBeforeTransition = trace.getStatevarsBeforeTransition();		
		SortedMap<Integer, Integer> statevarsAfterTransition=Trace.getUpdatedStateVars(statevarsBeforeTransition,statevarUpdates);
		trace.updateStatevars(statevarsAfterTransition)	;
		
		////updates: HashMap <Integer,Integer> trace.value2stateVarIndex
		//trace.updateValue2stateVarIndex(statevarsBeforeTransition,statevarUpdates);
		
		////updates: HashMap <Integer,Parameter>  trace.stateVarIndex2lastAssignedParam
		//trace.updateStateVariableParamSource(statevarsBeforeTransition,statevarUpdates);
	}
	
	
}
