package abslearning.mapper;

import java.util.LinkedHashMap;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
/*
 *  responsibility: 
 *       store memorable values into statevars of current trace using as helping input
 *       the full trace leading to current state
 *       
 *  a trace iterator's index is the transition index
 *  current state of trace is the trace reached by current transition index
 *  
 *  for transition x we can find:
 *    input 
 *    output
 *    statevarupdates
 *    transitionindex  ( 0 for  first transition)
 *    statevars after transition  (transition2statevarsAfterTransition(transitionindex)  -> assume statevars are initially empty)
 *    statevars before transition (transition2statevarsAfterTransition(transitionindex-1) )
 *  
 *  transition index is increased when input of a transition is stored,
 *  so meaning for  
 *      
 *  note: constants are never put in memV so we don't need to know the constants to execute this mapping  
 */
public interface StateVarMapper {
	public void determineStateVarUpdatesFromMemVs(InputAction concreteInput, Trace trace);
	public void updateStateVars(Trace trace,LinkedHashMap<Integer, Integer> statevarUpdates);
}
