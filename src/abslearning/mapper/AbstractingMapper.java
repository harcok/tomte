package abslearning.mapper;

import java.util.SortedMap;

import abslearning.stores.ValueStore;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

/*
 * Learner -- abs input   -> AbstractingMapper -> conc input  ->...
 * Learner <- abs output  <- AbstractionMapper <- conc output <-
 */
public interface AbstractingMapper {
	public InputAction concretizeInput(String abstractInputString, SortedMap<Integer, Integer> statevars, ValueStore valueStore);
	public String abstractOutput(SortedMap<Integer, Integer> statevars, OutputAction outputAction);
}
