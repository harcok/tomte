package abslearning.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import abslearning.determinizer.Determinizer;
import abslearning.exceptions.BugException;
import abslearning.predicates.InputParameterPredicate;
import abslearning.relation.Relation;
import abslearning.stores.CanonicalValueStore;
import abslearning.stores.PredicateStore;
import abslearning.stores.StochasticValueStore;
import abslearning.stores.ValueStore;
import abslearning.trace.AbstractionVariable;
import abslearning.trace.Trace;
import abslearning.trace.action.Action;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.IntegerParamValue;
import abslearning.trace.action.OutputAction;
import abslearning.trace.action.Parameter;

public class AbstractionMapper {
	private static final Logger logger = Logger.getLogger(AbstractionMapper.class);

	protected static List<Integer> constants;
	protected PredicateStore predicateStore;
	
	public AbstractionMapper(PredicateStore predicateStore, List<Integer> constants) {
		AbstractionMapper.constants = constants;
		this.predicateStore = predicateStore;

	}

	//------------------------------------------------------------    
	//  Abstracts an output symbol in state mode: ex f0_f1_f2_x0=x1_x2=f0_x3=p1 ...
	//------------------------------------------------------------    

	private String abstractOutputPrefix(abslearning.trace.action.InputAction concreteInput, abslearning.trace.action.OutputAction concreteOutput, abslearning.trace.Trace trace) {

		StringBuilder updatePrefixBuilder = new StringBuilder();
		int transitionIndex = trace.getTransitionIndex();
		LinkedHashMap<Integer, Integer> statevarUpdates = trace.getStatevarUpdates(transitionIndex);
		SortedMap<Integer,Integer> statevarsBefore=trace.getStateVarsAfterTransition(transitionIndex-1);
		List<Integer> stateVarValues = new ArrayList<Integer>(statevarsBefore.values());
		List<Integer> uniqueFreshOutputValues = getFreshCanonizedOutputValues(concreteOutput, statevarsBefore, concreteInput);
		List<Integer> concreteInputValues = concreteInput.getConcreteParameters();
		
		// first we build the fresh output prefix where are the fresh values in the concrete output
		for(int freshIndex=0; freshIndex<uniqueFreshOutputValues.size(); freshIndex++) {
			updatePrefixBuilder.append("f").append(freshIndex).append("_");
		}
		
		// we concatenate then the statVar prefix, which matches each stateVar update with the source value
		for (Integer stateVarIndex : statevarUpdates.keySet()) {
			Integer stateVarUpdate = statevarUpdates.get(stateVarIndex);
			 updatePrefixBuilder.append("x").append(stateVarIndex);
			if (stateVarUpdate != null) {
				String absParamString =  outputParamValueToParamString(stateVarUpdate, Collections.emptyList(), stateVarValues, concreteInputValues, uniqueFreshOutputValues );
				updatePrefixBuilder.append("=").append(absParamString);
			}
			updatePrefixBuilder.append("_");
		}
		
		return updatePrefixBuilder.toString();
	}
	
	// Gets the fresh canonized output values. 
	private static List<Integer> getFreshCanonizedOutputValues(OutputAction concreteOutput, Map<Integer, Integer> stateVarsBeforeTrans, InputAction concreteInput) {
		Set<Integer> canonizedFreshValues = Determinizer.getCanonizedValueSet(concreteOutput);
		Collection<Integer> valuesRelatedToStateVars = Relation.getRelatedValuesTo(stateVarsBeforeTrans.values(), canonizedFreshValues);
		Collection<Integer> valuesRelatedToInputParams = Relation.getRelatedValuesTo(concreteInput.getConcreteParameters(), canonizedFreshValues);
		canonizedFreshValues.removeAll(valuesRelatedToStateVars);
		canonizedFreshValues.removeAll(valuesRelatedToInputParams);
		return new ArrayList<Integer> (canonizedFreshValues);
	}
	
	//TODO !!!! Make classes for processing strings so it is evident which symbol is used for each parameter type
	private String abstractOutputSuffix(abslearning.trace.action.InputAction concreteInput, abslearning.trace.action.OutputAction concreteOutput, List<Integer> constants, abslearning.trace.Trace trace) {
		StringBuilder outputSuffixBuilder = new StringBuilder();

		SortedMap<Integer, Integer> stateVarsBeforeTrans = trace.getStatevarsBeforeTransition();
		List<Integer> stateVarValuesBeforeTrans = new ArrayList<Integer>(stateVarsBeforeTrans.values());
		List<Integer> concreteInputValues = concreteInput.getConcreteParameters();
		List<Integer> uniqueFreshOutputValues = getFreshCanonizedOutputValues(concreteOutput, stateVarsBeforeTrans, concreteInput);

		for (abslearning.trace.action.Parameter p : concreteOutput.getParameters()) {
			Integer concreteParamValue = ((IntegerParamValue) p.getConcreteValue()).getValue();
			String absParamString = outputParamValueToParamString(concreteParamValue, constants, stateVarValuesBeforeTrans, concreteInputValues, uniqueFreshOutputValues);
			outputSuffixBuilder.append("_").append(absParamString);
		}
		return outputSuffixBuilder.toString();
	}
	
	private String outputParamValueToParamString(Integer concreteParamValue, List<Integer> constants, List<Integer> stateVarValuesBeforeTrans, List<Integer> concreteInputParamValues, List<Integer> freshValues) {
		char type='\0';
		int index;
		if (constants.contains(concreteParamValue)) {
			index = constants.indexOf(concreteParamValue);
			type = 'c';
		} else if (Relation.hasRelatedValue(stateVarValuesBeforeTrans, concreteParamValue)) {
			List<Integer> relatedValues = Relation.getRelatedValues(stateVarValuesBeforeTrans, concreteParamValue);
			Integer firstRelatedValue = relatedValues.get(0);
			Relation relationToValue = Relation.getRelation(firstRelatedValue, concreteParamValue);
			index=  stateVarValuesBeforeTrans.indexOf(firstRelatedValue);
			switch(relationToValue) {
			case EQUAL:
			type = 'x'; break;
			case SUCC:
			case NEXT:
			type = 'i'; break;
			default:
				throw new BugException("case not handled for " + relationToValue);
			}
			
		} else if (Relation.hasRelatedValue(concreteInputParamValues, concreteParamValue)) {
			List<Integer> relatedValues = Relation.getRelatedValues(concreteInputParamValues, concreteParamValue);
			Integer firstRelatedValue = relatedValues.get(0);
			Relation relationToValue = Relation.getRelation(firstRelatedValue, concreteParamValue);
			// take index
			index = concreteInputParamValues.indexOf(firstRelatedValue);
			if ( index == -1 ) throw new BugException("Unknown source param for value used in output");
			
			switch (relationToValue) {
			case EQUAL:
				type = 'p';
				break;
			case SUCC:
			case NEXT:
				type = 'q';
				break;
			default:
				throw new BugException("case not handled for " + relationToValue);
			}

		} else if (freshValues.contains(concreteParamValue)) {
			type = 'f';
			index = freshValues.indexOf(concreteParamValue);
		} else {
			//throw new BugException("The abstract model is not well defined");
			// Integer concreteParamValue, List<Integer> constants, List<Integer> stateVarValuesBeforeTrans, List<Integer> concreteInputParamValues, List<Integer> freshValues
			throw new BugException("unknown source of output value " + concreteParamValue).
			addDecoration("stateVarValuesBeforeTrans", stateVarValuesBeforeTrans).
			addDecoration("concreteInputParamValues", concreteInputParamValues).addDecoration("constants", constants);
		}
		
		if (index == -1 || type == '\0') {
			throw new BugException("Unknown source for value " + concreteParamValue);
		}
		
		return new StringBuilder().append(type).append(index).toString();
	} 

	
	/***
	 * Returns an abstract output based on the concrete output received by the learner, the input that triggered it, the output received before the input (if any) 
	 * and the set of updates, as found in the trace object. 
	 * <br/> <br/>
	 * The abstract output is built as a concatenation of a prefix, the output method and a suffix. 
	 * The prefix encodes updates on the existing state variables. 
	 * The suffix encodes the set of parameter abstractions that replace the concrete parameters in the abstract output.
	 * 
	 * @return prefix(input, outputBeforeInput, updates) + output.methodName + suffix(input, output, updates)
	 * 
	 */
	public String abstractOutput(abslearning.trace.action.InputAction concreteInput, abslearning.trace.action.OutputAction concreteOutput, abslearning.trace.Trace trace) {
		String updatePrefix = abstractOutputPrefix(concreteInput, concreteOutput, trace); 
		String outputSuffix = abstractOutputSuffix(concreteInput, concreteOutput, constants, trace);

		String methodName = concreteOutput.getMethodName();
		String abstractOutput = updatePrefix + methodName + outputSuffix;
		return abstractOutput;
	}

	//-------------------------------------------------------------------------------------------------------
	//  abstraction and concretion functions for input :   abstract input <-> concrete input   (both ways!!)
	//  abstraction functions for output :   concrete output -> abstract output  (for MQ and EQ) 
	//-------------------------------------------------------------------------------------------------------

	/*  abstractInput
	 * 
	 *   given a concrete input from a trace determine the corresponding abstract input
	 *   reversed of : concretizeInput
	 */
	public void abstractInput(InputAction input, Trace currentTrace) {
		//BEFORE  SortedMap<Integer, Integer> statevars = currentTrace.getStatevarsAfterTransition();
		SortedMap<Integer, Integer> statevars = currentTrace.getStatevarsBeforeTransition();
		abstractInput(input, statevars);
	}
	
	public void abstractInput(InputAction input, SortedMap<Integer, Integer> statevars) {
		int numParams = input.getNumberParams();
		for (int parameterIndex = 0; parameterIndex < numParams; parameterIndex++) {
			Parameter inputParam = input.getParam(parameterIndex);
			Integer abstractValue=getAbstractValueForInputParam(inputParam,statevars);
			inputParam.setAbstractValue(abstractValue);
		}
	}
	
	private Integer getAbstractValueForInputParam(Parameter param,SortedMap<Integer, Integer> statevars) {
		
		int parameterIndex=param.getParameterIndex();
		Integer concreteValue = param.getIntegerValue();
		Action action = param.getAction();
		List<Integer>  concreteParams=param.getAction().getConcreteParameters();
		List<Integer> previousConcreteParams = concreteParams.subList(0, parameterIndex);
		String methodName=action.getMethodName();
		
		// first try if concreteValue matches a validation of an existing abstraction for input parameter
		int numberAbstractions = predicateStore.getInputAbstractions(methodName, parameterIndex).size();
		for (int abstractionIndex = 0; abstractionIndex < numberAbstractions; abstractionIndex++) {
			AbstractionVariable abstraction = predicateStore.getInputAbstraction(methodName, parameterIndex, abstractionIndex);
			
			if (abstraction == null) {
				// abstraction could not be applied on current input param in current trace
				continue;
			}
		
			Integer valueByAbstraction = concretizeAbstractionVariable(abstraction, previousConcreteParams , statevars );

			if (valueByAbstraction == null) {
				// abstraction could not be applied on current input param in current trace
				continue;
			}

			if (valueByAbstraction.equals(concreteValue)) {
				// abstraction index found (lowest possible)
				return abstractionIndex;
			}
		}

		// no abstraction found which delivers concreteValue, thus it must be a fresh value
		return -1;
	}
	
	protected Map<Integer, Integer> getConcreteValuesForInputParamForAbstractions(Parameter param,SortedMap<Integer, Integer> statevars, Collection<Integer> abstractionIndexes) {
		Map<Integer,Integer> abstractToConcrete = new HashMap<Integer,Integer>();
		int parameterIndex=param.getParameterIndex();
		Integer concreteValue = param.getIntegerValue();
		Action action = param.getAction();
		List<Integer>  concreteParams=param.getAction().getConcreteParameters();
		List<Integer> previousConcreteParams = concreteParams.subList(0, parameterIndex);
		String methodName=action.getMethodName();
		
		for (int abstractionIndex : abstractionIndexes) {
			AbstractionVariable abstraction = predicateStore.getInputAbstraction(methodName, parameterIndex, abstractionIndex);
			
			if (abstraction == null) {
				// abstraction could not be applied on current input param in current trace
				continue;
			}
		
			Integer valueByAbstraction = concretizeAbstractionVariable(abstraction, previousConcreteParams , statevars );

			if (valueByAbstraction == null) {
				// abstraction could not be applied on current input param in current trace
				continue;
			}

			if (valueByAbstraction.equals(concreteValue)) {
				abstractToConcrete.put(abstractionIndex, concreteValue);
			}
		}

		// no abstraction found which delivers concreteValue, thus it must be a fresh value
		return abstractToConcrete;
	}
	

	/**
	 * concretize input action in currentTrace using current abstractions
	 * 
     * It concretizes all abstract values for all parameters in the InputAction.
     * 
	 *  returns:
	 *     0 : successful concretization
	 *     1:  unsuccessful concretization: statevar not defined   => currently we generate a fresh value because lotos problem
     *     2:  unsuccessful concretization: not canonical abstract input	      
	 */		
	public int concretizeInput(InputAction input, SortedMap<Integer, Integer> stateVars, ValueStore valueStore) {
		//logger.debug("Concretizing: " + input.getAbstractionString());
		//logger.debug("input.getMethodName(): " + input.getMethodName());
		//logger.debug("mapperState.getInputActionPredicate(input.getMethodName()).getAll(): " + mapperState.getInputActionPredicate(input.getMethodName()).getAll());
		List<Parameter> currentAbstractParameterValues = input.getParameters();

		int returnValue = 0;
		for (int j = 0; j < currentAbstractParameterValues.size(); j++) {
			returnValue = concretizeInputParameter(input, j, stateVars, valueStore);
			if (returnValue != 0) {

				// only for mq: put the choosen fresh values in the input back into the mq store
				//              REASON: by always using succeeding values in mq we get less queries stored in observation tree!
				if ( valueStore instanceof CanonicalValueStore ) {
					CanonicalValueStore store= (CanonicalValueStore) valueStore;
 				    for (int param = j - 1; param >= 0; param--) {
						Parameter currentParam = input.getParam(param);
						Integer currentAbstractParamValue = currentParam.getAbstractValue();
						if (currentAbstractParamValue == -1) {
							store.prependFreshValue(currentParam.getIntegerValue());
						}
					}
				}

				break;
			}
		}

		return returnValue;
	}


	private List<Integer> getConcretizedValuesForAllAbstractions(InputParameterPredicate abstractionsForCurrentParameter, InputAction input, SortedMap<Integer, Integer> statevars, int excludeEndIndex) {
		
		List<Integer> previousInputValues= input.getConcreteParametersSoFarDefined();
		
		List<Integer> existingValues = new ArrayList<Integer>();
		AbstractionVariable abstraction;
		for (int k = 0; k < excludeEndIndex; k++) {
			abstraction = abstractionsForCurrentParameter.get(k);
			Integer existingValue = concretizeAbstractionVariable(abstraction, previousInputValues,statevars ); 
			if (existingValue != null) {
				existingValues.add(existingValue);
			}
		}
		return existingValues;
	}

	
	/* Concretizes a parameter with no abstraction. Depending on the value store,
	 * concretization is done canonically or randomly based on stateVars and previous params.
	 */
	protected Integer getNewValue(InputAction input, SortedMap<Integer, Integer> statevars,InputParameterPredicate abstractionsForCurrentParameter, ValueStore valueStore){
		Integer concreteInputValue;
		if ( valueStore instanceof StochasticValueStore ) {
			
			StochasticValueStore valueStoreTest = (StochasticValueStore)valueStore;
			// get all possible values you can get when using a defined abstraction for the current input parameter
			List<Integer> valuesFoundByUsingAbstractionRulesForParam = getConcretizedValuesForAllAbstractions(abstractionsForCurrentParameter, input, statevars, abstractionsForCurrentParameter.size());
			// get value from small set in which may not be found with an abstraction rule 
			concreteInputValue = valueStoreTest.getValue(valuesFoundByUsingAbstractionRulesForParam,input.getConcreteParametersSoFarDefined());
		} else {
			
			CanonicalValueStore valueStoreTest = (CanonicalValueStore)valueStore;
			concreteInputValue = valueStoreTest.popFreshValue();
			
			// concreteInputValue = currentTrace.getValueStoreMQ().popFreshValue();
		}
		return concreteInputValue;
	}
	
	/*  concretize an abstract value for a parameter
	 *  returns:
	 *     0 : successful concretization
	 *     1:  unsuccessful concretization: statevar not defined    => currently we generate a fresh value because lotos problem
     *     2:  unsuccessful concretization: not canonical abstract input	        
	 */
	protected int concretizeInputParameter(InputAction input, int currentParamIndex,SortedMap<Integer, Integer> statevars, ValueStore valueStore) {

		InputParameterPredicate abstractionsForCurrentParameter = predicateStore.getInputAbstractions(input.getMethodName(), currentParamIndex);

		List<Integer> previousInputValues= input.getConcreteParametersSoFarDefined();
		
		// get params of current input
		List<Parameter> params = input.getParameters();
		// get abstract parameters for current parameter
		Integer currentAbstractParamValue = params.get(currentParamIndex).getAbstractValue();

		if (currentAbstractParamValue == -1) {
			// no abstraction defined (red edge)=> choose new value (in case of mq this is a fresh value!)
			Integer newValue=getNewValue(input,statevars,abstractionsForCurrentParameter, valueStore);
			input.setConcreteParameter(new IntegerParamValue(newValue), currentParamIndex);
		} else {
			// abstraction defined : use abstraction  to concretize value and add green edge  
			AbstractionVariable abstraction = abstractionsForCurrentParameter.get(currentAbstractParamValue);
			//int concreteValue=abstraction.getValue(currentTrace);

			Integer concreteInputValue = concretizeAbstractionVariable(abstraction, previousInputValues,statevars );

			
			if (concreteInputValue == null) {
				 // means statevar not defined to do concretization of statevar abtraction!
				return 1;  // skip abstract input and return abstract undefined output and 
				           // don't send anything to sut,  
				           // =>  learned in hyp as selfloop with undefined (statevar) output
				           //     which can easily be removed from hyp later    

                /* other solution instead of selfloop:
				//  =>  do same as "currentAbstractParamValue == -1" case !
				*/
			}

			// check if not an earlier abstraction also gives same value
			List<Integer> valuesFoundForHigherPrioAbstractions = getConcretizedValuesForAllAbstractions(abstractionsForCurrentParameter, input, statevars, currentAbstractParamValue);

			if (valueStore instanceof CanonicalValueStore  && valuesFoundForHigherPrioAbstractions.contains(concreteInputValue)) {
				// not canonical abstract input
				return 2; // emulate with selfloop in hyp (see return 1 above)
			}
			input.setConcreteParameter(new IntegerParamValue(concreteInputValue), currentParamIndex);
		}
		return 0;
	}


	/* concretizeAbstractionVariable - concretize (input) abstraction variable
	 *  inputs:
	 *     - abstraction : abstraction variable from some input param to be concretized
	 *     - previousInputValues :  input values from earlier input params from same input action
	 *     - statevars : state variables in state when the input action is received
	 *  returns:
	 *    - concrete value  : succesfull concretization of abstraction
	 *    - or null : meaning undefined statevar
	 *    
	 *   note: param and constant abstractions can always be concretized to concrete value,
	 *         only a statevar abstraction cannot be always be concretized to concrete value
	 *         if the statevar itself is undefined (=null)
	 */
	//TODO !! succ: make this nicer, right now it is a bit hacky
	private Integer concretizeAbstractionVariable(AbstractionVariable abstraction, List<Integer> previousInputValues, SortedMap<Integer, Integer> statevars) {
		
		char type = abstraction.getVariableType();
		int index = abstraction.getVariableIndex();
		Integer concreteInputValue;
		switch(type){
		case 'c':
			concreteInputValue = constants.get(index); break;
		case 'x':
			concreteInputValue = statevars.get(index); break;
		case 'p':
			concreteInputValue = previousInputValues.get(index); break;
		case 'i':
			if (statevars.containsKey(index)) {
			    if(Relation.NEXT.isActive())
			        concreteInputValue = Relation.NEXT.map(statevars.get(index));
			    else {
			        concreteInputValue = Relation.SUCC.map(statevars.get(index));
			    }
			} else {
				concreteInputValue = null;  
			}
			break;
		default:
			throw new BugException("unknown abstraction type");
		}
		
		return concreteInputValue;
	}
	 
	 public static LinkedHashMap<Integer, Integer> getStateVarUpdatesFromOutputAbstractionString(SortedMap<Integer, Integer> statevarsBeforeTransition,InputAction input, OutputAction output, String outputAbstractionString) {
		LinkedHashMap<Integer, Integer> statevarUpdates = new LinkedHashMap<Integer, Integer>();
		
		
		
        // retrieve statevar updates from outputAbstractionString
		ArrayList<String> xpieces= new ArrayList<String>(); 
		String[] pieces = outputAbstractionString.split("_");
		for (String piece : pieces) {
			if (piece.startsWith("I"))
				throw new BugException("cannot be used on input abstraction string ");

			if (piece.startsWith("O")) {
				// output prefix; verify it:  
				if ( !piece.equals(output.getMethodName())) 
					throw new BugException("output action("+ output  +") and output abstraction string("+ outputAbstractionString +") are in conflict");
				// you're done handling because you reached output prefix
				break;
			} else if (piece.startsWith("x")) {
				xpieces.add(piece);
			}
		}
        
		// translate each statevar update string to real statevarUpdates 
		for (String piece : xpieces) {
			try {
			   addUpdateFromUpdateStringToStateVarUpdates(input, output, piece,statevarsBeforeTransition, statevarUpdates);
			} catch ( IndexOutOfBoundsException  e ) {
				// output a more clarifying error message and rethrow original exception
			    logger.fatal("\n\ndetermining statevars updates:\n     output action("+ output  +") and output abstraction string(" + outputAbstractionString + ") are in conflict\n\n\n");			    
			    throw e;
			}
		}

		return statevarUpdates;	
	}	 
	 
	/* use for  get  statevars updates from output abstraction string */
	public LinkedHashMap<Integer, Integer> getStateVarUpdatesFromOutputAbstractionString(Trace trace, InputAction input, OutputAction output, String outputAbstractionString) {

		
		
		int transitionIndex=trace.getTransitionIndex();
		SortedMap<Integer, Integer> statevarsBeforeTransition = trace.getStateVarsAfterTransition(transitionIndex-1);
		return getStateVarUpdatesFromOutputAbstractionString(statevarsBeforeTransition, input, output, outputAbstractionString);
			

	}

	private static void addUpdateFromUpdateStringToStateVarUpdates(InputAction concreteInput, OutputAction concreteOutput, String piece,SortedMap<Integer, Integer> statevarsBeforeTransition, LinkedHashMap<Integer, Integer> statevarUpdates) {

		int equalCharPos = piece.indexOf('=');
		if (equalCharPos == -1) { // unset statevar
			// has not = in piece
			// must be an unassignment of statevar of form x<number>
			// remove x from head of string and convert rest of string to an integer
			Integer stateVarIndex = new Integer(piece.substring(1));
			// unset statevariable to 'Undefined' = null 
			statevarUpdates.put(stateVarIndex, null);
		} else { // set statevar
			// everything before = must be of form  x<number>
			// remove x from head of string and convert rest of string until = to an integer
			Integer stateVarIndex = new Integer(piece.substring(1, equalCharPos));

			// everything behind = must be of from p<number>
			char type = piece.charAt(equalCharPos + 1);
			Integer index = new Integer(piece.substring(equalCharPos + 2));
			Integer updateValue;

			switch(type){
			case 'p':
				updateValue = concreteInput.getParam(index).getIntegerValue();
				statevarUpdates.put(stateVarIndex, updateValue); break;
			case 'q':
			    if (Relation.SUCC.isActive()) {
			        updateValue = Relation.SUCC.map(concreteInput.getParam(index).getIntegerValue());
			    } else {
			        updateValue = Relation.NEXT.map(concreteInput.getParam(index).getIntegerValue());
			    }
				statevarUpdates.put(stateVarIndex, updateValue); break;
			case 'x':
				updateValue = statevarsBeforeTransition.get(index);
				statevarUpdates.put(stateVarIndex, updateValue); break;
			case 'i':
			    if (Relation.SUCC.isActive()) {
			        updateValue = Relation.SUCC.map(statevarsBeforeTransition.get(index));
			    }else {
			        updateValue = Relation.NEXT.map(statevarsBeforeTransition.get(index));
			    }
				statevarUpdates.put(stateVarIndex, updateValue); break;
			case 'c':
				updateValue = constants.get(index);
				statevarUpdates.put(stateVarIndex, updateValue); break;				
			case 'f':
				List<Integer> freshValuesInOutput = getFreshCanonizedOutputValues(concreteOutput, statevarsBeforeTransition, concreteInput);
				updateValue = freshValuesInOutput.get(index);
				statevarUpdates.put(stateVarIndex, updateValue); break;
			default:
				throw new BugException("Cannot recognize the right operand of the update: " + type + "" + index).
				addDecoration("input", concreteInput).
				addDecoration("output", concreteOutput).
				addDecoration("stateVars", statevarsBeforeTransition);
			}
		}

	}

}
