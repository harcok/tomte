package abslearning.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import sut.info.ActionSignature;
import sut.info.SutInfo;
import abslearning.exceptions.BugException;
import abslearning.exceptions.Messages;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;

public class StateVar1Mapper implements StateVarMapper{
	private static final Logger logger = Logger.getLogger(StateVar1Mapper.class);	
	
	private HashMap<String,Integer> methodAndParamIndex2stateVarIndex= new HashMap <String,Integer>();
	private Integer getStateVariableIndexFromLabel(String stateVarLabel){
		return methodAndParamIndex2stateVarIndex.get(stateVarLabel);

	}
	private void setStateVariableLabel2stateVarIndex(String stateVarLabel, int stateVarIndex){
		methodAndParamIndex2stateVarIndex.put(stateVarLabel, stateVarIndex);
	}	

	//------------------------------------------------------------------------------------------------------------------	

	
    // note: constants are never put in statevars
	public StateVar1Mapper() {
		this.generateStateVariables();
	}	
	

//------------------------------------------------------------------------------------------------------------------	

	

	
	/**
	 * Generates for every parameter (per input) one state variable and assigns an index to the state variable
	 */
	private void generateStateVariables() {
		int stateVarCounter=0;
		//loop over all input signatures (are sorted by methodname)
		for (ActionSignature sig : SutInfo.getInputSignatures()) {
			//loop over all parameter types
			for(int paramIndex=0; paramIndex<sig.getParameterTypes().size(); paramIndex++){
				
				String stateVarLabel=sig.getMethodName()+"_"+ paramIndex;
				logger.fatal("Add new state variable: " + stateVarLabel + " -> x" + stateVarCounter);
				setStateVariableLabel2stateVarIndex(stateVarLabel, stateVarCounter);
				
				//increment the state variable counter
				stateVarCounter++;
			}
		}
	}
	
	
	

	

	
//------------------------------------------------------------------------------------------------------------------	
	
	
	
//--------------------------------------------------------     
//  update state vars in trace
//--------------------------------------------------------     
     
	   
	 /**
     * Updates state variables when a new value occurs in memV or a value in memV has been removed
	 * Approach where we only have one state variable per parameter
     * @param concreteInput The input that contains the new value we have to store
     * @param trace The trace for which we have to update the state variables
     */
    public void determineStateVarUpdatesFromMemVs(InputAction concreteInput, Trace trace){
        // we want to find statevar updates  (from memorable values before and after transition)
 		LinkedHashMap<Integer, Integer> statevarUpdates=new LinkedHashMap<Integer, Integer>();
    	
    	// get memorable values before and after the transition 
    	Integer transIndex=trace.getTransitionIndex();
    	LinkedHashSet<Integer> memVafterTrans=trace.getMemvAfterTransition(transIndex);
    	LinkedHashSet<Integer> memVbeforeTrans=trace.getMemvAfterTransition(transIndex-1);
   
    	// IMPORTANT
    	// first do remove of memorable of statevar, and then do update of statevar
    	// because a statevariable can be assigned a new value. If we would first the update and then 
    	// the unset then statevariable would get unset instead of updated!
    	
    	//update state variables when value in memV has been removed
    	LinkedHashSet<Integer> removedMemVValues = util.JavaUtil.getElementsOnlyInFirstSet(memVbeforeTrans,memVafterTrans);    	
    	for ( Integer mem : removedMemVValues )	 {
    		// find out in which statevar this value is stored
    		// we ask trace because trace stores statevars
    		Integer stateVarIndex=trace.getStateVariableContainingValue(mem);
    		// add update to unset state variable
    		statevarUpdates.put(stateVarIndex, null);
 		}    	
    	
    	//update state variables when new value occurs in memV
    	LinkedHashSet<Integer> newMemVValues = util.JavaUtil.getElementsOnlyInFirstSet(memVafterTrans,memVbeforeTrans);  
    	ArrayList<Integer> concreteParamValues = concreteInput.getConcreteParameters();
    	for ( Integer mem : newMemVValues )	 {
    		//get the index of the parameter in the input
			// IMPORTANT: if multiple parameters have this value 
			//            take the first one 
			//            => free to choose some other strategy, but this is the choice by Frits in its Mapper definition!     		
    		// note: constants are never put in memV
    		//       thus only possible source of value in  memV is an input parameter    	   		    		
    		// thus add extra check new memorable value is really from input	
    		if(! concreteParamValues.contains(mem)) throw new BugException(Messages.UNKNOWN_MEMV_VALUE);    		
			
    		//get the index of the parameter in the input
			// IMPORTANT: if multiple parameters have this value 
			//            take the first one 
			//            => free to choose some other strategy, but this is the choice by Frits in its Mapper definition! 
    		int paramIndex=concreteParamValues.indexOf(mem);
  		
			Integer stateVarIndex= getStateVariableIndexFromLabel(concreteInput.getMethodName()+"_"+paramIndex);
    				
    		// add update for statevar
    		statevarUpdates.put(stateVarIndex, mem);
		}
    	
    	updateStateVars(trace,statevarUpdates);
    }
    
	public void updateStateVars(Trace trace,LinkedHashMap<Integer, Integer> statevarUpdates) {
		
		// store stateVar updates in trace 
		trace.addStatevarUpdates(statevarUpdates);    	
		
		// calculate statevars after transition and update this in trace  
		SortedMap<Integer, Integer> statevarsBeforeTransition = trace.getStatevarsBeforeTransition();		
		SortedMap<Integer, Integer> statevarsAfterTransition=Trace.getUpdatedStateVars(statevarsBeforeTransition,statevarUpdates);
		trace.updateStatevars(statevarsAfterTransition)	;
		
		//updates: HashMap <Integer,Integer> trace.value2stateVarIndex          
		// => needed for StateVar1 : if value is not memorable anymore we use  the function 
		//    trace.getStateVariableContainingValue(mem) which uses the reversed map 'trace.value2stateVarIndex' 
		//    to find which statevar has this value stored and must be unset 
		//    note: we could also search statevar values to find first matching index, but using reversed
		//          index is much faster for a little extra storage
		//-> not needed for StateVarN : because directly maps on memv, so  if we get less memv_i then last x_j, the x_j for j>i are unset
		trace.updateValue2stateVarIndex(statevarsBeforeTransition,statevarUpdates);
		
		//updates: HashMap <Integer,Parameter>  trace.stateVarIndex2lastAssignedParam
		trace.updateStateVariableParamSource(statevarsBeforeTransition,statevarUpdates);
	}
	

}
