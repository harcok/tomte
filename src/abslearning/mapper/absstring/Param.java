package abslearning.mapper.absstring;

public abstract class Param {
	protected final char type;
	protected final int index;
	public Param(char type, int index) {
		this.type = type;
		this.index = index;
	}
	
	public String toString() {
		return new StringBuilder().append(type).append(index).toString();
	}
}
