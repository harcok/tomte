package abslearning.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import abslearning.exceptions.BugException;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.trace.action.Parameter;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.SymbolImpl;
import util.Tuple2;


/**
 * The testing mapper is placed over the learned abstract state machine, together producing a concretization. 
 * Since during testing values may be chosen equal, care must be taken so that the mapping is one to one. 
 * That is, for one abstract input, there is exactly one concrete input. 
 * 
 * Abstraction for param p is done as follows:
 * 	-1 if p is not equal to any of the concretizations for the relevant abstractions for p in
 * 		the current location, or if some of these concretizations are equal to each other
 *  absIndex if p is equal to the concretization of abstraction at index absIndex and all
 *  	concretizations are pairwise distinct
 *   
 * An abstraction is relevant for p if replacing it with -1 induces a different behavior of the system.
 *   
 * This can be quite expensive, hence at first we use the AbstractionMapper. Only in case the 
 * obtained abstraction is not captured by the reduced state machine do we use the abstraction above
 */

public class TestingMapper implements ConcretizingMapper{
	private Automaton reducedhyp;
	private State currentState;
	private AbstractionMapper abstractionMapper;
	private Map<Tuple2<State,String>,Map<Integer, Set<Integer>>> relevantAbstractions;
	
	// stores the abstractions that are relevant for every parameter of every action, for every state(location)
	// the map is updated as abstraction is done
	
	public TestingMapper(AbstractionMapper abstractionMapper, Automaton reducedHyp) {
		this.abstractionMapper = abstractionMapper;
		this.reducedhyp = reducedHyp;
		this.currentState = this.reducedhyp.getStart();
		this.relevantAbstractions = new HashMap<Tuple2<State,String>,Map<Integer,Set<Integer>>>();
	}
	
	
	public void abstractInput(InputAction concreteInput, SortedMap<Integer, Integer> stateVars) {
		String method = concreteInput.getMethodName();
		int numParams = concreteInput.getNumberParams();
		Map<Integer, Set<Integer>> relevantParamAbstractions = getRelevantParamAbstractions(method, numParams);
		this.abstractInput(concreteInput, stateVars, relevantParamAbstractions);
		if (!transitionOnAbstractInput(concreteInput)) {
			throw new BugException("Could not find a suitable abstraction for "+ concreteInput);
		}
	} 
	
	private Map<Integer, Set<Integer>> getRelevantParamAbstractions(String method, int numParams) {
		Map<Integer, Set<Integer>> relevantParamAbstractions;
		Tuple2<State, String> locationMethodKey = new Tuple2<State,String>(this.currentState, method);
		if (relevantAbstractions.containsKey(locationMethodKey)) {
			relevantParamAbstractions = relevantAbstractions.get(locationMethodKey);
		} else {
			List<Symbol> possibleAbstractions = getSymbolsForMethodAtState(currentState, method);
			relevantParamAbstractions = getParameterAbstractions(possibleAbstractions, numParams);
			relevantAbstractions.put(locationMethodKey, relevantParamAbstractions);
		}
		return relevantParamAbstractions;
	}
	
	private boolean transitionOnAbstractInput(InputAction concreteInput) {
		String abstractInputString = concreteInput.getAbstractionString();
		Symbol inputSymbol = new SymbolImpl(abstractInputString);
		Symbol outputSymbol = currentState.getTransitionOutput(inputSymbol);
		if (outputSymbol != null) {
			this.currentState = currentState.getTransitionState(inputSymbol);
			return true;
		} else {
			return false;
		}
	}
	
	private void abstractInput(InputAction input, SortedMap<Integer, Integer> statevars, Map<Integer, Set<Integer>> relevantAbstractionsForState) {
		int numParams = input.getNumberParams();
		for (int parameterIndex = 0; parameterIndex < numParams; parameterIndex++) {
			Parameter param = input.getParam(parameterIndex);
			Integer abstractValue = null;
			Set<Integer> relevantAbstractions = relevantAbstractionsForState.get(parameterIndex);
			if ( relevantAbstractions == null || relevantAbstractions.isEmpty()) {
				abstractValue = -1;
			} else {
				Map<Integer, Integer> abstractToConcrete = this.abstractionMapper.getConcreteValuesForInputParamForAbstractions(param, statevars, relevantAbstractions);
				if (! pairWiseDifferent(abstractToConcrete.values())) {
					abstractValue = -1;
				} else {
					Collection<Integer> concreteValues = abstractToConcrete.values();
					if ( Collections.frequency(concreteValues, param.getIntegerValue()) > 1) {
						throw new BugException("Non-det abstraction");
					} else {
						for (Entry<Integer, Integer> entry : abstractToConcrete.entrySet()) {
							if (entry.getValue() == param.getIntegerValue()) {
								abstractValue = entry.getKey();
								break;
							}
						}
						if (abstractValue == null) 
							abstractValue = -1;
					}
				}
			}
			
			param.setAbstractValue(abstractValue);
		}
	}
	
	// checks if concretizations of relevant abstractions are pairwise different
	private boolean pairWiseDifferent(Collection<Integer> concretizationsOfRelevantAbstractions) {
		int totalValues = concretizationsOfRelevantAbstractions.size();
		int uniqueValues = new HashSet<Integer> (concretizationsOfRelevantAbstractions).size();
		return totalValues == uniqueValues;
	}
	
	
	private List<Symbol> getSymbolsForMethodAtState(State state, String method) {
		List<Symbol> abstractions = new ArrayList<Symbol>(); 
		for (Symbol sym : currentState.getInputSymbols()) {
			String symMethod = new InputAction(sym.toString()).getMethodName(); 
			if (symMethod.equals(method) && currentState.getTransitionOutput(sym) != null) {
				abstractions.add(sym);	
			}
		}
		return abstractions;
	}
	
	/**
	 * Builds a map from parameter indexes to all their different corresponding abstractions
	 * in the list of symbols.
	 * @param method 
	 */
	private Map<Integer, Set<Integer>> getParameterAbstractions(List<Symbol> symbolsWithSameMethod, int numParams) {
		Map<Integer, Set<Integer>> relevantParamAbstractions = new HashMap<Integer, Set<Integer>>();
		for (int i =0; i < numParams; i ++) {
			relevantParamAbstractions.put(i, new HashSet<Integer>());
		}
		
		for (Symbol symbol : symbolsWithSameMethod) {
			InputAction inputAction = new InputAction(symbol.toString());
			ArrayList<Integer> abstractParams = inputAction.getAbstractParameters();
			for (int index=0; index < abstractParams.size(); index++) {
				if (abstractParams.get(index) != -1) {
					Integer abs = abstractParams.get(index);
					relevantParamAbstractions.get(index).add(abs);
				}
			}
		}
		
		return relevantParamAbstractions;
	}
	
	
	private String symbolToAbs(Symbol sym) {
		String symStr = sym.toString();
		int methStart;
		if (symStr.contains("O")) {
			methStart = symStr.indexOf("O");
		} else {
			if (symStr.contains("I")) {
				methStart = symStr.indexOf("I");
			}	else {
				throw new BugException("Invalid symbol " + sym);
			}
		}
		return symStr.substring(methStart);
	}
	
	public void reset() {
		this.currentState = this.reducedhyp.getStart();
	}


	//TODO ! The code from RegisterAutomaton should probably move here. 
	public OutputAction concretizeOutput(InputAction concreteInput, SortedMap<Integer, Integer> stateVars, String abstractOutputString) {
		return null;
	}
}
