package abslearning.verifier;

import org.apache.log4j.Logger;

import de.ls5.jlearn.interfaces.Automaton;

import abslearning.app.Statistics;
import abslearning.learner.Run;
import abslearning.predicates.InputParameterPredicate;
import abslearning.stores.PredicateStore;
import abslearning.trace.AbstractionVariable;

public class DataStructureVerifier implements HypVerifier{
	private static final Logger logger = Logger.getLogger(DataStructureVerifier.class);

	private String dataStructure;
	private int numberOfDataStructureElements;
	private Statistics statistics;
	private Run runner;

	private PredicateStore predicateStore;

	/**
	 * verifies hypothesis using learned abstractions and states for map and fifo-set model
	 * @param dataStructure either fifo,lifo, map or fifo-set
	 * @param numberOfDataStructureElements the number of elements in the data structure
	 * @param predicateStore contains the learned abstractions
	 */
	public DataStructureVerifier(Run run, String dataStructure,
			int numberOfDataStructureElements, PredicateStore predicateStore) {
	    this.runner = run;
		this.dataStructure = dataStructure;
		this.numberOfDataStructureElements = numberOfDataStructureElements;
		this.statistics = Statistics.getInstance();
		this.predicateStore= predicateStore;  // note: predicate store is singleton which is updated everytime with new abstractions when abstractionrefinement is done
	}

	public boolean verifyHyp(HypDesc notused_hypFile){
		boolean learningSuccessful = true;

		Automaton abstractHyp = runner.getAbstractReducedHyp();

		int numberOfStatesInLearnedModel = abstractHyp.getAllStates().size();

		if(dataStructure.equals("fifo")){
			logger.fatal("dataStructure.equals(fifo)");

			//check if all states have been found
			logger.fatal("numberOfStatesInLearnedModel: " + numberOfStatesInLearnedModel);
			if(numberOfStatesInLearnedModel != (numberOfDataStructureElements+1)){
				//state is missing!
				logger.fatal("Number of missing states: " + (numberOfDataStructureElements-numberOfStatesInLearnedModel+1));
				learningSuccessful=false;
			}
		}
		else if(dataStructure.equals("lifo")){
			logger.fatal("dataStructure.equals(lifo)");

			//check if all states have been found
			logger.fatal("numberOfStatesInLearnedModel: " + numberOfStatesInLearnedModel);
			if(numberOfStatesInLearnedModel != (numberOfDataStructureElements+1)){
				//state is missing!
				logger.fatal("Number of missing states: " + (numberOfDataStructureElements-numberOfStatesInLearnedModel+1));
				learningSuccessful=false;
			}
		}
		else if(dataStructure.equals("fifo-set")){
			logger.fatal("dataStructure.equals(fifo-set)");

			//check if all abstractions for Put(p0) have been found
			InputParameterPredicate ippPut = predicateStore.getInputAbstractions("IPut", 0);
			logger.fatal("Input parameter predicates Put: " + ippPut);
			for(int i=0; i<numberOfDataStructureElements-1; i++){
				AbstractionVariable a = new AbstractionVariable('x', i);
				if(!ippPut.containsInputParameterPredicate(a)){
					//abstraction is missing!
					logger.fatal("Number of missing state variables for IPut: " + (numberOfDataStructureElements-i-1));
					learningSuccessful=false;
					break;
				}
			}

			//check if all states have been found
			logger.fatal("numberOfStatesInLearnedModel: " + numberOfStatesInLearnedModel);
			if(numberOfStatesInLearnedModel != (numberOfDataStructureElements+1)){
				//state is missing!
				logger.fatal("Number of missing states: " + (numberOfDataStructureElements-numberOfStatesInLearnedModel+1));
				learningSuccessful=false;
			}
		}
		else if(dataStructure.equals("map")){

			logger.fatal("dataStructure.equals(map)");

			//check if all abstractions for Get(p0) have been found
			InputParameterPredicate ippGet = predicateStore.getInputAbstractions("IGet", 0);
			logger.fatal("Input parameter predicates Get: " + ippGet);
			for(int i=0; i<numberOfDataStructureElements; i++){
				AbstractionVariable a = new AbstractionVariable('x', 2*i);
				if(!ippGet.containsInputParameterPredicate(a)){
					//abstraction is missing!
					logger.fatal("Number of missing state variables for IGet: " + (numberOfDataStructureElements-i));
					learningSuccessful=false;
					break;
				}
			}

			//check if all abstractions for Put(p0) have been found
			InputParameterPredicate ippPut = predicateStore.getInputAbstractions("IPut", 0);
			logger.fatal("Input parameter predicates Put: " + ippPut);
			for(int i=0; i<numberOfDataStructureElements; i++){
				AbstractionVariable a = new AbstractionVariable('x', 2*i);
				if(!ippPut.containsInputParameterPredicate(a)){
					//abstraction is missing!
					logger.fatal("Number of missing state variables for IPut: " + (numberOfDataStructureElements-i));
					learningSuccessful=false;
					break;
				}
			}

			//check if all states have been found
			logger.fatal("numberOfStatesInLearnedModel: " + numberOfStatesInLearnedModel);
			if(numberOfStatesInLearnedModel != (numberOfDataStructureElements+1)){
				//state is missing!
				logger.fatal("Number of missing states: " + (numberOfDataStructureElements-numberOfStatesInLearnedModel+1));
				learningSuccessful=false;
			}
		}

		if ( learningSuccessful ) {
			   logger.info("WHITEBOX_EQUIV: verified has needed abstractions/states thus hypothesis equal SUT\n\nEQUALS\n\n");

			   // finished learning
		       statistics.terminationReason="Correct hyp. found according to WHITEBOX_DATA_EQUIV equivalence check  (when new hypothesis learned)";
	           statistics.addDescriptionTerminationRun(statistics.terminationReason);
	           logger.fatal(statistics.terminationReason);
		}
		return learningSuccessful;
	}
}
