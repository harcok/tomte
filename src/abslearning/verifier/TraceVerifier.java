package abslearning.verifier;

import java.util.List;

import org.apache.log4j.Logger;

import abslearning.learner.Run;
import abslearning.tester.TraceTester;
import abslearning.trace.Trace;
import abslearning.trace.action.OutputAction;
import util.Tuple2;

public class TraceVerifier implements HypVerifier {

	private static final Logger logger = Logger.getLogger(TraceVerifier.class);
	private TraceTester traceTester;

	public TraceVerifier(Run run, List<String> verificationStrings) {
		this.traceTester = new TraceTester(run, verificationStrings);
	}
	public boolean verifyHyp(HypDesc notused_hypFile) {
		Tuple2<Trace, OutputAction> result = this.traceTester.testHyp(0);
		boolean success = result == null;
		if (!success) {
			logger.info("TRACE VERIFICATION: invalid hypothesis" + "\n\n    TRACE VERIFICATION yields counterexample: "
					+ result.tuple0 + "\n     Corresponding hyp output is:" + result.tuple1 + "\n\n");
		} else {
			logger.info(" DEBUG VERIFICATION: verification passed");
		}

		return success;
	}
}
