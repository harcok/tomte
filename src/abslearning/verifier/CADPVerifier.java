package abslearning.verifier;


import org.apache.log4j.Logger;

import abslearning.app.HypStore;
import abslearning.verifier.VerificationWithCADP.CompareResult;

public class CADPVerifier implements HypVerifier {
	private static final Logger logger = Logger.getLogger(CADPVerifier.class);
	private final VerificationWithCADP verifier;
	private String sutModelFile;
	private int compareMinValue;
	private int compareMaxValue;

	public CADPVerifier(VerificationWithCADP verifier, String sutModelFile, int compareMinValue, int compareMaxValue) {
	    this.verifier = verifier;
	    this.sutModelFile = sutModelFile;
	    this.compareMinValue=compareMinValue;
	    this.compareMaxValue=compareMaxValue;

	}



	public boolean verifyHyp( HypDesc modelDesc) {
		HypStore.buildConcreteHypXml(modelDesc.abstractionFile, modelDesc.dotModelFile, modelDesc.concreteXmlModelFile); 
		String hypModelFile = modelDesc.concreteXmlModelFile; 
		

        logger.info("compare hyp: " + hypModelFile + "  <-> sut: " + sutModelFile);

        CompareResult cmpres = verifier.compareModels(hypModelFile,
                sutModelFile, compareMinValue, compareMaxValue);

        logger.info("CADP: " + cmpres.verboseToString() );


		return cmpres.equal;
	}
}
