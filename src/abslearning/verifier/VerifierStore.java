package abslearning.verifier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import abslearning.app.Config;
import abslearning.exceptions.BugException;
import abslearning.learner.Run;
import abslearning.stores.PredicateStore;


public class VerifierStore {
    private static Map<Class<? extends HypVerifier>,
            HypVerifier> verifierMap = new HashMap<Class<? extends HypVerifier>, HypVerifier>();

    public static HypVerifier getVerifier(Class<? extends HypVerifier> verifierClass) {
        HypVerifier HypVerifier = verifierMap.get(verifierClass);
        return HypVerifier;
    }

    public static void addVerifier(HypVerifier hypVerifier) {
        Class<? extends HypVerifier> verifierType = hypVerifier.getClass();
        if (verifierMap.containsKey(verifierType)) {
            throw new BugException("Already created hyp verifier of type " + verifierType);
        }
        verifierMap.put(hypVerifier.getClass(), hypVerifier);
    }

    public static HypVerifier getVerifier(Config config) {
        HypVerifier hypVerifier = null;
        List<HypVerifier> usedHypVerifiers = new ArrayList<HypVerifier>();
        Run run = Run.getInstance();

        for (String verifier_method : config.verification_methods) {
            switch (verifier_method) {
            case "cadp":
                // get cadp verifier
                VerificationWithCADP verifier = new VerificationWithCADP(config.dependency_sutCompareCmd,
                        config.dependency_sutCompareRemoteCmd, config.verification_cadp_server);
                // get hyp verifier
                hypVerifier = new CADPVerifier(verifier, config.sutInterface_model_file, config.verification_cadp_compareMinValue,
                        config.verification_cadp_compareMaxValue);
                break;
            case "traces":
                hypVerifier = new TraceVerifier(run, config.verification_traces_verificationTraces);
                break;
            case "dataStructure":
                // predicate store is singleton which is updated everytime with new abstractions
                // when abstractionrefinement is done
                PredicateStore predicateStore = PredicateStore.getInstance();
                hypVerifier = new DataStructureVerifier(run, config.verification_dataStructure_type,
                        config.verification_dataStructure_size, predicateStore);
                break;
            default:
                throw new BugException("Undefined verifier method " + verifier_method);
            }
            addVerifier(hypVerifier);
            usedHypVerifiers.add(hypVerifier);
        }

        if (usedHypVerifiers.size() > 1) {
            hypVerifier = new CompositeVerifier(usedHypVerifiers.toArray(new HypVerifier[usedHypVerifiers.size()]));
        }
        return hypVerifier;
    }

}
