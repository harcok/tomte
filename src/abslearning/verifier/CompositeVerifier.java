package abslearning.verifier;

public class CompositeVerifier implements HypVerifier {

    private HypVerifier[] verifiers;

    public CompositeVerifier(HypVerifier... verifiers) {
        this.verifiers = verifiers;
    }

    @Override
    public  boolean verifyHyp(HypDesc hypCounter) {
        boolean result = true;
        for (HypVerifier verifier : verifiers) {
            result = verifier.verifyHyp(hypCounter);
            if (result == false)
                break;
        }
        return result;
    }
}
