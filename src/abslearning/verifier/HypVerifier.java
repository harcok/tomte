package abslearning.verifier;

/**
 *   give the verifier a hypothesis and it will verify if it is equivalent with the SUT
 *
 *   returns:
 *     - boolean: specifies whether it is equivalent
 */
public interface HypVerifier {
	public abstract boolean verifyHyp(HypDesc hypDescription);

}
