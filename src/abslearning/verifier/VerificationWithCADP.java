package abslearning.verifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.common.collect.ImmutableSet;

import abslearning.app.Config;
import abslearning.trace.newaction.ConcreteInput;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.interfaces.Word;
import de.ls5.jlearn.shared.SymbolImpl;
import de.ls5.jlearn.shared.WordImpl;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.util.KeyLookup;
import sut.info.SutInfo;
import util.RunCmd;

public class VerificationWithCADP {

	private static final Logger logger = Logger.getLogger(VerificationWithCADP.class);

    private String dependency_sutCompareCmd;
    private String dependency_sutCompareRemoteCmd;
    private String verification_cadp_server;

    public VerificationWithCADP(String dependency_sutCompareCmd, String dependency_sutCompareRemoteCmd,
            String verification_cadp_server) {
        this.dependency_sutCompareCmd=dependency_sutCompareCmd;
        this.dependency_sutCompareRemoteCmd=dependency_sutCompareRemoteCmd;
        this.verification_cadp_server=verification_cadp_server;
    }

    public static class CompareResult {

        public boolean  equal=false;

        public ArrayList<ConcreteInput> inputs=new ArrayList<>();


        /**POJO representing result of comparison with cadp
        *
        * The result class is constructed using the json result of the lower commandline tool used in the background.
        *
        * @param jsonResult
        */
       public CompareResult(JSONObject jsonResult) {
           // get equal param
           this.equal=(Boolean) jsonResult.get("equal");
           if (this.equal) return;

           // get counterExampleInputs
           List<?> counterExampleInputList=(List<?>) jsonResult.get("inputsCounterExample");
           assert( counterExampleInputList!=null  );


           ImmutableSet<DataInputActionType> inputAlphabet=SutInfo.getInputAlphabet();
           String separator = "_";

           // note: CADP does uppercase the methodname so we use the uppercase of the methodname in the key
           Function<DataInputActionType,String> value2key = value -> {return value.getName().toUpperCase() + "_" + value.getParameterTypes().size(); };
           KeyLookup<String,DataInputActionType> lookup = new KeyLookup<>(inputAlphabet, value2key);

           for (int i = 0; i < counterExampleInputList.size(); i++) {
               String cadp_input = (String) counterExampleInputList.get(i);

               // get methodName
               String methodName = util.JavaUtil.getPrefixFromString(cadp_input, separator);
               List<Integer> values;
               if (methodName == null) {
                   methodName = cadp_input;
                   values = new ArrayList<Integer>();
               } else {
                   // getValues
                   String serializedValues = util.JavaUtil.removePrefixFromString(cadp_input, separator);
                   values = util.JavaUtil.deserializeIntValues(serializedValues, separator);
               }
               String key = methodName.toUpperCase() + "_" + values.size();
               DataInputActionType inputType=lookup.get(key);
               ConcreteInput input = new ConcreteInput(inputType, values);
               inputs.add(input);
           }
       }

       public String verboseToString() {
           if ( this.equal ) {
               return "models are EQUAL";
           } else {
               return "models are DIFFERENT"  +
                  "\n\nDIFFERENT" +
                  "\n   counterexample inputs:  " + inputs.toString() +
                  "\n\n";
           }

       }
    }




    private  JSONObject basicCompareModels(   String modelfile1, String modelfile2 ,  int minValue, int maxValue) {


        String minStr= new Integer(minValue).toString();
        String maxStr= new Integer(maxValue).toString();




        String output;
        if ( verification_cadp_server != null ) {

            String[] cmd_compareModel = {
                dependency_sutCompareRemoteCmd,
                verification_cadp_server,
                modelfile1,
                modelfile2,
                minStr, maxStr };
            output = RunCmd.runCmd(cmd_compareModel);

        } else {

                String[] cmd_compareModel = {
                dependency_sutCompareCmd ,
                modelfile1,
                modelfile2,
                minStr, maxStr };
                output = RunCmd.runCmd(cmd_compareModel);

        }


        // parse json
        JSONObject jsonResult=(JSONObject)JSONValue.parse(output);

        return jsonResult;

    }


    /** compare two concrete uppaal models
    *
    * @param config
    * @param modelfile_hyp
    * @param modelfile_sut
    * @param minValue
    * @param maxValue
    * @return
    */
    public CompareResult compareModels(   String modelfile_hyp, String modelfile_sut,
            int minValue, int maxValue) {

            JSONObject jsonResult=basicCompareModels(modelfile_hyp,modelfile_sut,minValue,maxValue);
            CompareResult result=new CompareResult(jsonResult);
            return result;
    }



}
