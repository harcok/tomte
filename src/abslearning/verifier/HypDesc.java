package abslearning.verifier;

import java.io.File;

import abslearning.app.Config;
import abslearning.app.HypStore;

public class HypDesc {
	public final String abstractionFile;
	public final String dotModelFile;
	public final String concreteXmlModelFile;

	public HypDesc( String absFile, String dotFile, String xmlFile) {
		this.abstractionFile = absFile;
		this.dotModelFile = dotFile;
		this.concreteXmlModelFile = xmlFile;
	}
	
	public static HypDesc getDescForHyp(int hypCounter) {
		String abstractionFile = HypStore.getAbstractionsJsonFile(hypCounter);
		String dotModelFile = HypStore.getAbstractHypothesisDotFile(hypCounter);
		String concreteXmlModelFile = HypStore.getConcreteHypXmlFile(hypCounter);
		return new HypDesc(abstractionFile, dotModelFile, concreteXmlModelFile);
	}
	
	public static HypDesc getDescForLearnedModel(Config config) {
		String abstractionFile = out(config, config.learnResults_learnedModelDataFile);
		String dotModelFile = out(config, config.learnResults_abstractModelDotFile);
		String concreteXmlModelFile = out(config, config.learnResults_learnedConcreteModelFile);
		return new HypDesc(abstractionFile, dotModelFile, concreteXmlModelFile);
	}
	
	private static String out(Config config, String inOut) {
		return (new File(config.learnResults_outputDir + "/" + inOut))
                .toString();
	}
	
}
