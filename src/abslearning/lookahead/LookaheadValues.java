package abslearning.lookahead;

import java.util.LinkedHashSet;
import java.util.List;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

/**
 * Extracts lookahead values from the last transition's input, output, prevMemV, 
 * constants and trace to the last transition (inc. input + output)
 */
public class LookaheadValues {
	private LinkedHashSet<Integer> lookaheadValues;
	public LookaheadValues(LinkedHashSet<Integer> memVBeforeTransition, 
			InputAction lastInput, OutputAction lastOutput, Trace traceWithLastTransition, List<Integer> constants) {
		lookaheadValues = getLookaheadValues(memVBeforeTransition, traceWithLastTransition, lastInput, lastOutput, constants);
	}
	
	/**
	 * lookaheadValues = currentNode.memV + values(concreteInput) +
	 * values(freshOutput)
	 */
	private LinkedHashSet<Integer> getLookaheadValues(LinkedHashSet<Integer> parentMemV, Trace traceToCursor,
			abslearning.trace.action.InputAction concreteInput, abslearning.trace.action.OutputAction concreteOutput, List<Integer> constants) {
		LinkedHashSet<Integer> lookaheadValues = new LinkedHashSet<Integer>();
		
		LinkedHashSet<Integer> paramV = new LinkedHashSet<Integer>();
		if(concreteInput != null)
		paramV.addAll(concreteInput.getConcreteParameters());
		if(concreteOutput != null)
		paramV.addAll(concreteOutput.getConcreteParameters());
		
		//List<Integer> memVExtendedByParamV = Relation.getRelatedValuesFrom(parentMemV, paramV);
		LinkedHashSet<Integer> addedParentMemV = new LinkedHashSet<Integer>(parentMemV);
		//addedParentMemV.removeAll(memVExtendedByParamV);
		
		lookaheadValues.addAll(paramV);
		lookaheadValues.addAll(addedParentMemV);
		
		if (constants != null)
		lookaheadValues.removeAll(constants);
		return lookaheadValues;
	}
	
	public LinkedHashSet<Integer> getValues() {
		return lookaheadValues;
	}
}
