package abslearning.lookahead;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;

import abslearning.lookahead.trace.LookaheadTrace;
import abslearning.stores.CanonicalValueStoreProvider;

public class LookaheadConcretizer {
	
	private CanonicalValueStoreProvider freshInputValueStoreProvider;
	private CanonicalValueStoreProvider freshOutputValueStoreProvider;
	private LvCombinationsGenerator combinationsGenerator;
	private List<BiPredicate<LookaheadTrace, List<Integer>>> filters = new LinkedList<BiPredicate<LookaheadTrace, List<Integer>>>();
	
	public LookaheadConcretizer(CanonicalValueStoreProvider freshInputValueStoreProvider, CanonicalValueStoreProvider freshOutputValueStoreProvider, LvCombinationsGenerator  combinationsGenerator) {
		this.freshInputValueStoreProvider = freshInputValueStoreProvider;
		this.freshOutputValueStoreProvider = freshOutputValueStoreProvider;
		this.combinationsGenerator = combinationsGenerator;
	}
	
	public List<List<abslearning.trace.action.InputAction>> generateConcreteLookaheadTraces(LookaheadTrace lookaheadTrace, LinkedHashSet<Integer> lookaheadValues) {
		// return if there are not enough lookahead values to instantiate all different lookahead variables 
		if (lookaheadTrace.getLookaheadParamCounter() > lookaheadValues.size()) {
			return new ArrayList<List<abslearning.trace.action.InputAction>>();
		}

		List<List<Integer>> lookaheadCombinations = this.combinationsGenerator.generateLookaheadValueCombinations(lookaheadValues, lookaheadTrace.getLookaheadParamCounter());
		
		for (BiPredicate<LookaheadTrace,List<Integer>> filter : filters) {
			lookaheadCombinations.removeIf(combo -> !filter.test(lookaheadTrace, combo));
		}
		
		List<List<abslearning.trace.action.InputAction>>  concreteLookaheadTraces = new ArrayList<List<abslearning.trace.action.InputAction>> ();
		
		for ( List<Integer> lookaheadCombination : lookaheadCombinations ) {
	    	List<abslearning.trace.action.InputAction> concreteLookaheadTrace = lookaheadTrace.concretize(lookaheadCombination, 
	    			freshInputValueStoreProvider.newValueStore(), freshOutputValueStoreProvider.newValueStore());
	    	concreteLookaheadTraces.add(concreteLookaheadTrace);
	    }
		
		return concreteLookaheadTraces;
		
	}
	
	public void addFilter(BiPredicate<LookaheadTrace, List<Integer>> combinationFilter) {
		filters.add(combinationFilter);
	}
}