package abslearning.lookahead;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.BiConsumer;

import org.apache.log4j.Logger;

import abslearning.exceptions.DecoratedRuntimeException;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.tree.CacheEntry;
import abslearning.tree.ConcreteTree;
import abslearning.tree.Inconsistency;
import abslearning.tree.TreeCacheIterator;
import abslearning.tree.trace.CachedTraceRunner;
import abslearning.tree.trace.SutTraceInterface;
import sut.info.TomteSutInterface;
import util.Tuple2;

/**
 * Implements the SutTraceInterface and additionally, using a lookahead oracle it 
 * computes the memorable values and adds them to the trace.
 */
public class LookaheadTraceRunner implements SutTraceInterface{
	private static final Logger logger = Logger.getLogger(LookaheadTraceRunner.class);
	
	private TreeCacheIterator cache;
	public static String memVOrder = "lookahead";
	private LookaheadOracle lookaheadOracle;
	private int currentVersion;
	private List<Integer> constants;
	private LookaheadStore lookaheadStore;
	private BiConsumer<Inconsistency, Boolean> inconsistencyHandler;
	
	public LookaheadTraceRunner(TomteSutInterface determinedSutOracle, LookaheadStore lookaheadStore, ConcreteTree concreteTree, BiConsumer<Inconsistency, Boolean> inconsistencyHandler, 
			List<Integer> constants) {
		// setup runner in lookahead trace oracle, we setup a cached runner, thus all executions of the oracle are cached
		CachedTraceRunner cachedSutOracleRunner = new CachedTraceRunner(determinedSutOracle, new TreeCacheIterator(constants, concreteTree));
		LookaheadOracle ltOracle = lookaheadStore.buildLookaheadOracle(cachedSutOracleRunner, constants);
		
		init(lookaheadStore, ltOracle, new TreeCacheIterator(constants, concreteTree), constants);
		this.inconsistencyHandler = inconsistencyHandler;
	}
	
	private void init(LookaheadStore lookaheadStore, LookaheadOracle lookaheadOracle, TreeCacheIterator cache, List<Integer> constants) {
		this.lookaheadStore = lookaheadStore;
		this.cache = cache;
		this.lookaheadOracle = lookaheadOracle;
		this.currentVersion = cache.getVersion();
		this.constants = constants;
	}

	/**
	 * @return concrete output and trace with updated memV. 
	 */
	public OutputAction sendInput(InputAction concreteInput, Trace trace) {
		try {
			// clarity over compactness wrt. code
			OutputAction concreteOutput;
			LinkedHashSet<Integer> memV;
			LinkedHashSet<Integer> parentMemV = cache.current().getMemV();
			boolean needsMemVUpdate = false;
			
			// we check if the input is cached
			if (cache.hasNext(concreteInput)) {
				CacheEntry entry = cache.next(concreteInput);
				// if so, we can already get the output from the cache.
				concreteOutput = entry.getOutput();
				
				// we check if memV are outdated, if so, we re-do lookahead and update memV in cache
				if (currentVersion > entry.getVersion()) {
					Tuple2<OutputAction, LinkedHashSet<Integer>> outputAndMemV = this.lookaheadOracle.doLookaheadForGuardAndOutput(parentMemV, concreteInput, trace);
					memV = outputAndMemV.tuple1;
					needsMemVUpdate = true;
				} else {
					memV = entry.getMemV();
				}
			} 
			
			// if the cache doesn't contain an entry for the input, by executing the lookahead oracle, a new cache entry
			// for the input is created (the oracle uses a cached runner), we must only update memV to those returned in cache
			else {
				Tuple2<OutputAction, LinkedHashSet<Integer>> outputAndMemV = 
						this.lookaheadOracle.doLookaheadForGuardAndOutput(parentMemV, concreteInput, trace);
				concreteOutput = outputAndMemV.tuple0;
				memV = outputAndMemV.tuple1;
				cache.next(concreteInput); //move iterator to the new position
				needsMemVUpdate = true;
			}
			
			// memV update is needed when either a new entry is built, or when an entry's version is outdated 
			if (needsMemVUpdate) { 
				updateMemVInCache(memV);
			}
			
			trace.addMemV(memV);
			return concreteOutput;
		} catch(DecoratedRuntimeException e) {
			e.addDecoration("trace", trace);
			throw e;
		}
	}

	public void sendReset() {
		this.currentVersion = this.cache.getVersion();
		this.cache.reset();
	}
	
	
	private void updateMemVInCache(LinkedHashSet<Integer> newMemV) {
		Inconsistency inconsistency = cache.updateCache(newMemV);
		if (inconsistency != null) {
			boolean successful = this.lookaheadStore.updateFromInconstencyTrace(inconsistency.fullTrace, inconsistency.inconsistencyTrace);
			if (successful) {
				this.cache.increaseVersion();
				logger.fatal(this.lookaheadStore.logStateUpdate());
			}
			this.inconsistencyHandler.accept(inconsistency, successful);
		}
	}
}
