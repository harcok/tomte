package abslearning.lookahead;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Concretizes a symbolic lookahead trace using value store providers for concretizing fresh symbolic 
 * and fresh symbolic output lt params respectively. Also uses a set of lookahead values for concretizing
 * symbolic referring lt params.
 */
public class PermutationsGenerator implements LvCombinationsGenerator{

	@Override
	public List<List<Integer>> generateLookaheadValueCombinations(LinkedHashSet<Integer> lookaheadValues,
			int numDifferentLookaheadParams) {

        // get different possible combinations of different lookahead values
	    List<List<Integer>> lookaheadCombinations= new LinkedList<List<Integer>>();
	    if ( numDifferentLookaheadParams == 0 ) {
	    	// only one combination of empty list of lookahead values
	    	lookaheadCombinations.add(new LinkedList<Integer> ()); 
	    } else {
	    	// generate combinations
	    	List<List<Integer>> outerList = new LinkedList<List<Integer>>();
		    for ( int i=0; i < numDifferentLookaheadParams ; i++ ) {
		    	outerList.add( new LinkedList<Integer>(lookaheadValues) );
		    }
	        util.JavaUtil.generateCombinationsDifferent(lookaheadCombinations, outerList, new LinkedList<Integer>());
	    }
		return lookaheadCombinations;
	}	

}
