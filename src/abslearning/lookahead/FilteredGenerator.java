package abslearning.lookahead;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Predicate;

public class FilteredGenerator implements LvCombinationsGenerator{
	private LvCombinationsGenerator combinationsGenerator;
	private Predicate<List<Integer>> filter;
	
	public FilteredGenerator(LvCombinationsGenerator combinationsGenerator, Predicate<List<Integer>> filter) {
		super();
		this.combinationsGenerator = combinationsGenerator;
		this.filter = filter;
	}

	@Override
	public List<List<Integer>> generateLookaheadValueCombinations(LinkedHashSet<Integer> lookaheadValues,
			int numDifferentLookaheadParams) {
		List<List<Integer>> combinations = combinationsGenerator.generateLookaheadValueCombinations(lookaheadValues, numDifferentLookaheadParams);
		combinations.removeIf(this.filter);
		return combinations;
	}
	
}
