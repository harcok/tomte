package abslearning.lookahead.trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import abslearning.exceptions.BugException;
import abslearning.exceptions.DecoratedRuntimeException;
import abslearning.stores.CanonicalValueStore;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.Parameter;

public class LookaheadTrace {
    private List<LookaheadTraceAction> trace;
    private List<LookaheadTraceParam> referringParams;
	private int traceIndex=-1;
    
	private int lookaheadParamCounter = -1;
	
	public int getLookaheadParamCounter() {
		return lookaheadParamCounter;
	}

    private  static List<Integer> constants;
    /*
     *  constants in trace are stored as list of constants 
     */
    public static void initConstants(List<Integer> constants) {
    	LookaheadTrace.constants=constants;
    }    
    
    List<LookaheadTraceParam> getLookaheadParamsOfType(LookaheadVariableType type) {
		ArrayList<LookaheadTraceParam> params = new ArrayList<LookaheadTraceParam>();
		for (LookaheadTraceAction action : this.trace) 
			for (LookaheadTraceParam param : action.getParameters()) 
				if(param.getVariableType() == type) 
					params.add(param);
		return params;
    }
    
    public LookaheadTrace(LookaheadTrace lookaheadTrace) {
    	this.trace = new ArrayList<LookaheadTraceAction>(lookaheadTrace.trace);
    	this.referringParams = lookaheadTrace.referringParams;
    	this.traceIndex = lookaheadTrace.traceIndex;
    	this.lookaheadParamCounter = lookaheadTrace.lookaheadParamCounter;
    }
    
    public  LookaheadTrace(List<LookaheadTraceAction> actions) { 
    	trace = new ArrayList<LookaheadTraceAction>();
    	this.lookaheadParamCounter = 0;
    	actions.forEach(action -> this.add(action));
    	
    	List<LookaheadTraceParam> lParams = getLookaheadParamsOfType(LookaheadVariableType.l);
    	for (LookaheadTraceParam lParam : lParams) {
    		if (lParam.getVariableIndex() >= this.lookaheadParamCounter)
    			this.lookaheadParamCounter = lParam.getVariableIndex()+1;
    	}
    	
    	referringParams = new ArrayList<LookaheadTraceParam>();
    }
    
    
    private void add(LookaheadTraceAction action) {
    	traceIndex=traceIndex+1;
    	trace.add(action);
    	action.setTraceIndex(traceIndex);
    	
    }
    
    public LookaheadTraceAction getAction(int index){
    	return trace.get(index);
    }
    
    public List<LookaheadTraceAction> getAllActions(){
    	return this.trace;
    }
    
    public int size() {
		return trace.size();
	}
    
    @Override
    public String toString(){
    	StringBuilder result = new StringBuilder();
		for (int i = 0; i < trace.size(); i++) {
			result.append(trace.get(i).toString());
			if (i != trace.size() - 1) {
				result.append(" -> ");   
			}
		}
		
		return result.toString();
    }
    
    
    public boolean implies(LookaheadTrace lttrace) {
    	if (this == lttrace) 
    		return true;
    
    	if (traceIndex != lttrace.traceIndex) 
			return false;
    	
    	if (trace.size() != lttrace.trace.size()) 
    		return false;
    	
    	for (int actionIndex = 0; actionIndex < trace.size(); actionIndex++ )
    		if (!trace.get(actionIndex).implies(lttrace.trace.get(actionIndex))) 
    			return false;
    	
    	return true;
    }
    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof LookaheadTrace))
			return false;

		LookaheadTrace that = (LookaheadTrace) obj;
		
		if (trace.size() != that.trace.size())
			return false;
		
		if (traceIndex != that.traceIndex)
			return false;
	
		if (!trace.equals(that.trace))   // referring param handled in hashcode param itself
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return trace.hashCode() + traceIndex;  // referring param handled in hashcode param itself
	}

	// FUTURE: if we reindex abstractions after updating LookaheadStore we can allow multiple referring params in trace 
	public LookaheadTraceParam getReferringParam() {
		if ( referringParams.size() == 0 ) {
		    return null;
	    } else { 
		    return referringParams.get(0);
		}	
	}	
	


	public void addReferringParam( int actionIndex, int paramIndex) {
		
		
		// FUTURE: if we reindex abstractions after updating LookaheadStore we can allow multiple referring params in trace 
		if ( referringParams.size() == 1  )  throw new BugException("only one referring param allowed!");
		
		// get the LookaheadTraceParam and make it referring (cannot be undone!)
		if (trace.size() <= actionIndex) {
			throw new BugException("Action index " + actionIndex+ " refers to outside of the lookahead trace " + trace);
		}
		LookaheadTraceParam lookaheadTraceParam = trace.get(actionIndex).getParam(paramIndex);
		try {
		lookaheadTraceParam.setReferingParameter();
		}catch(DecoratedRuntimeException refParamException) {
			throw refParamException.addDecoration("lookahead trace", this);
		}
		
		// add this referring LookaheadTraceParam to set of referring params  => currently can have at max 1!
		referringParams.add(lookaheadTraceParam);
		
	}

	public List<InputAction> concretize(List<Integer> lookaheadCombination, CanonicalValueStore inputValueStore, CanonicalValueStore outputValueStore) {
		LinkedList<abslearning.trace.action.InputAction> concreteLookaheadTrace = new LinkedList<abslearning.trace.action.InputAction>(); 
		Map <LookaheadVariable, Integer> lookaheadVariable2Value = new HashMap<LookaheadVariable, Integer>();
		
		for ( int abstractInputActionIndex=0 ; abstractInputActionIndex < this.size() ; abstractInputActionIndex++  ) {
			LookaheadTraceAction abstractInputAction=this.getAction(abstractInputActionIndex);
			LinkedList<Integer> parameters = new LinkedList<Integer>();
			for (   LookaheadTraceParam abstractInputParam :abstractInputAction.getParameters()  ) {
				LookaheadVariable lookaheadVariable = abstractInputParam.getVariable();
				Integer concreteValue = null;
				if (!lookaheadVariable2Value.containsKey(lookaheadVariable)) {
					switch(lookaheadVariable.getVariableType()) {
					case c:
						// a new constant as parameter
						concreteValue =constants.get( abstractInputParam.getVariableIndex() ); break;
					case f:
						// a new fresh input value as parameter
	                	concreteValue = inputValueStore.popFreshValue(); break;
					case l:
						// a new lookahead value as parameter
						concreteValue = lookaheadCombination.get(lookaheadVariable.getVariableIndex()); break;
					case s:
						// a new fresh output value as parameter
						concreteValue = outputValueStore.popFreshValue(); break;
					case i:
						// an parameter refers to a previous lookahead parameter
						LookaheadTraceParam refLookaheadParam = this.getParameter(lookaheadVariable.getVariableIndex());
						// we get the value of the lookahead param
						InputAction actionReferred = concreteLookaheadTrace.get(refLookaheadParam.getInputActionIndex());
						Parameter paramReferred = actionReferred.getParam(refLookaheadParam.getParameterIndex()); 
						//concreteLookaheadTrace.get(refLookaheadParam.getInputActionIndex());
						concreteValue = paramReferred.getIntegerValue(); break;
					default:
						throw new BugException("abstractInputParam of type " + abstractInputParam.getVariableType() + " is wrongly defined!");
					}  
					lookaheadVariable2Value.put(lookaheadVariable, concreteValue);
				} else {
					concreteValue = lookaheadVariable2Value.get(lookaheadVariable);
				}
				parameters.add(abstractInputParam.getRelation().map(concreteValue));
        	}
        	abslearning.trace.action.InputAction inputAction  = new abslearning.trace.action.InputAction(  parameters, abstractInputAction.getMethodName() ) ;
        	concreteLookaheadTrace.add(inputAction);
		}
		return concreteLookaheadTrace;
	}
	
	/**
	 * Fetches the parameter at the given index. Parameters are indexed from from 0 (first param in first action) to 
	 * n (last param in last action).
	 */
	public LookaheadTraceParam getParameter(int globalParamIndex) {
		LookaheadTraceAction paramAction = null;
		for (LookaheadTraceAction abstractInputAction : trace) {
			if (globalParamIndex < abstractInputAction.size()) {
				paramAction = abstractInputAction;
				break;
			} else {
				globalParamIndex -= abstractInputAction.size();
			}
		}
		if (paramAction == null) {
			throw new BugException("Could not find lookahead parameter " + globalParamIndex).addDecoration("lookahead trace", this);
		}
		return paramAction.getParam(globalParamIndex);
	}
	
}
