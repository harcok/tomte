package abslearning.lookahead.trace;

public class LookaheadParamSignature {
	public final String actionName;
	public final Integer paramIndex;
	
	public LookaheadParamSignature(abslearning.trace.action.Parameter param) {
		this.actionName = param.getAction().getMethodName();
		this.paramIndex = param.getParameterIndex();
	}
	
	public LookaheadParamSignature(String actionName, Integer paramIndex) {
		super();
		this.actionName = actionName;
		this.paramIndex = paramIndex;
	}
	
	public boolean matchesSignature(abslearning.trace.action.Parameter param) {
		return new LookaheadParamSignature(param).equals(this);
	}
	
	public boolean matchesSignature(LookaheadParamSignature signature) {
		return this.equals(signature);
	}
	
	public String toString() {
		return this.actionName + "." + "p" + this.paramIndex;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionName == null) ? 0 : actionName.hashCode());
		result = prime * result + ((paramIndex == null) ? 0 : paramIndex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LookaheadParamSignature other = (LookaheadParamSignature) obj;
		if (actionName == null) {
			if (other.actionName != null)
				return false;
		} else if (!actionName.equals(other.actionName))
			return false;
		if (paramIndex == null) {
			if (other.paramIndex != null)
				return false;
		} else if (!paramIndex.equals(other.paramIndex))
			return false;
		return true;
	}
}
