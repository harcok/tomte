package abslearning.lookahead.trace;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import abslearning.trace.action.Parameter;

/**
 * A lookahead trace which also stores the source of each lookahead variable. This
 * is useful, since then lookahead traces can be instantiated with those lookahead values which originate
 * from parameters of a type corresponding to their sources.
 */
public class SourcedLookaheadTrace extends LookaheadTrace{
	
	/** map containing mappings from ltParams and the source signature. **/
	private Map<LookaheadVariable, LookaheadParamSignature> lVarsSignature;

	
	public SourcedLookaheadTrace(LookaheadTrace ltTrace, LinkedHashMap<LookaheadVariable, LookaheadParamSignature> lVarsSignature) {
		super(ltTrace);
		this.lVarsSignature = lVarsSignature;
	}



	public boolean checkSourceSignature(List<Parameter> sourceParamsOfLookaheadValues) {
		if (sourceParamsOfLookaheadValues.size() <= lVarsSignature.size()) {
			for (Entry<LookaheadVariable, LookaheadParamSignature> entry : lVarsSignature.entrySet()) {
				LookaheadVariable lookaheadVariable = entry.getKey();
				LookaheadParamSignature sourceSignature = entry.getValue();
				Parameter sourceParam = sourceParamsOfLookaheadValues.get(lookaheadVariable.getVariableIndex());
				
				if ( !sourceSignature.matchesSignature(sourceParam) ) 
					return false;
			}
			return true;
		} else 
			return false;
	}
	
	
	public String toString() {
		String ltString = super.toString();
		final StringBuilder builder = new StringBuilder();
		builder.append(ltString);
		
		// attach signature  of all lookahead variables
		builder.append(" {");
		this.lVarsSignature.forEach((var, sign) -> 
		builder.append("{").append(var).append(":").append(sign).append("}"));
		builder.append("}");
		
		
		return builder.toString();
	}
	
	public boolean equals(Object that) {
		boolean equals = super.equals(that);
		if (equals) {
			if (that instanceof SourcedLookaheadTrace) {
				equals = ((SourcedLookaheadTrace) that).lVarsSignature.equals(this.lVarsSignature);
			} else equals = false;
		}
		return equals;
	}
	
	public boolean implies(LookaheadTrace ltTrace) {
		boolean implies = super.implies(ltTrace);
		if (implies) {
			implies = ((SourcedLookaheadTrace) ltTrace).lVarsSignature.equals(this.lVarsSignature);
		} 
		return implies;
	}
}
