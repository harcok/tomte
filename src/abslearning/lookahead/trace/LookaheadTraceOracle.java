package abslearning.lookahead.trace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiPredicate;

import org.apache.log4j.Logger;

import abslearning.ceanalysis.graph.RelationGraph;
import abslearning.ceanalysis.graph.TraceParameterNode;
import abslearning.exceptions.ValueCannotBeDecanonizedException;
import abslearning.lookahead.LookaheadConcretizer;
import abslearning.lookahead.LookaheadConcretizerProvider;
import abslearning.lookahead.LookaheadOracle;
import abslearning.lookahead.LookaheadValues;
import abslearning.relation.Relation;
import abslearning.stores.FreshInputValueStore;
import abslearning.trace.Trace;
import abslearning.trace.Transition;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.IntegerParamValue;
import abslearning.trace.action.OutputAction;
import abslearning.trace.action.Parameter;
import abslearning.tree.trace.SutTraceInterface;
import util.Tuple2;

/**
 * Retrieves all memorable values by running a set of lookahead traces. 
 */
public class LookaheadTraceOracle implements LookaheadOracle{
	private static Logger logger = Logger.getLogger(LookaheadTraceOracle.class);
	private static boolean debug = false;
	private final List<Integer> constants;
	private SutTraceInterface sutTrace;
	private LookaheadTraceStore lookaheadStore;
	private LookaheadConcretizerProvider concretizerProvider;
	private boolean skipInputActionsToNodeOnce;
	private boolean useSourceFilter;

	public LookaheadTraceOracle(SutTraceInterface traceRunner, LookaheadTraceStore store, LookaheadConcretizerProvider concretizerProvider, List<Integer> constants) {
		this.constants = constants;
		this.sutTrace = traceRunner;
		this.lookaheadStore = store;
		this.concretizerProvider = concretizerProvider;
		this.useSourceFilter = false;
	}
	
	void useSourceFilter() {
		this.useSourceFilter = true;
	}
	
	public Tuple2<OutputAction, LinkedHashSet<Integer>> doLookaheadForGuardAndOutput(LinkedHashSet<Integer> memVBeforeInput, InputAction input, Trace traceToCurrentPosition) {
		this.skipInputActionsToNodeOnce = false;
		OutputAction output = runInputActionsToNodeAndGetLastOutput(traceToCurrentPosition.getConcreteInputActions());
		traceToCurrentPosition.add(output);
		this.skipInputActionsToNodeOnce = true;
		LinkedHashSet<Integer> lookaheadValues = new LookaheadValues(memVBeforeInput, input, output, traceToCurrentPosition, constants).getValues();
		LinkedHashSet<Integer> newMemV = new LinkedHashSet<Integer>();
		LinkedHashSet<Integer> observations = new LinkedHashSet<Integer>();
		
		// combined loop over : OUTPUT LOOKAHEAD + GUARDED LOOKAHEAD + INITIAL
				// (TEST) LOOKAHEAD
		List<LookaheadTrace> abstractLookaheadTraces = this.lookaheadStore.getLookaheadTraces();
		List<InputAction> actionsToCurrentNode = traceToCurrentPosition.getConcreteInputActions();
		if (debug)
			logger.fatal("traceToCurrentNode: " + traceToCurrentPosition);

		for (LookaheadTrace abstractLookaheadTrace : abstractLookaheadTraces) {
	
			List<List<abslearning.trace.action.InputAction>> generatedConcreteLookaheadTraces;
			generatedConcreteLookaheadTraces = generateConcreteLookaheadTraces(abstractLookaheadTrace,
					lookaheadValues, traceToCurrentPosition);
		
			Tuple2<LinkedHashSet<Integer>, LinkedHashSet<Candidate>> memVAndCandidates = executeConcreteLookaheadTracesAndFetchMemV(
					actionsToCurrentNode, traceToCurrentPosition, abstractLookaheadTrace, generatedConcreteLookaheadTraces);
			newMemV.addAll(memVAndCandidates.tuple0);
			memVAndCandidates.tuple1.forEach(c -> observations.add(c.referringValue));
			
			if (debug)
				logger.fatal("abstractLookaheadTrace: " + abstractLookaheadTrace + "\n" + memVAndCandidates.tuple0);
	
		}
		
		traceToCurrentPosition.removeLastAction();
	//	newMemV = getReducedMemV(newMemV, observations);
		
		return new Tuple2<OutputAction, LinkedHashSet<Integer>>(output, newMemV);
	}
	
	/**
	 * Instantiates a symbolic lookahead trace using a permutitive concretizer. 
	 * Returns a list of concrete lookahead traces.
	 */
	private List<List<abslearning.trace.action.InputAction>> generateConcreteLookaheadTraces( LookaheadTrace lookaheadTrace,
			LinkedHashSet<Integer> lookaheadValues, Trace traceToLookaheadNode) {

		LookaheadConcretizer concretizer = concretizerProvider.newConcretizerForTrace(traceToLookaheadNode);
		if (useSourceFilter) {
			concretizer.addFilter(new LvSourceFilter(traceToLookaheadNode, lookaheadValues));
		}
		List<List<abslearning.trace.action.InputAction>> concreteLookaheadTraces = concretizer.generateConcreteLookaheadTraces(lookaheadTrace, lookaheadValues);

		
		return concreteLookaheadTraces;
	}


	/**
	 * Ensures that the source parameter of each lookahead value has a signature matching
	 * the source parameter's corresponding lookahead variable.
	 */
	static class LvSourceFilter implements BiPredicate<LookaheadTrace, List<Integer>> {
		private HashMap<Integer, Parameter> valueToParam;

		public LvSourceFilter(Trace trace, LinkedHashSet<Integer> lookaheadValues) {
			this.valueToParam = mapLvToSourceParams(trace, lookaheadValues);
		}
		
		

		private HashMap<Integer, Parameter> mapLvToSourceParams(Trace trace, LinkedHashSet<Integer> lookaheadValues) {
			HashMap<Integer, Parameter> valueToMap = new HashMap<Integer,Parameter>();
			
			for (Integer lookaheadValue : lookaheadValues) {
				Integer value = lookaheadValue;
				Parameter sourceParam = trace.getSourceParameter(value);
				
				valueToMap.put(lookaheadValue, sourceParam);
			}
			return valueToMap;
		}


		// Depending on the source of the parameter, we might not execute a lookahead trace
		public boolean test(LookaheadTrace ltTrace, List<Integer> lookaheadCombination) {
			SourcedLookaheadTrace sourcedLtTrace = ((SourcedLookaheadTrace) ltTrace);
			List<Parameter> sourceParams = new ArrayList<Parameter>();
			// add all source params to list
			lookaheadCombination.forEach(val -> sourceParams.add(this.valueToParam.get(val)));
			return sourcedLtTrace.checkSourceSignature(sourceParams);
		}
		
	}
	
	/**
	 * Runs the inputs to a lookahead node and the lookahead inputs.
	 * Consists of two steps: running the inputs to the current node, followed by
	 * both running and tracing the lookahead inputs after the current node.
	 *  
	 * Returns the resulting trace, containing inputs and outputs.
	 */
	protected Trace runConcreteLookaheadTrace(
			List<abslearning.trace.action.InputAction> initTraceInputActions,
			List<abslearning.trace.action.InputAction> lookaheadInputActions) {

	    // reset sut and run inputs to node
	    runInputActionsToNodeAndGetLastOutput(initTraceInputActions);
	    
	    // run the lookahead inputs and build the lookahead trace
	    Trace ltTrace = runLookaheadContinuationAndBuildTrace(lookaheadInputActions);
		
		return ltTrace;
	}
	
	protected OutputAction runInputActionsToNodeAndGetLastOutput(List<abslearning.trace.action.InputAction> initTraceInputActions) {
		if (this.skipInputActionsToNodeOnce) {
			this.skipInputActionsToNodeOnce = false;
			return null;
		}
		OutputAction output = null;
	    // reset sut
	    sutTrace.sendReset();

	    // send first inputs to lookahead node
	    for (abslearning.trace.action.InputAction input : initTraceInputActions) {
	        output = sutTrace.sendInput(input, null);
	    }
	    return output;
	}
	
	protected Trace runLookaheadContinuationAndBuildTrace(List<abslearning.trace.action.InputAction> lookaheadInputActions) {
	    Trace ltTrace = new Trace();
        
        // send lookahead inputs and catch the outputs
        for (abslearning.trace.action.InputAction input : lookaheadInputActions) {
            ltTrace.add(input);
            abslearning.trace.action.OutputAction output = sutTrace.sendInput(input, null);
            ltTrace.add(output);
        }
        return ltTrace;
	}

	/**
	 * Executes each concrete lookahead trace, fetches the candidates (candidateMemV + referringValue), 
	 * and retains the candidateMemV which are found to be memorable. 
	 */
	private Tuple2<LinkedHashSet<Integer>,LinkedHashSet<Candidate>> executeConcreteLookaheadTracesAndFetchMemV(
			List<abslearning.trace.action.InputAction> concreteInputActionsToLookaheadNode, Trace traceToLookaheadNode, 
			LookaheadTrace abstractLookaheadTrace, List<List<abslearning.trace.action.InputAction>> generatedConcreteLookaheadTraces) {
		LinkedHashSet<Integer> memV = new LinkedHashSet<Integer>();
		LinkedHashSet<Candidate> memVCandidates = new LinkedHashSet<>();
		Set<Integer> valuesToLookaheadNode = new HashSet<>(traceToLookaheadNode.getAllIntegerValues());
		
		for (List<abslearning.trace.action.InputAction> concreteLookaheadInputActions : generatedConcreteLookaheadTraces) {
			if (debug)
				logger.fatal("concreteLookaheadInputActions" + concreteLookaheadInputActions);

			Trace traceFromLookaheadNode = null;

			try {
				traceFromLookaheadNode = runConcreteLookaheadTrace(concreteInputActionsToLookaheadNode,
						concreteLookaheadInputActions);
			} catch (ValueCannotBeDecanonizedException exception) {
				// running lookahead trace with s_i value failed
				// So we cannot find any lookahead values with this trace!
				continue;
			}
			
			LinkedHashSet<Candidate> candidates = new LinkedHashSet<>();
			candidates.addAll(getGuardMemVCandidates(abstractLookaheadTrace, concreteLookaheadInputActions, traceFromLookaheadNode));
			candidates.addAll(getOutputMemVCandidates(valuesToLookaheadNode, traceFromLookaheadNode));
			//memVCandidates.addAll(getOutputMemVCandidates(new LinkedHashSet<>(traceToLookaheadNode.getAllIntegerValues()), traceFromLookaheadNode));
			
			addNewMemVFromCandidates(candidates, memV, concreteInputActionsToLookaheadNode, traceToLookaheadNode, traceFromLookaheadNode);
			memVCandidates.addAll(candidates);
		
		}

		return new Tuple2<>(memV, memVCandidates);
	}
	
	private LinkedHashSet<Integer> addNewMemVFromCandidates(LinkedHashSet<Candidate> candidates, LinkedHashSet<Integer> memV,  
			List<abslearning.trace.action.InputAction> concreteInputActionsToLookaheadNode,
			Trace traceToLookaheadNode, Trace traceFromLookaheadNode) {
		for (Candidate candidate : candidates) {
			if (memV.contains(candidate.candidateMemV)) 
				continue;
			boolean independentOfTrace;
			try {
				independentOfTrace = LtValueIndependentTrace(concreteInputActionsToLookaheadNode, traceToLookaheadNode,
							candidate.referringValue, traceFromLookaheadNode);
			} catch (ValueCannotBeDecanonizedException exception) {
				// running lookahead trace with s_i value failed
				// So we cannot prove lookahead value is dependent on trace,
				// and therefore cannot be sure it is memorable.
				// So it is not proven memorable!!
				continue;
			}

			if (!independentOfTrace)
				memV.add(candidate.candidateMemV);
		}
		return memV;
	}
	
	// we want to collect all output values apart from those which originate from an input value in the lookahead trace
	private LinkedHashSet<Candidate> getOutputMemVCandidates(Set<Integer> valuesToLookaheadNode, Trace traceFromLookaheadNode) {
		LinkedHashSet<Integer> valuesSoFar = new LinkedHashSet<Integer>();
		LinkedHashSet<Candidate> memVCandidates = new LinkedHashSet<Candidate>();
		for (Transition trans : traceFromLookaheadNode.eachTransition()) {
			ArrayList<Integer> inputValues = trans.input.getConcreteParameters();
			valuesSoFar.addAll(inputValues);
			ArrayList<Integer> outputValues = trans.output.getConcreteParameters();
			LinkedHashSet<Integer> outputValueSet = new LinkedHashSet<>(outputValues);
			outputValueSet.removeAll(constants); // we are not interested in constants
			for (Integer outputValue : outputValueSet) {
				boolean hasRelatedOutput = Relation.hasRelatedValue(valuesSoFar, outputValue);
				valuesSoFar.add(outputValue);
				if (hasRelatedOutput) 
					continue; // if the output value is rooted in a previous output value we skip it

				Integer referredValue = Relation.getFirstRelatedValue(valuesToLookaheadNode, outputValue);
				if (referredValue != null) {
					Integer referringValue = outputValue;
					memVCandidates.add(new Candidate(referredValue, referringValue));
				}
				
			}
		}

		return memVCandidates;
	}
	
	private LinkedHashSet<Candidate> getGuardMemVCandidates(LookaheadTrace abstractLookaheadTrace, List<InputAction> concreteLookaheadInputActions, Trace traceFromLookaheadNode ) {
		LinkedHashSet<Candidate> memVCandidates = new LinkedHashSet<Candidate>();
		
		// get referring param value
		// note: referring param is always of type 'l' and must be verified
		// that it is really memorable!
		LookaheadTraceParam abstractLookaheadParam = abstractLookaheadTrace.getReferringParam();
		if (abstractLookaheadParam != null) {
			// we got a guarded lookahead trace
			int actionIndex = abstractLookaheadParam.getLookaheadInputAction().getTraceIndex();
			int paramIndex = abstractLookaheadParam.getParameterIndex();
			Integer referringValue = concreteLookaheadInputActions.get(actionIndex).getConcreteParameters()
					.get(paramIndex);
			Integer valueReferred = abstractLookaheadParam.getRelation().rev(referringValue);
			memVCandidates.add(new Candidate(valueReferred, referringValue));
		}
		
		return memVCandidates;
	}
	
	
	/**
	 * A candidate is a pair comprising a possibly memorable value and the 
	 * value which refers to it. The referring value is replaced by a fresh value, and then
	 * the trace is re-run and outputs are compared to check if the relations
	 * are relevant.
	 */
	static class Candidate {
		public final Integer candidateMemV;
		public final Integer referringValue;
		public Candidate(Integer candidateMemV, Integer referringValue) {
			super();
			this.candidateMemV = candidateMemV;
			this.referringValue = referringValue;
		}
		
		public String toString() { 
			return "candidateMemV:" + candidateMemV + "; referringValue:" + referringValue; }
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((candidateMemV == null) ? 0 : candidateMemV.hashCode());
			result = prime * result + ((referringValue == null) ? 0 : referringValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Candidate other = (Candidate) obj;
			if (candidateMemV == null) {
				if (other.candidateMemV != null)
					return false;
			} else if (!candidateMemV.equals(other.candidateMemV))
				return false;
			if (referringValue == null) {
				if (other.referringValue != null)
					return false;
			} else if (!referringValue.equals(other.referringValue))
				return false;
			return true;
		}
	}
	

	/**
	 * Checks if the lookahead value is independent of the trace(=trace to point
	 * we are doing lookahead). If lookahead value is independent of trace then
	 * it is not memorable. However if NOT independent of trace then it must be
	 * memorable.
	 * 
	 * 
	 * 
	 * @param inputs
	 *            - concrete lookahead input actions
	 * @param outputs
	 *            - concrete lookahead output actions
	 */
	private boolean LtValueIndependentTrace(List<abslearning.trace.action.InputAction> concreteInputActionsToLookaheadNode,
			Trace traceToLookaheadNode, Integer lookaheadParamValue, Trace traceFromLookaheadNode) {
		// we need to toggle the first param with lookaheadParamValue fresh, while preserving any relations it has with subsequent params,
		// we do that by storing all  trace parameter values in a graph tree and toggling the first param with lookaheadParamValue
		Integer freshValue = getFreshValueForTrace(traceToLookaheadNode, traceFromLookaheadNode); 

		RelationGraph relationGraph = new RelationGraph(traceFromLookaheadNode.getAllParameters(), Collections.emptyList(), false);
		
		TraceParameterNode nodeToToggle = relationGraph.getFirstNodeWithValue(lookaheadParamValue);
		
		relationGraph.updateNodeNaively(nodeToToggle, freshValue);
		
		// we get a copy of the lookahead trace and update it with the new values resulting from the toggle 
		Trace toggledTrace = new Trace(traceFromLookaheadNode);
		
		for (TraceParameterNode paramNode : relationGraph.getNodes()) {
			if (!paramNode.isConstant())
				paramNode.getCorrespondingParam(toggledTrace).setConcreteValue(new IntegerParamValue(paramNode.getContent()));
		}
		
		// we run the toggled fresh lookahead trace
		Trace resultOfToggle = runConcreteLookaheadTrace(concreteInputActionsToLookaheadNode,  toggledTrace.getConcreteInputActions());
		
		// we compare the two traces, the one obtained by executing a 
		// normal lookahead sequence and relabeling to the one obtained by executing a relabeled lookahead sequence
		return toggledTrace.equalsConcreteOutputs(resultOfToggle);
	}

	
	private Integer getFreshValueForTrace(Trace traceToLookaheadNode, Trace traceFromLookaheadNode) {
		List<Integer> values = new ArrayList<Integer>();
		values.addAll(constants);
		values.addAll(traceToLookaheadNode.getAllIntegerValues());
		values.addAll(traceFromLookaheadNode.getAllIntegerValues());
		FreshInputValueStore valueStore = new FreshInputValueStore(values);
		Integer freshInt = valueStore.popFreshValue();
		return freshInt;
	}
}
