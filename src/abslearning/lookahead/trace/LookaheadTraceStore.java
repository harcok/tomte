package abslearning.lookahead.trace;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import abslearning.exceptions.BugException;
import abslearning.lookahead.LookaheadConcretizerProvider;
import abslearning.lookahead.LookaheadOracle;
import abslearning.lookahead.LookaheadStore;
import abslearning.lookahead.trace.SuffixClosedLtSetBuilder;
import abslearning.trace.Trace;
import abslearning.trace.action.Parameter;
import abslearning.tree.trace.SutTraceInterface;

public class LookaheadTraceStore implements LookaheadStore{
	private List<LookaheadTrace> lookaheadTraces;
	private boolean useSourcedLts;
	private LookaheadConcretizerProvider concretizerProvider;
	
	public LookaheadTraceStore(List<LookaheadTrace> initialLookaheadTraces, LookaheadConcretizerProvider concretizerProvider) {
		this.lookaheadTraces = initialLookaheadTraces;
		this.useSourcedLts = false;
		this.concretizerProvider = concretizerProvider;
	}
	
	public LookaheadTraceStore(LookaheadConcretizerProvider concretizerProvider) {
		this.lookaheadTraces = new ArrayList<LookaheadTrace>();
		this.concretizerProvider = concretizerProvider;
		this.useSourcedLts = false;
	}
	
	public void useSourceLts() {
		this.useSourcedLts = true;
	}
	

	/**
	 * Builds and adds guard lookahead trace to the store.
	 */
	public boolean updateFromCounterExample(Trace counterexample, Trace traceFromLosingTransition, Parameter refParam,
			int reemergingTransitionIndex) {
		try {
			ShiftingLookaheadTraceBuilder builder = new GuardLookaheadTraceBuilder().referring(refParam)
			.concreteLookaheadTrace(traceFromLosingTransition).ceTrace(counterexample);
		if (useSourcedLts) { 
			builder = new SourcedLookaheadTraceBuilder().basicBuilder((BasicLookaheadTraceBuilder)builder); 
		}
			
		SuffixClosedLtSetBuilder ltSetBuilder = new SuffixClosedLtSetBuilder(builder);
		if (reemergingTransitionIndex != -1) {
			// we cut off before the reemerging transation 
			ltSetBuilder.cutoff(reemergingTransitionIndex * 2 - 1);
		}
		
		LinkedHashSet<LookaheadTrace> lookaheadTracesToAdd = ltSetBuilder.build();
		
		// we remove the (useless) traces without ref param
		// TODO * maybe this could be made smarter
		lookaheadTracesToAdd.removeIf(lt -> lt.getReferringParam() == null);
		
		boolean addedNewTraces = addNewLookaheadTraces(lookaheadTracesToAdd);
		
		return addedNewTraces;
		}catch(BugException exception) {
			throw exception.addDecoration("ce", counterexample).
			addDecoration("traceFromLosingTransition", traceFromLosingTransition).
			addDecoration("refParam", refParam.toStringWithAction());
		}
	}

	/**
	 * Builds and adds a normal lookahead trace to the store.
	 */
	public boolean updateFromInconstencyTrace(Trace fullTrace, Trace inconsistencyTrace) {
		ShiftingLookaheadTraceBuilder builder = new BasicLookaheadTraceBuilder().concreteLookaheadTrace(inconsistencyTrace).ceTrace(fullTrace);
		if (useSourcedLts) { 
			builder = new SourcedLookaheadTraceBuilder().basicBuilder((BasicLookaheadTraceBuilder)builder); 
		}
		SuffixClosedLtSetBuilder ltSetBuilder = new SuffixClosedLtSetBuilder(builder);
		
		LinkedHashSet<LookaheadTrace> lookaheadTracesToAdd = ltSetBuilder.build();
		
		boolean addedNewTraces = addNewLookaheadTraces(lookaheadTracesToAdd);
		
		return addedNewTraces;
	}
	
	/**
	 * Builds a lookahead trace oracle.
	 */
	public LookaheadOracle buildLookaheadOracle(SutTraceInterface cachedSutOracleRunner, List<Integer> constants) {
		LookaheadTraceOracle lookaheadOracle = new LookaheadTraceOracle(cachedSutOracleRunner, this, this.concretizerProvider, constants);
		if (useSourcedLts) {
			lookaheadOracle.useSourceFilter();
		}
		return lookaheadOracle;
		
	}

	@Override
	public String logStateUpdate() {
		// print all lookahead traces
		List<LookaheadTrace> newLookaheadTraces = new LinkedList<LookaheadTrace>(this.lookaheadTraces);
		newLookaheadTraces.removeAll(this.prevLookaheadTraces);
		
		List<LookaheadTrace> removedLookaheadTraces = new LinkedList<LookaheadTrace>(this.prevLookaheadTraces);
		removedLookaheadTraces.removeAll(this.lookaheadTraces);
		
		String msg = ""; 
		
		if (!newLookaheadTraces.isEmpty())
			msg += this.logLookaheadTraces("new lookahead traces", newLookaheadTraces);
		if (!removedLookaheadTraces.isEmpty())
			msg += this.logLookaheadTraces("removed lookahead traces", removedLookaheadTraces);
		
		return msg;
	}
	
	protected List<LookaheadTrace> getLookaheadTraces() {
		return new ArrayList<LookaheadTrace>(this.lookaheadTraces);
	}
	
	private List<LookaheadTrace> prevLookaheadTraces = new ArrayList<LookaheadTrace>();
	
	private boolean addNewLookaheadTraces(LinkedHashSet<LookaheadTrace> lookaheadTracesToAdd) {
		this.prevLookaheadTraces = new ArrayList<LookaheadTrace>(this.lookaheadTraces);
		// first remove any traces already covered by the store
		lookaheadTracesToAdd.removeIf(lt -> this.isCoveredByCollection(this.lookaheadTraces, lt));
		
		// if all traces are covered, then we cannot add any, so ret false
		if (lookaheadTracesToAdd.isEmpty())
			return false;
		else {

			// if some traces are not covered, thus new, we remove any current lookahead traces that might be covered  
			this.lookaheadTraces.removeIf(lt -> this.isCoveredByCollection(lookaheadTracesToAdd, lt));
			this.lookaheadTraces.addAll(lookaheadTracesToAdd);
			return true;
		}
	}
	
	/**
	 * Checks if a lookahead is already covered by the collection of lookaheads. That is, by running the collection,
	 * we needn't run the lookahead trace.
	 */
	private boolean isCoveredByCollection(Collection<LookaheadTrace> lookaheadTraces, LookaheadTrace lookaheadTrace) {
		boolean isImplied = false;
		for (LookaheadTrace lt : lookaheadTraces) {
			if (lt.equals(lookaheadTrace) || lt.implies(lookaheadTrace)) {
				isImplied = true; break;
			}
		}
		
		return isImplied;
	}
	
	public String toString() {
		return logLookaheadTraces("lookaheadTraces", this.lookaheadTraces);
	}
	
	private String logLookaheadTraces(String label, List<LookaheadTrace> lookaheadTraces) {
		StringBuilder result = new StringBuilder();
		result.append("\n");
		
		result.append(label).append("\n");
		for ( LookaheadTrace lt :lookaheadTraces ) {
			result.append("   ");
			result.append(lt);
			result.append("\n");
		}	
		return result.toString();
	}
}
