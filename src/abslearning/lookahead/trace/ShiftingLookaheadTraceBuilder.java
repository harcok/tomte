package abslearning.lookahead.trace;

public interface ShiftingLookaheadTraceBuilder {
	public LookaheadTrace build();
	public boolean shift();
}
