package abslearning.lookahead.trace;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;

public class LookaheadTraceParam {
	private int parameterIndex;
	private LookaheadTraceAction abstractInputAction;
	private Relation relation;
	
	public int getParameterIndex() {
		return parameterIndex;
	}

	public LookaheadTraceAction getLookaheadInputAction() {
		return abstractInputAction;
	}

	
	
	// isReferingParameter only used for a lookahead for finding a memorized value used in a guard (input abstraction) 
	// isReferingParameter is set only true for the parameter in the guard  (in the input abstraction) 
	private boolean isReferingParameter = false;
	private LookaheadVariable variable;
	
	
	public LookaheadTraceParam(LookaheadVariable lookaheadVariable, Relation relationOverVariable, int parameterIndex,LookaheadTraceAction abstractInputAction){
		this.variable = lookaheadVariable;
		this.relation = relationOverVariable;
		this.parameterIndex = parameterIndex;
		this.abstractInputAction = abstractInputAction;
	}
	
	public LookaheadVariable getVariable() {
		return variable;
	}
	
	public LookaheadVariableType getVariableType() {
		return variable.getVariableType();
	}
	
	public int getVariableIndex() {
		return variable.getVariableIndex();
	}
	
	public Relation getRelation() {
		return relation;
	}
	
	public boolean isReferingParameter() {
		return isReferingParameter;
	}

	public void setReferingParameter() {
		if ( ! getVariableType().equals(LookaheadVariableType.l) ) {
			throw new BugException("Cannot make "+ this.toString() +" referring. You can only make a LookaheadTraceParam referring if type is l!  ");
		}
		this.isReferingParameter = true;
	}	

	@Override
	public String toString() {
		String strType;
		if ( isReferingParameter ) {
			strType=getVariableType().toString().toUpperCase();
		} else {
			strType=getVariableType().toString();
		}
		return "(" + strType + getVariableIndex() + "," + relation + ")";
	}
	
	/**
	 * Two parameters are similar if the type and index are equal. Note, 
	 * the isReferring boolean does not differentiate between parameters.
	 * 
	 * This allows us to check contains in a list of lookahead traces
	 * regardless of the referring nature of lookahead params. For example:
	 * [IACT1_L0,IACT2_f0].contains(IACT1.l0) == true
	 * 
	 */
	public boolean implies(LookaheadTraceParam param) {
		if (this == param) {
			return true;
		}
		
		if (!getVariable().equals(param.getVariable())) {
			return false;
		}
		
		if (relation != param.relation ) {
			return false;
		}
		
		if (!this.isReferingParameter && param.isReferingParameter == true) {
				return false;
		} 
		
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LookaheadTraceParam)) {
			return false;
		}
		
		LookaheadTraceParam that = (LookaheadTraceParam)obj;
		
		if (!getVariable().equals(that.getVariable())) {
			return false;
		}
		
		if (relation != that.relation ) {
			return false;
		}
		
		if (isReferingParameter != that.isReferingParameter) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		return variable.hashCode() * 3 + this.parameterIndex;
	}

	public int getInputActionIndex() {
		return abstractInputAction.getTraceIndex();
	}
}
