package abslearning.lookahead.trace;

public class LookaheadVariable {

	private LookaheadVariableType variableType;
	private int variableIndex;
	
	public LookaheadVariable(LookaheadVariableType variableType, int variableIndex){
		this.variableType = variableType;
		this.variableIndex = variableIndex;
	}
	
	public LookaheadVariableType getVariableType() {
		return variableType;
	}
	
	public int getVariableIndex() {
		return variableIndex;
	}
	

	public String toString() {
		return variableType.toString() + variableIndex;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + variableIndex;
		result = prime * result + ((variableType == null) ? 0 : variableType.hashCode());
		return result;
	}	
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LookaheadVariable other = (LookaheadVariable) obj;
		if (variableIndex != other.variableIndex)
			return false;
		if (variableType != other.variableType)
			return false;
		return true;
	}
}
