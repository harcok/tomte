package abslearning.lookahead.trace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//TODO change so lookahead traces are given in [lta1, lta2 ... ] format, that way no more hackey parsing is needed.
public class LookaheadTraceParser {
    private static int paramIndex;
    private static int actionIndex;
    private static boolean isReferring = false;
    private static int refParamIndex;
    private static int refActionIndex;
    
    
    public static List<LookaheadTrace> buildTraces(String traces) {
    	String[] splits = traces.split("\\n");
    	List<LookaheadTrace> lts = new ArrayList<LookaheadTrace> ();
    	for(String split : splits) {
    		LookaheadTrace lt = buildTrace(split.trim());
    		lts.add(lt);
    	}
    	return lts;
    }
    
    /**
     * Builds a lookahead trace from line. The line should be formatted:
     * actionLine := lta [-> lta]*  
     */
    public static LookaheadTrace buildTrace(String actionLine) {
    	String[] splits = actionLine.split("\\s*\\-\\>\\s*");
    	LookaheadTrace lt = buildTrace(Arrays.asList(splits));
    	return lt;
    }
    
    /**
     * Builds a lookahead trace from a list of strings, each representing an action. 
     */
    public static LookaheadTrace buildTrace(List<String> actionStrings) {
    	List<LookaheadTraceAction> actions = new ArrayList<LookaheadTraceAction>();
    	isReferring = false;
    	for(int i = 0; i < actionStrings.size(); i ++) {
    		actionIndex = i;
    		String actionString = actionStrings.get(i);
    		LookaheadTraceAction action = buildAction(actionString);
    		actions.add(action);
    		
    	}
    	LookaheadTrace trace = new LookaheadTrace(actions);
    	if(isReferring == true) {
    		trace.addReferringParam(refActionIndex, refParamIndex);
    	}
    	return trace;
    }
    
    /**
     * Builds a lookahead action from a string. The string should be formatted:
     * actionString := ltamethod[_ltaparam]*
     */
    public static LookaheadTraceAction buildAction(String actionString) {
    	String [] splits = actionString.split("_");
    	List<LookaheadVariable> variables = new ArrayList<LookaheadVariable> ();
    	String actionName = splits[0];
    	for(int i = 1; i < splits.length; i ++) {
    		paramIndex = i - 1;
    		LookaheadVariable var = buildVariable(splits[i]);
    		variables.add(var);
    	}
    	LookaheadTraceAction traceAction = new LookaheadTraceAction(actionName, variables, null);
    	return traceAction;
    }
    
    private static LookaheadVariable buildVariable(String varString) {
		String type = varString.substring(0, 1);
		String index = varString.substring(1, 2);
		LookaheadVariable variable = new LookaheadVariable(LookaheadVariableType.valueOf(type.toLowerCase()), Integer.valueOf(index));
		if(type.equals("L")) {
			refParamIndex = paramIndex;
			refActionIndex = actionIndex;
			isReferring = true;
		}
		return variable;
	}
}
