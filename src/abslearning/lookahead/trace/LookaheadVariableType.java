package abslearning.lookahead.trace;

import abslearning.exceptions.BugException;
import abslearning.exceptions.Messages;

public enum LookaheadVariableType {
	f, 
	c, 
	l,
	s, 
	i;	

	public static LookaheadVariableType fromChar(char c){
		
		switch(c){
		case'f':
			return LookaheadVariableType.f;
		case 'c':
			return LookaheadVariableType.c;
		case 'l':
			return LookaheadVariableType.l;
		case 's':
			return LookaheadVariableType.s;
		case 'i':
			return LookaheadVariableType.i;
		default:
			throw new BugException(Messages.UNKNOWN_LOOKAHEAD_PARAM_TYPE);
		}
		
	}

}


