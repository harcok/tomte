package abslearning.lookahead.trace;

import java.util.ArrayList;
import java.util.List;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;

public class LookaheadTraceAction {
	private String methodName;
	private List<LookaheadTraceParam> parameters;
	private int traceIndex;
	
	public LookaheadTraceAction(String methodName, List<LookaheadVariable> variables, List<Relation> relationOverVariables){
		if (variables.size() != relationOverVariables.size()) {
			throw new BugException("The number of variables should be equal to the number of relations over them");
		}
		this.methodName = methodName.intern();
		// store parameters in ArrayList for efficient indexing
		this.parameters = new ArrayList<LookaheadTraceParam>();
		int parameterIndex=0;
		for ( int index = 0; index < variables.size(); index ++) {
			this.parameters.add( new LookaheadTraceParam(variables.get(index), relationOverVariables.get(index),parameterIndex,this) ); 
			parameterIndex++;
		}
	}
	
	public LookaheadTraceAction(String methodName, List<LookaheadVariable> variables, List<Relation> relationOverVariables, 
			final List<LookaheadParamSignature> variableSourceParam) {
		this(methodName, variables, relationOverVariables);
//		this.parameters.forEach( 
//				param -> param.setSourceParameterSignature(variableSourceParam.get(param.getParameterIndex())));
	}
	
	public String getMethodName() {
		return this.methodName;
	}
	
	public List<LookaheadTraceParam> getParameters() {
		return this.parameters;
	}
	
    public LookaheadTraceParam getParam(int index){
    	return parameters.get(index);
    }
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(methodName);
		for (int i = 0; i < parameters.size(); i++) {
			result.append("_" + parameters.get(i).toString());
		}
		return result.toString();
	}	
	
	public boolean implies(LookaheadTraceAction action) {
		if ( !methodName.equals(action.methodName) )
			return false;
		
		if ( parameters.size() != action.parameters.size() ) 
			return false;
		
		for ( int paramIndex = 0; paramIndex < parameters.size(); paramIndex ++ ) {
			if ( !parameters.get(paramIndex).implies(action.parameters.get(paramIndex)) ) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof LookaheadTraceAction))
			return false;

		LookaheadTraceAction that = (LookaheadTraceAction) obj;

		if (!methodName.equals(that.methodName)) {
			return false;
		}

		if (!parameters.equals(that.parameters)) {
			return false;
		}

		return true;
	}
	
	@Override
	public int hashCode() {
		return methodName.hashCode() + parameters.hashCode();
	}

	public int getTraceIndex() {
		return traceIndex;
	}

	public void setTraceIndex(int traceIndex) {
		this.traceIndex=traceIndex;
	}
	
	public int size() {
		return this.parameters.size();
	}
}
