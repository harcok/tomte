package abslearning.lookahead.trace;

import java.util.LinkedHashMap;
import java.util.List;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.Parameter;

/**
 * Builds sourced lookahead traces, that is lookahead traces which also maintain the signatures (parameter types)
 * of the parameters referred by 'l' type variables.
 */
public class SourcedLookaheadTraceBuilder implements ShiftingLookaheadTraceBuilder{

	private BasicLookaheadTraceBuilder basicBuilder;

	public SourcedLookaheadTraceBuilder basicBuilder(BasicLookaheadTraceBuilder builder) {
		this.basicBuilder = builder; return this;
	} 
	

	public LookaheadTrace build() {
		LookaheadTrace ltTrace = this.basicBuilder.build();
		LinkedHashMap<LookaheadVariable, LookaheadParamSignature> sourceToSignature = extractSignature(ltTrace, 
				this.basicBuilder.getPrefix(), this.basicBuilder.getCLTrace());
		
		return new SourcedLookaheadTrace(ltTrace, sourceToSignature);
	}

	/**
	 * Builds a signature of the L lookahead variables. 
	 */
	private LinkedHashMap<LookaheadVariable, LookaheadParamSignature> extractSignature(LookaheadTrace ltTrace,
			Trace cLPrefix, Trace cLTrace) {
		List<InputAction> inputs = cLTrace.getConcreteInputActions();
		List<LookaheadTraceParam> lParams = ltTrace.getLookaheadParamsOfType(LookaheadVariableType.l);
		LinkedHashMap<LookaheadVariable, LookaheadParamSignature> varToSource = new LinkedHashMap<LookaheadVariable, LookaheadParamSignature>();
		for (LookaheadTraceParam lParam : lParams) {
			LookaheadVariable lVar = lParam.getVariable();
			if (!varToSource.containsKey(lVar)) {
				Parameter correspondingParam = inputs.get(lParam.getInputActionIndex()).getParam(lParam.getParameterIndex());
				Integer paramValue = correspondingParam.getIntegerValue();
				Parameter sourceParam = cLPrefix.getSourceParameter(paramValue);
				varToSource.put(lVar, new LookaheadParamSignature(sourceParam));
			}
		}
		return varToSource;
	}


	public boolean shift() {
		return this.basicBuilder.shift();
	}
}
