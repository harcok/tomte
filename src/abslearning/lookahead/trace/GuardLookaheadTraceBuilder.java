package abslearning.lookahead.trace;

import abslearning.trace.action.Action;
import abslearning.trace.action.Parameter;

public class GuardLookaheadTraceBuilder extends BasicLookaheadTraceBuilder{

	private Integer actionIndex;
	private Integer paramIndex;
	
	public GuardLookaheadTraceBuilder() {
		super();
		this.actionIndex = null;
		this.paramIndex = null;
	}  
	
	public GuardLookaheadTraceBuilder referring(Parameter refParam) {
		Action action = refParam.getAction();
		this.actionIndex = action.getTraceIndex()/2;
		this.paramIndex = refParam.getParameterIndex();
		return this;
	}
	
	public boolean shift() {
		boolean succ = super.shift();
		if (succ) {
			this.actionIndex --;
			// can shift only if the referring parameter index is still in the trace after the shift
			succ = this.actionIndex >= 0; 
		}
		return succ;
	}
	
	public LookaheadTrace build() {
		LookaheadTrace abstractLookaheadTrace = super.build();
		abstractLookaheadTrace.addReferringParam(this.actionIndex, this.paramIndex);
		return abstractLookaheadTrace;
	}
	
}
