package abslearning.lookahead.trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import abslearning.relation.Relation;
import abslearning.trace.Trace;
import abslearning.trace.action.Action;
import abslearning.trace.action.InputAction;
import sut.info.SutInfo;

public class BasicLookaheadTraceBuilder implements ShiftingLookaheadTraceBuilder{
	private Trace cLTrace;
	private SortedSet<Integer> seenValues;
	private LinkedHashSet<Integer> freshOutputValues;
	private Trace fullTrace;
	private List<Integer> constants;

	public BasicLookaheadTraceBuilder() {
		this.seenValues = new TreeSet<Integer>();
		this.freshOutputValues = new LinkedHashSet<Integer>();
		this.constants = SutInfo.getConstants();
	}
	
	
	public BasicLookaheadTraceBuilder concreteLookaheadTrace(Trace concreteLookaheadTrace) {
		this.cLTrace = concreteLookaheadTrace; 
		return this;
	}
	
	public BasicLookaheadTraceBuilder ceTrace(Trace fullTrace) {
		this.fullTrace = fullTrace;
		return this;
	}
	
	public LookaheadTrace build() {
		Trace traceBeforeLtTrace = getPrefix();
		this.seenValues = new TreeSet<Integer>(traceBeforeLtTrace.getAllDefinedIntegerValues());
		this.freshOutputValues = getFreshOutputValuesInTrace(cLTrace, seenValues);
		LookaheadTrace lookaheadTrace = buildLookaheadTrace(cLTrace, seenValues, freshOutputValues);
		return lookaheadTrace;
	}
	
	
	/**
	 * Left shifts by removing the starting transition of the concrete lookahead trace.
	 * @return true if shift successful, false if the concrete lookahead trace is empty.
	 */
	public boolean shift() {
		if (cLTrace.size() <= 2) {
			return false;
		} else {
			Action inputAction = cLTrace.get(0);
			Action outputAction = cLTrace.get(1);
			this.cLTrace = cLTrace.getSubTrace(2, cLTrace.size() - 1);
			ArrayList<Integer> moreSeenValues = new ArrayList<Integer>();
			moreSeenValues.addAll(inputAction.getConcreteParameters());
			moreSeenValues.addAll(outputAction.getConcreteParameters());
			seenValues.addAll(moreSeenValues);
			freshOutputValues.removeAll(moreSeenValues);
			return true;
		}
	}
	
	
	/**
	 * Builds a lookahead trace from a concrete trace with input and output values, the values seen before the trace
	 * and the output values in the trace that are fresh.
	 */
	private LookaheadTrace buildLookaheadTrace(Trace concreteLookaheadTrace, SortedSet<Integer> seenValues,
			LinkedHashSet<Integer> futureFreshOutputValues) {
		List<LookaheadTraceAction> ltActions = new ArrayList<LookaheadTraceAction>();
    	int freshParamCounter = 0;
    	
    	// create new lookahead trace from concrete Input Actions and earlier seen values
		//hash map that stores for a concrete Integer value the corresponding lookaheadParamType and lookaheadParamIndex,
		//e.g. f1, l1 or c1
		Map <Integer, LookaheadVariable> value2Variable = new HashMap <Integer,LookaheadVariable> ();
		Map <Integer, Relation> value2Relation = new HashMap <Integer, Relation>();
		Map <Integer, Integer> value2ParamIndex = new HashMap<Integer, Integer>();
		List<Integer> sourceValues = new ArrayList<Integer>(); 
		int globalParamIndex = 0; // used to make i parameters.
		List<Integer> indexedFutureFreshOutputValues = new ArrayList<Integer>(futureFreshOutputValues); // get() functionality
		
		//loop over all input actions in the concreteLookaheadTrace 
		//for(int transitionIndex = 0; transitionIndex <= concreteLookaheadTrace.getTransitionIndex(); transitionIndex++){
		for(InputAction lookaheadAction : concreteLookaheadTrace.getInputActions()){
			//InputAction lookaheadAction = concreteLookaheadTrace.getInputAction(transitionIndex*2);
			List<LookaheadVariable> variables = new ArrayList<LookaheadVariable>();
			List<Relation> relationsOverVariables = new ArrayList<Relation>();
			List<LookaheadParamSignature> sourceParams = new ArrayList<LookaheadParamSignature>();
			Integer sourceValue = null;
			
			//loop over all parameters in the lookaheadAction
			for(abslearning.trace.action.Parameter param : lookaheadAction.getParameters()){
				Integer paramValue = param.getIntegerValue();
				LookaheadVariableType varType=null;
				LookaheadVariable variable = null;
				Relation relationOverVariable = null;
				int varIndex=-1;
				if(value2Variable.containsKey(paramValue)) {
					variable = value2Variable.get(paramValue);
					varType = variable.getVariableType();
					varIndex = variable.getVariableIndex();
					relationOverVariable = value2Relation.get(paramValue);

				} else if ( constants.contains(paramValue) ) {
			    	//the parameter refers to a constant
			    	varType = LookaheadVariableType.c;
			    	varIndex = constants.indexOf(paramValue); 
			    	relationOverVariable = Relation.EQUAL;
			    } else {
					LinkedHashSet<Relation> relations = Relation.getRelations(seenValues, param.getIntegerValue());
					//TODO !! Analyze how picking a relation to build a ltp influences learning, when several are possible.  
//					if (relations.size() > 1) 
//						throw new ConfusionException("Between seenValues: " + seenValues + " and param  " + param.toStringWithAction() +" with value " +param);
					if(relations.size() >= 1) {
						//the concrete value stems from a seen value by way of a relation, 
						varType = LookaheadVariableType.l;
						//relationOverVariable = new ArrayList<Relation>(relations).get(rand.nextInt(relations.size()));
						//relationOverVariable = relations.iterator().next();
						relationOverVariable = first(relations);
						sourceValue = relationOverVariable.rev(param.getIntegerValue());
						
						// if the source of the value is not contained in the source of 
						if (!sourceValues.contains(sourceValue)) {
							sourceValues.add(sourceValue);
						}
						varIndex = sourceValues.size() - 1;
						
				//the concrete value is a fresh output generated when executing the lookahead trace
				//the param type is 's' TODO !! implement relations over 's' parameters
				} else {
					relations = Relation.getRelations(futureFreshOutputValues, param.getIntegerValue());
					if(relations.size() >= 1) {
						varType = LookaheadVariableType.s;
						relationOverVariable = relations.iterator().next();
						sourceValue = relationOverVariable.rev(param.getIntegerValue());
						varIndex = indexedFutureFreshOutputValues.indexOf(sourceValue);
				} else {
					relations = Relation.getRelations(value2Variable.keySet(), param.getIntegerValue());
					if(relations.size() >= 1) {
						varType = LookaheadVariableType.i;
						relationOverVariable = relations.iterator().next();
						// get the value of the referred lookahead parameter
						sourceValue = relationOverVariable.rev(param.getIntegerValue());
						// get the index for this parameter
						varIndex = value2ParamIndex.get(sourceValue);
					}
					
					//the value has not been seen before and it is not a constant -> it is fresh
					//the param type is 'f' (LookaheadParamType value 0 in the enumeration)
					//and added together with the index of the freshParamCounter to the hash map
					else {
						sourceValue = null; // no source value, it's fresh
						varType = LookaheadVariableType.f;
						varIndex = freshParamCounter;
						relationOverVariable = Relation.EQUAL;
						freshParamCounter++;
					}
				} 
				}	
				}
			    
				//create new lookahead variable and add it to the list
			    LookaheadVariable lookaheadVariable = new LookaheadVariable(varType, varIndex);
				variables.add(lookaheadVariable);
				// paramValue should not be contained by either of the two maps
				value2Relation.put(paramValue, relationOverVariable);
				value2Variable.put(paramValue, lookaheadVariable);
				value2ParamIndex.put(paramValue, globalParamIndex);
				relationsOverVariables.add(relationOverVariable);
				globalParamIndex ++;
			}
			//create new lookahead trace action and add it to the lookahead trace
			LookaheadTraceAction lookaheadTraceAction = new LookaheadTraceAction(lookaheadAction.getMethodName(),variables, relationsOverVariables, sourceParams);
			ltActions.add(lookaheadTraceAction); //updates also traceIndex
		}
		
		return new LookaheadTrace(ltActions);
	}
	

    private Relation first(LinkedHashSet<Relation> relations) {
    	return relations.iterator().next();
    }
    

	/**
	 * Returns all fresh values in the trace.
	 */
	private LinkedHashSet<Integer> getFreshOutputValuesInTrace(Trace concreteLookaheadTrace, Set<Integer> seenValues) {
		LinkedHashSet<Integer> freshOutputValues = new LinkedHashSet<Integer>();
		List<abslearning.trace.action.OutputAction> outputActions = concreteLookaheadTrace.getOutputActions();
		
		
		for(abslearning.trace.action.OutputAction output : outputActions) {
			ArrayList<Integer> outputParams = output.getConcreteParameters();
			List<Integer> paramsRelatedToSeenValues = Relation.getRelatedValuesTo(seenValues, outputParams);
			freshOutputValues.addAll(outputParams);
			freshOutputValues.removeAll(paramsRelatedToSeenValues);
		}
		
		freshOutputValues.removeAll(seenValues);
		
		return freshOutputValues;
	}
	

	Trace getCLTrace() {
		return this.cLTrace;
	}
	
	Trace getFullTrace() {
	    return this.fullTrace;
	}
	
	
	Trace getPrefix() {
		return fullTrace.getSubTrace(0, fullTrace.size() - this.cLTrace.size() - 1);
	}
	
}
