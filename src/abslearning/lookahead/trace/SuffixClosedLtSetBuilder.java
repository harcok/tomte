package abslearning.lookahead.trace;

import java.util.LinkedHashSet;

/**
 * Builds a suffixed closed set of lookahead traces
 */
public class SuffixClosedLtSetBuilder {
	private ShiftingLookaheadTraceBuilder builder;
	private Integer cutOff;

	public SuffixClosedLtSetBuilder(ShiftingLookaheadTraceBuilder builder) {
		this.builder = builder;
		this.cutOff = null;
	}
	
	/**
	 * Stops adding lookahead traces to the suffix after a certain number of them is added, 
	 * number specified by the cutOff. 
	 */
	public SuffixClosedLtSetBuilder cutoff(Integer cutOff) {
		this.cutOff = cutOff;
		return this;
	}
	
	/**
	 * @return ordered set of lts, in decremental order relative to length
	 */
	public LinkedHashSet<LookaheadTrace> build() {
		LinkedHashSet<LookaheadTrace> suffixClosedLookaheadList = new LinkedHashSet<>();
		if (cutOff == null)
			do {
				LookaheadTrace absLTrace = builder.build();
				suffixClosedLookaheadList.add(absLTrace);
			} while(builder.shift());
		else 
			do {
				LookaheadTrace absLTrace = builder.build();
				suffixClosedLookaheadList.add(absLTrace);
				cutOff --;
			} while(builder.shift() && cutOff > 0 );
		return suffixClosedLookaheadList;
	}
	
}
