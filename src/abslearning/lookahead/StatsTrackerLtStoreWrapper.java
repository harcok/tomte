package abslearning.lookahead;

import java.util.List;

import abslearning.app.Statistics;
import abslearning.trace.Trace;
import abslearning.trace.action.Parameter;
import abslearning.tree.trace.SutTraceInterface;

public class StatsTrackerLtStoreWrapper implements LookaheadStore{
	private LookaheadStore lookaheadStore;
	private Statistics statistics;
	
	public StatsTrackerLtStoreWrapper(LookaheadStore lookaheadStore, Statistics statistics) {
		this.lookaheadStore = lookaheadStore;
		this.statistics = statistics;
	}

	@Override
	public boolean updateFromCounterExample(Trace counterexample, Trace traceFromLosingTransition, Parameter refParam,
			int reemergingTransitionIndex) {
		boolean success = lookaheadStore.updateFromCounterExample(counterexample, traceFromLosingTransition, refParam, reemergingTransitionIndex);
		if (success) {
			statistics.storeLookaheadUpdateOnCE(this.lookaheadStore.logStateUpdate());
			statistics.incNumLookaheadUpdatesOnCE();
		}
		
		return success;
	}

	@Override
	public boolean updateFromInconstencyTrace(Trace fullTrace, Trace inconsistencyTrace) {
		boolean success = lookaheadStore.updateFromInconstencyTrace(fullTrace, inconsistencyTrace);
		if  (success) {
//			statistics.addDescriptionRun("\n\n Termination Reason: inconsistency in concrete tree found  => updated store => restart learning"
//					+ "\n" + lookaheadStore.logStateUpdate());
			statistics.incNumLookaheadUpdatesOnInconsistency();
			statistics.storeLookaheadUpdateOnInconsistency(lookaheadStore.logStateUpdate());
		}
		
		return success;
	}

	@Override
	public String logStateUpdate() {
		return lookaheadStore.logStateUpdate();
	}

	@Override
	public LookaheadOracle buildLookaheadOracle(SutTraceInterface cachedSutOracleRunner, List<Integer> constants) {
		return lookaheadStore.buildLookaheadOracle(cachedSutOracleRunner, constants);
	}
}
