package abslearning.lookahead;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

public class SingleCombinationGenerator implements LvCombinationsGenerator{
	public List<List<Integer>> generateLookaheadValueCombinations(LinkedHashSet<Integer> lookaheadValues,
			int lookaheadParams) {
		List<Integer> lookaheadValueList = new ArrayList<Integer>(lookaheadValues); 
		return Arrays.asList(lookaheadValueList);
	}
}
