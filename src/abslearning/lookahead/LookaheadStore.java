package abslearning.lookahead;

import java.util.List;

import abslearning.trace.Trace;
import abslearning.trace.action.Parameter;
import abslearning.tree.trace.SutTraceInterface;

/**
 * Interface for lookahead stores.  
 */
public interface LookaheadStore {
	/**
	 * Updates the lookahead store based on a counterexample trace which contains two related parameters
	 */
	public boolean updateFromCounterExample(Trace counterexample, Trace traceFromLosingTransition, Parameter refParam, int reemergingTransitionIndex) ;
	
	/**
	 * Updates the lookahead store based on a trace which exposed inconsistency in the case. 
	 * {@code seenValuesBeforeInconsistency} contains the values in inputs and outputs leading up
	 * to the trace.
	 * 
	 * @return true if state of the store, false if not 
	 */
	public boolean updateFromInconstencyTrace(Trace fullTrace, Trace inconsistencyTrace);
	
	
	/**
	 * Builds a lookahead oracle for the store.
	 */
	public LookaheadOracle buildLookaheadOracle(SutTraceInterface cachedSutOracleRunner, List<Integer> constants);
	
	/**
	 * Logs the current state of the lookahead store.
	 * @return 
	 */
	public String logStateUpdate();
}
