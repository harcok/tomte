package abslearning.lookahead;

import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.trace.FreshValueStoreBuilder;
import abslearning.trace.InputValueStoreBuilder;
import abslearning.trace.Trace;

public enum LookaheadConcretizerProvider {
	PERMUTATIVE,
	FIXED;
	
	public LookaheadConcretizer newConcretizerForTrace(Trace traceToLookaheadNode) {
		CanonicalValueStoreProvider freshInputValueStoreProvider = new InputValueStoreBuilder(traceToLookaheadNode);
		CanonicalValueStoreProvider freshOutputValueStoreProvider = new FreshValueStoreBuilder(traceToLookaheadNode);
		LvCombinationsGenerator generator = null; 
		switch(this) {
		case PERMUTATIVE: generator = new PermutationsGenerator(); break;
		case FIXED: generator = new SingleCombinationGenerator(); break;
			
		}
		
		return new LookaheadConcretizer(freshInputValueStoreProvider, freshOutputValueStoreProvider, generator);
	}
}
