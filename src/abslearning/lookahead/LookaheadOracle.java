package abslearning.lookahead;

import java.util.LinkedHashSet;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import util.Tuple2;

/**
 * Retrieves all memorable values after an input/output transition done from a
 * node in a tree.
 */
public interface LookaheadOracle {
	/**
	 * Runs the {@code input} after the trace {@code traceToParentNode} on the sut and gets the output. 
	 * Updates {@code traceToParentNode} with the input/output transition.
	 * Computes memorable values after this transition, from {@code input}, the output obtained and the 
	 * memorable values before the transition.
	 *    
	 */
	public Tuple2<OutputAction, LinkedHashSet<Integer>> doLookaheadForGuardAndOutput(LinkedHashSet<Integer> parentMemV, InputAction input, Trace traceToParent);
}
