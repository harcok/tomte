package abslearning.lookahead;

import java.util.LinkedHashSet;
import java.util.List;

public interface LvCombinationsGenerator {
	public List<List<Integer>>  generateLookaheadValueCombinations(LinkedHashSet<Integer> lookaheadValues,
			int numDifferentLookaheadParams);
}
