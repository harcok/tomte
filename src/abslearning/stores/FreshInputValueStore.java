package abslearning.stores;

import java.util.Collections;
import java.util.List;

public class FreshInputValueStore extends StepWiseValueStore implements CanonicalValueStore {
	private static int freshMax () { 
		return  freshMin() + 30 * step();
		}
	private static int freshMin() { 
		return  0; 
		};
	
	private static int next(List<Integer> allLearnerIntegerValues) {
	    Integer minFreshAfterLearnerValues;
	    if (!allLearnerIntegerValues.isEmpty()) {
	        Integer max = Collections.max(allLearnerIntegerValues);
	        minFreshAfterLearnerValues = Math.max((((int) max/step()) + 1) * step(), freshMin());
	    } else {
	        minFreshAfterLearnerValues = freshMin();
	    }
		return minFreshAfterLearnerValues; 
	}
	

	public FreshInputValueStore(FreshInputValueStore valueStoreMQ) {
		super(valueStoreMQ);
	}
	
	public FreshInputValueStore() {
		 super(freshMin(), freshMax(), Direction.ASCENDING);
	}
	
	// the values should generated should be greater than the max 
	public FreshInputValueStore(List<Integer> allLearnerIntegerValues) {
		super(next(allLearnerIntegerValues), 
				next(allLearnerIntegerValues) + freshMax(), Direction.ASCENDING);
	}
	
	public FreshInputValueStore(Integer minSteps, Integer maxSteps) {
		super(minSteps * step(), maxSteps * step(), Direction.ASCENDING);
	}
}
