package abslearning.stores;

import abslearning.exceptions.BugException;

public enum ConfigurableValueStoreProvider implements CanonicalValueStoreProvider{
	FRESH, 
	INPUT,
	BIGINPUT;

	public CanonicalValueStore newValueStore() {
		switch(this) {
		case FRESH:
			return new FreshOutputValueStore();
		case INPUT:
			return new FreshInputValueStore();
		case BIGINPUT: // used for RegisterAutomata hypothesis
			return new FreshInputValueStore(100000,100000 + StepWiseValueStore.defaultStep * 40);
		default:
			throw new BugException("Value store not defined for the provider " + this.name());
		}
	}
	
}
