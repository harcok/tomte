package abslearning.stores;

import java.util.EnumMap;
import java.util.Map.Entry;
import java.util.Random;

import abslearning.exceptions.BugException;

public class ProbabilityManager {
	private EnumMap<Event, Integer>  eventProbs;
	public ProbabilityManager( EnumMap<Event, Integer> eventProbs) {
		this.eventProbs = eventProbs;
	}
	
	public ProbabilityManager() {
		this(new EnumMap<Event, Integer>(Event.class));
	}
	
	public ProbabilityManager clone() {
		return new ProbabilityManager(new EnumMap<Event, Integer> (eventProbs));
	}
	
	public Event drawEvent(Random random) {
		Integer pick = (int)(100 * random.nextDouble()) + 1;
		Integer sum = 0;
		for (Entry<Event, Integer> eventEntry : eventProbs.entrySet()) {
			Integer eventProb = eventEntry.getValue();
			if (pick >= sum && pick <= sum + eventProb) {
				return eventEntry.getKey();
			}
			sum += eventProb;
		}
		throw new BugException("Could not draw").addDecoration("Probability Vector", this.eventProbs);
	}
	
	/**
	 * Add an event with a probability. The probability can be between 1 and 100. In case events 
	 * with non-zero probability are impossible, their associated probability is removed and the
	 * probabilities for all other events are redistributed uniformly.
	 */
	public ProbabilityManager addEvent(Event event, Integer prob) {
		eventProbs.put(event, prob);
		return this;
	}
	
	public ProbabilityManager removeEvent(Event event) {
		Integer eventProb = eventProbs.get(event);
		eventProbs.remove(event);
		Integer remainingEventProb = 100 - eventProb;
		redistributeProb(remainingEventProb);
		return this;
	}
	
	private void redistributeProb(Integer sumOfAllEventProbs) {
		if (eventProbs.isEmpty())
			return; 
		int index = 0;
		Integer sum = 0;
		for (Entry<Event, Integer> eventEntry : eventProbs.entrySet()) {
			// we use floor so if probabilities are shifted due to rounding trimming, they are done so upwards
			Integer newProb = Math.floorDiv(100 * eventEntry.getValue(), sumOfAllEventProbs);
			sum = newProb + sum;
			// we want the sum to always be 100
			if (index == eventProbs.size() -1) {
				Integer leftOver = 100 - sum;
				newProb += leftOver;
			}
			eventEntry.setValue(newProb);
			index ++;
		}
		
	}
	
	public boolean matches(ProbabilityManager probManager) {
		if (this.eventProbs.size() != probManager.eventProbs.size()) {
			return false;
		}
		for (Event event : this.eventProbs.keySet()) {
			if (!this.eventProbs.get(event).equals(probManager.eventProbs.get(event))) {
				return false;
			}
		}
		return true;
	}
	
	public String toString() {
		return "Probability Vector: " + this.eventProbs;
	}
 	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventProbs == null) ? 0 : eventProbs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProbabilityManager other = (ProbabilityManager) obj;
		if (eventProbs == null) {
			if (other.eventProbs != null)
				return false;
		} else if (!eventProbs.equals(other.eventProbs))
			return false;
		return true;
	}
}
