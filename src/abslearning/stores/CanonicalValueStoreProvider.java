package abslearning.stores;

public interface CanonicalValueStoreProvider {
	public CanonicalValueStore newValueStore();
}
