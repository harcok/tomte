package abslearning.stores;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import sut.info.ActionSignature;
import sut.info.SutInfo;
import abslearning.ceanalysis.AbstractionPair;
import abslearning.exceptions.BugException;
import abslearning.predicates.InputActionPredicate;
import abslearning.predicates.InputParameterPredicate;
import abslearning.trace.AbstractionVariable;
import abslearning.trace.action.Action;
import abslearning.trace.action.InputAction;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.AlphabetImpl;
import de.ls5.jlearn.shared.SymbolImpl;

public class PredicateStore {
	private static final Logger logger = Logger.getLogger(PredicateStore.class);
	
	private Map<String, InputActionPredicate> inputActionPredicates = new LinkedHashMap<String, InputActionPredicate>();
	
	
	
    // singleton
    // ---------
    private static final PredicateStore INSTANCE = new PredicateStore();

    public static PredicateStore getInstance() {
        return INSTANCE;
    }

    // constructor cannot be called because private => we can only get single
    private PredicateStore() {
    }	
	
    //-----------------------------------------------------------------------------
    
    
	public void createInputPredicate(ActionSignature signature) {
		InputActionPredicate value = new InputActionPredicate(signature.getParameterTypes().size());
		inputActionPredicates.put(signature.getMethodName(), value);
	}
		
	public void clearAllInputActionPredicates() {
		for (InputActionPredicate predicate : inputActionPredicates.values()) {
			predicate.clearPredicates();
		}
	}
	
	public Symbol addInputPredicate(List<AbstractionPair> abstractionsForAction) {
		if (abstractionsForAction.isEmpty()) {
			throw new BugException("Cannot create new predicate from an empty list of abstractions");
		}
		
		String actionName = abstractionsForAction.get(0).parameter.getAction().getMethodName();
		InputActionPredicate actionPredicate = inputActionPredicates.get(actionName);
		
		for (AbstractionPair abstractionPair : abstractionsForAction) {
			
		}
		return null;
	}
	
	/**
	 * Adds a new abstraction to the set of abstractions for the parameter. 
	 * @return symbol containing the abstraction or null if the predicate store already
	 * contains the parameter abstraction. 
	 */
	public Symbol addInputPredicate(Action action, Integer paramIndex, AbstractionVariable newAbstraction) {
		String methodName = action.getMethodName();
		Symbol newSymbol;
		
		// add input predicate
		boolean successfullyAdded = inputActionPredicates.get(methodName).addPredicate(paramIndex, newAbstraction);
		
		if (!successfullyAdded) {
			// in case the abstraction variable was already found no new symbol is generated
			newSymbol = null;
		} else {
			// log message
			int abstrIndex= inputActionPredicates.get(methodName).indexOf(paramIndex, newAbstraction);
			logger.info("add inputPredicate: for " +  methodName + " param p"+ paramIndex + " add abstraction with index " + abstrIndex);
	
	
			
			//create new input symbol and return it to the mapper
			// 1. create copy of action
			Action updatedAction = new InputAction(action);
			// 2. update abstract parameter value in action
			List<Integer> abstractValues = updatedAction.getAbstractParameters();		
			abstractValues.set(paramIndex, abstrIndex);
			updatedAction.setAbstractParameters(abstractValues);
			// 3. create new symbol from abstraction string of action
			newSymbol = new SymbolImpl(updatedAction.getAbstractionString());
			
			logger.fatal("New symbol: " + newSymbol);
		}
		
		return newSymbol;
	}
	
	
	//-----------------------------------------------------------------------------------	
	
	public AbstractionVariable getInputAbstraction(String methodName, int parameterIndex,int abstractionIndex) {
		return inputActionPredicates.get(methodName).get(parameterIndex).get(abstractionIndex);
	}
	
	public InputParameterPredicate getInputAbstractions(String methodName, int parameterIndex) {
		return inputActionPredicates.get(methodName).get(parameterIndex);
	}
	
	//-----------------------------------------------------------------------------------	
		
	public InputActionPredicate getInputActionPredicate(String methodName) {
		return inputActionPredicates.get(methodName);
	}


	
	//-----------------------------------------------------------------------------------	
		
	
	private Alphabet inputAlphabet;

	
	public Alphabet getInputAlphabet() {
		return inputAlphabet;
	}


/**
 * Generates alphabet from input predicates, ex: <br/>
 * 	LOG([]) -> [LOG_-1] <br/>
 * 	LOG([x1]) -> [LOG_-1, LOG_0] <br/>
 *  LOG([x1,x2]) -> [LOG_-1, LOG_0, LOG_1] <br/>
 *  LOG([x1],[x2]) -> [LOG_-1_-1, LOG_-1_0, LOG_0_-1, LOG_0_0] 
 */
	public void generateInputAlphabet() {
		Alphabet result = new AlphabetImpl();

		for (ActionSignature sig : SutInfo.getInputSignatures()) {
			List<String> currentAlpha = new ArrayList<String>();
			currentAlpha.add(sig.getMethodName());
			logger.debug("sig.getMethodName(): " + sig.getMethodName());
			InputActionPredicate inputActionPredicate = getInputActionPredicate(sig.getMethodName());
			for (int j = 0; j < inputActionPredicate.size(); j++) {
				InputParameterPredicate paramPred = inputActionPredicate.get(j);
				List<String> newAlpha = new ArrayList<String>();

				for (String currentSymbol : currentAlpha) {
					if (paramPred.size() > 0) {
						for (int i = 0; i < paramPred.size() + 1; i++) {
							newAlpha.add(currentSymbol + "_" + (i-1));
						}
					} else {
						newAlpha.add(currentSymbol + "_" + -1);
					}
				}
				currentAlpha = newAlpha;
			}
			for (String currentSymbol : currentAlpha) {
				result.addSymbol(new SymbolImpl(currentSymbol));
			}
		}		
		this.inputAlphabet=result; 
	}

	public String toString() {
	    return this.inputActionPredicates.toString();
	}
	
}
