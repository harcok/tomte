package abslearning.stores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import abslearning.exceptions.ConfigurationException;
import abslearning.exceptions.Messages;

/**
 * A random ranged value store which dishonestly implements the 
 * CanonicalValueStore interface. 
 */
public class RandomRangedValueStore  implements CanonicalValueStore {
	// some static initialization variables 
	static private Random random;
	private static int minValue;  
	private static int maxValue;
	private static Logger logger = Logger.getLogger(RandomRangedValueStore.class); 
	private static List<Integer> constants=new ArrayList<Integer>(); 
	private LinkedList<Integer> randomFreshValues; // shuffled list of integers from minValue to maxValue excluding the constants
	private static boolean isExtendable;
	
	
	public static void setFreshValueRange(int minValue,int maxValue, boolean isExtendable) {
		RandomRangedValueStore.minValue = minValue;
		RandomRangedValueStore.maxValue = maxValue;
		RandomRangedValueStore.isExtendable = isExtendable;
	}
	
	public RandomRangedValueStore(Random random) {
		
		randomFreshValues=generateFreshValues(constants, minValue, maxValue);		
		Collections.shuffle(randomFreshValues, random); // shuffled inline!
	}
	
	/**
	 * Returns the first element of the shuffled freshValues array list and removes the value from the list
	 * .
	 * If no fresh values are available a ConfigurationException("No fresh values available! Increase range.") is thrown.
	 * @return First element of the shuffled freshValues array list; if no fresh values are available a ConfigurationException is thrown
	 */
	private Integer popRandomFreshValue() {
		if(randomFreshValues.isEmpty()) {
			if (isExtendable) {
				int range = maxValue - minValue;
				int previousMaxValue = maxValue;
				maxValue = (int) (maxValue + range * 1.5 + 1);
				randomFreshValues = generateFreshValues(Collections.<Integer> emptyList(), previousMaxValue, maxValue);
				logger.fatal("Fresh Value ranged increased to: " + minValue + " - " + maxValue);
				Collections.shuffle(randomFreshValues, random);
			} else {
				throw new ConfigurationException(Messages.RANGE_ERROR);
			}
		}
			
		Integer freshValue = randomFreshValues.pop();
		return freshValue;			
	}
	
	/**
	 * Generate fresh values 
	 *   as list of integers from minValue to maxValue excluding the constants . 
	 */
	private LinkedList<Integer> generateFreshValues(List<Integer> constants, int minValue, int maxValue ) {
		LinkedList<Integer> freshValues = new LinkedList<Integer>();
		for (int i = minValue; i < maxValue; i++) {
			if (!constants.contains(new Integer(i))) {
				freshValues.add(new Integer(i));
			}
		}	
		return freshValues;
	}

	@Override
	public Integer popFreshValue() {
		return popRandomFreshValue();
	}

	@Override
	public void prependFreshValue(Integer integerValue) {
		randomFreshValues.add(integerValue);
	}

	public static void setConstants(List<Integer> constants) {
		RandomRangedValueStore.constants.addAll(constants);
	}	

}
