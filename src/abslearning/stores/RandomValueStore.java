package abslearning.stores;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

public class RandomValueStore implements StochasticValueStore{
	private static final Logger logger = Logger.getLogger(RandomValueStore.class);
	
	// statevars set according to situation in hyp => then can choose statevars with more likelyhood than  other values
	private Collection<Integer> statevars;
	private CanonicalValueStore freshValueStore;

	private static ProbabilityManager refManager = new ProbabilityManager();
	{
		refManager.addEvent(Event.DRAW_STATEVAR, 40);
		refManager.addEvent(Event.DRAW_PREVPARAM, 30);   
		refManager.addEvent(Event.DRAW_CONSTANT, 10); 
		refManager.addEvent(Event.DRAW_HISTORY, 20);
		//refManager.addEvent(Event.DRAW_FRESH, 10);   
		// we exclude the draw fresh event and draw this event separately with a fixed probability 
	}
	
	private final List<Integer> constants; 
	
	private HashSet<Integer> history;

	private Double drawFreshProbability;

	private Random random;  
	
	public RandomValueStore(List<Integer> constants, CanonicalValueStore freshValueStore, Double drawFreshProbability, Random random) {	
		this.drawFreshProbability = drawFreshProbability;
		//valueSetToPickFrom = new ArrayList<Integer>();
		//if (useConstants) valueSetToPickFrom.addAll(constants);
		
		this.history=new HashSet<Integer>();
		this.freshValueStore = freshValueStore;
		this.random = random;
		this.constants = constants;
	}
	
	
	// public methods to use
	//-----------------------
		
	public void updateStateVars(Collection<Integer> collection) {
		this.statevars=collection;
	}
	
	// TODO extremely ugly, should be fixed. 
	public void removeFromHistory(Collection<Integer> values) {
		this.history.removeAll(values);
	}
	
	/**
	 * Draws a fresh value with the probability {@code drawFreshProbability} if there are
	 * values in the history or with prob 1 if there aren't. 
	 * If a fresh value isn't drawn, it draws a history value or constant with configurable
	 * probabilities that are redistributed (in case the set of candidate values 
	 */
	public Integer getValue(List<Integer> excludeValues,List<Integer> prevParamValues) {
		HashSet<Integer> knownValuePool = new HashSet<Integer>(history);
		knownValuePool.removeAll(constants);
		knownValuePool.removeAll(excludeValues);
		if (random.nextDouble() < this.drawFreshProbability || knownValuePool.isEmpty()) {
			Integer picked = this.freshValueStore.popFreshValue();
			history.add(picked);
			return picked;
		}
		
		RandomValuePicker picker = new RandomValuePicker(random);
		picker.addElementsToExclude(excludeValues);
		
		ProbabilityManager probabilityManager = refManager.clone();
		
		SetBuilder setBuilder = new SetBuilder().excludeValues(excludeValues).excludeValues(constants);
		
		// building our selection sets
		Set<Integer> stateVarsSelection = setBuilder.addValues(statevars).buildCopy();
		Set<Integer> prevParamSelection = setBuilder.excludeValues(statevars).addValues(prevParamValues).buildCopy();
		Set<Integer> historySelection = setBuilder.addValues(history).buildCopy();
		
		
		// optimization remove events for which no draw can be made
		removeEventIfEmpty(probabilityManager,  statevars, Event.DRAW_STATEVAR);
		removeEventIfEmpty(probabilityManager,  prevParamSelection, Event.DRAW_PREVPARAM);
		removeEventIfEmpty(probabilityManager,  constants, Event.DRAW_CONSTANT);
		removeEventIfEmpty(probabilityManager, historySelection, Event.DRAW_HISTORY);
		
		
		Integer picked = null;
		
		while (picked == null) {
			Event eventDrawn = probabilityManager.drawEvent(random);
			
			switch(eventDrawn) {
			case DRAW_STATEVAR: 
				picked = picker.pick(stateVarsSelection); break;
			case DRAW_PREVPARAM:
				picked = picker.pick(prevParamSelection); break;
			case DRAW_CONSTANT:
				picked = picker.pick(constants, false); break; // for constants, we only pick values equal to them
			case DRAW_HISTORY:
				picked = picker.pick(historySelection); break;
			default: //DRAW_FRESH
				picked = this.freshValueStore.popFreshValue();
			}
			
			if (picked == null) 
				probabilityManager.removeEvent(eventDrawn);
		}
		
		history.add(picked);
		return picked;
	}
	
	private void removeEventIfEmpty(ProbabilityManager mgr, Collection<Integer> selectionSet, Event event) {
		if (selectionSet.isEmpty())
			mgr.removeEvent(event);
	}
	
	static class SetBuilder {
		public Set<Integer> values;
		public Set<Integer> valuesExcluded;
 		public SetBuilder() {
			values = new HashSet<Integer>();
			valuesExcluded = new HashSet<Integer>();
		}
		
		SetBuilder addValues(Collection<Integer> values) {
			values.forEach(val -> {if (val != null) { this.values.add(val);}});
			return this;
		}
		
		SetBuilder removeValues(Collection<Integer> values) {
			this.values.removeAll(values);
			return this;
		}
		
		Set<Integer> buildCopy() {
			HashSet<Integer> copy = new HashSet<Integer>(values);
			copy.removeAll(valuesExcluded);
			return copy;
		}
		
		/**
		 * Excluding differs from removing in that values are excluded
		 * indefinitely from the generated copies.
		 */
		SetBuilder excludeValues(Collection<Integer> values) {
			this.valuesExcluded.addAll(values);
			return this;
		}
	}

	/**
	 * Adds a list of values to the selection list of values  
	 * eg. newly found fresh outputs can be added
	 * @param values the set of values added.
	 */
	public void addValues(List<Integer> values) {
		if(values.isEmpty() == false) {
			logger.debug("Added " + values);
		}
		history.addAll(values);
	}

}
