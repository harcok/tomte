package abslearning.stores;

public interface CanonicalValueStore extends ValueStore {

	Integer popFreshValue();

	void prependFreshValue(Integer integerValue);

}
