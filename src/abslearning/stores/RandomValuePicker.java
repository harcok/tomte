package abslearning.stores;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import abslearning.relation.Relation;

//TODO * implement weighter random picker
public class RandomValuePicker {
	private Set<Integer> elementsToExclude;
	private Random random;

	public RandomValuePicker(Random random) {
		this.elementsToExclude = new HashSet<Integer>();
		this.random = random;

	}

	public void addElementsToExclude(Collection<Integer> elements) {
		elementsToExclude.addAll(elements);
	}

	public Integer pick(Collection<Integer> fromValues) {
		return pick(fromValues, true);
	}
	
	public Integer pick(Collection<Integer> fromValues, boolean pickRandomlyRelated) {
		Integer pickedValue = null;
		if (!fromValues.isEmpty()) {
			LinkedList<Integer> selectionPool = new LinkedList<Integer>(fromValues);
			selectionPool.removeAll(elementsToExclude);
			if (!selectionPool.isEmpty()) {
				pickedValue = pickRandRelatedToValueFromList(selectionPool);
				if (pickRandomlyRelated)
					pickedValue = pickARandomlyRelatedValue(pickedValue);
			}
		}

		return pickedValue;
	}

	private Integer pickRandRelatedToValueFromList(List<Integer> values) {
		Integer pick = null;
		int size = values.size();
		if (size > 0) {
			pick = values.get(random.nextInt(size));
		}

		return pick;
	}

	private Integer pickARandomlyRelatedValue(Integer value) {
		Relation[] relations = Relation.getActiveRelations();
		int randIndex = random.nextInt(relations.length);
		return relations[randIndex].map(value);
	}
}