package abslearning.stores;

import java.util.List;

public interface StochasticValueStore extends ValueStore {
	
	/**
	 * getValue 
	 * 
	 *   get value from store where value may not be one of the
	 *   values in excludeValues
	 */
	public Integer getValue(List<Integer> excludeValues,List<Integer> prevParamValues);

	/**
	 * adds a list of values to the value selection list
	 * eg. newly found fresh outputs can be added
	 */
	public void addValues(List<Integer> values);
}
