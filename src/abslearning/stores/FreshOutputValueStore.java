package abslearning.stores;

import java.util.Collections;
import java.util.List;

public class FreshOutputValueStore extends StepWiseValueStore {
	private static int freshMax () { 
		return  (-1) * 10 * step();
		}
	private static int freshMin() { 
		return  freshMax() - 30 * step();
		};
	
	private static int next(List<Integer> allLearnerIntegerValues) {
	    Integer maxFreshAfterLearnerValues;
	    if (!allLearnerIntegerValues.isEmpty()) {
	        Integer min = Collections.min(allLearnerIntegerValues);
	        maxFreshAfterLearnerValues = min > 0 ? freshMax() : (((int) min/step()) - 1) * step(); 
	    } else {
	        maxFreshAfterLearnerValues = freshMax();
	    }
		return maxFreshAfterLearnerValues; 
	}

	public FreshOutputValueStore() {
		 super(freshMin(), freshMax(), Direction.DESCENDING);
	}
	
	public FreshOutputValueStore(List<Integer> allOutputIntegerValues) {
		super(next(allOutputIntegerValues) + freshMin(), 
				next(allOutputIntegerValues), Direction.DESCENDING);
	}
	
	public FreshOutputValueStore(FreshOutputValueStore freshOutputValueStore) {
		super(freshOutputValueStore);
	}
	
}
