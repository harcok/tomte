package abslearning.stores;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import abslearning.exceptions.ConfigurationException;
import abslearning.exceptions.Messages;
import sut.info.SutInfo;

public abstract class StepWiseValueStore implements CanonicalValueStore{

	public static enum Direction {
		ASCENDING,
		DESCENDING
	}
	
	public static int step() {
		return defaultStep;
	}
	
	static int defaultStep=1;
	
	// default values which are used for the general MQ store
	static int defaultMinValue=1;  
	static int defaultMaxValue=defaultStep * 20; 
	static Direction defaultDirection= Direction.ASCENDING;
	static boolean isExtendable = true;
	
	public static void setDefaultStep(int step) {
		defaultStep = step;
	}
	
	private List<Integer> freshValues;
	private int minValue;
	private int maxValue;
	private int step;
	private Direction direction;

	public StepWiseValueStore() {
		freshValues=generateFreshValuesForDefault(SutInfo.getConstants());
		setFieldsToDefault();
	}
	
	public StepWiseValueStore(StepWiseValueStore valueStoreMQ) {
		freshValues = new LinkedList<Integer>(valueStoreMQ.freshValues);
		setFields(valueStoreMQ.minValue, valueStoreMQ.maxValue, defaultStep, valueStoreMQ.direction);
	}
	
	protected StepWiseValueStore(int minValue, int maxValue, Direction direction) {
		freshValues = generateFreshValues(SutInfo.getConstants(), minValue, maxValue, defaultStep, direction);
		setFields(minValue, maxValue, defaultStep, direction);
	}
		
	protected void setFieldsToDefault() {
		setFields(defaultMinValue, defaultMaxValue, defaultStep, defaultDirection);
	}
	
	protected void setFields(int minValue, int maxValue, int step, Direction direction) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.step = step;
		this.direction = direction;
	}
	
	/**
	 * Generate fresh values 
	 *   as list of integers from minValue to maxValue excluding the constants . 
	 */
	private List<Integer> generateFreshValues(List<Integer> constants, int minValue, int maxValue, int step, Direction direction) {
		List<Integer> freshValues = new LinkedList<Integer>();
		Integer minConst = constants.isEmpty() ? null : Collections.min(constants);
		Integer maxConst = constants.isEmpty() ? null : Collections.max(constants);
		if (direction == Direction.ASCENDING) {
			for (int i = minValue; i < maxValue; i += step) {
				if (minConst == null || i > maxConst) {
					freshValues.add(new Integer(i));
				}
			}	
		} else {
			for (int i = maxValue; i > minValue; i -= step) {
				if (minConst == null || i < minConst) {
					freshValues.add(new Integer(i));
				}
			}
		}
		return freshValues;
	}		
	
	private List<Integer> generateFreshValuesForDefault(List<Integer> constants) {
		return generateFreshValues(constants, defaultMinValue, defaultMaxValue, defaultStep, defaultDirection);
	}

	private Integer getFreshValue() {
		if (freshValues.isEmpty()) { 
			if (isExtendable) {
				int range = maxValue - minValue;
				if (direction == Direction.ASCENDING) {
					minValue = maxValue;
					maxValue = (int) (maxValue + range * 1.5 + 1);
				} else {
					maxValue = minValue;
					minValue = (int) (minValue - range*1.5 - 1);
				}
				freshValues = generateFreshValues(Collections.<Integer> emptyList(), minValue, maxValue, step, direction);
			} else {
				throw new ConfigurationException(Messages.RANGE_ERROR);
			}
		}
		return freshValues.get(0);
	}
	

	// public methods to use
	//-----------------------
	
	// used in:
	//  - abslearning.trace.Trace.canonicalize()
	//  - abslearning.mapper.AbstractionMapper.getNewValue(InputAction, Trace, InputParameterPredicate, boolean)
	//  - abslearning.mapper.Mapper.findAbstractionAndOrLookaheadTrace(Trace, Trace) 
	public Integer popFreshValue() {
		Integer result = getFreshValue();
		freshValues.remove(result);
		return result;
	}
	
	// used only in : abslearning.mapper.AbstractionMapper.concretizeInput(InputAction, boolean, Trace)
	public void prependFreshValue(Integer value) {
	   freshValues.add(0, value);
	}	
	
	public static int getDefaultStep() {
		return defaultStep;
	}
}
