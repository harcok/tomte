package abslearning.stores;

public enum Event {
DRAW_STATEVAR,
DRAW_PREVPARAM,
DRAW_CONSTANT,
DRAW_HISTORY,
DRAW_FRESH;
}
