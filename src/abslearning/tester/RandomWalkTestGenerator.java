package abslearning.tester;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import abslearning.trace.action.InputAction;
import util.learnlib.AutomatonUtil;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.SymbolImpl;

public class RandomWalkTestGenerator implements TestGenerator{

	private int maxTestLength;
	private int minTestLength;
	private Random random;
	private Automaton hyp;
	private double stopProbability;

	/**
	 *
	 * @param minTestLength  : minimal test length
	 * @param avgNumStepsAfterMinLength  : average steps after done minimal test length
	 * @param maxTestLength  : maximal test length
	 * @param random
	 */
	public RandomWalkTestGenerator(int minTestLength, int avgNumStepsAfterMinLength, int maxTestLength, long seed) {
		this.minTestLength = minTestLength;
		this.stopProbability = ((double)1/avgNumStepsAfterMinLength);
		this.maxTestLength = maxTestLength;
		this.random =  new Random(seed);
	}

	public void initialize(Automaton hyp) {
		this.hyp = hyp;
	}


	public List<InputAction> nextTest() {
		List<InputAction> abstractInputActions = new ArrayList<InputAction>();

		State current=hyp.getStart();
		List<Symbol> abstractInputs=hyp.getAlphabet().getSymbolList();

	    for (int i = this.minTestLength; i < this.maxTestLength; i ++) {

		    Symbol abstractInput=null;
		    Symbol abstractOutput=null;
	    	 // draw randomly an input in reduced hyp which is enabled in current state
	    	 // note: if not enabled we get output=null
	    	 while (! AutomatonUtil.isDefined(abstractOutput)) {
	    	    abstractInput=abstractInputs.get(random.nextInt(abstractInputs.size()));
	    	    abstractOutput = current.getTransitionOutput(abstractInput);
	    	 }

	    	 // do transition in reduced hyp
	    	 current = current.getTransitionState(abstractInput);
	    	 if ( abstractInput != null )  abstractInputActions.add(new InputAction(abstractInput.toString()));
	    	 if (random.nextDouble() < this.stopProbability) {
	    		 break;
	    	 }

	    }

	    return abstractInputActions;
	}

	public void terminate() {
	}
}
