package abslearning.tester;

import java.util.List;

import org.apache.log4j.Logger;

import util.Tuple2;
import de.ls5.jlearn.interfaces.Automaton;

import abslearning.app.Statistics;
import abslearning.learner.Run;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

public class LastCeTester implements HypTester{

	private Run runner;
	private Statistics statistics;
	private Logger logger = Logger.getLogger(LastCeTester.class);

	public LastCeTester(Run run) {
		this.runner = run;
		this.statistics = Statistics.getInstance();
	}

	@Override
	public Tuple2<Trace, OutputAction> testHyp(int hypCounter) {
		Tuple2<Trace, OutputAction> cexPair = null;

		if (statistics.getLastCe() != null) {
			List<InputAction> lastCe = statistics.getLastCe().getConcreteInputActions();
			cexPair = runner.checkIsCounterExample(lastCe);
			if (cexPair != null) {
				logger.fatal("REUSE COUNTEREXAMPLE!!");
				statistics.incNumberOfReusedCounterExamples();
			} else {
				// unset lastCe which is proven not to be ce anymore
				// and continue with testing
				statistics.setLastCe(null);
			}
		}
		return cexPair;
	}


}
