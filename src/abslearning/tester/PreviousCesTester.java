package abslearning.tester;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.ceanalysis.CeStore;
import abslearning.learner.Run;
import abslearning.trace.Trace;
import abslearning.trace.action.OutputAction;
import de.ls5.jlearn.interfaces.Automaton;
import util.Tuple2;

/**
 * Checks if counterexamples found in previous runs are counterexamples
 * for the current run. This can happen because learning can be restarted,
 * in which case the counterexamples before the restart are likely still
 * counterexamples for the new learning run. 
 * 
 */

//TODO ! We should cache these tests.
public class PreviousCesTester implements HypTester{

	private Run runner;
	private Statistics statistics;
	private Logger logger = Logger.getLogger(PreviousCesTester.class);
	private CeStore ceStore;
	

	public PreviousCesTester(Run run, CeStore ceStore) {
		this.runner = run;
		this.ceStore = ceStore;
		this.statistics = Statistics.getInstance();
	}

	@Override
	public Tuple2<Trace, OutputAction> testHyp(int hypCounter) {
		Tuple2<Trace, OutputAction> cexPair = null;
		for (Trace ceTrace : ceStore.getCes()) {
			cexPair = runner.checkIsCounterExampleCached(ceTrace.getConcreteInputActions());
			if (cexPair != null) {
				logger.fatal("REUSE COUNTEREXAMPLE!!");
				statistics.incNumberOfReusedCounterExamples();
				break;
			} 
		}
		return cexPair;
	}

	
}
