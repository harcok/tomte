package abslearning.tester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import abslearning.determinizer.Canonicalizer;
import abslearning.exceptions.BugException;
import abslearning.exceptions.ConfigurationException;
import abslearning.learner.Run;
import abslearning.trace.Trace;
import abslearning.trace.TraceBuilder;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import util.Tuple2;

public class TraceTester implements HypTester{
    private static final Logger logger = Logger.getLogger(TraceTester.class);

	private List<Trace> testTraces;
    private Run run;


	public  TraceTester(Run run, List<String> testStrings) {
		this.run = run;
		this.testTraces = testStrings.stream()
				.map(ts -> toTrace(run, ts, true))
				.collect(Collectors.toList());
	}
	
	// transforms test strings to actual sut traces. Accepts test strings encoded as either input action sequences or as traces (traces also contain the expected output for each input). 
	// In either case it applies canonization. In case strings describe outputs, after canonization the outputs are optionally verified on the SUT. 
    private Trace toTrace(Run runner, String testString, boolean verifyTrace) {
		Trace trace = null;
		if (TraceBuilder.isStringInputSequence(testString)) {
			List<InputAction> inpActions = TraceBuilder.buildInputActionList(testString);
			Trace sutTrace = runner.runOnConcreteSut(inpActions);
			trace = new Canonicalizer().canonicalize(sutTrace);
		} else {
			if (TraceBuilder.isStringTrace(testString)) {
				Trace suppliedTrace = TraceBuilder.buildTrace(testString);
				trace = new Canonicalizer().canonicalize(suppliedTrace);
				if (verifyTrace)
					checkTestTrace(runner, trace);
			} else
				throw new ConfigurationException("Bad trace");
		}
		return trace;
	}

	private void checkTestTrace(Run runner, Trace testTrace) {
		List<InputAction> inputs = testTrace.getInputActions();
		Trace sutTrace = runner.runOnConcreteSut(inputs);
		if (!sutTrace.equalsConcrete(testTrace)) {
			StringBuilder error = new StringBuilder()
					.append("  * invalid verification trace: " + testTrace.toString()
							+ "\n                SUT gives trace: " + sutTrace.toString() + "\n");
			throw new ConfigurationException(error.toString());
		}
	}

	public Tuple2<Trace, OutputAction> testHyp(int unused_hypCounter) {
		for (Trace sutTrace : this.testTraces) {
			Trace hypTrace = this.run.runOnConcreteHypothesis(sutTrace.getInputActions());
			
			// check if the traces are consistent
			if (!sutTrace.equalsConcrete(hypTrace)) {
				Trace distSutTrace = new Trace(); 
				for (int i=0; i<=sutTrace.getTransitionIndex(); i++) {
					distSutTrace.add(sutTrace.getInputAction(i));
					distSutTrace.add(sutTrace.getOutputAction(i));
					if (!sutTrace.getOutputAction(i).concreteEquals(hypTrace.getOutputAction(i))) {
						return new Tuple2<Trace, OutputAction>(distSutTrace, hypTrace.getOutputAction(i));
					}
				} 
				throw new BugException("It should have found an inconsistency");
			} 
		}
		return null;
	}
}
