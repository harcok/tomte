package abslearning.tester;

import java.util.List;

import abslearning.trace.action.InputAction;
import de.ls5.jlearn.interfaces.Automaton;

/**
 * The interface all test generators should implement. This provides a much needed decoupling from the learner alphabet input classes.
 * All InputAction's delivered by nextTest are abstract inputs and thus, need to be concretized.
 * <br> </br>
 * <i>initialize</i> and <i>terminate</i> methods are mainly used for initiating, respectively closing connection with the hypothesis. But we could 
 * additionally use them to build the Hypothesis. 
 */
public interface TestGenerator {
	public void initialize(Automaton hyp);
	public List<InputAction> nextTest();
	public void terminate();
}
