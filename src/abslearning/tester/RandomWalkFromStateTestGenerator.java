package abslearning.tester;

import java.util.ArrayList;
import java.util.List;

import util.learnlib.AutomatonUtil;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import java.util.Random;

import abslearning.trace.action.InputAction;

public class RandomWalkFromStateTestGenerator implements TestGenerator {
	private int randomLength;
	private Random random;
	private Automaton hyp;

	public RandomWalkFromStateTestGenerator(int randomLength, long seed) {
		this.randomLength = randomLength;
		this.random =  new Random(seed);
	}

	public void initialize(Automaton hyp) {
		this.hyp = hyp;
	}

	public List<InputAction> nextTest() {

		// draw a random state and get the input trace to it from the start state.
		State randomState = util.learnlib.AutomatonUtil.pickRandomState(hyp, random);
		List<Symbol> testSymbols = util.learnlib.AutomatonUtil.traceToState(hyp, randomState, random);
		List<Symbol> abstractInputs = hyp.getAlphabet().getSymbolList();

		State current = randomState;

		for (int i = 0; i < randomLength; i++) {
			Symbol abstractInput = null;
			Symbol abstractOutput = null;
			// draw randomly an input in reduced hyp which is enabled in
			// current state
			// note: if not enabled we get output=null
			while (!AutomatonUtil.isDefined(abstractOutput)) {
				abstractInput = abstractInputs.get(random
						.nextInt(abstractInputs.size()));
				abstractOutput = current.getTransitionOutput(abstractInput);
			}
			// do transition in reduced hyp
			current = current.getTransitionState(abstractInput);
			testSymbols.add(abstractInput);
		}

		List<InputAction> abstractInputActions = new ArrayList<InputAction>();
		for (Symbol testSym : testSymbols) {
			abstractInputActions.add(new InputAction(testSym.toString()));
		}
		return abstractInputActions;
	}

	public void terminate() {
	}
}
