package abslearning.tester;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import abslearning.app.Config;
import abslearning.exceptions.BugException;
import abslearning.learner.Run;
import abslearning.mapper.AbstractionMapper;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.tree.trace.SutTraceInterface;
import abslearning.verifier.VerificationWithCADP;
import de.ls5.jlearn.interfaces.Alphabet;
import sut.info.SutInfo;

public class TesterStore {
    private static Map<Class<? extends HypTester>,
            HypTester> testerMap = new HashMap<Class<? extends HypTester>, HypTester>();

    public static HypTester getTester(Class<? extends HypTester> testerClass) {
        HypTester hypTester = testerMap.get(testerClass);
        return hypTester;
    }


    public static void addTester(HypTester hypTester) {
        Class<? extends HypTester> testerType = hypTester.getClass();
        if (testerMap.containsKey(testerType)) {
            throw new BugException("Already created hyp tester of type " + testerType);
        }
        testerMap.put(hypTester.getClass(), hypTester);
    }

    public static HypTester getTester(Config config,AbstractionMapper abstractionMapper, Alphabet inputAlphabet, SutTraceInterface sut) {

		HypTester hypTester = null;
		List<HypTester> usedHypTesters = new ArrayList<HypTester>();
		Run run= Run.getInstance();
		List<Integer> constants = SutInfo.getConstants();

		if (config.learning_reduceCounterExample) {
			usedHypTesters.add(new LastCeTester(run));
		}

		for(String testing_method : config.testing_methods) {
			switch(testing_method) {

            case "randomWalkFromState":
                hypTester = new Tester( sut, run, config.testing_randomWalkFromState_maxNumTraces, config.testing_randomWalkFromState_drawFresh,
                        config.testing_seed, new RandomWalkFromStateTestGenerator( config.testing_randomWalkFromState_randomLength, config.testing_seed), constants);
                break;
            case "randomWalk":
                hypTester = new Tester( sut, run, config.testing_randomWalk_maxNumTraces, config.testing_randomWalk_drawFresh,
                        config.testing_seed, new RandomWalkTestGenerator( config.testing_randomWalk_minTraceLength, config.testing_randomWalk_avgNumStepsAfterMinLength, config.testing_randomWalk_maxTraceLength, config.testing_seed), constants);
                break;
			case "yannakakis":
				if (config.testing_yannakakis_cmd == null) {
					throw new BugException("The run command for the yannakakis framework cannot be null. \n " +
							"Set the testing_yannakakisCmd parameter in config.yaml to the path of the yannakakis executable");
				}
				hypTester = new Tester( sut, run, config.testing_yannakakis_maxNumTraces, config.testing_yannakakis_drawFresh,
				        config.testing_seed, new IOTestGenerator(new YannakakisWrapper()), constants);
				break;
			case "cadp":
                // get verifier
                VerificationWithCADP verifier = new VerificationWithCADP(config.dependency_sutCompareCmd,
                        config.dependency_sutCompareRemoteCmd, config.verification_cadp_server);
                hypTester = new CADPTester(verifier, run, inputAlphabet, config.sutInterface_model_file,
                        config.verification_cadp_compareMinValue, config.verification_cadp_compareMaxValue);
                break;
			case "traces":
	            hypTester = new TraceTester(run, config.testing_traces_testTraces);
	            break;
			default:
				throw new BugException("Undefined test method " + testing_method);
			}
			addTester(hypTester);
			usedHypTesters.add(hypTester);
		}

		if (usedHypTesters.size() > 1) {
			hypTester = new CompositeTester(usedHypTesters.toArray(new HypTester [usedHypTesters.size()]));
		}

		return hypTester;
	}
}
