package abslearning.tester;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import abslearning.app.HypStore;
import abslearning.learner.Run;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.trace.newaction.ConcreteInput;
import abslearning.trace.newaction.LegacyUtil;
import abslearning.verifier.HypDesc;
import abslearning.verifier.VerificationWithCADP;
import de.ls5.jlearn.interfaces.Alphabet;
import util.Tuple2;

/**
 * Tests the hypothesis using model checking via CADP.
 */
public class CADPTester implements HypTester {
    private VerificationWithCADP verifier;
    private Map<Integer, Tuple2<Trace, OutputAction>> cadpResults; // (hypCount:result), if run again on the same hyp,
                                                                   // result returned
    private Run run;
    private String sutModelFile;
    private int compareMinValue;
    private int compareMaxValue;

    private static final Logger logger = Logger.getLogger(CADPTester.class);

    public CADPTester(VerificationWithCADP verifier, Run run, Alphabet inputAlphabet, String sutModelFile,
                     int compareMinValue, int compareMaxValue) {
        this.verifier = verifier;
        this.run = run;
        this.sutModelFile = sutModelFile;
        this.compareMinValue=compareMinValue;
        this.compareMaxValue=compareMaxValue;

        // cache results
        this.cadpResults = new HashMap<Integer, Tuple2<Trace, OutputAction>>();
    }

    /*
     * (non-Javadoc)
     *
     * @see abslearning.learner.EquivalenceTester#testHyp(de.ls5.jlearn.interfaces
     * .Automaton, int, abslearning.trace.Trace)
     */
    @Override
    public Tuple2<Trace, OutputAction> testHyp(int hypCounter) {

        // try cache first
        if (cadpResults.containsKey(hypCounter)) {
            return cadpResults.get(hypCounter);
        }

        CompareResult compareResult = cadpEquivalenceOracle( hypCounter);
        Tuple2<Trace, OutputAction> counterExample = null;

        if (compareResult.equal) {
            logger.fatal("CADP:  EQUAL!!");
            counterExample = null;
        } else {
            logger.fatal("CE_CADP: " + compareResult.concreteInputs);
            Trace sutTrace = this.run.runOnConcreteSut(compareResult.concreteInputs);
            Trace hypTrace = this.run.runOnConcreteHypothesis(compareResult.concreteInputs);

            logger.fatal("hypTrace:" + hypTrace.toStringConcrete());
            logger.fatal("sutTrace:" + sutTrace.toStringConcrete());

            counterExample = new Tuple2<Trace, OutputAction>(sutTrace, hypTrace.getLastOutputAction());
        }

        cadpResults.put(hypCounter, counterExample);
        return counterExample;
    }

    public  CompareResult cadpEquivalenceOracle(
             int hypCounter) {

        // get hyp model
    	HypDesc hypDesc = HypDesc.getDescForHyp(hypCounter);
    	HypStore.buildConcreteHypXml(hypDesc.abstractionFile, hypDesc.dotModelFile, hypDesc.concreteXmlModelFile);
        String learnedModelFile = hypDesc.concreteXmlModelFile;

        logger.fatal("compare hyp: " + learnedModelFile + "  <-> sut: " + sutModelFile);


        VerificationWithCADP.CompareResult cmpResult = verifier.compareModels(learnedModelFile,
                sutModelFile, compareMinValue, compareMaxValue);

        logger.info("CADP: " + cmpResult.verboseToString() );

        // for ce convert ConcreteInput into legacy InputAction
        List<InputAction> counterExampleInputs = null;
        if (!cmpResult.equal) {
            counterExampleInputs = new ArrayList<InputAction>();
            for ( ConcreteInput input : cmpResult.inputs) {
                InputAction inputaction=LegacyUtil.convertConcreteInput2InputAction(input);
                counterExampleInputs.add(inputaction);
            }
        }
        return new CompareResult(cmpResult.equal, counterExampleInputs);
    }

    public static class CompareResult {
        public boolean equal = false;
        public List<InputAction> concreteInputs;

        public CompareResult(boolean equal, List<InputAction> inputs) {
            this.equal = equal;
            this.concreteInputs = inputs;
        }
    }

}
