package abslearning.tester;

import java.io.BufferedReader;

import de.ls5.jlearn.interfaces.Automaton;

public interface IOWrapper {
	public void initialize(Automaton hyp);
	public BufferedReader out();
	public void close();
}
