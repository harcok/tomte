package abslearning.tester;

import util.Tuple2;
import abslearning.trace.Trace;
import abslearning.trace.action.OutputAction;

public class CompositeTester implements HypTester {

    private HypTester[] testers;

    public CompositeTester(HypTester... testers) {
        this.testers = testers;
    }

    @Override
    public Tuple2<Trace, OutputAction> testHyp(int hypCounter) {
        Tuple2<Trace, OutputAction> cex = null;
        for (HypTester tester : testers) {
            cex = tester.testHyp(hypCounter);
            if (cex != null)
                break;
        }
        return cex;
    }
}
