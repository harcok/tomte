package abslearning.tester;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.determinizer.Determinizer;
import abslearning.exceptions.DecoratedRuntimeException;
import abslearning.exceptions.TesterException;
import abslearning.exceptions.ValueCannotBeDecanonizedException;
import abslearning.learner.Run;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.stores.ConfigurableValueStoreProvider;
import abslearning.stores.FreshInputValueStore;
import abslearning.stores.RandomValueStore;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.tree.trace.SutTraceInterface;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.SymbolImpl;
import sut.info.TomteHypInterface;
import util.Tuple2;
import util.learnlib.AutomatonUtil;

public class Tester implements HypTester{
	private TestGenerator testGenerator;
    private static final Logger logger = Logger.getLogger(HypTester.class);
	private final Logger logger_equivTraces=Logger.getLogger("equivTraces");
	private final SutTraceInterface sut;
	private final Run runner;
	private final int maxTraces;
	private double drawFreshProbability;
    private long seed;
	private final List<Integer> constants;
	private final static Statistics statistics = Statistics.getInstance();

	public Tester(SutTraceInterface sut, Run run, int maxTraces, double drawFreshProbability, long seed,
			TestGenerator testGenerator, List<Integer> constants) {

		this.sut = sut;
		this.runner = run;
		this.maxTraces = maxTraces;
		this.testGenerator = testGenerator;
		this.drawFreshProbability = drawFreshProbability;
        this.seed = seed;
        this.constants = constants;
	}

	public Tuple2<Trace, OutputAction> testHyp(int unused_hypCounter) {

		Tuple2<Trace,OutputAction> result = null;

		TomteHypInterface testHyp = runner.getConcreteHyp();
		Automaton abstractHyp = runner.getAbstractHyp();  // not reduced one
		Random random = new Random(seed);
		long totalTestLength = 0;
		int testIndex;
		testGenerator.initialize(abstractHyp);

         if ( logger_equivTraces.isDebugEnabled() ) {
            logger_equivTraces.debug("-----------------------------------------------------");
            logger_equivTraces.debug("run          :" + statistics.getCurrentRunNumber());
            logger_equivTraces.debug("-----------------------------------------------------");
         }


		 TRACE: for (testIndex=0; testIndex< maxTraces; testIndex++ ) {

			 sut.sendReset();

			 testHyp.sendReset();
			 SortedMap<Integer, Integer> statevarsBeforeTransition=testHyp.getStatevars();
			 SortedMap<Integer, Integer> statevarsAfterTransition=null; // no inputs yet done



			 statistics.incEquivQueries();

			 Trace hypTrace = new abslearning.trace.Trace();
			 Trace sutTrace = new abslearning.trace.Trace();

			 RandomValueStore valueStore = new RandomValueStore(this.constants, new FreshInputValueStore(), drawFreshProbability, random);

			 State currentState = abstractHyp.getStart();
			 List<InputAction> nextTest = testGenerator.nextTest();

		     for (int transitionIndex = 0; transitionIndex < nextTest.size(); transitionIndex++) {
                totalTestLength += nextTest.size();
		    	 try {
		    	 InputAction input = nextTest.get(transitionIndex);

		    	 Symbol inputSymbol = new SymbolImpl(input.toStringAbstract());
		    	 Symbol transOutput = currentState.getTransitionOutput(inputSymbol);

		    	 if (!AutomatonUtil.isDefined(transOutput)) {
		    		 continue;
		    	 } else {
			    	 State transState = currentState.getTransitionState(inputSymbol);
		    		 currentState = transState;
		    	 }

		    	 valueStore.updateStateVars(statevarsBeforeTransition.values());


		    	 hypTrace.add(input); // NEEDED to get transition index increased in Trace : when adding input then new transition index is increase

                    int returnValue = runner.abstractionMapper.concretizeInput(input, hypTrace.getStatevarsBeforeTransition(),
                            valueStore);

				 if ( returnValue !=0 ) {
					     hypTrace.removeLastAction();
					     nextTest.remove(transitionIndex);
					     // TODO this is a very ugly hack, it would be better if the history (or the full trace) got passed along as a parameter
					     valueStore.removeFromHistory(input.getConcreteParametersSoFarDefined());
					     transitionIndex--; // redo input
						 continue;  // only interested in good concrete inputs
				 }
				 sutTrace.add(input);


				 statistics.incEquivInputs();

				 OutputAction outputSut = null;

				 outputSut = sut.sendInput(input, sutTrace);


				 //  add fresh output values to (stochastic) valueStore
				 Set<Integer> freshOutputValues = Determinizer.getCanonizedValueSet(outputSut);
				 valueStore.addValues(new ArrayList<Integer>(freshOutputValues));

				 OutputAction outputHyp = null;
				 outputHyp = testHyp.sendInput(input);

			    statevarsAfterTransition=testHyp.getStatevars();

			    hypTrace.add(outputHyp);
			    sutTrace.add(outputSut);


			    hypTrace.updateStatevars(statevarsAfterTransition)	;

				logger.debug("hyp: " + input + " \\ " + outputHyp + " sut:" +  input + " \\ " +  outputSut);

				// check whether SUT and Hypothesis give same output
				if(!outputSut.concreteEquals(outputHyp)){
					result=runner.checkIsCounterExample(hypTrace.getConcreteInputActions());
					if ( result!= null) {
					    // verified to be counterexample
						logger.fatal("hypTrace:" + hypTrace.toStringConcrete());
						logger.fatal("sutTrace:" + sutTrace.toStringConcrete());
						logger.fatal("SUT:" + outputSut.toStringFull());
						logger.fatal("HYP:" + outputHyp.toStringFull());

						break;
					} else {
						logger.fatal("Collision detected in:");
						logger.fatal("hypTrace:" + hypTrace.toStringConcrete());
						logger.fatal("sutTrace:" + sutTrace.toStringConcrete());

						continue TRACE;
					}
				}

				// no counterexample found: continue with next input of test trace

				// continue to next input: what was after becomes before!
				statevarsBeforeTransition=statevarsAfterTransition;
		    	 }catch(DecoratedRuntimeException exc) {
		    		 if (exc instanceof ValueCannotBeDecanonizedException) {
		    			 // if the hyp diverges in terms of values this can happen
		    			 continue TRACE;
		    		 }
//		    		 continue TRACE;
		    		 sutTrace.removeLastAction();
		    		 sutTrace = runner.runOnConcreteSutAddStateVarsAndAbstractions(sutTrace.getConcreteInputActions());
		    		 logger.fatal(sutTrace.toStringWithStatevars());
		    		 logger.fatal(hypTrace.toStringWithStatevars());
		    		 runner.increaseVersionOfObservationTree();
		    		 Trace freshTrace = runner.runFreshTraceOnSut(sutTrace.getAbstractInputWord());
		    		 freshTrace = runner.runOnConcreteSutAddStateVarsAndAbstractions(freshTrace.getConcreteInputActions());
		    		 logger.fatal(freshTrace.toStringWithStatevars());
					 throw new TesterException(exc, sutTrace, testIndex, transitionIndex);//.
				 }
		     }

		     if ( logger_equivTraces.isDebugEnabled() ) {
                logger_equivTraces.debug(
                        "Run.phase.index.num.trace: " + statistics.getCurrentRunNumber() + "." + statistics.getCurrentPhase() + "."
                                + testIndex + "." + statistics.getNumEquivQueries() + " abstract: " + hypTrace.toStringAbstract());
                logger_equivTraces.debug(
                        "Run.phase.index.num.trace: " + statistics.getCurrentRunNumber() + "." + statistics.getCurrentPhase() + "."
                                + testIndex + "." + statistics.getNumEquivQueries() + " concrete: " + hypTrace.toStringConcrete());
		     }

		     if (result != null) {
		    	 break;
		     }

		     // no counterexample found: continue with next test trace
	    }

        logger.info("DONE TESTING: average test length: " + totalTestLength / (testIndex + 1) + "; totalTestLength: " + totalTestLength
                + "; numberTestsDone:" + (testIndex+1) );
         testGenerator.terminate();

	     return result; // means : no counterexample found

	}
}
