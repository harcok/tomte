package abslearning.tester;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import de.ls5.jlearn.interfaces.Automaton;

import abslearning.exceptions.BugException;
import abslearning.trace.action.InputAction;

public class IOTestGenerator implements TestGenerator {
	private IOWrapper wrapper;
	public IOTestGenerator(IOWrapper wrapper) {
		this.wrapper = wrapper;
	}
	
	public void initialize(Automaton hyp) {
		this.wrapper.initialize(hyp);
	}
	
	public List<InputAction> nextTest() {
		List<InputAction> testQuery = null;
		String line;
		try {
			line = wrapper.out().readLine();
		
			if ( line != null) {
				testQuery = new ArrayList<InputAction>();
				Scanner s = new Scanner(line);
				while(s.hasNext()) {
					testQuery.add(new InputAction(s.next()));
				}
				s.close();
			}
		} catch (IOException e) {
			throw new BugException("Generated IO Exception while generating tests from stdin");
		}
		
		return testQuery;
	}
	
	public void terminate() {
		wrapper.close();
	}
}
