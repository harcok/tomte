package abslearning.tester;

import util.Tuple2;
import abslearning.trace.Trace;
import abslearning.trace.action.OutputAction;
import de.ls5.jlearn.interfaces.Automaton;


/**
 *   give the tester a hypothesis counter and it will search for a counter example.
 *
 *   returns:
 *     - null: no counter example found
 *     - Tuple2<Trace, OutputAction>  :  sut trace and hypothesis last output differing from the one in the sut trace
 *
 */
public interface HypTester {

    public abstract Tuple2<Trace, OutputAction> testHyp(int hypCounter);

}