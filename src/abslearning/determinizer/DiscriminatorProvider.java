package abslearning.determinizer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import abslearning.exceptions.BugException;
import abslearning.exceptions.ValueCannotBeDecanonizedException;
import abslearning.relation.Relation;
import abslearning.trace.action.Action;
import abslearning.trace.action.InputAction;

public enum DiscriminatorProvider {
	SYSTEM,
	LEARNER,
	NOTCANONIZED,
	CANONIZED;
	private static Discriminator notCanonized = new NotCanonizedDiscriminator();
	private static Discriminator canonized = new CanonizedDiscriminator();
	
	public Discriminator newDiscriminator() {
		switch(this) {
		case SYSTEM:
			return new SystemDiscriminator();//SystemThoroughDiscriminator();
		case NOTCANONIZED:
			return notCanonized;
		case CANONIZED:
			return canonized;
		case LEARNER:
			return new LearnerDiscriminator();
		}
		throw new BugException("Case not defined for discriminator " + this.name());
	}
	
	// This discriminator distinguishes values originating from the system. It assumed
	// the system does not generate any negative values
	static class SystemDiscriminator implements Discriminator {
		private Set<Integer> learnerValues = new HashSet<Integer>();
		public void reset() {
			learnerValues.clear();
		}

		// all values discriminated should be negative, as they stem from the system
		@Override
		public List<Integer> discriminate(Action action) {
			List<Integer> concreteValues = action.getConcreteParameters();
			List<Integer> discriminatedValues = new ArrayList<Integer>();
			if (action instanceof InputAction) {
				// the only values originate stem from system values are those already canonized
				List<Integer> canonizedSystemValues = Determinizer.getCanonizedValues(concreteValues);
				List<Integer> learnerValues = action.getConcreteParameters();
				learnerValues.removeAll(canonizedSystemValues);
				this.learnerValues.addAll(learnerValues);
				discriminatedValues.addAll(canonizedSystemValues);
			} else {
				// the system might generate values obtained by relations on learner values. 
				// We need to remove these values
				//TODO !! get related values isn't working properly since it doesn't count for sequences of relations
				List<Integer> learnerRelatedValues = Relation.getRelatedValuesTo( this.learnerValues, concreteValues);
				discriminatedValues.addAll(concreteValues);
				discriminatedValues.removeAll(learnerRelatedValues);
				this.learnerValues.addAll(learnerRelatedValues);
				
			}
			return discriminatedValues;
		}
	}

	static class NotCanonizedDiscriminator extends ConditionalDiscriminator {
		protected boolean isDiscriminated(Integer value) {
			return !Determinizer.isCanonized(value);
		}
	}
	
	static class CanonizedDiscriminator extends ConditionalDiscriminator {
		protected boolean isDiscriminated(Integer value) {
			return Determinizer.isCanonized(value);
		}
		
	}
	
	static abstract class ConditionalDiscriminator implements Discriminator {
		@Override
		public List<Integer> discriminate(Action action) {
			List<Integer> concreteValues = action.getConcreteParameters();
			List<Integer> discriminatedValues = new ArrayList<Integer>();
			for (Integer concreteValue : concreteValues) {
				if (isDiscriminated(concreteValue)) {
					discriminatedValues.add(concreteValue);
				}
			}
			return discriminatedValues;
		}
		
		protected abstract boolean isDiscriminated(Integer value);
		
		public void reset() {
		}
	}
	
	static class LearnerDiscriminator implements Discriminator {
		private Set<Integer> learnerValues = new HashSet<Integer>();
		private Set<Integer> systemValues = new HashSet<Integer>();
		public void reset() {
			learnerValues.clear();
			systemValues.clear();
		}

		// all values discriminated should be negative, as they stem from the system
		@Override
		public List<Integer> discriminate(Action action) {
			List<Integer> concreteValues = action.getConcreteParameters();
			List<Integer> discriminatedValues = new ArrayList<Integer>();
			if (action instanceof InputAction) {
				// the only values originate stem from system values are those already canonized
				List<Integer> systemRelatedValues = Relation.getRelatedValuesTo( this.systemValues, concreteValues);
				discriminatedValues.addAll(concreteValues);
				discriminatedValues.removeAll(systemRelatedValues);
				this.systemValues.addAll(systemRelatedValues);
				this.learnerValues.addAll(discriminatedValues);
			} else {
				// the system might generate values obtained by relations on learner values. 
				List<Integer> learnerRelatedValues = Relation.getRelatedValuesTo( this.learnerValues, concreteValues);
				List<Integer> systemRelatedValues = new ArrayList<Integer>(concreteValues);
				systemRelatedValues.removeAll(learnerRelatedValues);
				discriminatedValues.addAll(learnerRelatedValues);
				this.systemValues.addAll(systemRelatedValues);
				this.learnerValues.addAll(learnerRelatedValues);
				
			}
			return discriminatedValues;
		}
	}
}
