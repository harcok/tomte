package abslearning.determinizer.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import abslearning.exceptions.ConfusionException;
import abslearning.relation.Relation;
import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;

public class ListValueMapper implements ValueMapper{
	private List<Integer> actualValues;
	private List<Integer> canonizedValues;
	private CanonicalValueStoreProvider valueStoreProvider;
	private CanonicalValueStore valueStore;
	
	public ListValueMapper(CanonicalValueStoreProvider valueStoreProvider) {
		this.valueStoreProvider = valueStoreProvider;
		this.actualValues = new ArrayList<Integer>();
		this.canonizedValues = new ArrayList<Integer>();
		this.valueStore = this.valueStoreProvider.newValueStore();
	}
	
	public BiMap<Integer,Integer> buildCanonicalMap(Collection<Integer> values) throws ConfusionException{
		BiMap<Integer,Integer> canonicalMap = HashBiMap.create();
		for (Integer value : values) {
			Integer analogousValue = findAndAddAnalgousValue(value, this.actualValues, this.canonizedValues);
			if (analogousValue == null) {
				analogousValue = findAndAddAnalgousValue(value, this.canonizedValues, this.actualValues);
				if (analogousValue == null) {
					this.actualValues.add(value);
					Integer freshCanonizedValue = this.valueStore.popFreshValue();
					this.canonizedValues.add(this.valueStore.popFreshValue());
					canonicalMap.put(value, freshCanonizedValue);
				} else {
					canonicalMap.put(analogousValue, value);
				}
			} else {
				canonicalMap.put(value, analogousValue);
			}
		}
		return canonicalMap;
	}

	private Integer findAndAddAnalgousValue(Integer value, List<Integer> fromList, List<Integer> inList) throws ConfusionException {
		Integer mappedValue = null;
		List<Integer> relatedValues = Relation.getRelatedValues(fromList, value);
		if (relatedValues.size() > 1) {
			
			return null;
		}
		if (relatedValues.size() > 1) {
			throw new ConfusionException();
		} else 
			if (relatedValues .size() == 1) {
				Integer relatedActualValue = relatedValues.get(0);
				Relation relationWithActualValue = Relation.getRelation(value, relatedActualValue);
				Integer analogousCanonizedValue = inList.get(relatedActualValue);
				
				if (relationWithActualValue.equals(Relation.EQUAL)) {
					mappedValue = analogousCanonizedValue;
				} else {
					mappedValue = relationWithActualValue.map(analogousCanonizedValue);
					if (inList.contains(mappedValue)) {
						throw new ConfusionException();
					} else {
						inList.add(mappedValue);
					}
				}
			}
		return mappedValue;
	}
	
	
	
	public void reset() {
		this.actualValues = new ArrayList<Integer>();
		this.canonizedValues = new ArrayList<Integer>();
		this.valueStore = this.valueStoreProvider.newValueStore();
	}

}
