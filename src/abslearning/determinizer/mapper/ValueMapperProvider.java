package abslearning.determinizer.mapper;

import abslearning.exceptions.BugException;
import abslearning.stores.CanonicalValueStoreProvider;

public enum ValueMapperProvider {
	LIST,
	DUALFORREST,
	FORREST;
	
	public ValueMapper newValueMapper(CanonicalValueStoreProvider  valueStoreProvider) {
		switch(this) {
		case LIST:
			return new ListValueMapper(valueStoreProvider);
		case DUALFORREST:
			return new DualRelationForest(valueStoreProvider);
		case FORREST:
			return new DualRelationForest(valueStoreProvider);
		}
		throw new BugException("ValueMapper undefined for "+ this.name());
	}
}
