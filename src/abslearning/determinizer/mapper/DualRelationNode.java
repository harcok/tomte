package abslearning.determinizer.mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;

public class DualRelationNode{
	private Map<Relation, DualRelationNode> children;
	private Integer value;
	private Integer canonicalValue;

	public DualRelationNode(Integer value, Integer canonicalValue) {
		this.value = value;
		this.canonicalValue = canonicalValue;
		this.children = new HashMap<Relation, DualRelationNode>();
	}

	public void buildTreeMapping(Map<Integer, Integer> treeMapping) {
		treeMapping.put(value, canonicalValue);
		for (Entry<Relation, DualRelationNode> nodeEntry : children.entrySet()) {
			nodeEntry.getValue().buildTreeMapping(treeMapping);
		}
	}

	public boolean containsValue(Integer value) {
		boolean hasValue = false;
		if (this.value.equals(value)) {
			hasValue = true;
		} else 
			if (this.canonicalValue.equals(value)) {
				hasValue = true;
		} else {
			for (DualRelationNode child : children.values()) {
				hasValue = child.containsValue(value);
				if (hasValue) {
					break;
				}
			}
		}

		return hasValue;
	}

	public DualRelationNode tryToAddNewValue(Integer value) {
		DualRelationNode resultNode = null;
		Relation relation = Relation.getRelation(this.value, value);
		if (relation != null && !children.containsKey(relation) && !this.value.equals(value)) {
			DualRelationNode relationNode = new DualRelationNode(value, relation.map(canonicalValue));
			children.put(relation, relationNode);
			resultNode = relationNode;
		} else {
			relation = Relation.getRelation(this.canonicalValue, value);
			if (relation != null && !children.containsKey(relation) && !this.value.equals(value)) {
				DualRelationNode relationNode = new DualRelationNode(relation.map(this.value), value);
				children.put(relation, relationNode);
				resultNode = relationNode;
			} else {
			for (DualRelationNode child : children.values()) {
				DualRelationNode testNode = child.tryToAddNewValue(value);
				if (testNode != null) {
					if (resultNode == null) { 
						resultNode = testNode;
					} else {
						throw new BugException("Collision when adding new node to tree");
					}
				}

			}
		}}

		return resultNode;
	}

	// public boolean isMergeCandidate(Node node) {
	// Relation relation = Relation.getRelation(this.value, node.value);
	// return relation != null && !children.containsKey(relation) &&
	// this.value != node.value;
	// }

	public boolean merge(DualRelationNode node) {
		boolean successful = false;
		Relation relation = Relation.getRelation(this.value, node.value);
		// !children.containsKey(relation) it doesn't matter by which
		// relation equality occurred
		if (relation != null && !this.value.equals(node.value)) {
			children.put(relation, node);
			successful = true;
		} else {
			for (DualRelationNode child : children.values()) {
				if (child.merge(node)) {
					successful = true;
					break;
				}
			}

		}
		return successful;
	}

	public boolean isLeaf() {
		return children.isEmpty();
	}

	public String toString() {
		return "[(" + value + ", " + canonicalValue + ");"+ children.toString() + "]";
	}
}
