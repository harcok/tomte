package abslearning.determinizer.mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;

public class RelationNode {
	private Map<Relation, RelationNode> children;
	private Integer value;

	public RelationNode(Integer value) {
		this.value = value;
		this.children = new HashMap<Relation, RelationNode>();
	}

	public void buildTreeMapping(Map<Integer, Integer> treeMapping, int mappedValue) {
		treeMapping.put(value, mappedValue);
		for (Entry<Relation, RelationNode> nodeEntry : children.entrySet()) {
			Integer nextMappedValue = nodeEntry.getKey().map(mappedValue);
			nodeEntry.getValue().buildTreeMapping(treeMapping, nextMappedValue);
		}
	}

	public boolean containsValue(Integer value) {
		boolean hasValue = false;
		if (this.value.equals(value)) {
			hasValue = true;
		} else {
			for (RelationNode child : children.values()) {
				hasValue = child.containsValue(value);
				if (hasValue) {
					break;
				}
			}
		}

		return hasValue;
	}
	
	protected Integer getValue() {
		return value;
	}
	
	protected Map<Relation, RelationNode> getChildren() {
		return children;
	}
	
	public RelationNode tryToAddNewValue(Integer value) {
		RelationNode resultNode = null;
		Relation relation = Relation.getRelation(this.value, value);
		if (relation != null && !children.containsKey(relation) && !this.value.equals(value)) {
			RelationNode relationNode = new RelationNode(value);
			children.put(relation, relationNode);
			resultNode = relationNode;
		} else {
			for (RelationNode child : children.values()) {
				RelationNode testNode = child.tryToAddNewValue(value);
				if (testNode != null) {
					if (resultNode == null) { 
						resultNode = testNode;
					} else {
						throw new BugException("Collision when adding new node to tree");
					}
				}

			}
		}

		return resultNode;
	}

	// public boolean isMergeCandidate(Node node) {
	// Relation relation = Relation.getRelation(this.value, node.value);
	// return relation != null && !children.containsKey(relation) &&
	// this.value != node.value;
	// }

	public boolean merge(RelationNode node) {
		boolean successful = false;
		Relation relation = Relation.getRelation(this.value, node.value);
		// !children.containsKey(relation) it doesn't matter by which
		// relation equality occurred
		if (relation != null && !this.value.equals(node.value)) {
			children.put(relation, node);
			successful = true;
		} else {
			for (RelationNode child : children.values()) {
				if (child.merge(node)) {
					successful = true;
					break;
				}
			}

		}
		return successful;
	}

	public boolean isLeaf() {
		return children.isEmpty();
	}

	public String toString() {
		return "[" + value + "; " + children.toString() + "]";
	}
}
