package abslearning.determinizer.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;
import abslearning.stores.CanonicalValueStore;

/**
 * The relation forest is a list of trees in which each node contains a value
 * and can refer to children containing values related (but not equal) to that value. Thus, in each tree, all values
 * are necessarily related from top to bottom.
 * </p>
 * Example: [(60 (+1 61 (+1 62 )) (*2 120)); ... ]
 * </p>
 * When attempting to add a value to the forest, we look for a tree which either 
 * 		already contains the value (in which case the value is not added)
 * 		has a leaf node value that is inversely related to it (in which case it is added to the leaf node)
 * </br>
 * If no tree is found, a new tree is created, with root containing this value and an empty list of children.
 * Adding a value to the tree can lead to mergers of two trees, in case the value is also related to a root value
 * in a different tree. 
 * </br>
 * Adding restrictions:
 * 		there must be at most one tree on which the value can be added 
 * 		if there is such a tree, there must be only one node which can be parent to the newly created node
 * 		if a merger results after the addition, there can be at most one merging tree candidate
 * </br>
 * Given a canonical value store, we can instantiate a canonical mapping for the values in the tree, for the example:
 *  [{60:-100},{61:-101},{62,-102},{120:-200}]
 */
public class RelationForest {
	private List<RelationNode> rootNodes; 
	
	public RelationForest() {
		this.rootNodes = new ArrayList<RelationNode>();
	}
	
	public RelationForest(RelationForest relationTree) {
		this.rootNodes = relationTree.rootNodes;
	}

	/**
	 * Add value to relation tree. There is collision if 
	 */
	public void addValue(Integer value) {
		RelationNode addedNode = null;
		RelationNode updatedTreeRoot = null;

		if (!containsValue(value)) {
			for (RelationNode treeRoot : rootNodes) {
				RelationNode testNode = treeRoot.tryToAddNewValue(value);
				if (testNode != null) {
					if (addedNode != null) {
						throw new BugException("Collision as value could be added to several trees");
					} else {
						addedNode = testNode;
						updatedTreeRoot = treeRoot;
					}
				}
			}
			
			// if the node wasn't found, we need to add it.
			if (addedNode == null) {
				addedNode = new RelationNode(value);
				updatedTreeRoot = addedNode;
				rootNodes.add(addedNode);
			}
			
			// only if we could not find a node and we created a new one do we
			// try to merge the new node with existing roots, not
			List<RelationNode> mergeCandidates = new ArrayList<RelationNode>(rootNodes);
			mergeCandidates.remove(updatedTreeRoot);
			List<RelationNode> mergedRoots = mergeRootsToNode(addedNode, mergeCandidates);
			rootNodes.removeAll(mergedRoots);
		}
	}

	private boolean containsValue(Integer value) {
		boolean hasValue = false;
		for (RelationNode treeRoot : rootNodes) {
			if (treeRoot.containsValue(value)) {
				hasValue = true;
				break;
			}
		}

		return hasValue;
	}

	public Map<Integer, Integer> buildFreshTreeMapping(CanonicalValueStore freshValueProvider) {
		Map<Integer, Integer> freshTreeMapping = new HashMap<Integer, Integer>();
		for (RelationNode node : rootNodes) {
			int nextFreshValueAtRoot = freshValueProvider.popFreshValue();
			node.buildTreeMapping(freshTreeMapping, nextFreshValueAtRoot);
		}

		return freshTreeMapping;
	}

	private List<RelationNode> mergeRootsToNode(RelationNode addedNode, List<RelationNode> mergeCandidates) {

		List<RelationNode> mergedRoots = new ArrayList<RelationNode>();
		boolean merged;

		do {
			merged = false;
			for (RelationNode mergeCandidate : mergeCandidates) {
				if (addedNode.merge(mergeCandidate)) {
					if (!merged) {
						merged = true;
						mergedRoots.add(mergeCandidate);

					} else {
						throw new BugException("Collision when merging node. Several merging candidates");
					}
				}
			}
			mergeCandidates.removeAll(mergedRoots);
		} while (merged);

		return mergedRoots;
	}

	public void display() {
		for (RelationNode rootNode : rootNodes) {
			System.out.println(rootNode.toString());
		}
	}

	

	// relation tree test
	public static void main(String[] args) {
		testMerging();
	}
	
	public static void testGrouping() {
		RelationForest relationTree = new RelationForest();

		relationTree.addValue(191);
		relationTree.addValue(55);
		relationTree.addValue(191);
		relationTree.addValue(192);
		relationTree.addValue(192);
		relationTree.addValue(191);
		relationTree.addValue(56);
		relationTree.display();
	}

	public static void testMerging() {
		RelationForest relationTree = new RelationForest();
		relationTree.addValue(10);
		relationTree.addValue(11);
		relationTree.addValue(12);
		relationTree.addValue(12);
		relationTree.addValue(15);
		relationTree.addValue(14);
		relationTree.addValue(13);
		relationTree.display();
	}
}
