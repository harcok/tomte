package abslearning.determinizer.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import abslearning.exceptions.BugException;
import abslearning.exceptions.ConfusionException;
import abslearning.relation.Relation;
import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;

/**
 * A dual relation forest runs under the same mechanism as the basic relation forest, the main differences are that
 * each node now contains 2 values. The first value of each node in a tree form a collection of 
 * "actual values". The second values form a collection of "canonical values". The relation tying 
 * a parent to its child must hold for their actual values, and for their canonical values independently.
 * </br>
 * Example: (60 -100 (+1 61 -101 (+1 62 -102)) (*2 120 -200)) 
 * </br>
 * When a value is added, relations are now considered to both the actual and the canonical set of values.
 * If such a relation is found, a new node is added where one value is the added value, and the other value
 * is the relation applied to the parent's value from the other collection as the added value. 
 * </br>
 * This gives more flexibility, as we can now add both actual values and canonized values.  
 * </br>
 * Assumption: the actual values and canonical values MUST be from distinct domains, otherwise mapping
 * doesn't function. 
 */
public class DualRelationForest implements ValueMapper{
	private List<Node> rootNodes;
	private CanonicalValueStoreProvider valueStoreProvider;
	private CanonicalValueStore valueStore;
	
	public DualRelationForest(CanonicalValueStoreProvider valueStoreProvider) {
		this.rootNodes = new ArrayList<Node>();
		this.valueStoreProvider = valueStoreProvider;
		this.valueStore = valueStoreProvider.newValueStore();
	}
	
	public DualRelationForest(DualRelationForest relationForest) {
		this.rootNodes = relationForest.rootNodes;
	}
	

	public void reset() {
		this.rootNodes.clear();
		this.valueStore = this.valueStoreProvider.newValueStore();
	}
	
	//TODO map values
	public BiMap<Integer, Integer> buildCanonicalMap(Collection<Integer> values) throws ConfusionException {
		for (Integer value : values) {
			this.addValue(value);
		}
		BiMap<Integer,Integer> canonicalMap = HashBiMap.create(this.buildFreshTreeMapping());
		return canonicalMap;
	}
	
	

	/**
	 * Add value to relation tree. There is collision if 
	 */
	public void addValue(Integer value) {
		Node addedNode = null;
		Node updatedTreeRoot = null;

		if (!containsValue(value)) {
			for (Node treeRoot : rootNodes) {
				Node testNode = treeRoot.tryToAddNewValue(value);
				if (testNode != null) {
					if (addedNode != null) {
						throw new BugException("Collision as value could be added to several trees");
					} else {
						addedNode = testNode;
						updatedTreeRoot = treeRoot;
					}
				}
			}
			
			// if the node wasn't found, we need to add it.
			if (addedNode == null) {
				addedNode = new Node(value, this.valueStore.popFreshValue());
				updatedTreeRoot = addedNode;
				rootNodes.add(addedNode);
			}
			
			// only if we could not find a node and we created a new one do we
			// try to merge the new node with existing roots, not
			List<Node> mergeCandidates = new ArrayList<Node>(rootNodes);
			mergeCandidates.remove(updatedTreeRoot);
			List<Node> mergedRoots = mergeRootsToNode(addedNode, mergeCandidates);
			rootNodes.removeAll(mergedRoots);
		}
	}

	private boolean containsValue(Integer value) {
		boolean hasValue = false;
		for (Node treeRoot : rootNodes) {
			if (treeRoot.containsValue(value)) {
				hasValue = true;
				break;
			}
		}

		return hasValue;
	}

	private Map<Integer, Integer> buildFreshTreeMapping() {
		Map<Integer, Integer> freshTreeMapping = new HashMap<Integer, Integer>();
		for (Node node : rootNodes) {
			node.buildTreeMapping(freshTreeMapping);
		}
		return freshTreeMapping;
	}

	private List<Node> mergeRootsToNode(Node addedNode, List<Node> mergeCandidates) {

		List<Node> mergedRoots = new ArrayList<Node>();
		boolean merged;

		do {
			merged = false;
			for (Node mergeCandidate : mergeCandidates) {
				if (addedNode.merge(mergeCandidate)) {
					if (!merged) {
						merged = true;
						mergedRoots.add(mergeCandidate);

					} else {
						throw new BugException("Collision when merging node. Several merging candidates");
					}
				}
			}
			mergeCandidates.removeAll(mergedRoots);
		} while (merged);

		return mergedRoots;
	}

	public void display() {
		System.out.println(this.toString());
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		int index = 0;
		for (Node rootNode : rootNodes) {
			builder.append("Tree " ).append(index++);
			builder.append(rootNode.toString()).append("\n");
		}
		return builder.toString();
	}

	static class Node {
		private Map<Relation, Node> children;
		private Integer value;
		private Integer canonicalValue;

		private Node(Integer value, Integer canonicalValue) {
			this.value = value;
			this.canonicalValue = canonicalValue;
			this.children = new HashMap<Relation, Node>();
		}

		public void buildTreeMapping(Map<Integer, Integer> treeMapping) {
			treeMapping.put(value, canonicalValue);
			for (Entry<Relation, Node> nodeEntry : children.entrySet()) {
				nodeEntry.getValue().buildTreeMapping(treeMapping);
			}
		}

		public boolean containsValue(Integer value) {
			boolean hasValue = false;
			if (this.value.equals(value)) {
				hasValue = true;
			} else 
				if (this.canonicalValue.equals(value)) {
					hasValue = true;
			} else {
				for (Node child : children.values()) {
					hasValue = child.containsValue(value);
					if (hasValue) {
						break;
					}
				}
			}

			return hasValue;
		}

		public Node tryToAddNewValue(Integer value) {
			Node resultNode = null;
			Relation relation = Relation.getRelation(this.value, value);
			if (relation != null && !children.containsKey(relation) && !this.value.equals(value)) {
				Node relationNode = new Node(value, relation.map(canonicalValue));
				children.put(relation, relationNode);
				resultNode = relationNode;
			} else {
				relation = Relation.getRelation(this.canonicalValue, value);
				if (relation != null && !children.containsKey(relation) && !this.value.equals(value)) {
					Node relationNode = new Node(relation.map(this.value), value);
					children.put(relation, relationNode);
					resultNode = relationNode;
				} else {
				for (Node child : children.values()) {
					Node testNode = child.tryToAddNewValue(value);
					if (testNode != null) {
						if (resultNode == null) { 
							resultNode = testNode;
						} else {
							throw new BugException("Collision when adding new node to tree");
						}
					}

				}
			}}

			return resultNode;
		}

		// public boolean isMergeCandidate(Node node) {
		// Relation relation = Relation.getRelation(this.value, node.value);
		// return relation != null && !children.containsKey(relation) &&
		// this.value != node.value;
		// }

		public boolean merge(Node node) {
			boolean successful = false;
			Relation relation = Relation.getRelation(this.value, node.value);
			// !children.containsKey(relation) it doesn't matter by which
			// relation equality occurred
			if (relation != null && !this.value.equals(node.value)) {
				children.put(relation, node);
				successful = true;
			} else {
				for (Node child : children.values()) {
					if (child.merge(node)) {
						successful = true;
						break;
					}
				}

			}
			return successful;
		}

		public boolean isLeaf() {
			return children.isEmpty();
		}

		public String toString() {
			return "[(" + value + ", " + canonicalValue + ");"+ children.toString() + "]";
		}
	}

	// relation tree test
	public static void main(String[] args) {
//		testMerging();
	}
//	
//	public static void testGrouping() {
//		DualRelationTree relationTree = new DualRelationTree();
//
//		relationTree.addValue(191);
//		relationTree.addValue(55);
//		relationTree.addValue(191);
//		relationTree.addValue(192);
//		relationTree.addValue(192);
//		relationTree.addValue(191);
//		relationTree.addValue(56);
//		relationTree.display();
//	}
//
//	public static void testMerging() {
//		DualRelationTree relationTree = new DualRelationTree();
//		relationTree.addValue(10);
//		relationTree.addValue(11);
//		relationTree.addValue(12);
//		relationTree.addValue(12);
//		relationTree.addValue(15);
//		relationTree.addValue(14);
//		relationTree.addValue(13);
//		relationTree.display();
//	}
}
