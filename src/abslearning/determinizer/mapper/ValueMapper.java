package abslearning.determinizer.mapper;

import java.util.Collection;

import com.google.common.collect.BiMap;

import abslearning.exceptions.ConfusionException;

/**
 * The purpose of a value mapper is to map a random sequence of values to a canonical one,
 * in which all recognized relations between elements are preserved.
 * 
 * For example the list:  
 * 39 40 190 190 191 
 * is mapped to:
 * -100 -101 -200 -200 -201 
 * 
 * How are the canonical values generated is a function of the value store provider used. 
 * 
 */
public interface ValueMapper{
	public BiMap<Integer, Integer> buildCanonicalMap (Collection<Integer> values) throws ConfusionException;
	public void reset();
}
