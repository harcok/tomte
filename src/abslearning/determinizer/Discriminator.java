package abslearning.determinizer;

import java.util.List;

import abslearning.trace.action.Action;

/**
 * The sole purpose of this class is to discriminate between learner and system generated values.
 * Because we use the convention that learner values are positive whereas system values are negative, this is
 * very easily implemented.
 */

public interface Discriminator {
	public void reset();
	public List<Integer> discriminate(Action action);
}
