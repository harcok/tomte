package abslearning.trace;

import java.util.List;

import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.stores.FreshOutputValueStore;

public class FreshValueStoreBuilder implements CanonicalValueStoreProvider {
	private List<Integer> excludedValues;

	public FreshValueStoreBuilder(Trace trace) {
		this.excludedValues = trace.getAllOutputIntegerValues();
	}

	@Override
	public CanonicalValueStore newValueStore() {
		return new FreshOutputValueStore(this.excludedValues);
	}

}
