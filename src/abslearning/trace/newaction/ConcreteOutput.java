package abslearning.trace.newaction;


import java.util.List;

import statemachine.model.elements.action.DataOutputActionType;

public final class ConcreteOutput extends BaseConcrete{

    public ConcreteOutput(DataOutputActionType actionType, List<Integer> parameterValues) {
        super(actionType, parameterValues);

        // TODO: check using actionType whether we have the right number and type of  parameters
    }

    @Override
    public String toString() {
        return "!" + super.toString();
    }

    @Override
    public DataOutputActionType getType() {
        return (DataOutputActionType) this.type; // assume type is immutable!
    }
}
