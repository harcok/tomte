package abslearning.trace.newaction;

import java.util.List;

import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

public class LegacyUtil {
    // convert new input to legacy input
     static public InputAction convertConcreteInput2InputAction(ConcreteInput input) {
        String name = input.getType().getName();
        List<Integer> intparams=input.getArgumentValues();
        return new InputAction(intparams,name);
    }

    static public OutputAction convertConcreteOutput2InputAction(ConcreteOutput output) {
        String name = output.getType().getName();
        List<Integer> intparams=output.getArgumentValues();
        return new OutputAction(intparams,name);
    }
}
