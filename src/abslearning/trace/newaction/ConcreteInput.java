package abslearning.trace.newaction;

import java.util.List;

import statemachine.model.elements.action.DataInputActionType;

public final class ConcreteInput extends BaseConcrete{

    public ConcreteInput(DataInputActionType actionType, List<Integer> parameterValues) {
        super(actionType, parameterValues);

        // TODO: check using actionType whether we have the right number and type of  parameters
    }

    @Override
    public String toString() {
        return "?" + super.toString();
    }

    @Override
    public DataInputActionType getType() {
        return (DataInputActionType) this.type; // assume type is immutable!
    }
}
