package abslearning.trace.newaction;

import org.antlr.symtab.Type;
import org.antlr.symtab.TypedSymbol;
import org.eclipse.jdt.annotation.Nullable;

import com.google.common.collect.ImmutableList;

import statemachine.model.elements.action.BaseDataActionType;

public abstract class BaseConcrete  implements TypedSymbol{


    protected final Type type;

    protected final ImmutableList<Integer> parameterValues;


    @SuppressWarnings("null") // to suppress warning for nonNull unchecked conversion with
                              // ImmutableList.copyOf(values) assignment
    public BaseConcrete(BaseDataActionType actionType, Iterable<Integer> values) {
        this.type = actionType;
        this.parameterValues = ImmutableList.copyOf(values);
    }

    /*
     * => disabled anyway because type made final in this class However still
     * inherit from TypedSymbol to classify like that in symtab library
     */
    public void setType(@Nullable Type type) {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @return immutable List of values
     */
    public ImmutableList<Integer> getArgumentValues() {
        return parameterValues;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(this.type.getName());
        result.append("(");
        if (parameterValues.size() != 0) {
            // add each value followed by a ','
            for (Integer p : parameterValues) {
                result.append(p.toString()).append(",");
            }
            // delete last ','
            result.deleteCharAt(result.length()-1);
        }
        result.append(")");
        return result.toString();
    }

}
