package abslearning.trace;

import java.util.SortedMap;

import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

public class Transition {
	public final SortedMap<Integer,Integer> stateVarsAfter;
	public final SortedMap<Integer,Integer> stateVarsBefore;
	public final InputAction input;
	public final OutputAction output;
	public final int index;
	
	public Transition(InputAction input, OutputAction output, int index, 
			SortedMap<Integer, Integer> stateVarsBefore, SortedMap<Integer, Integer> stateVarsAfter) {
		super();
		this.stateVarsAfter = stateVarsAfter;
		this.stateVarsBefore = stateVarsBefore;
		this.input = input;
		this.output = output;
		this.index = index;
	}
	
	public String toString() {
		return this.stateVarsBefore.values() + " " + input.toString() + "/" + output + " " + this.stateVarsAfter.values();  
	}
}
