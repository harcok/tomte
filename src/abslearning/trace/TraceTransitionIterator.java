package abslearning.trace;

import java.util.Iterator;

public class TraceTransitionIterator implements Iterator<Transition>{

	private Trace trace;
	private int currentTransition;

	public TraceTransitionIterator(Trace trace) {
		this.trace = trace;
		this.currentTransition = -1;
	}
	
	public TraceTransitionIterator(Trace trace, int fromTransition) {
		this.trace = trace;
		this.currentTransition = fromTransition -1;
	}
	
	public boolean hasNext() {
		return trace.getTransitionIndex() > this.currentTransition;
	}
	
	
	public Transition next() {
		if (hasNext()) {
			this.currentTransition ++;
			return buildTransition(this.currentTransition, this.trace);
		}
		return null;
	}
	
	
	private Transition buildTransition(int currentTransition, Trace trace) {
	    
		return new Transition(trace.getInputAction(currentTransition),
				trace.getOutputAction(currentTransition), currentTransition,
				trace.getStateVarsBeforeTransition(currentTransition),
				trace.getStateVarsAfterTransition(currentTransition));
	}
}
