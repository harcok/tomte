package abslearning.trace.action;

import abslearning.exceptions.BugException;
import abslearning.exceptions.NotImplementedException;
import abslearning.exceptions.UpdateConstantActionException;


public class Parameter implements sut.interfaces.Parameter {
	private Integer abstractValue;
	private ParameterValue concreteValue;
	private int parameterIndex;
	private Action action;

	public Parameter(Integer abstractValue, ParameterValue concreteValue, 
			 int parameterIndex, Action action) {
		this.abstractValue = abstractValue;
		this.concreteValue = concreteValue;
		this.parameterIndex = parameterIndex;
		this.action = action;
	}

	public int getValue() {
		Integer value=this.getIntegerValue();
		if ( value==null ) {
			throw new BugException(abslearning.exceptions.Messages.UNKNOWN_TYPE_PARAMETER_VALUE);
		} else  {
			return value.intValue();
		}
	}	
	
	public Integer getIntegerValue() {

		if ( concreteValue instanceof IntegerParamValue ){
			return ((IntegerParamValue) concreteValue).getValue();
		} else if (  concreteValue instanceof BooleanParamValue  ) {
			boolean val=((BooleanParamValue) concreteValue).getValue().booleanValue();
			if ( val ) {
				return new Integer(1);
			} else {
				return new Integer(0);
			}
		} else {
			return null;
			//throw new BugException(abslearning.exceptions.Messages.UNKNOWN_TYPE_PARAMETER_VALUE);
		}
	}	
	
	public String getConcreteValueAsString() {

		if ( concreteValue instanceof IntegerParamValue ){
			return ((IntegerParamValue) concreteValue).getValue().toString();
		} else if (  concreteValue instanceof BooleanParamValue  ) {
			boolean val=((BooleanParamValue) concreteValue).getValue().booleanValue();
			if ( val ) {
				return "T";
			} else {
				return "F";
			}
		} else {
			return "?"; // means unset 
		}
	}	
	
	public Integer getAbstractValue() {
		return abstractValue;
	}
	
	public String getAbstractValueAsString() {
		if (abstractValue == null ) return "?";
		return abstractValue.toString();
	}	

	public ParameterValue getConcreteValue() {
		return concreteValue;
	}

	public int getParameterIndex() {
		return parameterIndex;
	}

	@Override
	public int hashCode() {
		 throw new NotImplementedException();
	}	
	
	@Override
	public boolean equals(Object obj) {
		throw new NotImplementedException();
	}

	
	@Override
	public String toString() {
		return concreteValue + "[" + abstractValue + "]";
	}
	
	public String toStringWithAction() {
		return new StringBuilder().append( this.getAction().getMethodName()).append( ".p").
		append( this.getParameterIndex()).append("[").append(this.getValue()).append("]").toString();
	}

	public String toStringComplete() {
		return "(value:"+ concreteValue + "[" + abstractValue + "],traceIndex:" + action.getTraceIndex() + ",actionName:" + action.getMethodName() +  ",paramIndex:" + parameterIndex +")";
	}	
	
	public String toStringConcrete() {
		return concreteValue.toString();
	}	

	public void setConcreteValue(ParameterValue concreteValue) {
		if (action.isConstantAction()) {
			throw new UpdateConstantActionException();
		}
		this.concreteValue = concreteValue;
	}

	public void setAbstractValue(Integer abstractValue) {
		if (action.isConstantAction()) {
			throw new UpdateConstantActionException();
		}
		this.abstractValue = abstractValue;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}


}
