package abslearning.trace.action;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.exceptions.BugException;
import abslearning.exceptions.NotImplementedException;
import abslearning.exceptions.UpdateConstantActionException;
import abslearning.trace.Trace;


/*

From outside Action uses an abstraction string and concrete params only
but internally for some learning methods still abstract parameters are
used instead of abstraction string. 

In those case you can access the  params and update an abstract param.
You can only update absract params by this internal access, never directly
by calling a method on the action class!! 

*/

public abstract class Action implements Comparable<Action>, sut.interfaces.Action{
	private static final Logger logger = Logger.getLogger(Action.class);
	public static final String CONSTANT_ACTION_NAME = "_CONSTANTS_";

	private int traceIndex; // index of this action in trace 
	
	protected String methodName; 
	// since there are a limited number of inputs/outputs names, we store internalized versions to save memory
	private int methodIndex;  // index of current methodName in trace  
	

	protected List<Parameter> parameters;  // concrete and abstract parameters (latter should be replaced abstraction string)
	protected String abstractionString;	   // abstraction of action represented in a string

    
	
   public static <E extends Action> boolean  concreteEquals(List<E>a, List<E>b){
    	for ( int i=0; i< a.size(); i++) {
    		if ( ! (a.get(i)).concreteEquals( b.get(i)) ) return false;
    	}
    	return true;
    }
	
   // changes list of actions inline
	public static <E extends Action> int replaceConcreteValue( List<E> actions,ParameterValue oldValue, ParameterValue newValue ) {
		int replacesDone=0;
		for ( int i=0; i< actions.size(); i++) {   
			replacesDone=replacesDone+actions.get(i).replaceConcreteValue(oldValue, newValue);
		}
		return replacesDone;
	}  
	
	public static <E extends Action> LinkedHashSet<Integer> getValues(List<E> actions){
   	    // get all output values
   	    LinkedHashSet<Integer> outputsFromActions= new LinkedHashSet<Integer>();
   	    for ( Action action  : actions) {
   	    	ArrayList<Integer> values=action.getConcreteParameters();
   	    	outputsFromActions.addAll( values );     
   	    } 
   	    return outputsFromActions;
	}	
	
//---------------------------------------------------------------------------------------------- 	
// copy constructor
//	  does full deep copy            
//---------------------------------------------------------------------------------------------- 	
		
    // copy constructor 
	/* DEEP copy action from another action */
	// note : everything will be copied except ParamValue(s) for the concrete Parameters 
	//        because they are shared between actions. 
	//        This is not a problem because a ParamValue is created once and never changed afterwards, 
	//        representing an immutable value.		
	public Action(abslearning.trace.action.Action action) {
		//this(action.getMethodName(), action.getParameters()); // deep copy concrete (and abstract) parameters
		
		this.methodName = action.getMethodName().intern(); // deep copy string
		
		// deep copy parameters
		this.parameters = new ArrayList<Parameter>();
		for (Parameter parameter : action.getParameters()) {
			Parameter newParam = new Parameter(parameter.getAbstractValue(), parameter.getConcreteValue(), parameter.getParameterIndex(),this);
			this.parameters.add(newParam);
		}
		
		
		this.traceIndex = action.traceIndex;  // int always deep copy
		this.methodIndex = action.methodIndex;  // int always deep copy
	//	this.setAbstractionString(action.getAbstractionString());  // deep copy abstraction string
		this.abstractionString=action.abstractionString;
	}	
	
	// DEEP copy action followed by setting all values to the ones given in the list. 
	public Action(List<Integer> values, abslearning.trace.action.Action action) {
		this(action);
		this.setConcreteValues(values);
	}
	
	public Parameter getFirstParamWithValue(Integer value) {
		for (Parameter param : this.parameters) {
			if (value.equals(param.getIntegerValue())) {
				return param;
			}
		}
		return null;
	}
	

	/* replaceConcreteValue - replace multiple occurrences of a concrete value by another concrete value 
	 * 
	 *  ParameterValue is immutable, thus we must replace the ParameterValue
	 */
	public int replaceConcreteValue( ParameterValue oldValue, ParameterValue newValue ) {
		// replace oldValue by newValue in action
		int replacesDone=0;
		for(Parameter param : this.parameters){
			if(param.getConcreteValue().equals(oldValue)){
				param.setConcreteValue(newValue);
				replacesDone++;
			}
		}	
		return replacesDone;
	}		

	
//---------------------------------------------------------------------------------------------- 	
// basic constructors 
//    abstract constructor	
//    concrete constructor	
//---------------------------------------------------------------------------------------------- 	
		
	
	/* Abstract constructor  (DEEP copy)
	 * Creates an action from the symbol (as String) received from LearnLib/the hypothesis,
	 * meaning create action from abstract action serialized as String.
	 */	
	public  Action(String abstractionString){
		setAbstractionString(abstractionString);
	};  
			
	// Concrete constructor 
	// DEEP copy  : copy concrete parameters + string copy methodname			
	public Action( List<Integer> values  , String methodName) {
		// note: swapped params and methodname to prevent type clash with  Action(String methodName, List<Parameter> parameters)
		
		this.methodName = methodName.intern();
		this.setConcreteValues(values);
	}


    // Abstract and concrete constructor   (DEEP copy)
	public Action(String methodName,  List<Integer> values  , String abstractionString) {
		this.methodName = methodName.intern();
		this.setAbstractionString(abstractionString);
		this.setConcreteValues(values);
	}		
	
//---------------------------------------------------------------------------------------------- 	
// CONCRETE constructors from other action type classes => always deep copies
//			    * action used in concrete tree 
//              * action used in communication with SUT
//---------------------------------------------------------------------------------------------- 	
	


	/* set action from a concrete tree action used within the concrete tree (deep copy always) */
	public Action(abslearning.tree.action.Action action) {
		this.methodName = action.getMethodName().intern();
		this.parameters = new ArrayList<Parameter>();
		int paramIndex = 0;
		for ( abslearning.tree.action.Parameter p: action.getParameters() ) {
			Parameter newParam=new abslearning.trace.action.Parameter(null, new IntegerParamValue(p.getConcreteValue()), paramIndex,this);
			this.parameters.add(newParam);
			paramIndex = paramIndex+1;					
		}			
	}
	
	/* set action from a concrete action used with SUT  (deep copy always) */
	public Action(sut.interfaces.Action action) {
		this.methodName = action.getMethodName().intern();
		this.parameters = new ArrayList<Parameter>();
		int paramIndex = 0;
		for ( sut.interfaces.Parameter p: action.getParams() ) {
			Parameter newParam=new abslearning.trace.action.Parameter(null, new IntegerParamValue(p.getValue()), paramIndex,this);
			this.parameters.add(newParam);	
			paramIndex = paramIndex+1;	
		}			
	}	
	
//---------------------------------------------------------------------------------------------- 	
//   special CONSTANTS action identified by methodname==CONSTANT_ACTION_NAME
//    => in params and abstraction is checked it is not modified!!
//	
//   note: constantsAction not in trace 
//          but used in black edges to let a param refer to a constant's action param 	
	
//---------------------------------------------------------------------------------------------- 	
	
	public boolean isConstantAction() {
		if ( methodName == null ) return false;
		return methodName.equals(CONSTANT_ACTION_NAME);
	}	
	
		
//---------------------------------------------------------------------------------------------- 	
// implementation of Interface:  sut.interfaces.Action
//		                                  `-> so that we can pass action directly to Sut instance	
//---------------------------------------------------------------------------------------------- 	
	
	
	
	/* needed for interface "sut.interfaces.Action" 
	 * so that we can pass action directly to Sut instance
	 * 
	 *   get  methodname of type String
	 */	
	public String getMethodName() {
		return methodName;
	}
	
	/* needed for interface "sut.interfaces.Action" 
	 * so that we can pass action directly to Sut instance
	 * 
	 *   get  params of type sut.interfaces.Parameter 
	 */
	public List<sut.interfaces.Parameter> getParams() {
		 ArrayList<sut.interfaces.Parameter> iparams = new ArrayList<sut.interfaces.Parameter> ();  
		 for (Parameter parameter : parameters) {
			 iparams.add(parameter);  // parameter implements sut.interfaces.Parameter
		 }
		 return iparams ;
	}		
	
	
//---------------------------------------------------------------------------------------------- 	
	

	// We assume here that InputAction and OutputAction will remain the only types of Action, so instanceof doesn't
	// hurt as much
	public Action clone() {
		Action clonedAction = null;
		if(this instanceof OutputAction) {
			clonedAction = new OutputAction(this);
		} else {
			if(this instanceof InputAction) {
				clonedAction = new InputAction(this);
			}
		}
		if(clonedAction == null) {
			throw new BugException("Should always be able to clone");
		}
		return clonedAction;
	}
	
	public int getNumberParams() {
		if (this.parameters!=null){ 
			return this.parameters.size();
		} else {
			return 0;
		}
		
	}
	
	public int getTraceIndex() {
		return traceIndex;
	}
	
	public int getTransitionIndex() {
		return traceIndex/2;
	}
	

	public int getMethodIndex() {
		return methodIndex;
	}	
	
	public void setTraceIndex(int traceIndex) {
		this.traceIndex = traceIndex;
	}

	public void setMethodIndex(int actionIndex) {
		this.methodIndex = actionIndex;
	}
	
	
	


// ------------------------------------------------------------------------------------------		
//  methods specifically for Concrete parameters 		
// ------------------------------------------------------------------------------------------

	
	//POLICY
	// get concrete parameters (as integers)
    //		
	// note:  only call when it is guaranteed that all concrete parameters are set, 
	//	      because if some or all of them are not set then an BugExeption is thrown
	public ArrayList<Integer> getConcreteParameters() {
		
		ArrayList<Integer> concreteParams= getDefinedConcreteParametersSafe();
		if ( concreteParams == null ) {
			throw new BugException("concrete parameters must first all be set!");
		}		
		return concreteParams;
	}	
	
	public ArrayList<Integer> getConcreteParametersSoFarDefined() {
		
		ArrayList<Integer> concreteParams=  getConcreteParametersSafe();
		return concreteParams;
	}	
	
	
	//MECHANISM
	// get concrete parameters (as Integer)
	//  -  if no concrete parameters are set : return null
	//  -  if action has no concrete parameters : return empty list 	
	//  -  only some concrete parameters are set : 
	//         an undefined param will be represented as value null in list returned
	private ArrayList<Integer> getConcreteParametersSafe() {
		
		if ( parameters == null ) {
			return null;
		}				
		
		ArrayList<Integer> concreteParams=new ArrayList<Integer>();		
		for (Parameter p : parameters) {
			if(p.getConcreteValue()!=null){
				concreteParams.add( p.getIntegerValue() ); 
			} else {
				concreteParams.add(null);
			}
		}

		return concreteParams;
	}	
	
	//MECHANISM
	// get fully set concrete parameters (as integers)
	//  -  if no concrete parameters are set : return null
	//  -  if action has no abstract parameters : return empty list 	
	//  -  only some concrete parameters are set : return null       
	private ArrayList<Integer> getDefinedConcreteParametersSafe() {
		ArrayList<Integer> params= getConcreteParametersSafe();
		if ( params == null ) {
			return null;
		}		
		if ( params.contains(null) ) {
			return null;
		}		
		return params;
	}
	
	/*  setConcreteParameter - sets value in concrete parameter at given index 
	 */
	public void setConcreteParameter(ParameterValue concreteValue,int position) {
		assert (position < parameters.size()) : ("parameter index out of bounds! " + concreteValue + " " + getMethodName());	
		parameters.get(position).setConcreteValue(concreteValue);
	}	
	
	/*  setConcreteValue - sets value in concrete parameter at given index 
	 */	
	public void setConcreteValue(Integer concreteValue,int position)
	{	
		IntegerParamValue param=new IntegerParamValue(concreteValue);
		setConcreteParameter(param, position);
	}
	
	
	
	
	
	/* 
	 *  setConcreteParameters - sets or updates  concrete parameters in action
	 *                          and leaves abstraction value already set in action unaffected!                    
	 *   note:
	 *    - if already concrete parameters are set, then the values are updated, however
	 *      it is required that have the same size of input values as parameters set  
	 *    - if no parameters are already initialized then the are created on the fly
	 */	
	public void setConcreteParameters(Trace currentTrace, InputAction input, List<ParameterValue> concreteValues) {
		
		setConcreteParameters(concreteValues); 
	}
	
	/* 
	 *  setConcreteParameters - sets or updates concrete parameters in action
	 *                          and leaves abstraction value already set in action unaffected!                    
	 *  
	 *   note:
	 *    - if already concrete parameters are set, then the values are updated, however
	 *      it is required that have the same size of input values as parameters set  
	 *    - if no parameters are already initialized then the are created on the fly 
	 */
	public void setConcreteParameters( List<ParameterValue> concreteValues) {
		//concreteSet=true;
		if (parameters==null) {		
			// create new parameters
			parameters = new ArrayList<Parameter>();	
			int paramIndex = 0;
			for ( ParameterValue concreteValue: concreteValues ) {
				Parameter newParam=new abslearning.trace.action.Parameter(null,concreteValue, paramIndex,this);
				parameters.add(newParam);
				paramIndex = paramIndex+1;					
			}				
		} else {
			assert ( concreteValues.size() == parameters.size()) : ("Setting wrong number of concrete parameterValues! " + concreteValues + " " + getMethodName());

			// update already existing parameters
			for (int i = 0; i < parameters.size(); i++) {
				parameters.get(i).setConcreteValue(concreteValues.get(i));
			}
		}
		
	}	
	
	
	/* 
	 *  setConcreteValues - sets or updates  concrete parameters in action
	 *                      and leaves abstraction value already set in action unaffected! 
	 *   note:
	 *    - if already concrete parameters are set, then the values are updated, however
	 *      it is required that have the same size of input values as parameters set  
	 *    - if no parameters are already initialized then the are created on the fly
	 *    
	 *     
	 */	
	public void setConcreteValues(List<Integer> values)
	{	
		
		List<ParameterValue> concreteValues= new  ArrayList<ParameterValue>();
		for ( Integer value: values ) {
			concreteValues.add(new IntegerParamValue(value)); 
		}
		setConcreteParameters(concreteValues);

	}
	
	
	

// ------------------------------------------------------------------------------------------		
//  helper	method to make  Abstraction String from abstraction params 		
//------------------------------------------------------------------------------------------


    /* setAbstraction_forAbstractionUsingArrayIndicesAsSuffix
     * 
     *    when using abstract params in abstraction string using this method
     *    we can set/update the abstract parameters in the action
     *    without affecting the current concrete parameters 
     *    
     *    note: used only in setAbstractionString method implemented in Input/OutputAction classes
     *          and then only for specific learning methods  
     */
	protected void setAbstraction_forAbstractionUsingArrayIndicesAsSuffix(String abstractActionAsString) {
        // separator used in abstraction
		String separator="_";

		// get methodname and parameter values from  abstractActionAsString
		List<Integer> values; 
		String newMethodName = util.JavaUtil.getPrefixFromString(abstractActionAsString,separator);
	    if 	( newMethodName == null ) {
	    	// no parameters in abstractActionAsString
	    	newMethodName = abstractActionAsString; // always update, could be not yet set
	    	
	    	values= new ArrayList<Integer>();
	    } else {
	    	String serializedValues=util.JavaUtil.removePrefixFromString(abstractActionAsString, separator);
	        values=util.JavaUtil.deserializeIntValues(serializedValues,separator);
	    }   
		
		if ( methodName == null ) {		// action in total not yet set	(nor abstract nor concrete)
	        methodName=newMethodName; 			
		}  else {	
			// check valid method name in abstraction
			if (! methodName.equals(newMethodName) ) {
			 //	throw new BugException("cannot update current action=" + this  + " with abstractionString=" + abstractActionAsString +  " with different method name ");
			}
		}    
	    
	    // create/update parameters from values
	    if  ( parameters == null ) {  //  concrete value not yet set
	    	// create new parameters
	        parameters = new ArrayList<Parameter>();
			
			int paramIndex = 0;
			for ( Integer value: values ) {
				Parameter newParam=new abslearning.trace.action.Parameter(value, null, paramIndex,this);
				parameters.add(newParam);
				paramIndex = paramIndex+1;					
			}
	    } else {
	    	// update existing parameters 
	    	assert parameters.size() == values.size() : "cannot update action '" + this.toString() + "' with abstraction '" + abstractActionAsString + "' with different number of abstract parameters ";
			for (int i = 0; i < parameters.size(); i++) {
				parameters.get(i).setAbstractValue(values.get(i));
			}        	
	    }
	    
	}	
	
	// get method name from abstractPrefixSuffixOutput
	private String getMethodNameFromAbstractionString( String abstractString )	
	{    
	    String[] pieces = abstractString.split("_");
	    for(String piece : pieces) 
	    {
			if(  piece.startsWith("O") ||  piece.startsWith("I") )
			{
				return piece; 					    
			}/* else {
				if ( ! ( piece.startsWith("c") ||  piece.startsWith("x") || piece.startsWith("p")  ) ) {
					//throw new BugException(Messages.BUG);
				}	
			}*/
	    }	
	    throw new BugException("Error: Cannot retreive method name from abstraction string: " + abstractString );
	    
	}	
    
// ------------------------------------------------------------------------------------------		
//  set/get  Abstraction String 		
//------------------------------------------------------------------------------------------

	// set/getAbstractionString is implemented in InputAction and OutputAction classes
	// because that is specific for the kind of abstraction used

	

	// get type of abstraction used internally for action : abstraction string or abstraction parameters
	//
	// note: per mode we have different output abstractions thus also
	//       the abstract string representing the abstract output give 
	//       to learnlib is different		
	abstract public boolean usesAbstractionString();	// implemented in InputAction and OutputAction classes	
                                                        // because that is specific for the kind of abstraction used         
	

	// set abstraction string
	//
	// note: per mode we have different output abstractions thus also
	// the abstract string representing the abstract output give 
	// to learnlib is different
	public void setAbstractionString(String givenAbstractionString) {	
		if (this.isConstantAction()) {
			throw new UpdateConstantActionException();
		}
		if (usesAbstractionString()) { 
		
            // 1. if method not yet initialized first set method
			//    and if method already set check it is the same as in abstraction string
			//  get method name for abstract output
			String newMethodName=getMethodNameFromAbstractionString(givenAbstractionString);
			if ( methodName == null ) {			
		        methodName=newMethodName; //  action not yet set	
		        //                               `-> thus no concrete parameters yet set  (still params=null)
		        //                                   and setting first the abstraction in action             
			}  else {	
				// check valid method name in abstraction
				if (! methodName.equals(newMethodName) ) {
			//		throw new BugException("cannot update current action=" + this  + " with abstractionString=" + givenAbstractionString +  " with different method name ");
				}
			}    			
			
			// 2. set abstraction string
			abstractionString=givenAbstractionString;
			
			
		} else {
			setAbstraction_forAbstractionUsingArrayIndicesAsSuffix(givenAbstractionString);
		}
	}		
	
	
	
	// returns null if not (fully) defined
	// note about fully: if some abstraction parameters are set but not all are defined also return null
	public String getAbstractionString() {	
		if (usesAbstractionString()) { 
			if ( abstractionString == null) {
	  		   return null;
      	    } else {
			    return abstractionString;
			} 
		}  else {
			List<Integer> abstractParameters=this.getDefinedAbstractParametersSafe();
			if ( abstractParameters == null ) {
				return null;
			} else {
               return methodName + util.JavaUtil.serializeListAsSuffixes(abstractParameters);
			}  			
		}
    }		
	
// ------------------------------------------------------------------------------------------		
//  toString		
// ------------------------------------------------------------------------------------------
				

    // does print as much as possible to print all set abstraction info in string
	// note: if abstraction parameters used and they are not all set, still include info about ones set!
	public String toStringAbstract() {	
		if (usesAbstractionString()) {  
			if ( abstractionString == null) {
				return methodName + "_?"; // single ? => signifies single abstraction string
	      	} else {
				return abstractionString;
			} 
		}  else {
			List <Integer> abstparams= this.getAbstractParametersSafe();
			if ( abstparams == null ) {
				return methodName + "_???"; // multiple ? => signifies multiple abstraction params
			} else {
                return methodName + util.JavaUtil.serializeListAsSuffixes(abstparams);
			}   			
		}   
    }		


	protected String toStringConcreteParams() {
		List <Integer> concreteParams=this.getConcreteParametersSafe();
		if ( concreteParams==null ) {
			return  "(???)";
		} else {
		    return util.JavaUtil.serializeListAsFunctionCall(this.getConcreteParametersSafe());
		}   		
	}
	
	public String toString() {	
		return toStringAbstract() +  toStringConcreteParams();
	}			
	
	public String toStringConcrete() {
		return methodName +  toStringConcreteParams();
	}	
	

	
	
	
	/*
	public String toStringFull() {	
		if (getNumberParams()>  0) {
		  // return getAbstractionString() +  util.JavaUtil.serializeListAsFunctionCall(this.getConcreteParams()) + "_" + traceIndex  + "_" + methodIndex;
		   return toStringAbstract() +  util.JavaUtil.serializeListAsFunctionCall(this.getConcreteParameters()) + "_" + traceIndex  + "_" + methodIndex;
		} else {
		   return methodName + "()";	
		}
	}	*/
	
	public String toStringFull() {	
		   return   "abstr:"+toStringAbstract() + " concr:" + toStringConcrete() + " trIndx:" + traceIndex  + " methIndx:" + methodIndex;
	}	
	
// ------------------------------------------------------------------------------------------		
//  compareTo		
// ------------------------------------------------------------------------------------------
		
	
    /* 
     * compareTo : used to check order of action in trace
     */
	public int compareTo(Action o) {
		assert (o instanceof Action) : ("Comparing action to non-action");
		
		Action that = (Action)o;
		
		if (traceIndex < that.traceIndex) {
			return -1;
		} else if (traceIndex == that.traceIndex) {
			return 0;
		} else {
			return 1;
		}
	}
	
	
	
// ------------------------------------------------------------------------------------------		
//  hashCode  (depends on definition equals) 		
// ------------------------------------------------------------------------------------------
		
	
	
	@Override
	public int hashCode() {
		throw new NotImplementedException();
	}	

	
// ------------------------------------------------------------------------------------------		
//  equals (depends on definition equals) 		
// ------------------------------------------------------------------------------------------
		
	
    /*
     * equals :  if exact the same action in Trace, thus also same index in trace!
     * 
     */
	@Override
	public boolean equals(Object obj) {
		
		assert obj != null : "to compare actions they must be set";
		
		if (this == obj)
			return true;

		if (!(obj instanceof Action))
			return false;

		Action that = (Action) obj;

		if (!methodName.equals(that.methodName)) {
			return false;
		}
        if ( parameters == null  && that.parameters != null ){
        	return false;
        }
		if (parameters!=null && !parameters.equals(that.parameters)) {
			return false;
		}

		if (traceIndex != that.traceIndex) {
			return false;
		}

		if (methodIndex != that.methodIndex) {
			return false;
		}

		return true;
	}
	


	public boolean abstractAndConcreteEquals(Action action) {
        return abstractEquals(action) && concreteEquals(action);
	}

	
    // note:  abstractEquals uses getAbstractionString which is implemented 
	//        in child class in the right way for that learning mode
	public boolean abstractEquals(Action action) {
		assert action != null : "to compare actions they must be set";

		if (this == action) return true;		
		
		return  this.getAbstractionString().equals(action.getAbstractionString());
	}
	

	
	public boolean concreteEquals(Action action) {
		
		logger.debug("COMPARE " + this + " TO " + action);
		if (!methodName.equals(action.methodName)) {
			return false;
		}
				
		// first check if they have same number of params
		if (getNumberParams() != action.getNumberParams()) {
			return false;
		}
	
		// only compare concrete params  if you have params
		// -> two actions without params are concrete equals in concrete params
		if ( getNumberParams() > 0 ) {
			for (Parameter parameter : parameters) {
					if (!parameter.getConcreteValue().equals(action.getParam(parameter.getParameterIndex()).getConcreteValue())) {
						return false;
					}
			}
		}
		return true;
	}



// ------------------------------------------------------------------------------------------		
//  flattening		
// ------------------------------------------------------------------------------------------
		
		
	/*
	 * flattens concrete methodname and its parameters into a concrete actions with just only a methodname 
	 * in which the params are part of the methodname of the new action
	 *  
	 * note: flattening of the params into the methodname is done to be able to learn machines
	 *       with data in their params using standard learnlib
	 */
	public void flatten() {		
		this.methodName = this.methodName + util.JavaUtil.serializeListAsSuffixes( this.getConcreteParameters() );
		this.parameters = new ArrayList<Parameter>();	
		logger.debug("flattened input:" +this.methodName);
	}	
	
    /*
     * do inverse of flatten: see flatten
     */
	public void deflatten() {
		    String flattenedActionString=this.getMethodName();
		    logger.debug("output before deflatten:" + flattenedActionString);
		 
			String separator="_";
			methodName = util.JavaUtil.getPrefixFromString(flattenedActionString,separator);
		
			
		    if 	( methodName.equals(flattenedActionString) ) {
		    	// no parameters
		    	parameters = new ArrayList<Parameter>();
		    } else {
		    	String serializedValues=util.JavaUtil.removePrefixFromString(flattenedActionString, separator);
		        List<Integer> values=util.JavaUtil.deserializeIntValues(serializedValues,separator);
		        
		        parameters = new ArrayList<Parameter>();
		        setConcreteValues(values);	        
		    }	
	}	

		
		
//===========================================================================================
//===========================================================================================
//  methods below  use  abstraction in parameters
//  => TODO: remove dependency on abstract parameters, instead only use abstraction String	
//===========================================================================================
//===========================================================================================
	
// ------------------------------------------------------------------------------------------		
//  methods specifically for Abstract + Concrete parameters 		
// ------------------------------------------------------------------------------------------
			
		// get parameters 
		//  -  if no  parameters are set : return null
		//  -  if action has no  parameters : return empty list 	
		//  -  note: parameters can be not set completely (concrete/abstract)
	    //
	    //TODO: currently does wrong:
        //   -  if no concrete parameters are set : returns empty list instead of null	
		public List<Parameter> getParameters() {
			if ( parameters == null ) {
				// TODO: FIX in code which calls this method 
				//     -> it should be sure concrete params set before calling this method !!       				
				//        and this method should return null if no parameters set!!

				//return null
				return  new ArrayList<Parameter>();
			}
			return new ArrayList<Parameter>(parameters);
		}

		public Parameter getParam(int index) {
			if ( parameters == null ) {
				throw new BugException("parameters must first be set!");
			}			
			return this.parameters.get(index);
		}
		
		public Parameter getParam(Integer index){
			if ( parameters == null ) {
				throw new BugException("parameters must first be set!");
			}			
			return this.parameters.get(index.intValue());
		}

		
//------------------------------------------------------------------------------------------		
//methods specifically for Abstract parameters 		
//------------------------------------------------------------------------------------------

		
	//POLICY
	// get abstract parameters (as integers)
    //		
	// note:  only call when it is guaranteed that all abstract parameters are set, 
	//	      because if some or all of them are not set then an BugExeption is thrown
	public ArrayList<Integer> getAbstractParameters() {
		
		ArrayList<Integer> abstractParams= getDefinedAbstractParametersSafe();
		if ( abstractParams == null ) {
			throw new BugException("abstract parameters must first all be set!");
		}		
		return abstractParams;
	}		
		
	//MECHANISM		
	// get abstract parameters (as integers)
	//  -  if no abstract parameters are set : return null
	//  -  if action has no abstract parameters : return empty list 	
	//  -  only some abstract parameters are set : 
	//         an undefined param will be represented as value null in list returned
	private ArrayList<Integer> getAbstractParametersSafe() {
		
		if ( parameters == null ) {
			return null;
		}			

		ArrayList<Integer> abstractParams=new ArrayList<Integer>();
		for (Parameter p : parameters) {
			abstractParams.add(p.getAbstractValue()); 
		}
		return abstractParams;
	}

	//MECHANISM
	// get fully set abstract parameters (as integers)
	//  -  if no abstract parameters are set : return null
	//  -  if action has no abstract parameters : return empty list 	
	//  -  only some abstract parameters are set : return null       
	private ArrayList<Integer> getDefinedAbstractParametersSafe() {
		ArrayList<Integer> params= getAbstractParametersSafe();
		if ( params == null ) {
			return null;
		}		
		if ( params.contains(null) ) {
			return null;
		}		
		return params;
	}	
	
	
	
	//  set abstract parameters (as integers) 
	//  note: there must be concrete or abstract parameters already set!)
	public void setAbstractParameters(List<Integer> abstractValues) {
		assert (abstractValues.size() == parameters.size()) : ("Setting wrong number of abstract parameterValues! " + abstractValues + " " + getMethodName());
		
		for (int i = 0; i < parameters.size(); i++) {
			parameters.get(i).setAbstractValue(abstractValues.get(i));
		}
	}			
		

		
	
}
