package abslearning.trace.action;

/**
 * A dirty hack used in a RelationPair abstraction in place of a constant.
 */
public final class ConstantParameter extends Parameter{

	public ConstantParameter(Integer constantValue) {
		super(-1, new IntegerParamValue(constantValue), -1, null);
	}
	
	public ConstantParameter(ParameterValue constantValue) {
		super(-1, constantValue, -1, null);
	}
	
	@Override
	public String toString() {
		return "c[" + this.getIntegerValue() + "]";
	}
	
	public String toStringWithAction() {
		return toString();
	}

	public String toStringComplete() {
		return toString();
	}	
	
	public String toStringConcrete() {
		return toString();
	}	

	public void setConcreteValue(ParameterValue concreteValue) {
	}

	public void setAbstractValue(Integer abstractValue) {
	}

}
