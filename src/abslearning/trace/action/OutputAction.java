package abslearning.trace.action;

import java.util.List;

import org.apache.log4j.Logger;

public class OutputAction extends Action implements sut.interfaces.OutputAction{
	private static final Logger logger = Logger.getLogger(OutputAction.class);
	
	public OutputAction(sut.interfaces.OutputAction  action) {
		super(action);
	}	
	
	


	
	// concrete constructor
	public OutputAction( List <Integer> parameters, String methodName) {
		super(parameters, methodName);
	}		

	public OutputAction(OutputAction action) {
		super((Action)action);
	}
	
	public OutputAction(Action action) {
		super(action);
	}
		
	
	public OutputAction(abslearning.tree.action.OutputAction action) {
		super(action);
	}	
	
	public OutputAction(String abstractOutputActionAsString) {
		super(abstractOutputActionAsString);
	}	
	

	
	// get type of abstraction used internally for action : abstraction string or abstraction parameters
	//
	// note: per mode we have different output abstractions thus also
	//       the abstract string representing the abstract output give 
	//       to learnlib is different	
	@Override
	public boolean usesAbstractionString() {
		return true;
	}	

	


}
