package abslearning.trace.action;

import java.util.List;

import org.apache.log4j.Logger;

public class InputAction extends Action implements sut.interfaces.InputAction{
	private static final Logger logger = Logger.getLogger(InputAction.class);	

	
	// concrete constructor
	public InputAction( List <Integer> parameters, String methodName) {
		super(parameters, methodName);
	}	

	public InputAction(abslearning.trace.action.InputAction action) {
		super((abslearning.trace.action.Action)action);
	}
	
	public InputAction(List<Integer> concreteParameters,
			abslearning.trace.action.InputAction concreteInput) {
		super(concreteParameters, concreteInput);
	}
	
	public InputAction(abslearning.trace.action.Action action) {
		super(action);
	}	
	
	public InputAction(abslearning.tree.action.InputAction action) {
		super(action);
	}		
	
	public InputAction(sut.interfaces.InputAction  action) {
		super(action);
	}	
	
	public InputAction(String abstractInputActionAsString) {
		super(abstractInputActionAsString);
	}
	

	// get type of abstraction used internally for action : abstraction string or abstraction parameters
	//
	// note: per mode we have different output abstractions thus also
	//       the abstract string representing the abstract output give 
	//       to learnlib is different		
	@Override
	public boolean usesAbstractionString() {
		return false;
	}
	
	
	
}
