package abslearning.trace.action;

/*
 *  BooleanParamValue - immutable object
 */
public class BooleanParamValue implements ParameterValue {
	private Boolean value;

	public Boolean getValue() {
		return new Boolean(value);
	}



	public BooleanParamValue(Boolean value) {
		this.value = value;
	}

	public BooleanParamValue(boolean value) {
		this.value = new Boolean(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BooleanParamValue)) {
			return false;
		}

		return value.equals(((BooleanParamValue) obj).getValue());
	}
	
	public boolean isRelated(ParameterValue obj) {
		if (equals(obj)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public String toString() {
		return value.toString();
	}
}
