package abslearning.trace.action;

public interface ParameterValue {
	/**
	 * If value <b>a</b> is related is value <b>b</b>, 
	 * there is relation <b>rel</b> so that <b>rel(b,a)</b> holds.
	 * <p></p>
	 * The order is important: 2 is related to 1 by way of the succ relation,
	 * but 1 is not related to 2.
	 */
	public boolean isRelated(ParameterValue paramValue);
}
