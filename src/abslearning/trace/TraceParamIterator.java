package abslearning.trace;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import abslearning.trace.action.*;

public class TraceParamIterator implements Iterator<Parameter>{	
	private Iterator<Parameter> paramIter;
	private List<Parameter> traceParams;
	private int paramIndex = 0;
	public TraceParamIterator(Trace trace) {
		this(trace, 0, 0);
	}
	
	private TraceParamIterator(List<Parameter> traceParams) {
		this.paramIter = traceParams.iterator();
	}
	
	public TraceParamIterator(Trace trace, Parameter fromParam) {
		this(trace, fromParam.getAction().getTraceIndex(), fromParam.getParameterIndex());
	}
	
	public TraceParamIterator(Trace trace, int actionIndex, int paramIndex) {
		this.traceParams = getParams(trace,actionIndex,paramIndex);
		this.paramIter = this.traceParams.iterator();
	}
	

	public boolean hasNext() {
		return paramIter.hasNext();
	}

	public Parameter next() {
		Parameter param = paramIter.next();
		paramIndex ++;
		return param;
	}
	
	public Parameter current() {
		return this.traceParams.get(paramIndex);
	}
	
	public TraceParamIterator remainingParamsIterator() {
		return new TraceParamIterator(traceParams.subList(paramIndex, traceParams.size())); 
	}
	
	public List<Parameter> remainingParams() {
		return Collections.unmodifiableList(traceParams.subList(paramIndex, traceParams.size()));
	}
	
	
	private List<Parameter> getParams(Trace trace, int startingActionIndex, int startingParamIndex) {
		boolean firstAction = true;
		List<Parameter> params = new ArrayList<Parameter>();
		for(int actionIndex = startingActionIndex; actionIndex < trace.size(); actionIndex ++) {
			List<Parameter> actionParams = trace.get(actionIndex).getParameters();
			if (firstAction) {
				actionParams = actionParams.subList(startingParamIndex, actionParams.size());
				firstAction = false;
			}
			params.addAll(actionParams);
		}
		return params;
	}
}
