package abslearning.trace;

import java.util.ArrayList;
import java.util.List;

import abslearning.exceptions.BugException;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

public class TraceBuilder {
	public static List<List<InputAction>> buildTreeInputActionLists(List<String> concreteInputActionSequences) {
		List<List<InputAction>> inputLists = new ArrayList<List<InputAction>>();
		if(concreteInputActionSequences != null)
			for(String testString : concreteInputActionSequences) {
				List<InputAction> inputList = buildInputActionList(testString);
				inputLists.add(inputList);
			}
		return inputLists;
	}
	
	/**
	 * Builds the list of input actions from a string with 
	 * input and output transitions, of the form: 
	 * 	<b>i1\o1 -> i2\o2 -> i3\o3 </b>, or from a string with only inputs: 
	 *  <b> i1 -> i2 -> i3 </b>.
	 */
    public static List<InputAction> buildInputActionList(String concreteTraceString) {
    	String [] transitionStrings = splitTrace(concreteTraceString);
    	List<InputAction> concreteInputs = buildInputActionList(transitionStrings);
    	return concreteInputs;
    }
    
    private static String [] splitTrace(String concreteTrace) {
    	return concreteTrace.split("\\s*\\-\\>\\s*");
    }
    
    public static Trace buildTrace(String concreteTraceString) {
    	String [] transitionStrings = splitTrace(concreteTraceString);
    	Trace trace = buildTrace(transitionStrings);
    	return trace;
    }
    
    //TODO nicer implementation
    public static boolean isStringTrace(String concreteString) {
    	String [] transitionStrings = splitTrace(concreteString);
    	if (transitionStrings.length > 0) {
    		return splitTransition(transitionStrings[0]).length == 2;
    	}
    	return false;
    }
    

    public static boolean isStringInputSequence(String concreteString) {
    	String [] transitionStrings = splitTrace(concreteString);
    	if (transitionStrings.length > 0) {
    		return splitTransition(transitionStrings[0]).length == 1;
    	}
    	return false;
    }
    
    private static Trace buildTrace(String[] transitionStrings) {
    	Trace trace = new Trace();
    	for(String transitionString : transitionStrings) {
    		String[] transitionSplit = splitTransition(transitionString); 
    		if (transitionSplit.length != 2) {
    			throw new BugException(transitionString + " is of bad form");
    		}
    		InputAction inputAction = buildInputActionFromString(transitionSplit[0]);
    		OutputAction outputAction = buildOutputActionFromString(transitionSplit[1]);
    		trace.add(inputAction);
    		trace.add(outputAction);
    	}
    	
    	return trace;
	}

	private static List<InputAction> buildInputActionList(String [] transitionStrings) {
    	List<InputAction> concreteInputs = new ArrayList<InputAction>();
    	for(String transitionString : transitionStrings) {
    		// in case it's in transition, we only select the input string ie from a / b -> c / d, we pick a and c
    		String[] transitionSplit = splitTransition(transitionString); 
    		if (transitionSplit.length > 1) {
    			transitionString = transitionString.split("/")[0];
    		}
    		InputAction inputAction = buildInputActionFromString(transitionString);
    		concreteInputs.add(inputAction);
    	}
    	return concreteInputs;
    }
    
    private static String [] splitTransition(String transition) {
    	return transition.split("/");
    }

	private static InputAction buildInputActionFromString(String inputString) {
		inputString = normalize(inputString);
		InputAction inputAction = new InputAction(new sut.implementation.InputAction(inputString));
		return inputAction;
	}
	
	private static OutputAction buildOutputActionFromString(String outputString) {
		outputString = normalize(outputString);
		OutputAction outputAction = new OutputAction(new sut.implementation.OutputAction(outputString));
		return outputAction;
	}
    
    private static String normalize(String inputString) {
    	inputString = inputString.trim();
    	String methodName = inputString.split("_|\\(")[0];
    	String methodParams = inputString.substring(inputString.indexOf('(')+1, inputString.indexOf(')'));
    	methodParams = methodParams.replaceAll(",", "_");
    	return methodName + "_" + methodParams; 
    			//inputString.trim().replaceAll("\\(|,", "_").replaceAll("\\)", "");
    }
    
}
