package abslearning.trace;

import java.util.Iterator;
import java.util.function.Predicate;

import abslearning.trace.action.Parameter;

public class ConditionalTraceParamIterator implements Iterator<Parameter>{

	private Iterator<Parameter> paramIter;
	private Predicate<Parameter> predicate;
	private Parameter currentParam;
	
	public ConditionalTraceParamIterator(Trace trace, Predicate<Parameter> predicate) {
		this.paramIter = new TraceParamIterator(trace);
		this.predicate = predicate;
	}
	
	public ConditionalTraceParamIterator(Trace trace, int actionIndex, int paramIndex, Predicate<Parameter> predicate) {
		this.paramIter = new TraceParamIterator(trace, actionIndex, paramIndex);
		this.predicate = predicate;
	}
	
	@Override
	public boolean hasNext() {
		boolean hasNext = false;
		while (currentParam == null || (paramIter.hasNext() && !predicate.test(currentParam))) {
			currentParam = paramIter.next();
		}
		if (paramIter.hasNext()) {
			hasNext = true;
		} else {
			hasNext = false;
		}
		
		return hasNext;
	}

	@Override
	public Parameter next() {
		if (hasNext()) {
			return currentParam;
		}
		return null;
	}

}
