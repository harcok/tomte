package abslearning.trace;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.log4j.Logger;

import abslearning.determinizer.Determinizer;
import abslearning.exceptions.BugException;
import abslearning.exceptions.Messages;
import abslearning.relation.Relation;
import abslearning.trace.action.Action;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.trace.action.Parameter;
import de.ls5.jlearn.interfaces.Word;
import de.ls5.jlearn.shared.SymbolImpl;
import de.ls5.jlearn.shared.WordImpl;

//public class Trace implements Comparator<Action> {
public class Trace implements Iterable<Parameter>{	
	private static final Logger logger = Logger.getLogger(Trace.class);
	
	private List<Action> trace;
	

	/*
	 * map which records the total amount of occurrences of a method name
	 */
	private Map<String, Integer> methodCountInTrace;
	private int traceIndex = -1;  // incremented when action (input or output added to trace)
	
	
	// each new input added to trace starts new transition
	// transition 0 is the first transition
	// the first action added to trace gets traceIndex 0
	// 
	// thus:
	//     transitionIndex = traceIndex/2 (integer division)
	//     inputIndex =  transitionIndex*2
	//     outputIndex =  transitionIndex*2+1;
	//
	// returns -1 if there are no transitions in trace 		
	public int getTransitionIndexFromTraceIndex(int traceIndexInput) {
		return traceIndex == -1? -1 : traceIndexInput/2;
	}	
	
	public int getTransitionIndex() {
		//return this.numberInputs-1;
		return getTransitionIndexFromTraceIndex(traceIndex);
	}	
	
	
	
	private  static InputAction constantsAction;
    public static List<Integer> constants= new ArrayList < Integer > ();
    
    
    
    /*
     *  constants in trace are stored as list of constants and in a special constantsAction
     *  note: latter is created once and used in all trace instances and is mainly
     *        used to get a param with a constant
     */
    public static void initConstants(List<Integer> constants) {
    	Trace.constants=constants;
		constantsAction = new InputAction(constants,Action.CONSTANT_ACTION_NAME);
		constantsAction.setTraceIndex(-1);    	
		// note: constantsAction not in trace 
		//       but used in black edges to let a param refer to a constant's action param 
    }
   

	public Trace() {
		traceIndex=-1;
		
		trace = new ArrayList<Action>();
		methodCountInTrace = new LinkedHashMap<String, Integer>();
	}

	/* does deepcopy of trace member, but looses greenEdges,methodCountInTrace
	 * members -> reset to new empty ones! 
	 */
	public Trace(Trace t) {
		this();
		for (Action action : t.trace) {
			if (action instanceof InputAction) {
				add(new InputAction((abslearning.trace.action.InputAction)action));  
			} else if (action instanceof OutputAction) {
				add(new OutputAction((abslearning.trace.action.OutputAction)action));   
			} else {
				throw new BugException(Messages.UNKNOWN_ACTION_TYPE);
			}
		}
	}
	
	/* copy constructor which either only does concrete or abstract copy 
	 *   
	 *   concretecopy == true => concrete copy done
	 *   concretecopy == false => abstract copy done
	 */
	public Trace(Trace t, boolean concretecopy) {
		this();
		if (concretecopy ) {
			for (Action action : t.trace) {
				if (action instanceof InputAction) {
					add(new InputAction(action.getConcreteParameters(),action.getMethodName()));   // add concrete copy of action
				} else if (action instanceof OutputAction) {
					add(new OutputAction(action.getConcreteParameters(),action.getMethodName()));   // add concrete copy of action
				} else {
					throw new BugException(Messages.UNKNOWN_ACTION_TYPE);
				}
			}
		} else {
			for (Action action : t.trace) {
				if (action instanceof InputAction) {
					add(new InputAction(action.getAbstractionString()));   // add abstract copy of action
				} else if (action instanceof OutputAction) {
					add(new OutputAction(action.getAbstractionString()));   // add concrete copy of action
				} else {
					throw new BugException(Messages.UNKNOWN_ACTION_TYPE);
				}
			}			
		}
	     
	}
	
	public Trace(Collection<Action> actions) {
		this();
		for(Action action : actions) {
			add(action);
		}
	}
	
	/**
	 *  getSubTrace - gets a deep copy of the part in the trace from and to (both incl.) the actions
	 *  			  with start and end trace indexes
	 *  
	 *                note: actions are each fully deep copied; meaning that 
	 *                      both concrete as abstract it is deep copied
	 * 
	 * gets all actions from the trace from start till (including) end
	 * @param start - the first action of trace that is copied  
	 * @param end - the last action of the trace that is copied
	 * @return a Trace from start to (including) end
	 */
	public Trace getSubTrace(int start, int end){
		assert(start<=this.size()) : ("The specified start is larger than the size of the trace!");
		assert(end<=this.size()) : ("The specified end is larger than the size of the trace!");
		
		logger.debug("Start: " + start);
		logger.debug("End: " + end);
		Trace result = new Trace();

		for (Action action : trace) {
			if(action.getTraceIndex()>=start && action.getTraceIndex()<=end){
				logger.debug("Action: " + action + " with trace index: " + action.getTraceIndex());
				if(action instanceof InputAction){
					InputAction oa = new InputAction(action); // full deep copy : both concrete and abstract
					result.add(oa);  
				} else if(action instanceof OutputAction){
					OutputAction oa = new OutputAction(action); // full deep copy : both concrete and abstract
					result.add(oa); 
				} else {
					throw new BugException(Messages.BUG);
				}
			}
		}

		return result;
	}
	
	public List<Parameter> getAllParameters() {
		List<Parameter> params = new ArrayList<Parameter>();
		for (Parameter param : this) {
			params.add(param);
		}
		return params;
	}
	
	
	/**
	 * Sets the actionTypeIndex and traceIndex for the new action.
	 * Adds the action to the trace.
	 * 
	 * @param action that is added to the trace
	 */
	public void add(Action action) { 
		Integer methodIndex;
	

	    traceIndex=traceIndex+1;
	    
		if (methodCountInTrace.containsKey(action.getMethodName())) {
			methodIndex = methodCountInTrace.get(action.getMethodName());
		} else {
			methodIndex = new Integer(0);
			methodCountInTrace.put(action.getMethodName(), methodIndex);
		}
		
		action.setMethodIndex(methodIndex);
		methodIndex = new Integer(methodIndex + 1);
		methodCountInTrace.put(action.getMethodName(), methodIndex);
		
		action.setTraceIndex(traceIndex);
		trace.add(action);
	}
	
	public void removeLastAction() {
		//decrement methodIndex in trace
		Action action = this.trace.get(traceIndex);
		int methodIndex = methodCountInTrace.get(action.getMethodName());
		methodCountInTrace.put(action.getMethodName(), methodIndex-1);

		// remove action from trace
		this.trace.remove(traceIndex);
		// decrement trace index
		traceIndex=traceIndex-1;
	}	
	
	 
	
    public void updateTraceIndexes(){
    	for(int i=0; i<this.size(); i++){
    		Action action = this.get(i);
    		action.setTraceIndex(i);
    	}
    }
	
	
	public Integer getMethodCountInTrace(String methodName){
		Integer methodCount;
		if (methodCountInTrace.containsKey(methodName)) {
			methodCount = methodCountInTrace.get(methodName);    
		} else {			
			// not yet set : initialize
			methodCount = new Integer(0);
		}	
		return methodCount;
	}
	
	
	public List<Parameter> getAllInputParameters() {
		List<Parameter> params = new ArrayList<Parameter>();
		for (Action action : trace) {
			if (action instanceof InputAction) {
				params.addAll(action.getParameters());
			}
		}
		return params;
	}
	
	
	public OutputAction getLastOutputAction() {
		int lastActionIndex;
		if(this.size() > 1) {
			lastActionIndex = this.size() - 1;
			if(trace.get(lastActionIndex) instanceof InputAction) {
				lastActionIndex --;
			}
			return (OutputAction) this.get(lastActionIndex);
		} else { 
			return null;
		}
	}
	

	public int size() {
		return trace.size();
	}

	
	public Action get(int index) {
		Action a = trace.size() > index ? trace.get(index) : null;
		return a;
	}
	
	public Set<Integer> getAllFreshOutputValues() {
		Set<Integer> freshOutputValues = Determinizer.getCanonizedValueSet(trace);
		return freshOutputValues;
	}

	/**
	 * Returns all input actions of the entire trace
	 * 
	 * @return all input actions in the trace
	 */
	public List<InputAction> getInputActions() {
		List<InputAction> inputActions = new ArrayList<InputAction>();
		for (Action action : trace) {
			if (action instanceof InputAction) {
				inputActions.add((InputAction) action);
			}
		}
		return inputActions;
	}
	
	// get inputs of concrete copy of trace
	public List<InputAction> getConcreteInputActions() {
		
		Trace concreteTrace=new Trace(this, true); 
		return concreteTrace.getInputActions();
	}

	/**
	 * Returns all output actions of the entire trace
	 
	 * @return all output actions in the trace
	 */
	public List<OutputAction> getOutputActions() {
		List<OutputAction> outputActions = new ArrayList<OutputAction>();
		for (Action action : trace) {
			if (action instanceof OutputAction) {
				outputActions.add((OutputAction) action);
			}
		}
		return outputActions;
	}
	

	public boolean existsAction(String methodName) {
		for (Action action : trace) {
			if (action.getMethodName().equals(methodName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns the output values of all output actions added to the trace thus far.
	 * It assumes all values are defined up to this point.
	 * 
	 * @return all output values in the trace
	 */
	public List<Integer> getAllOutputIntegerValues() {
		List<OutputAction> outputs = getOutputActions();
		List<Action> actions = new ArrayList<Action>(outputs);
		List<Integer> outputValues = getAllIntegerValuesFromActions(actions, true);
		return outputValues;
	}
	
	
	
	/**
	 * Returns the input values of all input actions added to the trace thus far.
	 * It assumes all values are defined up to this point.
	 * 
	 * @return all input values in the trace
	 */
	public List<Integer> getAllInputIntegerValues() {
		List<InputAction> inputs = getInputActions();
		List<Action> actions = new ArrayList<Action>(inputs);
		List<Integer> inputValues = getAllIntegerValuesFromActions(actions, true);
		return inputValues;
	}
	
	public List<Integer> getAllIntegerValues() {
		List<Integer> inputOutputValues = getAllIntegerValuesFromActions(this.trace, true);
		return inputOutputValues;
	}
	
	public List<Integer> getAllDefinedIntegerValues() {
		return getAllIntegerValuesFromActions(trace, false);
	}
	
	public List<Integer> getAllDefinedInputIntegerValues() {
		return getAllIntegerValuesFromActions(new ArrayList<Action>(this.getInputActions()), false);
	}
	
	/*
	 * fetches all integers from a list of actions
	 */
	private List<Integer> getAllIntegerValuesFromActions(List<Action> actions, boolean assumeDefined) {
		List<Integer> result = new ArrayList<Integer>();
		for (Action action : actions) {
			if(assumeDefined)
				result.addAll(action.getConcreteParameters());
			else 
				result.addAll(action.getConcreteParametersSoFarDefined());
		}
		return result;
	}


	/*
	 *  get input actions from the beginning to and including the traceIndex 
	*/
	private List<InputAction> getInputActions(int traceIndex) {
		List<InputAction> result = new ArrayList<InputAction>();

		for (int i=0; i<trace.size();i++) {
			Action action = trace.get(i);
			if (action instanceof InputAction && i<=traceIndex) {
				result.add((InputAction) action);
			}
		}
		return result;
	}

	
	public Word getAbstractInputWord() {
		List<InputAction> inputs = getInputActions();
		Word result = new WordImpl();

		for (InputAction i : inputs) {
			String abstractionString= i.getAbstractionString();
			if (abstractionString==null) {
				throw new BugException("getting an abstract word if abstraction not set!");
			}
			result.addSymbol(new SymbolImpl(abstractionString));
		}
		return result;
	}
	
	/**
	 * gets all abstract inputs from the trace from the beginning of the trace till and including the specified traceIndex
	 * @param traceIndex specifies the last abstract symbol in the abstract input word
	 * @return an abstract input word from the beginning of the trace till the specified traceIndex
	 */
	public Word getAbstractInputWord(int traceIndex) {
		assert(traceIndex<=this.size()) : ("The specified traceIndex is larger than the size of the trace!");
		
		List<InputAction> inputs = getInputActions(traceIndex);
		Word result = new WordImpl();

		for (InputAction i : inputs) {
			result.addSymbol(new SymbolImpl(i.toStringAbstract()));
//			result.addSymbol(new SymbolImpl(i.getAbstractionString()));
		}
		return result;
	}


	public Word getAbstractOutputWord() {
		List<OutputAction> outputs = getOutputActions();
		Word result = new WordImpl();

		for (OutputAction o : outputs) {
			result.addSymbol(new SymbolImpl(o.toStringAbstract()));
//			result.addSymbol(new SymbolImpl(o.getAbstractionString()));

		}
		return result;
	}
	
	public List<String> getConcreteInputWord() {
		List<InputAction> inputs = getInputActions();
		List<String> result = new ArrayList<String>();

		for (InputAction i : inputs) {
			result.add(i.toStringConcrete());
		}
		return result;
	}
	
	public List<String> getConcreteOutputWord() {
		List<OutputAction> outputs = getOutputActions();
		List<String> result = new ArrayList<String>();

		for (OutputAction o : outputs) {
			result.add(o.toStringConcrete());
		}

		return result;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < trace.size(); i++) {
			result.append(trace.get(i).toString());
			if (i != trace.size() - 1) {
				if ( (i & 1) == 0 ) {  // check whether last bit is 0  
					// i even...  ( input added )
					result.append(" / ");  // separate input and output 
				} else { 
					//odd...  ( output added )
					result.append(" -> "); //  separate transitions
				}
			}
		}
		return result.toString();
	}
	
	public String toStringWithStatevars() {
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < trace.size(); i++) {
			result.append("\n" + trace.get(i).toString());
		//	if (i != trace.size() - 1) {
				if ( (i & 1) == 0 ) {  // check whether last bit is 0  
					// i even... ( input added )
					result.append(" / ");  // separate input and output 
				} else { 
					//odd... ( output added )
					result.append("\n"); // separate transitions
					result.append("   memV: " + getMemvAfterTransition(i/2)); 
					result.append("   stateVarUpdates: " + getStatevarUpdates(i/2)); 
					result.append("   stateVars: " +  getStateVarsAfterTransition(i/2)); 
					//result.append("\n"); // separate transitions
				}
		//	}
		}
		result.append("\n"); 
		return result.toString();
	}
    
	public String toStringAbstract() {
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < trace.size(); i++) {
			result.append(trace.get(i).getAbstractionString());
			if (i != trace.size() - 1) {
				if ( (i & 1) == 0 ) {  // check whether last bit is 0  
					// i even... 
					result.append(" / ");  // separate input and output 
				} else { 
					//odd... 
					result.append(" -> "); // separate output and next input
				}
			}
		}
		return result.toString();
	}	
	
	public String toStringConcrete() {
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < trace.size(); i++) {
			result.append(trace.get(i).toStringConcrete());
			if (i != trace.size() - 1) {
				if ( (i & 1) == 0 ) {  // check whether last bit is 0  
					// i even... 
					result.append(" / ");  // separate input and output 
				} else { 
					//odd... 
					result.append(" -> "); // separate output and next input
				}
			}
		}
		return result.toString();
	}		
	
	public  String logString() {
		//String phase, String type, String id, Word query;
		Word query=this.getAbstractInputWord();
		StringWriter stream = new StringWriter();
		//stream.write("Phase          :" + phase);
		//stream.write("\nType           :" + type);
		//stream.write("\nId             :" + id);
		stream.write("Size           :" + query.size());
		stream.write("\nAbstract input :" + query.toString() );  
		stream.write("\nAbstract output:" + this.getAbstractOutputWord().toString()); 
		stream.write("\nConcrete input :" + this.getConcreteInputWord().toString());	
		stream.write("\nConcrete output:" + this.getConcreteOutputWord().toString() + "\n"); 
		return  stream.toString();
	}
	
	

	
//---------------------------------------------------------------------------------------------------
//  equals and hashcode
//---------------------------------------------------------------------------------------------------
		/*	
	@Override
	public int hashCode() {
		 throw new NotImplementedException();
	}	
	
	@Override
	public boolean equals(Object obj) {
		throw new NotImplementedException();
	}	
	*/
	
	
	public boolean equalsConcreteOutputs(Trace otherTrace) {
		return equalsConcrete(otherTrace, a -> a instanceof OutputAction);
	}	
	
	public boolean equalsConcrete(Trace otherTrace)  {
		return equalsConcrete(otherTrace, a -> true);
	}
	
	private boolean equalsConcrete(Trace otherTrace, Predicate<Action> predicate) {
		if (this == otherTrace)	return true;
		if (otherTrace == null)	return false;
		
		if (this.size() != otherTrace.size()) return false;
		
		for ( int traceIndex=0; traceIndex < trace.size(); traceIndex++ ) {
			if (predicate.test(this.get(traceIndex)) && ! this.get(traceIndex).concreteEquals(otherTrace.get(traceIndex)) ) 
				return false;
		}
		return true;
	}
	
	
	static public boolean equalsConcreteInputs(List<InputAction> inputs1, List<InputAction> inputs2) {
		if (inputs1 == inputs2)	return true;
		
		if (inputs1 == null)	return false;
		if (inputs2 == null)	return false;
		
		int size=inputs1.size();
		if (size != inputs2.size()) return false;
		
		for ( int i=0; i < size; i++ ) {
			if (! inputs1.get(i).concreteEquals(inputs2.get(i)) ) return false;
		}
		return true;
	}		

	public boolean equalsAbstractInputs(Trace otherTrace) {
		if (this == otherTrace)	return true;
		if (otherTrace == null)	return false;
		
		if (this.size() != otherTrace.size()) return false;
		for ( int traceIndex=0; traceIndex < trace.size(); traceIndex=traceIndex+2 ) {
			if (! this.get(traceIndex).abstractEquals(otherTrace.get(traceIndex)) ) return false;
		}		
		return true;
	}	

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trace == null) ? 0 : trace.hashCode());
		return result;
	}

//---------------------------------------------------------------------------------------------------
//  calculate statevars using update specification from their old values 
//---------------------------------------------------------------------------------------------------

	/* 
	 * calculates  statevarsAfterTransition
	 * 
	 */
	static public  SortedMap<Integer, Integer> getUpdatedStateVars(SortedMap<Integer, Integer> statevarsBeforeTransition,LinkedHashMap<Integer, Integer> statevarUpdates) {
		
		
		// copy statevarsBeforeTransition in new statevarsAfterTransition
		SortedMap<Integer, Integer> statevarsAfterTransition= new TreeMap<Integer,Integer>(statevarsBeforeTransition);
				

		// first do all unsets and then then the updates 
		//
		// first do all unsets  
		for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
			Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
			if ( stateVarUpdate == null ) {
				// update statevars
				statevarsAfterTransition.remove(stateVarIndex);
			}
		}
		// then do updates
		for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
			Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
			
			if ( stateVarUpdate != null ) {
				// update statevars
				statevarsAfterTransition.put(stateVarIndex, stateVarUpdate);
			}	
		}	
		return statevarsAfterTransition;
	}	
  
	
	public void applyValueMapping(Map<Integer,Integer> oldToNewValueMapping) {
		for (int i=0; i<trace.size();i++) {
			Action action = trace.get(i);
			List<Integer> concreteParamValues = action.getConcreteParameters();
			List<Integer> newValues = new ArrayList<Integer>();
			for (Integer value : concreteParamValues) {
				if(oldToNewValueMapping.containsKey(value)) {
					newValues.add(oldToNewValueMapping.get(value));
				} else {
					newValues.add(value);
				}
			}
			action.setConcreteValues(newValues);
		}
	}
	
//---------------------------------------------------------------------------------------------------			
//  access to input/output using transitionIndex
//---------------------------------------------------------------------------------------------------			

		
		
		public OutputAction getOutputAction(int transitionIndex) {
			int outputIndex =  transitionIndex*2+1;
			return (OutputAction) this.get(outputIndex);
		}
		
		public InputAction getInputAction(int transitionIndex) {
			int inputIndex =  transitionIndex*2;
			return (InputAction) this.get(inputIndex);
		}

//---------------------------------------------------------------------------------------------------			
//  memv after transition
//---------------------------------------------------------------------------------------------------			
			
		
	    // memV  after transition
		public List <LinkedHashSet<Integer>> transition2memVafterTransition = new ArrayList<LinkedHashSet<Integer>>();
		
		public LinkedHashSet<Integer> getMemvAfterTransition(int transitionIndex) {
			// after transition -1 we are at begin of Trace where no memorable values can be yet set 
			if ( transitionIndex == -1 ) return new LinkedHashSet<Integer>();
			
			// if no index in list for transitionIndex then it is undefined => null
			if (transitionIndex  >= transition2memVafterTransition.size() ) {
				return null;
			}
			return transition2memVafterTransition.get(transitionIndex);
		}
		
		public void addMemV(LinkedHashSet<Integer> memV) {
			 transition2memVafterTransition.add(memV);
		}
		

		
		
//---------------------------------------------------------------------------------------------------			
//        statevar updates in transition  
//---------------------------------------------------------------------------------------------------			
				
			
			
			
			// stateUpdates during transition, the updates have an order determined by lookahead
			// note: when in same state order of updates found by lookahead is the same
			public List <LinkedHashMap<Integer,Integer>> transition2statevarUpdates = new ArrayList<LinkedHashMap<Integer,Integer>>();

			
			public LinkedHashMap<Integer, Integer> getStatevarUpdates(int transitionIndex) {
                // 0 is first transition 
				if ( transitionIndex == -1 ) return new LinkedHashMap<Integer,Integer>();

				// if no index in list for transitionIndex then it is undefined => null
				if (transitionIndex  >= transition2statevarUpdates.size() ) {
					return null;
				}
				return transition2statevarUpdates.get(transitionIndex);
			}
			
			public void addStatevarUpdates(LinkedHashMap<Integer, Integer> statevarUpdates) {
				this.transition2statevarUpdates.add(statevarUpdates);				
			}
			
		
//---------------------------------------------------------------------------------------------------			
//      statevars  storage
//---------------------------------------------------------------------------------------------------			

			

			
			// Extra redundant storage, but convenient:
			//   * easier to print trace with statevars after each transition
			//   * easier to detect if after some transition a value is still stored 
			// Note: after each query a Trace is 
			//       thrown away anyway, so not really effecting performance
			List<SortedMap<Integer,Integer>> transition2statevarsAfterTransition = new ArrayList<SortedMap<Integer,Integer>>();
		
			public void updateStatevars(SortedMap<Integer, Integer> updatedStatevars) {
				//this.statevarsBeforeTransition = this.statevarsAfterTransition;
				//this.statevarsAfterTransition = updatedStatevars;
				
				// Extra: store statevars after each transition
				transition2statevarsAfterTransition.add(new TreeMap<Integer,Integer>(updatedStatevars));				
			}			
			
			public SortedMap<Integer, Integer> getStateVarsAfterTransition(int transitionIndex) {
				// before transition 0 we are at begin of Trace where no statevars can be yet set 
				if ( transitionIndex < 0 ) return new TreeMap<Integer,Integer>();
				
				// if no index in list for transitionIndex then it is undefined => null
				if (transitionIndex  >= transition2statevarsAfterTransition.size() ) {
					return null;
				}				
				return transition2statevarsAfterTransition.get(transitionIndex);
			}
			
			public SortedMap<Integer,Integer> getStateVarsBeforeTransition(int transitionIndex) {
				SortedMap<Integer, Integer> stateVarsBeforeTransition;
				if (transitionIndex == 0) {
					stateVarsBeforeTransition = new TreeMap<Integer, Integer>(); 
				} else {
					stateVarsBeforeTransition = this.getStateVarsAfterTransition(transitionIndex - 1);
				}
				return stateVarsBeforeTransition;
			}
			
//---------------------------------------------------------------------------------------------------			
//	      statevars before and after current transition
//---------------------------------------------------------------------------------------------------			


			// statevars before/after transition
		//	public SortedMap<Integer,Integer> statevarsBeforeTransition = new TreeMap<Integer,Integer>();			
		//	public SortedMap<Integer,Integer> statevarsAfterTransition = new TreeMap<Integer,Integer>();		
			public SortedMap<Integer, Integer> getStatevarsAfterTransition() {
				//return statevarsAfterTransition;
				int transindex=getTransitionIndex();				
				return getStateVarsAfterTransition(transindex);
				//return transition2statevarsAfterTransition.get(transindex);
			}

			public SortedMap<Integer, Integer> getStatevarsBeforeTransition() {
				int transindex=getTransitionIndex()-1;
				
				/*
				if ( transindex <= 0 ) {
					return new TreeMap<Integer,Integer>();
				} else {
				   return transition2statevarsAfterTransition.get(transindex);				
				}*/
				
				return getStateVarsAfterTransition(transindex);
				   //return statevarsBeforeTransition;
			}			
			
//---------------------------------------------------------------------------------------------------
//  value -> statevar ( or statevar which last used to store that value) (but not anymore)	
//---------------------------------------------------------------------------------------------------
				
				/* get from value a statevar assigned to that value
				* if not returns null
				*/
				private HashMap <Integer,Integer> value2stateVarIndex = new HashMap <Integer,Integer> ();
					
//				private HashMap <Integer,Integer> value2lastStateVarIndex = new HashMap <Integer,Integer> ();
				
				/**
				 * Gets the  state variable that contains the specified value
				 * @param   value 
				 * @return  the state variable containing that value
				 * 
				 * used by StateVarMapper to find out which stateVariable
				 * must be unset if a memorable value is not memorable anymore
				 */
				public Integer getStateVariableContainingValue(Integer stateVarValue){
					return value2stateVarIndex.get(stateVarValue);
				}    
				
				/*
				 *  update mapping  value to stateVarIndex
				 *  Handy to quickly find in fresh query from which stateVariable the input value came.
				 *  note: in a not-fresh query the input value could by accident choosen the same as a statevar,
				 *        but the input value had not significant cause for the taken path  
				 */
				public void updateValue2stateVarIndex(Integer stateVarValue,Integer stateVarIndex) {
					if ( stateVarIndex ==null ) {
				        value2stateVarIndex.remove(stateVarValue);
					} else {
						value2stateVarIndex.put(stateVarValue,stateVarIndex);
					}
			    }		
				
//---------------------------------------------------------------------------------------------------
//   source param	for  AbstractionVariable 
//  => only used for Mapper.addGreenEdgeOnParameterOfInput(InputAction, int, Trace)				
//---------------------------------------------------------------------------------------------------
					
			    /*
			     *  get source param from statevar got its last value
			     */
				private HashMap <Integer,Parameter> stateVarIndex2lastAssignedParam = new HashMap <Integer,Parameter> ();
				

				
				/**
				 * Updates the state variable  when  a new value in statevar is set 
				 * (a new value occurs in memV or a value in memV has been removed)
				 * Approach where we only have one state variable per parameter
				 * @param stateVarIndex The index of the state variable
				 * @param parameter The parameter that contains the value of the state variable
				*/ 
				private void updateStateVariableParamSource(Integer stateVarIndex, Parameter parameter){
					stateVarIndex2lastAssignedParam.put(stateVarIndex, parameter);
				}

				private HashMap<Integer,Parameter> getStateVarIndex2lastAssignedParam(){
					return new  HashMap<Integer,Parameter>( stateVarIndex2lastAssignedParam);
				}
				
				/**
				 * Gets for a specified abstraction the input param which for current trace would be its source param
				 * 
				 * => only used for Mapper.addGreenEdgeOnParameterOfInput(InputAction, int, Trace)
				 */
				public Parameter getCurrentSourceParamForAbstractionVariable(InputAction input, AbstractionVariable abstractionVariable){
					
					if(abstractionVariable instanceof AbstractionVariable){  // abstraction only defined for stateVar method
						char abstractionVariableType = abstractionVariable.getVariableType();
						int abstractionVariableIndex = abstractionVariable.getVariableIndex();
						if(abstractionVariableType=='c'){
							Parameter p = constantsAction.getParam(abstractionVariableIndex);
							return p;
						}
						else if(abstractionVariableType=='p'){
							return input.getParam(abstractionVariableIndex);
						}
						else if(abstractionVariableType=='x'){
							logger.fatal("x"+abstractionVariableIndex);
							logger.fatal("stateVarIndex2lastAssignedParam "+stateVarIndex2lastAssignedParam);
							
							Parameter param=stateVarIndex2lastAssignedParam.get(abstractionVariableIndex);
							if ( param == null ) throw new BugException( " cannot find source param of statevar: x" + abstractionVariableIndex);
                            
							logger.fatal("param"+param.toStringComplete());
							return param; 					
						}	
						else {
							throw new BugException(Messages.APISTATEVAR_NOT_C_P_OR_X);
						}
					}
					else {
						throw new BugException(Messages.API_SHOULD_BE_STATEVAR);
					}

				}	
				
				
				//IPut_-1_-1(0,1) / x0=p1_x1=p0_OOK() -> IPut_-1_-1(2,3) / x1=p1_x2=x1_x3=p0_OOK() -> IGet_1(0)
				public void updateValue2stateVarIndex(SortedMap<Integer, Integer> statevarsBeforeTransition,LinkedHashMap<Integer, Integer> statevarUpdates) {
					
					// first do all unsets and then then the updates
					//   to make sure that if a value get assigned to different statevar
					//   we do not by accident remove it from value2statevarIndex		
					//
					// first do all unsets  
					for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
						Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
						if ( stateVarUpdate == null ) {
							// update in trace  value2statevarIndex mapping
							// needed 
							//   in 'updateStateVars' to find out which stateVariable
							//   must be unset if a memorable value is not memorable anymore
							Integer stateVarValue=statevarsBeforeTransition.get(stateVarIndex);
							this.updateValue2stateVarIndex(stateVarValue,null);
						}
					}
					// then do updates
					for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
						Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
						
						if ( stateVarUpdate != null ) {
							// update in trace  value2statevarIndex mapping
							// needed 
							//   in 'updateStateVars' to find out which stateVariable
							//   must be unset if a memorable value is not memorable anymore
							this.updateValue2stateVarIndex(stateVarUpdate,stateVarIndex);			
						}	
					}	
				}

				public void updateStateVariableParamSource(SortedMap<Integer, Integer> statevarsBeforeTransition,LinkedHashMap<Integer, Integer> statevarUpdates) {
					
					// get deep copy of Statevar2lastAssignedParam
					HashMap <Integer,Parameter> oldStatevar2lastAssignedParam= this.getStateVarIndex2lastAssignedParam();	
					
					// first do all unsets and the updates to
					// make sure that if a value get assigned to different statevar
					// we do not by accident remove it from value2sv
					//
					// first do all unsets  
					for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
						Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
						if ( stateVarUpdate == null ) {
							// update in trace the param source for statevar 
							this.updateStateVariableParamSource(stateVarIndex, null);
						}
					}
					// then do updates
					for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
						Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
						
						if ( stateVarUpdate != null ) {
							// update in trace the param source for statevar 
							if (! statevarsBeforeTransition.containsValue(stateVarUpdate)) {

								Parameter sourceParam = getSourceParamForUpdateFromTrace(stateVarUpdate);
								if ( sourceParam != null ) {
									this.updateStateVariableParamSource(stateVarIndex, sourceParam);
								} else {
									logger.fatal(this.trace);
									logger.fatal("update stateVars " + statevarsBeforeTransition + " with "+ statevarUpdates);
									throw new BugException("Unknown source param for value used in statevar update " + stateVarUpdate);
								}
							} else {
								// SOURCE: param of earlier input
								// update value in statevars in previous state before transition 
								// get source param from  statevar2sourceparam index in previous state
								boolean found=false;
								for ( Integer statevarIndex: statevarsBeforeTransition.keySet() ) {
									if ( statevarsBeforeTransition.get(statevarIndex).equals(stateVarUpdate) ) {
										Parameter sourceParam=oldStatevar2lastAssignedParam.get(statevarIndex);
										this.updateStateVariableParamSource(stateVarIndex, sourceParam);
										found=true;
										break;
									}
								}
								if (!found) throw new BugException("Cannot find source param");
							}
						}	
					}	
				}
				
				/**
				 * Retrieves the source parameter which lead to the given update or null if it cannot find it. 
				 * It looks for it in the current input action and in the current output action.
				 * 
				 * @return the Parameter object or null if the source isn't found
				 */
				private Parameter getSourceParamForUpdateFromTrace(Integer stateVarUpdate) {
					int transitionIndex=this.getTransitionIndex();
					InputAction inputAction=this.getInputAction(transitionIndex);
					OutputAction outputAction=this.getLastOutputAction();
					List<Integer> inputValues=inputAction.getConcreteParameters();
					Parameter sourceParam = null;
					int sourceParamIndex=inputValues.indexOf(stateVarUpdate);
					if(sourceParamIndex != -1) {
						sourceParam = inputAction.getParam(sourceParamIndex);
					} else {
						if( outputAction != null) {
							List<Integer> outputValues = outputAction.getConcreteParameters();
							sourceParamIndex=outputValues.indexOf(stateVarUpdate);
							if(sourceParamIndex != -1) {
								sourceParam = outputAction.getParam(sourceParamIndex);
							} 		
						}
					}
					return sourceParam;
				}
				

				// Iterators over trace
				/**
				 * Iterates over the transitions starting from (incl.) {@code fromTransition}.
				 */
				public Iterable<Transition> eachTransition(final int fromTransition) {
					final Trace getThis = this;
					return new Iterable<Transition>() {
						public Iterator<Transition> iterator() {
							return new TraceTransitionIterator(getThis, fromTransition);
						}
					};
				}
				
				public Iterable<Transition> eachTransition() {
					return eachTransition(0);
				}

				@Override
				public Iterator<Parameter> iterator() {
					return new TraceParamIterator(this);
				}
				
				public Iterator<Parameter> eachParameter(Parameter param) {
				    return new TraceParamIterator(this, param);
				}

				public Parameter getFirstRelatedParameter(Integer toValue) {
					Parameter paramSource = null;
					for (Parameter param : this) {
						if (param.getIntegerValue() != null && Relation.areRelated(param.getIntegerValue(), toValue))  {
							paramSource = param;
							break;
						}
					}
					return paramSource;
				}
				
				/**
				 *  a (very) inefficient way of fetching the source param for each value
				 */
				public Parameter getSourceParameter(Integer value) {
				    Parameter sourceParam = null;
    				for (Parameter testParam = this.getFirstRelatedParameter(value); testParam != sourceParam; 
                            testParam = this.getFirstRelatedParameter(value)) {
                        sourceParam = testParam;
                        value = sourceParam.getIntegerValue();
                    }
    				return sourceParam;
				}
				
				public Parameter getFirstParameterWithValue(Integer value) {
					Parameter paramWithValue = null;
					for (Parameter param : this) {
						if (param.getIntegerValue() != null && param.getIntegerValue() == value)  {
							paramWithValue = param;
							break;
						}
					}
					return paramWithValue;
				}
}
