package abslearning.trace;

import java.util.HashMap;

public class TraceState {

	HashMap<String,Integer> stateVar2Value= new HashMap<String,Integer>();
	
	public int  getStateVarValue(String stateVar) {	
		return stateVar2Value.get(stateVar).intValue();		
	}
	
	public void  setStateVarValue(String stateVar, int value) {	
		 stateVar2Value.put(stateVar, new Integer(value)  );	
	}	
	
}
