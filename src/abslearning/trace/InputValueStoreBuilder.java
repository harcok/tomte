package abslearning.trace;

import java.util.List;

import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.stores.FreshInputValueStore;

public class InputValueStoreBuilder implements CanonicalValueStoreProvider {
	private List<Integer> excludedValues;

	public InputValueStoreBuilder(Trace trace) {
		this.excludedValues = trace.getAllInputIntegerValues();
	}

	@Override
	public CanonicalValueStore newValueStore() {
		return new FreshInputValueStore(this.excludedValues);
	}

}
