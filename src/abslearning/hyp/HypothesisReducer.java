package abslearning.hyp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import abslearning.trace.action.InputAction;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import util.Tuple2;
import util.learnlib.AutomatonUtil;

public class HypothesisReducer {
	public static Logger logger = Logger.getLogger(HypothesisReducer.class);
	
	public static Automaton reduceHypothesisToEliminateNonDeterminism(Automaton abstractHyp) {
		// remove unnecessary transitions
		Set<String> removeStateInputPairs=getRemovableTransitionsForIrrelevantStatevars(abstractHyp);			
		logger.debug("removeStateInputPairs: " + removeStateInputPairs);			
		Automaton abstractReducedHyp = AutomatonUtil.getCopyWithRestrictedAlphabet(abstractHyp, abstractHyp.getAlphabet(),removeStateInputPairs);
		return abstractReducedHyp;
	}
	
	private static Set<String> getRemovableTransitionsForIrrelevantStatevars(
			Automaton aut) {
		HashSet<String> removeStateInputPairs = new HashSet<String>();
		List<State> states = AutomatonUtil.getStatesInBFSOrder(aut);
		for (State s : states) {
			if (s == null)
				continue;

			logger.debug("s.getId(): " + s.getId());

			// get collections of set of abstract inputs with same methodname
			// begin/end abstract output
			// Collection<List<Symbol>> collectionOfListOfAbstractInputs =
			// getCollectionOfListOfAbstractInputsWithSameNextStateAndOutput(s);

			Map<String, List<List<Symbol>>> method2ListsOfInputsWithSameEffectiveOutput = getCollectionOfListOfAbstractInputsWithSameNextStateAndOutput(s);

			logger.debug("method2ListsOfInputsWithSameEffectiveOutput: "
					+ method2ListsOfInputsWithSameEffectiveOutput);
			for (String method : method2ListsOfInputsWithSameEffectiveOutput
					.keySet()) {
				logger.debug("method:" + method);

				// list of params to remove for method
				List<List<Integer>> listOfPossibleRemovableParams = new ArrayList<List<Integer>>();

				// per method keep for each paramIndex a set of abstractValues
				// it is dependent on
				Map<Integer, Set<Integer>> paramIndex2dependentAbstractVals = new HashMap<Integer, Set<Integer>>();

				// get for each method multiple sets of abstract inputs where
				// each set contains inputs from transitions
				// from current state to the same destination state with same
				// abstract output.
				List<List<Symbol>> listOfListOfAbstractInputs = method2ListsOfInputsWithSameEffectiveOutput
						.get(method);

				logger.debug("listOfListOfAbstractInputs:"
						+ listOfListOfAbstractInputs);

				// make map to find symbol belonging to params
				HashMap<ArrayList<Integer>, Symbol> params2inputSymbol = new HashMap<ArrayList<Integer>, Symbol>();

				// search for each set of abstract inputs which of the inputs
				// can be removed
				for (List<Symbol> abstractInputs : listOfListOfAbstractInputs) {
					// abstractInputs: set of abstract inputs with same
					// methodname begin/end abstract output
					logger.debug("abstractInputs: " + abstractInputs);

					List<List<Integer>> listOfParams = new ArrayList<List<Integer>>();
					for (Symbol input : abstractInputs) {
						InputAction ia = new InputAction(input.toString());
						List<Integer> params = ia.getAbstractParameters();
						params2inputSymbol.put((ArrayList<Integer>) params,
								input);
						listOfParams.add(params);
					}

					// get list of Params
					// List<List<Integer>> listOfParams=new
					// ArrayList<List<Integer>>(params2inputSymbol.keySet());

					logger.debug("listOfParams: " + listOfParams);
					Tuple2<List<List<Integer>>, List<List<Integer>>> tupleOfRemovableAndNotRemovable = findListOfParamsToRemove(listOfParams);
					List<List<Integer>> removableParams = tupleOfRemovableAndNotRemovable.tuple0;
					List<List<Integer>> notRemovableParams = tupleOfRemovableAndNotRemovable.tuple1;

					logger.debug("removableParams: " + removableParams);
					logger.debug("notRemovableParams: " + notRemovableParams);
					listOfPossibleRemovableParams.addAll(removableParams);

					updateDependentAbstraction(
							paramIndex2dependentAbstractVals,
							notRemovableParams);

					/*
					 * for ( List<Integer> params : findListOfParamsToRemove (
					 * listOfParams )) { removeStateInputPairs.add(
					 * s.getId()+"_"+params2inputSymbol.get(params).toString());
					 * }
					 */
					// continue next set of abstract inputs for this specific
					// method
				}

				logger.debug("paramIndex2dependentAbstractVals: "
						+ paramIndex2dependentAbstractVals);
				logger.debug("listOfPossibleRemovableParams: "
						+ listOfPossibleRemovableParams);
				List<List<Integer>> listOfRemovableParams = removeFalseRemovableInputs(
						paramIndex2dependentAbstractVals,
						listOfPossibleRemovableParams);
				;
				logger.debug("listOfRemovableParams: " + listOfRemovableParams);

				for (List<Integer> params : listOfRemovableParams) {
					removeStateInputPairs.add(s.getId() + "_"
							+ params2inputSymbol.get(params).toString());
				}

				// continue next method
			}
			// continue next state
		}
		return removeStateInputPairs;
	}
	

	private static Tuple2<List<List<Integer>>, List<List<Integer>>> findListOfParamsToRemove(
			List<List<Integer>> listOfParams) {

		// get sorted List of params
		Collections.sort(listOfParams, new Comparator<List<Integer>>() {
			public int compare(List<Integer> p1, List<Integer> p2) {
				for (int i = 0; i < p1.size(); i++) {
					if (p1.get(i) < p2.get(i)) {
						return -1;
					} else if (p1.get(i) > p2.get(i)) {
						return 1;
					}
				}
				return 0;
			}
		});

		// find removable List of params
		logger.debug("search listOfParams: " + listOfParams);
		List<List<Integer>> matchedListOfParams = new ArrayList<List<Integer>>();
		int numParamsInput = listOfParams.get(0).size();
		for (int paramIndex = numParamsInput - 1; paramIndex > -1; paramIndex--) {
			logger.debug("search paramIndex: " + paramIndex);
			for (int i = 0; i < listOfParams.size(); i++) {
				List<Integer> params = listOfParams.get(i);

				if (params.get(paramIndex).intValue() != -1)
					continue;

				// search for matching next items in sorted listOfParams
				// note: each match is removed from listOfParams
				matchedListOfParams
						.addAll(searchForMatchingParamsExceptForNthParam(
								paramIndex, i, listOfParams));

			}
		}
		return new Tuple2<List<List<Integer>>, List<List<Integer>>>(
				matchedListOfParams, listOfParams);

	}
	
	
	private static List<List<Integer>> searchForMatchingParamsExceptForNthParam(
			int paramIndex, int i, List<List<Integer>> listOfParams) {

		// search for matching next items in sorted listOfParams
		List<Integer> params = listOfParams.get(i);
		logger.debug("try to find matches for : " + params);
		List<List<Integer>> matches = new ArrayList<List<Integer>>();
		for (int nextParamsIndex = i + 1; nextParamsIndex < listOfParams.size(); nextParamsIndex++) {

			List<Integer> nextParams = listOfParams.get(nextParamsIndex);
			if (equalExceptN(paramIndex, params, nextParams)) {
				logger.debug("rm : " + nextParams);
				matches.add(nextParams);
				listOfParams.remove(nextParamsIndex); // when searching same
														// list for other
														// paramIndex then this
														// already removed
				nextParamsIndex = nextParamsIndex - 1; // continue with same
														// index, because next
														// item is shifted
														// inplace of the
														// removed one!
				logger.debug("listOfParams: " + listOfParams);

			} else {
				logger.debug("keep : " + nextParams);
				// list of params is sorted from left to right
				// thus we can skip searching further for an equal if value at
				// paramIndex-1
				// is already different => because higher digits will only
				// increase further in sorted list
				if (paramIndex != 0
						&& params.get(paramIndex - 1).intValue() != nextParams
								.get(paramIndex - 1).intValue()) {
					logger.debug("continue with finding matches for next params set");
					break; // search for equal params
				}
			}
		}
		return matches;
	}
	
	private static boolean equalExceptN(int paramIndex, List<Integer> params,
			List<Integer> nextParams) {

		for (int i = 0; i < params.size(); i++) {
			if (i == paramIndex)
				continue;
			if (!params.get(i).equals(nextParams.get(i)))
				return false;
		}
		return true;
	}
	
	// ----------------------------------------------------------------------------

		/*
		 * getCollectionOfListOfAbstractInputsWithSameNextStateAndOutput - finds
		 * transitions which are mergable
		 * 
		 * return: set of abstraction
		 */
	private static Map<String, List<List<Symbol>>> getCollectionOfListOfAbstractInputsWithSameNextStateAndOutput(
				State s) {
			// find sets of transitions with same inputMethod,begin,end and
			// outputSymbol
			// thus collect inputs with same effective transition:
			// => map: method_endstate_output -> list of symbol inputs
			HashMap<Tuple2<String, String>, List<Symbol>> methodWithEffectiveOutput2inputs = new HashMap<Tuple2<String, String>, List<Symbol>>();
			for (Symbol input : s.getInputSymbols()) {
				InputAction ia = new InputAction(input.toString());
				// String methodName =
				// util.JavaUtil.getPrefixFromString(input.toString(),"_");
				String methodName = ia.getMethodName();
				if (ia.getAbstractParameters().size() == 0)
					continue;

				// String result = methodName +"_"+
				// s.getTransitionState(input).getId()+"_"+s.getTransitionOutput(input);
				String effectiveOutput = s.getTransitionState(input).getId() + "_"
						+ s.getTransitionOutput(input);

				Tuple2<String, String> method_effectiveOutput = new Tuple2<String, String>(
						methodName, effectiveOutput);

				List<Symbol> inputs;
				if (methodWithEffectiveOutput2inputs
						.containsKey(method_effectiveOutput)) {
					inputs = methodWithEffectiveOutput2inputs
							.get(method_effectiveOutput);
				} else {
					inputs = new ArrayList<Symbol>();
					methodWithEffectiveOutput2inputs.put(method_effectiveOutput,
							inputs);
				}
				inputs.add(input);
			}
			logger.debug("methodWithEffectiveOutput2inputs: "
					+ methodWithEffectiveOutput2inputs);

			Map<String, List<List<Symbol>>> method2ListsOfInputsWithSameEffectiveOutput = new HashMap<String, List<List<Symbol>>>();
			for (Tuple2<String, String> method_effectiveOutput : methodWithEffectiveOutput2inputs
					.keySet()) {
				String method = method_effectiveOutput.tuple0;
				List<Symbol> listOfInputs = methodWithEffectiveOutput2inputs
						.get(method_effectiveOutput);
				List<List<Symbol>> listOfListOfInputs;
				if (method2ListsOfInputsWithSameEffectiveOutput.containsKey(method)) {
					listOfListOfInputs = method2ListsOfInputsWithSameEffectiveOutput
							.get(method);
				} else {
					listOfListOfInputs = new ArrayList<List<Symbol>>();
					method2ListsOfInputsWithSameEffectiveOutput.put(method,
							listOfListOfInputs);
				}
				// add list of Inputs
				listOfListOfInputs.add(listOfInputs);
			}

			return method2ListsOfInputsWithSameEffectiveOutput;
		}

	private static List<List<Integer>> removeFalseRemovableInputs(
			Map<Integer, Set<Integer>> paramIndex2dependentAbstractVals,
			List<List<Integer>> listOfPossibleRemovableParams) {

		List<List<Integer>> listOfRemovableParams = new ArrayList<List<Integer>>();
		if (listOfPossibleRemovableParams.size() == 0)
			return listOfRemovableParams;
		int numParams = listOfPossibleRemovableParams.get(0).size();
		removableParamLoop: for (List<Integer> possibleRemovableParams : listOfPossibleRemovableParams) {
			for (int paramIndex = 0; paramIndex < numParams; paramIndex++) {
				Integer abstractValue = possibleRemovableParams.get(paramIndex);
				if (abstractValue.intValue() == -1)
					continue;
				/*
				 * // case a) if one of params is dependable param => cannot be
				 * removed if (
				 * paramIndex2dependentAbstractVals.containsKey(paramIndex) &&
				 * paramIndex2dependentAbstractVals
				 * .get(paramIndex).contains(abstractValue) ) { continue
				 * removableParamLoop; // false removable param; continue
				 * searching for really removable ones }
				 */

				// case b) if one of params is not dependable => can be removed
				// => all params must be: dependable or -1

				Set<Integer> dependableParams = null;
				if (paramIndex2dependentAbstractVals.containsKey(paramIndex)) {
					dependableParams = paramIndex2dependentAbstractVals
							.get(paramIndex);
				}
				if (dependableParams == null
						|| !dependableParams.contains(abstractValue)) {
					// abstractValue is not a dependableParam
					// => thus possible removable input with this abstractValue
					// can be safely be removed
					// if (
					// paramIndex2dependentAbstractVals.containsKey(paramIndex)
					// &&
					// !paramIndex2dependentAbstractVals.get(paramIndex).contains(abstractValue)
					// ) {
					listOfRemovableParams.add(possibleRemovableParams);
					continue removableParamLoop; // positive removable param
				}

			}
			// case b) all params are either : -1 or dependable

			// case a) none of the params were dependent
			// listOfRemovableParams.add(possibleRemovableParams);
		}
		return listOfRemovableParams;

	}

	private static void updateDependentAbstraction(
			Map<Integer, Set<Integer>> paramIndex2dependentAbstractVals,
			List<List<Integer>> notRemovableParams) {
		int numParams = notRemovableParams.get(0).size();
		for (List<Integer> params : notRemovableParams) {
			for (int paramIndex = 0; paramIndex < numParams; paramIndex++) {
				Integer abstractValue = params.get(paramIndex);
				if (abstractValue.intValue() != -1) {
					Set<Integer> dependentAbstractValues;
					if (paramIndex2dependentAbstractVals
							.containsKey(paramIndex)) {
						dependentAbstractValues = paramIndex2dependentAbstractVals
								.get(paramIndex);
					} else {
						dependentAbstractValues = new HashSet<Integer>();
						paramIndex2dependentAbstractVals.put(paramIndex,
								dependentAbstractValues);
					}
					dependentAbstractValues.add(abstractValue);
				}
			}
		}

	}

}
