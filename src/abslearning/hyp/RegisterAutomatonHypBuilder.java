package abslearning.hyp;

import java.util.List;

import abslearning.mapper.AbstractionMapper;
import abslearning.mapper.TestingMapper;
import abslearning.stores.CanonicalValueStoreProvider;
import de.ls5.jlearn.interfaces.Automaton;
import sut.info.TomteHypInterface;

public class RegisterAutomatonHypBuilder implements HypothesisBuilder{
	private AbstractionMapper abstractionMapper;
	private List<Integer> constants;
	private CanonicalValueStoreProvider valueStoreProvider;

	public RegisterAutomatonHypBuilder(AbstractionMapper abstractionMapper, List<Integer> constants, CanonicalValueStoreProvider valueStoreProvider) {
		this.abstractionMapper = abstractionMapper;
		this.constants = constants;
		this.valueStoreProvider = valueStoreProvider;
	}

	public TomteHypInterface buildHypothesis(Automaton hypAutomaton, int hypNum) {
		Automaton reducedAutomaton = HypothesisReducer.reduceHypothesisToEliminateNonDeterminism(hypAutomaton);
		return new RegisterAutomaton(hypAutomaton, new TestingMapper(abstractionMapper, reducedAutomaton), constants, valueStoreProvider);
	}
}
