package abslearning.hyp;

import java.util.List;

import abslearning.app.Config;
import abslearning.mapper.AbstractionMapper;
import abslearning.stores.ConfigurableValueStoreProvider;
import abslearning.stores.PredicateStore;

public class HypBuilderStore {

	public static HypothesisBuilder getHypBuilder(Config config, PredicateStore predicateStore, AbstractionMapper abstractionMapper, List<Integer> constants) {
		if (config.learning_generateHypCode == true) {
			return new JarHypBuilder(predicateStore);
		} else {
			return new RegisterAutomatonHypBuilder(abstractionMapper, constants , ConfigurableValueStoreProvider.FRESH);
		}
	}

}
