package abslearning.hyp;

import de.ls5.jlearn.interfaces.Automaton;
import sut.info.TomteHypInterface;

public interface HypothesisBuilder {
	public TomteHypInterface buildHypothesis(Automaton hypAutomaton, int hypNum);
}
