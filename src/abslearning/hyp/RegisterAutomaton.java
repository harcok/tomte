package abslearning.hyp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import abslearning.exceptions.BugException;
import abslearning.mapper.ConcretizingMapper;
import abslearning.relation.Relation;
import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.SymbolImpl;
import sut.info.TomteHypInterface;
import util.Tuple2;

/**
 * Implements a register automaton based on the abstract Mealy Machine obtained for a learned model.
 */
public class RegisterAutomaton implements TomteHypInterface{
	private SortedMap<Integer, Integer> stateVars;
	private Automaton absMachine;
	private State currentState;
	private ConcretizingMapper concretizingMapper;
	public Trace trace;
	private CanonicalValueStoreProvider valueStoreProvider;
	private CanonicalValueStore valueStore;
	private List<Integer> constants;
	
	public RegisterAutomaton(Automaton automaton, ConcretizingMapper abstractionMapper, List<Integer> constants, CanonicalValueStoreProvider valueProvider) {
		this.concretizingMapper = abstractionMapper;
		this.absMachine = automaton;
		this.valueStoreProvider = valueProvider;
		this.constants = constants;
		sendReset();
	}

	public void sendReset() {
		this.currentState = this.absMachine.getStart();
		this.stateVars = new TreeMap<Integer, Integer>();
		this.trace = new Trace();
		this.valueStore = valueStoreProvider.newValueStore();
		this.concretizingMapper.reset();
	}

	public OutputAction sendInput(InputAction concreteInput) {
		this.concretizingMapper.abstractInput(concreteInput, this.stateVars);
		Symbol inputSymbol = new SymbolImpl(concreteInput.getAbstractionString());
		this.trace.add(concreteInput);
		Symbol outputSymbol = currentState.getTransitionOutput(inputSymbol);
		currentState = currentState.getTransitionState(inputSymbol);
		OutputAction concreteOutput = concretizeOutput(concreteInput, outputSymbol.toString());
		this.trace.add(concreteOutput);
		this.trace.updateStatevars(this.stateVars);
		return concreteOutput;
	}
	
	
	public OutputAction concretizeOutput(InputAction concreteInput, String abstractOutputString) {
		Map<Integer, Integer> freshMap = new HashMap<Integer, Integer>();
		OutputAction outputAction = new OutputAction(abstractOutputString);
		String methodName = outputAction.getMethodName();
		LinkedHashMap<Integer, Integer> stateVarUpdates = null;
		Tuple2<String, String> outputSplits = splitOutputString(abstractOutputString, methodName);
		String updatesPrefix = outputSplits.tuple0;
		String outputParamsSuffix = outputSplits.tuple1;
		List<Integer> concreteInputValues = concreteInput.getConcreteParameters();
		
		if (updatesPrefix != null) {
			stateVarUpdates = generateStateVarUpdatesFromPrefix(updatesPrefix, stateVars, concreteInputValues, freshMap);
		}
		if (outputParamsSuffix != null) {
			List<Integer> concreteOutputValues = generateConcreteOutputValuesFromSuffix(outputParamsSuffix, stateVars, concreteInputValues, freshMap);
			if (concreteOutputValues == null) {
				throw new BugException("Couldn't concretize output")
				.addDecoration("output", abstractOutputString).addDecoration("stateVars", this.stateVars)
				.addDecoration("input", concreteInput);
				//return new OutputAction(new ArrayList<Integer>(),AutomatonUtil.UNDEF.toString());
			}
			outputAction.setConcreteValues(concreteOutputValues);
		} else {
			outputAction.setConcreteValues(new ArrayList<Integer>());
		}
		if (stateVarUpdates != null)
			this.stateVars = getUpdatedStateVars(this.stateVars, stateVarUpdates);
		
		return outputAction;
	}
	
	
	private  SortedMap<Integer, Integer> getUpdatedStateVars(SortedMap<Integer, Integer> statevarsBeforeTransition,LinkedHashMap<Integer, Integer> statevarUpdates) {
		
		
		// copy statevarsBeforeTransition in new statevarsAfterTransition
		SortedMap<Integer, Integer> statevarsAfterTransition= new TreeMap<Integer,Integer>(statevarsBeforeTransition);
				

		// then do updates
		for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
			Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
			
			if ( stateVarUpdate != null ) {
				// update statevars
				statevarsAfterTransition.put(stateVarIndex, stateVarUpdate);
			}	
		}	
		

		// first do all unsets and then then the updates 
		//
		// first do all unsets  
		for ( Integer stateVarIndex: statevarUpdates.keySet() ) {
			Integer stateVarUpdate=statevarUpdates.get(stateVarIndex);
			if ( stateVarUpdate == null ) {
				// update statevars
				statevarsAfterTransition.remove(stateVarIndex);
			}
		}
		
		return statevarsAfterTransition;
	}	
	
	private Tuple2<String, String> splitOutputString(String abstractOutputString, String outputMethod) {
		int endOfPrefix = abstractOutputString.indexOf(outputMethod);
		int startOfSuffix = endOfPrefix + outputMethod.length();
		String prefix = (endOfPrefix!=0)?abstractOutputString.substring(0, endOfPrefix-1):null;
		String suffix = (startOfSuffix != abstractOutputString.length())?abstractOutputString.substring(startOfSuffix+1, abstractOutputString.length()):null;
		return new Tuple2<String,String>(prefix, suffix);
	}
	
	/**
	 * Maps the output abstract parameters to concrete values. 
	 * @return list of values or null if some of the abstractions are undefined (like for example, state var abstractions)  
	 */
	private List<Integer> generateConcreteOutputValuesFromSuffix(String suffix, SortedMap<Integer,Integer> stateVars, List<Integer> concreteInputParamValues, Map<Integer, Integer> freshMap) {
		List<Integer> concreteOutputValues = new ArrayList<Integer>();
		String[] absOutputParams = suffix.split("_");
		for (String outputParam : absOutputParams) {
			if (outputParam.isEmpty()) {
				continue;
			}
			Integer paramValue = varStringToValue(outputParam, stateVars, concreteInputParamValues, freshMap);
			if (paramValue == null) {
				return null;
			}
			concreteOutputValues.add(paramValue);
		}
		return concreteOutputValues;
	}
	
	//TODO !! This code seems to be a duplicate of what's already present in AbstractionMapper. Should be merged with it.
	private LinkedHashMap<Integer, Integer> generateStateVarUpdatesFromPrefix(String updatesPrefix, SortedMap<Integer,Integer> stateVars, 
			List<Integer> concreteInputParamValues, Map<Integer, Integer> freshMap) {
		LinkedHashMap<Integer, Integer> updateMap = new LinkedHashMap<Integer, Integer>();
		String[] split = updatesPrefix.split("_");
		for (String update : split) {
			if (update.isEmpty()) {
				continue; 
			}
			if (update.contains("=")) {
				Integer newConcreteValue = null;
				String [] updatePiece = update.split("=");
				String leftOperand = updatePiece[0];
				String rightOperand = updatePiece[1];
				if (leftOperand.charAt(0) != 'x') {
					typeException("left update operand",leftOperand.charAt(0));
				}
				Integer stateVarIndex = Integer.valueOf(leftOperand.substring(1));
				newConcreteValue = varStringToValue(rightOperand, stateVars, concreteInputParamValues, freshMap);
				
				updateMap.put(stateVarIndex, newConcreteValue);
			} else {
				char varType = update.charAt(0);
				Integer varIndex = Integer.valueOf(update.substring(1)); 
				switch(varType){
				case 'f':
					if (!freshMap.containsKey(varIndex))
					freshMap.put(varIndex, valueStore.popFreshValue());break;
				case 'x': 
					updateMap.put(varIndex, null);
					break;
				default: typeException("pre update",varType);
				}
			}
		}
		
		return updateMap;
	}

	//TODO Is it a problem that variables can be null at this point
	private Integer varStringToValue(String absVar, SortedMap<Integer, Integer> stateVars, List<Integer> concreteInputParamValues,
			Map<Integer, Integer> freshMap) {
		
		Integer concreteValue = null;
		Integer varIndex = Integer.valueOf(absVar.substring(1));
		char varType = absVar.charAt(0);
		try {
		switch(varType) {
		case 'x': concreteValue = stateVars.get(varIndex); break;
		case 'i':
		    if (Relation.SUCC.isActive())
		        concreteValue = Relation.SUCC.map(stateVars.get(varIndex));
		    else
		        concreteValue = Relation.NEXT.map(stateVars.get(varIndex)); 
		    break;
		case 'f': concreteValue = freshMap.get(varIndex); break;
		case 'c': concreteValue = constants.get(varIndex); break;
		case 'p': concreteValue = concreteInputParamValues.get(varIndex); break;
		case 'q': 
		    if (Relation.SUCC.isActive())
		        concreteValue = Relation.SUCC.map(concreteInputParamValues.get(varIndex));
		    else
		        concreteValue = Relation.NEXT.map(concreteInputParamValues.get(varIndex));
		    break;
		case '?': throw new BugException("The abstract model is not well defined");
		default: typeException("abstract variable",varType);
		}
		} catch(NullPointerException nl) {
		} 
		if (concreteValue == null && (varType != 'x' && varType != 'i')) {
			throw new BugException("Couldn't concretize abs variable " + absVar).
			addDecoration("absVar", absVar).addDecoration("stateVars", stateVars). 
			addDecoration("concreteInputParamValues", concreteInputParamValues).
			addDecoration("freshMap", freshMap).addDecoration("trace", this.trace.toStringWithStatevars());
			
		}
		return concreteValue;
	}
	
	private void typeException(String position, char type) {
		throw new BugException("Invalid " + position + " value type " + type);
	}

	@Override
	public SortedMap<Integer, Integer> getStatevars() {
		return stateVars;
	}
	
}
