package abslearning.hyp;

import abslearning.app.HypStore;
import abslearning.learner.Main;
import abslearning.logger.LearnLogger;
import abslearning.stores.PredicateStore;
import de.ls5.jlearn.interfaces.Automaton;
import sut.info.SutInfo;
import sut.info.TomteHypInterface;
import sut.interfaces.SutInterface;
import sut.interfacing.DeterminizerWrapper;
import sut.interfacing.HypWithFullInfoWrapper;

public class JarHypBuilder implements HypothesisBuilder{
	private PredicateStore predicateStore;

	public JarHypBuilder(PredicateStore predicateStore) {
		this.predicateStore = predicateStore;
	}

	public TomteHypInterface buildHypothesis(Automaton hypAutomaton, int hypCounter) {
		Automaton reducedHypothesis = HypothesisReducer.reduceHypothesisToEliminateNonDeterminism(hypAutomaton);
		LearnLogger.logAbstractReducedHypothesisDot(reducedHypothesis, hypCounter);
		LearnLogger.logAbstractions(hypCounter, predicateStore);
		createHyp( predicateStore, hypCounter, false);
		newInstanceOfHyp(hypCounter);
		createHyp( predicateStore, hypCounter, true);
		TomteHypInterface concreteHypWithFullInfo = newInstanceOfHypWithFullInfo(hypCounter);

		return concreteHypWithFullInfo ;
	}

	private void createHyp(PredicateStore predicateStore, int hypCounter, boolean fullHypOutput)  {

		String suffix = fullHypOutput ? "fullHypOutput" : "";
		String jarFile= HypStore.getHypJarFile(hypCounter, suffix) ;
		String classPackage="hypothesis";
		String className="Hyp"+suffix +hypCounter;


	    String abstractModelDotFilename = HypStore.getAbstractReducedHypothesisDotFile(hypCounter);
	    String abstractionsJsonFilename = HypStore.getAbstractionsJsonFile(hypCounter);

	    sut.implementation.Util.createJarFileFromAbstractionsAndAbstractModel(
	    		abstractionsJsonFilename,abstractModelDotFilename,
	    		jarFile, classPackage, className,fullHypOutput);
	}


	private  sut.info.TomteSutInterface newInstanceOfHyp(int counter) {
		SutInterface concHyp = newInstanceOfHyp(counter, false);
		sut.info.TomteSutInterface hyp = new DeterminizerWrapper(concHyp, SutInfo.getConstants());
		return hyp;
	}

	private  sut.info.TomteHypInterface newInstanceOfHypWithFullInfo(int counter) {
		SutInterface concHyp = newInstanceOfHyp(counter, true);
		sut.info.TomteHypInterface hyp = new HypWithFullInfoWrapper(new DeterminizerWrapper(concHyp, SutInfo.getConstants()));
		return hyp;
	}


	private SutInterface newInstanceOfHyp(int hypCounter, boolean fullHypOutput)  {
		String suffix = fullHypOutput ? "fullHypOutput" : "";
		String jarFile=HypStore.getHypJarFile(hypCounter, suffix);
		String classPackage="hypothesis";
		String className="Hyp"+suffix +hypCounter;

		SutInterface concHyp = (sut.interfaces.SutInterface) util.JavaUtil.loadJavaClass(jarFile,classPackage + "." + className);
	    return concHyp;
	}
}
