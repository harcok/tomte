package abslearning.exceptions;

public class ConfusionException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ConfusionException(){}
	
	public ConfusionException(String msg) {
		super(msg);
	}

}
