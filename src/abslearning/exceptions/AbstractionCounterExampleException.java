package abslearning.exceptions;

import abslearning.ceanalysis.reduction.Counterexample;

public class AbstractionCounterExampleException extends CounterExampleException {

	public AbstractionCounterExampleException(Counterexample counterexample) {
		super(counterexample);
	}

}
