package abslearning.exceptions;

import abslearning.ceanalysis.reduction.Counterexample;

public class LearnerCounterExampleException extends CounterExampleException {

	public LearnerCounterExampleException(Counterexample counterexample) {
		super(counterexample);
	}

}
