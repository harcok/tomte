package abslearning.exceptions;

import abslearning.app.Statistics;
import abslearning.trace.Trace;

public class TesterException extends DecoratedRuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TesterException(Exception exception, Trace sutTrace, int testIndex, int transitionIndex) {
		super();
		exception.printStackTrace();
		super.addDecoration("testIndex", testIndex).
		addDecoration("runIndex", Statistics.getInstance().getCurrentRunNumber()).
		addDecoration("transitionIndex", transitionIndex).
		addDecoration("sutTrace", sutTrace.toStringWithStatevars());
	}
}
