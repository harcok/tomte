package abslearning.exceptions;

public class MaxNumRunsException extends RuntimeException{
	private static final long serialVersionUID = 3349908291398696155L;

	public MaxNumRunsException(int maxNumRuns) {
		super("The system could not be learned in "+ maxNumRuns + " runs. Set learner_maxNumRuns higher or remove it from the config file");
	}
}
