package abslearning.exceptions;

public class ValueCannotBeDecanonizedException extends DecoratedRuntimeException {
	private Integer value;
	private String valueMapper;
	public ValueCannotBeDecanonizedException(Integer value, String valueMapper) {
		super();
		this.value = value;
		this.valueMapper = valueMapper;
	}
	

	private static final long serialVersionUID = -5853772282610882668L;
	
	
	public String getMessage() {
		String msg =  "value " + value + " cannot be decanonized\n";
		msg+= "in value mapper: "+ valueMapper.toString() + 
				super.getMessage();
		return msg;
	}
	

}
