package abslearning.exceptions;

import abslearning.tree.Inconsistency;

public class InconsistencyException extends DecoratedRuntimeException {
	private boolean resolved;

	public InconsistencyException(Inconsistency inconsistency, boolean resolved) {
		super();
		this.resolved = resolved;
		this.addDecoration("inconsistency", inconsistency);
		this.addDecoration("resolved", resolved);
	}
	
	public boolean wasResolvedSuccessfully() {
		return resolved;
	}
}
