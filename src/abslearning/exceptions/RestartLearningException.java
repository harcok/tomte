package abslearning.exceptions;

import abslearning.ceanalysis.reduction.Counterexample;

public class RestartLearningException extends RuntimeException {
	private static final long serialVersionUID = 3349908291398696155L;
	private Counterexample counterexample;
	public String type;

	public RestartLearningException(String message, Counterexample counterexample,String type) {
		super(message);
		this.counterexample = counterexample;
		this.type=type;
	}
	
	/*public RestartLearningException(String message) {
		super(message);
	}*/
	
	public Counterexample getCounterexample() {
		return counterexample;
	}
}
