package abslearning.exceptions;

import abslearning.ceanalysis.reduction.Counterexample;

public abstract class CounterExampleException extends RuntimeException {
	private Counterexample counterexample;

	private static final long serialVersionUID = -510834207835758474L;

	public CounterExampleException(Counterexample counterexample) {
		this.counterexample = counterexample;
	}

	public Counterexample getCounterexample() {
		return counterexample;
	}
}
