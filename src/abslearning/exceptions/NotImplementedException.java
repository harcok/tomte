package abslearning.exceptions;

public class NotImplementedException extends RuntimeException{
	private static final long serialVersionUID = 3349908291398696166L;

	public NotImplementedException() {
		super("Not implemented");
	}
}

