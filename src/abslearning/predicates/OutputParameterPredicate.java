package abslearning.predicates;

import java.util.ArrayList;
import java.util.List;

import abslearning.trace.AbstractionVariable;
//import abslearning.trace.ActionParameterIndexFirstLast;

public class OutputParameterPredicate {
	private List<AbstractionVariable> predicateIndexes;

	public OutputParameterPredicate() {
		predicateIndexes = new ArrayList<AbstractionVariable>();
	}

	public AbstractionVariable get(int index) {
		return predicateIndexes.get(index);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof InputParameterPredicate))
			return false;

		OutputParameterPredicate that = (OutputParameterPredicate) obj;

		if (!predicateIndexes.equals(that.predicateIndexes)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return predicateIndexes.toString();
	}

	@Override
	public int hashCode() {
		return predicateIndexes.hashCode();
	}

	public int add(AbstractionVariable predicate) {
		predicateIndexes.add(predicate);
		return predicateIndexes.size()-1;
	}

	public List<AbstractionVariable> getAll() {
		return new ArrayList<AbstractionVariable>(predicateIndexes);
	}
	
	public void setAll( List<AbstractionVariable> predicateIndexes) {
		this.predicateIndexes = predicateIndexes;
	}	

	public int size() {
		return predicateIndexes.size();
	}

	public int indexOf(AbstractionVariable newActionParamIndex) {
		return predicateIndexes.indexOf(newActionParamIndex);
	}
	
	public void remove(AbstractionVariable apiToRemove) {
		predicateIndexes.remove(apiToRemove);
	}
}
