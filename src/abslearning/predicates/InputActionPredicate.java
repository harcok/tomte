package abslearning.predicates;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.trace.AbstractionVariable;

/**
 * PredicateStore
 *    contains two maps :
 *       inputActionPredicates :  methodname -> InputActionPredicate
 *       OutputActionPredicate :  methodname -> OutputActionPredicate
 * 
 * InputActionPredicate
 *     Contains for each parameter of the action an InputParameterPredicate object.
 * 
 * InputParameterPredicate 
 *     Contains a set of abstractions for that parameter
 *
 */
public class InputActionPredicate {
	private static final Logger logger = Logger.getLogger(InputActionPredicate.class);
	
	
	// predicates : list which  parameterIndex -> list of abstractions for that parameter
	//   - index is parameterIndex
	//   - value of index is  list of abstractions (api:actionParameterIndex) for that parameter
	private List<InputParameterPredicate> predicates;
	
	public InputActionPredicate(int size) {
		predicates = new ArrayList<InputParameterPredicate>();
		for (int i = 0; i < size; i++) {
			predicates.add(new InputParameterPredicate());
		}
	} 
	
	public void clearPredicates() {
		for (int i = 0; i < predicates.size(); i++) 
			predicates.get(i).clear();
	}
	
	public boolean addPredicate(int parameterIndex, AbstractionVariable predicate) {
		return predicates.get(parameterIndex).add(predicate);
	}
	


	public InputParameterPredicate get(int parameterIndex) {
		return predicates.get(parameterIndex);
	}

	public List<InputParameterPredicate> getAll() {
		return new ArrayList<InputParameterPredicate>(predicates);
	}
	
	public boolean containsInputParameterPredicate(Integer paramIndex, AbstractionVariable predIndex) {	
		logger.fatal("containsInputParameterPredicate");
		logger.fatal("InputActionPredicate: " + this);
		logger.fatal("paramIndex: " + paramIndex);
		logger.fatal("predIndex: " + predIndex);
		return predicates.get(paramIndex).containsInputParameterPredicate(predIndex);							
	}
	
	
	// returns abstract parameter for given abstraction 
	public int indexOf(int paramIndex, AbstractionVariable abstraction) {
		return predicates.get(paramIndex).indexOf(abstraction);
	}		
	
	

	public int size() {
		return predicates.size();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof InputActionPredicate))
			return false;

		InputActionPredicate that = (InputActionPredicate) obj;

		if (!predicates.equals(that.predicates)) {
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		return predicates.hashCode();
	}

	@Override
	public String toString() {
		return predicates.toString();
	}
	

}
