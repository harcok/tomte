package abslearning.predicates;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import abslearning.ceanalysis.AbstractionPair;
import abslearning.stores.PredicateStore;

public class PredicateManager {
	private final PredicateStore predicateStore;
	
	public PredicateManager(PredicateStore predicateStore) {
		this.predicateStore = predicateStore;
	}
	
	public void updatePredicateStoreWithNewAbstractions(List<AbstractionPair> abstractionPairs ) {
		Map<Integer, List<AbstractionPair>> indexToAbstractionMap = arrangeAbstractionsByActionIndex(abstractionPairs);
		for (Integer actionIndex : indexToAbstractionMap.keySet()) {
			List<AbstractionPair> abstractionsForAction = indexToAbstractionMap.get(actionIndex);
			
		}
		
	}
	
	private Map<Integer, List<AbstractionPair>> arrangeAbstractionsByActionIndex (List<AbstractionPair> abstractionPairs ) {
		Map<Integer, List<AbstractionPair>> actionIndexToAbstraction = new LinkedHashMap<>();
		for (AbstractionPair absPair : abstractionPairs) {
			Integer actionIndex = absPair.parameter.getAction().getTransitionIndex();
			if (!actionIndexToAbstraction.containsKey(actionIndex)) {
				actionIndexToAbstraction.put(actionIndex, new ArrayList<AbstractionPair>());
			}
			actionIndexToAbstraction.get(actionIndex).add(absPair);
		}
		return actionIndexToAbstraction;
	}
}
