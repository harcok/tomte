package abslearning.predicates;

import java.util.ArrayList;
import java.util.List;

import abslearning.trace.AbstractionVariable;

/**
 * Contains a list of the ActionParameterIndexes which a parameter 
 * of an input action can equal.
 *
 */
public class InputParameterPredicate {
	private List<AbstractionVariable> predicateIndexes;
	
	public InputParameterPredicate() {
		predicateIndexes = new ArrayList<AbstractionVariable>();
	}
	
	public AbstractionVariable get(int index) {
		return predicateIndexes.get(index);
	}
	
	public List<AbstractionVariable> getAll() {
		return new ArrayList<AbstractionVariable>(predicateIndexes);
	}
	
	public void clear() {
		predicateIndexes.clear();
	}
	
	/**
	 * Returns a sub list of predicates (abstractions) between the specified fromIndex, inclusive, and toIndex, exclusive  
	 * @param fromIndex Start of the list, inclusive
	 * @param toIndex End of the list, exclusive
	 * @return The sub list of predicates (abstractions)
	 */
	public List<AbstractionVariable> getSubList(int fromIndex, int toIndex) {
		return new ArrayList<AbstractionVariable>(predicateIndexes.subList(fromIndex, toIndex));
	}
	
	public boolean containsInputParameterPredicate(AbstractionVariable predIndex) {	
		return predicateIndexes.contains(predIndex);
	}
	
	public int size() {
		return predicateIndexes.size();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof InputParameterPredicate))
			return false;

		InputParameterPredicate that = (InputParameterPredicate) obj;

		if (!predicateIndexes.equals(that.predicateIndexes)) {
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		return predicateIndexes.hashCode();
	}
	
	
	
	
	@Override
	public String toString() {
		return predicateIndexes.toString();
	}
	


	public boolean add(AbstractionVariable predicate) {
		if (predicateIndexes.contains(predicate)) {
			return false; //throw new BugException("abstraction: " + predicate + " already found!");
		}
		predicateIndexes.add(predicate);
		java.util.Collections.sort(predicateIndexes); // does inline sort
		return true;
	}

	public int indexOf(AbstractionVariable newActionParamIndex) {
		return predicateIndexes.indexOf(newActionParamIndex);
	}	
	
}
