package abslearning.predicates;

import java.util.ArrayList;
import java.util.List;

import abslearning.trace.AbstractionVariable;
//import abslearning.trace.ActionParameterIndexFirstLast;

public class OutputActionPredicate {
	private List<OutputParameterPredicate> predicates;

	public OutputActionPredicate(int size) {
		predicates = new ArrayList<OutputParameterPredicate>();
		for (int i = 0; i < size; i++) {
			predicates.add(new OutputParameterPredicate());
		}
	}

	public int addPredicate(int paramIndex, AbstractionVariable predicate)  {
		return predicates.get(paramIndex).add(predicate);
	}

	public int size() {
		return predicates.size();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof InputActionPredicate))
			return false;

		OutputActionPredicate that = (OutputActionPredicate) obj;

		if (!predicates.equals(that.predicates)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return predicates.hashCode();
	}

	@Override
	public String toString() {
		return predicates.toString();
	}

	public OutputParameterPredicate getParamPredicate(int i) {
		return predicates.get(i);
	}

	public List<OutputParameterPredicate> getAll() {
		return new ArrayList<OutputParameterPredicate>(predicates);
	}
/*	
	public void setAll( List<OutputParameterPredicate> predicates ) {
		 this.predicates=  predicates;
	}	
*/
	public int indexOf(int i, AbstractionVariable newActionParamIndex) {
		return predicates.get(i).indexOf(newActionParamIndex);
	}
}
