package abslearning.tree;

import java.util.function.BiConsumer;

import org.apache.log4j.Logger;

import abslearning.exceptions.BugException;
import abslearning.exceptions.Messages;
import abslearning.exceptions.RestartLearningException;

/**
 * TODO The inconsistency handling is not ideal. In case of SURPRESS, what we would actually
 * want to happen is for the lookaheadTraceRunner to re-run the whole trace instead of just
 * ignoring the inconsistency. The problem is, this breaks the SutTrace interface.
 */
public interface InconsistencyHandling {
	static Logger logger = Logger.getLogger(InconsistencyHandling.class);
	
	/**
	 * Do nothing on inconsistency
	 */
	public static BiConsumer<Inconsistency, Boolean> SURPRESS
	= (t, u) -> { 
	};
	
	
	/**
	 * Restart learning, in case store update fails
	 */
	public static BiConsumer<Inconsistency, Boolean> VERIFY_RESTART_LEARNING 
	= (t, u) -> {
		if (!u) {
			throw new BugException("Unresolved inconsistency, output lookahead traces already in store")
			.addDecoration("inconsistency\n", t);
		} else {
			throw new RestartLearningException(Messages.RESTART_LEARNING, null,"newLookaheadTraceFoundByInconsistencyInTree");
		}
	};
}
