package abslearning.tree.action;

public class Parameter {
	private int concreteValue;
	private int parameterIndex;
	private Action action;


	public Parameter( int concreteValue, 
			 int parameterIndex) {
		this.concreteValue = concreteValue;
		this.parameterIndex = parameterIndex;	
	}
	
	public Parameter(Action action, int concreteValue, 
			 int parameterIndex) {
		this(concreteValue, parameterIndex);
		setAction(action);
	}
	
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}
	

	public int getConcreteValue() {
		return concreteValue;
	}

	public int getParameterIndex() {
		return parameterIndex;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Parameter)) {
			return false;
		}
		
		Parameter that = (Parameter)obj;
		
		if (concreteValue!=that.concreteValue) {
			return false;
		}
		
		if (parameterIndex != that.parameterIndex) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return parameterIndex;
	}

	@Override
	public String toString() {
		return Integer.toString(concreteValue); 
	}
}
