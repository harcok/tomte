package abslearning.tree.action;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Action  {
	public static final String CONSTANT_ACTION_NAME = "_CONSTANTS_";
	private int traceIndex;
	private String methodName;
	private Parameter [] parameters;
	
	
	public Action(String methodName, List<Parameter> parameters) {
		this.methodName = methodName.intern();
		this.parameters = new Parameter [parameters.size()];
		int index = 0;
		for (Parameter parameter : parameters) {
			this.parameters[index++]= new Parameter( parameter.getConcreteValue(), parameter.getParameterIndex());
		}
	}
	
	public Action(abslearning.trace.action.Action action) {
		List<abslearning.trace.action.Parameter> inParameters;
		inParameters=action.getParameters();
		Parameter [] parameters=new Parameter[inParameters.size()];
		int index = 0;
		for (abslearning.trace.action.Parameter p: inParameters ) {
			int value=   ( (abslearning.trace.action.IntegerParamValue) p.getConcreteValue() ).getValue(); 
			Parameter newParam=new Parameter(this, value, p.getParameterIndex());
			parameters[index++] = newParam;
		}	
		this.traceIndex = action.getTraceIndex();
		this.methodName = action.getMethodName().intern();
		this.parameters = parameters;	
	}	
		
	public String getMethodName() {
		return methodName;
	}

	public List<Parameter> getParameters() {
		return Arrays.asList(parameters);
	}
	
	public ArrayList<Integer> getConcreteParams() {
		ArrayList<Integer> concreteParams = new ArrayList<Integer>();
		for(Parameter p : parameters){
			concreteParams.add(new Integer(p.getConcreteValue()));
		}
		return concreteParams;
	}

	public Parameter getParam(int index) {
		return this.parameters[index];
	}
	
	public Parameter getParam(Integer index){
		return this.parameters[index.intValue()];
	}
	
	public int getParamCount() {
		return this.parameters.length;
	}
	
	
	public Integer getMaxParamValue(){
		
		
		if (this.parameters.length == 0 ) {
			return null;
		}
		
		int maxVal=parameters[0].getConcreteValue();
		for (Parameter parameter : parameters) {
			int val= parameter.getConcreteValue();
			if ( val > maxVal ) maxVal=val;			
		}		
		return  new Integer(maxVal);		
	}	
	
	public int getTraceIndex() {
		return traceIndex;
	}

	public void setTraceIndex(int traceIndex) {
		this.traceIndex = traceIndex;
	}


	public String toString() {
		StringBuilder result = new StringBuilder(methodName);

		if (parameters.length != 0) {
			result.append(" ");
			for (Parameter p : parameters) {
				result.append(p.toString()).append(" ");
			}
		}
		return result.toString().trim();
	}
	

	public String toFunctionCallString() {
		StringBuilder result = new StringBuilder(methodName);
		result.append("(");
		
		if (parameters.length != 0) {			
			for (Parameter p : parameters) {
				result.append(p.toString()).append(",");
			}
			// delete last ,
			result.deleteCharAt(result.length()-1);
		}
		
		result.append(")");

		String s=result.toString();
		
		
		return s;

		//return result.toString().trim() +  " meth_idx:" + methodIndex;
	}	
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof Action))
			return false;

		Action that = (Action) obj;

		if (!methodName.equals(that.methodName)) {
			return false;
		}

		if (!Arrays.equals(parameters,that.parameters)) {
			return false;
		}
		
		if (traceIndex != that.traceIndex) {
			return false;
		}

		return true;
	}
	

	public boolean equalsMethodParams(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof Action))
			return false;

		Action that = (Action) obj;

		if (!methodName.equals(that.methodName)) {
			return false;
		}
		

		if (!Arrays.equals(parameters,that.parameters)) {
			return false;
		}

		return true;
	}
	
	
	
	
	@Override
	public int hashCode() {
		return methodName.hashCode() + parameters.hashCode() + traceIndex;
	}

	public boolean isConstantAction() {
		return getMethodName().equals(CONSTANT_ACTION_NAME);
	}
}

