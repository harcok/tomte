package abslearning.tree.action;

import java.util.List;

public class InputAction extends Action {


	public InputAction(abslearning.trace.action.Action action) {
		super(action);		
	}

	public InputAction(String methodName, List<Parameter> parameters) {
		super(methodName, parameters);
	}

}
