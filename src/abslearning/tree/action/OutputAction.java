package abslearning.tree.action;

import java.util.List;

public class OutputAction extends Action {
	
	public OutputAction(abslearning.trace.action.Action action) {
		super(action);		
	}
	public OutputAction(String methodName, List<Parameter> parameters) {
		super(methodName, parameters);
	}
}
