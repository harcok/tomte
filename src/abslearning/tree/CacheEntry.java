package abslearning.tree;

import java.util.LinkedHashSet;

import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

public class CacheEntry {
	private final InputAction input;
	private final OutputAction output;
	private final LinkedHashSet<Integer> memV;
	private final int versionId;


	public CacheEntry(InputAction input, OutputAction output, LinkedHashSet<Integer> memV, int versionId) {
		this.input = input;
		this.output = output;
		this.memV = memV;
		this.versionId = versionId;
	}
	
	public int getVersion() {
		return versionId;
	}
	
	public InputAction getInput() {
		return input;
	}
	
	public OutputAction getOutput() {
		return output;
	}

	public LinkedHashSet<Integer> getMemV() {
		return memV;
	}
}
