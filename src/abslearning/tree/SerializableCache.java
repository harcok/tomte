package abslearning.tree;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import abslearning.trace.action.InputAction;

/**
 * A serializable cache is a minimal cache that can be serialized. 
 * This is used for dumping or loading useful cache (which implements
 * the CacheIterator interface)
 */
public class SerializableCache implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3505726517586169378L;
	private Node root;

	public SerializableCache() {
	}
	
	
	public void fillCache(CacheIterator cache) {
		root = new Node();
	 	fillCache( cache, root);
	}
	
	public void dumpCache(CacheIterator cache) {
		dumpCache(cache, root);
	}
	
	private void dumpCache(CacheIterator cache, Node node) {
		for (Transition trans : node.getTransitions()) {
			cache.addToCache((abslearning.trace.action.InputAction)trans.input.toTraceAction(true), 
					(abslearning.trace.action.OutputAction)trans.output.toTraceAction(false));
			dumpCache(cache, node.getChild(trans));
		}
		cache.previous();
	}


	private void fillCache(CacheIterator cache,  Node node) {
		for (InputAction input : cache.next()) {
			CacheEntry entry = cache.next(input);
			Node child = new Node();
			node.addNode(new Transition(new Action(input), new Action(entry.getOutput())), child);
			fillCache(cache, child);
		}
		cache.previous();		
	}
	
	

	static class Node implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 674822318825938492L;
		private Map<Transition, Node> children;
		
		public Node() {
			children = new LinkedHashMap<Transition, Node>();
		}
		
		public void addNode(Transition transition, Node node) {
			children.put(transition, node);
		}
		
		public boolean hasChild(Transition transition) {
			return children.containsKey(transition);
		}
		
		public Set<Transition> getTransitions() {
			return children.keySet();
		}
		
		public Node getChild(Transition transition) {
			return children.get(transition);
		}
	}
	
	static class Transition implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public final Action input;
		public final Action output;

		public Transition(Action input, Action output) {
			this.input = input;
			this.output = output;
		}
	}
	
	
	static class Action implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String method;
		private Integer [] params;
		public Action(abslearning.trace.action.Action action) {
			method = action.getMethodName();
			params = action.getConcreteParameters().toArray(new Integer [action.getNumberParams()]);
		}
		
		public abslearning.trace.action.Action toTraceAction(boolean isInput) {
			if (isInput)
				return new abslearning.trace.action.InputAction(Arrays.asList(params), method);
			else
				return new abslearning.trace.action.OutputAction(Arrays.asList(params), method);
		}
	}
}
