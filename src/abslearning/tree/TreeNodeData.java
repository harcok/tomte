package abslearning.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import abslearning.tree.action.InputAction;
import abslearning.tree.action.OutputAction;

public class TreeNodeData {
	private int id;
	private InputAction input;
	private OutputAction output;
	
	private int  version;
	
	/*
	//before we had a sorted set for memV, but this does not work, because the values
	//are not ordered necessarily, e.g. 1 followed by 0, where 0 refers to a previous value
	//then a sorted set does not work any more
	
    */
	private Integer [] memV;
	//private LinkedHashSet memV;

	public TreeNodeData(int id, InputAction input,OutputAction output) {
		this.setId(id);
		this.setInput(input);
		this.setOutput(output);	
		
		this.version=-1; // version of updating memV, initially is -1
		
		this.memV=new Integer [0]; 
		//this.memV = new LinkedHashSet<Integer>();
		//this.memV.add(0);
				
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setInput(InputAction input) {
		this.input = input;
	}

	public InputAction getInput() {
		return input;
	}

	public void setOutput(OutputAction output) {
		this.output = output;
	}

	public OutputAction getOutput() {
		return output;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String toString() {
		return "id: " + id + ", input: " + input.toString() + "/ output: " + output.toString() + ", version: " + version +  ", memV: " +  memVString();
	}
	
	public String toTreeString() {
		 String mem =  memVString().toString();
		 String transition =   input + " &rarr; " + output;
		 String state = "v" + version +  " i" + " memV=" + mem ;
		 String nodeDescString =  transition + "<br/> <br/>" + id + "<br/>" + state;
		 return nodeDescString;
	}
	
	
	private static boolean reducedDisplay = true;

	public String toNodeString() {
		 String mem = memVString();	
		 String nodeStr = "N" + id + "<br/>" + mem;
		 
		 if (!reducedDisplay) {
				 
			 
			String state = "v" + version + ") <br/> memV=" + mem ;
			nodeStr += "<br/>" + state;  
		 } 
		 
		 return nodeStr;
	}
	
	private String memVString(){
		return Arrays.asList(memV).toString();
	}
	
	public String toEdgeString() {
		// String transition =   input + " &rarr; " + output;
		 String transition =   input.toFunctionCallString() + "<br/>" + output.toFunctionCallString(); // + "<br/>" + abstractInput + "(" + abstractInputVersionText + ")";
		 return transition;
	}

	public void setMemV(LinkedHashSet<Integer> newMemV) {
		this.memV= newMemV.toArray(new Integer[newMemV.size()]);
		//this.memV = newMemV;
	}
	
	public LinkedHashSet<Integer> getMemV() {
		LinkedHashSet<Integer> memV = new LinkedHashSet<Integer>();
		for (Integer mem : this.memV) {
			memV.add(mem);
		}
		return memV;	
	}
	
	private static String [] createAlphabet(String prefix,int size) {
		String [] alphabet = new String[size];
		for (int i=0; i<size; i++) {
			alphabet[i] = prefix + "MSG" + i;
		}
		return alphabet;
	}
	
	
	private static int measureNodeCapacity(int inputAlphabetSize, int outputAlphabetSize, long maxDelay) {
		String [] inputAlphabet = createAlphabet("I", inputAlphabetSize);
		String [] outputAlphabet = createAlphabet("O", outputAlphabetSize);
		List<TreeNodeData> treeNodeData = new ArrayList<TreeNodeData> ();
//		java.util.Random rand = new java.util.Random(1); // use this to generate random strings for each action
		long refTime = System.currentTimeMillis();
		int i=0;
		
		for (i=0; i < 10000000; i ++) {
			int inputIndex = i % inputAlphabet.length;
			int outputIndex = i % outputAlphabet.length;
			abslearning.trace.action.InputAction input = new abslearning.trace.action.InputAction(new ArrayList<Integer>(), inputAlphabet[inputIndex]);// + rand.nextInt(1000000));
			abslearning.trace.action.OutputAction output = new abslearning.trace.action.OutputAction(new ArrayList<Integer>(), outputAlphabet[outputIndex]);// + rand.nextInt(1000000));
			treeNodeData.add(new TreeNodeData(i, new InputAction(input), new OutputAction(output)));
			if (i%1000 == 0) {
				System.out.println("progress "+ i/1000);
			} 
			if (i%100 == 0) {
				long currentTime = System.currentTimeMillis();
				if (currentTime-refTime > maxDelay) {
					// if adding additional nodes takes too long, break
					break;
				}
				refTime = currentTime;
			}
		}
		
		return i;
	}
	
	// A method we can run to see how many TreeNodeData instances we can hold in heap.
	public static void main(String args[]) {
		int capacity = measureNodeCapacity(20, 20, 10000);
		System.out.println("Node capacity: " + capacity);
	}
	
}
