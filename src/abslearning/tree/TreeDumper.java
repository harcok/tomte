package abslearning.tree;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.app.Statistics;
import abslearning.tree.TreeLoader.StatisticsObject;
import util.Tuple2;

public class TreeDumper {
	Logger logger = Logger.getLogger(TreeDumper.class);
	private ConcreteTree concreteTree;
	private Statistics statistics;
	private List<Integer> constants;


	public TreeDumper(ConcreteTree concreteTree, Statistics statistics, List<Integer> constants) {
		this.concreteTree = concreteTree;
		this.statistics = statistics;
		this.constants = constants;
		
	}
	
	public  void setupTreeDumperHook(final String fileName) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				logger.fatal("Detected shutdown, dumping cache");
				TreeDumper.this.writeCacheTreeWithStats(fileName);
			}
		});
	}
	

	public void writeCacheTreeWithStats(String fileName) {
		SerializableCache cache = new SerializableCache();
		cache.fillCache(new TreeCacheIterator(this.constants, this.concreteTree));
		StatisticsObject storableStatistics = new StatisticsObject(statistics);
		Tuple2<SerializableCache, StatisticsObject> toDump = new Tuple2<> (cache, storableStatistics);
		try (
				OutputStream file = new FileOutputStream(fileName);
				OutputStream buffer = new BufferedOutputStream(file);
				ObjectOutput output = new ObjectOutputStream(buffer);
				) {
			output.writeObject(toDump);
			output.close();
		}  
		catch (IOException ex){
			logger.fatal("Could not write observation tree: \n " + ex.getMessage());
		}
	}
}
