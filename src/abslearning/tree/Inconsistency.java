package abslearning.tree;

import abslearning.trace.Trace;

public class Inconsistency {
	public final Trace inconsistencyTrace;
	public final Trace fullTrace;
	public final Integer inconsistencyValue;
	public Inconsistency(Trace inconsistencyTrace, Trace fullTrace, Integer inconsistencyValue) {
		super();
		this.inconsistencyTrace = inconsistencyTrace;
		this.fullTrace = fullTrace;
		this.inconsistencyValue = inconsistencyValue;
	}
	
	public String toString() {
		return new StringBuilder().
		append("inconsistencyTrace: ").append(inconsistencyTrace).
		append("\nfullTrace: ").append(fullTrace).
		append("\ninconsistencyValue: ").append(inconsistencyValue).append("\n").toString();
	}
}
