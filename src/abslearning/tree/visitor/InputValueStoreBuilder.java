package abslearning.tree.visitor;

import generictree.GenericTreeIteratorUpwards;
import generictree.GenericTreeNode;
import generictree.TreeNodeVisitor;

import java.util.ArrayList;
import java.util.List;

import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.stores.FreshInputValueStore;
import abslearning.tree.TreeNodeData;

public class InputValueStoreBuilder implements CanonicalValueStoreProvider, TreeNodeVisitor<TreeNodeData>{
	private List<Integer> concreteInputValues; 
	
	public InputValueStoreBuilder() {
	}
	
	public CanonicalValueStore newValueStore() {
		return new FreshInputValueStore(concreteInputValues);
	}
	
	@Override
	public void visit(GenericTreeNode<TreeNodeData> node) {
		this.concreteInputValues = getAllConcreteInputValues(node);
	}
	
	private List<Integer> getAllConcreteInputValues(GenericTreeNode<TreeNodeData> currentNode) {
		List<Integer> inputValues = new ArrayList<Integer>();
		GenericTreeIteratorUpwards<TreeNodeData> upIter = new GenericTreeIteratorUpwards<TreeNodeData>(currentNode);
    	
		while (upIter.hasNext()) {
    		GenericTreeNode<TreeNodeData> upperNode = upIter.fetchNext();
    		abslearning.tree.action.InputAction input = upperNode.getData().getInput();
    		if (input != null && input.getParamCount() > 0) {
    			inputValues.addAll(input.getConcreteParams());
    		}
    	}
    	
    	return inputValues;
	}

}
