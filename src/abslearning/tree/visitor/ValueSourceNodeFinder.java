package abslearning.tree.visitor;

import java.util.Collections;
import java.util.Set;

import abslearning.relation.Relation;
import abslearning.tree.TreeNodeData;
import abslearning.tree.action.InputAction;
import abslearning.tree.action.OutputAction;
import generictree.GenericTreeIteratorUpwards;
import generictree.GenericTreeNode;
import generictree.TreeNodeVisitor;


/**
 * Finds the source node from which a value originates. 
 */
public class ValueSourceNodeFinder implements TreeNodeVisitor<TreeNodeData> {
	private GenericTreeNode<TreeNodeData> sourceNode;
	private Integer value;

	public ValueSourceNodeFinder(Integer value) {
		this.value = value;
	}
	
	public void visit(GenericTreeNode<TreeNodeData> node) {
		this.sourceNode = findSourceNodeOfValue(node, value);
	}

	private GenericTreeNode<TreeNodeData> findSourceNodeOfValue(GenericTreeNode<TreeNodeData> node, Integer value) {
		GenericTreeIteratorUpwards<TreeNodeData> iterator = new GenericTreeIteratorUpwards<TreeNodeData>(node);
		GenericTreeNode<TreeNodeData> sourceNode = null;
		while (iterator.hasNext()) {
			GenericTreeNode<TreeNodeData> currentNode = iterator.fetchNext();
			OutputAction treeOutput = currentNode.getData().getOutput();
			InputAction treeInput = currentNode.getData().getInput();
			Set<Integer> memV;
			if (currentNode.getParent() != null)
				memV = currentNode.getParent().getData().getMemV();
			else 
				memV = Collections.emptySet();
			//TODO ? Are there any type of relation chains needed here.
			if (Relation.hasRelatedValue(memV, value) || Relation.hasRelatedValue(treeInput.getConcreteParams(),value) ||
					Relation.hasRelatedValue(treeOutput.getConcreteParams(), value)) {
				sourceNode = currentNode;
				break;
			}
		}

		return sourceNode;
	}
	
	public GenericTreeNode<TreeNodeData> getSourceNode() {
		return this.sourceNode;
	}
	
}
