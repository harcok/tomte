package abslearning.tree.visitor;

import java.util.ArrayDeque;
import java.util.Deque;

import abslearning.trace.Trace;
import abslearning.trace.action.Action;
import abslearning.tree.TreeNodeData;
import generictree.GenericTreeIteratorUpwards;
import generictree.GenericTreeNode;
import generictree.TreeNodeVisitor;

public class TraceToAncestorNodeBuilder implements TreeNodeVisitor<TreeNodeData>{
	private Trace trace = null;
	private GenericTreeNode<TreeNodeData> ancestorNode = null; 
	
	
	public TraceToAncestorNodeBuilder(GenericTreeNode<TreeNodeData> node) {
		this.ancestorNode = node;
	}
	
	public Trace getTrace() {
		return this.trace;
	}


	public void visit(GenericTreeNode<TreeNodeData> node) {
		this.trace = buildTraceToAncestor(node, this.ancestorNode);
	}
	
	private abslearning.trace.Trace buildTraceToAncestor(GenericTreeNode<TreeNodeData> ancestorNode, GenericTreeNode<TreeNodeData> fromNode) {

        GenericTreeIteratorUpwards<TreeNodeData> upIter = new GenericTreeIteratorUpwards<TreeNodeData>(fromNode);
        Deque<Action> actions = new ArrayDeque<Action>();
		abslearning.trace.Trace traceConnectingNodes = null;
		while(upIter.hasNext()) {
			GenericTreeNode<TreeNodeData> node = upIter.next();
			if(node.equals(ancestorNode)) break;
			abslearning.trace.action.InputAction concreteInput = new abslearning.trace.action.InputAction(node.getData().getInput());
			abslearning.trace.action.OutputAction concreteOutput = new abslearning.trace.action.OutputAction(node.getData().getOutput());
			actions.addFirst(concreteOutput);
			actions.addFirst(concreteInput);
		}
		traceConnectingNodes = new Trace(actions);
		
		return traceConnectingNodes;
	}
}
