package abslearning.tree.visitor;

import generictree.GenericTreeIteratorUpwards;
import generictree.GenericTreeNode;
import generictree.TreeNodeVisitor;

import java.util.ArrayList;
import java.util.List;

import abslearning.stores.CanonicalValueStore;
import abslearning.stores.CanonicalValueStoreProvider;
import abslearning.stores.FreshOutputValueStore;
import abslearning.tree.TreeNodeData;

/**
 * Builds a fresh output value store for a node in the obs. 
 */
public class FreshOutputValueStoreBuilder implements CanonicalValueStoreProvider, TreeNodeVisitor<TreeNodeData>{
	private List<Integer> concreteOutputValues;
	
	public FreshOutputValueStoreBuilder() {
	}
	
	public CanonicalValueStore newValueStore() {
		return new FreshOutputValueStore(concreteOutputValues);
	}
	
	public void visit(GenericTreeNode<TreeNodeData> node) {
		this.concreteOutputValues = getAllConcreteOutputValues(node);
	}

	private List<Integer> getAllConcreteOutputValues(GenericTreeNode<TreeNodeData> currentNode) {
		List<Integer> outputValues = new ArrayList<Integer>();
		GenericTreeIteratorUpwards<TreeNodeData> upIter = new GenericTreeIteratorUpwards<TreeNodeData>(currentNode);
    	
		while (upIter.hasNext()) {
    		GenericTreeNode<TreeNodeData> upperNode = upIter.fetchNext();
    		abslearning.tree.action.OutputAction output = upperNode.getData().getOutput();
    		if (output != null && output.getParamCount() > 0) {
    			outputValues.addAll(output.getConcreteParams());
    		}
    	}
    	
    	return outputValues;
	}

}
