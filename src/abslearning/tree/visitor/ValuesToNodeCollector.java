package abslearning.tree.visitor;

import java.util.ArrayList;
import java.util.List;

import abslearning.tree.TreeNodeData;
import generictree.GenericTreeIteratorUpwards;
import generictree.GenericTreeNode;
import generictree.TreeNodeVisitor;

public class ValuesToNodeCollector implements TreeNodeVisitor<TreeNodeData>{
	
	private List<Integer> values = null;
	

	@Override
	public void visit(GenericTreeNode<TreeNodeData> node) {
		this.values = getAllConcreteValues(node);
	}
	
	private List<Integer> getAllConcreteValues(GenericTreeNode<TreeNodeData> node) {
		List<Integer> values = new ArrayList<Integer>();
		GenericTreeIteratorUpwards<TreeNodeData> upIter = new GenericTreeIteratorUpwards<TreeNodeData>(node);
    	
		while (upIter.hasNext()) {
    		GenericTreeNode<TreeNodeData> upperNode = upIter.fetchNext();
    		abslearning.tree.action.InputAction input = upperNode.getData().getInput();
    		if (input != null && input.getParamCount() > 0) {
    			values.addAll(input.getConcreteParams());
    		}
    		abslearning.tree.action.OutputAction output = upperNode.getData().getOutput();
    		if (output != null && output.getParamCount() > 0) {
    			values.addAll(output.getConcreteParams());
    		}
    	}
    	
    	return values;
	}
	
	public List<Integer> getAllConcreteValues() {
		return new ArrayList<Integer>(this.values);
	}
}
