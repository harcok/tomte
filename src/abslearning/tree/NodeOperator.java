package abslearning.tree;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import abslearning.determinizer.Determinizer;
import abslearning.exceptions.BugException;
import abslearning.relation.Relation;
import abslearning.trace.Trace;
import abslearning.tree.action.InputAction;
import abslearning.tree.action.OutputAction;
import abslearning.tree.visitor.TraceToAncestorNodeBuilder;
import abslearning.tree.visitor.ValueSourceNodeFinder;
import abslearning.tree.visitor.ValuesToNodeCollector;
import generictree.GenericTreeIteratorBreadthFirst;
import generictree.GenericTreeNode;

// does most node related operations by using visitor/iterator type classes
// it is not quite general enough, but it does what is required
public class NodeOperator {
	
	/**
	 * Finds the first node which sources the value moving up (to root) in the tree from the given node. Includes the node given.
	 * @return  the first node that has the value as input/output downwards from {@code downFromNode}.
	 */
	//TODO !! Should give the source node even in cases with sequential relation: put 0 /ok  put 1 /ok put 2/ok pop/out(0) 
	public GenericTreeNode<TreeNodeData> findNodeSourceOfValueMovingUp(GenericTreeNode<TreeNodeData> upFromNode, Integer value) {
		ValueSourceNodeFinder valueSourceNodeFinder = new ValueSourceNodeFinder(value);
		upFromNode.accept(valueSourceNodeFinder);
		GenericTreeNode<TreeNodeData> sourceNode = valueSourceNodeFinder.getSourceNode();
		return sourceNode;
	} 
	
	/**
	 * Finds the next node with the value in input/output moving down (to leaf) the tree. Includes the node given.
	 * @return  the next node that has the value as input/output downwards from {@code downFromNode}.
	 */
	public GenericTreeNode<TreeNodeData> findNodeWithValueMovingDown(GenericTreeNode<TreeNodeData> downFromNode, Integer value) {
		GenericTreeIteratorBreadthFirst<TreeNodeData> iter = new GenericTreeIteratorBreadthFirst<TreeNodeData>(downFromNode);
		GenericTreeNode<TreeNodeData> nextNodeWithValue = null;
		while(iter.hasNext()) { 
			GenericTreeNode<TreeNodeData> node = iter.next();
			InputAction treeInput = node.getData().getInput();
			if (treeInput.getConcreteParams().contains(value)) {
				nextNodeWithValue = node;
				break;
			} else {
				 OutputAction treeOutput = node.getData().getOutput();
				 if (treeOutput.getConcreteParams().contains(value)) {
					 nextNodeWithValue = node;
					 break;
				 }
			}
		}
		
		return nextNodeWithValue;
	}
	
	/**
	 * Gets all concrete values going up from <b>startNode</b> to the root of the tree
	 */
	public LinkedHashSet<Integer> getConcreteValuesToNode(GenericTreeNode<TreeNodeData> upFromNode) {
		ValuesToNodeCollector valueCollector = new ValuesToNodeCollector();
		upFromNode.accept(valueCollector);
		List<Integer> concreteValues = valueCollector.getAllConcreteValues();
		//get the values set in the inputs and outputs before the lookahead trace
		LinkedHashSet<Integer> concreteValueSet = new LinkedHashSet<Integer>(concreteValues);
		return concreteValueSet;
	}

	public List<Integer> getFreshValuesInOutput(GenericTreeNode<TreeNodeData> upFromNode, abslearning.tree.action.InputAction concreteAction, abslearning.tree.action.OutputAction concreteOutput) {
		ValuesToNodeCollector collector = new ValuesToNodeCollector();
		upFromNode.accept(collector);
		List<Integer> seenValues = collector.getAllConcreteValues();
		seenValues.addAll(concreteAction.getConcreteParams());
		List<Integer> outputParams = concreteOutput.getConcreteParams();
		List<Integer> paramsRelatedToSeenValues = Relation.getRelatedValuesTo(seenValues, outputParams);
		LinkedHashSet<Integer> freshValues = new LinkedHashSet<Integer>(outputParams);
		freshValues.addAll(outputParams);
		freshValues.removeAll(paramsRelatedToSeenValues);

		for (Integer freshValue : freshValues) {
			if (!Determinizer.isCanonized(freshValue)) {
				throw new BugException("Any new fresh value should have been canonized. " + freshValue);
			}
		}
		
		return new ArrayList<Integer>(freshValues);
	}

	
	public List<Integer> getFreshValuesInOutput(GenericTreeNode<TreeNodeData> upFromNode, abslearning.trace.action.InputAction concreteAction, abslearning.trace.action.OutputAction concreteOutput) {
		ValuesToNodeCollector collector = new ValuesToNodeCollector();
		upFromNode.accept(collector);
		List<Integer> seenValues = collector.getAllConcreteValues();
		seenValues.addAll(concreteAction.getConcreteParameters());
		List<Integer> outputParams = concreteOutput.getConcreteParameters();
		List<Integer> paramsRelatedToSeenValues = Relation.getRelatedValuesTo(seenValues, outputParams);
		LinkedHashSet<Integer> freshValues = new LinkedHashSet<Integer>(outputParams);
		freshValues.addAll(outputParams);
		freshValues.removeAll(paramsRelatedToSeenValues);

		for (Integer freshValue : freshValues) {
			if (!Determinizer.isCanonized(freshValue)) {
				throw new BugException("Any new fresh value should have been canonized. " + freshValue);
			}
		}
		
		return new ArrayList<Integer>(freshValues);
	}
	
	/**
	 * Builds a trace connecting two nodes going down from fromNode (excl.) to toNode (incl.) .
	 * @return  a trace between the two nodes.
	 */
	public Trace buildTraceConnectingNodes(GenericTreeNode<TreeNodeData> fromNode, GenericTreeNode<TreeNodeData> toNode) {
		TraceToAncestorNodeBuilder traceBuilder = new TraceToAncestorNodeBuilder(toNode);
		fromNode.accept(traceBuilder);
		return traceBuilder.getTrace();
	}
}
