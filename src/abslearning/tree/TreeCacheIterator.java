package abslearning.tree;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import abslearning.exceptions.BugException;
import abslearning.relation.Relation;
import abslearning.trace.Trace;
import abslearning.trace.action.Action;
import abslearning.tree.action.InputAction;
import abslearning.tree.action.OutputAction;
import generictree.GenericTreeNode;

/**
 * Implements an iterator over a cache implemented by a tree structure.
 */

public class TreeCacheIterator  implements CacheIterator{
	private static Logger logger = Logger.getLogger(TreeCacheIterator.class); 
	private ConcreteTree concreteTree;
	private GenericTreeNode<TreeNodeData> currentNode;
	private NodeOperator nodeOperator;
	private List<Integer> constants;
	private boolean checkInputConsistency; // if true, input values will also be checked

	
	public TreeCacheIterator(List<Integer> constants, ConcreteTree concreteTree, boolean checkInputConsistency) {
		this.constants = constants;
		this.concreteTree = concreteTree;
		this.currentNode = concreteTree.getRoot();
		this.nodeOperator = new NodeOperator();
		this.checkInputConsistency =checkInputConsistency;
	}
	
	public TreeCacheIterator(List<Integer> constants, ConcreteTree concreteTree) {
		this(constants, concreteTree, false);
	}

	public CacheEntry next(abslearning.trace.action.InputAction input) {
		CacheEntry cachedOutput = null;
		InputAction treeInput = new InputAction(input);
		GenericTreeNode<TreeNodeData> nextNode = concreteTree.getExistingConcreteTreeNode(currentNode,  treeInput);
		if (nextNode != null) {
			cachedOutput = treeToNewEntry(nextNode);
			this.currentNode = nextNode;
		}
		return cachedOutput;
	}
	
	public List<abslearning.trace.action.InputAction> next() {
		List<abslearning.trace.action.InputAction> nextInputs = new ArrayList<>();  
		this.currentNode.getChildren().stream().map(node -> node.getData().getInput()).
		forEach(input -> nextInputs.add(new abslearning.trace.action.InputAction(input)));
		return nextInputs;
	}
	
	public boolean hasNext(abslearning.trace.action.InputAction action) {
		return this.currentNode.getChildren().contains(new InputAction(action));
	}
	
	public CacheEntry current() {
		return treeToNewEntry(this.currentNode);
	}
	
	public CacheEntry previous() {
		CacheEntry previousNode = null;
		if (this.currentNode.getParent() != null) {
			previousNode = treeToNewEntry(this.currentNode.getParent());
			this.currentNode = this.currentNode.getParent();
		}
		return previousNode;
	}
	
	private CacheEntry treeToNewEntry(GenericTreeNode<TreeNodeData> node) {
		TreeNodeData nodeData = node.getData();
		InputAction treeInput = nodeData.getInput();
		OutputAction treeOutput = nodeData.getOutput();
		LinkedHashSet<Integer> memV = nodeData.getMemV();
		int version = nodeData.getVersion();
		abslearning.trace.action.InputAction traceInput = new abslearning.trace.action.InputAction(treeInput);
		abslearning.trace.action.OutputAction traceOutput = new abslearning.trace.action.OutputAction(treeOutput);
		return new CacheEntry(traceInput, traceOutput, memV, version);
	}

	public void addToCache(abslearning.trace.action.InputAction input, abslearning.trace.action.OutputAction output) {
		InputAction treeInput = new InputAction(input);
		GenericTreeNode<TreeNodeData> nextNode = concreteTree.getExistingConcreteTreeNode(currentNode,  treeInput);
		OutputAction treeOutput = new OutputAction(output);

		if (nextNode == null) {
			this.currentNode = concreteTree.addEmptyConcreteTreeNode(this.currentNode, treeInput, treeOutput);
		} else {
//			if (nextNode != null && !treeOutput.equals(nextNode.getData().getOutput())) {
				throw new BugException("Cache overwrite is not allowed");
//			} else {
//				// entry already present in cache, possibly inefficient use of cache
//				logger.fatal("extra input");
//			}
		}
	}
	
	public Trace getTraceSoFar() {
		GenericTreeNode<TreeNodeData> iter = this.currentNode;//.getParent();
		//Trace trace = new Trace();
		Deque<Action> a = new ArrayDeque<Action>();
		while(iter.getParent() != null) {
			a.addFirst(new abslearning.trace.action.OutputAction(iter.getData().getOutput()));
			a.addFirst(new abslearning.trace.action.InputAction(iter.getData().getInput()));
			iter = iter.getParent();
		}
		Trace trace = new Trace (a);
		//Trace traceToRoot =  this.nodeOperator.buildTraceConnectingNodes(this.currentNode, this.concreteTree.getRoot());
		return trace;
	}
	
	
	public Inconsistency updateCache(LinkedHashSet<Integer> memV) {
		Inconsistency inconsistency = null;
		GenericTreeNode<TreeNodeData> parentNode = this.currentNode.getParent();
		InputAction treeInput = this.currentNode.getData().getInput();
		OutputAction treeOutput = this.currentNode.getData().getOutput();
		LinkedHashSet<Integer> oldMemV = this.currentNode.getData().getMemV();
		checkUpdateConsistency(memV, oldMemV);
		Integer inconsistentValue = checkInconsistencyInTree(memV, parentNode, treeInput, treeOutput);
		if (inconsistentValue == null) {
			this.concreteTree.updateConcreteTreeNode(this.currentNode, memV);
		} else {
			inconsistency = buildInconsistencyObject(inconsistentValue, this.currentNode);
			logger.fatal("Inconsistency in tree!\n" + inconsistency);
		}
		return inconsistency;
	}
	
	private void checkUpdateConsistency(LinkedHashSet<Integer> memV, LinkedHashSet<Integer> oldMemV) {
//		if (!memV.containsAll(oldMemV) ) {
//			throw new BugException("Inconsistent update of memorable values \n oldMemV: " + oldMemV + " \n memV: " + memV);
//		}
	}
	
	private Originator originator = new Originator();
	
	/*
	 * Check inconsistency in memV
	 * Values in memV of a node and in the output leading up to it should be found in either:
	 * 		- in the parent memV, 
	 *  	- in the previous input, OR
	 *  	- fresh output values or constants
	 * If they cannot be found in either list, then we have inconsistency in the tree.
	 * 
	 * A similar condition holds for values in the input leading up to the node, which should originate from:
	 *     - parent memV
	 *     - be fresh
	 * @return the inconsistent value; if there is no inconsistency return null
	 */
	private Integer checkInconsistencyInTree(LinkedHashSet<Integer> memV, GenericTreeNode<TreeNodeData> parent, abslearning.tree.action.InputAction concreteInput, abslearning.tree.action.OutputAction concreteOutput){
		//SortedSet<Integer> parentMemV = parent.getData().getMemV();
		LinkedHashSet<Integer> parentMemV = parent.getData().getMemV();
		List<Integer> prevTransitionInputValues = concreteInput.getConcreteParams();
		Set<Integer> valuesToMatch = new HashSet<Integer>();
		boolean isConsistent;
	
		// memV and output inconsistency
		if (!this.checkInputConsistency) {
			List<Integer> freshOutputValues  = nodeOperator.getFreshValuesInOutput(parent, concreteInput, concreteOutput);
			valuesToMatch.addAll(memV);
			valuesToMatch.addAll(concreteOutput.getConcreteParams());
			isConsistent =  originator.from(constants).from(parentMemV).
					from(prevTransitionInputValues).from(freshOutputValues).originate(valuesToMatch);

		// input inconsistency	(originate from prev memV, constants or are fresh)
		} else {
			LinkedHashSet<Integer> valuesToNode = nodeOperator.getConcreteValuesToNode(parent);
			List<Integer> notFreshInputValues = Relation.getRelatedValuesTo(valuesToNode, prevTransitionInputValues);
			valuesToMatch.addAll(notFreshInputValues);
			isConsistent = originator.from(constants).from(parentMemV).originate(valuesToMatch);
		}
		
		if (!isConsistent) {
			Integer inconsistentValue = originator.getOddValue();
			return inconsistentValue;
		}
		
		return null;
	}
	
	private static class Originator {
		private Set<Integer> values;
		private Integer oddValue;
		public Originator() {
			values = new HashSet<Integer>();
		}
		
		public Originator from(Collection<Integer> values) {
			this.values.addAll(values);
			return this;
		}
		
		public boolean originate(Collection<Integer> values) {
			boolean originates = true;
			for(Integer val: values){	
				if (! Relation.hasRelatedValue(this.values, val)) {
					this.oddValue = val; 
					originates = false;
					break;
				}
			}
			this.values.clear();
			return originates;
		}
		
		public Integer getOddValue() {
			return this.oddValue;
		}
	}
	
	
	
	/*
	 * Build Inconsistency object which is used to update the lookahead oracle
	 */
	private Inconsistency buildInconsistencyObject(Integer inconsistentValue, GenericTreeNode<TreeNodeData> currentNode){
        GenericTreeNode<TreeNodeData> losingValueNode = nodeOperator.findNodeSourceOfValueMovingUp(currentNode.getParent(), inconsistentValue);
        nodeShouldExist(losingValueNode, "source node containing value", inconsistentValue);
        GenericTreeNode<TreeNodeData> firstNodeContainingValue = nodeOperator.findNodeWithValueMovingDown(currentNode, inconsistentValue); 
        nodeShouldExist(firstNodeContainingValue, "last node containing value", inconsistentValue);
        abslearning.trace.Trace traceWithInconsistency = nodeOperator.buildTraceConnectingNodes( losingValueNode, firstNodeContainingValue);
        abslearning.trace.Trace fullTrace = nodeOperator.buildTraceConnectingNodes(this.concreteTree.getRoot(), firstNodeContainingValue);
		return new Inconsistency(traceWithInconsistency, fullTrace, inconsistentValue);
	}
	
	private void nodeShouldExist(GenericTreeNode<TreeNodeData> node, String description, Integer inconsistentValue) {
		if (node == null) {
			throw new BugException("Couldn't find " + description)
			.addDecoration("inconsistentValue", inconsistentValue)
			.addDecoration("currentNode", currentNode.getData())
			.addDecoration("children:", currentNode.getChildren());
			
		}
	}
	
	public void reset() {
		this.currentNode = concreteTree.getRoot();
	}
	
	public int getVersion() {
		return this.concreteTree.getVersion();
	}

	public void increaseVersion() {
		this.concreteTree.increaseVersion();
	}

}
