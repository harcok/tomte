package abslearning.tree.trace;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;


//TODO: try to remove Trace argument in sendInput and then replaces
///     with other interface : TomteSutInterface   (Tomte keyword specifies tomte's actions are used)
//      => make TomteSutTreeInterface (which needs trace argument) independent of TomteSutInterface


/**
 * communicate directly with SUT, not using an observation tree
 * basic interface which is extended by SutTreeInterface 
 * 
 */
public interface SutTraceInterface {

	public OutputAction sendInput(InputAction concreteInput, Trace trace);

	public void sendReset();
}
