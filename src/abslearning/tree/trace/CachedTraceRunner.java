package abslearning.tree.trace;

import java.util.ArrayList;
import java.util.List;

import abslearning.exceptions.DecoratedRuntimeException;
import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;
import abslearning.tree.CacheEntry;
import abslearning.tree.CacheIterator;
import sut.info.TomteSutInterface;

/**
 * A sut interface class that utilizes a cache. Whenever possible, queries are answered via
 * the cache.
 *
 * The CachedTraceRunner allows exterior modifications to the cache while running a trace.
 *
 * So it may happen that the CachedTraceRunner hits a cache miss but later on can use
 * the cache again because an exterior modification to the cache happened.
 * So the CachedTraceRunner always checks the cache first, even when it previously had a cache miss before.
 */
public class CachedTraceRunner implements SutTraceInterface {

    private TomteSutInterface sut;
    private CacheIterator cache;
    private boolean useCache;
    private int inputNum; // used for debug
    private List<InputAction> cacheHittingInputs;

    public CachedTraceRunner(TomteSutInterface sut, CacheIterator cache) {
        this.sut = sut;
        this.cache = cache;
        this.cacheHittingInputs = new ArrayList<InputAction>();
        this.inputNum = 0;
    }


    private OutputAction sendInputToSut(InputAction concreteInput, Trace trace) {
        OutputAction concreteOutput = null;

        // so we now run inputs on sut instead and use its result to extend the cache.
        // To do that we first need to update the sut's position to the current one in the cache.
        updateSutToCurrentCachePosition(cacheHittingInputs);
        // We now clear the list, so that the list can again be filled with future cache hit inputs,
        // which again can be used to synchronize the sut on again a cache miss.
        cacheHittingInputs.clear();
        // run input on sut and add input,output pair to cache
        try {
            concreteOutput = sut.sendInput(concreteInput);
        } catch (DecoratedRuntimeException exception) {
            exception.addDecoration("trace", trace);
            exception.addDecoration("input nr", inputNum);
            throw exception;
        }
        // cache input output pair
        cache.addToCache(concreteInput, concreteOutput);
        return concreteOutput;
    }


    private OutputAction sendInputToCache(InputAction concreteInput, Trace trace) {
        OutputAction concreteOutput = null;
        CacheEntry entry = cache.next(concreteInput);
        if (entry != null) {
           concreteOutput = entry.getOutput();
           // Inputs having cache hits are stored in list. When an input cannot be found in cache,
           // a cache miss, the inputs in the list  are used to bring the sut at the same position
           // in the trace as the cache so that we can run the input on the sut itself.
           cacheHittingInputs.add(concreteInput);
        }
        return concreteOutput;
    }

    @Override
    public OutputAction sendInput(InputAction concreteInput, Trace trace) {

        OutputAction concreteOutput = null;

       /* The CachedTraceRunner allows exterior modifications to the cache while running a trace.
        * So it may happen that the CachedTraceRunner hits a cache miss but later on can use
        * the cache again because an exterior modification to the cache happened.
        * So the CachedTraceRunner always checks the cache first, even when it previously
        * had a cache miss before.
        */
        concreteOutput=sendInputToCache(concreteInput,trace);
        if ( concreteOutput == null) {
            // Cache miss happened,
            // rerun input but now on real sut instead of using cache
            // note: sendInputToSut updates the sut first to current position in trace before running the new input
            concreteOutput=sendInputToSut(concreteInput,trace);
        }
        inputNum++;

        return concreteOutput;
    }

    // Set the sut to the current position of the cache's cursor!
    private void updateSutToCurrentCachePosition(List<InputAction> cacheHittingInputs) {
        for (InputAction input : cacheHittingInputs) {
            sut.sendInput(input);
        }
    }

    public void sendReset() {
        // reset sut
        sut.sendReset();
        // reset used cache
        cache.reset();
        // cleanup stored cach hitting inputs
        cacheHittingInputs.clear();
        // reset input count
        inputNum = 0;
    }

}
