package abslearning.tree;

//TODO ? This might still be useful if we want to refer to parameters found at specific positions
public class RefVector   {
	private String methodName;
	private int paramIndex;
	private int value;
	private int smallId;
	private int largeId;
	
	public RefVector(String methodName, int paramIndex, int value, int smallId,
			int largeId) {
		super();
		this.methodName = methodName;
		this.paramIndex = paramIndex;
		this.value = value;
		this.smallId = smallId;
		this.largeId = largeId;
	}	
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public String getKey() {
		return methodName + "_p" + paramIndex;
	}	
	
	public String getMethodName() {
		return methodName;
	}
	public void setParamIndex(int paramIndex) {
		this.paramIndex = paramIndex;
	}
	public int getParamIndex() {
		return paramIndex;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getValue() {
		return value;
	}
	public void setSmallId(int smallId) {
		this.smallId = smallId;
	}
	public int getSmallId() {
		return smallId;
	}
	public void setLargeId(int largeId) {
		this.largeId = largeId;
	}
	public int getLargeId() {
		return largeId;
	}

	@Override
	public String toString() {
		return "RefVector [methodName=" + methodName + ", paramIndex="
				+ paramIndex + ", value=" + value + ", smallId=" + smallId
				+ ", largeId=" + largeId + "]";
	}		
	
}
