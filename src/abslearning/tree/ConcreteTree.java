package abslearning.tree;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.Logger;

import abslearning.logger.StructLogger;
import abslearning.tree.action.InputAction;
import abslearning.tree.action.OutputAction;
import abslearning.tree.action.Parameter;
import generictree.GenericTree;
import generictree.GenericTreeNode;

/*
 * ConcreteTree
 * 
 * - tree constructed by combining all trace interactions with sut
 * - the tree class has all kind of utility methods to retrieve info from the tree
 * 
 * TODO * Make 2 classes of TreeNodeData, one only with input/output, another with i/o and memV/version. 
 * Currently we have only one type of node data, comprising input/output/memV/version. It can
 * be argued that caching is done at two levels. At a lower level, you cache only i/o, at a higher 
 * level you also cache memV/version. Some Sut Runners only use the lower level cache, while
 * others use the higher level cache.
 * 
 */
public class ConcreteTree implements TreeInterface{

    private static final Logger logger = Logger.getLogger(ConcreteTree.class);
	
	private GenericTree<TreeNodeData> concreteTree;

	private int nodeId;
	private int version;
	
	public void increaseVersion() {
		logger.fatal( "\n\n   INCREASE VERSION of tree!! \n\n");
		version=version+1;
	}
	
	public int  getVersion() {
		 return version;
	}	 
	


	
	public ConcreteTree(List<Integer> constants) {
	    // create concrete tree
		concreteTree = new GenericTree<TreeNodeData>();
	    nodeId=0;
	    version=1; 
	    
	    // create constantsAction 
		ArrayList<Parameter> constantsParamList = new ArrayList<Parameter>();		
		for (int i = 0; i < constants.size(); i++) {
			constantsParamList.add(new Parameter(constants.get(i), i));
		}	    
	    InputAction constantsAction = new InputAction(abslearning.trace.action.Action.CONSTANT_ACTION_NAME, constantsParamList);
	    
	    // in rootNode set as data the constantsAction 
	    GenericTreeNode<TreeNodeData> root = new GenericTreeNode <TreeNodeData>( new TreeNodeData( nodeId,  constantsAction,  new OutputAction("root", new ArrayList<Parameter>())));
	    root.getData().setVersion(version);
	    concreteTree.setRoot(root);
	    
	    nodeId++;
	}
	
	public GenericTreeNode<TreeNodeData> getRoot() {
        return 	concreteTree.getRoot();	
	}	

	public GenericTree<TreeNodeData> getTree() {
		return concreteTree;
	}
		
	/**
	 * Adds a new child the current node, initializing its data with the given input/output actions. 
	 * The child is considered to be empty, because its version or memorable have not been set yet. 
	 */
	
	public GenericTreeNode<TreeNodeData> addEmptyConcreteTreeNode(GenericTreeNode<TreeNodeData> currentNode,InputAction concreteInput,OutputAction concreteOutput) {		
		// create new node 
		GenericTreeNode<TreeNodeData> childNode;
		childNode=new GenericTreeNode <TreeNodeData>(new TreeNodeData(nodeId,concreteInput,concreteOutput));
		//add new node as child of currentNode to the tree
		currentNode.addChild(childNode);
		// increment node id 
		nodeId=nodeId+1;

		return childNode; 
	}	
	
	/**
	 * Updates the node data's {@code memV} to the given list. Also, updates the data's version to the tree's
	 * current version. 
	 */
	public void updateConcreteTreeNode(GenericTreeNode<TreeNodeData> currentNode, LinkedHashSet<Integer> memV) {
		TreeNodeData nodeData = currentNode.getData();
		nodeData.setMemV(memV);
		nodeData.setVersion(this.version);
	}
	
	
	public GenericTreeNode<TreeNodeData> getExistingConcreteTreeNode(GenericTreeNode<TreeNodeData> currentNode,InputAction concreteInput) {
        // search among children of currentNode whether they represent given concrete input
		for(GenericTreeNode<TreeNodeData> child : currentNode.getChildren()){
	   		 if (child.getData().getInput().equalsMethodParams(concreteInput)   ) {
	   			 // concrete input found as child
	   			 return child;
			 } 
		 }
	     // concrete input not as child found 
	   	 return null;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------	
	//  debug helpers
	//-------------------------------------------------------------------------------------------------------------------------------	
	   
	public void printSutConcreteTreeInfo(){
		StructLogger.printSutConcreteTreeInfo(  this, abslearning.app.Config.currentOutputDir + "/concreteTree_when_createInputAbstraction.dot" );
	}	
}
