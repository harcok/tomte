package abslearning.tree;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;

import org.apache.log4j.Logger;

import abslearning.app.Config;
import abslearning.app.Statistics;

import util.Tuple2;

public class TreeLoader {
	
	Logger logger = Logger.getLogger(TreeLoader.class);
	private Statistics statistics;
	private List<Integer> constants;
	
	public TreeLoader(Statistics statistics, List<Integer> constants) {
		this.statistics = statistics;
		this.constants = constants;
	}
	
	public ConcreteTree loadTreeAndUpdateStats(String fileName) {
		ConcreteTree newTree = new ConcreteTree(constants);
		if (!new File(fileName).exists()) {
			logger.fatal("File " + fileName + " not found, returning empty new tree ");
			return newTree;
		}
		try(
				InputStream file = new FileInputStream(fileName);
				InputStream buffer = new BufferedInputStream(file);
				ObjectInput input = new ObjectInputStream (buffer);
				) {
			logger.fatal("Loading cache with tree and partial stats from " + fileName);
			@SuppressWarnings("unchecked")
			Tuple2<SerializableCache, StatisticsObject> fromDump = (Tuple2<SerializableCache, StatisticsObject>) input.readObject();
			fromDump.tuple0.dumpCache( new TreeCacheIterator(constants, newTree));
			fromDump.tuple1.updateStatistics(statistics);
			return newTree;
		}
		catch(ClassNotFoundException ex) {
			System.err.println("Cache file corrupt " + ex.getMessage());
			return null;
		}
		catch(IOException ex) {
			System.err.println("Could not read cache file " + ex.getMessage());
			return null;
		}
	}
	
	
	/** Along with the concrete tree, statistics are also cached. These contain data essential 
	 * for benchmarking like number of inputs, resets, membership and equivalence queries. 
	 * Tests are not cached, thus are not included. Since we use a constant seed, the tests run 
	 * from an execution to the next shouldn't change.
	 */
	static class StatisticsObject implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -5049357565672534200L;
		private Boolean learningSuccessful;
		private long testInputs;
		private long testResets;
		private long ceAnalysisInputs;
		private long ceAnalysisResets;
		private long learnInputs;
		private long learnResets;

		public StatisticsObject(Statistics statistics) {
		}
		
		void updateStatistics(Statistics statistics) {
		    statistics.startNewRun(true);
		    statistics.addDescriptionRun("Mock run to fill cached stats");
		    statistics.startLearning();
			updateStatIncrementally(statistics, stat -> stat.incSutInput(), this.learnInputs);
			updateStatIncrementally(statistics, stat -> stat.incSutReset(), this.learnResets);
			statistics.stopLearning();
			statistics.startTesting();
			updateStatIncrementally(statistics, stat -> stat.incSutInput(), this.testInputs);
            updateStatIncrementally(statistics, stat -> stat.incSutReset(), this.testResets);
			statistics.stopTesting();
			statistics.startAnalysisCounterexample();
			updateStatIncrementally(statistics, stat -> stat.incSutInput(), this.ceAnalysisInputs);
            updateStatIncrementally(statistics, stat -> stat.incSutReset(), this.ceAnalysisResets);
            statistics.stopAnalysisCounterexample();
            
			statistics.learningSuccessful = this.learningSuccessful;
		}
		
		private void updateStatIncrementally(Statistics statistics, Consumer<Statistics> consumer, long numberOfTimes) {
			for (long i=0; i < numberOfTimes; i ++) {
					consumer.accept(statistics);
			} 
		}
	}		
}
