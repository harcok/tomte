package abslearning.tree;

import java.util.LinkedHashSet;
import java.util.List;

import abslearning.trace.Trace;
import abslearning.trace.action.InputAction;
import abslearning.trace.action.OutputAction;

/**
 * An interface for a cache iterator universally used to access/update cache. 
 * For every caching mechanism implemented there should be an implementation of this iterator.
 * The general interpretation of a cache is that of a tree with inputs on edges and inputs, outputs and memorable value sets on nodes.
 */
/* Example structure:
 * ( {null,[]}
 *  register(10) (
 *   {ok, [10]}
 *   login(10) (
 *    {ok, [10]}
 *    null)
 *   login(20) (
 *    {nok, [10]}
 *    null)
 *   )
 *  logout() (
 *   {nok, []}
 *   null)
 * )
 * 
 */
public interface CacheIterator {
	
	/**
	 * The iterator checks if an entry for the input exists and if so, moves on the given input. If it doesn't,
	 * the iterator retains its previous position.
	 * @return the corresponding entry in the cache or {@code null} if no entry existed
	 */
	public CacheEntry next(InputAction input);
	
	/**
	 *  @return all the children of the current node.
	 */
	public List<InputAction> next();
	
	public boolean hasNext(InputAction input);
	
	/**
	 * The iterator adds an entry with only an output for the input and moves to this entry. 
	 */
	public void addToCache(InputAction input, OutputAction output);
	
	public Trace getTraceSoFar();
	
	/**
	 *
	 * The iterator updates the entry for the current position input. 
	 * Only the memorable value set can be changed or the version of the cache. 
	 * The updated memVs MUST contain the memVs previously stored. 
	 *  </br> </br>
	 * Adding new memVs, might make the cache inconsistent, in which case an 
	 * Inconsistency object is returned:
	 * 		(1) the trace exposing this inconsistency
	 * 		(2) the full trace (including the inconsistency trace)
	 * 		(3) the inconsistent value
	 *  </br> </br>
	 * A cache is consistent if all memorable values and output values in a node 
	 * originate from constants, the node's input or the parent node's memorable values.
	 */
	public Inconsistency updateCache(LinkedHashSet<Integer> memV);
	
	/**
	 * @return the current position in the cache
	 */ 
	public CacheEntry current();
	
	/**
	 * Returns the previous entry and moves the cursor to the previous position.
	 * @return the previous position in the cache
	 */
	public CacheEntry previous();
	
	/**
	 * The iterator resets its position to the root position.
	 */
	public void reset();
	
	/**
	 * Prompts a reset, as well as an increase in version. All newly built cached entries will have the increased version.  
	 */
	public void increaseVersion();
	
	/**
	 * @return the version of the tree
	 */
	public int getVersion();
}
