package util.learnlib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

import abslearning.exceptions.BugException;
import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.interfaces.Word;
import de.ls5.jlearn.shared.AlphabetImpl;
import de.ls5.jlearn.shared.AutomatonImpl;
import de.ls5.jlearn.shared.SymbolImpl;
import de.ls5.jlearn.shared.WordImpl;
import net.automatalib.automata.transout.MealyMachine;

public class AutomatonUtil {
	public static Logger logger = Logger.getLogger(AutomatonUtil.class);
	
	static public Word copyWord(Word in) {
		Word out = new WordImpl();
		for (Symbol s : in.getSymbolList()) {
			out.addSymbol(s);
		}
		return out;
	}

	public static List<State> getStatesInBFSOrder(Automaton automaton) {
		List<Symbol> inputs = automaton.getAlphabet().getSymbolList();
		java.util.Collections.sort(inputs);

		Queue<State> statestovisit = new LinkedList<State>();
		List<State> result = new ArrayList<State>();
		HashSet<State> states = new HashSet<State>(); // to check if state is
														// not seen already by
														// other transition

		statestovisit.offer(automaton.getStart());
		result.add(automaton.getStart());
		states.add(automaton.getStart());

		State current = (State) statestovisit.poll();
		while (current != null) {
			for (Symbol input : inputs) {
				State s = current.getTransitionState(input);
				if ((s != null) && (!states.contains(s))) {
					statestovisit.offer(s);
					result.add(s);
					states.add(s);
				}
			}

			if (statestovisit.isEmpty()) {
				break;
			}
			current = (State) statestovisit.poll();
		}

		return result;
	}
	
	/**
	 * Gives a random length path from the start state to the given state. We build this path based 
	 * on the random value generator. 
	 */
	public static List<Symbol> traceToState(Automaton automaton, State state, Random random) {
		List<Symbol> inputs = automaton.getAlphabet().getSymbolList();
		java.util.Collections.sort(inputs);
		List<State> states = getStatesInBFSOrder(automaton);
		List<Symbol> traceToState = new ArrayList<Symbol>();
		
		State nextState = state;
		while (nextState != automaton.getStart()) {
			// reverse-iterate and collect all states that are successors to nextState
			List<State> nextStates = new ArrayList<State>();
			int stateIndex = states.indexOf(nextState);
			for (int i = stateIndex-1; i >= 0; i --) {
				if ( Arrays.asList(states.get(i).getSuccessorStates()).contains(nextState)) {
					nextStates.add(states.get(i));
				}
			}
			
			// pick one of these states using the random number generator to be nextState
			// because of reverse-iteration on a BFS ordering, eventually nextState will be the initial state.
			State previousState = nextState;
			nextState = nextStates.get(random.nextInt(nextStates.size()));
			
			// obtain the input symbol connecting the two states and add it to the trace
			for (Symbol input : inputs) {
				if (nextState.getTransitionState(input) == previousState) {
					traceToState.add(0, input);
					break;
				}
			}
		} 
		
		return traceToState;
	}
	
	/**
	 * Returns a state from the automaton randomly in accordance to the seed of the Random generator.
	 */
	public static State pickRandomState(Automaton automaton, Random random) {
		List<State> autStates = automaton.getAllStates();
		int randomId = random.nextInt(autStates.size());
		State randomState = null;
		for ( State state : autStates) {
			if (state.getId() == randomId ) {
				randomState = state;
				break;
			}
		}
	
		if (randomState == null) {
			throw new BugException("Could not find a state with an id between 0 and " + autStates.size());
		}
		return randomState;
	}
	
	

	public static boolean hasDuplicateStates(Automaton aut) {
		for (State state : aut.getAllStates()) {
			if (aut.getOtherStateWithSameSignature(state) != null)
				return true;
		}
		return false;
	}
	
	public static final Symbol UNDEF = new SymbolImpl("Oundefined");
	
	public static boolean isDefined(Symbol output) {
		return output != null && !UNDEF.equals(output);
	}
	
	public static Automaton getCopy(Automaton aut) {
		return getCopyWithRestrictedAlphabet(aut, aut.getAlphabet(), new HashSet<String>());
	}
	
	public static Automaton getCopyWithRestrictedAlphabet(Automaton aut,
			Alphabet alpha, Set<String> removeStateInputPairs) {
		
		// get new empty automaton with copy of alphabet
		// note: automaton already has start start
		Automaton copy = getEmptyCopyWithReducedAlphabet( aut);
		
		// create new states in copy (one for each in original)
		// return map which maps between original and its resp. new state
		Map<State, State> stateMap = copyStates(aut, copy);

		// for each orig state copy its transitions to its resp. copy state
		copyTransitionsDefinedAndInStateMap( stateMap, removeStateInputPairs);

		return copy;
	}
	

	

	public static Automaton getCopyWithRemappedOutputAlphabet(Automaton aut,
			Map<Symbol, Symbol> outputSymbolReMap) {

		Alphabet alphabet = aut.getAlphabet();

		Map<State, State> stateMap = new HashMap<State, State>();
		Automaton copy = new AutomatonImpl(alphabet);

		List<State> origstates = AutomatonUtil.getStatesInBFSOrder(aut);

		for (int i = 0; i < origstates.size(); ++i) {
			State orig = (State) origstates.get(i);
			State newstate = null;
			if (i == 0) {
				newstate = copy.getStart();
			} else {
				newstate = copy.addNewState();
			}
			stateMap.put(orig, newstate);

		}

		for (State orig : origstates) {
			for (int i = 0; i < alphabet.size(); ++i) {
				Symbol sym = alphabet.getSymbolByIndex(i);
				Symbol output = orig.getTransitionOutput(sym);
				State transsate = orig.getTransitionState(sym);
				State newstate = (State) stateMap.get(orig);
				State newtrans = (State) stateMap.get(transsate);
				if ((output != null) && (newtrans != null)) {

					Symbol remappedOutput = outputSymbolReMap.get(output);
					newstate.setTransition(sym, newtrans, remappedOutput);
				}
				// newstate.setOutput(orig.getOutput()); MOORE machines have
				// output in state itself! Mealy in transitions!
			}
		}

		return copy;
	}
	


	/**
	 *   get copy of alphabet of the alphabet in the automaton
	 *   
	 * @param aut
	 * @return
	 */
	private static Alphabet getReducedAlphabet( Automaton aut) {
		HashSet<Symbol> alphabetSet = new HashSet<Symbol>();
		Alphabet newAlphabet = new AlphabetImpl();
		
	    for (int i = 0; i < aut.getAlphabet().size(); ++i) {
	    	alphabetSet.add(aut.getAlphabet().getSymbolByIndex(i));
	    }
		
		for (int i = 0; i < aut.getAlphabet().size(); ++i) {
			Symbol sym = aut.getAlphabet().getSymbolByIndex(i);
			if (alphabetSet.contains(sym)) {
				newAlphabet.addSymbol(sym);
			}
		}
		
		return newAlphabet;
	}
	
	private static Automaton getEmptyCopyWithReducedAlphabet( Automaton aut) {
		Alphabet newalphabet = getReducedAlphabet(aut);
		Automaton copy = new AutomatonImpl(newalphabet);
		return copy; 
	}
	
	/** for each state in source automaton creates new state
	 *  note: only start state in copy is kept (was already there)
	 * 
	 * @param aut
	 * @param copy
	 * 
	 * @return map which maps source state to dest state
	 */
	private static  Map<State, State> copyStates( Automaton aut, Automaton copy) {
		Map<State, State> stateMap = new HashMap<State, State>();
		List<State> origstates = AutomatonUtil.getStatesInBFSOrder(aut);

		for (int i = 0; i < origstates.size(); ++i) {
			State orig = (State) origstates.get(i);
			State newstate = null;
			if (i == 0) {
				newstate = copy.getStart();
			} else {
				newstate = copy.addNewState();
			}
			stateMap.put(orig, newstate);
		}
		
		return stateMap;
	} 
	
	public static void makeAutomatonInputCompleteByAddingSelfLoops(Automaton copy) {
		Alphabet alpha = copy.getAlphabet();
		for (State state : copy.getAllStates()) {
			for (Symbol sym : alpha.getSymbolList()) {
				if (state.getTransitionState(sym) == null) {
					state.setTransition(sym, state, AutomatonUtil.UNDEF);
				}
 			}
		}
	}

	
	/** for each orig state copy its transitions to its resp. copy state
	 * 
	 * @param stateMap
	 * @param removeStateInputPairs
	 */
	private static void copyTransitionsDefinedAndInStateMap(
			Map<State, State> stateMap, Set<String> removeStateInputPairs) {
		
		// per original state
		for (State orig : stateMap.keySet()) {
			
			// walk over input symbols in orig state
			for (Symbol sym : orig.getInputSymbols()) {

				// get output for input in orig state 
				Symbol output = orig.getTransitionOutput(sym);
				if (!AutomatonUtil.isDefined(output))
					continue;
				
                // get destination for input in orig state 
				State origDestState = orig.getTransitionState(sym);
				
				
				State srcState = (State) stateMap.get(orig);
				State dstState = (State) stateMap.get(origDestState);

				if ((output != null) && (dstState != null)) {
					if ((removeStateInputPairs != null)
							&& removeStateInputPairs.contains(orig.getId()
									+ "_" + sym.toString())) {
						logger.debug("state:" + srcState.getId() + " hide sym:"
								+ sym);
						continue;
					}
                    
					// for mealy machine : add transition in copy states
					srcState.setTransition(sym, dstState, output);
				} /*
				 * else { logger.debug("STRANGE UNDEF TRANSITION");
				 * 
				 * }
				 */
				
				// for moore machine : add output to copy state
				srcState.setOutput(orig.getOutput());
			}
		}
	}
	
	
	
	private static  <S,T>  Map<S, State> copyStatesFromNewLearlibHyp( MealyMachine<S, Symbol, T, Symbol> newlearner_abstractHyp, Automaton copy, Alphabet inputAlphabet) {
		Map<S, State> stateMap = new HashMap<S, State>();
		S initial=newlearner_abstractHyp.getInitialState();
		for (  S state : newlearner_abstractHyp.getStates() ) {
			State newstate = null;
			if (state.equals(initial)) {
				newstate = copy.getStart();
			} else {
				newstate = copy.addNewState();
			}
			stateMap.put(state, newstate);
		}
		return stateMap;
	} 
	
	
	
	/** for each orig state copy its transitions to its resp. copy state
	 * 
	 * @param stateMap
	 * @param removeStateInputPairs
	 */
	private static <S,T> void copyTransitionsFromNewLearnlibToOldLearnlib(
			Map<S, State> stateMap,MealyMachine<S, Symbol, T, Symbol> newlearner_abstractHyp, Alphabet inputAlphabet) {
		
		// per original state
		for (S origState : stateMap.keySet()) {
			
			// walk over input symbols in orig state
			//for (Symbol sym : orig.getInputSymbols()) {
			
			// walk over input alphabet
			for ( Symbol input : inputAlphabet.getSymbolList()) {

				// get transition for input
				T transition=newlearner_abstractHyp.getTransition(origState, input);
				
				// get destination state of transition
				S destState=newlearner_abstractHyp.getSuccessor(transition);
				// get output of transition
				Symbol output = newlearner_abstractHyp.getTransitionOutput(transition);

				
				State srcState = (State) stateMap.get(origState);
				State dstState = (State) stateMap.get(destState);

				if ((output != null) && (dstState != null)) {
                    
					// for mealy machine : add transition in copy states
					srcState.setTransition(input, dstState, output);
				}
			}
		}
	}
	
	// <S,I,T,O>
	public static <S,T> Automaton convertHyp(MealyMachine<S, Symbol, T, Symbol> newlearner_abstractHyp, Alphabet inputAlphabet) {
		AutomatonImpl automaton= new AutomatonImpl(inputAlphabet);
		
		Map<S, State> stateMap =copyStatesFromNewLearlibHyp(newlearner_abstractHyp,automaton, inputAlphabet);
		
		// for each orig state copy its transitions to its resp. copy state
		copyTransitionsFromNewLearnlibToOldLearnlib(stateMap,newlearner_abstractHyp,inputAlphabet);
		
		return automaton;
	}
	
}
