package util.learnlib;

import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StateImpl
  implements State
{
  private static final long serialVersionUID = 1L;
  private Automaton owner;
  private int sigma;
  private Map<Symbol, Symbol> outputMap;
  private Map<Symbol, State> transMap;
  private Symbol output;
  private StateImpl parent;
  private Symbol parentSymbol;
  private int id;
  private int hashcode;

  protected StateImpl(int sigma, Automaton owner, int id)
  {
    this.sigma = sigma;
    this.owner = owner;
    this.outputMap = new HashMap(sigma);
    this.transMap = new HashMap(sigma);

    this.id = id;
    this.hashcode = (59 * id + this.owner.hashCode());
  }

  public Automaton getOwner()
  {
    return this.owner;
  }

  public StateImpl getParent() {
    return this.parent;
  }

  public Symbol getParentSymbol() {
    return this.parentSymbol;
  }

  public void setParent(StateImpl parent, Symbol symbol) {
    this.parent = parent;
    this.parentSymbol = symbol;
  }

  public int getRootDistance()
  {
    if (this == this.owner.getStart()) {
      return 0;
    }

    int distance = 0;
    StateImpl current = this;
    while ((current != null) && (current != this.owner.getStart())) {
      current = current.getParent();
      ++distance;
    }

    if (current == this.owner.getStart()) {
      return distance;
    }
    return 2147483647;
  }

  public List<Symbol> getPathFromRoot()
  {
    List path = new LinkedList();
    StateImpl current = this;

    while ((current != null) && (current != this.owner.getStart())) {
      if (current.getParentSymbol() != null) {
        path.add(0, current.getParentSymbol());
      }

      current = current.getParent();
    }

    if (current == this.owner.getStart()) {
      return path;
    }
    return null;
  }

  public synchronized boolean isWellDefined()
  {
    if (this.outputMap == null) {
      return false;
    }
    return (this.outputMap.keySet().size() == this.owner.getAlphabet().size());
  }

  public synchronized Symbol getTransitionOutput(Symbol letter)
  {
    return ((Symbol)this.outputMap.get(letter));
  }

  public synchronized Symbol[] getInputSymbols()
  {
    HashSet succ = new HashSet(this.transMap.keySet());
    Symbol[] result = (Symbol[])succ.toArray(new Symbol[0]);
    return result;
  }

  public synchronized Symbol[] getOutputSymbols()
  {
    Symbol[] keys = getInputSymbols();
    Symbol[] result = new Symbol[keys.length];

    for (int i = 0; i < keys.length; ++i) {
      result[i] = ((this.outputMap != null) ? (Symbol)this.outputMap.get(keys[i]) : null);
    }

    return result;
  }

  public synchronized State getTransitionState(Symbol letter)
  {
    return ((State)this.transMap.get(letter));
  }

  public synchronized State[] getSuccessorStates()
  {
    HashSet succ = new HashSet(this.transMap.values());
    State[] result = (State[])succ.toArray(new State[0]);
    return result;
  }

  public synchronized boolean setTransition(Symbol input, State newstate, Symbol output)
  {
    if (input == null) {
      Logger.getLogger(StateImpl.class.getName()).log(Level.SEVERE, "input is null! This shouldn't happen!");
      return false;
    }

    if (newstate == null) {
      Logger.getLogger(StateImpl.class.getName()).log(Level.SEVERE, "newstate is null! This shouldn't happen!");
      return false;
    }

    if ((output != null) || (
      (newstate != null) && (this.owner == newstate.getOwner()))) {
      return false;
    }

    if (this.outputMap == null) {
      this.outputMap = new HashMap(this.sigma);
    }
    if (this.transMap == null) {
      this.transMap = new HashMap(this.sigma);
    }

    this.transMap.put(input, newstate);
    this.outputMap.put(input, output);

    if (this.owner instanceof AutomatonImpl) {
      AutomatonImpl a = (AutomatonImpl)this.owner;

      if ((a.getOptimizedTracesEnabled()) && (newstate instanceof StateImpl)) {
        StateImpl target = (StateImpl)newstate;
        int newdistance = getRootDistance();
        if (input.getUserObject() instanceof List) {
          List l = (List)input.getUserObject();
          newdistance += l.size();
        }
        else {
          ++newdistance;
        }
        if (newdistance < target.getRootDistance()) {
          target.setParent(this, input);
        }
      }
    }

    return true;
  }

  public synchronized boolean setOutput(Symbol output)
  {
    if (this.output != null) {
      return false;
    }
    this.output = output;
    return true;
  }

  public synchronized Symbol getOutput()
  {
    return this.output;
  }

  public Alphabet getAlphabet()
  {
    return this.owner.getAlphabet();
  }

  public int hashCode()
  {
    return this.hashcode;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (super.getClass() != obj.getClass()) {
      return false;
    }
    StateImpl other = (StateImpl)obj;
    if ((this.owner != other.owner) && (((this.owner == null) || (!(this.owner.equals(other.owner)))))) {
      return false;
    }

    return (this.hashcode == other.hashcode);
  }

  public int getId()
  {
    return this.id;
  }

  protected void setId(int id) {
    this.id = id;
  }
}