package util.learnlib;

import de.ls5.jlearn.interfaces.Alphabet;
import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.interfaces.Word;
import de.ls5.jlearn.shared.AlphabetImpl;
import de.ls5.jlearn.shared.WordImpl;
import de.ls5.jlearn.util.AutomatonUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AutomatonImpl
  implements Automaton
{
	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutomatonImpl.class);
	
	private static final long serialVersionUID = 1L;
  private State start;
  private Alphabet alphabet;
  private boolean optimizedTraces = false;

  private int ids = 0;

  public AutomatonImpl(Alphabet alphabet)
  {
    this.alphabet = alphabet;
    this.start = new StateImpl(alphabet.size(), this, this.ids++);
  }

  public AutomatonImpl(Alphabet alphabet, boolean optimizedTraces)
  {
    this.alphabet = alphabet;
    StateImpl startstate = new StateImpl(alphabet.size(), this, this.ids++);
    this.start = startstate;
    this.optimizedTraces = optimizedTraces;

    if (!(optimizedTraces))
      return;
    startstate.setParent(null, null);
  }

  public boolean getOptimizedTracesEnabled()
  {
    return this.optimizedTraces;
  }

  public State getStart()
  {
    return this.start;
  }

  public Alphabet getAlphabet()
  {
    return this.alphabet;
  }

  public Alphabet getOutputAlphabet()
  {
    Set seenSymbols = new HashSet();
    Alphabet outputAlpha = new AlphabetImpl();

    for (State s : getAllStates()) {
      for (int i = 0; i < this.alphabet.size(); ++i) {
        Symbol sym = this.alphabet.getSymbolByIndex(i);
        Symbol output = s.getTransitionOutput(sym);
        if (!(seenSymbols.contains(output))) {
          outputAlpha.addSymbol(output);
          seenSymbols.add(output);
        }
      }
    }

    return outputAlpha;
  }

  public State addNewState()
  {
    State s = new StateImpl(this.alphabet.size(), this, this.ids++);
    logger.fatal("State id: " + s.getId());
    return s;
  }

  public boolean isWellDefined()
  {
    for (State s : getAllStates()) {
      if (!(s.isWellDefined())) {
        return false;
      }
    }
    return true;
  }

  public List<State> getAllStates()
  {
    return AutomatonUtil.getStatesInBFSOrder(this);
  }

  public List<State> getIncompleteStates()
  {
    LinkedList result = new LinkedList();
    for (State s : getAllStates()) {
      if (!(s.isWellDefined())) {
        result.add(s);
      }
    }
    return result;
  }

  public State getOtherStateWithSameSignature(State s)
  {
    List<State> states = getAllStates();
    for (State otherstate : states) {
      if (otherstate != s) {
        boolean equals = true;
        for (int i = 0; i < this.alphabet.size(); ++i) {
          Symbol sym = this.alphabet.getSymbolByIndex(i);
          Symbol out1 = s.getTransitionOutput(sym);
          Symbol out2 = otherstate.getTransitionOutput(sym);
          equals = (equals) && (out1.equals(out2));
          if (!(equals)) {
            break;
          }
        }
        if (equals) {
          return otherstate;
        }
      }
    }
    return null;
  }

  public Automaton getCopyWithRestrictedAlphabet(Alphabet alpha)
  {
    return AutomatonUtil.getCopyWithRestrictedAlphabet(this, alpha);
  }

  public Word getTraceToState(State destination)
  {
    if ((this.optimizedTraces) && (destination instanceof StateImpl)) {
      Word optimResult = null;
      try {
        optimResult = getTraceToStateOptimized((StateImpl)destination);
      } catch (Exception e) {
        Logger.getLogger(super.getClass().getName()).log(Level.SEVERE, e.toString());
      }
      if ((optimResult == null) || (getTraceState(optimResult, optimResult.size()) != destination))
        Logger.getLogger(super.getClass().getName()).log(Level.WARNING, "optimized getTraceToState gave wrong result, falling back to Dijkstra");
      else {
        return optimResult;
      }
    }

    Map costs = new HashMap();
    Map parent = new HashMap();
    Map<State, Symbol> transSym = new HashMap<State, Symbol>();
    List<State> queue = new ArrayList<State>();

    List<State> states = getAllStates();
    for (int i = 0; i < states.size(); ++i) {
      costs.put(states.get(i), Integer.valueOf(2147483647));
      queue.add(states.get(i));
    }
    costs.put(this.start, Integer.valueOf(0));

    boolean search = true;
    while ((!(queue.isEmpty())) && (search))
    {
      Integer cost = Integer.valueOf(2147483647);
      State current = null;
      for (State s : queue) {
        Integer statecost = (Integer)costs.get(s);
        if (statecost.intValue() < cost.intValue()) {
          cost = statecost;
          current = s;
        }

      }

      if (current == null) {
        break;
      }
      queue.remove(current);

      for (int i = 0; i < this.alphabet.size(); ++i) {
        Symbol sym = this.alphabet.getSymbolByIndex(i);
        State succ = current.getTransitionState(sym);
        Integer newcost = Integer.valueOf(cost.intValue() + 1);
        Integer oldcost = (Integer)costs.get(succ);

        if (oldcost == null) {
          oldcost = Integer.valueOf(2147483647);
        }

        if (newcost.intValue() < oldcost.intValue()) {
          costs.put(succ, newcost);
          parent.put(succ, current);
          transSym.put(succ, sym);

          if (succ == destination) {
            search = false;
            break;
          }
        }
      }

    }

    List<Symbol> symTrace = new LinkedList();

    State current = destination;
    while (current != this.start) {
      symTrace.add(0, transSym.get(current));
      current = (State)parent.get(current);
    }

    Word result = new WordImpl();
    for (Symbol sym : symTrace) {
      result.addSymbol(sym);
    }

    return result;
  }

  public Word getTraceToStateOptimized(StateImpl destination)
  {
    List<Symbol> path = destination.getPathFromRoot();

    if (path == null) {
      return null;
    }

    Word result = new WordImpl();

    for (Symbol sym : path) {
      result.addSymbol(sym);
    }

    return result;
  }

  public Word getTraceOutput(Word trace)
  {
    State current = this.start;
    Symbol[] as = trace.getSymbolArray();
    Symbol[] result = new Symbol[trace.size()];

    for (int i = 0; i < as.length; ++i)
    {
      result[i] = current.getTransitionOutput(as[i]);
      current = current.getTransitionState(as[i]);
    }

    return new WordImpl(result);
  }

  public State getTraceState(Word trace, int steps)
  {
    State current = this.start;
    int stepcount = 0;

    for (int i = 0; i < trace.size(); ++i) {
      if (stepcount++ >= steps) {
        return current;
      }
      Symbol letter = trace.getSymbolByIndex(i);
      current = current.getTransitionState(letter);
    }

    return current;
  }

  public void reassignStateIds()
  {
    this.ids = 0;
    for (State s : AutomatonUtil.getStatesInBFSOrder(this)) {
      StateImpl si = (StateImpl)s;
      si.setId(this.ids++);
    }
  }
}