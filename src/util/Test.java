package util;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONValue;
import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.manipulation.Filter;

import java.util.ArrayList;
import java.util.List;



public class Test {
	

	/*
	 *  src: http://planetjava.org/java-junit-user/2007-05/msg00011.html
	 *  
	 *  usage:
	 *      JUnitCore core = new JUnitCore();
	 *      Request testRequest = Request.aClass(aTestClass);
	 *      if (0 < someArguments.length) {
	 *         Filter matchFilter = new MatchingFilter(aTestClass, someArguments);
	 *         testRequest = testRequest.filterWith(matchFilter);
	 *      }
	 *      Result result=core.run(testRequest);
	 */


	/**
	 * This class provides a mechanism to filter tests by name, allowing a subset of the tests in a test
	 * class to be defined by a developer.
	 * 
	 * @since 2.0
	 */
	 private static class TestMatchingFilter extends Filter {

	  /**
	   * The references to the tests that should be executed.
	   */
	  private List<Description> tests = new ArrayList<Description>();

	  /**
	   * Initializes an instance of <code>MatchingFilter</code> with the data provided.
	   * 
	   * @param aTestClass
	   *          the class for which tests will be executed
	   * @param someTestNames
	   *          the method names
	   */
	  public TestMatchingFilter(Class aTestClass, String[] someTestNames) {
	    for (String testName : someTestNames) {
	      this.tests.add(Description.createTestDescription(aTestClass,testName));
	    }
	  }

	  /**
	   * Implemented abstract method to provide detail filtering mechanism. Only test methods that match
	   * the provided names will be executed.
	   * 
	   * @see org.junit.runner.manipulation.Filter#shouldRun(org.junit.runner.Description)
	   */
	  @Override
	  public boolean shouldRun(Description aDescription) {
	    return this.tests.contains(aDescription);
	  }

	  /**
	   * Implemented abstract method.
	   * 
	   * @see org.junit.runner.manipulation.Filter#describe()
	   */
	  @Override
	  public String describe() {
	    return "This filter excludes any test methods that do not exactly match the provided names";
	  }

	}
	
	
	private static class RunJunitTestRunListener extends RunListener {
        private long runStart = 0L;
        private long testStart = 0L;

        @Override
        public void testRunStarted(Description description) throws Exception {
            System.err.println("runStarted");
            runStart = System.currentTimeMillis();
            methodTimes=new LinkedHashMap<String,Long> ();
            super.testRunStarted(description);
        }

        @Override
        public void testRunFinished(Result result) throws Exception {
            System.err.println("runFinished " + (System.currentTimeMillis() - runStart) + "ms");
            super.testRunFinished(result);
        }

        @Override
        public void testStarted(Description description) throws Exception {
            System.err.println("testStarted: " + description.getMethodName());
            testStart = System.currentTimeMillis();
            super.testStarted(description);
        }

        @Override
        public void testFinished(Description description) throws Exception {
        	String methodName=description.getMethodName();
        	long runTime=System.currentTimeMillis() - testStart;
            System.err.println("testFinished: " + methodName + "(" + (runTime) + "ms)");
            methodTimes.put(description.getMethodName(), runTime);
            super.testFinished(description);
        }
   }	
	

	public static String testClassName;
	public static LinkedHashSet<String> testMethods;      // LinkedHashSet keeps order of elements
	public static LinkedHashSet<String> allTestMethods;
	public static String testDir="models/";
	public static boolean outputJson;
	public static int verboseLevel=0;
	public static LinkedHashMap<String,Long> methodTimes;
	
	@SuppressWarnings("unchecked")
	public static boolean handleArgs(String[] args) {
		String testMethodNames=null;
		for (int i = 0; i < args.length; i++) {
			if ("--verbose".equals(args[i]) || "-v".equals(args[i])  ) {
				verboseLevel = 1;
				continue;
			}	
			if ("-vv".equals(args[i])  ) {
				verboseLevel = 2;
				continue;
			}	
			if ("--output-json".equals(args[i])  ) {
				outputJson = true;
				continue;
			}
			if ("--help".equals(args[i]) || "-h".equals(args[i])  ) {
				printUsage(System.out);
				System.exit(0);
			}				
			if ("--test-dir".equals(args[i])) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --test-dir.");
					printUsage();
					System.exit(-1);
				}
				testDir = args[++i] +"/";
				continue;
			}		
			if ("--test-class".equals(args[i])) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --test-model.");
					printUsage();
					System.exit(-1);
				}
				testClassName = args[++i];
				continue;
			}	
			if ("--test-methods".equals(args[i])) {
				if (i == args.length - 1) {
					System.err.println("Missing argument for --test-methods.");
					printUsage();
					System.exit(-1);
				}
				testMethodNames = args[++i];
				continue;
			}				
		}
		if ( testClassName == null ) {
		    return false;
		} 
			

		try {
			allTestMethods= Test.getTestMethods(Test.testClassName);
		} catch (ClassNotFoundException e1) {
            System.err.println("test class '" + Test.testClassName + "' not found");
			printUsage();
			System.exit(-1);
		}	
		
		
		
		// get test methods to execute
		if ( testMethodNames == null ) {
			testMethods=allTestMethods;
		} else {
		    testMethods=new LinkedHashSet<String>( Arrays.asList(testMethodNames.split(",")) ) ;
		    for ( String method : testMethods ) {
		    	if ( ! allTestMethods.contains(method) ) {
					System.err.println("testClass '" + Test.testClassName + "' doesn't has test method '" + method + "', it only has test methods:" + allTestMethods);
					printUsage();
					System.exit(-1);		    		
		    	}
		    }
		}
        return true;		
		
	}	
	public static void printUsage() {
		printUsage(System.err); 
	}
	public static void printUsage(PrintStream s) {
		s.println("usage: java -jar tomte.jar [options]");
		s.println("");
		s.println("   options:");
		s.println("    --test-class       - Test class to run.");
		s.println("    --test-methods     - Comma separated list of test methods to run. By default all test methods are run.");		
		s.println("    --test-dir         - Directory where all test classes are stored.");
		s.println("    --output-json      - Output test results as json for further processing.");
		s.println("    --verbose | -v     - Give verbose output.");	
		s.println("    -vv                - Give very verbose output.");
		s.println("    --help | -h        - Give help info.");	
	}		

	
	
	//throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException 
	public  static void  runTest() {
		//testClassName, int verboseLevel
			 
		 PrintWriter w=new PrintWriter(new OutputStreamWriter (System.out));
		// String testClassName = "modeltest.Test_" + Test.testModel;
		

		 
		 // print info before running test
		 if (! Test.outputJson ) {
			w.println("run test:");
			w.println(" * test class: " + Test.testClassName);
			w.println(" * available test methods : " + Test.allTestMethods);
			w.println(" * executing test methods : " + Test.testMethods);			
			w.println(" * test directory: " + Test.testDir);	
			w.flush();
		 }
		 
		 // run test
		 Result result = null;
		try {
			// test specific initialization
			Class testClass=initTestClass(Test.testClassName,Test.testDir);
			// generic way to run test method
			result = Test.runTestClassMethods(testClass,Test.testMethods);
		} catch (SecurityException e) {
			
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		}
		 
		 // print results
		 if ( Test.outputJson ) {
			 Test.writeTestResultAsJson( w ,result,Test.testClassName,Test.testMethods,Test.allTestMethods ,Test.verboseLevel,Test.methodTimes);						 
		 } else {
			 Test.writeTestResult( w ,result,Test.testClassName,Test.testMethods,Test.allTestMethods ,Test.verboseLevel,Test.methodTimes);
		 }		
		 w.flush();
	} 
	
	
	public static void  writeTestResult (Writer outstream, Result result, String testClassname, Set<String> testnames, Set<String> alltestnames , int verboseLevel,LinkedHashMap<String,Long> methodTimes) {
		
		PrintWriter stream = new PrintWriter(outstream);
	
		// print test result
		if (  result.wasSuccessful() && verboseLevel==0 ) {
			stream.println("successfull runned all tests(" + result.getRunTime() + "ms)");
		} else {
			//stream.println("test result:\n    test class : " + testClassname);
			//stream.println("    available test methods : " + alltestnames);
			//stream.println("    executed test methods : " + testnames);			
			
			// initialize testresults to pass ; only when a failed messsage is found it is changed to FAIL (see below)!
			LinkedHashMap <String,String>tests= new LinkedHashMap<String,String>();
			for (String testname : testnames) {
		         // set by default to PASS
				tests.put(testname, "PASS("+ methodTimes.get(testname) + "ms)" );
			}
			// note: failure is either caused by a junit assert(failure) or by any Exception thrown(error)
			int failureCount=result.getFailureCount();
			if ( failureCount > 0 ) {
			      stream.println("test results: failed " + failureCount + " of " + result.getRunCount() + ":");
				  for (Failure failure : result.getFailures()) {
						String msg=failure.getMessage();
						if (msg == null ) msg="error in running unit test ";
						String header=failure.getTestHeader();
						String testname=header.replaceAll("\\(.*", "");
						tests.put(testname,"FAIL("+ methodTimes.get(testname) + "ms) - " + msg);
				  }			
			} else {
				  stream.println("test results: success for all " + result.getRunCount() + " tests:");
			}

			// print listing  of tests and their result : PASS or FAILED
			for ( String testname: tests.keySet() ) {
				stream.printf(" * %-20.20s : %s\n" , testname , tests.get(testname));
			}
			
			
			// only if verbose on print the stacktraces of the failures
			if ( verboseLevel > 1 && failureCount > 0) {
				stream.println("\nStacktraces of failures:");
				
				for (Failure failure : result.getFailures()) {
					  String header=failure.getTestHeader();
					  String testname=header.replaceAll("\\(.*", "");
					  String msg=failure.getMessage();
					  if (msg == null ) msg="error in running unit test ";
					  
					  stream.println(" * " + testname  + " : " + msg +"\n");
					  String trace = failure.getTrace();
					  stream.println(trace.replaceAll("^", "      "));
				}	  
			}
		}	
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public
	static void  writeTestResultAsJson (Writer outstream, Result result, String testClassname, Set<String> testnames, Set<String> alltestnames , int verboseLevel,LinkedHashMap<String,Long> methodTimes) {
		

	    JsonWriter stream = new JsonWriter(outstream);
		
		
		// initialize testresults to pass ; only when a failed messsage is found it is changed to FAIL (see below)!
		Map <String,String>tests= new LinkedHashMap<String,String>();
		for (String testname : testnames) {
	         // set by default to PASS
			tests.put(testname, "PASS");
		}			
		
		Map root=new LinkedHashMap(); // keeps key order!!
		
		root.put("successful",result.wasSuccessful());
		root.put("testClass",testClassname);
		
		root.put("availableTestMethods", alltestnames);
		root.put("executedTestMethods", testnames);			
		
		root.put("runCount",new Integer(result.getRunCount()));		
		root.put("failureCount",result.getFailureCount());
		root.put("ignoreCount",result.getIgnoreCount());
		root.put("runTimeInMs",result.getRunTime());

		
		Map failures = new LinkedHashMap();
		for (Failure failure : result.getFailures()) {
			  Map test = new LinkedHashMap(); // keeps key order!!
			  String header=failure.getTestHeader();
			  String testname=header.replaceAll("\\(.*", "");
			  String msg=failure.getMessage();
			  //Description d=failure.getDescription();
			  if (msg == null ) msg="error in running unit test ";
			  
			
			  
			  test.put("msg", msg);
			  
			  String trace = failure.getTrace();
			  test.put("trace", trace);
			 
			  failures.put(testname,test);			  
			  tests.put(testname, "FAIL");
		}			
		
		
		
		root.put("tests", tests);
		root.put("failures",failures);
		root.put("testTimesInMs", methodTimes);
		
		String jsonText = JSONValue.toJSONString(root);
		try {
			stream.write(jsonText);
			stream.flush();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}	
    public static Class initTestClass(String testClassname, String test_dir) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	    // get test class   		
		Class testClass=Class.forName(testClassname);
    	    	
		// set testdir in test class 
		@SuppressWarnings("unchecked")
		Method method=testClass.getMethod("setTestDir", String.class);
		method.invoke(testClass, test_dir);
		
		return testClass;
    }
	
	@SuppressWarnings("unchecked")
	public
	static Result  runTestClassMethods(Class testClass, LinkedHashSet<String> testMethods) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	
	
		
		
		
		// get method names
		String[] testMethodNames=  testMethods.toArray(new String[0]);
		
		
		// run test where we ignore all output
		PrintStream origOut=System.out; 
		PrintStream origErr=System.err; 
		System.setOut( new PrintStream( new util.NullOutputStream()) );
		System.setErr( new PrintStream( new util.NullOutputStream()) );		
		
		/*
		 *  run single  test method :
		 *     Request request = Request.method(testClass,"testLearn");
		 *     Result result = new JUnitCore().run(request);
		 *  run all test methods :
		 *     Result result = JUnitCore.runClasses(testClass); 
		 *  run several test methods : use TestMatchingFilter :     
		*/
		JUnitCore core = new JUnitCore();
	    Request testRequest = Request.aClass(testClass);
	    if (0 < testMethodNames.length) {
	      Filter matchFilter = new TestMatchingFilter(testClass, testMethodNames );
	      testRequest = testRequest.filterWith(matchFilter);
	    }
	    core.addListener(new RunJunitTestRunListener());
	    Result result=core.run(testRequest);	
		
		System.setOut(origOut);
		System.setErr(origErr);
		
		return result;
		
	}

	public static LinkedHashSet<String>  getTestMethods(String testClassname ) throws ClassNotFoundException {	
		
		// get testClass
		Class<?> testClass=Class.forName(testClassname);
		
		// get all test's names		
		LinkedHashSet <String>testnames = new LinkedHashSet<String>();		
		Method[] methods = testClass.getMethods();
		for (Method m : methods) {
			String methodname=m.getName();
			if (methodname.startsWith("test")) {
				  testnames.add(methodname);
			}      
		}				
		return testnames;
	}



}
