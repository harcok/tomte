package util;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;

import org.apache.log4j.Logger;





import java.util.Collections;



/* 
 *  java helpers
 * 
 */
public class JavaUtil {
	private static final Logger logger = Logger.getLogger(JavaUtil.class);
	
	private static String javacCmd;

	public static String getJavacCmd() {
		return javacCmd;
	}

	public static void setJavacCmd(String javacCmd) {
		JavaUtil.javacCmd = javacCmd;
	}
	
	public static   <T> List<T> elementElimination (List<T> list, ListTestInterface listTest) {
        int length=list.size();
        int pos=0; 
        while ( pos < length )  {        
          List<T> smallerList = new ArrayList<T>(list);
          smallerList.remove(pos);
          if ( listTest.test(smallerList) ) {
             length=length-1;
             list= smallerList;              
          } else {
             pos=pos+1;              
          }   
        }
		return list;
	}	
	
	
	  /*  findFirstKeyOfSortedMapHavingValue
	   *      find first key in sorted map which has same value as searchValue
	   *  returns:
	   *    - key:  first key which has searchValue as value
	   *    - null: searchValue not found in map
	   */
	  static public <K,V>  K  findFirstKeyOfSortedMapHavingValue(SortedMap<K,V> sortedmap, V searchValue) {


			for ( K key : sortedmap.keySet() ) {
				V value=sortedmap.get(key);
				if ( value.equals(searchValue)) {
					return key;
				}
			}
			return null;
      }	  	

    static public void sortListOfIntegerLists(List<List<Integer>> combinations ) {
        Collections.sort(combinations, new java.util.Comparator<List<Integer>>() {
            public int compare(List<Integer> a, List<Integer> b) {
                for ( int i=0; i<a.size();i++ ) {
                    int val_a = a.get(i);	
                    int val_b = b.get(i);
                	if ( val_a == val_b ) continue;
                	if ( val_a < val_b ) {
                		return -1;
                	} else {
                		return +1;
                	}
                }
                return 0; // fully equal
            }
        }); 
       
    }

	
	/* 
	 * example:
	 *     int k=2,n=5;
	 *     ArrayList<ArrayList<Integer>> combinations=util.JavaUtil.k_out_of_n_order_independent(k,n);		
	 *	   // sort combinations  : (0,1) before (0,2) and (0,3) before (1,0)
	 *	   util.JavaUtil.sortListOfIntegerLists(combinations);
	 *	   // reverse sorted list 
	 *	   Collections.reverse(combinations);
	 *
	 * Using recursion and backtracking(=just break of dead ends). 
	 * We keep adding elements recursively until the size of partial solution exceeds k.
	 */
    static public List<List<Integer>> k_out_of_n_order_independent(int k, int n) {
    	List<List<Integer>> combinations = new ArrayList<List<Integer>>();
        k_out_of_n_order_independent__recursion(k,n,new ArrayList<Integer>(), combinations);
       
        /*          
        // test if right number of combinations made: 
		int num=com.google.common.math.IntMath.binomial(n, k);
		assert num == combinations.size() : "error in k_out_of_n_order_independent__recursion";
        */
        return combinations;
    }
    

     
    static private void k_out_of_n_order_independent__recursion( int k, int n, List<Integer> prefixedList,List<List<Integer>> foundCombinations) {
        
    	
    	// first check end of recursion reached
    	int numberPicksDone=prefixedList.size();
    	
    	
    	
        if(numberPicksDone == k ) {
        	// recursion DONE: store result
        	Collections.reverse(prefixedList);
            foundCombinations.add(prefixedList);               	
        } else {   
        	// do operation and recursion on newPrefixedList        	
        	int numberPicksLeft=k-numberPicksDone;
            for(int i = n-1; i >= 0; --i) {	
            	//System.out.println("i="+ i);
            	if ( (numberPicksLeft-1) > i  ) {
            		// on wrong track : we cannot pick the rest of numbers anymore, because not enought left in subset
            		//System.out.println("impossible to pick " +  (numberPicksLeft-1) + " from " + i + "numbers" );
            		return;
            	}
            	List<Integer> newPrefixedList = new ArrayList<Integer>();
                newPrefixedList.addAll(prefixedList);
                newPrefixedList.add(i);
                
                //System.out.println("recurse with n="+ i + "newPrefixedList " + newPrefixedList + " numberPicksLeft=" + (numberPicksLeft-1));
                k_out_of_n_order_independent__recursion( k, i, newPrefixedList, foundCombinations);
            }
        }
    }	
    /*
    static private void k_out_of_n( int k, int n, ArrayList<Integer> prefixedList,ArrayList<ArrayList<Integer>> foundCombinations) {
        int numberPicksDone=prefixedList.size();
        if(numberPicksDone == k ) {
        	// DONE: store result
        	Collections.sort(prefixedList);
            foundCombinations.add(prefixedList);               	
        } else {       	
            for(int i = n; i >= 1; --i) {
                ArrayList<Integer> newPrefixedList = new ArrayList<Integer>();
                newPrefixedList.addAll(prefixedList);
                newPrefixedList.add(i);
                k_out_of_n( k, i-1, newPrefixedList, foundCombinations);
            }
        }
    }	      
    
    static private void recursion(int n, int k, ArrayList<Integer> prefixedList,ArrayList<ArrayList<Integer>> combinations) {
        if(prefixedList.size() == k && !combinations.contains(prefixedList)) {
            Collections.sort(prefixedList);
            combinations.add(prefixedList);
        } else if(prefixedList.size() > k) {            
        	return;
        } else {
            for(int i = n; i >= 1; --i) {
                ArrayList<Integer> partial_sol = new ArrayList<Integer>();
                partial_sol.addAll(prefixedList);
                partial_sol.add(i);
                recursion(i-1, k, partial_sol, combinations);
            }
        }
    }	
        
    
     */
    

	/* mathematical direct product :
	 * 
	 *   given n set of lists it generates  all possibles lists of size n of possible combinations of values from the n lists 
	 *   eg.  [1,2] [4,5] ->  [1,4] [1,5] [2,4] [2,5]
     *
	 */
    public static <T> void generateCombinations(LinkedList<LinkedList<T>> result, LinkedList<LinkedList<T>> outerList, LinkedList<T> outPut) {
        LinkedList<T> list = outerList.get(0);

        for(T i : list) {
            LinkedList<LinkedList<T>> newOuter = new LinkedList<LinkedList<T>>(outerList);
            newOuter.remove(list);

            LinkedList<T> newOutPut = new LinkedList<T>(outPut); 
            newOutPut.add(i);
            if(outerList.size() > 1) {
            	generateCombinations(result,newOuter, newOutPut );
             } else {
            	result.add(newOutPut); 
             }
        }
    }
    
    /* same as generateCombinations but values must be different */
    public static <T> void generateCombinationsDifferent(List<List<T>> result, List<List<T>> outerList, List<T> outPut) {
        List<T> list = outerList.get(0);

        for(T i : list) {
            
        	// deepcopy
            List<List<T>> newOuter = new LinkedList<List<T>>();
            for (List<T> sublist: outerList  ) { 
            	newOuter.add(  new LinkedList<T> ( sublist)   );            	
            }            
            newOuter.remove(list);

            for (List<T> sublist: newOuter  ) { 
            	sublist.remove(i);            	
            }

            LinkedList<T> newOutPut = new LinkedList<T>(outPut); 
            newOutPut.add(i);
            if(outerList.size() > 1) {
            	generateCombinationsDifferent(result,newOuter, newOutPut );
             } else {
            	result.add(newOutPut); 
             }
        }
    }      
    
	//Helper method to convert int arrays into Lists
	// source: http://stackoverflow.com/questions/960431/how-to-convert-listinteger-to-int-in-java
	public static List<Integer> intArrayAsList(final int[] a) {
	    if(a == null)
	        throw new NullPointerException();
	    return new AbstractList<Integer>() {

	        @Override
	        public Integer get(int i) {
	            return a[i];//autoboxing
	        }
	        @Override
	        public Integer set(int i, Integer val) {
	            final int old = a[i];
	            a[i] = val;//auto-unboxing
	            return old;//autoboxing
	        }
	        @Override
	        public int size() {
	            return a.length;
	        }
	    };
	}	
	
	static public <T> String serializeListAsFunctionCall( List<T> list) {
		StringBuilder result = new StringBuilder();		
		
		result.append("(");
		for (T p : list) {
			if ( p==null  ) {
				result.append("?");
			} else {	
			   result.append(p.toString());
			}			
			result.append(",");
		}
		if (list.size() > 0 ) result.deleteCharAt(result.length()-1);
		result.append(")");	
		return result.toString();		
	}	
	
	static public <T> String serializeListAsSuffixes( List<T> list) {
		StringBuilder result = new StringBuilder();		
		
		for (T p : list) {
			result.append("_");
			if ( p==null  ) {
				result.append("?");
			} else {	
			   result.append(p.toString());
			}
		}
		return result.toString();		
	}	
	
	
	static public String getPrefixFromString(String str, String separator) {
		int pos=str.indexOf(separator);
		if ( pos == -1 ) {
			return null;
		} else {
		    return str.substring(0, pos );
		}    
	}
	
	static public String removePrefixFromString(String str, String separator) {
		int pos=str.indexOf(separator);
		if ( pos == -1 ) {
			return str;
		} else {
		    return str.substring(pos + separator.length() );
		}   		
	}	

	static public Integer deserializeIntValue(String str) {
        Integer integerValue;
        try {
        	integerValue = new Integer(str);
        } catch (NumberFormatException ex) {
        	String msg="Error deserializing integer from string "  + str ;
            throw new RuntimeException(msg);
        }	   		
		return integerValue;
	}
	
	static public List<Integer> deserializeIntValues(String serializedValues, String separator ) {
		
		String[] values = serializedValues.split(separator);
		List<Integer> paramVals = new ArrayList<Integer>();
        for (String valueStr : values) {
            paramVals.add(deserializeIntValue(valueStr));            
        }
        return paramVals;
	}	
	
	
	
	public static String getStackTraceString(Throwable e){
	    StringWriter sw = new StringWriter();
	    e.printStackTrace(new PrintWriter(sw));
	    return sw.toString();			
	}
	
	
	public static String repeat(String s, int times){
	    StringBuffer b = new StringBuffer();

	    for(int i=0;i < times;i++){
	        b.append(s);
	    }

	    return b.toString();
	}	
	
	public static String join(Object[] s, String glue)
	{
		int k = s.length;
		if (k == 0)
			return "";
		StringBuilder out = new StringBuilder();
		out.append(s[0]);
		for (int x = 1; x < k; ++x)
			out.append(glue).append(s[x]);
		return out.toString();
	}	
	
	public static String getClassPath(String classPackage, String className) {
		 String packagePath = classPackage.replace('.', '/');
		 return  packagePath + '/' +  className + ".java";	 	 		
	}

	public static String getFullClassPath(String sourcePathDir, String classPackage, String className) {			
		 return  sourcePathDir + "/" + getClassPath(classPackage, className);	 	 		
	}

	/* compileJavaSource - compile a java source file 
	 * 
	 * - compiles <classPathDir>/<classPackage as path>/<className>.java
	 * - <classPath> is the java CLASSPATH which specifies all paths having  java compiled bytecode classes which may be needed
	 *   for compiling dependencies
	 * - note: <classPathDir> must already exist but <classPackage as path> will be made on the fly
	*/
	public static void compileJavaSource(String classPath, String sourcePathDir, String classPackage, String className) {
		 String cmd_str=javacCmd +" -sourcepath " + sourcePathDir + " -classpath " + classPath + " " + getFullClassPath(sourcePathDir,classPackage, className);
		 RunCmd.runCmd(cmd_str,System.out,System.err,true,true);
		 logger.debug(cmd_str);
	}

	//  java class loader where you can specify runtime the CLASSPATH
	//
	// when a class is not in the CLASSPATH you normally cannot use it in java,
	// however with this function you can!
	// 
	// note: after loading you must cast the object to the right interface
	//
    // IMPORTANT:  classpath can  be directory or an jar file !!
	    //
    public static 	Object	 loadJavaClass(String classPath ,String fullClassName) {
	    ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
	
	    URL dirUrl;
	    
	    URLClassLoader urlClassLoader = null; 
	     
		try {
			dirUrl = new File(classPath).toURL();
			
	        // Add the conf dir to the classpath
	        // Chain the current thread classloader
	         urlClassLoader
	         = new URLClassLoader(new URL[]{dirUrl},
	        		 currentThreadClassLoader);
	
	        // Replace the thread classloader - assumes
	        // you have permissions to do so
	      //  Thread.currentThread().setContextClassLoader(urlClassLoader);
	        
		} catch (MalformedURLException e) {
			throw new ExceptionAdapter(e);
		}
		Class cls = null;
		Object inst= null;
		try {
			cls=urlClassLoader.loadClass(fullClassName);
			try {
				inst= (Object) cls.newInstance();
			} catch (InstantiationException e) {
				throw new ExceptionAdapter(e);
			} catch (IllegalAccessException e) {
				throw new ExceptionAdapter(e);
			}
		} catch (ClassNotFoundException e) {
			throw new ExceptionAdapter(e);
		}
				
		return inst; 
	}	
    
    static public <T> List<List<T>> twoDimArrayToListOfList(T[][] twoDArray) {
        List<List<T>> list = new ArrayList<List<T>>();
        for (T[] array : twoDArray) {
            list.add(Arrays.asList(array));
        }
        return list;
    }
    
    /*
     * get all elements from s1 and  from s2     ( mathematically: s1 \/ s2 )
     * 
     * return: union in LinkedHashSet in order of elements in s1 and then for remaining in order of s2
     */        
    static public <T>  LinkedHashSet<T>  getUnion( Set<T> s1, Set<T>  s2 ) { 
    	LinkedHashSet<T> union = new LinkedHashSet<T>(s1);  // retains insertion order of s1
        union.addAll(s2);
        return union;
    }
    
    /*
     * get elements both in s1 and in s2     ( mathematically: s1 /\ s2 )
     * 
     * return: intersection in LinkedHashSet in order of elements in s1 
     */    
    static public <T>  LinkedHashSet<T>  getIntersection( Set<T> s1, Set<T>  s2 ) { 
    	LinkedHashSet<T> intersection = new LinkedHashSet<T>(s1); // retains insertion order of s1
        intersection.retainAll(s2);
        return intersection;
    }    

    /*
     * get elements in s1 which are not in s2     ( mathematically: s1\s2 )
     * 
     * return: difference in LinkedHashSet in order of elements in s1  
     */
    static public <T>  LinkedHashSet<T>  getElementsOnlyInFirstSet( Set<T> s1, Set<T>  s2 ) {
    	LinkedHashSet<T> difference = new LinkedHashSet<T>(s1);  // retains insertion order of s1
        difference.removeAll(s2);
        return difference;  // returns difference in order of s1 => first difference of s1, then second difference s2
    }

}
