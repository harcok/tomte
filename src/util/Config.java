package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import abslearning.exceptions.ConfigurationException;




public class Config {

    protected Object obj = this;


    /**
     *  walk recursively through data of nested maps keeping track of the walking path,
     *  and when reaching a leaf field, then store the leaf's path and value as key value pair in the field2value Map
     */
    public void getField2value(Object data, String path ,HashMap<String,Object> field2value ) {

            Map<?, ?> map = (Map<?, ?>) data;
            for (Object key :map.keySet()  ) {
                Object value=map.get(key);
                String newpath;

                // config parameter in yaml file  must have a value
                //  null values are often empty sections(empty map) for which all field assignments are commented out
                if ( value == null ) continue;

                if ( path.equals("")) {
                    newpath=(String)key;
                } else {
                    newpath = path + "_" + key;
                }
                if ( value instanceof Map<?,?> ) {
                    getField2value(value,newpath,field2value);
                } else {
                    field2value.put(newpath, value);
                }
            }
    }

    /** get map with flattened field names with their corresponding values  by flattening a nested map data structure
     *
     * @param object  which is either a basic value or map.  When it is a map it contains fields which cantain similar
     *                objects which again are either a value or map. So it is a nested map data structure.
     * @return
     */
    public Map<String,Object> getField2value(Object object) {
        HashMap<String,Object> field2value=new HashMap<>();

        if ( object instanceof Map<?,?> ) {
            Map<?, ?> map = (Map<?, ?>) object;
            getField2value(map,"",field2value);
        }

        return  field2value;
    }

    public void handleConfigFile(String configFileName) {

        if (configFileName == null) {
            throw new ConfigurationException("Config file not defined");
        }

        // check configfile readable :
        //   throws CheckException if not readable such that applications using this
        //   library are able to supply the error message itself
        util.Check.readableFile(configFileName);

        InputStream input = null;
        try {
            input = new FileInputStream(configFileName);
        } catch (FileNotFoundException e) {
            throw new ConfigurationException("Cannot read config file : " + configFileName);
        }

        // instanciate yaml loader
        Yaml yaml = new Yaml();

        // read config fields from config yaml file
        Object object =yaml.load(input);
       // System.out.println(yaml.dump(object));


        Map<String,Object> field2value=getField2value(object);

        // get current config object (fields access works with inheritance)
        Class<? extends Object> thisClass = obj.getClass();

        StringBuilder errors= new StringBuilder();
        for ( String fieldname : field2value.keySet()) {
            try {
                Field field=thisClass.getField(fieldname);
                Object value= field2value.get(fieldname);
                try {
                    field.set(obj, value);  // can cause conversion errors! (eg. "bla" cannot be converted to int)
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    String fieldtype=field.getType().getName();
                    errors.append("  * Invalid value '" +  value + "' for parameter '" + fieldname + "' with type '" + fieldtype + "'\n");
                }

            } catch (NoSuchFieldException|SecurityException e) {
                errors.append("  * Invalid parameter '" +  fieldname + "'\n");
            }
        }
        if ( errors.length() > 0 ) {
            errors.insert(0,"\n\nERROR: Invalid config file '" + configFileName + "':\n" );
            errors.append("\nExiting because don't allow running tool with invalid configuration to prevent unknown misconfiguration.");
            throw new ConfigurationException(errors.toString());
        }

    }

    /** Handle config options passed as list of strings.
     * <p/>
     * Overrule a config option on the commandline as: --config-option optionname=optionvalue<br/>
     * eg. --config-option learn_useSutSimulation=true
     *
     * @param configOptions
     */
    public void handleConfigOptions(List<String> configOptions) {
        Map<String, String> option2value = new HashMap<String, String>();

        for (String configOption : configOptions) {
            String[] parts = configOption.split("=");
            if (parts.length != 2) {
                throw new ConfigurationException("Error parsing config parameter : " + configOption);
            }
            option2value.put(parts[0], parts[1]);
        }

        // get fields from current config object (by inheritance)
        Class<? extends Object> thisClass = obj.getClass();
        Field[] fields = thisClass.getFields();

        // lookup each object's field value in config options
        for (Field field : fields) {
            String name = field.getName();

            // check if field given by option
            if (!option2value.containsKey(name))
                continue;

            // fetch value from option and store in field
            // String strValue=option2value.get(name);
            String value = option2value.get(name);

            try {
                if (value != null) {
                    // Class<?> type= field.getType(); // next line can be replaced with this line
                    // java.lang.reflect.Type type=field.getGenericType();
                    String type = field.getType().toString();
                    // System.err.println("name : '" + name + "'(type:" + field.getType() + ") with
                    // value '" + value + "' ");

                    // next lines can cause conversion errors! (eg. "bla" cannot be converted to
                    // boolean)
                    if (type.equals("double")) {
                        Double doubleValue = Double.parseDouble(value);
                        field.set(obj, doubleValue);
                    } else if (type.equals("float")) {
                        Float floatValue = Float.parseFloat(value);
                        field.set(obj, floatValue);
                    } else if (type.equals("byte")) {
                        Byte byteValue = Byte.parseByte(value);
                        field.set(obj, byteValue);
                    } else if (type.equals("short")) {
                        Short shortValue = Short.parseShort(value);
                        field.set(obj, shortValue);
                    } else if (type.equals("int")) {
                        Integer intValue = Integer.parseInt(value);
                        field.set(obj, intValue);
                    } else if (type.equals("long")) {
                        Long longValue = Long.parseLong(value);
                        field.set(obj, longValue);
                    } else if (type.equals("boolean")) {
                        Boolean booleanValue = Boolean.parseBoolean(value);
                        field.set(obj, booleanValue);
                    } else if (type.equals("class java.lang.String")) {
                        field.set(obj, value);
                    } else if (type.equals("interface java.util.List")) {
                        value = value.replaceAll("\\[|\\]|\"", "");
                        String[] split = value.split(",");
                        List<String> splitList = Arrays.asList(split);
                        splitList.replaceAll(str -> str.trim());
                        field.set(obj, splitList);

                    } else {
                        // don't allow unknown options: exit with error, because you could have made a typo!
                        throw new ConfigurationException("Error config parameter type not supported: '" + name + "'(type:"
                                + field.getType() + ") with value '" + value + "'  NOT SUPPORTED");
                    }

                }
                option2value.remove(name);
            } catch (Exception e) {
                throw new ConfigurationException("Error applying config parameter '" + name + "'(type:" + field.getType()
                                   + ") with value '" + value + "'");
            }
        }
        if (!option2value.isEmpty()) {
            for (String name : option2value.keySet()) {
                // don't allow unknown options: exit with error, because you could have made a typo!
                throw new ConfigurationException("Error: unknown config parameter '" + name + "'");
            }
        }
    }

    public void dumpAsYaml(Writer outstream, String indentStep) {

        PrintWriter stream = new PrintWriter(outstream);

        Class<? extends Object> thisClass = obj.getClass();

        Field[] fields = thisClass.getFields();

        String[] prevParts = {};
        for (Field field : fields) {
            String name = field.getName();
            String[] parts = name.split("_");

            for (int i = 0; i < (parts.length - 1); i++) {
                if (prevParts.length - 1 > i && parts[i].equals(prevParts[i])) {
                    continue;
                } else {
                    stream.println(repeat(indentStep, i) + parts[i] + ":");
                }
            }
            Object value = null;
            try {
                value = field.get(obj);
            } catch (IllegalArgumentException e) {

                e.printStackTrace();
            } catch (IllegalAccessException e) {

                e.printStackTrace();
            }
            String value_str = "";
            if (value != null) {
                /*
                 * if ( value.getClass().isArray() ) { //Object[] arr=(Object[]) value;
                 * value_str="[ " + join(unpack(value),", ") + " ]"; // value.toString();
                 * stream.println( repeat(indentStep, parts.length-1 ) + parts[parts.length-1] +
                 * ": " + value_str ); } else {
                 *
                 *
                 * value_str=value.toString(); stream.println( repeat(indentStep, parts.length-1
                 * ) + parts[parts.length-1] + ": " + value_str ); }
                 */

                value_str = convertObjectValueToYamlString(value);
                stream.println(repeat(indentStep, parts.length - 1) + parts[parts.length - 1] + ": " + value_str);

            } else {
                value_str = "null";
                stream.println(repeat(indentStep, parts.length - 1) + parts[parts.length - 1] + ": " + value_str);
            }
            prevParts = parts;
        }
    }

    public String convertObjectValueToYamlString(Object value) {
        String value_str = "";
        if (value instanceof Collection<?>) {
            value_str = "[ ";
            Collection<?> valueCollection = (Collection<?>) value;
            for (Object s : valueCollection) {
                value_str = value_str + "'" + convertObjectValueToYamlString(s) + "', "; // note: use single instead double quotes,
                                                                       // because within double-quotes special
                                                                       // characters are interpreted which we don't have
                                                                       // in the Strings!
            }
            value_str = value_str + "]";

        } else if (value instanceof Map<?, ?>) {
            value_str = value.toString();
        } else {
            value_str = value.toString();
        }
        return value_str;
    }

    public String repeat(String s, int times) {
        StringBuffer b = new StringBuffer();

        for (int i = 0; i < times; i++) {
            b.append(s);
        }
        return b.toString();
    }

}
