package util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;


public class RunCmd {
	private static final Logger logger = Logger.getLogger(RunCmd.class);
    public static Process currentProcess; // for debugging


    private static class  MyHandler implements Thread.UncaughtExceptionHandler {

        private boolean exitOnException;
        private AtomicReference throwableReference;



		public MyHandler( AtomicReference throwableReference,boolean exitOnException) {
        	this.exitOnException= exitOnException;
        	this.throwableReference=throwableReference;
        }

		public void uncaughtException(Thread arg0, Throwable e) {
			//logger.error("exception happened when running cmdline command in RunCmd");
			logger.error( "exception: " + e.getClass().toString() + ", msg: " + e.getMessage() );


		    if (this.exitOnException) {
		    	 throwableReference.set(e);
		    	// note: all exceptions thrown here will be ignored!
		    	//       Thus instead pass them to parent thread using an AtomicReference class  objectinstance: throwableReference
		    }
		}

    }
    private static class MyThread extends Thread {

		private volatile boolean threadAlive = false;
		private final InputStream in;
		private final OutputStream out;
		private int sleepcounter;
		private String name;

		public MyThread(final InputStream in, final OutputStream out, String name ) {
			this.in=in;
			this.out=out;
			this.name=name;
            this.sleepcounter=0;
		}

		public void mystop() {
			threadAlive = false;
	    }

		public void run() {
			threadAlive = true;
			logger.debug("run process redirector: " + name);
			if ( in == null ) {
				logger.debug("no input stream in process redirector: " + name);
				return;
			}
			if ( out == null ) {
				logger.debug("no output stream in process redirector: " + name);
				return;
			}
			try {

             boolean readInBlocks=true;
             if ( readInBlocks ) {
                int expectedDataLength = 4096; //todo - set accordingly/experiment. Does not have to be precise value.
                //int expectedDataLength = 512; //todo - set accordingly/experiment. Does not have to be precise value.
                //int expectedDataLength = 128; //todo - set accordingly/experiment. Does not have to be precise value.
                byte[] chunk = new byte[expectedDataLength];
                int numBytesJustRead;
                while((numBytesJustRead = in.read(chunk)) != -1) {
                    //			out.write((char) c);
                	logger.debug("redirecting bytes: "  + name);
                    out.write(chunk, 0, numBytesJustRead);
                    logger.debug("done redirecting bytes: "  + name);
                    if ( !threadAlive ) {
			            logger.fatal("thread not alive");
                        break;
                    }

                }
            } else {
               while ( threadAlive ) {
						int c = in.read();
						if (c == -1) {
							break;
						}
						out.write((char) c);
               }
            }
	//	while ( threadAlive ) {
                    /*
						int c = in.read();
						if (c == -1) {
							break;
						}
						out.write((char) c);
                      */


				  /*
					if(in.available() > 0) {
						int c = in.read();
						if (c == -1) {
							break;
						}
						out.write((char) c);
					} else {
                        //DISABLED SLEEP
                        //int ms=-1;
                        // sleep
			        	//util.Time.millisleep(ms);

                       // 	util.Time.yield();  //-> cause larger sys time in unix 'time' command
                         //util.Time.millisleep(100);
                //         util.Time.millisleep(1);
                         this.sleepcounter++;
						int c = in.read();
						if (c == -1) {
							break;
						}
						out.write((char) c);
					}
                    */
		//	}
        //	logger.fatal("sleepcounter: " + this.sleepcounter);

				// read last bytes in "in" inputstream which got there during last millisleep
				// note: always quickly terminates when buffer empty!!
				while ( in.available() > 0 ) {

			        logger.fatal("in.available check");

					int c = in.read();
					if (c == -1) {
						break;
					}
					out.write((char) c);
				}

				//logger.fatal("sleepcounteragain: " + this.sleepcounter);


			//} catch (InterruptedException e) {
			//	logger.debug("Problem handling Interrups of running command with java RunCmd API: " +name);
			} catch (IOException e) { // just exit
				logger.debug("Problem handling IO of running command with java RunCmd API: " +name);
				//e.printStackTrace(); // to stderr stream
			}
			logger.debug("process redirector stopped: " + name);
		}

	}

	/*
	 * Function to automatically redirect output from a subprocess to another
	 * output stream.
	 *
	 * e.g. redirectIO(process.getInputStream(), System.out);
	 *
	 * Note: process.getInputStream() is the output from the subprocess which is
	 * input for current process!!
	 */

	public static MyThread redirectIO(final InputStream in, final OutputStream out, String name) {
		// http://stackoverflow.com/questions/60302/starting-a-process-with-inherited-stdin-stdout-stderr-in-java-6



		MyThread t=new MyThread(in,out, name);

		/*
		Thread t=new Thread(new Runnable() {
			private volatile boolean threadAlive = false;

			public void mystop() {
				threadAlive = false;
		    }

			public void run() {
				threadAlive = true;
				try {
					while (threadAlive) {
						int c = in.read();
						if (c == -1)
							break;
						out.write((char) c);
					}
				} catch (IOException e) { // just exit
					logger.debug("Problem handling IO of running command with java RunCmd API");
					//e.printStackTrace(); // to stderr stream
				}
			}
		});
		*/
		t.start();
		return t;

	}



	public  static void osOpenFile(String filename) {
		String os=System.getProperty("os.name");
		if ( os.startsWith("Windows") ) {
		     String[] cmdarray= new String[3];
		     cmdarray[0]="cmd";
		     cmdarray[1]="/c";
		     cmdarray[2]=filename;
		     osDetachedRunCmd(cmdarray);
		} else if ( os.startsWith("Linux") ) {
		     String[] cmdarray= new String[2];
		     cmdarray[0]="xdg-open";
		     cmdarray[1]=filename;
		     osDetachedRunCmd(cmdarray);
		} else if ( os.startsWith("Mac") ) {   // Mac os x
		     String[] cmdarray= new String[2];
		     cmdarray[0]="open";
		     cmdarray[1]=filename;
		     osDetachedRunCmd(cmdarray);
		} else {
			logger.debug("Problem: unknown operating system, so do not know how to open file : " + filename);
		}
    }


	// run process detached from java runtime environment!
	//
	// note: with Runtime.exec you cannot start a process fully detached from
	//       the java Runtime. The only way to do it, is to start a local
	//       application launcher which launches it detached for you.
	//       see : http://stackoverflow.com/questions/931536/how-do-i-launch-a-completely-independent-process-from-a-java-program
	//
	// note: that this works ok when running command from commandline,
	//       however if running from eclipse it fails : bug in eclipse terminate button!!
	//
	public  static void osDetachedRunCmd(final String[] cmd) {
		String os=System.getProperty("os.name");
		if ( os.startsWith("Windows") ) {
			 //List <String> cmdlist=Arrays.asList(cmd);
			 ArrayList <String> cmdlist = new ArrayList <String> ( Arrays.asList(cmd) );
			 cmdlist.add(0, "start");
			 cmdlist.add(0, "/c");
			 cmdlist.add(0, "cmd");
			 String cmdarray[]=cmdlist.toArray(new String[0]);
			 RunCmd.runCmd(cmdarray, null,null, true,true); // note: waits until launch command succeeded!
		} else if ( os.startsWith("Linux") ) {
		     String[] cmdarray= new String[2];
		     cmdarray[0]="xdg-open";

		     logger.debug("Problem: osDetachedRunCmd not yet implemented for Mac");

		} else if ( os.startsWith("Mac") ) {   // Mac os x
		     String[] cmdarray= new String[2];
		     cmdarray[0]="open";

		     logger.debug("Problem: osDetachedRunCmd not yet implemented for Mac");

		} else {
			 logger.debug("Problem: unknown operating system, so do not know how to start cmd ");
		}
    }




	public static  String  runCmd(final String cmd) {
		return runCmd(cmd,null);
	}

	public static String runCmd(final String[] cmdarray) {
		return runCmd(null,cmdarray);
	}

	public static String runCmd(final String cmd,final String[] cmdarray) {
		//PrintStream outstream=new PrintStream(new ByteArrayOutputStream());
		ByteArrayOutputStream outstream=new ByteArrayOutputStream();


	    // redirect stderr to logger (only when problem arises!)
		ByteArrayOutputStream errstream=new ByteArrayOutputStream();
		try {
   		    runCmd(cmd,cmdarray,outstream,errstream,true,true);
		} catch ( ExceptionAdapter e) {
			// log stderr of runned cmd when exception is thrown
			String error=errstream.toString().trim();
			String output=outstream.toString().trim();
			if ( output.equals("") ) output="<empty stdout>";
			if ( error.equals("") ) error="<empty stderr>";
			error=error.replaceAll("(?m)^", "    ");
			output=output.replaceAll("(?m)^", "    ");

			// get cmdline from cmd or cmdarray
			String str;
			if ( cmd!=null ) {
	    	    str=cmd;
	    	} else {
	    		str=util.JavaUtil.join(cmdarray, " ");
	    	}
			//logger.info("run command: " + str);

			//  + e.originalException.getMessage()
			String logMsg="\nproblem running command:\n\n        " + str + "\n" + "\n\nstderr of command:\n" + error + "\n\nstdout of command:\n" + output + "\n";

			//logger.fatal("stderr of command:\n" + error );
			//logger.fatal("stdout of command:\n" + output );



			logMsg=logMsg.replaceAll("(?m)^", "    ");
			logger.fatal(logMsg);

			// rethrow exception with original stacktrace
			// src: http://stackoverflow.com/questions/1097527/rethrowing-exceptions-in-java
			throw e;

			//throw new RuntimeException(logMsg);
		}


		/*
		// redirect stderr to stderr of current process
		PrintStream errstream=System.err;
		runCmd(cmd,cmdarray,outstream,errstream,true,true);
        */

		// errstream = null  -> NOT redirected(inherited) to stderr of current process => thus lost!!
		//runCmd(cmd,cmdarray,outstream,null,true,true);


//		try {
//			outstream.flush();
//			errstream.flush();
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}
	/*
		//outstream.flush();
		String error=errstream.toString();
	//	if (error!=null)
		logger.fatal( error );
		*/

		return outstream.toString();
	}


	public  static Thread runCmd(final String cmdstr,final OutputStream outstream, final OutputStream errstream, final boolean wait,  final boolean exitOnException)  {
		return runCmd(cmdstr,null,outstream,errstream,wait, exitOnException);
	}
	public  static Thread runCmd(final String[] cmdarray,final OutputStream outstream, final OutputStream errstream, final boolean wait, final boolean exitOnException)  {
		return runCmd(null,cmdarray,outstream,errstream,wait, exitOnException);
	}



	/*
	 * Run a command in a separate process and redirect all its output to the
	 * given PrintStream
	 *
	 * eg. runCmd("python plot.py",System.out,true);
	 *
	 * runCmd("python plot.py",stdout,false);try {Thread.sleep(2000);} catch
	 * (Exception e) {};System.exit(-1);
	 * runCmd("python plot.py",stdout,true);System.exit(-1);
	 */
	public static Thread runCmd(final String cmd,final String[] cmdarray, final OutputStream outstream, final OutputStream errstream, final boolean wait, final boolean exitOnException)  {
		logger.debug("enter runCmd function");
		final boolean done=false;

	    AtomicReference throwableReference = new AtomicReference<Throwable>();



		String str;
		if ( cmd!=null ) {
    	    str=cmd;
    	} else {
    		str=util.JavaUtil.join(cmdarray, " ");
    	}
		logger.info("cmd: " + str);

		Thread t = new Thread(new Runnable() {
			public void run() {

	    		MyThread redirectout = null;
	    		MyThread redirecterr = null;
	    		Process process=null;
				String str=null;



				try {

					if ( cmd!=null ) {
			    	    str=cmd;
			    	} else {
			    		str=util.JavaUtil.join(cmdarray, " ");
			    	}

					if ( cmd!=null ) {
			    	    process = Runtime.getRuntime().exec(cmd);
			    	} else {
			    		process = Runtime.getRuntime().exec(cmdarray);
			    	}

					currentProcess=process;
		    		process.getOutputStream().close();		// Solution: closes output of subproces which is directed to input of cmd which is run by subprocess. -> cmd cannot wait for input!!


		    		// redirect output/error streams
			    	if (outstream != null){
			    		logger.debug("redirect out");
			    		redirectout=redirectIO(process.getInputStream(), outstream,"out");
			    	} else {
			    		logger.debug("no redirect out");
			    		process.getInputStream().close();
			    	}
			    	if (errstream != null){
			    		logger.debug("redirect err");
			    		redirecterr=redirectIO(process.getErrorStream(), errstream,"err");
			    	} else {
			    		logger.debug("no redirect err");
			    		process.getErrorStream().close();
			    	}



			    	logger.debug("start waiting for cmd");
			    	process.waitFor();
			    	logger.debug("finished waiting for cmd");



					logger.debug("stop cmd redirectors for stdout/stder to RunCmd's outstream/errstream ");
					// stop stream redirector processes
					if (outstream != null && redirectout != null){
						if (redirectout.isAlive()) {
							redirectout.mystop();
							//util.Time.millisleep(2000);
							while ( redirectout.isAlive() ) {
								logger.debug("sleep waiting redirecterr stops ");
								util.Time.millisleep(100);
							}
						}

						//logger.debug("redirector stdout alive: " + redirectout.isAlive());
					}
					if (errstream != null  && redirecterr != null){
						if (redirecterr.isAlive()) {
							redirecterr.mystop();
							//util.Time.millisleep(2000);
						}
						while ( redirecterr.isAlive() ) {
							logger.debug("sleep waiting redirecterr stops ");
							util.Time.millisleep(100);
						}
						//logger.debug("redirector stderr alive: " + redirecterr.isAlive());
					}

					logger.debug("flush RunCmd's outstream/errstream ");
			    	// flush streams when process finished
			    	// note: do no close them, because it can be System.out/System.err which must
			    	//       be used later
			    	if (outstream != null) {
			    		  outstream.flush();
			    	}
			    	if (errstream != null) {
			    		  errstream.flush();
			    	}


			    	// test if process really finished
					if (process.exitValue() != 0) {

						logger.debug("Exit(" + process.exitValue() + "): Problem  running command : " + str);
					//	System.err.println(Thread.currentThread().getStackTrace().  .toString() );
					//

					//	RuntimeException e=new RuntimeException("Exit(" + process.exitValue() + "): Problem  running command : " + str);
					//	e.printStackTrace();

						/*
						for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
						    System.out.println(ste);
						}
						*/
						throw new RuntimeException("Exit(" + process.exitValue() + "): Problem  running command : " + str);
					}


			    	logger.debug("succesfully finished cmd  : " + str);

				} catch (InterruptedException e) {
					// if calling t.interrupt you get here  (t is current thread running the  cmdline process
					// check process is still running, if so destroy it

					logger.debug("Interrupt called on process running command : " + str + ", InterruptedException msg: " + e.getMessage());
					if ( process != null ) {
						try {
							process.exitValue();
						} catch  ( IllegalThreadStateException  exc ){
							// process not exited yet
							logger.debug("IllegalThreadStateException, command still running : we destroy it");
							process.destroy();
						}
					}
					// interrupt caused by user by calling t.interrupt and therefore expected

					// let MyHandler decide what to do (exit or continue) :
					throw new ExceptionAdapter(e);

				} catch (IOException e) {
					logger.debug("Problem handling IO of running command : " + str +", IOException msg: " + e.getMessage() );
					//e.printStackTrace();
					if ( process != null ) {
						try {
							process.exitValue();
						} catch  ( IllegalThreadStateException  exc ){
							// process not exited yet
							logger.debug("command still running : we destroy it");
							process.destroy();
						}
					}
					// unexpected interrupt : throw to end user
					throw new ExceptionAdapter(e);
				} catch (Exception e) {

					logger.debug("Problem with running command : " + str);

					if ( process != null ) {
						try {
							process.exitValue();
						} catch  ( IllegalThreadStateException  exc ){
							// process not exited yet
							logger.debug("IllegalThreadStateException, command still running : we destroy it");
							process.destroy();
						}
					}
					// unexpected interrupt : throw to end user
					throw new ExceptionAdapter(e);
				}



			}
		});


		t.setUncaughtExceptionHandler(new MyHandler(throwableReference,exitOnException));

		if (wait) {
			logger.debug("start thread; wait for cmd to finish by waiting for thread ");
			try {
				t.start();
				t.join();
				// if an exception happened in thread t, then fetch it and throw it in current thread
				Throwable throwable = (Throwable) throwableReference.get();
				 if (throwable != null) {
						throw new RuntimeException(throwable.getMessage());
				 }
				logger.debug("thread is finished waiting, meaning cmd is finished");
			} catch (InterruptedException e) {
				logger.debug("Interrupted thread running cmd : " + str);
				// current Thread is interrupted, also interrupt thread t running the cmdline process
				t.interrupt();
				// explicit   interrupting runCmd means you want to kill current program even if
				// runcmd is configured to let program continue to run on problems in running cmdline process
				throw new ExceptionAdapter(e);
			} catch (Exception e) {
				// make sure thread t running the cmdline process is also stopped
				t.interrupt();
				throw new ExceptionAdapter(e);
			}
			// note: outstream and errstream are flushed when cmd is finished above in Thread.run method!

//			// flush outstreams to be sure buffer of stream is made empty (note: already flushed after process ended above)
//			try {
//				logger.debug("out/err flush ");
//				if (outstream != null ) outstream.flush();
//				if (errstream != null ) errstream.flush();
//				logger.debug("out/err flush done ");
//			} catch (IOException e) {
//				throw new ExceptionAdapter(e);
//			}

		} else {
			logger.debug("start thread; DO NOT wait for thread running the cmd to be finished");
			t.start();
		}
		logger.debug("leave runCmd function");


		return t;
	}


	public static void listThreads() {
		ThreadGroup rootGroup = Thread.currentThread( ).getThreadGroup( );
		RunCmd.listThreads(rootGroup,System.err,"   ");
	}
	// List all threads and recursively list all subgroup
    // src: http://stackoverflow.com/questions/1323408/get-a-list-of-all-threads-currently-running-in-java
	public static void listThreads(ThreadGroup group, PrintStream stream, String indentStep ) {
    	String indent="";
		stream.println(indent + "Group[" + group.getName() +
                        ":" + group.getClass()+"]");
        int nt = group.activeCount();
        Thread[] threads = new Thread[nt*2 + 10]; //nt is not accurate
        nt = group.enumerate(threads, false);

        // List every thread in the group
        for (int i=0; i<nt; i++) {
            Thread t = threads[i];
            stream.println(indent + "  Thread[" + t.getName()
                        + ":" + t.getClass() + "]  daemon:" + t.isDaemon() );
        }

        // Recursively list all subgroups
        int ng = group.activeGroupCount();
        ThreadGroup[] groups = new ThreadGroup[ng*2 + 10];
        ng = group.enumerate(groups, false);

        for (int i=0; i<ng; i++) {
            listThreads(groups[i], stream, indent + indentStep);
        }
    }

}
