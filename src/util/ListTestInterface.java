package util;

import java.util.List;

public interface  ListTestInterface {
    public <T> boolean test( List<T> list);
}
