
function abspath_of_file() {
  (
  cd $(dirname $1)         # or  cd ${1%/*}
  echo $PWD/$(basename $1) # or  echo $PWD/${1##*/}
  )
}
function abspath_of_dir() {
  (
  cd  $1
  echo $PWD
  )
}



# $0 not set if doing source or .  script
# instead use BASH_ARGV[0]
script_dir=$(dirname ${BASH_ARGV[0]})
echo script_dir=$script_dir

tomte_root_path=$(abspath_of_dir $script_dir/..)
echo  tomte_root_path=$tomte_root_path

export tomte_version=$(cat $tomte_root_path/release-version.txt)

sut_root_path=$(abspath_of_dir $tomte_root_path/../sut-$tomte_version)
echo  sut_root_path=$sut_root_path

export TOMTE_ROOT_PATH=$tomte_root_path
export SUT_ROOT_PATH=$sut_root_path
PATH=$SUT_ROOT_PATH/bin:$TOMTE_ROOT_PATH/bin:$PATH ; export PATH
